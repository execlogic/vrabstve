<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 25.07.18
 * Time: 13:17
 */

// Время работы скрипта
$start = microtime(true);

require_once 'app/MemberInterface.php';
require_once 'app/Member.php';
require_once 'app/PayRollInterface.php';

require_once "admin/RoleInterface.php";
require_once 'admin/User.php';

require_once 'app/Notify.php';

session_start();
if (isset($_SESSION['UserObj'])) {
    $User = $_SESSION['UserObj'];
} else {
    header("Location: index.php");
    exit();
}

if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());


$date_m = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');

if ($_REQUEST['id']) {
    $_REQUEST['id'] = strip_tags($_REQUEST['id']);
    $_REQUEST['id'] = htmlspecialchars($_REQUEST['id']);
    $_REQUEST['id'] = preg_replace("/[^0-9]/i", "", $_REQUEST['id']);
    $user_id = $_REQUEST['id'];


    if (!is_numeric($user_id)) {
        echo "Error!";
        exit();
    }


    $MI = new MemberInterface();
    try {
        $member = $MI->fetchMemberByID($user_id);
    } catch (Exception $e) {
        echo "Нету такого пользователя! " . $e->getMessage();
        exit();
    }

//    $MFB = new MemberFinanceBlock($member);
//    $MFB->fetch();
//    $MFB->fetch_NDFL();
    $PRI = new PayRollInterface();

    if (isset($_POST['payout_date'])) {
        $payout_date = $_POST['payout_date'];
    } else {
        $payout_date = date("Y");
    }

    $payouts = $PRI->getFinancialPayoutByUser($user_id, $payout_date);


    if (is_null($member)) {
        echo "Нету такого пользователя!";
        exit();
    }


    /*****************             ДОСТУПЫ  */
    $member_department_id = $member->getDepartment()['id'];

    $Permission_ProfilePayments = 0;
    $Permission_ProfileAWH = 0; // not implemented yet
    $Permission_ProfileWorkInfo = 0;
    $Permission_ProfilePersonalInfo = 0;

    $Permission_ChangeMember = 0;
    $Permission_ShowHistory = 0;

    $Permission_AvansCalculation = 0;
    $Permission_Avans2Calculation = 0;
    $Permission_PremiyaCalculation = 0;
    $Permission_RaschetCalculation = 0; // not implemented yet

    if ($User->getMemberId() == $user_id) {
        $Permission_ProfilePayments = 1;
        $Permission_ProfileAWH = 1;
        $Permission_ProfileWorkInfo = 1;
        $Permission_ProfilePersonalInfo = 1;

        $Permission_ChangeMember = 1;
        $Permission_ShowHistory = 0;

        $Permission_AvansCalculation = 1;
        $Permission_Avans2Calculation = 1;
        $Permission_PremiyaCalculation = 1;
        $Permission_RaschetCalculation = 1;
    } else {
        // Выплаты
        $current = array_filter($Roles, function ($Role) use ($member_department_id) {
            return (($Role->getProfilePayments() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        });

        if ($current) {
            $Permission_ProfilePayments = 1;
            $current = null;
        };

        // Not implemented
//    $current = array_filter($Roles, function ($Role)  {
//        return ($Role->getProfileAWH()==1);
//    });
//
//    if ($current) {
//        $ProfilePayments = 1;
//        $current = null;
//    };

        // Рабочая информация
        $current = array_filter($Roles, function ($Role) use ($member_department_id) {
            return (($Role->getProfileWorkInfo() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        });

        if ($current) {
            $Permission_ProfileWorkInfo = 1;
            $current = null;
        };

        // Личная информация
        $current = array_filter($Roles, function ($Role) use ($member_department_id) {
            return (($Role->getProfilePersonalInfo() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        });

        if ($current) {
            $Permission_ProfilePersonalInfo = 1;
            $current = null;
        };

        // Изменять пользователя
        $current = array_filter($Roles, function ($Role) use ($member_department_id) {
            return (($Role->getChangeMember() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        });

        if ($current) {
            $Permission_ChangeMember = 1;
            $current = null;
        };

        // Показывать историю
        $current = array_filter($Roles, function ($Role) use ($member_department_id) {
            return (($Role->getShowHistory() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        });

        if ($current) {
            $Permission_ShowHistory = 1;
            $current = null;
        };


        /// Расчет Аванса
        $current = array_filter($Roles, function ($Role) use ($member_department_id) {
            return (($Role->getAvansCalculation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        });

        if ($current) {
            $Permission_AvansCalculation = 1;
            /*
             * Если может расчитывать аванс, то может и аванс2 28.12.2020, заглушка
             */
            $Permission_Avans2Calculation = 1;
            $current = null;
        };

        /// Расчет Аванс 2
        $current = array_filter($Roles, function ($Role) use ($member_department_id) {
            return (($Role->getAvans2Calculation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        });

        if ($current) {
            $Permission_Avans2Calculation = 1;
            $current = null;
        };

        /// Расчет Премия
        $current = array_filter($Roles, function ($Role) use ($member_department_id) {
            return (($Role->getPremiyaCalculation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        });

        if ($current) {
            $Permission_PremiyaCalculation = 1;
            $current = null;
        };

        if ($User->getMemberId() == 94 && $member->getId() == 65) {
            $Permission_ProfilePayments = 1;
            $Permission_ProfileAWH = 1;
            $Permission_ProfileWorkInfo = 1;
            $Permission_ProfilePersonalInfo = 1;

            $Permission_ChangeMember = 1;
            $Permission_ShowHistory = 1;

            $Permission_AvansCalculation = 1;
            $Permission_Avans2Calculation = 1;
            $Permission_PremiyaCalculation = 1;
            $Permission_RaschetCalculation = 1;
        }
    }
    /*****************             Конец доступа ДОСТУПЫ  */

    $chart_labels = array();
    $chart = array();
    $chart_avans = array();
    $chart_premium = array();

    foreach ($payouts as $item) {
        $key = substr($item['date'],0, 7);
        if (!in_array($key, $chart_labels)) {
            $chart_labels[] = $key;
        }

        if (!isset($chart[$key])) {
            $chart[$key] = array("avans" => 0, "premium" => 0);
        }

        if ($item['type_of_payment'] == 1) {
            $chart[$key]["premium"] = $item['Total'];
        } else if ($item['type_of_payment'] == 2) {
            $chart[$key]["avans"] = $item['Total'];
        }
    }

    foreach ($chart as $item) {
        $chart_avans[] = $item['avans'];
        $chart_premium[] = $item['premium'];
    }

    if (isset($_POST['date'])) {
        $date = $_POST['date'];
    } else {
        $date = date("Y");
    }

    switch ($member->getMotivation()){
        case 1:
            $profit = $MI->getProfitByYear($member->getId(), $member->getMotivation(), $date);
            break;
        case 2:
            $profit = $MI->getProfitByYear($member->getId(), $member->getMotivation(), $date);
            break;
        case 4:
            $profit = $MI->getProfitByYear($member->getId(), $member->getMotivation(), $date);
            break;
        case 8:
            $profit = $MI->getProfitByYear($member->getDepartment()['id'], $member->getMotivation(), $date);
            break;
        default:
            break;
    }
//    if ($member->getMotivation() == 1 || $member->getMotivation() == 2) {
//        $profit = $MI->getProfitByYear($member->getId(), $member->getMotivation(), 2019);
//    }

} else {
    echo "Нету такого пользователя!";
    exit();
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Профиль сотрудника</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Profile Styles -->
    <link rel="stylesheet" href="../../css/profile.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">

    <style>
        .layout-boxed .wrapper {
            box-shadow: 0 0 0px rgba(0, 0, 0, 0.5);
        }

        /*body p {*/
            /*font-size: 13px;*/
        /*}*/

        /*ul.sidebar-menu > li {*/
            /*font-size: 12px;*/
        /*}*/
    </style>

</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Профиль сотрудника
            </h1>
            <ol class="breadcrumb">
                <li><a href="member_list.php"><i class="fa fa-dashboard"></i> Список пользователей</a></li>
                <li class="active">Профиль сотрудника</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <?php
                    switch ($member->getWorkstatus()) {
                        case 1:
                            echo "<div class=\"box box-primary\"  style=\"min-height: 515px;\">";
                            break;
                        case 2:
                        case 3:
                            echo "<div class=\"box box-warning\"  style=\"min-height: 515px;\">";
                            break;
                        case 4:
                            echo "<div class=\"box box-danger\"  style=\"min-height: 515px;\">";
                            break;
                        default:
                            echo "<div class=\"box\"  style=\"min-height: 515px;\">";
                            break;
                    }
                    ?>
                    <!--                    <div class="box box-primary">-->
                    <div class="box-body box-profile">
                        <?php
                        if (is_file("images/" . $user_id . "/photo.jpg")) {
                            ?>
                            <img src="/images/<?php echo $user_id; ?>/photo.jpg" class="img-responsive"
                                 style="margin: 0 auto; width: 200px; height: 280px;">
                            <?php
                        } else {
                            echo "<div style='display: block; margin: 0 auto; width: 200px; height: 280px; border: 1px solid #3d8dbc'></div>";
                        }
                        ?>
                        <h3 class="profile-username text-center"><?php echo $member->GetName() . " " . $member->GetLastName(); ?></h3>
                        <?php
                        echo '<p class="text-muted text-center">Должность: ' . $member->getPosition()['name'] . '</p>';
                        switch ($member->getWorkstatus()) {
                            case 1:
                                echo '<p class="text-muted text-center" style="color: green;">';
                                echo "Рабочий статус: работает";
                                echo '</p>';
                                break;
                            case 2:
                                echo '<p class="text-muted text-center" style="color: #f39c12;">';
                                echo "Рабочий статус: помечен на увольнение";
                                echo '</p>';
                                break;
                            case 3:
                                echo '<p class="text-muted text-center" style="color: #f39c12;">';
                                echo "Рабочий статус: декрет";
                                echo '</p>';
                                break;
                            case 4:
                                echo '<p class="text-muted text-center"  style="color: red;">';
                                echo "Рабочий статус: уволен " . $member->getDismissalDate();
                                echo '</p>';
                                break;
                            default:
                                echo "<p>Ошибка статуса</p>";
                                break;
                        }
                        ?>
                    </div>

                    <div class="box-footer">
                        <div class="row">
                            <?php if ($Permission_ShowHistory == 0) { ?>
                            <div class="col-md-12" style="padding-bottom: 12px">
                                <?php } else { ?>
                                <div class="col-md-9" style="padding-bottom: 12px">
                                    <?php }; ?>
                                    <?php
                                    if ($Permission_ChangeMember == 1) {
                                        ?>
                                        <a href="edit.php?id=<?php echo $user_id; ?>" class="btn btn-block btn-flat"
                                           style="background-color: rgba(0,192,239,0.1); border-color: #00acd6;">Изменить</a>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                                if ($Permission_ShowHistory == 1) {
                                    ?>
                                    <div class="col-md-3" style="padding: 0 12px 6px 2px">
                                        <a href="history.php?id=<?php echo $user_id; ?>" class="btn btn-block btn-flat"
                                           style="background-color: rgba(0,192,239,0.1); border-color: #00acd6;" title="История изменений"><span class="fa fa-history"></span></a>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                            <div class="row">
                                <?php
                                if ($Permission_AvansCalculation == 1 || $Permission_Avans2Calculation == 1 ||
                                    $Permission_PremiyaCalculation == 1 || $Permission_RaschetCalculation == 1) {
                                    ?>
                                    <div class="col-md-12">
                                        <?php
                                        //                                    if (($member->getWorkstatus() != 4) && ($member->getWorkstatus() != 3)) {
                                        ?>
                                        <a class="btn btn-block btn-flat dropdown-toggle" data-toggle="dropdown"
                                           style="background-color: rgba(0,192,239,0.1); border-color: #00acd6;"> Тип расчета <span class="caret"></span></a>
                                        <ul class="dropdown-menu" style="width: 100%; background-color: #fff; border-color: #3d8dbb; border-radius: 0">
                                            <li>
                                                <div class="col-sm-12">
                                                    <?php

                                                    if (($member->getWorkstatus() == 1) && ($Permission_AvansCalculation == 1) && ($member->getAvansPay() == 1)) {
                                                        if ($member->getPosition()['id'] == 34) {
                                                            echo "<a class=\"btn btn-block btn-flat btn-info\" href=\"#\" style=\"margin-top: 5px;\" disabled='disabled'>Аванс</a>";
                                                        } else {
                                                            echo "<a class=\"btn btn-block btn-flat btn-info\" href=\"salary.php?id=" . $user_id . "\" style=\"margin-top: 5px;\">Аванс</a>";
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (($member->getWorkstatus() == 1) && ($Permission_Avans2Calculation == 1)) {
                                                        echo "<a class=\"btn btn-block btn-flat btn-info\" href=\"salary2.php?id=" . $user_id . "\"
                                               style=\"margin-top: 5px;\">Аванс2</a>";
                                                    }
                                                    ?>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (($member->getWorkstatus() == 1) && ($Permission_PremiyaCalculation == 1)) {
                                                        echo "<a class=\"btn btn-block btn-flat btn-info\" href=\"premium.php?id=" . $user_id . "\"
                                               style=\"margin-top: 5px;\">Премия</a>";
                                                    }
                                                    ?>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (($member->getWorkstatus() == 1)) {
                                                        echo "<a class=\"btn btn-block btn-flat btn-info\" href='unscheduled.php?id=" . $user_id . "' style=\"margin-top: 5px;\">Внеплановая выплата</a>";
                                                    }
                                                    ?>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-sm-12">
                                                    <?php
                                                    if ($member->getWorkstatus() != 1) {
                                                        echo "<a class=\"btn btn-block btn-flat btn-warning\" style=\"margin-top: 5px;\" href=\"leaving_premium.php?id=" . $user_id . "\"> Расчет </a>";
                                                    } else {
                                                        echo "<a class=\"btn btn-block btn-flat btn-warning\" style=\"margin-top: 5px;\" disabled='disabled'> Расчет </a>";
                                                    }
                                                    ?>
                                                </div>
                                            </li>
                                        </ul>
                                        <?php
                                        //                                    }
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-lg-9 col-md-8">
                    <?php
                    switch ($member->getWorkstatus()) {
                        case 1:
                            echo "<div class=\"box box-primary\">";
                            break;
                        case 2:
                        case 3:
                            echo "<div class=\"box box-warning\">";
                            break;
                        case 4:
                            echo "<div class=\"box box-danger\">";
                            break;
                        default:
                            echo "<div class=\"box\">";
                            break;
                    }
                    ?>
                    <!--                    <div class="box">-->
                    <div class="box-header">
                        <h3>
                            <?php
                            if ($member->getMaidenName()) {
                                echo $member->getLastName() . " ( " . $member->getMaidenName() . " ) " . $member->getName() . " " . $member->getMiddle();
                            } else {
                                echo $member->getLastName() . " " . $member->getName() . " " . $member->getMiddle();
                            };
                            ?>
                        </h3>
                    </div>
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-5"><p class="text-muted">Дата рождения: </p></div>
                            <div class="col-md-7"><p><?php echo $member->getBirthday(); ?></p></div>
                        </div>

                        <div class="row">
                            <div class="col-md-5"><p class="text-muted">Пол: </p></div>
                            <div class="col-md-7"><p><?php echo $member->SexToText(); ?></p></p></div>
                        </div>

                        <div class="profile_info_block clear_fix">
                            <div class="profile_info_header_wrap">
                                <span class="profile_info_header">Рабочая информация</span>
                            </div>

                            <div class="row">
                                <div class="col-md-5"><p class="text-muted">Направление: </p></div>
                                <div class="col-md-5"><p><?php echo $member->getDirection()['name']; ?></p></div>
                                <div class="col-md-2"><p class="text-muted small"><?php echo $member->getDirection()['date']; ?></p></div>
                            </div>
                            <div class="row">
                                <div class="col-md-5"><p class="text-muted">Отдел: </p></div>
                                <div class="col-md-5"><p><?php echo $member->getDepartment()['name']; ?></p></div>
                                <div class="col-md-2"><p class="text-muted small"><?php echo $member->getDepartment()['date']; ?></p></div>
                            </div>
                            <div class="row">
                                <div class="col-md-5"><p class="text-muted">Должность: </p></div>
                                <div class="col-md-5"><p><?php echo $member->getPosition()['name']; ?></p></div>
                                <div class="col-md-2"><p class="text-muted small"><?php echo $member->getPosition()['date']; ?></p></div>
                            </div>

                        </div>

                        <div class="profile_info_block clear_fix">
                            <div class="profile_info_header_wrap">
                                <span class="profile_info_header">Телефоны</span>
                            </div>

                            <div class="row">
                                <?php
                                echo '<div class="col-md-5"><p class="text-muted">Рабочий номер: </p></div>';
                                echo '<div class="col-md-7 "><div class="row">';
                                if ($member->getWorkphone()!==null) {
                                    foreach ($member->getWorkphone() as $value) {
                                        echo '<div class="col-md-12"><p><a href="tel:' . $value['phone'] . '">' . '(' . substr($value['phone'], 0, 3) . ')' . substr($value['phone'], 3, 3) . '-' . substr($value['phone'], 6, 2) . '-' . substr($value['phone'], 8, 2) . '</a></p></div>';
                                    }
                                }
                                echo '</div></div>';
                                ?>
                            </div>


                            <div class="row">
                                <?php
                                echo '<div class="col-md-5"><p class="text-muted">Добавочный номер: </p></div>';
                                echo '<div class="col-md-7 "><div class="row">';
                                if ($member->getWorklocalphone()) {
                                    echo '<div class="col-md-12"><p><a href="tel:' . $member->getWorklocalphone()['phone'] . '">' . $member->getWorklocalphone()['phone'] . '</a></p></div>';
                                }
                                echo '</div></div>';
                                ?>
                            </div>

                            <div class="row">
                                <?php
                                echo '<div class="col-md-5"><p class="text-muted">Рабочий мобильный номер: </p></div>';
                                echo '<div class="col-md-7 "><div class="row">';
                                if ($member->getWorkmobilephone()!==null) {
                                    foreach (($member->getWorkmobilephone()) as $value) {
                                        echo '<div class="col-md-12"><p><a href="tel:' . $value['phone'] . '">' . '(' . substr($value['phone'], 0, 3) . ')' . substr($value['phone'], 3, 3) . '-' . substr($value['phone'], 6, 2) . '-' . substr($value['phone'], 8, 2) . '</a></p></div>';
                                    }
                                }
                                echo '</div></div>';
                                ?>
                            </div>

                            <div class="row">
                                <?php
                                echo '<div class="col-md-5"><p class="text-muted">Личный мобильный номер: </p></div>';
                                echo '<div class="col-md-7 "><div class="row">';
                                if ($member->getPersonalmobilephone()!==null) {
                                    foreach ($member->getPersonalmobilephone() as $value) {
                                        echo '<div class="col-md-12"><p><a href="tel:' . $value['phone'] . '">' . '(' . substr($value['phone'], 0, 3) . ')' . substr($value['phone'], 3, 3) . '-' . substr($value['phone'], 6, 2) . '-' . substr($value['phone'], 8, 2) . '</a></p></div>';
                                    }
                                }
                                echo '</div></div>';
                                ?>
                            </div>
                        </div>

                        <div class="profile_info_block clear_fix">
                            <div class="profile_info_header_wrap">
                                <span class="profile_info_header">Почта</span>
                            </div>
                            <div class="row">
                                <?php
                                echo '<div class="col-md-5"><p class="text-muted">Почтовые адреса: </p></div>';
                                echo '<div class="col-md-7 "><div class="row">';
                                if ($member->getEmails()!==null) {
                                    foreach ($member->getEmails() as $value) {
                                        echo '<div class="col-md-6"><p><a href="mailto:' . $value['email'] . '">' . $value['email'] . '</a></p></div>';
                                        echo '<div class="col-md-6"><p>' . $value['note'] . '</p></div>';
                                    }
                                }
                                echo '</div></div>';
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?php
            if ($Permission_ProfileWorkInfo == 1) {
                ?>
                <!--                Рабочая информация-->
                <div class="box box-solid">
                    <div class="box-header">
                        <span>Рабочая информация</span>
                    </div>
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-5"><p class="text-muted">Логин: </p></div>
                            <div class="col-md-7">
                                <p>
                                    <?php
                                    echo $member->getLogin();
                                    ?>
                                </p>
                            </div>
                        </div>

                        <div class="profile_info_block clear_fix">
                            <div class="profile_info_header_wrap">
                                <span class="profile_info_header">Информация по 1C</span>
                            </div>

                            <div class="row">
                                <div class="col-md-5"><p class="text-muted">Табельный номер ЗИК: </p></div>
                                <div class="col-md-7">
                                    <p>
                                        <?php
                                        echo $member->getPersonalnumber();
                                        ?>
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5"><p class="text-muted">1С7.7 Склад </p></div>
                                <div class="col-md-7">
                                    <p>
                                        <?php
                                        echo $member->getSkladName();
                                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="profile_info_block clear_fix">
                            <div class="profile_info_header_wrap">
                                <span class="profile_info_header">Телефон и почта</span>
                            </div>

                            <div class="row">
                                <?php
                                echo '<div class="col-md-5"><p class="text-muted">Рабочий телефон: </p></div>';
                                echo '<div class="col-md-7 "><div class="row">';
                                if ($member->getWorkphone() !== null) {
                                    foreach ($member->getWorkphone() as $value) {
                                        echo '<div class="col-md-12"><p><a href="tel:' . $value['phone'] . '">' . '(' . substr($value['phone'], 0, 3) . ')' . substr($value['phone'], 3, 3) . '-' . substr($value['phone'], 6, 2) . '-' . substr($value['phone'], 8, 2) . '</a></p></div>';
                                    }
                                }
                                echo '</div></div>';
                                ?>
                            </div>

                            <div class="row">
                                <?php
                                echo '<div class="col-md-5"><p class="text-muted">Почтовые адреса: </p></div>';
                                echo '<div class="col-md-7 "><div class="row">';
                                if ($member->getEmails() !== null) {
                                    foreach ($member->getEmails() as $value) {
                                        echo '<div class="col-md-6"><p><a href="mailto:' . $value['email'] . '">' . $value['email'] . '</a></p></div><div class="col-md-6"><p>' . $value['note'] . '</p></div>';
                                    }
                                }
                                echo '</div></div>';
                                ?>
                            </div>
                        </div>

                        <div class="profile_info_block clear_fix">
                            <div class="profile_info_header_wrap">
                                <span class="profile_info_header">Прием/Увольнение</span>
                            </div>

                            <div class="row">
                                <div class="col-md-5"><p class="text-muted">Принят на работу: </p></div>
                                <div class="col-md-7">
                                    <p>
                                        <?php
                                        echo $member->getEmploymentDate();
                                        ?>
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5"><p class="text-muted">Уволен: </p></div>
                                <div class="col-md-7">
                                    <p>
                                        <?php
                                        if ($member->getDismissalDate() != "01.01.9999") {
                                            echo $member->getDismissalDate();
                                        }
                                        ?>
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5"><p class="text-muted">Причина увольнения: </p></div>
                                <div class="col-md-7">
                                    <p>
                                        <?php
                                        echo $member->getReasonLeaving();
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5"><p class="text-muted">Заметка при увольнении: </p></div>
                                <div class="col-md-7">
                                    <p>
                                        <?php
                                        echo $member->getReasonLeavingNote();
                                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }

            if ($Permission_ProfilePersonalInfo == 1) {
                ?>
                <!--                Личная информация-->
                <div class="box box-solid">
                    <div class="box-header">
                        <span>Личная информация</span>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-5"><p>Национальность</p></div>
                            <div class="col-md-7"><p><?php echo $member->getNationName(); ?></p></div>
                        </div>

                        <div class="profile_info_block clear_fix">
                            <div class="profile_info_header_wrap">
                                <span class="profile_info_header">Личные контактные данные</span>
                            </div>

                            <div class="row">
                                <?php
                                echo '<div class="col-md-5"><p class="text-muted">Телефон: </p></div>';
                                echo '<div class="col-md-7"><div class="row">';
                                if ($member->getPersonalphone()!==null) {
                                    foreach ($member->getPersonalphone() as $value) {
                                        echo '<div class="col-md-12"><p><a href="tel:' . $value['phone'] . '">' . '(' . substr($value['phone'], 0, 3) . ')' . substr($value['phone'], 3, 3) . '-' . substr($value['phone'], 6, 2) . '-' . substr($value['phone'], 8, 2) . ' </a></p></div>';
                                    }
                                }
                                echo '</div></div>';
                                ?>
                            </div>

                            <div class="row">
                                <?php
                                echo '<div class="col-md-5"><p class="text-muted">Мобильный телефон: </p></div>';
                                echo '<div class="col-md-7"><div class="row">';
                                if ($member->getPersonalmobilephone() !== null) {
                                    foreach ($member->getPersonalmobilephone() as $value) {
                                        echo '<div class="col-md-12"><p><a href="tel:' . $value['phone'] . '">' . '(' . substr($value['phone'], 0, 3) . ')' . substr($value['phone'], 3, 3) . '-' . substr($value['phone'], 6, 2) . '-' . substr($value['phone'], 8, 2) . ' </a></p></div>';
                                    }
                                }
                                echo '</div></div>';
                                ?>
                            </div>

                            <div class="row">
                                <?php
                                echo '<div class="col-md-5"><p class="text-muted">Аварийный телефон: </p></div>';
                                echo '<div class="col-md-7"><div class="row">';
                                if ($member->getEmergencyphone() !== null) {
                                    foreach ($member->getEmergencyphone() as $value) {
                                        echo '<div class="col-md-6"><a href="tel:' . $value['phone'] . '">' . '(' . substr($value['phone'], 0, 3) . ')' . substr($value['phone'], 3, 3) . '-' . substr($value['phone'], 6, 2) . '-' . substr($value['phone'], 8, 2) . ' </a></div><div class="col-md-6"><p>' . $value['note'] . '</p></div>';
                                    }
                                }
                                echo '</div></div>';
                                ?>
                            </div>

                        </div>

                    </div>
                </div>
                <?php
            }

            if ($Permission_ProfilePayments == 1) {
                ?>
                <div class="box box-solid">
                    <div class="box-header">
                        <span>График выплат</span>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="lineChart" style="height:250px"></canvas>
                        </div>
                    </div>
                </div>

                <!--                    Выплаты-->
            <form method="post" class="form-horizontal">
                <div class="box box-solid">
                    <div class="box-header">
                        <span>Выплаты</span>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Год: </label>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker_payment" name="payout_date" data-inputmask="'alias': 'yyyy'" data-mask=""
                                           value="<?php echo $payout_date; ?>" autocomplete="off">
                                </div>

                            </div>
                            <div class="col-sm-1">
                                <input type="submit" class="btn btn-sm btn-flat btn-default" name="SendPayment" value="Выбрать">
                            </div>
                        </div>

                        <table class="table table-striped table-hover">
                            <thead>
                            <th style='text-align: center;'>Дата</th>
                            <th style='text-align: center;'>Тип выплаты</th>
                            <th style='text-align: center;'>Оклад</th>
                            <th style='text-align: center;'>Отсутствия</th>
                            <th style='text-align: center;'>НДФЛ</th>
                            <th style='text-align: center;'>ИЛ</th>
                            <th style='text-align: center;'>Ком. премия</th>
                            <th style='text-align: center;'>KPI</th>
                            <th style='text-align: center;'>Адм. премия</th>
                            <th style='text-align: center;'>Удержания</th>
                            <th style='text-align: center;'>Корретировки</th>
                            <th style='text-align: center;'>Сумма</th>
                            </thead>
                            <tbody>
                            <?php
                            if (isset($payouts) && count($payouts) > 0) {
                                $sumSalary = 0;
                                $sumAbsence = 0;
                                $sumNDFL = 0;
                                $sumRO = 0;
                                $sumCommercialPremium = 0;
                                $sumKPI = 0;
                                $sumAdministrativePremium = 0;
                                $sumHold = 0;
                                $sumCorrecting = 0;
                                $sumTotal = 0;
                                foreach ($payouts as $item) {
                                    $sumSalary += $item['Salary'];
                                    $sumAbsence += $item['absence'];
                                    $sumNDFL += $item['ndfl'];
                                    $sumRO += $item['ro'];
                                    $sumCommercialPremium += $item['CommercialPremium'];
                                    $sumKpi += $item['Kpi'];
                                    $sumAdministrativePremium += $item['AdministrativePremium'];
                                    $sumHold += $item['Hold'];
                                    $sumCorrecting += $item['Correcting'];
                                    $sumTotal += $item['Total'];
                                    switch ($item['type_of_payment']) {
                                        case 1:
                                            $type = "Премия";
                                            break;
                                        case 2:
                                            $type = "Аванс";
                                            break;
                                        case 3:
                                            $type = "Аванс2";
                                            break;
                                        case 4:
                                            $type = "Расчет";
                                            break;
                                        case 5:
                                            $type = "Внеплановая выплата";
                                            break;
                                        case 6:
                                            $type = "Годовой бонус";
                                            break;
                                    }
                                    echo "<tr>";
                                    $tmp = explode("-", $item['date']);
                                    echo "<td style='text-align: left;'>" . $date_m[(int)$tmp[1] - 1] . " " . $tmp[0] . "</td>";
                                    echo "<td style='text-align: left;'>" . $type . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($item['Salary'], 2, ',', ' ') . " &#8381;</td>";
                                    echo "<td style='text-align: right;'>" . number_format($item['absence'], 2, ',', ' ') . " &#8381;</td>";
                                    echo "<td style='text-align: right;'>" . number_format($item['ndfl'], 2, ',', ' ') . " &#8381;</td>";
                                    echo "<td style='text-align: right;'>" . number_format($item['ro'], 2, ',', ' ') . " &#8381;</td>";
                                    echo "<td style='text-align: right;'>" . number_format($item['CommercialPremium'], 2, ',', ' ') . " &#8381;</td>";
                                    echo "<td style='text-align: right;'>" . number_format($item['Kpi'], 2, ',', ' ') . " &#8381;</td>";
                                    echo "<td style='text-align: right;'>" . number_format($item['AdministrativePremium'], 2, ',', ' ') . " &#8381;</td>";
                                    echo "<td style='text-align: right;'>" . number_format($item['Hold'], 2, ',', ' ') . " &#8381;</td>";
                                    echo "<td style='text-align: right;'>" . number_format($item['Correcting'], 2, ',', ' ') . " &#8381;</td>";
                                    echo "<td style='text-align: right;'>" . number_format($item['Total'], 2, ',', ' ') . " &#8381;</td>";
                                    echo "</tr>";
                                }
                            }
                            ?>
                            <tr>
                                <th>Итого:</th>
                                <th></th>
                                <th style='text-align: right;'><?=number_format($sumSalary, 2, ',', ' ')?>&#8381;</th>
                                <th style='text-align: right;'><?=number_format($sumAbsence, 2, ',', ' ')?> &#8381;</th>
                                <th style='text-align: right;'><?=number_format($sumNDFL, 2, ',', ' ')?> &#8381;</th>
                                <th style='text-align: right;'><?=number_format($sumRO, 2, ',', ' ')?> &#8381;</th>
                                <th style='text-align: right;'><?=number_format($sumCommercialPremium, 2, ',', ' ')?> &#8381;</th>
                                <th style='text-align: right;'><?=number_format($sumKpi, 2, ',', ' ')?> &#8381;</th>
                                <th style='text-align: right;'><?=number_format($sumAdministrativePremium, 2, ',', ' ')?> &#8381;</th>
                                <th style='text-align: right;'><?=number_format($sumHold, 2, ',', ' ')?> &#8381;</th>
                                <th style='text-align: right;'><?=number_format($sumCorrecting, 2, ',', ' ')?> &#8381;</th>
                                <th style='text-align: right;'><?=number_format($sumTotal, 2, ',', ' ')?> &#8381;</th>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </form>

                <!--                    Наценка -->
            <?php
                if (isset($profit)) {
                    ?>
                    <form method="post" class="form-horizontal">
                        <div class="box box-solid">
                        <div class="box-header">
                            <span>Наценка</span>
                        </div>
                        <div class="box-body">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Год: </label>
                                <div class="col-sm-5">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="datepicker" name="date" data-inputmask="'alias': 'yyyy'" data-mask=""
                                               value="<?php echo $date; ?>" autocomplete="off">
                                    </div>

                                </div>
                                <div class="col-sm-1">
                                    <input type="submit" class="btn btn-sm btn-flat btn-default" name="Send" value="Выбрать">
                                </div>
                            </div>

                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Дата</th>
                                    <?php
                                    if ($member->getMotivation() == 2)
                                        echo "<th>Отдел</th>";
                                    ?>
                                    <th>Наценка (руб)</th>
                                    <th>Бонусы (руб)</th>
                                    <th>Корректировки (y.e)</th>
                                    <th>Итого (руб)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $p = $b = $c = $f = 0;
                                foreach ($profit as $item) {
                                    $p += $item['p'];
                                    $b += $item['b'];
                                    $c += $item['c'];
                                    $f += $item['f'];
                                    $tmp = explode("-", $item['date']);

                                    echo "<tr>";
                                    echo "<td>" . $date_m[(int)$tmp[1] - 1] . " " . $tmp[0] . "</td>";
                                    if ($member->getMotivation() == 2)
                                        echo "<td>". $item['name']. "</td>";
                                    echo "<td>" . number_format($item['p'], 2, ',', ' ') . "</td>";
                                    echo "<td>" . number_format($item['b'], 2, ',', ' ') . "</td>";
                                    echo "<td>" . number_format($item['c'], 2, ',', ' ') . "</td>";
                                    echo "<td>" . number_format($item['f'], 2, ',', ' ') . "</td>";
                                    echo "</tr>";
                                }
                                echo "<tfoot><tr>";
                                echo "<th>Итого: </th>";
                                if ($member->getMotivation() == 2)
                                    echo "<td></td>";
                                echo "<th>" . number_format($p, 2, ',', ' ') . "</th>";
                                echo "<th>" . number_format($b, 2, ',', ' ') . "</th>";
                                echo "<th>" . number_format($c, 2, ',', ' ') . "</th>";
                                echo "<th>" . number_format($f, 2, ',', ' ') . "</th>";
                                echo "</tfoot></tr>";

                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </form>
                    <?php
                }
                    ?>

                <!-- /.box -->
                <?php
            }
            ?>
        </div>
    </div>
    </section>
</div>
<?php require_once 'footer.php'; ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../bower_components/chart.js/Chart.js"></script>

<script>
    $(function () {
        $(function () {
            $('#datepicker').datepicker({
                language: 'ru',
                autoclose: true,
                viewMode: "years",
                minViewMode: "years",
                format: 'yyyy'
            })

            $('#datepicker_payment').datepicker({
                language: 'ru',
                autoclose: true,
                viewMode: "years",
                minViewMode: "years",
                format: 'yyyy'
            })
        });

        // Get context with jQuery - using jQuery's .get() method.
        var lineChartCanvas = $('#lineChart').get(0).getContext('2d')
        // This will get the first returned node in the jQuery collection.
        var lineChart = new Chart(lineChartCanvas)

        var lineChartData = {
            labels: <?php echo json_encode($chart_labels); ?>,
            datasets: [
                {
                    label: 'Аванс',
                    fillColor: 'rgba(60,141,188,0.9)',
                    strokeColor: 'rgba(60,141,188,0.8)',
                    pointColor: '#3b8bba',
                    pointStrokeColor: 'rgba(60,141,188,1)',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data: <?php echo json_encode($chart_avans);?>,

                },
                {
                    label: 'Премия',
                    fillColor: 'rgba(188,60,60,0.9)',
                    strokeColor: 'rgba(188,60,60,0.8)',
                    pointColor: '#a50000',
                    pointStrokeColor: 'rgba(188,60,60,1)',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(188,60,60,1)',
                    data: <?php echo json_encode($chart_premium);?>,
                }
            ]
        }

        var lineChartOptions = {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: 'rgba(0,0,0,.05)',
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.05,
            //Boolean - Whether to show a dot for each point
            pointDot: true,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 1,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1.5,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 10,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: false,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 1,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: false,
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            //String - A legend template
            // legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        }

        //Create the line chart
        lineChart.Line(lineChartData, lineChartOptions)
    })
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
