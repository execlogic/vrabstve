<?php

require_once 'app/functions.php';

require_once 'app/MemberInterface.php';
require_once 'app/CustomPercent.php';

class Convert
{
    private $dbh = NULL;
    private $member_list = array();
    private $Procents = array();
    private $date_s = null;
    private $type = NULL;
    private $adv_procent_id = NULL;

    public function __construct()
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
    }

    public function get_members() {
        $MI = new MemberInterface();
        $MI->fetchMembers();
        foreach ($MI->GetMembers() as $item) {
            $this->member_list[] = $item->getId();
        }
        $MI = null;
//        if ($this->type == 1) {
//            $query = "SELECT member_id FROM zp.member_cp_statgroup group by member_id";
//        } else {
//            $query = "SELECT member_id FROM zp.member_cp_department group by member_id";
//        }
//        $sth = $this->dbh->prepare($query);
//        $sth->execute();
//        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
//
//        foreach ($raw as $item) {
//            $this->member_list[] = $item['member_id'];
//        }
    }

    public function getoldProcents($member_id) {
        unset($this->Procents);
        $this->Procents = array();

        if ($this->type == 1) {
            $query = "select * from member_cp_statgroup where member_id = ? and active = 1";
//            echo $query."<BR>";

            $sth = $this->dbh->prepare($query);
            $sth->execute([$member_id]);
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

            foreach ($raw as $item) {
                $this->Procents[$item['statgroup_id']] = $item['percent'];
                $this->date_s = $item['date_s'];
//                echo $item['statgroup_id']." ".$item['percent']."<BR>";
            }
        } else {
            $query = "select * from member_cp_department where member_id = ? and active = 1";
            $sth = $this->dbh->prepare($query);
            $sth->execute([$member_id]);
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

            foreach ($raw as $item) {
                $this->Procents[$item['department_id']] = $item['percent'];
                $this->date_s = $item['date_s'];
            }
        }
    }

//    public function getoldProcents2($member_id) {
//        unset($this->Procents);
//        $this->Procents = array();
//
//        echo $member_id."<BR>";
//
//        if ($this->type == 1) {
//
//            $query = "SELECT date_s, date_e FROM zp.member_cp_statgroup where member_id=? group by date_s";
//            $sth = $this->dbh->prepare($query);
//            $sth->execute([$member_id]);
//            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
//
//
//
//            $query = "select * from member_cp_statgroup where member_id = ? and (date_s >= '0000-00-00' AND date_s <= STR_TO_DATE(?,'%d.%m.%Y'))";
//            $sth = $this->dbh->prepare($query);
//            $sth->execute([$member_id]);
//            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
//
//            foreach ($raw as $item) {
//                $this->Procents[$item['statgroup_id']] = $item['percent'];
//                $this->date_s = $item['date_s'];
//                echo $item['statgroup_id']." ".$item['percent']."<BR>";
//            }
//        } else {
//            $query = "select * from member_cp_department where member_id = ? and active = 1";
//            $sth = $this->dbh->prepare($query);
//            $sth->execute([$member_id]);
//            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
//
//            foreach ($raw as $item) {
//                $this->Procents[$item['department_id']] = $item['percent'];
//                $this->date_s = $item['date_s'];
//            }
//        }
//    }

    public function newProcent($member_id) {
        $this->adv_procent_id = NULL;
        $serializeProcent = serialize($this->Procents);
        $Hash = hash('md5', $serializeProcent);

        $query = "select id,hash from member_percent_advanced where member_id = ? and type=? and active=1 LIMIT 1";
        $sth = $this->dbh->prepare($query);

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$member_id, $this->type]);
        $count = $sth->rowCount();

        if ($count > 0) {
            /*
             * Если хеши не совпадают, то это значения обновлены
             */
            $raw = $sth->fetch(PDO::FETCH_ASSOC);
            if ($raw['hash'] != $Hash) {
                $query = "update member_percent_advanced SET active=0, date_e=NOW(),changer_id=?, change_date=NOW() where member_id=? and type=?";
                $sth = $this->dbh->prepare($query);
                $sth->execute([-1, $member_id, $this->type]);

                $query = "insert into member_percent_advanced (member_id, type, procents, hash, date_s, changer_id) VALUES (?, ?, ?, ?, ?, ?)";
                $sth = $this->dbh->prepare($query);
                $sth->execute([$member_id, $this->type, $serializeProcent, $Hash, $this->date_s, -1]);

                $this->adv_procent_id = $this->dbh->lastInsertId();
            } else {
                $this->adv_procent_id = $raw['id'];
            }
        } else {
            /*
             * Значения еще не были занесены, заносим их
             */
            $query = "insert into member_percent_advanced (member_id, type, procents, hash, date_s, changer_id) VALUES (?, ?, ?, ?, ?, ?)";
//            echo $query."<BR>";
//            echo $member_id.", ".$this->type.", ".$serializeProcent.", ".$Hash.", ".$this->date_s.", -1 <BR>";
            $sth = $this->dbh->prepare($query);
            $sth->execute([$member_id, $this->type, $serializeProcent, $Hash, $this->date_s, -1]);
            $this->adv_procent_id = $this->dbh->lastInsertId();
        }
    }

    public function start() {
        $this->get_members();
        foreach ($this->member_list as $member_id) {
            $this->convert_financial_payout($member_id);
//            $this->getoldProcents($member_id);
//            $this->newProcent($member_id);

        }
        echo "Finish<BR>";
    }

    public function convert_financial_payout($member_id) {
        $this->adv_procent_id = NULL;
        $procent_id = NULL;
        $job = NULL;
        $motivation = NULL;

        $query = "select * from financial_payout where member_id=?";
        $sth = $this->dbh->prepare($query);
        $sth->execute([$member_id]);
        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        foreach ($raw as $item) {
            $date_tmp = explode("-", $item['date']);
            $date = $date_tmp[1].".".$date_tmp[0];
            $Member = new MemberInterface();
            $Member->fetchMemberByID($member_id);
            $motivation = $Member->getMotivationByDate($member_id, $date);

            switch ($motivation){
                case 2:
                case 3:
                    $this->type = 2;
                    break;
                default:
                    $this->type = 1;
                    break;
            }

            $job = $Member->getJobPostByDate($member_id, $date);

            $this->getoldProcents($member_id);
            $this->newProcent($member_id);

            $query_subquery = "select id, percent from member_percent WHERE 
            member_id=? AND 
            (date_s BETWEEN '0000-00-00' AND DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'), INTERVAL 1 MONTH) ) and (date_e BETWEEN DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'),INTERVAL 1 MONTH ) AND '9999-01-01') 
            order by id desc LIMIT 1";

            try {
                $sth = $this->dbh->prepare($query_subquery);
                $sth->execute([$member_id, "01.".$date, "01.".$date]);
            } catch (Exception $e) {
                throw new Exception("Ошибка в получении причины увольнения." . $e->getMessage());
            }
            $raw = $sth->fetch(PDO::FETCH_ASSOC);
            if ($raw) {
                $procent = $raw['percent'];
                $procent_id = $raw['id'];
            } else {
                $procent = NULL;
                $procent_id = NULL;
            }

            echo "Дата: ". $date ." Member_ID: ". $member_id. " Motivation: ". $motivation. " Job: ". $job['direction_id']." " .$job['department_id']." ".
                $job['position_id']." Procent: ". $procent ." Procent ID: ". $procent_id." AdvProcent_ID: ". $this->adv_procent_id
                ."<BR>";

            $query = "UPDATE financial_payout SET direction_id = ?, department_id = ?, position_id=?, member_advanced_percent_id=?, member_percent_id=?, motivation=? where id=?";
            try {
                $sth = $this->dbh->prepare($query);
                if (!$sth->execute([$job['direction_id'], $job['department_id'], $job['position_id'], $this->adv_procent_id, $procent_id, $motivation, $item['id']])) {
                    echo "Error: ".$sth->errorInfo()[2]."<BR>";
                }
            } catch (Exception $e) {
                throw new Exception("Ошибка в получении причины увольнения." . $e->getMessage());
            }

            $query = "update administrative_block SET administrative_block.lock=1 where member_id=? and type_of_payment=1 and date=STR_TO_DATE(?, '%m.%Y')";
            $sth = $this->dbh->prepare($query);
            $sth->execute([$member_id, $date]);

            $query = "update correcting_block SET correcting_block.lock=1 where member_id=? and type_of_payment=1 and date=STR_TO_DATE(?, '%m.%Y')";
            $sth = $this->dbh->prepare($query);
            $sth->execute([$member_id, $date]);

            $query = "update retention_block SET retention_block.lock=1 where member_id=? and type_of_payment=1 and date=STR_TO_DATE(?, '%m.%Y')";
            $sth = $this->dbh->prepare($query);
            $sth->execute([$member_id, $date]);


            $query = "update kpi_block SET kpi_block.lock=1 where member_id=? and type_of_payment=1 and date=STR_TO_DATE(?, '%m.%Y')";
            $sth = $this->dbh->prepare($query);
            $sth->execute([$member_id, $date]);


            $query = "update member_awh SET member_awh.lock=1 where member_id=? and type_of_payment=1 and date=STR_TO_DATE(?, '%m.%Y')";
            $sth = $this->dbh->prepare($query);
            $sth->execute([$member_id, $date]);
        }

    }

}

/*
 * Это не тип ведомости, это тип расширенных процентов
 */
$CV = new Convert();
$CV->start();

?>