<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.10.18
 * Time: 18:30
 */

// Время работы скрипта
$start = microtime(true);

require_once 'app/MemberInterface.php';
require_once 'app/AwhInterface.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

// Настройки

$Permission_ReportMemberAWH = 0;
$RoleDepartmentFilter = array();
foreach ($Roles as $Role) {
    if ($Role->getReportMemberAWH() == 1) {
        $Permission_ReportMemberAWH = 1;

        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[] = $Role->getDepartment();
        }
    }
}

if ($Permission_ReportMemberAWH == 0) {
    header("Location: 404.php");
    exit();
}


/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

$date_m = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
$AWH = new AwhInterface();

$MI = new MemberInterface();
$MI->fetchMembers(true,null,true);

//var_dump($AWH_Data);
$date = date("Y");
$AWHData = array();

if (isset($_POST['SendDate']) && isset($_POST['date']) && isset($_POST['MemberId'])) {
    $date = $_POST['date'];
    $Member_id = $_POST['MemberId'];
    $NDFLData = $AWH->getNdflByMemberYear($Member_id, $date);
}

$menu_open = 3;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Журнал НДФЛ + ИЛ по сотруднику</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
                Журнал НДФЛ + ИЛ по сотруднику
            </h4>
            <ol class="breadcrumb">
                <li class="active">Журнал ведомостей</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="box box-solid box-plane">
                <div class="box-body">
                    <form class="form-horizontal" method="post">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Выберите дату c: </label>
                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker" name="date" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                           value="<?php echo $date; ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Сотрудники: </label>
                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-check"></i>
                                    </div>
                                    <select class="form-control" name="MemberId">
                                        <?php
                                        foreach ($MI->GetMembers() as $member) {
                                            $dep_id = $member->getDepartment()['id'];

                                            if (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] != 0) {
                                                if (!in_array($dep_id, $RoleDepartmentFilter)) {
                                                    continue;
                                                }
                                            }

                                            if (isset($Member_id) && $Member_id == $member->getId()) {
                                                echo "<option value=\"" . $member->getId() . "\" selected >" . $member->getLastName() . " " . $member->getName()  ." ". ($member->getDismissalDate()=='01.01.9999'?"":" [уволен ".$member->getDismissalDate()."]") . "</option>";
                                            } else {
                                                echo "<option value=\"" . $member->getId() . "\">" . $member->getLastName() . " " . $member->getName() ." ". ($member->getDismissalDate()=='01.01.9999'?"":" [уволен ".$member->getDismissalDate()."]")  . "</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2"><input type="submit" class="btn btn-flat btn-default" name="SendDate" value="Получить данные"></div>
                        </div>
                    </form>
                </div>
            </div>

            <?php
            if (isset($_POST['SendDate']) && isset($_POST['date']) && isset($_POST['MemberId'])) {
                ?>
                <div class="box box-solid box-plane">
                    <div class="box-body">
                        <table class="table table-striped table-hover" id="DataTable">
                            <thead>
                            <tr>
                                <th>Дата</th>
                                <th>НДФЛ</th>
                                <th>ИЛ</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($NDFLData) > 0) {
                                $ndfl = 0;
                                $ro = 0;

                                foreach ($NDFLData as $value) {
                                    $ndfl += $value['ndfl'];
                                    $ro += $value['ro'];
                                    echo "<tr>";
                                    echo "<td>" . $value['date'] . "</td>";
                                    echo "<td>" . number_format($value['ndfl'], 2, ',', ' ') . "</td><td>" . number_format($value['ro'], 2, ',', ' ')  . "</td>";
                                    echo "</tr>";
                                }
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Итого</th>
                                <th><?php if (isset($ndfl)) echo number_format($ndfl, 2, ',', ' '); ?></th>
                                <th><?php if (isset($ro)) echo number_format($ro, 2, ',', ' '); ?></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <?php
            }
            ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
    $(function () {
        $('#DataTable').DataTable({
            'paging': false,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': false,
            'autoWidth': true
        })


        // Выбор даты
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "Years",
            minViewMode: "years",
            format: 'yyyy',
        })
    })
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>


