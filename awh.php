<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.08.18
 * Time: 18:29
 */

/*
* Импорт УРВ, НДФЛ, ИЛ
*/

// Время работы скрипта
$start = microtime(true);

require_once 'app/FileCSV.php';
require_once 'app/MemberInterface.php';
require_once 'app/AwhInterface.php';
require_once 'app/ErrorInterface.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";
require_once "app/ReportStatus.php";

require_once 'app/Notify.php';

session_start();
$User = $_SESSION['UserObj'];
/*
 * Если пользователь не аутентифицирован, то отпраляю на index.php
 */
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

/*
 * Объявляю объект ролей
 */
$RI = new RoleInterface();
/*
 * Получаю доступы
 */
$Roles = $RI->getRoles($User->getMemberId());

/*
 * Проверяю права доступа у роли, если нет доступа то отправляю в 404
 */
if (!array_filter($Roles, function ($Role) {
    return ($Role->getImportAWH() == 1);
})) {
    header("Location: 404.php" );
}

/*
 * Инициализируем объект ошибок
 */
$Errors = new ErrorInterface();

/*
 * Если нажата кнопка
 */
if (isset($_POST["submit"]) || isset($_POST["submit_import"])) {
    /*
     * Создаю объект пользовательского интерфейса
     */
    $MI = new MemberInterface(); // Интерфейс пользователя
    /*
     * Извлекаем пользователей в объект
     */
    $MI->fetchMembers();
    /*
     * Получаю массив объектов пользователей
     */
    $members = $MI->GetMembers();

    /*
     * Создаю объект УРВ
     */
    $AWH = new AwhInterface();

    /*
     * Объявляю переменнные
     */

    /*
     * Report статус
     */
    $RS = new ReportStatus();

    /*
     * Массив данных
     */
    $array = array();
    /*
     * Дата
     */
    $date = NULL;

    try {
        /*
         * Создаю объект для работы с CSV файлом
         */
        $uploadFile = new FileCSV();
        /*
         * Устанавливаю имя файла
         */
        $uploadFile->setFormName("CSVfileToUpload");
        /*
         * Загружаю
         */
        $uploadFile->Upload();
        /*
         * Распарсиваю и добавляю в массив
         */
        $array = $uploadFile->ParseCSV(";");
    } catch (Exception $e) {
        $Errors->add($e->getMessage()); // При ошибках добавляю в объект ошибок
    }

    /*
     * Если была нажата кнопка submit, то объявдяю массив SupperArray
     */
    if (isset($_POST["submit"])) {
        $SupperArray = array();
    }

    $type_of_payment = null;
    $date = null;

    /*
     * Разбираю полученный массив
     */
    foreach ($array as $key => $item) {
        /*
         * Ищу в массиве объектов сотрудников объект который содержит элементы из item
         */
        $neededObject = array_filter(
            $members,
            function ($e) use (&$item) {
                return $e->getLastname() == $item[4] && $e->getName() == $item[5] && $e->getMiddle() == $item[6] && $e->getDirection()['name'] == $item[2];
            }
        );

        $member_keys = array_keys($neededObject);
        sort($neededObject);

        /*
         * Если существует первый элемент, то ...
         */
        if (isset($neededObject[0])) {
            /*
             * Проверяю что это объект
             */
            if (is_object($neededObject[0])) {
                /*
                 * Переменной date присваиваю нулевой элемент массива
                 */
                $date = $item[0];

                /*
                 * Проверяю первый элемент массива, если аванс, то выставляю type_of_payment == 2 иначе == 1
                 */
                if ($item[1] == "аванс") {
                    $type_of_payment = 2;
                } else if ($item[1] == "премия") {
                    $type_of_payment = 1;
                }

                /*
                 * Если 7,8,9 элементы пустые то инициализирую нулями
                 */
                if (empty($item[7])) {
                    $item[7] = 0;
                }
                if (empty($item[8])) {
                    $item[8] = 0;
                }
                if (empty($item[9])) {
                    $item[9] = 0;
                }

                /*
                 *  Если все нули, то продолжаем
                 *  А если не было пропусков????
                 */
//                if ($item[7] == 0 && $item[8] == 0 && $item[9] == 0) {
//                    continue;
//                }

                /*
                 * Произвожу заменту запятой на точку и округляю до 2-го знака
                 */
                $item[7] = round(str_replace(",", ".", $item[7]), 2);
                $item[8] = round(str_replace(",", ".", $item[8]), 2);
                $item[9] = round(str_replace(",", ".", $item[9]), 2);

                /*
                 * Если нажата кнопка submit
                 */
                if (isset($_POST["submit"])) {
                    /*
                     * То в SupperArray добавляю значения
                     */
                    $SupperArray[] = [$item[0], $neededObject[0]->getId(), $item[4], $item[5], $item[6], $item[7], $item[8], $item[9]];
                } else {
                    /*
                     * Иначе добавляю в БД через метод объект addAwh
                     */

                    try {
                        $AWH->addAwh([$item[0], $item[7], $item[8], $item[9], $neededObject[0]->getId(), $type_of_payment]);
                    } catch (Exception $e) {
                        $Errors->add(["Ошибка при добавлении. ".$item[0]." ".$item[1]." ".$item[2]." ".$item[3]." ".$item[4]." ".$item[5]." ".$item[6]." ".$item[7]." ".$item[8]." ".$item[9], $e->getMessage()]);
                    }
                }
            }
        }

        /*
        * Удаляем из массива данных выборку
        */
        foreach ($member_keys as $kkk => $value) {
//            var_dump($neededObject);
            unset($array[$key]);
            unset($members[$value]);
        }
    }

    $work_members_without_awh = array_filter($members, function ($e) use ($date) {
        return $e->getWorkStatus() == 1;
    });
//    var_dump($work_members_without_awh); echo "<BR>";
//    foreach ($work_members_without_awh as $obj) {
//        echo $obj->getName()." ".$obj->getLastName()." ".$obj->getMiddle()." ".$obj->getEmploymentDate()."<BR>";
//    }


//    echo $type_of_payment."<BR>";
//    echo $date."<BR>";
//    echo date( 'm.Y', strtotime($date))." = 2342343 <BR>";
    switch ($type_of_payment) {
        case 1:
            /*
             * Получаю статус
             */
            if ($RS->getStatus(date( 'm.Y', strtotime($date)), $type_of_payment)['status_id'] == 0 ) {
                /*
                 * Проверяю все данные для установки статуса, в зависимости от типа оплаты
                 */
                if ($RS->checkStatus(date( 'm.Y', strtotime($date)), $type_of_payment)) {
                    /*
                     * Устанавливаю статус
                     */
                    $RS->ChangeStatus(date( 'm.Y', strtotime($date)), $type_of_payment, 1);
                }
            }
            break;
        case 2:
            if ($RS->getStatus(date( 'm.Y', strtotime($date)), $type_of_payment)['status_id'] == 0 ) {
                if ($RS->checkStatus(date( 'm.Y', strtotime($date)), $type_of_payment)) {
                    $RS->ChangeStatus(date( 'm.Y', strtotime($date)), $type_of_payment, 1);
                };
            }
            break;
    }

    /*
     * Сортирую остатки массива
     */
    sort($array);
    /*
     * И разбираю его, вывожу что такой пользователь не найден
     */
    foreach ($array as $value) {
//        var_dump($value);
        $Errors->add(["Не найден пользователь " . $value[4]." ".$value[5]." ".$value[6]]);
    }
    $array = NULL;

    /*
     * Удаляю файл
     */
    $uploadFile->DeleteFile();

    if (isset($_POST["submit_import"])) {
        try {
            /*
             * Получаю данные из БД
             */
            $AwhTable = $AWH->getAwh($date, $type_of_payment);
        } catch (Exception $e) {
            $Errors->add(["Ошибка при получении данных.", $e->getMessage()]);
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Импорт данных УРВ</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">

</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">

            <?php
            /*
            * Вывод информации об ощибках
            */
            if ($Errors->getCount() > 0) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="Table3">
                            <thead>
                            <th>Причина</th>
                            <th>Сообщение</th>
                            </thead>
                            <tbody>
                            <?php
                            /*
                             * Если есть ошибки, то вывожу их на экран
                             */
                            foreach ($Errors->getErrors() as $value) {
                                echo "<tr><td>" . $value['0'] . "</td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>

                <?php
            };
            ?>

            <?php
            if (isset($_POST["submit_import"])) {
                if (count($AwhTable) > 0) {
                    ?>
                    <div class="box box-solid box-pane">
                        <div class="box-header"><h4>Добавлено</h4></div>
                        <div class="box-body">
                            <table class="table table-bordered" id="MembersTable">
                                <thead>
                                <th>Дата</th>
                                <th>Фамилия</th>
                                <th>Имя</th>
                                <th>Отчество</th>
                                <th>Отсутствие(дни)</th>
                                <th>Отпуск(дни)</th>
                                <th>Болезнь(дни)</th>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($AwhTable)) {
                                    foreach ($AwhTable as $value) {
                                        echo '<tr>';
                                        echo "<td>" . $value['date'] . "</td><td>" . $value['lastname'] . "</td><td>" . $value['name'] . "</td><td>" . $value['middle'] . "</td><td>" . $value['absence'] . "</td><td>" . $value['holiday'] . "</td><td>" . $value['disease'] . "</td>";
                                        echo '</tr>';
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php
                }
            }

            if (isset($_POST["submit"])) {
                if (count($SupperArray) > 0) {
                    ?>
                    <div class="box box-success box-solid">
                        <div class="box-body">
                            <table class="table table-bordered" id="MembersTable">
                                <thead>
                                <th>#</th>
                                <th>Дата</th>
                                <th>Фамилия</th>
                                <th>Имя</th>
                                <th>Отчество</th>
                                <th>Отсутствие(дни)</th>
                                <th>Отпуск(дни)</th>
                                <th>Болезнь(дни)</th>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($SupperArray)) {
                                    foreach ($SupperArray as $value) {
                                        echo '<tr>';
                                        echo "<td>$value[1]</td><td>$value[0]</td><td>$value[2]</td><td>$value[3]</td><td>$value[4]</td><td>$value[5]</td><td>$value[6]</td><td>$value[7]</td>";
                                        echo '</tr>';
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>

            <div class="box box-solid box-pane box-widget">
                <form method="post" enctype="multipart/form-data">
                    <div class="box-header">
                        <h4>Блок УРВ импорт данных из CSV</h4>
                    </div>
                    <div class="box-body">
                        <div class="box box-solid box-info">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Выберите файл: </label>
                                    <input type="file" name="CSVfileToUpload" id="CSVfileToUpload">

                                    <p class="help-block">
                                        В CSV файле поля должны быть разделены с помощью точки с запятой';'.
                                    </p>
                                    <p class="help-block">
                                        Импортируются поля в следующей последовательности:
                                    <ul class="help-block">
                                        <li>Дата</li>
                                        <li>Тип(аванс или премия)</li>
                                        <li>Направление</li>
                                        <li>Отдел</li>
                                        <li>Фамилия</li>
                                        <li>Имя</li>
                                        <li>Отчество</li>
                                        <li>Отсутствие</li>
                                        <li>Отпуск</li>
                                        <li>Болезнь</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-flat btn-sm btn-success" name="submit"><i class="fa fa-check"></i> Проверить</button>
                                <button type="submit" class="btn btn-flat btn-sm btn-primary" name="submit_import"><i class="fa fa-save"></i> Импортировать</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <?php
            /*
             * Вывод информации об ощибках
             */
            if (isset($work_members_without_awh) && count($work_members_without_awh) > 0) {
                ?>
                <div class="modal fade" id="modalError" style="background-color: #1f292e">
                    <div style="width: 90%;" class="modal-dialog">
                        <div style="width: 100%;" class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h5>Нету данных УРВ по пользователям</h5>
                            </div>

                            <div class="modal-body">
                                <table class="table table-striped table-responsive">
                                    <thead>
                                    <th>Фамилия</th>
                                    <th>Имя</th>
                                    <th>Отчество</th>
                                    <th>Направление</th>
                                    <th>Отдел</th>
                                    <th>Должность</th>
                                    <th>Дата приема</th>
                                    <th>Дата увольнения</th>
                                    </thead>
                                    <tbody>
                                <?php

                                foreach ($work_members_without_awh as $obj) {
                                    echo "<tr>";
                                    echo "<td>".$obj->getLastName()."</td><td>".$obj->getName()."</td><td>".$obj->getMiddle()."</td>";
                                    echo "<td>".$obj->getDirection()['name']."</td>";
                                    echo "<td>".$obj->getDepartment()['name']."</td><td>".$obj->getPosition()['name']."</td>";
                                    echo "<td>".$obj->getEmploymentDate()."</td>";
                                    if ($obj->getDismissalDate() != "01.01.9999") {
                                        echo "<td>" . $obj->getDismissalDate() . "</td>";
                                    } else {
                                        echo "<td></td>";
                                    }
                                    echo "</tr>";
                                }
                                ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>

<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<?php
if (isset($work_members_without_awh) && count($work_members_without_awh) > 0) {
    echo "<script> $('#modalError').modal('show'); </script>";
}
?>

<script>
    $(function () {

        $('#MembersTable').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })

        $('#Table3').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })


    })
</script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>

