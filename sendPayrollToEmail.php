<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 06.12.19
 * Time: 14:03
 */

// Время работы скрипта
$start = microtime(true);

require_once 'app/PayRoll.php';
require_once 'app/PayRollInterface.php';
require_once 'app/functions.php';
require_once 'app/ReportStatus.php';
require_once 'app/Envelopes.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once "app/EnvelopeInterface.php";

require_once 'app/Notify.php';
require_once 'app/AdministrativeBlock.php';
require_once 'app/Retention.php';
require_once 'app/Correction.php';
require_once 'app/Kpi.php';
require_once 'app/MemberFinanceBlock.php';

if (!isset($_SESSION)) {
    session_start();
    $User = $_SESSION['UserObj'];
}

if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

if (!array_filter($Roles, function ($Role) use (&$Envelope_Update, &$is_Leader) {
    if ($Role->getId() == 5 || $Role->getId() == 6) {
        $Envelope_Update = 1;
    }

    if ($Role->getId() == 2 || $Role->getId() == 4) {
        $is_Leader = 1;
    }

    return ($Role->getId() == 2 || $Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6);
})) {
    header("Location: 404.php");
}

if (!isset($_SESSION)) {
    session_name('Envelope');
    session_start();
}

$AB = new AdministrativeBlock();
$RN = new Retention();
$CR = new Correction();
$KPI = new Kpi();
$MFB = new MemberFinanceBlockCorrect();


$date = $_SESSION['Envelope']['date'];
$type_of_payment = $_SESSION['Envelope']['TypeOfPayment'];

$rawData = $_SESSION['Envelope']['Data'];

$EmailData = array();
foreach ($rawData as $data) {
//    var_dump($data);
    if ($data->getEmail()) {
        if (!isset($EmailData[$data->getEmail()])) {
            $EmailData[$data->getEmail()] = "";
        }
        $member_id = $data->getId();
        $motivation = $data->getMotivation();
        $AB->fetch($member_id, $date);
        $RN->fetch($member_id, $date);
        $CR->fetch($member_id, $date);

        if ($motivation == 1) {
            try {
                $MFB->fetchCorrection($date, $member_id, $motivation);
            } catch (Exception $e) {
                echo "Ошибка получения корректировок " . $e->getMessage();
            }
        }

        $EmailData[$data->getEmail()] .= " <h4>ФИО: " . $data->getFio() . "</h4>";
        $EmailData[$data->getEmail()] .= "<div>Направление: " . $data->getDirection()['name'] . "</div>";
        $EmailData[$data->getEmail()] .= "<div>Отдел: " . $data->getDepartment()['name'] . "</div>";
        $EmailData[$data->getEmail()] .= "<div>Должность: " . $data->getPosition()['name'] . "</div>";
        $EmailData[$data->getEmail()] .= "<div>Оклад: " . number_format($data->getSalary(), 2, ',', ' ') . " </div>";
        $EmailData[$data->getEmail()] .= "<div>Выплаченный аванс: " .  number_format($data->getAdvancePayment(), 2, ',', ' ') . " </div>";
        $EmailData[$data->getEmail()] .= "<div>Отсутствия: " .  number_format($data->getAbsences(), 2, ',', ' ') . " </div>";
        $EmailData[$data->getEmail()] .= "<div>НДФЛ: " .  number_format($data->getNDFL(), 2, ',', ' ') . " </div>";
        $EmailData[$data->getEmail()] .= "<div>ИЛ: " .  number_format($data->getRO(), 2, ',', ' ') . " </div>";

        $EmailData[$data->getEmail()] .= "<BR>\r\n";
        $EmailData[$data->getEmail()] .= "<div><b>Коммерческая премия(руб)</b>: " .  number_format($data->getCommercial(), 2, ',', ' ') . " </div>";
        if ($motivation == 1) {
            foreach ($MFB->getData() as $item) {
                $EmailData[$data->getEmail()] .= "<i><div>";
                $EmailData[$data->getEmail()] .= "<b>" . $item['statgroup_name'] . "</b> Сумма(у.е.): " . $item['profit'] . " Корректировка " .  number_format($item['correct'], 2, ',', ' ')  . " у.е. Заметка: " . $item['correct_note'];
                $EmailData[$data->getEmail()] .= "</div></i>";
            }
        }

        $EmailData[$data->getEmail()] .= "<BR>\r\n";
        $EmailData[$data->getEmail()] .= "<div><b>KPI</b>: " .  number_format($data->getKPI(), 2, ',', ' ') . " </div>";

        if ($type_of_payment == 1) {
            $KPI->fetchPayment2($member_id, $date, 1);
            foreach ($KPI->get() as $item) {
                $EmailData[$data->getEmail()] .= "<i>";
                $EmailData[$data->getEmail()] .= "<div>KPI. Номер: ". $item->getNumber().".";
                $EmailData[$data->getEmail()] .= " Сумма: ". number_format($item->getSumm(), 2, ',', ' ');
                $EmailData[$data->getEmail()] .= ". Заметка: ". $item->getNote()."</div>";
                $EmailData[$data->getEmail()] .= "</i>";
            }
        }

        $EmailData[$data->getEmail()] .= "<BR>\r\n";
        $EmailData[$data->getEmail()] .= "<div><b>Административная премия(руб)</b>: " .  number_format($data->getAdministrative(), 2, ',', ' ') . " </div>";

        /*
         *  Если null значение Мобильной связи и есть в профиле, то выставляю из профиля
         */
        if (is_null($AB->getMobileCommunication())) {
            $AB->setMobileCommunicationFin($member_id);

        }

        if (is_null($AB->getCompensationFAL())) {
            $AB->setCompensationFALFin($member_id);
        }

        foreach ($AB->getAllData() as $key=>$item) {
            $EmailData[$data->getEmail()] .= "<i>";
            switch ($key) {
                case 0:
                    $EmailData[$data->getEmail()] .= "<div>Акции. ";
                    break;
                case 1:
                    $EmailData[$data->getEmail()] .= "<div>Административная премия. ";
                    break;
                case 2:
                    $EmailData[$data->getEmail()] .= "<div>Обучение. ";
                    break;
                case 3:
                    $EmailData[$data->getEmail()] .= "<div>Премия другого отдела. ";
                    break;
                case 4:
                    $EmailData[$data->getEmail()] .= "<div>Неликвиды и брак. ";
                    break;
                case 5:
                    $EmailData[$data->getEmail()] .= "<div>Воскресения. ";
                    break;
                case 6:
                    $EmailData[$data->getEmail()] .= "<div>Выплаты не менее. ";
                    break;
                case 7:
                    $EmailData[$data->getEmail()] .= "<div>Мобильная связь. ";
                    break;
                case 8:
                    $EmailData[$data->getEmail()] .= "<div>Переработки. ";
                    break;
                case 9:
                    $EmailData[$data->getEmail()] .= "<div>Компенсация ГСМ. ";
                    break;
            }

            $EmailData[$data->getEmail()] .= " Сумма: ". number_format($item['summ'], 2, ',', ' '). " Заметка: ".$item['note']."</>";
            $EmailData[$data->getEmail()] .= "</i>";
        }

        $EmailData[$data->getEmail()] .= "<BR><BR>\r\n";
        $EmailData[$data->getEmail()] .= "<div><b>Удержания(руб)</b>: " .  number_format($data->getHold(), 2, ',', ' ') . " </div>";
        foreach ($RN->getAllData() as $key=>$item) {
            $EmailData[$data->getEmail()] .= "<i>";
            switch ($key) {
                case 0:
                    $EmailData[$data->getEmail()] .= "<div>За опоздания. ";
                    break;
                case 1:
                    $EmailData[$data->getEmail()] .= "<div>За интернет. ";
                    break;
                case 2:
                    $EmailData[$data->getEmail()] .= "<div>Кассовый учет. ";
                    break;
                case 3:
                    $EmailData[$data->getEmail()] .= "<div>Дебиторская задолженность. ";
                    break;
                case 4:
                    $EmailData[$data->getEmail()] .= "<div>Кредит. ";
                    break;
                case 5:
                    $EmailData[$data->getEmail()] .= "<div>Другое. ";
                    break;
            }
            $EmailData[$data->getEmail()] .= " Сумма: ". number_format($item['summ'], 2, ',', ' '). " Заметка: ".$item['note']."</>";
            $EmailData[$data->getEmail()] .= "</i>";
        }

        $EmailData[$data->getEmail()] .= "<BR><BR>\r\n";
        $EmailData[$data->getEmail()] .= "<div><b>Корректировки(руб)</b>: " .  number_format($data->getCorrecting(), 2, ',', ' ') . " </div>";

        foreach ($CR->getAllData() as $key=>$item) {
            $EmailData[$data->getEmail()] .= "<i>";
            switch ($key) {
                case 0:
                    $EmailData[$data->getEmail()] .= "<div>Корректировка бонуса. ";
                    break;
                case 1:
                    $EmailData[$data->getEmail()] .= "<div>Расчет. ";
                    break;
                case 2:
                    $EmailData[$data->getEmail()] .= "<div>Отчисления в ГБ. ";
                    break;
            }
            $EmailData[$data->getEmail()] .= " Сумма: ". number_format($item['summ'], 2, ',', ' '). " Заметка: ".$item['note']."</>";
            $EmailData[$data->getEmail()] .= "</i>";
        }

        $EmailData[$data->getEmail()] .= "<BR><BR>\r\n";
        $EmailData[$data->getEmail()] .= "<div><b>Отчисления в ГБ</b>: " .  number_format($data->getYearbonusPay(), 2, ',', ' ') . " </div>";
        $EmailData[$data->getEmail()] .= "<hr>";
        $EmailData[$data->getEmail()] .= "<div><b>Итого(руб): " .  number_format($data->getSumm(), 2, ',', ' ') . " </b></div>";
    }
}

if ($_SESSION['Envelope']['TypeOfPayment'] == 1) {
    $Subject = "Зарплатная ведомость от ". $_SESSION['Envelope']['date'];
} else if ($_SESSION['Envelope']['TypeOfPayment'] == 2) {
    $Subject = "Авансовая ведомость от ". $_SESSION['Envelope']['date'];
} else {
    $Subject = "Выплата от ". $_SESSION['Envelope']['date'];
}

$Subject = "=?utf-8?B?" . base64_encode($Subject) . "?=";
$Header = "From: admin@tophouse.ru\r\n".
          "Content-type: text/html; charset=utf-8";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Отправка на почту</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
    <style>
        td > a {
            font-weight: bold;
        }
    </style>
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>Отправка ведомости на почту сотрудника</h4>
        </section>
        <section class="content">
            <div class="box box-solid">
                <div class="box-body">
                    <?php
                    foreach ($EmailData as $email=>$message) {
                        echo "<div style='font-size: 14px'><b>Email: ".$email."</b></div>";
                        echo $message;

                        if (@mail($email, $Subject, $message, $Header)) {
                            echo "<div style='font-size: 14px'>Статус: <span class='text-success'>Отправлено</span></div>";
                        } else {
                            echo "<div style='font-size: 14px'>Статус: <span class='text-danger'>Ошибка</span></div>";
                        }
                        echo "<BR>";
                    }
                    ?>
                </div>
            </div>
        </section>
    </div>

    <?php
    require_once 'footer.php';
    ?>
</div>

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
