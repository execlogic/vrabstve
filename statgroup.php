<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 31.07.18
 * Time: 13:25
 */

require_once 'app/StatgroupInterface.php';
require_once 'app/ErrorInterface.php';
require_once 'app/FileCSV.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());
// Настройки
if (!array_filter($Roles, function ($Role) {
    return ($Role->getSettings() == 1);
})) {
    header("Location: 404.php" );
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

$Statgroup = new StatgroupInterface();
$Errors = new ErrorInterface();

if (isset($_POST["submit"])) {

    $array = array();
    try {
        $uploadFile = new FileCSV();
        $uploadFile->setFormName("CSVfileToUpload");
        $uploadFile->Upload();
        $array = $uploadFile->ParseCSV(";");
//        var_dump($array);
    } catch (Exception $e) {
        $Errors->add($e->getMessage());
    }

    foreach ($array as $value) {
        try {
            $Statgroup->add($value[0], $value[1]);
        } catch (Exception $e) {
            $Errors->add($value[0] . " " . $value[1] . " " . $e->getMessage());
        }
    }
    $uploadFile->DeleteFile();
}

$Statgroup->fetch();
$menu_open = 2;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Справочник статгрупп</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>

    <div class="content-wrapper">
        <section class="content">
            <?php
            /*
             * Вывод информации об ощибках
             */
            if ($Errors->getCount() > 0) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <?php
                            foreach ($Errors->getErrors() as $error) {

                            }
                            echo "<tr><td>" . $error . "</td></tr>";
                            ?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php
            }
            ?>


            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Статгруппы</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="MembersTable" class="table table-bordered table-hover table-striped">
                        <thead>
                        <th>#</th>
                        <th>skaldID</th>
                        <th>Название</th>
                        </thead>
                        <tbody>
                        <?php
                        try {
                            foreach ($Statgroup->get() as $value) {
                                echo "<tr>";
                                echo "<td>" . $value->getID() . "</td><td>" . $value->getSkladId() . "</td><td>" . $value->getName() . "</td>";
                                echo "</tr>";
                            }
                        } catch (Exception $e) {
                            echo $e->getMessage();
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a class="btn btn-sm btn-flat btn-primary" data-toggle="modal" data-target="#modal-default" title="Добавить должность">
                        <i class="fa fa-plus"></i> Добавить
                    </a>

                    <a class="btn btn-sm btn-flat btn-primary" data-toggle="modal" data-target="#modal-csv" title="Импорт из csv">
                        <i class="fa fa-file"></i> Импорт
                    </a>
                </div>

                <div class="modal fade" id="modal-default">
                    <div style="width: 90%;" class="modal-dialog">
                        <div style="width: 100%;" class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title">Добавить статгруппу</h4>
                            </div>
                            <div class="modal-body">
                                <form method="post" class="form-horizontal">
                                    <div class="box box-info">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Название статгруппы</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="InputPosition" id="inputPosition" placeholder="Отдел">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer" style="border:0;">
                                <a class="btn btn-sm btn-flat btn-primary pull-left" data-dismiss="modal" title="Закрыть">
                                    <i class="fa fa-times"></i> Закрыть
                                </a>
                                <button type="submit" class="btn btn-sm btn-flat btn-primary" name="submit_add"><i class="fa fa-save"></i> Добавить статгруппу</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="modal-csv">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form method="post" enctype="multipart/form-data">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Импорт из CSV</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="box box-info">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="exampleInputFile">Выберите файл: </label>
                                                <input type="file" name="CSVfileToUpload" id="CSVfileToUpload">
                                                <p class="help-block">
                                                    Импортируются поля в следующей последовательности:
                                                <ul class="help-block">
                                                    <li>Название <b class="text-red">(Обязательное поле)</b></li>
                                                </ul>
                                                Перевод строки обозначает следующее название.
                                                </p>
                                                <p class="help-block">Директор;</p>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-sm btn-flat btn-primary pull-left" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                                    <button type="submit" class="btn btn-sm btn-flat btn-primary" name="submit"><i class="fa fa-save"></i> Импортировать</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            </div>

        </section>
    </div>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
