<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 31.07.18
 * Time: 13:25
 */

// Время работы скрипта
$start = microtime(true);

require_once 'app/DepartmentInterface.php';
require_once 'app/DirectionInterface.php';
require_once 'app/FileCSV.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());
// Настройки
if (!array_filter($Roles, function ($Role) {
    return ($Role->getSettings() == 1 || $Role->getAWHEdit() == 1);
})) {
    header("Location: 404.php");
}

$Errors = "";

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

$Departments = new DepartmentInterface();
$Directions = new DirectionInterface();
$Directions->fetchDirections();
$DL = $Directions->GetDirections();

if (isset($_POST["submit"])) {

    $array = array();
    try {
        $uploadFile = new FileCSV();
        $uploadFile->setFormName("CSVfileToUpload");
        $uploadFile->Upload();
        $array = $uploadFile->ParseCSV(";");
    } catch (Exception $e) {
        $Errors .= $e->getMessage() . "<br>";
    }

    foreach ($array as $value) {
        if (strlen($value[0]) < 4) { // количество символов меньше 4
            continue;
        }

        try {
            $Departments->AddDepartments($value[0]);
        } catch (Exception $e) {
            $Errors .= $value[0] . " " . $e->getMessage() . "<br>";
        }
    }
    $uploadFile->DeleteFile();
}

if (isset($_POST['PushButton1'])) {
    $name = null;
    $direction_id = null;
    $sklad_id = null;
    $sklad_name = null;

    $name = $_POST['InputDepartment'];
    $direction_id = implode(",", $_POST['direction_id']);
    $sklad_id = $_POST['SkladID'];
    $sklad_name = $_POST['SkladName'];

    if (is_null($name) || is_null($direction_id)) {
        $Errors .= "Не выбрано направление или не задано имя";
    } else {
        try {
            $Departments->AddDepartmentSingle($name, $direction_id, $sklad_id, $sklad_name);
        } catch (Exception $e) {
            $Errors .= $e->getMessage() . "<BR>";
        }
    }
}

if (isset($_POST['PushButton2'])) {
    $name = null;
    $direction_id = null;

    $id = $_POST["id"];
    $name = $_POST['name'];
    $direction_id = implode(",", $_POST['direction_id']);
    $sklad_id = $_POST['SkladID'];
    $sklad_name = $_POST['SkladName'];

    if (is_null($name) || is_null($direction_id)) {
        $Errors .= "Не выбран отдел или не задано имя";
    } else {
        try {
            $Departments->UpdateDepartmentSingle($id, $name, $direction_id, $sklad_id, $sklad_name);
        } catch (Exception $e) {
            $Errors .= $e->getMessage() . "<BR>";
        }
    }
}

$Departments->fetchDepartments();


if (isset($_POST['ModifyID'])) {
    $id = $_POST['ModifyID'];
    /*
     * Получаю из массива нужный элемент
     */
    $Modify = current(array_filter($Departments->getDepartments(), function ($d) use ($id) {
        return $d->getID(0) == $id;
    }));
}
$menu_open = 2;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Отделы компании</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>

    <div class="content-wrapper">


        <section class="content">
            <?php
            /*
             * Вывод информации об ощибках
             */
            if (!empty($Errors)) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        echo "<p>" . $Errors . "</p>";
                        ?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php
            }
            ?>

            <!-- Default box -->
            <form method="post">
                <div class="box box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Отделы компании</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="MembersTable" class="table table-bordered table-hover table-striped">
                            <thead>
                            <th>#</th>
                            <th>Название</th>
                            <th>Номер по 1C</th>
                            <th>Название по 1С</th>
                            <th>Направления</th>
                            <th>Управление</th>
                            </thead>
                            <tbody>
                            <?php
                            try {
                                foreach ($Departments->GetDepartments() as $value) {
                                    echo "<tr>";
                                    echo "<td>" . $value->getID() . "</td><td>" . $value->getName() . "</td><td>" . $value->getIdsklad() . "</td><td>" . $value->getNamesklad() . "</td>";
                                    echo "<td>";
                                    $c = 0;
                                    foreach ($value->getDirectionId() as $d) {
                                        $c++;
                                        $DObj = current(array_filter($DL, function ($dd) use ($d) {
                                            return $dd->getId() == $d;
                                        }));

                                        if (!$DObj)
                                            continue;

                                        if (count($value->getDirectionId()) != $c) {
                                            echo $DObj->getName() . ", ";
                                        } else {
                                            echo $DObj->getName() . " ";
                                        }

                                    }
                                    echo "</td>";
                                    echo "<td><button class=\"btn btn-sm btn-flat btn-default\" name='ModifyID' value='" . $value->getID() . "'>Изменить</button></td>";
                                    echo "</tr>";
                                }
                            } catch (Exception $e) {
                                echo $e->getMessage();
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a class="btn btn-flat btn-primary" data-toggle="modal" data-target="#modal-default" title="Добавить направление">
                            <i class="fa fa-plus" style="font-size: 12px;"></i><span style="padding-left: 5px;font-size: 12px;">Добавить</span>
                        </a>

                        <a class="btn btn-flat btn-warning" data-toggle="modal" data-target="#modal-csv" title="Импорт из csv">
                            <i class="fa fa-file" style="font-size: 12px;"></i><span style="padding-left: 5px;font-size: 12px;">Импорт</span>
                        </a>
                    </div>
                </div>
            </form>


            <form method="post" class="form-horizontal">
                <div class="modal fade" id="modal-default">
                    <div style="width: 90%;" class="modal-dialog">
                        <div style="width: 100%;" class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title">Добавить отдел</h4>
                            </div>

                            <div class="modal-body">
                                <div class="box box-info box-solid">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Направление</label>
                                            <div class="col-sm-10">

                                                <select name="direction_id[]" multiple class="form-control">
                                                    <?php
                                                    foreach ($Directions->GetDirections() as $value) {
                                                        echo "<option value='" . $value->GetID() . "''>" . $value->GetName() . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Название отдела</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="InputDepartment" id="inputDepartment" placeholder="Отдел">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Номер в 1С</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="SkladID" id="SkladID" placeholder="Номер в 1С">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Название по 1С</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="SkladName" id="SkladName" placeholder="Название по 1С">
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button class="btn btn-flat btn-danger" data-dismiss="modal" title="Закрыть" style="font-size: 12px;">
                                    <i class="fa fa-times"></i> Закрыть
                                </button>
                                <button type="submit" class="btn btn-flat btn-primary" name="PushButton1" style="font-size: 12px;"><i class="fa fa-save"></i> Добавить направление</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <?php
            if (isset($_POST['ModifyID'])) {
                ?>
                <form method="post" class="form-horizontal">
                    <div class="modal fade" id="modal-edit">
                        <div style="width: 90%;" class="modal-dialog">
                            <div style="width: 100%;" class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title">Изменить отдел</h4>
                                </div>
                                <div class="modal-body">
                                    <input name="id" type="hidden" value="<?php echo $Modify->getId(); ?>">
                                    <div class="box box-info box-solid">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Направление</label>
                                                <div class="col-sm-10">
                                                    <?php
                                                    $ModDir = $Modify->getDirectionID();
                                                    ?>

                                                    <select name="direction_id[]" class="form-control select2" multiple="multiple" data-placeholder="Выберите направление" style="width: 100%;">
                                                        <?php
                                                        foreach ($Directions->GetDirections() as $value) {
                                                            if (in_array($value->GetID(), $ModDir)) {
                                                                echo "<option value='" . $value->GetID() . "'' selected>" . $value->GetName() . "</option>";
                                                            } else {
                                                                echo "<option value='" . $value->GetID() . "''>" . $value->GetName() . "</option>";
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Название должности</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="name" id="inputPosition" placeholder="Отдел" value="<?php echo $Modify->getName(); ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Номер в 1С</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="SkladID" id="SkladID" placeholder="Номер в 1С" value="<?php echo $Modify->getIdsklad(); ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Название по 1С</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="SkladName" id="SkladName" placeholder="Название по 1С" value="<?php echo $Modify->getNamesklad(); ?>">
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer" style="border:0;">
                                    <a class="btn btn-sm btn-flat btn-danger" data-dismiss="modal" title="Закрыть">
                                        <i class="fa fa-times"></i> Закрыть
                                    </a>
                                    <button type="submit" class="btn btn-sm btn-flat btn-primary" name="PushButton2"><i class="fa fa-save"></i> Изменить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <?php
            }
            ?>

            <form method="post" enctype="multipart/form-data">
                <div class="modal fade" id="modal-csv">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Импорт из CSV</h4>
                            </div>
                            <div class="modal-body">
                                <div class="box box-info box-solid">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputFile">Выберите файл: </label>
                                            <input type="file" name="CSVfileToUpload" id="CSVfileToUpload">
                                            <p class="help-block">
                                                Импортируются поля в следующей последовательности:
                                            <ul class="help-block">
                                                <li>Название <b class="text-red">(Обязательное поле)</b></li>
                                            </ul>
                                            Перевод строки обозначает следующее название.
                                            </p>
                                            <p class="help-block">Агенты;</p>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-flat btn-danger" data-dismiss="modal" style="font-size: 12px;"><i class="fa fa-times"></i><span>Закрыть</span></button>
                                <button type="submit" class="btn btn-flat btn-primary" name="submit" style="font-size: 12px;"><i class="fa fa-save"></i> Импортировать</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </form>
        </section>
    </div>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
    <?php
    if ((isset($_POST['ModifyID']))) {
    ?>
    $('#modal-edit').modal('show');
    <?php
    }
    ?>
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
