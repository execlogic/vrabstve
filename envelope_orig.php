<?php

// Время работы скрипта
$start = microtime(true);

require_once 'app/PayRoll.php';
require_once 'app/PayRollInterface.php';
require_once 'app/functions.php';
require_once 'app/ReportStatus.php';
require_once 'app/Envelopes.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

if (!isset($_SESSION)) {
    session_start();
    $User = $_SESSION['UserObj'];
}

if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

$MAX_ENVELOPE_SUMM = 25000;
$MIN_ENVELOPE_SUMM = 1000;
$MAX_Envelope_NumSize = 5;

if (!isset($_SESSION)) {
    session_name('Envelope');
    session_start();
}

$rawData = $_SESSION['Envelope']['Data']; // Подучаю данные из сессии
$TypeOfPayment = $_SESSION['Envelope']['TypeOfPayment']; // Получаю тип выплаты из сессии
$date = $_SESSION['Envelope']['date']; // получаю дату из сессии
session_commit();


if (is_null($rawData) || is_null($TypeOfPayment) || is_null($date)) {
    echo "Нету данных для обработки.";
    exit();
}

if (isset($_POST['MAX_ENV_SUMM'])) {
    $MAX_ENVELOPE_SUMM = $_POST['MAX_ENV_SUMM'];
}

if (isset($_POST['MIN_ENV_SUMM'])) {
    $MIN_ENVELOPE_SUMM = $_POST['MIN_ENV_SUMM'];
}

if (isset($_POST['MAX_ENV_NUMSIZE'])) {
    $MAX_Envelope_NumSize = $_POST['MAX_ENV_NUMSIZE'];
}

$RS = new ReportStatus(); // Статус ведомости.
$ReportStatus = $RS->getStatus($date,$TypeOfPayment);


//echo "MAX в конверте: ".$MAX_ENVELOPE_SUMM."<BR>";
//echo "MIN в конверте: ".$MIN_ENVELOPE_SUMM."<BR>";
//echo "Max кол-во конвертов ".$MAX_Envelope_NumSize."<BR>";

// Округление до 10
foreach ($rawData as $item) {
    if ($item->getSumm() <= 0) { //Если сумма <= 0 то не обрабатываем данные
        continue;
    } else {
        $item->setSumm(ceil($item->getSumm())); // округляю сотые и десятые

        $ostatok = ($item->getSumm() % 10); // Получаю остаток от деления на 10
        if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
            $item->setSumm($item->getSumm() - $ostatok + 10);
        } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
            $item->setSumm($item->getSumm() - $ostatok);
        };
    }
}


/* ********************* Получаю данные из БД для сравнения ************************* */

$SQLEnvData = array(); // Данные конвертов из SQL
$SQLMembersSumm = array(); // Сумма всех конвертов по пользователю ['MEMBER_ID'] = SUMM

$dbh = dbConnect();

/** ********************************************************************************************************************* **/

$query = "SELECT SUM(summ) as summ, member_id FROM envelope where date=STR_TO_DATE(?, '%m.%Y') and type_of_payment=? group by member_id"; //Получаю ВСЮ сумму, member_id, зная тип и дату, группирую по id пользователя
try {
    $sth = $dbh->prepare($query); // Подготавливаем запрос
    $sth->execute([$date, $TypeOfPayment]); // Выполняю запрос
} catch (PDOException $e) {
    echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
}
if ($sth->rowCount() > 0) { // Если количество строк > 0, значит данные есть
    foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $item) { // Добавляю данные в массив SQLMemberSumm
        $SQLMembersSumm[$item['member_id']] = $item['summ'];
    }
}

$query = "SELECT COUNT(*) as total FROM zp.envelope where type_of_payment=? AND date=STR_TO_DATE(?,'%m.%Y') group by member_id order by total desc limit 1"; // Проверяю максимальное количество конвертов в БД.
try {
    $sth = $dbh->prepare($query); // Подготавливаем запрос
    $sth->execute([$TypeOfPayment, $date]); // Выполняю запрос
} catch (PDOException $e) {
    echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
}
if ($sth->rowCount() > 0) {
    $raw = $sth->fetch();
    if ($raw['total'] > $MAX_Envelope_NumSize) {
        $MAX_Envelope_NumSize = $raw['total'];
    }
}

$query = "select * from envelope where type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y')"; // Получаю ранее выданные конверты из БД
try {
    $sth = $dbh->prepare($query); // Подготавливаем запрос
    $sth->execute([$TypeOfPayment, $date]); // Выполняю запрос
} catch (PDOException $e) {
    echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
}
if ($sth->rowCount() > 0) { // Если количество строк больше > 0 значит данные есть
    $SQLRaw = $sth->fetchAll(PDO::FETCH_ASSOC); // Извлекаю данные и добавляю в массив объектов
    foreach ($SQLRaw as $item) {
        $Env = new Envelopes(); // Создаю объект конверта
        $Env->setSumm($item['summ']); // Устанавливаю остаток от суммы
        $Env->setOldSumm();
        $Env->setMemberId($item['member_id']); // ID пользователя
//        $Env->setName(); // ФИО
        $Env->setEnvelopeId($item['envelope_id']); // Номер конверта
        $SQLEnvData[] = $Env; // Добавляю в  массив данных
    }
}

/** ********************************************************************************************************************* **/

if (count($SQLEnvData) == 0) { // Если в БД ничего нету, то количество элементов в массиве == 0 и нам нужно создать конверты на лету.
    // Делаю копию массива объектов, для того чтобы его можно было ломать в разборе
    $tmpDataArray = array_map(function ($object) {
        return clone $object;
    }, $rawData);

    $EnvData = array();

    $EnvelopeNumber = 0; // Номер конверта
    $EnvelopeSize = 0;

    for ($i = 0; $i < $MAX_Envelope_NumSize; $i++) { // Максимум конвертов на сотрудника
        foreach ($tmpDataArray as $item) {
            if ($item->getSumm() <= 0) { // если сумма в ведомости <=0 тогда не выводим
                continue;
            }

            $EnvelopeNumber++; // Расчет номера конверта
            if (($MAX_Envelope_NumSize > 3) && ($i == ($MAX_Envelope_NumSize - 2)) && ($item->getSumm() > (2 * $MAX_ENVELOPE_SUMM))) { // Если Сумма в 4ом конверте > чем максимально возможная сумма
                $OSTATOK = ($item->getSumm() % $MAX_ENVELOPE_SUMM); // То расчитываю остаток
                if ($OSTATOK == 0) {
                    $OSTATOK = $MAX_ENVELOPE_SUMM;
                }
                $Env = new Envelopes(); // Создаю объект конверта
                $Env->setSumm($OSTATOK); // Устанавливаю остаток от суммы
                $Env->setMemberId($item->getId()); // ID пользователя
                $Env->setName($item->getFio()); // ФИО
                $Env->setEnvelopeId($EnvelopeNumber); // Номер конверта
                $item->setSumm($item->getSumm() - $OSTATOK); // Из суммы вычитаю остаток
                $EnvData[] = $Env; // Добавляю в  массив данных
            } else if ($i == ($MAX_Envelope_NumSize - 1)) {
                $Env = new Envelopes(); // Создаю объект конверта
                $Env->setSumm($item->getSumm()); // Устанавливаю ВСЕ от суммы
                $Env->setMemberId($item->getId()); // ID пользователя
                $Env->setName($item->getFio()); // ФИО
                $Env->setEnvelopeId($EnvelopeNumber); // Номер конверта
                $item->setSumm(0); // Сумму выставляю в 0
                $EnvData[] = $Env; // Добавляю в  массив данных
            } else {
                if (($item->getSumm() < $MAX_ENVELOPE_SUMM)) { // Если меньше Максимальной суммы
                    $Env = new Envelopes(); // Создаю объект
                    $Env->setSumm($item->getSumm()); //Устанавливаю сумму конверта
                    $Env->setMemberId($item->getId()); // Устанавливаю ID пользователя
                    $Env->setName($item->getFio()); // Устанавливаю ФИО
                    $Env->setEnvelopeId($EnvelopeNumber); // Установка номера конверта

                    $item->setSumm(0); // Установить сумму в 0.

                    $EnvData[] = $Env; // Добавить в массив
                } else if ($item->getSumm() > $MAX_ENVELOPE_SUMM) { // Если больше максимальной суммы
                    $Env = new Envelopes(); // Создаю объект
                    $Env->setSumm($MAX_ENVELOPE_SUMM); // Устанавливаю МАКСИМАЛЬНУЮ сумму конверта
                    $Env->setMemberId($item->getId()); // Устанавливаю ID пользователя
                    $Env->setName($item->getFio()); // Устанавливаю ФИО
                    $Env->setEnvelopeId($EnvelopeNumber);  // Установка номера конверта

                    $item->setSumm($item->getSumm() - $MAX_ENVELOPE_SUMM); // Вычитаю максимальную сумму

                    if ($item->getSumm() < $MIN_ENVELOPE_SUMM) { // Если остаток суммы меньше MIN_ENVELOPE_SUMM
                        $Env->setSumm($Env->getSumm() + $item->getSumm());  // То выдаем в том же конверте
                        $item->setSumm(0); // Сумму по ведомости ставлю в 0
                    }

                    $EnvData[] = $Env;
                }
            }
        };
    }
    $tmpDataArray = null; // Очищаю память от tmp массива объектов
    $FinalEnvData = $EnvData;
} else { // Если данные есть в SQL, то провожу проверку, нету ли измененных данных
    $FinalEnvData = $SQLEnvData;
    $EnvelopeNumber = $FinalEnvData[count($FinalEnvData) - 1]->getEnvelopeId(); // Получаю последний номер конверта

    foreach ($SQLMembersSumm as $member_id => $summ) {
        $current = current(array_filter($rawData, function ($obj) use ($member_id) { // Ищу RAW данные(данные из ведомости) по пользователю,
            return ($obj->getId() == $member_id); // ID пользователя в ведомости в конвертах совпадает, для сравнения ИТОГО суммы с SQL данными
        }));

        if ($current) { // В ведомости такой пользователь есть, если нету значи пользователь написал заявление на увольнение.

            if ($current->getSumm() == $summ) { // Суммы == значит, ничего не изменилось
                continue;
            } else if ($summ > $current->getSumm()) { // Если в БД сумма меньше чем по ведомости, например внесли удержания
                echo $current->getFio() . "<BR>";
                echo "Сумма > чем в ведомости<BR>";
                $RAZNICA = $summ - $current->getSumm(); // Получаю разницу в сумме
                echo "Разница " . $RAZNICA . "<BR>";

                $member_array = array_filter($FinalEnvData, function ($obj) use ($member_id) { // Ищу все конверты в SQL данных
                    return ($obj->getMemberId() == $member_id); // где ID пользователя в ведомости в конвертах совпадает
                });

                $list_keys = array(); // Получаю массив с ключами массива, где находятся конверты пользователя
                echo "Массив с ключами: <BR>";
                foreach ($member_array as $key => $value) {
                    $list_keys[] = $key; // Добавляю ключи в массив
                    echo "Key: " . $key . "<BR>";
                }

                if ($RAZNICA >= $summ) { // Если полученная разница больше чем сумма по конвертам, то удаляю эти конверты
                    echo "Разница >= ВСЕЙ суммы, удаляю все конверты<br>";
                    foreach ($list_keys as $value) {
                        $FinalEnvData[$value] = null; // Удаляю данные из основного массива
                    }
                } else {
                    echo "Перебираю конверты " . count($list_keys) . " эл.<br>";

                    for ($i = count($list_keys) - 1; $i > 0, $RAZNICA > 0; $i--) { // Перебираем конверты с конца
                        echo "Проверяю эл. массива: " . $list_keys[$i] . "<BR>";
                        if ($FinalEnvData[$list_keys[$i]]->getSumm() >= $RAZNICA) {
                            echo "В конверте " . $FinalEnvData[$list_keys[$i]]->getEnvelopeId() . " сумма >= чем в \"разнице\" вычитаю все из конверта, разницу обнуляю <BR>";

                            $FinalEnvData[$list_keys[$i]]->setSumm($FinalEnvData[$list_keys[$i]]->getSumm() - $RAZNICA);
                            $FinalEnvData[$list_keys[$i]]->setStatus(1); // Ставлю статуст что было изменение
                            $RAZNICA = 0;
                        } else {
                            echo "В конверте < чем в \"разнице\" вычитаю сумму конверта из \"разницы\", конверт " . $list_keys[$i] . " обнуляю <BR>";
                            $RAZNICA -= $FinalEnvData[$list_keys[$i]]->getSumm();
//                            unset($FinalEnvData[$list_keys[$i]]);
                            $FinalEnvData[$list_keys[$i]]->setSumm(0);
                            $FinalEnvData[$list_keys[$i]]->setStatus(2);
                        }
                    }
                }
//                sort($FinalEnvData);
                echo "<BR>";
            } else if ($summ < $current->getSumm()) { // Сумма в SQL < меньше чем в ведомости, т.е. поступили новые данные и начислили еще денег
                echo $current->getFio() . "<BR>";
                echo "Сумма ведомости > чем в SQL <BR>";
                $RAZNICA = $current->getSumm() - $summ; // Получаю разницу в сумме
                echo "Разница " . $RAZNICA . "<BR>";

                $member_array = array_filter($FinalEnvData, function ($obj) use ($member_id) { // Ищу все конверты в SQL данных
                    return ($obj->getMemberId() == $member_id); // где ID пользователя в ведомости в конвертах совпадает
                });

                $list_keys = array(); // Получаю массив с ключами массива, где находятся конверты пользователя
                echo "Массив с ключами: <BR>";
                foreach ($member_array as $key => $value) {
                    $list_keys[] = $key; // Добавляю ключи в массив
                    echo "Key: " . $key . "<BR>";
                }

                echo "Количество конвертов: " . count($list_keys) . "<BR>";

                if (count($list_keys) == $MAX_Envelope_NumSize) { // Если количество конвертов == максимальному количеству, ([предпоследний конверт] + RAZNICA) > MAX_SIZE,
                    echo "Количество конвертов == MAX_Envelope_NumSize<BR>";
                    // то добавляю в последний MAX_SIZE, и разницу в пердпоследний
                    if (($FinalEnvData[$list_keys[count($list_keys) - 2]]->getSumm() + $RAZNICA) > $MAX_ENVELOPE_SUMM) { // Если сумма предпоследнего элемента + РАЗНИЦА > чем Максимально возможная сумма
                        echo "Сумма передпоследнего + РАЗНИЦА > MAX размер в конверте<BR>";
                        $SUMM = ($FinalEnvData[$list_keys[count($list_keys) - 2]]->getSumm() + $FinalEnvData[$list_keys[count($list_keys) - 1]]->getSumm() + $RAZNICA); // Получаю сумму 2х предпоследних конвертов и РАЗНИЦЫ
                        echo "Сумма 2х последний эл-тов + RAZNICA: " . $SUMM . "<BR>";
                        $OSTATOK = ($SUMM % $MAX_ENVELOPE_SUMM); // Получаю остаток от деления на Максимальный размер конверта
                        $FinalEnvData[$list_keys[count($list_keys) - 2]]->setSumm($OSTATOK); // Устанавливаю остаток в предпоследний конверт
                        $FinalEnvData[$list_keys[count($list_keys) - 1]]->setSumm($SUMM - $OSTATOK); // Устанавливаю новую сумму в последний конверт
                        $FinalEnvData[$list_keys[count($list_keys) - 1]]->setStatus(1);
                        $FinalEnvData[$list_keys[count($list_keys) - 2]]->setStatus(1);
                        $RAZNICA = 0; // Ставлю разницу == 0
                    } else { // Иначе добавляю остаток в предпоследний конверт
                        echo "Сумма передпоследнего + РАЗНИЦА < MAX размер в конверте<BR>";
                        $FinalEnvData[$list_keys[count($list_keys) - 2]]->setSumm($FinalEnvData[$list_keys[count($list_keys) - 2]]->getSumm() + $RAZNICA); // Устанавливаю остаток в предпоследний конверт
                        $FinalEnvData[$list_keys[count($list_keys) - 2]]->setStatus(1);
                        $RAZNICA = 0; // Ставлю разницу == 0
                    }
                } else { // Если это не максимально возможное количество конвертов
                    echo "Количество конвертов != MAX_Envelope_NumSize<BR>";
                    echo "Номер последнего конверта: " . $EnvelopeNumber . "<BR>";

                    if ($FinalEnvData[$list_keys[count($list_keys) - 1]]->getSumm() + $RAZNICA <= $MAX_ENVELOPE_SUMM) { // Если сумма последнего конверта  и $RAZNICA <= максимально возможной суммы в конверте
                        echo "Сумма последнего конверта <= $MAX_ENVELOPE_SUMM<BR>";
                        $FinalEnvData[$list_keys[count($list_keys) - 1]]->setSumm($FinalEnvData[$list_keys[count($list_keys) - 1]]->getSumm() + $RAZNICA); // Добавляю сумму в этот же конверт
                        $FinalEnvData[$list_keys[count($list_keys) - 1]]->setStatus(1);
                        $RAZNICA = 0;
                    } else { // Если больше
                        echo "Сумма последнего конверта > $MAX_ENVELOPE_SUMM<BR>";
                        if ((count($list_keys) == ($MAX_Envelope_NumSize - 1)) && (($FinalEnvData[$list_keys[count($list_keys) - 1]]->getSumm() + $RAZNICA) >= $MAX_ENVELOPE_SUMM)) { // Если это предпоследний конверт и разница > чем максимально возможная сумма
                            echo "Если колчество конвертов == предпоследнему возможнуому конверту, а разница > максимальной сумме в конверте<BR>";
                            $SUMM = $FinalEnvData[$list_keys[count($list_keys) - 1]]->getSumm() + $RAZNICA; // Получаю сумму последнего конверта  и разницу
                            $OSTATOK = ($SUMM % $MAX_ENVELOPE_SUMM); // То расчитываю остаток
                            $FinalEnvData[$list_keys[count($list_keys) - 1]]->setSumm($OSTATOK); // Устанавливаю остаток в последний элемент
                            $FinalEnvData[$list_keys[count($list_keys) - 1]]->setStatus(1);

                            $EnvelopeNumber++;
                            $MEMBER_ID = $FinalEnvData[$list_keys[count($list_keys) - 1]]->getMemberId(); // Получаю ID пользователя
                            echo "ID пользователя: " . $MEMBER_ID . " <BR>";

                            $Env = new Envelopes(); // Создаю объект конверта
                            $Env->setSumm($SUMM - $OSTATOK); // Устанавливаю остаток от суммы
                            $Env->setMemberId($MEMBER_ID); // ID пользователя
                            $Env->setEnvelopeId($EnvelopeNumber); // Номер конверта
                            $Env->setStatus(1);
                            $RAZNICA = 0; // Из суммы вычитаю остаток
                            $FinalEnvData[] = $Env; // Добавляю в  массив данных
                        } else {
                            echo "Еще не предпоследний и не последний конверт <BR>";
                            $OSTATOK = $MAX_ENVELOPE_SUMM - $FinalEnvData[$list_keys[count($list_keys) - 1]]->getSumm(); // Получаю разницу между максимальным количеством в конверте и суммой в конверте
                            $FinalEnvData[$list_keys[count($list_keys) - 1]]->setSumm($MAX_ENVELOPE_SUMM); // В Последний конверт выставляю максимальную сумму
                            $RAZNICA -= $OSTATOK; // Получаю сумму

                            $MEMBER_ID = $FinalEnvData[$list_keys[count($list_keys) - 1]]->getMemberId(); // Получаю ID пользователя
                            echo "ID пользователя: " . $MEMBER_ID . " <BR>";

                            for ($i = (count($list_keys) - 1); $i < $MAX_Envelope_NumSize, $RAZNICA > 0; $i++) { // Максимум конвертов на сотрудника
                                $EnvelopeNumber++; // Расчет номера конверта
                                if (($MAX_Envelope_NumSize > 3) && ($i == ($MAX_Envelope_NumSize - 2)) && ($RAZNICA > (2 * $MAX_ENVELOPE_SUMM))) { // Если Сумма в 4ом конверте > чем максимально возможная сумма
                                    $OSTATOK = ($RAZNICA % $MAX_ENVELOPE_SUMM); // То расчитываю остаток
                                    $Env = new Envelopes(); // Создаю объект конверта
                                    $Env->setSumm($OSTATOK); // Устанавливаю остаток от суммы
                                    $Env->setMemberId($MEMBER_ID); // ID пользователя
                                    $Env->setEnvelopeId($EnvelopeNumber); // Номер конверта
                                    $Env->setStatus(1);
                                    $RAZNICA -= $OSTATOK; // Из суммы вычитаю остаток
                                    $FinalEnvData[] = $Env; // Добавляю в  массив данных
                                } else if ($i == ($MAX_Envelope_NumSize - 1)) {
                                    $Env = new Envelopes(); // Создаю объект конверта
                                    $Env->setSumm($RAZNICA); // Устанавливаю ВСЕ от суммы
                                    $Env->setMemberId($MEMBER_ID); // ID пользователя
                                    $Env->setEnvelopeId($EnvelopeNumber); // Номер конверта
                                    $RAZNICA = 0; // Сумму выставляю в 0
                                    $FinalEnvData[] = $Env; // Добавляю в  массив данных
                                } else {
                                    if (($RAZNICA < $MAX_ENVELOPE_SUMM)) { // Если меньше Максимальной суммы
                                        $Env = new Envelopes(); // Создаю объект
                                        $Env->setSumm($RAZNICA); //Устанавливаю сумму конверта
                                        $Env->setMemberId($MEMBER_ID); // Устанавливаю ID пользователя
                                        $Env->setEnvelopeId($EnvelopeNumber); // Установка номера конверта
                                        $Env->setStatus(1);

                                        $RAZNICA = 0; // Установить сумму в 0.

                                        $FinalEnvData[] = $Env; // Добавить в массив
                                    } else if ($RAZNICA > $MAX_ENVELOPE_SUMM) { // Если больше максимальной суммы
                                        $Env = new Envelopes(); // Создаю объект
                                        $Env->setSumm($MAX_ENVELOPE_SUMM); // Устанавливаю МАКСИМАЛЬНУЮ сумму конверта
                                        $Env->setMemberId($MEMBER_ID); // Устанавливаю ID пользователя
                                        $Env->setEnvelopeId($EnvelopeNumber);  // Установка номера конверта
                                        $Env->setStatus(1);

                                        $RAZNICA -= $MAX_ENVELOPE_SUMM; // Вычитаю максимальную сумму

                                        if ($RAZNICA < $MIN_ENVELOPE_SUMM) { // Если остаток суммы меньше MIN_ENVELOPE_SUMM
                                            $Env->setSumm($Env->getSumm() + $RAZNICA);  // То выдаем в том же конверте
                                            $RAZNICA = 0; // Сумму по ведомости ставлю в 0
                                        }

                                        $FinalEnvData[] = $Env;
                                    }
                                }
                            }
                        }
                    }
                }
                echo "<BR>";
//                sort($FinalEnvData);
            }
        } else { // Помечен на увольнение или уволен, убираем из ведомости
            $member_array = array_filter($FinalEnvData, function ($obj) use ($member_id) { // Ищу все конверты в SQL данных
                return ($obj->getMemberId() == $member_id); // где ID пользователя в ведомости в конвертах совпадает
            });

            foreach ($member_array as $key => $value) {
                $FinalEnvData[$key]->setSumm(0);
                $FinalEnvData[$key]->setStatus(2);
//                unset($FinalEnvData[$key]); //Удаляю
//                sort($FinalEnvData);
            }
        }
    }
}

// Получаю массив ID пользователей
//$Members_ID_Array = array();
//foreach ($FinalEnvData as $value) {
//    if (!in_array($value->getMemberId(), $Members_ID_Array)) {
//        $Members_ID_Array[] =$value->getMemberId();
//    }
//}
//var_dump($Members_ID);

if (isset($_POST['Confirm'])) {
//    $dbh = dbConnect();

    if (count($SQLEnvData) == 0) { // Если еще не заносили ничего в БД
        foreach ($rawData as $item) { //Перебираю ведомость, получаю id сотрудников, для поиска в объектах конвертов
            $current = array_filter($FinalEnvData, function ($obj) use ($item) {
                return $obj->getMemberId() == $item->getId(); // Ищем все конверты где ID пользователя в ведомости в конвертах совпадает
            });

            if ($current) {
                foreach ($current as $env_item) {
                    $query = "select * from envelope where member_id=? and envelope_id=? and type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y')";
                    try {
                        $sth = $dbh->prepare($query); // Подготавливаем запрос
                        $sth->execute([$item->getId(), $env_item->getEnvelopeId(), $item->getTypeOfPayment(), $item->getDate()]);
                    } catch (PDOException $e) {
                        echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                    }

                    if ($sth->rowCount() == 0) {
                        $query = "insert into envelope (member_id,envelope_id,summ,date,type_of_payment) VALUES (?,?,?,STR_TO_DATE(?,'%m.%Y'),?)";
                        try {
                            $sth = $dbh->prepare($query); // Подготавливаем запрос
                            $sth->execute([$item->getId(), $env_item->getEnvelopeId(), $env_item->getSumm(), $item->getDate(), $item->getTypeOfPayment()]);
                        } catch (PDOException $e) {
                            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                        }
                    }
                }
            } else {
                continue;
            }
        }
    } else {
        $current = array_filter($FinalEnvData, function ($obj) {
            return $obj->getStatus() > 0; // Ищем все конверты где ID пользователя в ведомости в конвертах совпадает
        });

        if ($current) {
            foreach ($current as $env_item) {
                $query = "select * from envelope where member_id=? and envelope_id=? and type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y')";
                try {
                    $sth = $dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$env_item->getMemberId(), $env_item->getEnvelopeId(), $TypeOfPayment, $date]);
                } catch (PDOException $e) {
                    echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                }

                if ($sth->rowCount() == 0) {
                    $query = "insert into envelope (member_id,envelope_id,summ,date,type_of_payment) VALUES (?,?,?,STR_TO_DATE(?,'%m.%Y'),?)";
                    try {
                        $sth = $dbh->prepare($query); // Подготавливаем запрос
                        $sth->execute([$env_item->getMemberId(), $env_item->getEnvelopeId(), $env_item->getSumm(), $date, $TypeOfPayment]);
                    } catch (PDOException $e) {
                        echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                    }
                } else {
                    $query = "update envelope SET summ=? where member_id=? AND envelope_id=? AND date=STR_TO_DATE(?,'%m.%Y') AND type_of_payment=?";
                    try {
                        $sth = $dbh->prepare($query); // Подготавливаем запрос
                        $sth->execute([$env_item->getSumm(),$env_item->getMemberId(), $env_item->getEnvelopeId(), $date, $TypeOfPayment]);
                    } catch (PDOException $e) {
                        echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                    }
                }
            }
        }
    }

    $RS->ChangeStatus($date,$TypeOfPayment,(++$ReportStatus['status_id']));
    $dbh = null;
    header("Refresh:0");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Test</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
                <?php
                if (isset($TypeOfPayment)) {
                    switch ($TypeOfPayment) {
                        case 1:
                            echo "Конверты премии " . $date;
                            break;
                        case 2:
                            echo "Конверты аванса " . $date;
                            break;
                        case 3:
                            echo "Конверты аванса2 " . $date;
                            break;
                        case 4:
                            echo "Конверты \"расчета\" " . $date;
                            break;
                    }
                } else {
                    echo "Не выбран тип выплаты";
                }
                ?>
            </h4>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Список пользователей</a></li>
                <li><a href="prepaid_expense.php?date=<?php echo $date; ?>&TypeOfPayment=<?php echo $TypeOfPayment; ?>">Расчет</a></li>
                <li class="active">Конверты</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">

            <?php
            if (count($SQLEnvData) == 0) {
                ?>

                <div class="box box-default box-solid">
                    <form method="post" class="form-horizontal">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Максимальная сумма в конверте</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="MAX_ENV_SUMM" value="<?php echo $MAX_ENVELOPE_SUMM; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Минимальная сумма в конверте</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="MIN_ENV_SUMM" value="<?php echo $MIN_ENVELOPE_SUMM ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Количество конвертов</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="MAX_ENV_NUMSIZE" value="<?php echo $MAX_Envelope_NumSize; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input type="submit" class="btn btn-info pull-right" value="Ok" name="Ок">
                        </div>
                    </form>
                </div>
                <?php
            }
            ?>

            <div class="box box-info box-solid">
                <div class="box-body" style="overflow-y: auto">
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ФИО</th>
                            <th class="col-md-2 text-center">Сумма начисленно</th>
                            <?php
                            for ($i = 0; $i < $MAX_Envelope_NumSize; $i++) {
                                echo "<th>№ (" . ($i + 1) . ")</th>";
                                echo "<th class='col-md-1'>&sum; (руб)</th>";
                            }
                            ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $count = 0;
                        $FullSumm = 0;

                        foreach ($rawData as $item) {
                            $current = array_filter($FinalEnvData, function ($obj) use ($item) {
                                return $obj->getMemberId() == $item->getId(); // Ищем все конверты где ID пользователя в ведомости в конвертах совпадает
                            });

                            if ($current) {
                                $count++;
                                $FullSumm += $item->getSumm();// Сумма итого

                                echo "<tr>";
                                echo "<td>" . $count . "</td>";
                                switch ($TypeOfPayment) { // Вывожу ФИО с ссылкой на персональный расчет
                                    case 1:
                                        echo "<td>";
                                        echo "<a href='premium.php?id=" . $item->getId() . "&date=" . $date . "'>" . $item->getFio() . "</a>";
                                        echo "</td>";
                                        break;
                                    case 2:
                                        echo "<td><a href='salary.php?id=" . $item->getId() . "&date=" . $date . "'>" . $item->getFio() . "</a>";
                                        echo "</td>";
                                        break;
                                }
//                                echo "<td>" . $item->getFio()  . "/td>";
                                echo "<td class='text-center'>" . number_format($item->getSumm(), 2, ',', ' ') . "</td>";


                                foreach ($current as $env_item) {
                                    echo "<td class='text-center'> <b>" . $env_item->getEnvelopeId() . "</b></td>";
                                    switch ($env_item->getStatus()) {
                                        case 0:
                                            echo "<td>" . number_format($env_item->getSumm(), 2, ',', ' ') . "</td>";
                                            break;
                                        case 1:
                                            echo "<td style='color: red' title='" . $env_item->getOldSumm() . "'>" . number_format($env_item->getSumm(), 2, ',', ' ') . "</td>";
                                            break;
                                        case 2:
                                            echo "<td style='color: cornflowerblue' title='" . $env_item->getOldSumm() . "'>" . number_format($env_item->getSumm(), 2, ',', ' ') . "</td>";
                                            break;
                                    }
                                }
                                echo "</tr>";
                            } else {
                                continue;
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="2">Итого:</th>
                            <th class="text-center"><?php echo number_format($FullSumm, 2, ',', ' ') ; ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="box-footer">
                    <form method="post">
                        <?php
                        switch ($ReportStatus['status_id']) {
                            case 1:
                                break;
                            case 2:
                                break;
                            case 3:
                                break;
                            case 4:
                                echo "<input type=\"submit\" class=\"btn btn-warning\" name=\"Confirm\" value=\"Выплачено\">";
                                break;
                            case 5:
                                break;
                            case 6:
                                echo "<a href=\"departmentalreport.php?date=".$date."&TypeOfPayment=".$TypeOfPayment."\" class='btn btn-flat btn-primary'  style='margin: 5px;' >Детальные расходы</a>";
                                break;
                            default:
                                break;
                        }
                        ?>
<!--                        <input type="submit" class="btn btn-warning pull-right" name="Confirm" value="Отправить в кассу">-->
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


<script>
    $(function () {

        $('#MembersTable').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })
    })
</script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
</body>
</html>

