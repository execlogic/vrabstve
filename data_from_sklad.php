<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.08.18
 * Time: 18:29
 */

/*
 * Импорт данных из 1С Склад
 */
require_once 'app/FileCSV.php';
require_once 'app/MemberInterface.php';
require_once 'app/StatgroupInterface.php';
require_once 'app/DepartmentInterface.php';

require_once 'app/ErrorInterface.php';

require_once 'app/DataItem.php';
require_once 'app/DataManager.php';
require_once 'app/DataParserInterface.php';
require_once 'app/DataInterface.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

/*
 * Роли и доступы
 * Создаю объект
 */
$RI = new RoleInterface();
/*
 * Получаю доступ
 */
$Roles = $RI->getRoles($User->getMemberId());

/*
 * Если пользователю не разрешено заносить данные, то отправляю в 404
 */
if (!array_filter($Roles, function ($Role) {
    return ($Role->getImport1C() == 1);
})) {
    header("Location: 404.php");
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

/*
 * Создаю класс ErrorInterface
 */
$Errors = new ErrorInterface();

/*
 * Report статус
 */
$RS = new ReportStatus();
/*
 * Разбор файла
 * Если были нажаты кнопки  submit или submit_import или submit_del_import
 */
if (isset($_POST["submit"]) || isset($_POST["submit_import"]) || isset($_POST["submit_del_import"])) {
    /*
     * Создаю объект MemberInterface
     */
    $MI = new MemberInterface();
    /*
     * Получаю список сотрудников из БД
     */
    $MI->fetchMembers();
    /*
     * Получая массив сотруднико
     */
    $members_array = $MI->GetMembers();

    /*
     * Создаю объект StatgroupInterface (статгруппы)
     */
    $ST = new StatgroupInterface();
    /*
     * Получаю список статгрупп из БД
     */
    $ST->fetch();
    /*
     * Полученные данные сохраняю в массив
     */
    $statgroup_array = $ST->get();

    /*
     * Создаю объект DepartmentInterface (отделы)
     */
    $DI = new DepartmentInterface();
    /*
     * Получаю список отделов из БД
     */
    $DI->fetchDepartments();
    /*
     * Полученные данные сохраняю в массив
     */
    $departments_array = $DI->GetDepartments();

    /*
     * Создаю объект и инициализирую переменными
     */
    $Data = new DataParserInterface($members_array, $statgroup_array, $departments_array);

    /*
     * Создаю переменную $array типа массив
     */
    $array = array();
    try {
        /*
         * Создаю объект FileCSV
         */
        $uploadFile = new FileCSV();
        /*
         * Задаю имя файла
         */
        $uploadFile->setFormName("CSVfileToUpload");
        /*
         * Загружаю на сервер
         */
        $uploadFile->Upload();
        /*
         * Распарсиваю в массив $array
         */
        $array = $uploadFile->ParseCSV(";");
    } catch (Exception $e) {
        $Errors->add($e->getMessage() . "<br>");
    }

    /*
     * Если последняя строка == "ИТОГО ПО ОТЧЕТУ:", то удаляю ее
     */
    if ($array[count($array) - 1][0] == "ИТОГО ПО ОТЧЕТУ:") {
        unset($array[count($array) - 1]); //Отрезаю последнюю строку если там есть ИТОГО
    }

    /*
     * Удаляю заголовки
     */
    unset($array[0]); //удаляю заголовки
    unset($array[1]);

    /*
     * Сортирую массив
     */
    sort($array);
    /*
     * Разбираю массив
     */
    foreach ($array as $item) {
        /*
         * Обрезаю концевые пробелы в item[4]
         */
        $item[4] = trim($item[4]);
        /*
         * Если item[4] пустой, то продолжаю разбор массива
         */
        if (empty($item[4])) {
            continue;
        }

        /*
         * Передаем метолу parser массив данных и ссылку на объект ошибок
         */
        try {
            $Data->parser($item, $Errors);
        } catch (Exception $e) {
            $Errors->add($e->getMessage());
        }
    }
    /*
     * Удаляем загруженный файл
     */
    $uploadFile->DeleteFile();
}

/*
 * Если была нажата кнопка submit_import
 */
if (isset($_POST["submit_import"])) {
    /*
     * Получаю дата в переменную
     */
    $date = $Data->getDate();

    /*
     * Разбираю массива объектов и добавляю в БД
     */
    foreach ($Data->get() as $dataobj) {
        $Data->add($dataobj, $Errors);
    }

    /*
     * Разбираю данные по торговым точкам, как объект, и добавляю их в БД
     */
    foreach ($Data->get_tt() as $dataobj) {
        $Data->add_tt($dataobj, $Errors);
    }

    /*
     * Разбираю данные по статгруппам, как объект, и добавляю их в БД
     */
    foreach ($Data->get_stat() as $dataobj) {
        $Data->add_statgroup($dataobj, $Errors);
    }

    /*
     * Разбираю данные как  служба сервиса, как объект, и добавляю их в БД
     */
    foreach ($Data->get_service() as $dataobj) {
        $Data->add_service($dataobj, $Errors);
    }


    if ($RS->getStatus($date, 1)['status_id'] == 0) {
        /*
         * Проверяю все данные для установки статуса, в зависимости от типа оплаты
         */
        if ($RS->checkStatus($date, 1)) {
            /*
             * Устанавливаю статус
             */
            $RS->ChangeStatus($item[0], 1, 1);
        };
    }

    /*
     * Проверка.
     * Создаю объект DataInterface
     */
    $DI = new DataInterface($members_array, $statgroup_array, $departments_array);
    /*
     * Извлекаю данные исходя из даты
     * Все, по ТТ, по статгруппе, по службе сервиса
     */
    $DI->fetch($date);
    $DI->fetch_tt($date);
    $DI->fetch_statgroup($date);
    $DI->fetch_service($date);

}

if (isset($_POST["submit_del_import"])) {
    /*
     * Получаю дату
     */
    $date = $Data->getDate();

    /*
     * Удаляю данные из БД
     */
//    try {
//        $Data->del();
//        $Data->del_tt();
//        $Data->del_statgroup();
//        $Data->del_service();
//    } catch (Exception $e) {
//        $Errors->add($e->getMessage());
//    }

    /*
     * Разбираю массива объектов и добавляю в БД
     */
    foreach ($Data->get() as $dataobj) {
        $Data->addUpdate($dataobj, $Errors);
    }

    /*
     * Разбираю данные по торговым точкам, как объект, и добавляю их в БД
     */
    foreach ($Data->get_tt() as $dataobj) {
        $Data->addUpdate_tt($dataobj, $Errors);
    }

    /*
     * Разбираю данные по статгруппам, как объект, и добавляю их в БД
     */
    foreach ($Data->get_stat() as $dataobj) {
        $Data->addUpdate_statgroup($dataobj, $Errors);
    }

    /*
     * Разбираю данные как  служба сервиса, как объект, и добавляю их в БД
     */
    foreach ($Data->get_service() as $dataobj) {
        $Data->addUpdate_service($dataobj, $Errors);
    }

    /*
     * Проверка вытаскиваю из таблицы
     * Создаю объект DataInterface
     */
    $DI = new DataInterface($members_array, $statgroup_array, $departments_array);
    try {
        /*
        * Извлекаю данные исходя из даты
        * Все, по ТТ, по статгруппе, по службе сервиса
        */
        $DI->fetch($date);
        $DI->fetch_tt($date);
        $DI->fetch_statgroup($date);
        $DI->fetch_service($date);
    } catch (Exception $e) {
        $Errors->add($e->getMessage());
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Импорт данных из 1С</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <?php
            /*
                 * Вывод информации об ощибках
                 */
            if ($Errors->getCount() > 0) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="TableErr">
                            <thead>
                            <th>Причина</th>
                            <th>Сообщение</th>
                            </thead>
                            <tbody>
                            <?php
                            foreach (array_unique($Errors->getErrors()) as $value) {
                                echo "<tr><td>" . $value . "</td><td></td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php
            }
            ?>

            <?php
            if ((isset($_POST["submit_import"])) || (isset($_POST["submit_del_import"]))) {
                ?>
                <div class="box box-solid box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Результат обработки импорта</h3>
                    </div>
                    <div class="box-body">
                        <div>
                            <h4>Все данные</h4>
                            <table class="table table-bordered" id="Table1">
                                <thead>
                                <th>Дата</th>
                                <th>Менеджер(id)</th>
                                <th>Статгруппа(id)</th>
                                <th>ТТ(id)</th>
                                <!--                                <th>Менеджер сервиса(id)</th>-->
                                <th>Продажи</th>
                                <th>Себестоимость</th>
                                <th>Прибыль</th>
                                <th>Прибыль %</th>
                                </thead>
                                <?php
                                foreach ($DI->get() as $item) {
                                    echo "<tr> <td>" . $item->getDate() . "</td> <td>" . $item->getName() . " (" . $item->getId() . ") </td> <td>" . $item->getStatgroupName() . " (" . $item->getStatgroupId() . ") </td> <td>" . $item->getDepartmentName() . " (" . $item->getDepartmentId() . ") </td> <td>" . number_format($item->getSale(), 2, ',', ' ') . "</td> <td>" . number_format($item->getConstPrice(), 2, ',', ' ') . "</td> <td>" . number_format($item->getProfit(), 2, ',', ' ') . "</td><td>" . number_format($item->getProfitprocent(), 2, ',', ' ') . "</td></tr>";
                                }
                                echo "<tfoot><th colspan='3'>Всего записей:</th><th>" . $DI->count() . "</th></tfoot>";
                                ?>
                            </table>
                        </div>


                        <div>
                            <h4>Данные по ТТ</h4>
                            <table class="table table-bordered" id="Table2">
                                <thead>
                                <th>Дата</th>
                                <th>ТТ(id)</th>
                                <th>Продажи</th>
                                <th>Себестоимость</th>
                                <th>Прибыль</th>
                                <th>Прибыль %</th>
                                </thead>
                                <?php
                                foreach ($DI->get_tt() as $item) {
                                    echo "<tr> <td>" . $item->getDate() . "</td> <td>" . $item->getName() . " (" . $item->getId() . ") </td> <td>" . number_format($item->getSale(), 2, ',', ' ') . "</td> <td>" . number_format($item->getConstPrice(), 2, ',', ' ') . "</td> <td>" . number_format($item->getProfit(), 2, ',', ' ') . "</td><td>" . number_format($item->getProfitprocent(), 2, ',', ' ') . "</td></tr>";
                                }
                                echo "<tfoot><th colspan='3'>Всего записей:</th><th>" . $DI->count_tt() . "</th></tfoot>";
                                ?>
                            </table>
                        </div>

                        <div>
                            <h4>Данные по статгрупп</h4>
                            <table class="table table-bordered" id="Table3">
                                <thead>
                                <th>Дата</th>
                                <th>Статгрупп(id)</th>
                                <th>Продажи</th>
                                <th>Себестоимость</th>
                                <th>Прибыль</th>
                                <th>Прибыль %</th>
                                </thead>
                                <?php
                                foreach ($DI->get_stat() as $item) {
                                    echo "<tr> <td>" . $item->getDate() . "</td> <td>" . $item->getName() . " (" . $item->getId() . ") </td> <td>" . number_format($item->getSale(), 2, ',', ' ') . "</td> <td>" . number_format($item->getConstPrice(), 2, ',', ' ') . "</td> <td>" . number_format($item->getProfit(), 2, ',', ' ') . "</td><td>" . number_format($item->getProfitprocent(), 2, ',', ' ') . "</td></tr>";
                                }
                                echo "<tfoot><th colspan='3'>Всего записей:</th><th>" . $DI->count_stat() . "</th></tfoot>";
                                ?>
                            </table>
                        </div>

                        <div>
                            <h4>Данные по сервис</h4>
                            <table class="table table-bordered" id="Table4">
                                <thead>
                                <th>Дата</th>
                                <th>Статгрупп(id)</th>
                                <th>Продажи</th>
                                <th>Себестоимость</th>
                                <th>Прибыль</th>
                                <th>Прибыль %</th>
                                </thead>
                                <?php
                                if ($DI->get_service() != null) {
                                    foreach ($DI->get_service() as $item) {
                                        echo "<tr> <td>" . $item->getDate() . "</td> <td>" . $item->getName() . " (" . $item->getId() . ") </td> <td>" . number_format($item->getSale(), 2, ',', ' ') . "</td> <td>" . number_format($item->getConstPrice(), 2, ',', ' ') . "</td> <td>" . number_format($item->getProfit(), 2, ',', ' ') . "</td><td>" . number_format($item->getProfitprocent(), 2, ',', ' ') . "</td></tr>";
                                    }
                                    echo "<tfoot><th colspan='3'>Всего записей:</th><th>" . $DI->count_service() . "</th></tfoot>";
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <?php
            if (isset($_POST["submit"])) {
                ?>
                <div class="box box-solid box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Результат обработки</h3>
                    </div>
                    <div class="box-body">
                        <div>
                            <h4>Все данные</h4>
                            <table class="table table-bordered" id="Table1">
                                <thead>
                                <th>Дата</th>
                                <th>Менеджер(id)</th>
                                <th>Статгруппа(id)</th>
                                <th>ТТ(id)</th>
                                <!--                                <th>Менеджер сервиса(id)</th>-->
                                <th>Продажи</th>
                                <th>Себестоимость</th>
                                <th>Прибыль</th>
                                <th>Прибыль %</th>
                                </thead>
                                <?php
                                foreach ($Data->get() as $item) {
//                                    echo "<tr> <td>" . $item->getDate() . "</td> <td>" . $item->getManager()['name'] . " (" . $item->getManager()['id'] . ") </td> <td>" . $item->getStatgroup()['name'] . " (" . $item->getStatgroup()['id'] . ") </td> <td>" . $item->getDepartment()['name'] . " (" . $item->getDepartment()['id'] . ") </td> <td>" . $item->getService()['name'] . " (" . $item->getService()['id'] . ") </td> <td>" . number_format($item->getSale(),2, ',', ' ') . "</td> <td>" . number_format($item->getConstPrice(),2, ',', ' ') . "</td> <td>" . number_format($item->getProfit(),2, ',', ' ') . "</td><td>" . number_format($item->getProfitprocent(),2, ',', ' ') . "</td></tr>";
                                    echo "<tr> <td>" . $item->getDate() . "</td> <td>" . $item->getName() . " (" . $item->getId() . ") </td> <td>" . $item->getStatgroupName() . " (" . $item->getStatgroupId() . ") </td> <td>" . $item->getDepartmentName() . " (" . $item->getDepartmentId() . ") </td> <td>" . number_format($item->getSale(), 2, ',', ' ') . "</td> <td>" . number_format($item->getConstPrice(), 2, ',', ' ') . "</td> <td>" . number_format($item->getProfit(), 2, ',', ' ') . "</td><td>" . number_format($item->getProfitprocent(), 2, ',', ' ') . "</td></tr>";
                                }
                                echo "<tfoot><th colspan='3'>Всего записей:</th><th>" . $Data->count() . "</th></tfoot>";
                                ?>
                            </table>
                        </div>


                        <div>
                            <h4>Данные по ТТ</h4>
                            <table class="table table-bordered" id="Table2">
                                <thead>
                                <th>Дата</th>
                                <th>ТТ(id)</th>
                                <th>Продажи</th>
                                <th>Себестоимость</th>
                                <th>Прибыль</th>
                                <th>Прибыль %</th>
                                </thead>
                                <?php
                                foreach ($Data->get_tt() as $item) {
                                    echo "<tr> <td>" . $item->getDate() . "</td> <td>" . $item->getName() . " (" . $item->getId() . ") </td> <td>" . number_format($item->getSale(), 2, ',', ' ') . "</td> <td>" . number_format($item->getConstPrice(), 2, ',', ' ') . "</td> <td>" . number_format($item->getProfit(), 2, ',', ' ') . "</td><td>" . number_format($item->getProfitprocent(), 2, ',', ' ') . "</td></tr>";
                                }
                                echo "<tfoot><th colspan='3'>Всего записей:</th><th>" . $Data->count_tt() . "</th></tfoot>";
                                ?>
                            </table>
                        </div>

                        <div>
                            <h4>Данные по статгрупп</h4>
                            <table class="table table-bordered" id="Table3">
                                <thead>
                                <th>Дата</th>
                                <th>Статгрупп(id)</th>
                                <th>Продажи</th>
                                <th>Себестоимость</th>
                                <th>Прибыль</th>
                                <th>Прибыль %</th>
                                </thead>
                                <?php
                                foreach ($Data->get_stat() as $item) {
                                    echo "<tr> <td>" . $item->getDate() . "</td> <td>" . $item->getName() . " (" . $item->getId() . ") </td> <td>" . number_format($item->getSale(), 2, ',', ' ') . "</td> <td>" . number_format($item->getConstPrice(), 2, ',', ' ') . "</td> <td>" . number_format($item->getProfit(), 2, ',', ' ') . "</td><td>" . number_format($item->getProfitprocent(), 2, ',', ' ') . "</td></tr>";
                                }
                                echo "<tfoot><th colspan='3'>Всего записей:</th><th>" . $Data->count_stat() . "</th></tfoot>";
                                ?>
                            </table>
                        </div>

                        <div>
                            <h4>Данные по сервис</h4>
                            <table class="table table-bordered" id="Table4">
                                <thead>
                                <th>Дата</th>
                                <th>Статгрупп(id)</th>
                                <th>Продажи</th>
                                <th>Себестоимость</th>
                                <th>Прибыль</th>
                                <th>Прибыль %</th>
                                </thead>
                                <?php
                                foreach ($Data->get_service() as $item) {
                                    echo "<tr> <td>" . $item->getDate() . "</td> <td>" . $item->getName() . " (" . $item->getId() . ") </td> <td>" . number_format($item->getSale(), 2, ',', ' ') . "</td> <td>" . number_format($item->getConstPrice(), 2, ',', ' ') . "</td> <td>" . number_format($item->getProfit(), 2, ',', ' ') . "</td><td>" . number_format($item->getProfitprocent(), 2, ',', ' ') . "</td></tr>";
                                }
                                echo "<tfoot><th colspan='3'>Всего записей:</th><th>" . $Data->count_service() . "</th></tfoot>";
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <div class="box">
                <form method="post" enctype="multipart/form-data">
                    <div class="box-header">
                        <h4>Импорт и разбор данных из 1С Склад(CSV)</h4>
                    </div>
                    <div class="box-body">
                        <div class="box box-info">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Выберите файл: </label>
                                    <input type="file" name="CSVfileToUpload" id="CSVfileToUpload">

                                    <p class="help-block">
                                        В CSV файле поля должны быть разделены с помощью точки с запятой';'.
                                    </p>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-flat btn-sm btn-success" name="submit"><i class="fa fa-check"></i> Проверить</button>
                                <button type="submit" class="btn btn-flat btn-sm btn-warning" name="submit_import"><i class="fa fa-save"></i> Импортировать</button>
                                <button type="submit" class="btn btn-flat btn-sm btn-danger" name="submit_del_import"><i class="fa fa-save"></i> Удалить и Импортировать</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>

<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>
<script>
    $(function () {

        $('#TableErr').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': false,
            'ordering': true,
            'info': false,
            'autoWidth': true
        })

        $('#Table1').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })

        $('#Table2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })

        $('#Table3').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })

        $('#Table4').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })

    })
</script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>

