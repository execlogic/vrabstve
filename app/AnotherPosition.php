<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 22.08.18
 * Time: 18:11
 */

require_once 'functions.php';

class AnotherPosition
{
    private $dbh;
    private $date;

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    public function get($days) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }


        $query = "select 
		t1.member_id,t5.name,t5.lastname,t5.middle,
        t1.direction_id, t2.name as direction_name, 
        t1.department_id,t3.name as department_name,
        t1.position_id,t4.name as new_position_name,
        DATE_FORMAT(t1.date_s,'%d.%m.%Y') as date
        from member_jobpost as t1 
        LEFT JOIN direction as t2 ON(t1.direction_id = t2.id)
        LEFT JOIN department as t3 ON(t1.department_id = t3.id)
        LEFT JOIN position as t4 ON(t1.position_id = t4.id)
        RIGHT JOIN member as t5 ON(t1.member_id= t5.id)
        where t1.active=1 and version>1 and date_s between (NOW() - INTERVAL ? DAY) AND NOW() order by t1.date_s desc";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$days])) {
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $raw;
    }

}