<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 24.08.18
 * Time: 18:37
 */


class StatGroup
{
    private $name;
    private $skald_id;
    private $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @return mixed
     */
    public function getSkladId()
    {
        return $this->skald_id;
    }

    /**
     * @param mixed $skald_id
     */
    public function setSkladId($skald_id)
    {
        $this->skald_id = $skald_id;
    }
}