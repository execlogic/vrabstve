<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.07.18
 * Time: 12:41
 */

class Department
{
    private $id;
    private $name;
    private $idsklad;
    private $namesklad;
    private $direction_id;


    /**
     * @param mixed $direction_id
     */
    public function setDirectionId($direction_id)
    {
        $this->direction_id = $direction_id;
    }

    /**
     * @return mixed
     */
    public function getDirectionId()
    {
        return $this->direction_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $idsklad
     */
    public function setIdsklad($idsklad)
    {
        $this->idsklad = $idsklad;
    }

    /**
     * @param mixed $namesklad
     */
    public function setNamesklad($namesklad)
    {
        $this->namesklad = $namesklad;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdsklad()
    {
        return $this->idsklad;
    }

    /**
     * @return mixed
     */
    public function getNamesklad()
    {
        return $this->namesklad;
    }


}