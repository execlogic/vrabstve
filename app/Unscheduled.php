<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 21.01.19
 * Time: 13:36
 */

require_once "app/functions.php";
require_once "app/MemberInterface.php";
require_once 'app/PayRollInterface.php';
require_once 'app/PayRoll.php';
require_once 'app/ReportStatus.php';
require_once 'app/Notify.php';

require_once "admin/RoleInterface.php";

class Unscheduled
{
    private $dbh = null;

    /*
     * Метод класса добавляет данные в FIX таблицу.
     * Выполняется когда статус меняется с 5 на 6
     */
    public function AddToPayment($ID)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        /*
        * Создаю объект интерфейса ведомости
        */
        $PI = new PayRollInterface();
        /*
        * Создаю объект MemberInterface
        */
        $MI = new MemberInterface();

        /*
         * Получаю список пользователей с минимальными данными
         */
        $MI->fetchMembers();


        $query = "select * from unscheduled where id=?";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$ID]); // Выполняю запрос
        } catch (PDOException $e) {
            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
        }

        /*
         * Получаю данные их БД
         */
        $rawData = $sth->fetch();

        /*
         * Получаю сумма и дату
         */
        $unshedulled_summ = $rawData['summ'];
        $unscheduled_id = $rawData['id'];
        $unscheduled_date = DateTime::createFromFormat("Y-m-d",$rawData['date']);
        $user_id = $rawData['member_id'];

        /*
         * Получаю данные для ведомости по пользователю
         */
        $member = current(array_filter($MI->getMembers(), function ($m) use (&$user_id) {
            return $m->getId() == $user_id;
        }));


        $PayRoll = new PayRoll(); //Создаем объект
        $PayRoll->setDate($unscheduled_date->format("m.Y")); // устанавливаем дату
        $PayRoll->setTypeOfPayment(5);// Тип выплаты
        $PayRoll->setId($user_id); // Получаю id пользователя
        $PayRoll->setDirection($member->getDirection()); // Получаю Направление
        $PayRoll->setDepartment($member->getDepartment()); // Получаю отдел
        $PayRoll->setFio($member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()); // Устанавливаю ФИО
        $PayRoll->setSalary(0); // получаю данные по окладу
        $PayRoll->setNDFL(0); // Получаю НДФЛ
        $PayRoll->setRO(0); // Получаю ИЛ
        $PayRoll->setAdvancePayment(0); // Получаю выплаченный аванс
        $PayRoll->setAbsences(0); // Отсутствия
        $PayRoll->setCommercial(0); // Устанавливаю коммерческую премию
//        $PayRoll->setAdministrative($unshedulled_summ); // Устанавливаю Административную премию
        $PayRoll->setHold(0); // Устанавливаю удержания
        $PayRoll->setCorrecting(0); //Устанавливаю корректировки
        $PayRoll->setMotivation(0);
        $PayRoll->setSumm($unshedulled_summ); // Устанавливаю Административную премию
        $PayRoll->setUnscheduledId($unscheduled_id);

//        $PayRoll->Calculation();

        /*
         * Добавляю в финансовую ведомость
         */
        try {
            $PI->AddToFinancialPayout($PayRoll);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /*
     * Отменить выплату
     * по ID выплаты
     */
    public function Cansel($ID)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "select t1.id as report_status_id,t1.member_id, t1.unscheduled_id, t1.status_id, t1.active, t2.date, t2.summ, t2.note
        from report_status as t1
        LEFT JOIN unscheduled as t2 ON (t1.unscheduled_id = t2.id)
        where t2.id=? LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$ID]); // Выполняю запрос
        } catch (PDOException $e) {
            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
        }

        if ($sth->rowCount() > 0) {
            $rawData = $sth->fetch();
            $report_status_id = $rawData['report_status_id'];

            $query = "UPDATE report_status SET active=0 where id=?";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$report_status_id]); // Выполняю запрос
            } catch (PDOException $e) {
                echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
            }
        }
    }

    /*
     * Инкрементирую статус ведомости
     */
    public function IncStatus($ID)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        /*
        * Создаю объект статуса ведомости
        */
        $RS = new ReportStatus();

        /*
         * Объект ролей
         */
        $RI = new RoleInterface();

        /*
         * Получаю ID статуса
         */
        $status = $RS->getMemberStatusById($ID);

        if ($status > 1 || $status < 6) {
            /*
             * Если статус == 5, то добавляю в ведомость
             */
            if ($status == 5) {
                $this->AddToPayment($ID);
            }

            /*
             * Инкрементирую статус
             */
            $RS->ChangeMemberStatusByID($ID, (++$status));

            switch ($status) {
                /*
                * С НЛ на кассира
                */
                case 4:
                    $query = "SELECT DISTINCT(member_id) FROM zp.Roles where RoleSettings_id=3 OR RoleSettings_id=5 OR RoleSettings_id=6;";
                    try {
                        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                        $sth->execute();
                    } catch (PDOException $e) {
                        throw new Exception($e->getMessage());
                    }
                    $rawDirectors = $sth->fetchAll(PDO::FETCH_ASSOC);
                    break;
                    /*
                     * С руководителя на НЛ, c Кассира на НЛ и с НЛ в расходы
                     */
                case 3:
                case 5:
                case 6:
                    $query = "SELECT DISTINCT(member_id) FROM zp.Roles where RoleSettings_id=5 OR RoleSettings_id=6;";
                    try {
                        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                        $sth->execute();
                    } catch (PDOException $e) {
                        throw new Exception($e->getMessage());
                    }
                $rawDirectors = $sth->fetchAll(PDO::FETCH_ASSOC);
            }

            /*
            * Уведомление пользователей
            */
            $NF = new Notify();

            switch ($status) {
                case 3:
                    $subj = "Внеплановая выплата";
                    $msg = "Отвественные директора проверку закончили";
                    break;
                case 4:
                    $subj = "Внеплановая выплата";
                    $msg = "Ведомость передана кассиру";
                    break;
                case 5:
                    $subj = "Внеплановая выплата";
                    $msg = "Выдано";
                    break;
                case 6:
                    $subj = "Внеплановая выплата";
                    $msg = "Ведомость закрыта";
                    break;
            }

            foreach ($rawDirectors as $item) {
                if (!is_null($item['member_id'])) {
                    $NF->addNotify($item['member_id'], $subj, $msg);
                }
            }
        }
    }

    /*
     * Добавляю внеплановую выплату
     */
    public function addUnscheduled($user_id, $summ, $note, $avans)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "insert into unscheduled (member_id,date,summ,note, avans ) VALUES (?,NOW(),?,?,?)";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$user_id, $summ, $note, $avans]); // Выполняю запрос
        } catch (PDOException $e) {
            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
        }

        if ($sth->rowCount() > 0) {
            $ID = $this->dbh->lastInsertId();

            $query = "insert into report_status (date,type_of_payment,status_id,member_id,unscheduled_id) VALUES (NOW(),?,?,?,?)";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                /*
                 * Отправляю запрос на отвественного директора
                 */
                $sth->execute([5, 2, $user_id, $ID]); // Выполняю запрос
            } catch (PDOException $e) {
                echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
            }

            if ($sth->rowCount() == 0) {
                echo "Ошибка добавляения в базу";
            } else {
                /*
                * Уведомление пользователей
                */
                $NF = new Notify();

                $query = "select t1.id as status_id, t1.member_id, t1.unscheduled_id, t1.status_id, t1.active, t2.date, t2.summ, t2.note,t3.department_id
                    from report_status as t1
                    LEFT JOIN unscheduled as t2 ON (t1.unscheduled_id = t2.id)
                    LEFT JOIN member_jobpost as t3 ON (t1.member_id = t3.member_id)
                    where t2.id=? LIMIT 1";

                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$ID]); // Выполняю запрос
                } catch (PDOException $e) {
                    echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                }

                $department_id = $sth->fetch()['department_id'];

                $query = "SELECT t1.member_id as director_id
                    FROM zp.Roles as t1
                    where (t1.RoleSettings_id = 4 AND (t1.department_id = ? || t1.department_id = 0)) || t1.RoleSettings_id = 6";

                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$department_id]); // Выполняю запрос
                } catch (PDOException $e) {
                    echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                }

                $rawDirectors = $sth->fetchAll(PDO::FETCH_ASSOC);
                $sth = null;

                $subj = "Внеплановая выплата";
                $msg = "Созднана новая заявка";

                foreach ($rawDirectors as $item) {
                    if (!is_null($item['director_id'])) {
                        $NF->addNotify($item['director_id'], $subj, $msg);
                    }
                }
            }

        }
        /*
         * Очистка памяти
         */
        $sth = null;
    }

    /*
     * Удаление внеплановой выплаты (отмена выплаты)
     */
    public function delUnscheduled($DeleteID)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        if (is_numeric($DeleteID)) {
            $query = "select * from report_status where unscheduled_id=?";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$DeleteID]); // Выполняю запрос
            } catch (PDOException $e) {
                echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
            }

            if ($sth->rowCount() > 0) {
                $query = "UPDATE report_status SET active=0 where unscheduled_id=?";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$DeleteID]); // Выполняю запрос
                } catch (PDOException $e) {
                    echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                }

                $query = "UPDATE unscheduled SET active=0 where id=?";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$DeleteID]); // Выполняю запрос
                } catch (PDOException $e) {
                    echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                }
            }
        }
    }

    /*
     * Получаю данные за месяц
     */
    public function getDataForTheMonth($date, $user_id) {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "select t1.*,t2.status_id,t3.name as status_name
	              from unscheduled as t1
	              LEFT JOIN report_status as t2 ON (t1.id=t2.unscheduled_id)
	              LEFT JOIN status as t3 on (t2.status_id=t3.id)
	              WHERE t1.date BETWEEN DATE_FORMAT(STR_TO_DATE(?,'%m.%Y') ,'%Y-%m-01') AND DATE_ADD(DATE_FORMAT(STR_TO_DATE(?,'%m.%Y') ,'%Y-%m-01'), INTERVAL 1 MONTH) 
                  AND t1.member_id=?
                  order by t1.active DESC ,t1.id DESC
                  ";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date, $date, $user_id]); // Выполняю запрос
        } catch (PDOException $e) {
            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
        }

        if ($sth->rowCount() > 0) { // Если количество строк > 0, значит данные есть
            $raw = $sth->fetchAll();

            $sth = null;
            return $raw;
        }

        $sth = null;

        return null;
    }

    /*
     * Получаю все активные данные
     */

    public function getData($RoleDepartmentFilter) {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $Where = "";
        if (is_array($RoleDepartmentFilter) && $RoleDepartmentFilter!= NULL && count($RoleDepartmentFilter) > 0) {
            foreach ($RoleDepartmentFilter as $key => $id) {
                if (strlen($Where) == 0) {
//                    $Where .= "t4.department_id=?";
                    $Where .= "t4.department_id=".$id;
                } else {
//                    $Where .= " || t4.department_id=?";
                    $Where .= " || t4.department_id=".$id;
                }
            }
        }


        /*
         * Создаю запрос для внеплановой выплаты
         */
        $query = "select t1.*,DATE_FORMAT(t1.date, '%d.%m.%Y') as date,t2.name,t2.lastname,t2.middle,t3.summ,t3.note,t3.avans,t5.id as department_id,t5.name as department_name,t6.name as status_name, t7.name as direction_name
          FROM report_status as t1
          LEFT JOIN member as t2 ON (t1.member_id = t2.id)
          LEFT JOIN unscheduled as t3 ON (t1.unscheduled_id=t3.id)
          LEFT JOIN member_jobpost as t4 on (t2.id = t4.member_id)
          LEFT JOIN department as t5 on (t4.department_id = t5.id)
          LEFT JOIN direction as t7 on (t4.direction_id = t7.id)
          LEFT JOIN status as t6 ON (t1.status_id = t6.id)
          WHERE t1.type_of_payment=5 AND t1.status_id < 6
          AND t1.active=1";

        if (strlen($Where) > 0) {
            $query .= " AND (" . $Where . ")";
        }

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            if (strlen($Where) > 0) {
                $sth->execute($RoleDepartmentFilter); // Выполняю запрос
            } else {
                $sth->execute(); // Выполняю запрос
            }
        } catch (PDOException $e) {
            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
        }

        /*
         * Если количество строк больше 0, то получаю данные в переменную raw
         */
        if ($sth->rowCount() > 0) {
            $raw = $sth->fetchAll();
            return $raw;
        }
        
        return null;
    }

    /**
     *
     */
    public function checkForAvans($member_id, $date)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $dt = DateTime::createFromFormat("m.Y", $date);
        $q = "select * from unscheduled where member_id = ? and date>STR_TO_DATE(?, '%Y-%m-%d') and date<STR_TO_DATE(?, '%Y-%m-%d') and avans=1 and active=1";
        try {
            $sth = $this->dbh->prepare($q);
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("PDO Error" . $e->getMessage());
        }
        $sth->execute([$member_id, $dt->format("Y-m")."-01", $dt->format("Y-m")."-31"]);

        if ($sth->rowCount()==0) {
            return false;
        } else {
            return true;
        }
    }
}
