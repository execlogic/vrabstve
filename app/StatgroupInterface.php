<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 24.08.18
 * Time: 18:36
 */

require_once 'functions.php';
require_once 'Statgroup.php';

class StatgroupInterface
{
    private $statgroupList = array();
    private $dbh;

    public function fetch() {
        $this->dbh=dbConnect();
        $query = "select * from statgroup order by id";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute();
        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $this->dbh = null;

        foreach ($rawresult as $value) {
            $statgroup = new StatGroup();
            $statgroup->setId($value['id']);
            $statgroup->setName($value['name']);
            $statgroup->setSkladId($value['idsklad']);
            array_push($this->statgroupList,$statgroup);
        }
    }

    public function get() {
        if (empty($this->statgroupList)) {
            throw new Exception("Пустой список");
        }
        return $this->statgroupList;
    }

    public function add($idskald,$name) {
        $this->dbh=dbConnect();

        if (empty($this->dbh)) {
            echo "Error<br>";
        }

//        if (empty($idskald)) {
//            $idskald=0;
//        }

        $query = "INSERT INTO statgroup (name,idsklad) VALUES (?,?)";
        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare'.$exception->getMessage());
        }

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$name,$idskald])) {
            $error = $sth->errorInfo();
            throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: '.$error[1].'.<br> Текст ошибки: '.$error[2]."<BR>");
        }

        $sth = null;
        $this->dbh = null;

        return true;
    }
}