<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 13.09.18
 * Time: 11:42
 */
require_once 'app/functions.php';
require_once 'app/ErrorInterface.php';
require_once 'app/DataParserInterface.php';

/**
 * Class DataInterface
 * Класс для получения данных из БД и вывода на экран
 */
class DataInterface extends  DataParserInterface {
    public function fetch($date) {
        $query = "select * from data_manager where date=?";
        $sth = $this->dbh->prepare($query);
        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$date]);
        if ($sth->rowCount() == 0) {
            throw  new Exception("Fetch: не могу получить данные");
        }

        foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $item) {
            $member = current(array_filter($this->members,function ($obj) use ($item) {
                return $obj->getId() == $item['member_id'];
            }));

            $statgroup = current(array_filter($this->statgroup,function ($obj) use ($item) {
                return $obj->getId() == $item['statgroup_id'];
            }));

            $department = current(array_filter($this->departments,function ($obj) use ($item) {
                return $obj->getId() == $item['department_id'];
            }));


            $DS = new DataManager();
            $DS->setId($item['member_id']);
            if ($member) {
                $DS->setName($member->getSkladName());
            }
            $DS->setStatgroupId($item['statgroup_id']);
            if ($statgroup) {
                $DS->setStatgroupName($statgroup->getName());
            }
            $DS->setDepartmentId($item['department_id']);
            if ($department) {
                $DS->setDepartmentName($department->getNamesklad());
            }
            $DS->setDate($item['date']);
            $DS->setSale($item['sales']);
            $DS->setConstPrice($item['const_price']);
            $DS->setProfit($item['profit']);
            $DS->setProfitProcent($item['profit_procent']);
            array_push($this->data, $DS); // Все данные

        }

        $sth = null;
    }

    public function fetch_tt($date) {
        $query = "select * from data_TT where date=?";
        $sth = $this->dbh->prepare($query);
        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$date]);
        if ($sth->rowCount() == 0) {
            throw  new Exception("Fetch: не могу получить данные");
        }

        foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $item) {
            $department = current(array_filter($this->departments,function ($obj) use ($item) {
                return $obj->getId() == $item['department_id'];
            }));

            $DTT = new DataItem();
            if ($department) {
                $DTT->setName($department->getNamesklad());
            }
            $DTT->setDate($date);
            $DTT->setId($item['department_id']);
            $DTT->setSale($item['sales']);
            $DTT->setConstprice($item['const_price']);
            $DTT->setProfit($item['profit']);
            $DTT->setProfitProcent($item['profit_procent']);
            array_push($this->data_tt, $DTT);
        }

        $sth = null;
    }

    public function fetch_statgroup($date) {
        $query = "select * from data_statgroup where date=?";
        $sth = $this->dbh->prepare($query);
        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$date]);
        if ($sth->rowCount() == 0) {
            throw  new Exception("Fetch: не могу получить данные");
        }

        foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $item) {
            $statgroup = current(array_filter($this->statgroup,function ($obj) use ($item) {
                return $obj->getId() == $item['statgroup_id'];
            }));

            $DTT = new DataItem();
            if ($statgroup) {
                $DTT->setName($statgroup->getName());
            }


            $DTT->setDate($date);
            $DTT->setId($item['statgroup_id']);
            $DTT->setSale($item['sales']);
            $DTT->setConstprice($item['const_price']);
            $DTT->setProfit($item['profit']);
            $DTT->setProfitProcent($item['profit_procent']);
            array_push($this->data_stat, $DTT);
        }
        $sth = null;
    }

    public function fetch_service($date) {
        $query = "select * from data_service where date=?";
        $sth = $this->dbh->prepare($query);
        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$date]);
        if ($sth->rowCount() == 0) {
//            throw  new Exception("Fetch: не могу получить данные");
            $this->data_service = null;
        } else {
            foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $item) {
                $mbrobj = current(array_filter($this->members, function ($obj) use (&$item) {
                    return $obj->getId() == $item['member_id'];
                }));

                $DTT = new DataItem();

                if ($mbrobj) {
                    $DTT->setName($mbrobj->getSkladName());
                }
                $DTT->setDate($date);
                $DTT->setId($item['member_id']);
                $DTT->setSale($item['sales']);
                $DTT->setConstprice($item['const_price']);
                $DTT->setProfit($item['profit']);
                $DTT->setProfitProcent($item['profit_procent']);
                array_push($this->data_service, $DTT);
            }
        }
        $sth = null;
    }
}