<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.07.18
 * Time: 12:07
 */

include_once "functions.php";
include_once "MemberDirection.php";

class Member
{
    private $id; //ID

    private $name; //Имя
    private $lastname; //Фамилия
    private $middle; //Отчество
    private $maiden_name; //Девичья фамлия

    private $sex; //Пол
    private $birthday; //Дата рождения
    private $login; //Логин
    private $personal_number; //Личный номер ЗИК
    private $sklad_name; //1C Склад
    private $employment_date; //Дата приема на работу
    private $note; // заметки

    private $direction; //Направление
    private $department; //Отдел
    private $position; // Должность

    private $workstatus=1;
    private $workstatus_note;
    private $workstatus_date;

    private $probation; // Комментарий испытательного срока
    private $dismissal_date; //Дата увольнения
    private $reason_leaving; //Причина увольнения
    private $reason_leaving_id; //ID Причина увольнения
    private $reason_leaving_note; // Заметка

    /* Рабочая информация */
    private $workphone; //Рабочий телефон
    private $workmobilephone; //Рабочий телефон
    private $worklocalphone; //Рабочий местный телефон

    /* Личные телефоны */
    private $personalphone; //личный телефон
    private $personalmobilephone; //Мобильный телефон
    private $emergencyphone; //Аварийные телефоны

    /* Почтовые адреса  */
    private $emails = null;

    /* Личная информация */
    private $nation; //ID
    private $nation_name; //Название

    /* Финансовый блок */
    private $basepercent; // процент наценки. double
    private $brandpercent; // процент наценки. Шумилов
    private $finance_matrix; // Финансовая матрица
    private $salary = NULL;

    private $AvansPay = 1; // Выплачивать аванс - 1 выплачивать, 0 - не выплачивать
    private $AvansProcent = 50; // Сколько процентов аванс от оклада

    private $AdministrativePrize=NULL; //Административная премия
    private $AdministrativePrizeNote = NULL;
    private $MobileCommunication=NULL; // Мобильная связь
    private $MobileFormula=NULL; // Расчет по формуле
    private $Transport=NULL; // Мобильная связь
    private $TransportFormula=NULL; // Расчет по формуле

    private $motivation; // Мотивация

    private $yearbonus_type = NULL; // Тип выплаты
    private $yearbonus_pay = NULL; // Годовой бонус отчисления
    private $yearbonus_note = NULL; // Комментарий к годовому бонусу

    /*
     * Блок УРВ
     */
    private $not_count_urv = 0; // Не считать УРВ
//    private $vacation_not_expire = 0; // Отпуск не сгорает
//    private $vacation_not_expire_date = NULL; // Отпуск не сгорает с даты
    private $vacation_extension_date = []; // Продление отпуска до...

    /*
     *
     *      Setters
     *
     */

    /**
     * @param array $vacation_extension_date
     */
    public function setVacationExtensionDate($vacation_extension_date)
    {
        $this->vacation_extension_date = $vacation_extension_date;
    }

    /**
     * @param int $vacation_not_expire
     */
//    public function setVacationNotExpire($vacation_not_expire)
//    {
//        $this->vacation_not_expire = $vacation_not_expire;
//    }

    /**
     * @param null $vacation_not_expire_date
     */
//    public function setVacationNotExpireDate($vacation_not_expire_date)
//    {
//        $this->vacation_not_expire_date = $vacation_not_expire_date;
//    }

    /**
     * @param int $not_count_urv
     */
    public function setNotCountUrv($not_count_urv)
    {
        $this->not_count_urv = $not_count_urv;
    }

    /**
     * @param mixed $brandpercent
     */
    public function setBrandpercent($brandpercent)
    {
        $this->brandpercent = $brandpercent;
    }

    /**
     * @param null $MobileFormula
     */
    public function setMobileFormula($MobileFormula)
    {
        $this->MobileFormula = $MobileFormula;
    }

    /**
     * @param null $Transport
     */
    public function setTransport($Transport)
    {
        $this->Transport = $Transport;
    }

    /**
     * @param null $TransportFormula
     */
    public function setTransportFormula($TransportFormula)
    {
        $this->TransportFormula = $TransportFormula;
    }

    /**
     * @param null $yearbonus_note
     */
    public function setYearbonusNote($yearbonus_note)
    {
        $this->yearbonus_note = $yearbonus_note;
    }

    /**
     * @return null
     */
    public function getYearbonusNote()
    {
        return $this->yearbonus_note;
    }


    /**
     * @param null $AdministrativePrizeNote
     */
    public function setAdministrativePrizeNote($AdministrativePrizeNote)
    {
        $this->AdministrativePrizeNote = $AdministrativePrizeNote;
    }

    /**
     * @return null
     */
    public function getAdministrativePrizeNote()
    {
        return $this->AdministrativePrizeNote;
    }


    /**
     * @param null $yearbonus_type
     */
    public function setYearbonusType($yearbonus_type)
    {
        $this->yearbonus_type = $yearbonus_type;
    }

    /**
     * @return null
     */
    public function getYearbonusType()
    {
        return $this->yearbonus_type;
    }


    /**
     * @param null $yearbonus_pay
     */
    public function setYearbonusPay($yearbonus_pay)
    {
        $this->yearbonus_pay = $yearbonus_pay;
    }

    /**
     * @return null
     */
    public function getYearbonusPay()
    {
        return $this->yearbonus_pay;
    }

    /**
     * @return int
     */
    public function getAvansProcent()
    {
        return $this->AvansProcent;
    }

    /**
     * @return int
     */
    public function getAvansPay()
    {
        return $this->AvansPay;
    }

    /**
     * @param int $AvansPay
     */
    public function setAvansPay($AvansPay)
    {
        $this->AvansPay = $AvansPay;
    }

    /**
     * @param int $AvansProcent
     */
    public function setAvansProcent($AvansProcent)
    {
        $this->AvansProcent = $AvansProcent;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $employment_date
     */
    public function setEmploymentDate($employment_date)
    {
        $this->employment_date = $employment_date;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @param mixed $maiden_name
     */
    public function setMaidenName($maiden_name)
    {
        $this->maiden_name = $maiden_name;
    }

    /**
     * @param mixed $middle
     */
    public function setMiddle($middle)
    {
        $this->middle = $middle;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @param mixed $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @param mixed $personal_number
     */
    public function setPersonalNumber($personal_number)
    {
        $this->personal_number = $personal_number;
    }

    /**
     * @param mixed $sklad_name
     */
    public function setSkladName($sklad_name)
    {
        $this->sklad_name = $sklad_name;
    }

    /**
     * @param mixed $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @param mixed $department
     */
    public function setDepartmentFromObject($dep)
    {
        $this->department = array("id"=>$dep->getId(), "name"=>$dep->getName());
    }

    /**
     * @param mixed $direction
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
    }

    public function setDirectionFromObject($dir)
    {
        $this->direction = array("id"=>$dir->getId(), "name"=>$dir->getName());
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function setPositionFromObject($pos)
    {
        $this->position = array("id"=>$pos->getId(), "name"=>$pos->getName());
    }

    /**
     * @param mixed $probation
     */
    public function setProbation($probation)
    {
        $this->probation = $probation;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @param mixed $dismissal_date
     */
    public function setDismissalDate($dismissal_date)
    {
        $this->dismissal_date = $dismissal_date;
    }

    /**
     * @param mixed $workphone
     */
    public function setWorkphone($workphone)
    {
        $this->workphone = $workphone;
    }

    /**
     * @param mixed $workmobilephone
     */
    public function setWorkmobilephone($workmobilephone)
    {
        $this->workmobilephone = $workmobilephone;
    }

    /**
     * @param mixed $worklocalphone
     */
    public function setWorklocalphone($worklocalphone)
    {
        $this->worklocalphone = $worklocalphone;
    }

    /**
     * @param mixed $personalphone
     */
    public function setPersonalphone($personalphone)
    {
        $this->personalphone = $personalphone;
    }

    /**
     * @param mixed $emergencyphone
     */
    public function setEmergencyphone($emergencyphone)
    {
        $this->emergencyphone = $emergencyphone;
    }

    /**
     * @param mixed $personalmobilephone
     */
    public function setPersonalmobilephone($personalmobilephone)
    {
        $this->personalmobilephone = $personalmobilephone;
    }

    /**
     * @param mixed $emails
     */
    public function setEmails($emails)
    {
        $this->emails = $emails;
    }

    /**
     * @param mixed $nation
     */
    public function setNation($nation)
    {
        $this->nation = $nation;
    }

    /**
     * @param mixed $nation_name
     */
    public function setNationName($nation_name)
    {
        $this->nation_name = $nation_name;
    }

    /**
     * @param mixed $reason_leaving
     */
    public function setReasonLeaving($reason_leaving)
    {
        $this->reason_leaving = $reason_leaving;
    }

    /**
     * @param mixed $reason_leaving_id
     */
    public function setReasonLeavingId($reason_leaving_id)
    {
        $this->reason_leaving_id = $reason_leaving_id;
    }

    /**
     * @param mixed $reason_leaving_note
     */
    public function setReasonLeavingNote($reason_leaving_note)
    {
        $this->reason_leaving_note = $reason_leaving_note;
    }

    /**
     * @param mixed $baseprecent
     */
    public function setBasePercent($percent)
    {
        $this->basepercent = $percent;
    }


    /**
     * @param mixed $finance_matrix
     */
    public function setFinanceMatrix($finance_matrix)
    {
        $this->finance_matrix = $finance_matrix;
    }

    /**
     * @param mixed $motivation
     */
    public function setMotivation($motivation)
    {
        $this->motivation = $motivation;
    }

    /**
     * @param mixed $salary
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    /**
     * @param int $MobileCommunication
     */
    public function setMobileCommunication($MobileCommunication)
    {
        $this->MobileCommunication = $MobileCommunication;
    }

    /**
     * @param int $AdministrativePrize
     */
    public function setAdministrativePrize($AdministrativePrize)
    {
        $this->AdministrativePrize = $AdministrativePrize;
    }

    /**
     * @param mixed $workstatus
     */
    public function setWorkstatus($workstatus)
    {
        $this->workstatus = $workstatus;
    }

    /**
     * @param mixed $workstatus_note
     */
    public function setWorkstatusNote($workstatus_note)
    {
        $this->workstatus_note = $workstatus_note;
    }

    /*
     *
     *      Getters
     *
     */

    /**
     * @return array
     */
    public function getVacationExtensionDate()
    {
        return $this->vacation_extension_date;
    }

    /**
     * @return int
     */
//    public function getVacationNotExpire()
//    {
//        return $this->vacation_not_expire;
//    }

    /**
     * @return null
     */
//    public function getVacationNotExpireDate()
//    {
//        return $this->vacation_not_expire_date;
//    }

    /**
     * @return int
     */
    public function getNotCountUrv()
    {
        return $this->not_count_urv;
    }

    /**
     * @return mixed
     */
    public function getBrandpercent()
    {
        return $this->brandpercent;
    }

    /**
     * @return null
     */
    public function getMobileFormula()
    {
        return $this->MobileFormula;
    }

    /**
     * @return null
     */
    public function getTransport()
    {
        return $this->Transport;
    }

    /**
     * @return null
     */
    public function getTransportFormula()
    {
        return $this->TransportFormula;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEmploymentDate()
    {
        return $this->employment_date;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return mixed
     */
    public function getMaidenName()
    {
        return $this->maiden_name;
    }

    /**
     * @return mixed
     */
    public function getMiddle()
    {
        return $this->middle;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @return mixed
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @return mixed
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @return mixed
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return mixed
     */
    public function getProbation()
    {
        return $this->probation;
    }

    /**
     * @return mixed
     */
    public function getDismissalDate()
    {
        return $this->dismissal_date;
    }

    /**
     * @return mixed
     */
    public function getWorkphone()
    {
        return $this->workphone;
    }

    /**
     * @return mixed
     */
    public function getWorkmobilephone()
    {
        return $this->workmobilephone;
    }

    /**
     * @return mixed
     */
    public function getWorklocalphone()
    {
        return $this->worklocalphone;
    }


    /**
     * @return mixed
     */
    public function getNation()
    {
        return $this->nation;
    }

    /**
     * @return mixed
     */
    public function getNationName()
    {
        return $this->nation_name;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @return mixed
     */
    public function getPersonalphone()
    {
        return $this->personalphone;
    }

    /**
     * @return mixed
     */
    public function getPersonalmobilephone()
    {
        return $this->personalmobilephone;
    }

    /**
     * @return mixed
     */
    public function getEmergencyphone()
    {
        return $this->emergencyphone;
    }

    /**
     * @return mixed
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function getSkladName()
    {
        return $this->sklad_name;
    }

    /**
     * @return mixed
     */
    public function getPersonalNumber()
    {
        return $this->personal_number;
    }

    /**
     * @return mixed
     */
    public function getReasonLeaving()
    {
        return $this->reason_leaving;
    }

    /**
     * @return mixed
     */
    public function getReasonLeavingId()
    {
        return $this->reason_leaving_id;
    }

    /**
     * @return mixed
     */
    public function getReasonLeavingNote()
    {
        return $this->reason_leaving_note;
    }

    /**
     * @return mixed
     */
    public function getFinanceMatrix()
    {
        return $this->finance_matrix;
    }

    /**
     * @return mixed
     */
    public function getBasePercent()
    {
        return $this->basepercent;
    }

    /**
     * @return mixed
     */
    public function getMotivation()
    {
        return $this->motivation;
    }

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @return int
     */
    public function getMobileCommunication()
    {
        return $this->MobileCommunication;
    }

    /**
     * @return int
     */
    public function getAdministrativePrize()
    {
        return $this->AdministrativePrize;
    }

    /**
     * @return mixed
     */
    public function getWorkstatus()
    {
        return $this->workstatus;
    }

    public function getWorkstatusDate()
    {
        return $this->workstatus_date;
    }

    /**
     * @return mixed
     */
    public function getWorkstatusNote()
    {
        return $this->workstatus_note;
    }

    /**
     * @param mixed $workstatus_date
     */
    public function setWorkstatusDate($workstatus_date)
    {
        $this->workstatus_date = $workstatus_date;
    }


    /*
     *
     *              Methods
     *
     */


    public function setAll($array) {
        $this->setId($array['id']);
        $this->setName($array['name']);
        $this->setLastname($array['lastname']);
        $this->setMiddle($array['middle']);
        $this->setMaidenName($array['maiden_name']);
        $this->setSex($array['sex']);
        $this->setBirthday($array['birthday']);
        $this->setEmploymentDate($array['employment_date']);
        $this->setDismissalDate($array['dismissal_date']);
        $this->setNotCountUrv($array['not_count_urv']);
//        $this->setVacationNotExpire($array['vacation_not_expire']);
//        $this->setVacationNotExpireDate($array['vacation_not_expire_date']);
    }

    public function SexToText() {
        if ($this->sex == 0) {
            return 'Мужской';
        } else {
            return 'Женский';
        }
        return 'Мужской';
    }


    public function ShowForTable() {
        echo "<tr>
        <td><a class='text-muted' href='profile.php?id=".$this->getId()."'>".$this->getId()."</a></td>
        <td><a class='text-muted' href='profile.php?id=".$this->getId()."'>".$this->getLastname(). "</a></td>
        <td><a class='text-muted' href='profile.php?id=".$this->getId()."'>".$this->getName(). "</a></td>
        <td><a class='text-muted' href='profile.php?id=".$this->getId()."'>".$this->getMiddle(). "</a></td>
        <td><a class='text-muted' href='profile.php?id=".$this->getId()."'>".$this->getMaidenName(). "</a></td>
        <td><a class='text-muted' href='profile.php?id=".$this->getId()."'>".$this->SexToText(). "</a></td>
        <td><a class='text-muted' href='profile.php?id=".$this->getId()."'>".$this->getBirthday(). "</a></td>
        <td><a class='text-muted' href='profile.php?id=".$this->getId()."'>".$this->getDirection()['name']."</a></td>
        <td><a class='text-muted' href='profile.php?id=".$this->getId()."'>".$this->getDepartment()['name']."</a></td>
        <td><a class='text-muted' href='profile.php?id=".$this->getId()."'>".$this->getPosition()['name']."</a></td>
        <td><a class='text-muted' href='profile.php?id=".$this->getId()."'>".$this->getEmploymentDate()."</a></td>
        <td>";
        if ($this->getDismissalDate()=='01.01.9999') {
            echo "<span class=\"label label-success\">Сотрудник работает</span>";
        } else {
            echo "<span class=\"label label-danger\" title='".$this->getReasonLeaving()."' >Сотрудник уволен [".$this->getDismissalDate()."]</span>";
        };
        echo "</td>
        </tr>";
    }
}