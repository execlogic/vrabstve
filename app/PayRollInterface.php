<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 08.10.18
 * Time: 18:24
 */

require_once 'functions.php';


class PayRollInterface
{
    private $dbh;
    private $financial_payout = array();

    private function roundSumm($summ)
    {
        // Округление до 10
        if ($summ > 0) { //Если сумма <= 0 то не обрабатываем данные
            //$summ = round($summ); // округляю сотые и десятые
            $summ = (int)$summ;

            $ostatok = ($summ % 10); // Получаю остаток от деления на 10
            if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
                $summ = $summ - $ostatok + 10;
            } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
                $summ = $summ - $ostatok;
            };
        } else {
            $summ = 0;
        }
        return $summ;
    }

    public function AddToFinancialPayout($PayRoll)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        if (!$this->dbh->inTransaction())
            $this->dbh->beginTransaction();

        if ($PayRoll->getTypeOfPayment() == 5) {
            $query = "select * from financial_payout where date=STR_TO_DATE(?, '%m.%Y') and member_id=? and type_of_payment=? and unshedulled_id=?";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$PayRoll->getDate(), $PayRoll->getId(), $PayRoll->getTypeOfPayment(), $PayRoll->getUnscheduledId()]);
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO' . $e->getMessage());
            }
        } else {
            $query = "select * from financial_payout where date=STR_TO_DATE(?, '%m.%Y') and member_id=? and type_of_payment=?";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$PayRoll->getDate(), $PayRoll->getId(), $PayRoll->getTypeOfPayment()]);
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO' . $e->getMessage());
            }
        }

        if ($sth->rowCount() > 0 && $PayRoll->getTypeOfPayment() != 5) {
            return false;
        } else {
            switch ($PayRoll->getTypeOfPayment()) {
                case 1:
                case 3:
                case 4:
                    /*
                     * Административная премия
                     */
                    if ($PayRoll->getTypeOfPayment() == 1 || $PayRoll->getTypeOfPayment() == 4) {
                        $query = "SELECT AdministrativePrize, Note from member_AdministrativePrize where member_id=? and active=1 LIMIT 1";
                        try {
                            $sth = $this->dbh->prepare($query);
                        } catch (PDOException $exception) {
                            throw new Exception('Ошибка prepare' . $exception->getMessage());
                        }

                        $sth->execute([$PayRoll->getId()]);
                        if ($sth->rowCount() > 0) {
                            $Member_AdmPrize = current($sth->fetchAll(PDO::FETCH_ASSOC));

                            $query = "SELECT id from administrative_block where type=2 and member_id=? and type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y')";
                            try {
                                $sth = $this->dbh->prepare($query);
                            } catch (PDOException $exception) {
                                throw new Exception('Ошибка prepare' . $exception->getMessage());
                            }

                            $sth->execute([$PayRoll->getId(), $PayRoll->getTypeOfPayment(), $PayRoll->getDate()]);

                            if ($sth->rowCount() == 0) {
                                $query = "insert into administrative_block (member_id, date, summ, note, active, change_date, type_of_payment, type, changer_id, administrative_block.lock) 
                                                                VALUES (?, STR_TO_DATE(?, '%m.%Y'), ?, ?, 1, NOW(), ?, 2, -1, 0)";
                                try {
                                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                                    $sth->execute([$PayRoll->getId(), $PayRoll->getDate(), $Member_AdmPrize['AdministrativePrize'], $Member_AdmPrize['Note'],
                                        $PayRoll->getTypeOfPayment()]);
                                } catch (PDOException $e) {
                                    throw new Exception('Не могу добавить в БД administrative_block.' . $e->getMessage());
                                }
                            }
                        }
                    }

                    /*
                     * Мобильная связь
                     */
//                    $query = "SELECT member_MobileCommunication from member_MobileCommunication where member_id=? and active=1 LIMIT 1";
//                    try {
//                        $sth = $this->dbh->prepare($query);
//                    } catch (PDOException $exception) {
//                        throw new Exception('Ошибка prepare' . $exception->getMessage());
//                    }
//
//                    $sth->execute([$PayRoll->getId()]);
//                    if ($sth->rowCount() > 0) {
//                        $Member_Mobile = current($sth->fetchAll(PDO::FETCH_ASSOC));
//
//                        $query = "SELECT * from administrative_block where type=8 and member_id = ? and type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y')";
//                        try {
//                            $sth = $this->dbh->prepare($query);
//                        } catch (PDOException $exception) {
//                            throw new Exception('Ошибка prepare' . $exception->getMessage());
//                        }
//
//                        $sth->execute([$PayRoll->getId(), $PayRoll->getTypeOfPayment(), $PayRoll->getDate()]);
//
//                        if ($sth->rowCount() == 0) {
//                            $query = "insert into administrative_block (member_id, date, summ, note, active, change_date,type_of_payment, type,changer_id, administrative_block.lock) VALUES (?, STR_TO_DATE(?, '%m.%Y'), ?, ?, 1, NOW(),?,?,?,1)";
//                            try {
//                                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
//                                $sth->execute([$PayRoll->getId(), $PayRoll->getDate(), $Member_Mobile['MobileCommunication'], "",
//                                    $PayRoll->getTypeOfPayment(), 8, -1]);
//                            } catch (PDOException $e) {
//                                throw new Exception('Не могу добавить в БД administrative_block.' . $e->getMessage());
//                            }
//                        }
//                    }

//                if ($PayRoll->getId()==2) {
//                    echo $PayRoll->getDate() . " " . $PayRoll->getId() . " " . $PayRoll->getTypeOfPayment() . " " . $PayRoll->getSalary(). " " .
//                    $PayRoll->getAdvancePayment() . " " . $PayRoll->getAbsences() . " " . $PayRoll->getNDFL() . " " . $PayRoll->getRo(). " " .
//                    $PayRoll->getCommercial() . " " . $PayRoll->getAdministrative() . " " . $PayRoll->getHold() . " " . $PayRoll->getCorrecting() . " " . $PayRoll->getYearBonusPay() . " " . $PayRoll->getSumm(), $this->roundSumm($PayRoll->getSumm()) . " " .
//                    $PayRoll->getDirectionId() . " " . $PayRoll->getDepartmentId() . " " . $PayRoll->getPositionId() . " " . $PayRoll->getAdvancedProcentId() . " " . $PayRoll->getBaseprocentId() . " " . $PayRoll->getMotivation();
//
//                    echo "INSERT INTO financial_payout(date, member_id, type_of_payment,
//                        Salary, PrepaidExpense, absence, ndfl, ro, CommercialPremium, AdministrativePremium, Hold, Correcting, YearBonusPay, sum, Total,
//                        direction_id, department_id, position_id, member_advanced_percent_id, member_percent_id, motivation, Kpi
//                    ) VALUES (STR_TO_DATE(".$PayRoll->getDate().", '%m.%Y'),".$PayRoll->getId().", ".$PayRoll->getTypeOfPayment().", ".$PayRoll->getSalary().",
//                    ".$PayRoll->getAdvancePayment().", ".$PayRoll->getAbsences().", ".$PayRoll->getNDFL().", ".$PayRoll->getRo().",
//                        ".$PayRoll->getCommercial().", ".$PayRoll->getAdministrative().", ".$PayRoll->getHold().", ".$PayRoll->getCorrecting().", ".$PayRoll->getYearBonusPay().", ".$PayRoll->getSumm().", ".$this->roundSumm($PayRoll->getSumm()).",
//                    ".$PayRoll->getDirectionId().", ".$PayRoll->getDepartmentId().", ".$PayRoll->getPositionId().", ".$PayRoll->getAdvancedProcentId().", ".$PayRoll->getBaseprocentId().", ".$PayRoll->getMotivation().",
//                        ".$PayRoll->getKPI().")<BR>";
//                    exit(255);
//                }

                    /*
                     * KPI
                     */
                    if ($PayRoll->getTypeOfPayment() == 1 || $PayRoll->getTypeOfPayment() == 4) {
                        $query = "SELECT * from member_kpi where member_id=? and active=1";
                        try {
                            $sth = $this->dbh->prepare($query);
                        } catch (PDOException $exception) {
                            throw new Exception('Ошибка prepare' . $exception->getMessage());
                        }

                        $sth->execute([$PayRoll->getId()]);
                        if ($sth->rowCount() > 0) {
                            $Member_KPI = $sth->fetchAll(PDO::FETCH_ASSOC);

                            foreach ($Member_KPI as $item) {
                                if (isset($item['num'])) {
                                    $query = "SELECT * from kpi_block where num=? and member_id=? and type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y')";
                                    try {
                                        $sth = $this->dbh->prepare($query);
                                    } catch (PDOException $exception) {
                                        throw new Exception('Ошибка prepare' . $exception->getMessage());
                                    }

                                    $sth->execute([$item['num'], $PayRoll->getId(), $PayRoll->getTypeOfPayment(), $PayRoll->getDate()]);

                                    if ($sth->rowCount() == 0) {
                                        $query = "insert into kpi_block (member_id, num, summ, note, date, type_of_payment, active, changer_id, kpi_block.lock) VALUES (?, ?, ?, ?, STR_TO_DATE(?, '%m.%Y'),?, 1, -1, 0)";
                                        try {
                                            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                                            $sth->execute([$PayRoll->getId(), $item['num'], $item['summ'], $item['note'], $PayRoll->getDate(), $PayRoll->getTypeOfPayment()]);
                                        } catch (PDOException $e) {
                                            throw new Exception('Не могу добавить в БД kpi_block.' . $e->getMessage());
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $query = "INSERT INTO financial_payout(date, member_id, type_of_payment, 
                    Salary, PrepaidExpense, absence, ndfl, ro, CommercialPremium, AdministrativePremium, Hold, Correcting, YearBonusPay, sum, Total,
                    direction_id, department_id, position_id, member_advanced_percent_id, member_percent_id, motivation, Kpi, data
                    ) VALUES (STR_TO_DATE(?, '%m.%Y'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    try {
                        $sth = $this->dbh->prepare($query);
                    } catch (PDOException $exception) {
                        throw new Exception('Ошибка prepare' . $exception->getMessage());
                    }
                    if (!$sth->execute([$PayRoll->getDate(), $PayRoll->getId(), $PayRoll->getTypeOfPayment(), $PayRoll->getSalary(),
                        $PayRoll->getAdvancePayment(), $PayRoll->getAbsences(), $PayRoll->getNDFL(), $PayRoll->getRo(),
                        $PayRoll->getCommercial(), $PayRoll->getAdministrative(), $PayRoll->getHold(), $PayRoll->getCorrecting(), $PayRoll->getYearBonusPay(), $PayRoll->getSumm(), $this->roundSumm($PayRoll->getSumm()),
                        $PayRoll->getDirectionId(), $PayRoll->getDepartmentId(), $PayRoll->getPositionId(), $PayRoll->getAdvancedProcentId(), $PayRoll->getBaseprocentId(), $PayRoll->getMotivation(),
                        $PayRoll->getKPI(), $PayRoll->getData()
                    ])) {
                        $error = $sth->errorInfo();
                        throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
                    }

                    break;
                case 2:
                    $query = "INSERT INTO financial_payout(date, member_id, type_of_payment, Salary, PrepaidExpense, absence, ndfl, ro, 
                    CommercialPremium, AdministrativePremium, Hold, Correcting, sum, Total, direction_id, department_id, position_id) 
                    VALUES (STR_TO_DATE(?, '%m.%Y'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    try {
                        $sth = $this->dbh->prepare($query);
                    } catch (PDOException $exception) {
                        throw new Exception('Ошибка prepare' . $exception->getMessage());
                    }

                    if (!$sth->execute([$PayRoll->getDate(), $PayRoll->getId(), $PayRoll->getTypeOfPayment(),
                        $PayRoll->getSalary(), $PayRoll->getAdvancePayment(), $PayRoll->getAbsences(),
                        $PayRoll->getNDFL(), $PayRoll->getRo(), $PayRoll->getCommercial(), $PayRoll->getAdministrative(),
                        $PayRoll->getHold(), $PayRoll->getCorrecting(), $PayRoll->getSumm(), $this->roundSumm($PayRoll->getSumm()),
                        $PayRoll->getDirectionId(), $PayRoll->getDepartmentId(), $PayRoll->getPositionId()])) {
                        $error = $sth->errorInfo();
                        throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
                    }
                    break;
                case 5:
                    $query = "INSERT INTO financial_payout(date, member_id, type_of_payment, AdministrativePremium, sum, Total, unscheduled_id) VALUES (STR_TO_DATE(?, '%m.%Y'),?,?,?,?,?,?)";
                    try {
                        $sth = $this->dbh->prepare($query);
                    } catch (PDOException $exception) {
                        throw new Exception('Ошибка prepare' . $exception->getMessage());
                    }

                    if (!$sth->execute([$PayRoll->getDate(), $PayRoll->getId(), $PayRoll->getTypeOfPayment(), $PayRoll->getAdministrative(), $PayRoll->getSumm(), $this->roundSumm($PayRoll->getSumm()), $PayRoll->getUnscheduledId()])) {
                        $error = $sth->errorInfo();
                        throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
                    }
                    break;
                case 6:
                    $query = "INSERT INTO financial_payout(date, member_id, type_of_payment, Correcting, sum, Total) VALUES (STR_TO_DATE(?, '%m.%Y'),?,?,?,?,?)";
                    try {
                        $sth = $this->dbh->prepare($query);
                    } catch (PDOException $exception) {
                        throw new Exception('Ошибка prepare' . $exception->getMessage());
                    }

                    if (!$sth->execute([$PayRoll->getDate(), $PayRoll->getId(), $PayRoll->getTypeOfPayment(), $PayRoll->getCorrecting(), $PayRoll->getSumm(), $this->roundSumm($PayRoll->getSumm())])) {
                        $error = $sth->errorInfo();
                        throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
                    }
                    break;
            }
        }

        if ($this->dbh->inTransaction())
            $this->dbh->commit();
    }

    /*
     * Блокирует внесенные данные на изменение в ведомости по сотруднику.
     * По-моему эта фигня не будет работать! Проще исходить из статуса
     */
    public function lockData($PayRoll)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "update administrative_block SET administrative_block.lock=1 where member_id=? and type_of_payment=1 and date=STR_TO_DATE(?, '%m.%Y')";
        $sth = $this->dbh->prepare($query);
        $sth->execute([$PayRoll->getId(), $PayRoll->getDate()]);

        $query = "update correcting_block SET correcting_block.lock=1 where member_id=? and type_of_payment=1 and date=STR_TO_DATE(?, '%m.%Y')";
        $sth = $this->dbh->prepare($query);
        $sth->execute([$PayRoll->getId(), $PayRoll->getDate()]);

        $query = "update retention_block SET retention_block.lock=1 where member_id=? and type_of_payment=1 and date=STR_TO_DATE(?, '%m.%Y')";
        $sth = $this->dbh->prepare($query);
        $sth->execute([$PayRoll->getId(), $PayRoll->getDate()]);


        $query = "update kpi_block SET kpi_block.lock=1 where member_id=? and type_of_payment=1 and date=STR_TO_DATE(?, '%m.%Y')";
        $sth = $this->dbh->prepare($query);
        $sth->execute([$PayRoll->getId(), $PayRoll->getDate()]);


        $query = "update member_awh SET member_awh.lock=1 where member_id=? and type_of_payment=1 and date=STR_TO_DATE(?, '%m.%Y')";
        $sth = $this->dbh->prepare($query);
        $sth->execute([$PayRoll->getId(), $PayRoll->getDate()]);
    }

    public function UpdateDataFinancialPayout($PayRollArray) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from financial_payout where date=STR_TO_DATE(?, '%m.%Y') and member_id=? and type_of_payment=?";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$PayRollArray['date'], $PayRollArray['member_id'], $PayRollArray['type_of_payment']]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $query = "UPDATE financial_payout SET data = ? WHERE date=STR_TO_DATE(?, '%m.%Y') and member_id=? and type_of_payment=?";

            try {
                $sth = $this->dbh->prepare($query);
            } catch (PDOException $exception) {
                throw new Exception('Ошибка prepare' . $exception->getMessage());
            }
            if (!$sth->execute([$PayRollArray['data'], $PayRollArray['date'], $PayRollArray['member_id'], $PayRollArray['type_of_payment']])) {
                $error = $sth->errorInfo();
                throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
            }
        }
    }

    public function UpdateFinancialPayout($PayRoll)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from financial_payout where date=STR_TO_DATE(?, '%m.%Y') and member_id=? and type_of_payment=?";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$PayRoll->getDate(), $PayRoll->getId(), $PayRoll->getTypeOfPayment()]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            switch ($PayRoll->getTypeOfPayment()) {
                case 1:
                case 2:
                case 3:
                case 4:
                    /*
                     * Административная премия
                     */
                    if ($PayRoll->getTypeOfPayment() == 1 || $PayRoll->getTypeOfPayment() == 4) {
                        $query = "SELECT AdministrativePrize, Note from member_AdministrativePrize where member_id=? and active=1 LIMIT 1";
                        try {
                            $sth = $this->dbh->prepare($query);
                        } catch (PDOException $exception) {
                            throw new Exception('Ошибка prepare' . $exception->getMessage());
                        }

                        $sth->execute([$PayRoll->getId()]);
                        if ($sth->rowCount() > 0) {
                            $Member_AdmPrize = current($sth->fetchAll(PDO::FETCH_ASSOC));

                            $query = "SELECT id from administrative_block where type=2 and member_id=? and type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y')";
                            try {
                                $sth = $this->dbh->prepare($query);
                            } catch (PDOException $exception) {
                                throw new Exception('Ошибка prepare' . $exception->getMessage());
                            }

                            $sth->execute([$PayRoll->getId(), $PayRoll->getTypeOfPayment(), $PayRoll->getDate()]);

                            if ($sth->rowCount() == 0) {
                                $query = "insert into administrative_block (member_id, date, summ, note, active, change_date, type_of_payment, type, changer_id, administrative_block.lock)
                                                                VALUES (?, STR_TO_DATE(?, '%m.%Y'), ?, ?, 1, NOW(), ?, 2, -1, 1)";
                                try {
                                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                                    $sth->execute([$PayRoll->getId(), $PayRoll->getDate(), $Member_AdmPrize['AdministrativePrize'], $Member_AdmPrize['Note'],
                                        $PayRoll->getTypeOfPayment()]);
                                } catch (PDOException $e) {
                                    throw new Exception('Не могу добавить в БД administrative_block.' . $e->getMessage());
                                }
                            }
                        }
                    }
//                    if ($PayRoll->getTypeOfPayment() == 1 || $PayRoll->getTypeOfPayment() == 3 || $PayRoll->getTypeOfPayment() == 4) {
//                        /*
//                         * Мобильная связь
//                         */
//                        $query = "SELECT member_MobileCommunication from member_MobileCommunication where member_id=? and active=1 LIMIT 1";
//                        try {
//                            $sth = $this->dbh->prepare($query);
//                        } catch (PDOException $exception) {
//                            throw new Exception('Ошибка prepare' . $exception->getMessage());
//                        }
//
//                        $sth->execute([$PayRoll->getId()]);
//                        if ($sth->rowCount() > 0) {
//                            $Member_Mobile = current($sth->fetchAll(PDO::FETCH_ASSOC));
//
//                            $query = "SELECT * from administrative_block where type=8 and member_id = ? and type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y')";
//                            try {
//                                $sth = $this->dbh->prepare($query);
//                            } catch (PDOException $exception) {
//                                throw new Exception('Ошибка prepare' . $exception->getMessage());
//                            }
//
//                            $sth->execute([$PayRoll->getId(), $PayRoll->getTypeOfPayment(), $PayRoll->getDate()]);
//
//                            if ($sth->rowCount() == 0) {
//                                $query = "insert into administrative_block (member_id, date, summ, note, active, change_date,type_of_payment, type,changer_id, administrative_block.lock) VALUES (?, STR_TO_DATE(?, '%m.%Y'), ?, ?, 1, NOW(),?,?,?,1)";
//                                try {
//                                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
//                                    $sth->execute([$PayRoll->getId(), $PayRoll->getDate(), $Member_Mobile['MobileCommunication'], "",
//                                        $PayRoll->getTypeOfPayment(), 8, -1]);
//                                } catch (PDOException $e) {
//                                    throw new Exception('Не могу добавить в БД administrative_block.' . $e->getMessage());
//                                }
//                            }
//                        }
//                    }

                    if ($PayRoll->getTypeOfPayment() == 1 || $PayRoll->getTypeOfPayment() == 4) {
                        /*
                         * KPI
                         */
                        $query = "SELECT * from member_kpi where member_id=? and active=1";
                        try {
                            $sth = $this->dbh->prepare($query);
                        } catch (PDOException $exception) {
                            throw new Exception('Ошибка prepare' . $exception->getMessage());
                        }

                        $sth->execute([$PayRoll->getId()]);
                        if ($sth->rowCount() > 0) {
                            $Member_KPI = $sth->fetchAll(PDO::FETCH_ASSOC);
                            foreach ($Member_KPI as $item) {
                                if (isset($item['num'])) {
                                    $query = "SELECT * from kpi_block where num=? and member_id=? and type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y')";
                                    try {
                                        $sth = $this->dbh->prepare($query);
                                    } catch (PDOException $exception) {
                                        throw new Exception('Ошибка prepare' . $exception->getMessage());
                                    }

                                    $sth->execute([$item['num'], $PayRoll->getId(), $PayRoll->getTypeOfPayment(), $PayRoll->getDate()]);

                                    if ($sth->rowCount() == 0) {
                                        $query = "insert into kpi_block (member_id, num, summ, note, date, type_of_payment, active, changer_id, kpi_block.lock) VALUES (?, ?, ?, ?, STR_TO_DATE(?, '%m.%Y'),?, 1, -1, 1)";
                                        try {
                                            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                                            $sth->execute([$PayRoll->getId(), $item['num'], $item['summ'], $item['note'], $PayRoll->getDate(), $PayRoll->getTypeOfPayment()]);
                                        } catch (PDOException $e) {
                                            throw new Exception('Не могу добавить в БД kpi_block.' . $e->getMessage());
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $query = "UPDATE financial_payout SET Salary=?, PrepaidExpense=?, absence=?, ndfl=?, ro=?, CommercialPremium=?, AdministrativePremium=?, Hold=?, Correcting=?, 
                    YearBonusPay=?, sum=?, Total=?, member_advanced_percent_id=?, member_percent_id=?, motivation=?, Kpi=?, data = ? WHERE date=STR_TO_DATE(?, '%m.%Y') and member_id=? and type_of_payment=?";

                    try {
                        $sth = $this->dbh->prepare($query);
                    } catch (PDOException $exception) {
                        throw new Exception('Ошибка prepare' . $exception->getMessage());
                    }
//                    var_dump($PayRoll->getData());exit(0);
                    if (!$sth->execute([$PayRoll->getSalary(),
                        $PayRoll->getAdvancePayment(), $PayRoll->getAbsences(), $PayRoll->getNDFL(), $PayRoll->getRo(),
                        $PayRoll->getCommercial(), $PayRoll->getAdministrative(), $PayRoll->getHold(), $PayRoll->getCorrecting(), $PayRoll->getYearBonusPay(), $PayRoll->getSumm(), $this->roundSumm($PayRoll->getSumm()),
                        $PayRoll->getAdvancedProcentId(), $PayRoll->getBaseprocentId(), $PayRoll->getMotivation(),
                        $PayRoll->getKPI(), $PayRoll->getData(), $PayRoll->getDate(), $PayRoll->getId(), $PayRoll->getTypeOfPayment()
                    ])) {
                        $error = $sth->errorInfo();
                        throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
                    }

                    break;
            }
        }
    }

    public function AddToFinancialPayoutLeave($PayRoll)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from financial_payout where date=STR_TO_DATE(?, '%m.%Y') and member_id=? and type_of_payment=?";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$PayRoll->getDate(), $PayRoll->getId(), $PayRoll->getTypeOfPayment()]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            throw new Exception("Такая запись уже присутсвует. " . $PayRoll->getDate() . " " . $PayRoll->getId() . " " . $PayRoll->getTypeOfPayment());
        } else {
            switch ($PayRoll->getTypeOfPayment()) {
                case 1:
                    $query = "INSERT INTO financial_payout(date, member_id, type_of_payment, type_of_payment_second, Salary, PrepaidExpense, absence, ndfl, ro, CommercialPremium, AdministrativePremium, Hold, Correcting, sum, Total) VALUES (STR_TO_DATE(?, '%m.%Y'),?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    try {
                        $sth = $this->dbh->prepare($query);
                    } catch (PDOException $exception) {
                        throw new Exception('Ошибка prepare' . $exception->getMessage());
                    }

                    if (!$sth->execute([$PayRoll->getDate(), $PayRoll->getId(), 4, $PayRoll->getTypeOfPayment(), $PayRoll->getSalary(), $PayRoll->getAdvancePayment(), $PayRoll->getAbsences(), $PayRoll->getNDFL(), $PayRoll->getRo(), $PayRoll->getCommercial(), $PayRoll->getAdministrative(), $PayRoll->getHold(), $PayRoll->getCorrecting(), $PayRoll->getSumm(), $this->roundSumm($PayRoll->getSumm())])) {
                        $error = $sth->errorInfo();
                        throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
                    }
                    break;
                case 2:
                    $query = "INSERT INTO financial_payout(date, member_id, type_of_payment, type_of_payment_second, Salary, absence, ndfl, ro, CommercialPremium, AdministrativePremium, Hold, Correcting, sum, Total) VALUES (STR_TO_DATE(?, '%m.%Y'),?,?,?,?,?,?,?,?,?,?,?,?)";
                    try {
                        $sth = $this->dbh->prepare($query);
                    } catch (PDOException $exception) {
                        throw new Exception('Ошибка prepare' . $exception->getMessage());
                    }

                    if (!$sth->execute([$PayRoll->getDate(), $PayRoll->getId(), 4, $PayRoll->getTypeOfPayment(), $PayRoll->getSalary(), $PayRoll->getAbsences(), $PayRoll->getNDFL(), $PayRoll->getRo(), $PayRoll->getCommercial(), $PayRoll->getAdministrative(), $PayRoll->getHold(), $PayRoll->getCorrecting(), $PayRoll->getSumm(), $this->roundSumm($PayRoll->getSumm())])) {
                        $error = $sth->errorInfo();
                        throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
                    }
                    break;
            }
        }
    }

    public function getAllFinancialPayout($date, $type)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from financial_payout where date=STR_TO_DATE(?, '%m.%Y') and type_of_payment=?";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date, $type]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
            return $raw;
        }
    }

    public function getAllFinancialPayoutByDepartment($date, $type, $department_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if (is_array($department_id)) {
            $in  = str_repeat('?,', count($department_id) - 1) . '?';
        }
        if (is_array($department_id)) {
            $query = "select * from financial_payout where date=STR_TO_DATE(?, '%m.%Y') and type_of_payment=? and department_id IN (".$in.")";
        } else {
            $query = "select * from financial_payout where date=STR_TO_DATE(?, '%m.%Y') and type_of_payment=? and department_id = ?";
        }
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            if (is_array($department_id)) {
                $param = array_merge([$date], [$type], $department_id);
                $sth->execute($param);
            } else {
                $sth->execute([$date, $type, $department_id]);
            }
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
            return $raw;
        }
    }

    public function getFinancialPayout($id, $date, $type)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from financial_payout where date=STR_TO_DATE(?, '%m.%Y') and member_id=? and type_of_payment=? LIMIT 1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date, $id, $type]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $raw = $sth->fetch(PDO::FETCH_ASSOC);
            return $raw;
        }
    }

    /*
     * Используется для отчета
     */
    public function getMemberFinancialPayout($member_id, $type, $dateS, $dateE)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if (!is_numeric($type)) {
            $type = 0;
        }

        switch ($type){
            case 0:
                $query = "select t1.*,DATE_FORMAT(t1.date,'%m.%Y') as date
                  from financial_payout as t1
                  where t1.member_id=? and date >= ? and date <= ? and active=1 order by date";
                break;
            case 1:
                $query = "select t1.*,DATE_FORMAT(t1.date,'%m.%Y') as date
                  from financial_payout as t1
                  where t1.member_id=? and (type_of_payment=? or type_of_payment=4) and date >= ? and date <= ? and active=1 order by date";
                break;
            case 2:
                $query = "select t1.*,DATE_FORMAT(t1.date,'%m.%Y') as date
                  from financial_payout as t1
                  where t1.member_id=? and type_of_payment=? and date >= ? and date <= ? and active=1 order by date";
                break;
        }

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            if ($type != 0) {
                $sth->execute([$member_id, $type, $dateS, $dateE]);
            } else {
                $sth->execute([$member_id, $dateS, $dateE]);
            }
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $raw = $sth->fetchAll();
            return $raw;
        }
    }

    /*
     * Финанслвая ведомость по всем пользователям за даты
     */
    public function getAllMemberFinancialPayout($type, $dateS, $dateE)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if (!is_numeric($type)) {
            $type = 0;
        }

        if ($type != 0) {
            $query = "select t1.*,DATE_FORMAT(t1.date,'%m.%Y') as date
                  from financial_payout as t1
                  where type_of_payment=? and date >= ? and date <= ? and active=1 order by date";
        } else {
            $query = "select t1.*,DATE_FORMAT(t1.date,'%m.%Y') as date
                  from financial_payout as t1
                  where  date >= ? and date <= ? and active=1 order by date";
        }

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            if ($type != 0) {
                $sth->execute([$type, $dateS, $dateE]);
            } else {
                $sth->execute([$dateS, $dateE]);
            }
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $raw = $sth->fetchAll();
            return $raw;
        }
    }


    /*
     * Финансовая ведомость по отделу за период
     */
    public function getDepartmentFinancialPayout($department_id, $type, $dateS, $dateE)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        if (!is_numeric($type)) {
            $type = 0;
        }

        if ($type != 0) {
            $query = "select t1.*,DATE_FORMAT(t1.date,'%m.%Y') as date
                  from financial_payout as t1
                  where t1.department_id=? and type_of_payment=? and date >= ? and date <= ? order by date";
        } else {
            $query = "select t1.*,DATE_FORMAT(t1.date,'%m.%Y') as date
                  from financial_payout as t1
                  where t1.department_id=? and date >= ? and date <= ? order by date";
        }
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            if ($type != 0) {
                $sth->execute([$department_id, $type, $dateS, $dateE]);
            } else {
                $sth->execute([$department_id, $dateS, $dateE]);
            }
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $raw = $sth->fetchAll();
            return $raw;
        }
    }

    /*
     * Расходы
     * type_of_payment=2
     */
    public function getPrepaidExpense($id, $date)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select AdministrativePremium,Hold,Total from financial_payout where date=STR_TO_DATE(?, '%m.%Y') and member_id=? and type_of_payment=2 and active=1 LIMIT 1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date, $id]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $raw = $sth->fetch(PDO::FETCH_ASSOC);
            return $raw;
        }
    }

    /*
     * type_of_payment == 3
     */
    public function getPrepaidExpense2($id, $date)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select AdministrativePremium,Hold,Total from financial_payout where date=STR_TO_DATE(?, '%m.%Y') and member_id=? and type_of_payment=3 LIMIT 1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date, $id]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $raw = $sth->fetch(PDO::FETCH_ASSOC);
            return $raw;
        }
    }

    // Проверяет была ли выплата
    public function checkPaid($id, $date, $type)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from financial_payout where date=STR_TO_DATE(?, '%m.%Y') and member_id=? and type_of_payment=? and active=1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date, $id, $type]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getFinancialPayoutByUser($id, $year)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $date = date_create_from_format('d-m-Y', '00-01-'.$year);
        $date_end = date_create_from_format('d-m-Y', '31-12-'.$year);

        $query = "select * from financial_payout where date>='".$date->format("Y-m-d")."' and date<='".$date_end->format("Y-m-d")."' and member_id=? and active=1 order by date";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
            return $raw;
        } else {
            return null;
        }
    }

    public function getFinanicalPayoutByJobPost($date, $type)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT t1.*,t2.direction_id,t2.department_id FROM financial_payout as t1
        LEFT JOIN member_jobpost as t2 ON (t1.member_id=t2.member_id)
        where t1.type_of_payment=? and t1.date=STR_TO_DATE(?,'%m.%Y') and t2.active=1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$type, $date]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
            return $raw;
        } else {
            return null;
        }
    }

    public function getUnshedulledAvans($member_id, $date) {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }
        $dt = DateTime::createFromFormat("m.Y", $date);

        $q = "select SUM(t3.Total) as summ
        FROM unscheduled as t1
        LEFT JOIN report_status as t2 ON (t1.id=t2.unscheduled_id and t2.active=1 and t1.member_id=t2.member_id)
        LEFT JOIN financial_payout as t3 ON (t1.id=t3.unscheduled_id and t3.active=1 and t1.member_id=t3.member_id)
        WHERE t1.member_id = ? AND 
            t1.date>STR_TO_DATE(?, '%Y-%m-%d') AND 
            t1.date<STR_TO_DATE(?, '%Y-%m-%d') AND 
            t1.avans=1 AND 
            t1.active=1 AND 
            t2.status_id=6 
        LIMIT 1" ;

        try {
            $sth = $this->dbh->prepare($q);
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("PDO Error" . $e->getMessage());
        }
        $sth->execute([$member_id, $dt->format("Y-m")."-01", $dt->format("Y-m")."-31"]);
        $r = $sth->fetch(PDO::FETCH_ASSOC);

        return $r;
    }
}
