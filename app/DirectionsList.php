<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.07.18
 * Time: 18:16
 */

require_once "Direction.php";
require_once "functions.php";

class DirectionsList
{
    private $dbh;
    private $directions = array();

    public function fetchDirections() {
        $this->dbh=dbConnect();
        $query = "select * from direction";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute();
        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $this->dbh = null;

        foreach ($rawresult as $value) {
            $Direction = new Direction();
            $Direction->setId($value['id']);
            $Direction->setName($value['name']);
            array_push($this->directions,$Direction);
        }
    }

    public function GetDirections() {
        return $this->directions;
    }


}