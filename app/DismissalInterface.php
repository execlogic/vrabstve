<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 22.08.18
 * Time: 18:11
 */

require_once 'functions.php';

class DismissalInterface
{
    private $dbh;
    private $date;

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    public function get($days) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select t1.*, DATE_FORMAT(t1.employment_date, '%d.%m.%Y') as employment_date_wp, 
	    DATE_FORMAT(t1.dismissal_date, '%d.%m.%Y') as dismissal_date,
        t2.name as reasons,
        t4.name as dir, t5.name as dep, t6.name as pos
        FROM member as t1 
        LEFT JOIN reasons_for_leaving as t2 ON (t1.reasons_for_leaving_id = t2.id)  
        LEFT JOIN member_jobpost as t3 ON (t3.member_id = t1.id and t3.active=1)
        LEFT JOIN direction as t4 ON (t3.direction_id = t4.id)
        LEFT JOIN department as t5 ON (t3.department_id = t5.id)
        LEFT JOIN position as t6 ON (t3.position_id = t6.id)
        WHERE dismissal_date BETWEEN (NOW() - INTERVAL ? DAY) AND NOW() order by t1.dismissal_date desc";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$days])) {
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);


        $q = 'SELECT t2.id, t2.lastname, t2.name, t2.middle, DATE_FORMAT(t2.employment_date, \'%d.%m.%Y\') as employment_date_wp, DATE_FORMAT(t1.date_s, \'%d.%m.%Y\') as dismissal_date, (select "Декрет") as reasons, t4.name as dir, t5.name as dep, t6.name as pos 
        FROM zp.member_workstatus as t1
            LEFT JOIN member as t2 ON (t1.member_id = t2.id)
            LEFT JOIN member_jobpost as t3 ON (t3.member_id = t1.member_id and t3.active=1)
            LEFT JOIN direction as t4 ON (t3.direction_id = t4.id)
            LEFT JOIN department as t5 ON (t3.department_id = t5.id)
            LEFT JOIN position as t6 ON (t3.position_id = t6.id)
        WHERE t1.status=3 and t1.active = 1 and t1.date_s BETWEEN (NOW() - INTERVAL ? DAY) AND NOW() order by dismissal_date desc';
        $sth = $this->dbh->prepare($q); // Подготавливаем запрос
        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$days])) {
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $r = $sth->fetchAll(PDO::FETCH_ASSOC);

        $raw = array_merge($raw, $r);

        return $raw;
    }

}