<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 31.07.18
 * Time: 13:09
 */

require_once 'Direction.php';
require_once 'functions.php';

class DirectionInterface
{
    private $directionList = array();
    private $dbh;

    public function fetchDirections() {
        $this->dbh=dbConnect();
        $query = "select * from direction order by id";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute();
        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $this->dbh = null;

        foreach ($rawresult as $value) {
            $direction = new direction();
            $direction->setId($value['id']);
            $direction->setName($value['name']);
            array_push($this->directionList, $direction);
        }
    }

    public function GetDirections() {
        if (empty($this->directionList)) {
            throw new Exception("Пустой список отделов");
        }
        return $this->directionList;
    }

    /**
     * @return bool
     * @throws Exception
     * Отображает Отделы
     */
    public function ShowdDrections() {
        if (empty($this->directionList)) {
            throw new Exception("Пустой список отделов");
            return false;
        }

        echo "<table class='table table-bordered'>";
        foreach ($this->directionList as $value ){
            echo "<tr>";
            echo "<td>".$value->getId()."</td><td>".$value->getName()."</td>";
            echo "</tr>";
        }
        echo "</table>";

        return true;
    }

    public function AddDirections($name) {
        $this->dbh=dbConnect();

        if (empty($this->dbh)) {
            echo "Error<br>";
        }

        $query = "INSERT INTO direction (name) VALUES (?)";
        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare'.$exception->getMessage());
        }

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$name])) {
            $error = $sth->errorInfo();
            throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: '.$error[1].'.<br> Текст ошибки: '.$error[2]."<BR>");
        }

        $sth = null;
        $this->dbh = null;

        return true;
    }

    public function findById($id) {
        if (count($this->directionList)==0) {
            $this->fetchDirections();
        }
        $member_direction = current(array_filter($this->directionList, function ($d) use ($id) {
            return $d->getId() == $id;
        }));

        if (isset($member_direction)) {
            return $member_direction;
        } else {
            $direction = new direction();
            $direction->setId($id);
            $direction->setName("None");
            return NULL;
        }
    }
}