<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 13.08.18
 * Time: 15:43
 */

require_once 'Motivation.php';
require_once 'functions.php';

class MotivationInterface {
    private $MotivationList = array();
    private $dbh;

    public function fetch() {
        $this->dbh=dbConnect();
        $query = "select * from motivation order by id";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute();
        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $this->dbh = null;

        foreach ($rawresult as $value) {
            $Motivation = new Motivation();
            $Motivation->setId($value['id']);
            $Motivation->setName($value['name']);
            $Motivation->setNote($value['note']);
            array_push($this->MotivationList,$Motivation);
        }
    }

    public function get() {
        if (empty($this->MotivationList)) {
            throw new Exception("Пустой список");
        }
        return $this->MotivationList;
    }


    public function add($name,$note) {
        $this->dbh=dbConnect();

        if (empty($this->dbh)) {
            echo "Error<br>";
        }

        if (empty($name)) {
            throw new Exception("Должно быть задано имя");
        }

        if (empty($note)) {
            $note = null;
        }

        $query = "INSERT INTO motivation (name,note) VALUES (?,?)";
        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare'.$exception->getMessage());
        }

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$name,$note])) {
            $error = $sth->errorInfo();
            throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: '.$error[1].'.<br> Текст ошибки: '.$error[2]."<BR>");
        }

        $sth = null;
        $this->dbh = null;

        return true;
    }

}