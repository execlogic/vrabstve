<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 22.08.18
 * Time: 13:52
 */

class ErrorInterface
{
    private $count  = 0; //Количество
    private $error = array(); //Список ошибок

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->error;
    }

    public function add($message)
    {
        array_push($this->error,$message);
        $this->count++;
    }
}