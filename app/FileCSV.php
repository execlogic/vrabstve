<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 18.07.18
 * Time: 13:51
 */

class FileCSV
{
    private $target_dir = "uploads/";
    private $target_file;
    private $uploadOk=true;
    private $FormName;
    private $FileType;
    private $maxfileSize = 15500000;
    private $Format = array("txt","csv");

    public function setFormName($name) {
        $this->FormName = $name;
    }

    public function Upload(){
        if (empty($this->FormName)) {
            throw new Exception("Нужно задать имя формы<br>");
        }

        $this->target_file = $this->target_dir . basename($_FILES[$this->FormName]["name"]);
        $this->FileType = strtolower(pathinfo($this->target_file,PATHINFO_EXTENSION));
        if (!$this->CheckExist()) {
            throw new Exception("Sorry, file already exists.");
        }

        if (!$this->Size()) {
            throw new Exception("Sorry, your file is too large.");
        }

        if (!$this->CheckFormat()) {
            throw new Exception("Sorry, only ".implode(' && ',$this->Format)." files are allowed.");
        }

        if ($this->uploadOk==false) {
            echo "Sorry, your file was not uploaded.";
        } else {
            if (!move_uploaded_file($_FILES[$this->FormName]["tmp_name"], $this->target_file)) {
                throw  new Exception("Sorry, there was an error uploading your file.");
            }
        }
    }

    private function Size() {
        if ($_FILES[$this->FormName]["size"] > $this->maxfileSize) {
            $this->uploadOk = false;
            return false;
        }
        return true;
    }

    private function CheckFormat() {
        if (!in_array($this->FileType,$this->Format)) {
            $this->uploadOk = false;
            return false;
        }

        return true;
    }

    private function CheckExist() {
        if (file_exists($this->target_file)) {
            $this->uploadOk=false;
            return false;
        }
        return true;
    }

    private function ShowInfo() {
        // Выводим информацию о загруженном файле:
        echo "<h3>Информация о загруженном на сервер файле: </h3>";
        echo "<p><b>Оригинальное имя загруженного файла: ".$_FILES[$this->FormName]['name']."</b></p>";
        echo "<p><b>Mime-тип загруженного файла: ".$_FILES[$this->FormName]['type']."</b></p>";
        echo "<p><b>Размер загруженного файла в байтах: ".$_FILES[$this->FormName]['size']."</b></p>";
        echo "<p><b>Временное имя файла: ".$_FILES[$this->FormName]['tmp_name']."</b></p>";
    }

    public function ReturnFileName() {
        if ($this->uploadOk == true && $this->target_file ) {
            return $this->target_file;
        }
        return false;
    }


    public function ParseCSV($delimiter=',') {
        if(!file_exists($this->target_file) || !is_readable($this->target_file))
            return false;

        $header = NULL;
        $data = array();
        if (($handle = fopen($this->target_file, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                array_push($data,$row);
            }
            fclose($handle);
        }
        return $data;
    }

    public function DeleteFile() {
        if(!file_exists($this->target_file) || !is_readable($this->target_file))
            return false;

        if (unlink($this->target_file)) {
            $this->target_file = null;
            $this->uploadOk = false;
            return true;
        } else {
            return false;
        }
    }

}