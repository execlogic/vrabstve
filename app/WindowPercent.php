<?php

require_once 'functions.php';

class WindowPercent
{
    private $table = 'window_manager_percent';
    private $dbh;

    public function get($user_id) {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $q = "select * from $this->table where manager_id=?";
        $sth = $this->dbh->prepare($q); // Подготавливаем запрос
        $sth->execute([$user_id]); // Выполняем запрос
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function add($array) {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $q = "INSERT INTO $this->table (".join(',',array_keys($array)).") VALUES (?, ?, ?, ?, ?)";
        echo "INSERT INTO $this->table (".join(',',array_keys($array)).") VALUES (".join(",",$array).")"."<BR>";

        $sth = $this->dbh->prepare($q); // Подготавливаем запрос
        $sth->execute(array_values($array)); // Выполняем запрос
    }

    public function delete($manager_id) {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $q = "DELETE FROM $this->table WHERE manager_id=?";
        $sth = $this->dbh->prepare($q); // Подготавливаем запрос
        $sth->execute([$manager_id]); // Выполняем запрос
    }
}