<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 20.02.19
 * Time: 12:34
 */

require_once "app/MemberInterface.php";

require_once "app/MemberFinanceBlock.php";
require_once 'app/DataItem.php';
require_once 'app/DataManager.php';

require_once 'app/AdministrativeBlock.php';
require_once 'app/Retention.php';
require_once 'app/Correction.php';

require_once 'app/ProductionCalendar.php';
require_once 'app/AwhInterface.php';
require_once 'app/PayRollInterface.php';

require_once 'app/PayHistory.php';
require_once 'app/Kpi.php';
require_once 'app/CustomPercent.php';

require_once 'app/TransportBlock.php';
require_once 'app/MobileBlock.php';

require_once 'app/WindowPercent.php';

class Premium
{
    /*
     * Дата
     */
    private $date;
    /*
     * По какому пользователю получаю премию
     */
    private $user_id;
    /*
     * мотивация пользователя
     */
    private $motivation;

    /*
     * Объект сотрудника
     */
    private $member = NULL;

    /*
     *  Вся наценка Коммерческий блок рубли
     */
    private $All_Markup_Rubles = 0;

    /*
     *  Итого по административному блоку
     */
    private $All_Administrative_Rubles = 0;
    /*
     *  Итого по удержаниям рублей
     */
    private $All_Hold_Rubles = 0;
    /*
    *  Корректировка в рублях
    */
    private $All_Correction_Rubles = 0;

    /*
     * Пропуски в руб.
     */
    private $All_Absence_Rub = 0;
    /*
     * Весь выплаченный аванс + аванс 2
     */
    private $All_Salary = 0;

    /*
     * Итого по Транспорту
     */
    private $All_Transport_Rubles = 0;

    /*
     * Итого по Связь
     */
    private $All_Mobile_Rubles = 0;

    /*
     * Вся сумма
     */
    private $fullsumm = 0;
    /*
     * Сумма наценки
     */
    private $summ_nachenka = 0;
    /*
     * Выдана премия или нет
     */
    private $payment_made = 0;

    /*
     * Выходные
     */
    private $Holidays = 0;
    /*
     * Больничные
     */
    private $Hospitals = 0;

    /*
     *  Массив для вывода данных коммерческой части
     */
    private $CommercialPart = array();

    private $CommercialPartManager = array();
    private $CommercialPartTT = array();
    private $CommercialPartStatGroup = array();
    private $CommercialPartService = array();

    /*
     * НДФЛ + ИЛ
     */
    private $ndfl = 0;
    private $ro = 0;


    /*
     * **************   Основные объекты
     */

    /*
    * Коммеческий блок
    */
    private $MFB = NULL;
    /*
     * Объект процентов и расширенных процентов
     */
    private $CPT = NULL;

    /*
     * Административный блок
     */
    private $AB= null;

    /*
     * Удержания
     */
    private $RN = null;
    /*
     * Корректировки
     */
    private $CR = null;

    /*
     * Транспорт
     */
    private $TR = null;

    /*
     * Связь
     */
    private $MB = null;

    /*
     * УРВ
     */

    /*
     * Объект УРВ
     */
    private $AWH=null;

    /*
     * Отсутсвтия
     */
    private $awh_new_absence = null;
    private $awh_absence = null;
    /*
     * Отпуск
     */
    private $awh_holiday = null;
    /*
     * Болень
     */
    private $awh_disease = null;
    /*
     * Заметка
     */
    private $awh_note = null;
    private $awh_extraholiday = null;

    /*
     * История изменений
     */
    private $PH = null;

    /*
     * Данные по истории административного блока
     */
    private $AdministrativeBlock_Array = array();
    /*
     * Удержания
     */
    private $RetitiotionBlock_Array = array();
    /*
     * Корректировки
     */
    private $CorrectingBlock_Array = array();

    /*
     * Вычисляем конверты и хвостики
     */
    private $konvert = false;

    /*
     * Отчисления в годовой бонус
     */
    private $year_bonuspay = 0;
    /*
     * Годовой бонус
     */
    private $year_bonus = 0;

    /*
     * kpi
     */
    private $KPI = NULL; // объекто KPI
    private $kpi_summ = 0;

    /*
     * Lock в зависимости от статуса ведомости
     */
    private $Administrative_lock = 0;
    private $Retention_lock = 0;
    private $Correcting_lock = 0;
    private $Commercial_lock = 0;
    private $AWH_lock = 0;
    private $kpi_lock = 0;
    private $working_days;

    private $older_format;

    private $status;


    # Проценты по окнам
    private $WP = null;
    private $wpList = null;

    private $date_now;

    public function __construct() {
        $this->date_now = new DateTime();
    }

    /*
     * Функция извлечения всех данных для расчета
     * Необходим member и user_id
     */
    public function fetch($user_id, $payment_made=null) {
        /*
         * **************    Обнуляю переменные
         */

        $this->CommercialPart = array();

        $this->CommercialPartManager = array();
        $this->CommercialPartTT = array();
        $this->CommercialPartStatGroup = array();
        $this->CommercialPartService = array();

        $this->fullsumm = 0;
        $this->summ_nachenka = 0;
        $this->ndfl = 0;
        $this->ro = 0;
        $this->awh_holiday = 0;
        $this->awh_disease = 0;
        $this->awh_absence = 0;
        $this->awh_new_absence = 0;
        $this->All_Absence_Rub = 0;
        $this->All_Salary = 0;
        $this->kpi_summ = 0;

        /*
        * Вся наценка Коммерческий блок рубли
        */
        $this->All_Markup_Rubles = 0;
        /*
         *  Итого по административному блоку
         */
        $this->All_Administrative_Rubles = 0;
        /*
         *  Итого по удержаниям рублей
         */
        $this->All_Hold_Rubles = 0;
        /*
         *  Корректировка в рублях
         */
        $this->All_Correction_Rubles = 0;


        /*
         * Присваиваю id
         */
        $this->user_id = $user_id;


        // Создаем объект MemberInterface
        $MI = new MemberInterface();

        if (is_null($this->member)) {
            /*
             * Получаю основные данные по пользователю
             */
            $this->member = $MI->fetchMemberByID($this->user_id);
            if (is_null($this->member))
                return false;
        }

        /*
         * Получаю мотивацию в переменую (зачем?)
         */
//        $this->motivation = $MI->getMotivationByDate($this->user_id, $this->date);
        $this->motivation = $MI->getMotivation($this->user_id);

        /*
         * Если у пользователя не выбрана мотивация, то считаем что мотивация произвольная
         */
        if ($this->member->getMotivation() == null) {
            $this->member->setMotivation(6);
        }

        /*
         * Создаю и инициализирую основные объекты необходимые для расчета премии
         * ******************************* ИНИЦИАЛИЗАЦИЯ ОСНОВНЫХ ОБЪЕКТОВ (по табам)
         */

        /*
         * Таб Административный объект
         */
        $this->AB = new AdministrativeBlock();
        /*
         * Тип выплат премия
         */
        $this->AB->setTypeOfPayment(1);

        /*
         * Таб Удержания
         */
        $this->RN = new Retention();
        /*
         * Тип выплат премия
         */
        $this->RN->setTypeOfPayment(1);

        /*
         * Таб Корректировки
         */
        $this->CR = new Correction();
        /*
         * Тип выплат премия
         */
        $this->CR->setTypeOfPayment(1);

        /*
         * Таб KPI
         */
        $this->KPI = new Kpi();

        /*
         * Получаю KPI и обрабатываю
         */
        $this->KPI->fetchPayment2($this->user_id, $this->date, 1);
        foreach ($this->KPI->get() as $item) {
            $this->kpi_summ += $item->getSumm();
        }

        /*
         * Транспорт
         */
        $this->TR = new TransportBlock();
        $this->TR->getTransport($this->date, 1, $this->user_id);
        $this->All_Transport_Rubles = $this->TR->getSumm();

        /*
         * Связь
         */
        $this->MB = new MobileBlock();
        $this->MB->getMobile($this->date, 1, $this->user_id);
        $this->All_Mobile_Rubles = $this->MB->getSumm();

        /*
         * Создаю объект ведомости
         */
        $PI = new PayRollInterface();
        /*
         * Выдана ли премия или нет
         */

        $RS = new ReportStatus();

        /*
         * Получаю статуст ведомости
         * Если рабочий статус != 1, Т.е. не работает, а увольняется или уволен, то делаю расчет.
         */
        if ($this->member->getWorkStatus() != 1) {
            $Status = $RS->getStatusMemberLeaving($this->date, $this->member->GetId());
            if ($Status)
                $Status = $Status['status_id'];
        } else {
            $Status = $RS->getFiltredStatus($this->date, 1, $this->member->getDepartment()['id'])['status_id'];
        }

        $this->status = $Status;

        /*
        *  Получаю данные по административному блоку
        */
        $this->AB->fetch($this->user_id, $this->date);
        /*
         *  Получаю данные по удержаниям
         */
        $this->RN->fetch($this->user_id, $this->date);
        /*
         *  Получаю данные по корректировкам
         */
        $this->CR->fetch($this->user_id, $this->date);

        /*
         * Финансовый блок
         */
        $this->MFB = new MemberFinanceBlock($this->member);

        /*
         * Расширенные проценты
        */
        $this->CPT = new CustomPercent();

        /*
         *  Создаю объект производственный календарь
        */
        $PC = new ProductionCalendar();
        /*
         *  Производственный календарь. Извлекаю данные из БД
        */
//            $PC->fetchByDate($this->date);
        $PC->fetchByDateWithDepartment($this->date, $this->member->getDepartment()['id']);

        /*
         * Получаю количество рабочих дней
         */
        $this->working_days = $PC->get()["working_days"];

        /*
         * Закрываю ведомость от всех при статусе больше 4
         */
        if ($Status >= 4) {
            $this->payment_made = true;
            $this->Administrative_lock=1;
            $this->Retention_lock=1;
            $this->Correcting_lock=1;
            $this->AWH_lock=1;
            $this->Commercial_lock = 1;
            $this->kpi_lock = 1;

        }

        /*
         * Если статус ведомости > 1, то ведомость записана в FPO
         * Поэтому данные по ком. блоку будем брать оттуда
         */
        if (($Status > 1 && $this->member->getWorkStatus()==1) || ($Status > 2 && $this->member->getWorkStatus() != 1)) {
            /*
             * Получаю данные по коммерческому блоку из ведомости
             */
            if ($this->member->getWorkStatus() == 4) {
                $raw = $PI->getFinancialPayout($this->user_id, $this->date, 4);
                /*
                * Преобразую из json в ассоциативный массив
                */
                $mfb = json_decode($raw['data'], true);
                /*
                * Добавляю данные в финансы для обработки
                 */
//                echo $this->date."<BR>"; exit(0);
                $this->MFB->setDataFromJson($mfb, $raw['motivation'], $this->date);

            } else {
                $test_date = DateTime::createFromFormat("d.m.Y", "01.".$this->date);

                if ($test_date->format("Y-m-d")<'2021-08-01') {
                    /*
                    * Получаю массив объектов с даннными по заданной дате
                    */
                    $raw = $PI->getFinancialPayout($this->user_id, $this->date, 1);
                    $this->MFB->fetchWithDateObj($this->date); // 0 при мотивации <=0 и >=6

                } else {
                    $raw = $PI->getFinancialPayout($this->user_id, $this->date, 1);
                    /*
                    * Преобразую из json в ассоциативный массив
                    */
                    $mfb = json_decode($raw['data'], true);
                    /*
                    * Добавляю данные в финансы для обработки
                     */
                    $this->MFB->setDataFromJson($mfb, $raw['motivation'], $this->date);
                }
            }

            /*
             * Если есть мотивация, то выставляю ту что была на момент проведения ведомости
             */
            $this->motivation = $raw['motivation'];

            /*
             *  Выставляю мотивацию у объекта
             */
            $this->CPT->setMotivation($this->motivation);

            $this->CPT->fetchBasePercentbyId($raw['member_percent_id']);

            if (is_null($this->CPT->getBasePercent()))
                $this->CPT->fetchBasePercentByDate($this->member->getId(), $this->date);
                //$this->CPT->fetchBasePercent($this->member->getId());

            $this->CPT->fetchById($raw['member_advanced_percent_id']);

            if (empty($this->CPT->get())) {
                try {
                    $this->CPT->fetchv3byDate($this->member->GetId(), $this->date);
                } catch (Exception $e) {
                    echo "member_id: ". $this->member->GetId() . " ". $e->getMessage()."<BR>";
                }
            }

            if (is_null($this->CPT->getBrandPrercent()) && $this->member->getMotivation()==10)
                $this->CPT->fetchBrendPercent($this->member->getId());

            /*
             * Если сделали новую запись по окладу
             * Нужно проверять в премии и в фин ведомости
             */
            $this->member->setSalary($raw['Salary']);

            /*
             * Устанавливаю NDFL
             */
            $this->ndfl = $raw['ndfl'];
            /*
             * Устанавливаю ИЛ
             */
            $this->ro = $raw['ro'];

            /*
            *  Создаю объект УРВ
            */
            $this->AWH = new AwhInterface();
            $this->setAwhExtraholiday($this->AWH->getExtraHoliday($this->date, $this->user_id));

            $member_awh = $this->AWH->getAwhByMember($this->date, 1, $this->user_id); //Премия
            $this->awh_new_absence = $member_awh['new_absence'];
            $this->awh_absence = $member_awh['absence'];
            $this->awh_holiday = $member_awh['holiday'];
            $this->awh_disease = $member_awh['disease'];
            $this->awh_note = $member_awh['note'];

            $this->All_Absence_Rub = $raw['absence'];

            $this->All_Salary = $raw['PrepaidExpense'];

            /*
            * Расчет выходных и больничных в зависимости от
            */
            if ($this->member->getWorkstatus() != 1 && $this->working_days != 0 && !is_null($this->working_days)) {
                $this->Holidays = (($this->member->getSalary() / 21) * $this->CR->getHoliday());
                /*
                 * В любом случае корректировка больничных это отрицательное значение
                 * и пофиг на всё!
                 */
                if ($this->CR->getHospital()<0) {
                    $this->Hospitals = (($this->member->getSalary() / 21) * $this->CR->getHospital());
                } else {
                    $this->Hospitals = (($this->member->getSalary() / 21) * ($this->CR->getHospital()*-1));
                }
            }
            
            /*
             * Административный блок
             */

            /*
             * Если статус ведомости больше 3, то берем данные из FIX
             */

            if (($this->member->getWorkStatus()==1 && $Status > 3) || ($this->member->getWorkStatus() != 1 && $Status > 3) ) {
                $this->All_Administrative_Rubles = $raw['AdministrativePremium'];
            } else {
                $this->All_Administrative_Rubles = $this->AB->getSumm();
            }

            /*
             * Удержания
             */
            /*
             * Если статус ведомости больше 3, то берем данные из FIX
             */
            if (($this->member->getWorkStatus()==1 && $Status > 3) || ($this->member->getWorkStatus()!=1 && $Status > 3) ) {
                $this->All_Hold_Rubles = $raw['Hold'];
            } else {
                $this->All_Hold_Rubles = $this->RN->getSumm();
            }

            /*
             * Корректировки
             */

            /*
             * Если статус ведомости больше 3, то берем данные из FIX
             */
            if (($this->member->getWorkStatus()==1 && $Status > 3) || ($this->member->getWorkStatus()!=1 && $Status > 3) ) {
                $this->All_Correction_Rubles = $raw['Correcting'];
            } else {
                $this->All_Correction_Rubles = $this->CR->getSumm();
                /*
                * Если сотрудник на увольнение, то расчитываю корректировку по отпускам и больничным
                */
                if ($this->member->getWorkstatus() != 1) {
                    $this->All_Correction_Rubles += $this->Holidays + $this->Hospitals;
                }
            }
            /*
             * Устанавливаю значение из годового бонуса
             */
            $this->member->setYearbonusPay($raw['YearBonusPay']);
            $this->year_bonuspay = $raw['YearBonusPay'];

            /*
             * Разбираю коммерческий блок
             */
            switch ($this->motivation) {
                case 1:
                case 8:
                    foreach ($this->MFB->getDataManager() as $item) {
                        $this->Data_Analysis($this->motivation, $item);
                    }
                    break;
                case 2:
                case 3:
                    foreach ($this->MFB->getDataTT() as $item) {
                        $this->Data_Analysis($this->motivation, $item);
                    }
                    break;
                case 4:
                    foreach ($this->MFB->getDataStatGroup() as $item) {
                        $this->Data_Analysis($this->motivation, $item);
                    }
                    break;
                case 5:
                    foreach ($this->MFB->getDataService() as $item) {
                        $this->Data_Analysis($this->motivation, $item);
                    }
                    break;
                case 7:
                    if ($this->date_now->format("Y-m-d") >= '2023-04-24') {
                        foreach ($this->MFB->getDataService2() as $item) {
                            $this->Data_Analysis($this->motivation, $item);
                        }
                    }  else {
                        foreach ($this->MFB->getDataService() as $item) {
                            $this->Data_Analysis($this->motivation, $item);
                        }
                    }
                case 9:
                    foreach ($this->MFB->getDataEstimator() as $item) {
                        $this->Data_Analysis($this->motivation, $item);
                    }
                    break;
                case 10:
                    foreach ($this->MFB->getDataStatGroup() as $item) {
                        $this->Data_Analysis(4, $item);
                    }
                    if ($this->date_now->format("Y-m-d") >= '2023-04-24') {
                        foreach ($this->MFB->getDataService2() as $item) {
                            $this->Data_Analysis(5, $item);
                        }
                    } else {
                        foreach ($this->MFB->getDataService() as $item) {
                            $this->Data_Analysis(5, $item);
                        }
                    }
                    break;
                default:
                    break;
            }
        } else {
            /*
             * Получаю массив объектов с даннными по заданной дате
             */
            $this->MFB->fetchWithDateObj($this->date); // 0 при мотивации <=0 и >=6
            /*
             *  Извлекаю НДФЛ за дату по пользователю переданному как объект
             */
            $this->MFB->fetch_NdflByDate($this->date);

            /*
             *  получаю НДФЛ в массив
             */
            $member_ndfl = $this->MFB->getNdfl();
            if (isset($member_ndfl['ndfl'])) {
                $this->ndfl = $member_ndfl['ndfl'];
            } else {
                $this->ndfl = 0;
            }

            /*
             * Получаю ИЛ
             */
            if (isset($member_ndfl['ro'])) {
                $this->ro = $member_ndfl['ro'];
            } else {
                $this->ro = 0;
            }


            /*
             * Расширенные проценты
             */

            /*
             *  Выставляю мотивацию у объекта
             */
            $this->CPT->setMotivation($this->member->getMotivation());
            /*
             *  Извлекаю базовый процент по пользователь, за дату.
             */
            $this->CPT->fetchBasePercent($this->member->getId());
            $this->CPT->fetchBrendPercent($this->member->getId());
            /*
             *  Получаю ассоциативный массив с данными.
             */
            $this->CPT->fetchv3($this->member->getId());

            /*
             *  Если null значение административной премии и есть в профиле, то выставляю из профиля
             */
            if (is_null($this->AB->getAdministrativePrize())) {
                $this->AB->setAdministrativePrizeFin($this->user_id);
            }

            /*
             *  Если null значение Мобильной связи и есть в профиле, то выставляю из профиля
             */
            if (is_null($this->MB->getSumm())) {
                $this->MB->setSumm($this->member->getMobileCommunication());
            }

            if (is_null($this->TR->getSumm())) {
                $this->TR->setSumm($this->member->getTransport());
            }



            /*
             *  Создаю объект УРВ
             */
            $this->AWH = new AwhInterface();

            /*
             * Устанавливаю данные по 'Дополнительный отпуск'
             */
            $this->setAwhExtraholiday($this->AWH->getExtraHoliday($this->date, $this->user_id));

            /*
             *  Получаю данные по УРВ
            */
            $member_awh = $this->AWH->getAwhByMember($this->date, 1, $this->user_id); //Премия
            if (isset($member_awh['new_absence'])) {
                $this->awh_new_absence = $member_awh['new_absence'];
            } else {
                $this->awh_new_absence = 0;
            }
            if (isset($member_awh['absence'])) {
                $this->awh_absence = $member_awh['absence'];
            } else {
                $this->awh_absence = 0;
            }
            if (isset($member_awh['holiday'])) {
                $this->awh_holiday = $member_awh['holiday'];
            } else {
                $this->awh_holiday = 0;
            }
            if (isset($member_awh['disease'])) {
                $this->awh_disease = $member_awh['disease'];
            } else {
                $this->awh_disease = 0;
            }
            if (isset($member_awh['note'])) {
                $this->awh_note = $member_awh['note'];
            } else {
                $this->awh_note = '';
            }

            /*
            * Разбираю дату приема на работу
            */
            if ($this->member->getEmploymentDate()) {
                if (strpos($this->member->getEmploymentDate(),"-") != false) {
                    $emp_date_arr = explode("-", $this->member->getEmploymentDate());
                } else {
                    $emp_date_arr = explode(".", $this->member->getEmploymentDate());
                }
            }

            if (($emp_date_arr[1] . "." . $emp_date_arr[2] == $this->date && $emp_date_arr[0] != 1) && $this->awh_new_absence == 0) {
                $PC->fetchWorkingArrayWithDepartment($this->date, $this->member->getDepartment()['id']);
                $working_days_array = $PC->getWDArray();

                foreach ($working_days_array as $wp) {
                    if ($emp_date_arr[0] != $wp ){
                        $this->awh_new_absence += 1;
                    } else {
                        break;
                    }
                }

                $this->AWH->setNewAbsenceByMember($this->user_id, $this->date,1, $this->awh_new_absence);
            }


            /*
             *  Если пустое значение, то выставляю в количество рабочих дней, т.е. не работал
             */
//            if (is_null($this->awh_absence)) {
////                $this->awh_absence = $PC->get()["working_days"];
//                $this->awh_absence = 0;
//            }
            if (is_null($this->awh_holiday)) {
                $this->awh_holiday = 0;
            }
            if (is_null($this->awh_disease)) {
                $this->awh_disease = 0;
            }

            /*
             * Считаю все отсутствия
             */
            $vse_otsutstviya = $this->awh_absence + $this->awh_new_absence + $this->awh_holiday + $this->awh_disease + $this->awh_extraholiday;


            /*
             * Расчет транспортного блока в зависимости от условий и УРВ
             */
            if ($this->TR->getSumm() <= 0  && $this->member->getTransport()>0) {
                if ($this->member->getTransportFormula() == 1) {
                    $this->TR->setSumm($this->member->getTransport());
                    $this->TR->recountCompensationFAL($vse_otsutstviya, $this->working_days);
                    $this->All_Transport_Rubles = $this->TR->getSumm();
                } else {
                    $this->TR->setTransport($this->date, 1, $this->user_id,$this->member->getTransport(), "", -1);
                    $this->All_Transport_Rubles = $this->TR->getSumm();
                }
            } else {
                if ($this->member->getTransportFormula() == 1) {
                    if ($this->TR->isAuto($this->date, 1, $this->user_id) == true) {
                        $this->TR->setSumm($this->member->getTransport());
                        $this->TR->recountCompensationFAL($vse_otsutstviya, $this->working_days);
                    }
                }
            }

            /*
             * Расчет блока мобильной связи в зависимости от условий и УРВ
             */
            if ($this->MB->getSumm() <= 0 && $this->member->getMobileCommunication()>0 ) {
                if ($this->member->getMobileFormula() == 1) {
                    $this->MB->setSumm($this->member->getMobileCommunication());
                    $this->MB->recountMobile($vse_otsutstviya, $this->working_days);
                    $this->All_Mobile_Rubles = $this->MB->getSumm();
                } else {
                    $this->MB->setMobile($this->date, 1, $this->user_id, $this->member->getMobileCommunication(), "", -1);
                    $this->All_Mobile_Rubles = $this->MB->getSumm();
                }
            } else {
                if ($this->member->getMobileFormula() == 1) {
                    if ($this->MB->isAuto($this->date, 1, $this->user_id) == true) {
                        $this->MB->setSumm($this->member->getMobileCommunication());
                        $this->MB->recountMobile($vse_otsutstviya, $this->working_days);
                    }
                }
            }

            /*
             *  Если мы получили количество рабочих дней в этом месяце, то расчитываем пропуски
             */
            if ( !empty($this->working_days) && (!is_null($this->awh_absence) || !is_null($this->awh_new_absence)) ) {
                /*
                 *  Расчитываю сумму отсутствия в рублях по формуле ( КОЛИЧЕСТВО_ПРОПУСКОВ * (ОКЛАД / КОЛИЧЕСТВО_РАБОЧИХ_ДНЕЙ))
                 */
                $this->All_Absence_Rub = ($this->awh_absence + $this->awh_new_absence) * ($this->member->getSalary() / $this->working_days); // пропуск * За один день.
            } else {
                $this->All_Absence_Rub = 0;
            }

            /*
             * Если это премия или расчет! мы должны получить данные об авансе
             * ПОЛУЧАЮ ДАННЫЕ ПО АВАНСУ И АВАНС 2
             */
            $PrepaidExpenseArray = $PI->getPrepaidExpense($this->user_id, $this->date);
            $PrepaidExpenseArray2 = $PI->getPrepaidExpense2($this->user_id, $this->date);
            $PrepaidExpenseArray3 = $PI->getUnshedulledAvans($this->user_id, $this->date);

            if (!isset($PrepaidExpenseArray['Total'])) {
                $PrepaidExpenseArray['Total'] = 0;
            }

            /*
             *  Выплаченный аванс
             */
            if (isset($PrepaidExpenseArray2)) {
                $this->All_Salary = $PrepaidExpenseArray['Total'] + $PrepaidExpenseArray2['Total']; // Выплаченный аванс + аванс 2
            } else {
                $this->All_Salary = $PrepaidExpenseArray['Total']; // Выплаченный аванс
            }

            /*
             * Внеплановая выплата за счет аванса
             */
            $this->CR->setBonusAdjustment($this->CR->getBonusAdjustment()-$PrepaidExpenseArray3['summ']);

            /*
             *  Если пустота, то ставлю 0
             */
            if (empty($this->All_Salary)) {
                $this->All_Salary = 0;
            }

            /*
             * Расчет выходных и больничных в зависимости от
             */
            if ($this->working_days != 0 && !is_null($this->working_days)) {
                if ($this->member->getWorkstatus() >= 2) {
                    $this->Holidays = (($this->member->getSalary() / 21) * $this->CR->getHoliday());
                    /*
                    * В любом случае корректировка больничных это отрицательное значение
                    * и пофиг на всё!
                    */
                    if ($this->CR->getHospital()<0) {
                        $this->Hospitals = (($this->member->getSalary() / 21) * $this->CR->getHospital());
                    } else {
                        $this->Hospitals = (($this->member->getSalary() / 21) * ($this->CR->getHospital()*-1));
                    }
                }
            } else {
                $this->Holidays = 0;
                $this->Hospitals = 0;
                throw new Exception("Производственный календарь, на текущий месяц, не заполнен.");
            }

            /*
             *  Извлекаю данные и заношу в переменные, наценка будет расчитана ниже
             */

            /*
             * Административный блок
             */
            $this->All_Administrative_Rubles = $this->AB->getSumm();

            /*
             * Удержания
             */
            $this->All_Hold_Rubles = $this->RN->getSumm();
            /*
             * Корректировки
             */
            $this->All_Correction_Rubles = $this->CR->getSumm();

            /*
             * Если сотрудник на увольнение, то расчитываю корректировку по отпускам и больничным
             */
            if ($this->member->getWorkstatus() >= 2) {
                $this->All_Correction_Rubles += $this->Holidays + $this->Hospitals;
            }

            $this->WP = new WindowPercent();
            $this->wpList = $this->WP->get($this->user_id);

            /*
             * Разбираю коммерческий блок
             */
            switch ($this->motivation) {
                case 1:
                case 8:
                    foreach ($this->MFB->getDataManager() as $item) {
                        $this->Data_Analysis($this->motivation, $item);
                    }
                    break;
                case 2:
                case 3:
                    foreach ($this->MFB->getDataTT() as $item) {
                        $this->Data_Analysis($this->motivation, $item);
                    }
                    break;
                case 4:
                    foreach ($this->MFB->getDataStatGroup() as $item) {
                        $this->Data_Analysis($this->motivation, $item);
                    }
                    break;
                case 5:
                    foreach ($this->MFB->getDataService() as $item) {
                        $this->Data_Analysis($this->motivation, $item);
                    }
                    break;
                case 7:
                    if ($this->date_now->format("Y-m-d") >= '2023-04-24') {
                        foreach ($this->MFB->getDataService2() as $item) {
                            $this->Data_Analysis($this->motivation, $item);
                        }
                    }  else {
                        foreach ($this->MFB->getDataService() as $item) {
                            $this->Data_Analysis($this->motivation, $item);
                        }
                    }
                    break;
                case 9:
                    foreach ($this->MFB->getDataEstimator() as $item) {
                        $this->Data_Analysis($this->motivation, $item);
                    }
                    break;
                case 10:
                    foreach ($this->MFB->getDataStatGroup() as $item) {
                        $this->Data_Analysis(4, $item);
                    }

                    if ($this->date_now->format("Y-m-d") >= '2023-04-24') {
                        foreach ($this->MFB->getDataService2() as $item) {
                            $this->Data_Analysis(5, $item);
                        }
                    } else {
                        foreach ($this->MFB->getDataService() as $item) {
                            $this->Data_Analysis(5, $item);
                        }
                    }

                    break;
                default:
                    break;
            }

            /*
             * Отчисления в годовой бонус
             */

//            $Raw = $MI->getYearBonusPayByDate($this->member->getId(), $this->date);
            $Raw = $MI->getYearBonusPay($this->member->getId());
            if (isset($Raw['type'])) {
                $this->member->setYearbonusType($Raw['type']);
            } else {
                $this->member->setYearbonusType(0);
            }

            if (isset($Raw['summ'])) {
                $this->member->setYearbonusPay($Raw['summ']);
            } else {
                $this->member->setYearbonusPay(0);
            }
            if ($this->member->getWorkstatus() == 1 ) {
                switch ($this->member->getYearbonusType()) {
                    case 1:
                        $this->year_bonuspay = $this->member->getYearbonusPay();
                        break;
                    case 2:
                        $tmp = explode(".", $this->date);
                        if ($tmp[0] >= 4 && $tmp[0] <= 11) {
                            $this->year_bonuspay = ($this->member->getSalary() + $this->All_Markup_Rubles + $this->All_Administrative_Rubles) * $this->member->getYearbonusPay();
                        }
                        break;
                }
            }
        }
    }

    private function Data_Analysis($motivation, $item) {
        /*
        *  Если установлен базовый процент, то заношу в переменную данные в переменную иначе 0
        */
        if ($this->CPT->getBasePercent() || $this->member->getMotivation()!=10) { //
            if ($item->getBaseProcent() != NULL) {
                $this->member_custom_procent_value = $item->getBaseProcent();
            } else {
                $this->member_custom_procent_value = $this->CPT->getBasePercent();
            }
        } else {
            $this->member_custom_procent_value = 0;
        }
        $is_this_window = 0;
        if ($this->wpList && $item->getServiceId()) {
            foreach ($this->wpList as $value) {
                if ($item->getServiceId() == $value['window_id']) {
                    $this->member_custom_procent_value = $value['percent'];
                    $is_this_window = 1;
                }
            }
        }

        /*
         *  Если мотивация 1 или 4, то разбор идет по статгруппам
         *  Если мотивация 2, то разбор идет по ТТ
         */
        if (($motivation == 1) || ($motivation == 4) || ($motivation == 5) || ($motivation == 7) || ($motivation==8) || ($motivation==9)) {
            if (isset($this->CPT->get()[$item->getStatgroupId()])) { // Если был выставлен процент по этой статгруппе,
                $this->member_custom_procent_value = $this->CPT->get()[$item->getStatgroupId()]; // То записываю его в переменную
            }
        } else if (($motivation == 2)) {
            if (isset($this->CPT->get()[$item->getDepartmentId()]) && !$is_this_window) { // Если был выставлен процент по этой ТТ,
                $this->member_custom_procent_value = $this->CPT->get()[$item->getDepartmentId()]; // То записываю его в переменную
            }
        }

        /*
         *  Если в итоге процент == 0, то сумма везде будет == 0 и не показываем эти строки
         */
        /*
         * Показываю нулевые строчки для наценки
         */
//        if ($this->member_custom_procent_value == 0) {
//            return;
//        }

        /*
         *  Получаю значение по бонусу
         */
        $bonus = $item->getBonus();
        /*
         *  Получаю значение по корректировкам
         */
        $correctirovka = $item->getCorrect();

        /*
         *  Делаю расчет в зависимости от мотивации
         */
        if (($this->member->getMotivation() == 3) || ($this->member->getMotivation() == 2)) {
            // (НАЦЕНКА + БОНУС + КОРРЕКТИРОВКА) * Коммерческий_Коэффициент * Установленный_Процент / 100
            $summ = (($item->getProfit()) - $bonus + $correctirovka) * $this->MFB->getCommercialProcent() * $this->member_custom_procent_value / 100;
        } else if ($this->member->getMotivation() == 10) {
            if ($motivation == 4 && $this->member->getMotivation()==10) {
                $this->member_custom_procent_value = $this->CPT->getBrandPrercent();
            }
            $summ = (($item->getProfit()) - $bonus + $correctirovka) * $this->member_custom_procent_value / 100;
        } else {
            // (НАЦЕНКА + БОНУС + КОРРЕКТИРОВКА) * Установленный_Процент / 100
            $summ = (($item->getProfit()) - $bonus + $correctirovka) * $this->member_custom_procent_value / 100;
        }
        /*
         *  Инкрементируем fullsumm на проученную summ;
         */

        $this->fullsumm += $summ;

        /*
         *  Инкрементируем $summ_nachenka на сумму наценки
         */
        $this->summ_nachenka += $item->getProfit();

        /*
         *  Получаю всю сумму в рублях.
         */
        if ($this->older_format)
            $this->All_Markup_Rubles = $this->fullsumm * 25;
        else
            $this->All_Markup_Rubles = $this->fullsumm;

        /*
         *  *************************************************** Строю массив данных
         */

        /*
         * В зависимости от мотивации вывожу значения строк
         * 7-ую мотивацию буду обрабатывать как мотивация 1 + 5
         */
        switch ($motivation) {
            case 1:
            case 8:
            case 9:
                if ($this->older_format)
                    $this->CommercialPartManager[] = [
                        'id' => $item->getId(),
                        'statgroup' => $item->getStatgroupName(),
                        'statgroup_id' => $item->getStatgroupId(),
                        'department' => $item->getDepartmentId(),
                        'department_name' => $item->getDepartmentName(),
                        'profit' => $item->getProfit(),
                        'bonus' => $item->getBonus(),
                        'bonus_note' => $item->getBonusNote(),
                        'correct' => $item->getCorrect(),
                        'correct_note' => $item->getCorrectNote(),
                        'preliminary_sum' => ($item->getProfit() - $bonus + $correctirovka),
                        'procent' => $this->member_custom_procent_value,
                        'sum' => $summ];
                else
                    $this->CommercialPartManager[] = [
                        'department' => $item->getDepartmentId(),
                        'date' => $item->getDate(),
                        'department_name' => $item->getDepartmentName(),
                        'profit' => $item->getProfit(),
                        'bonus' => $item->getBonus(),
                        'bonus_note' => $item->getBonusNote(),
                        'correct' => $item->getCorrect(),
                        'correct_note' => $item->getCorrectNote(),
                        'preliminary_sum' => ($item->getProfit() - $bonus + $correctirovka),
                        'procent' => $this->member_custom_procent_value,
                        'service_id' => $item->getServiceId(),
                        'sum' => $summ
                    ];
                break;
            case 2:
            case 3:
                if ($this->older_format)
                    $this->CommercialPartTT[] = [
                        'id' => $item->getId(),
                        'department_name' => $item->getDepartmentName(),
                        'profit' => $item->getProfit(),
                        'bonus' => $item->getBonus(),
                        'bonus_note' => $item->getBonusNote(),
                        'correct' => $item->getCorrect(),
                        'correct_note' => $item->getCorrectNote(),
                        'preliminary_sum' => ($item->getProfit() - $bonus + $correctirovka),
                        'procent' => $this->member_custom_procent_value,
                        'sum' => $summ
                    ];
                else
                    $this->CommercialPartTT[] = [
                        'department' => $item->getDepartmentId(),
                        'department_name' => $item->getDepartmentName(),
                        'profit' => $item->getProfit(),
                        'bonus' => $item->getBonus(),
                        'bonus_note' => $item->getBonusNote(),
                        'correct' => $item->getCorrect(),
                        'correct_note' => $item->getCorrectNote(),
                        'preliminary_sum' => ($item->getProfit() - $bonus + $correctirovka),
                        'procent' => $this->member_custom_procent_value,
                        'service_id' => $item->getServiceId(),
                        'sum' => $summ
                    ];
                break;
            case 4:
                if ($this->older_format)
                    $this->CommercialPartStatGroup[] = [
                        'id' => $item->getId(),
                        'statgroup' => $item->getStatgroupName(),
                        'profit' => $item->getProfit(),
                        'bonus' => $item->getBonus(),
                        'bonus_note' => $item->getBonusNote(),
                        'correct' => $item->getCorrect(),
                        'correct_note' => $item->getCorrectNote(),
                        'preliminary_sum' => ($item->getProfit() - $bonus + $correctirovka),
                        'procent' => $this->member_custom_procent_value,
                        'sum' => $summ
                    ];
                else
                    $this->CommercialPartStatGroup[] = [
                        'department' => $item->getDepartmentId(),
                        'department_name' => $item->getDepartmentName(),
                        'profit' => $item->getProfit(),
                        'bonus' => $item->getBonus(),
                        'bonus_note' => $item->getBonusNote(),
                        'correct' => $item->getCorrect(),
                        'correct_note' => $item->getCorrectNote(),
                        'preliminary_sum' => ($item->getProfit() - $bonus + $correctirovka),
                        'procent' => $this->member_custom_procent_value,
                        'sum' => $summ
                    ];
                break;
            case 5:
            case 7:
                $this->CommercialPartService[] = [
                    'id' => $item->getId(),
                    'name' => $this->member->getLastname() . " " . $this->member->getName(),
                    'statgroup' => $item->getStatgroupName() . "(" . $item->getStatgroupId() . ")",
                    'department_name' => $item->getDepartmentName(),
                    'profit' => $item->getProfit(),
                    'bonus' => $item->getBonus(),
                    'bonus_note' => $item->getBonusNote(),
                    'correct' => $item->getCorrect(),
                    'correct_note' => $item->getCorrectNote(),
                    'preliminary_sum' => ($item->getProfit() - $bonus + $correctirovka),
                    'procent' => $this->member_custom_procent_value,
                    'sum' => $summ
                ];
                break;
            default:
                break;
        }

    }

    /**
     * @return int
     */
    public function getWorkingDays()
    {
        return $this->working_days;
    }

    public function getYearBonusPay() {
        return $this->year_bonuspay;
    }

    /**
     * @return array
     */
//    public function getCommercialPart()
//    {
//        return $this->CommercialPart;
//    }

    /**
     * @return array
     */
    public function getCommercialPartManager()
    {
        return $this->CommercialPartManager;
    }

    public function getCommercialJson() {
        $d = [
            'motivation' => $this->member->getMotivation(),
            'data' => NULL,
        ];

        switch ($this->member->getMotivation()) {
            case 1:
            case 8:
            case 9:
                $d['data'] = $this->CommercialPartManager;
                break;
            case 2:
            case 3:
                $d['data'] = $this->CommercialPartTT;
                break;
            case 4:
                $d['data'] = $this->CommercialPartStatGroup;
                break;
            case 5:
            case 7:
                $d['data'] = $this->CommercialPartService;
                break;
            case 10:
                $d['data']['service'] = $this->CommercialPartService;
                $d['data']['brand'] = $this->CommercialPartStatGroup;
                break;
            default:
                break;
        }
        return json_encode($d, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    }

    /**
     * @return array
     */
    public function getCommercialPartTT()
    {
        return $this->CommercialPartTT;
    }

    /**
     * @return array
     */
    public function getCommercialPartStatGroup()
    {
        return $this->CommercialPartStatGroup;
    }

    /**
     * @return array
     */
    public function getCommercialPartService()
    {
        return $this->CommercialPartService;
    }


    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $test_date = DateTime::createFromFormat("d.m.Y", "01.".$date);
        if ($test_date->format("Y-m-d")<'2021-05-01')
            $this->older_format = 1;
        else
            $this->older_format = 0;
        unset($test_date);

        $this->date = $date;
    }

    /**
     * @param mixed $motivation
     */
    public function setMotivation($motivation)
    {
        $this->motivation = $motivation;
    }

    /**
     * @return mixed
     */
    public function getMotivation()
    {
        return $this->motivation;
    }

    /**
     * @return int
     */
    public function getSummNachenka()
    {
        return $this->summ_nachenka;
    }

    /**
     * @return int
     */
    public function getAllMarkupRubles()
    {
        return $this->All_Markup_Rubles;
    }

    /**
     * @return int
     */
    public function getFullsumm()
    {
        return $this->fullsumm;
    }

    /*
     * Получаю коммеческий процент
     */
    public function getCommercialProcent() {
        return $this->MFB->getCommercialProcent();
    }

    /**
     * @return int
     */
    public function getPaymentMade()
    {
        return $this->payment_made;
    }

    public function setPaymentMade($payment_made)
    {
        $this->payment_made = $payment_made;
    }

    /**
     * @return int
     */
    public function getAllAdministrativeRubles()
    {
        return $this->All_Administrative_Rubles;
    }

    /**
     * @return int
     */
    public function getAllCorrectionRubles()
    {
        return $this->All_Correction_Rubles;
    }

    /**
     * @return int
     */
    public function getAllHoldRubles()
    {
        return $this->All_Hold_Rubles;
    }

    /**
     * @return int
     */
    public function getAllSalary()
    {
        return $this->All_Salary;
    }

    /**
     * @return int
     */
    public function getAllAbsenceRub()
    {
        return $this->All_Absence_Rub;
    }

    /**
     * @return int
     */
    public function getSalary()
    {
        return $this->member->getSalary();
    }

    /*
     * Получаю НДФЛ
     */
    public function getNdfl() {
        return $this->ndfl;
    }

    public function getAwhNote() {
        return $this->awh_note;
    }

    /*
     * Получаю ИЛ
     */
    public function getRo() {
        return $this->ro;
    }

    /*
     * Получаю итоговую сумму
     */
    public function getSummary() {
        switch ($this->member->getMotivation()) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 8:
            case 9:
            case 10:
                return $this->getSalary() - $this->ndfl - $this->ro - $this->All_Salary - $this->All_Absence_Rub + $this->All_Markup_Rubles + $this->All_Administrative_Rubles - $this->All_Hold_Rubles + $this->All_Correction_Rubles - $this->year_bonuspay + $this->kpi_summ + $this->All_Transport_Rubles + $this->All_Mobile_Rubles;
                break;
            case 6:
                return $this->getSalary() - $this->ndfl - $this->ro - $this->All_Salary - $this->All_Absence_Rub + $this->All_Administrative_Rubles - $this->All_Hold_Rubles + $this->All_Correction_Rubles - $this->year_bonuspay + $this->All_Transport_Rubles + $this->All_Mobile_Rubles;
                break;
            case 7:
                return $this->getSalary() - $this->ndfl - $this->ro - $this->All_Salary - $this->All_Absence_Rub + $this->All_Administrative_Rubles - $this->All_Hold_Rubles + $this->All_Correction_Rubles - $this->year_bonuspay + $this->kpi_summ + $this->All_Transport_Rubles + $this->All_Mobile_Rubles;
                break;
        }

//        if ($this->member->getMotivation() != 6) {
//            // Оклад - НДФЛ - ИЛ - АВАНС - Опоздания + Коммерческая_премия + Административная_премия - Удержания + Корректировки
//            return $this->getSalary() - $this->ndfl - $this->ro - $this->All_Salary - $this->All_Absence_Rub + $this->All_Markup_Rubles + $this->All_Administrative_Rubles - $this->All_Hold_Rubles + $this->All_Correction_Rubles - $this->year_bonuspay;
//        } else {
//            return $this->getSalary() - $this->ndfl - $this->ro - $this->All_Salary - $this->All_Absence_Rub + $this->All_Administrative_Rubles - $this->All_Hold_Rubles + $this->All_Correction_Rubles - $this->year_bonuspay;
//        }
    }

    public function getKPI(){
        return $this->KPI;
    }

    public function getKPISumm(){
        return $this->kpi_summ;
    }

    /*
     * Получаю опоздания
     */
    public function getAbsence() {
        return $this->awh_absence;
    }

    /*
     * Получаю опоздания
     */
    public function getNewAbsence() {
        return $this->awh_new_absence;
    }

    /*
     * Устанавливаю опоздания
     */
    public function setAbsence($absence) {
        $this->awh_absence = $absence;
    }

    /*
     * Устанавливаю опоздания
     */
    public function setNewAbsence($absence) {
        $this->awh_new_absence = $absence;
    }

    /*
     * Получаю выходные
     */
    public function getHoliday() {
        return $this->awh_holiday;
    }

    /*
     * Устанавливаю выходные
     */
    public function setHoliday($holiday) {
        $this->awh_holiday = $holiday;
    }

    /*
     * Получаю болезни
     */
    public function getDisease() {
        return $this->awh_disease;
    }

    /*
    * Устанавливаю болезни
    */
    public function setDisease($d) {
        $this->awh_disease = $d;
    }

    /*
     * Получаю класс AB
     */
    public function getAB() {
        return $this->AB;
    }

    public function getAB_Data() {
        return (["Promotions"=>$this->AB->getPromotions(), "AdministrativePrize"=>$this->AB->getAdministrativePrize(), "Training"=>$this->AB->getTraining(), "PrizeAnotherDepartment"=>$this->AB->getPrizeAnotherDepartment(), "Defect"=>$this->AB->getDefect(), "Sunday"=>$this->AB->getSunday(), "NotLessThan"=>$this->AB->getNotLessThan(), "MobileCommunication"=>$this->MB->getSumm(), "Overtime"=>$this->AB->getOvertime(),  "CompensationFAL"=>$this->TR->getSumm()]);
    }

    public function getRN_Data() {
        return (["LateWork"=>$this->RN->getLateWork(), "Internet"=>$this->RN->getInternet(), "CachAccounting"=>$this->RN->getCachAccounting(), "Receivables"=>$this->RN->getReceivables(),  "Credit"=>$this->RN->getCredit(), "Other"=>$this->RN->getOther()]);
    }

    public function getCR_Data() {
        return (["BonusAdjustment"=>$this->CR->getBonusAdjustment(), "Calculation"=>$this->CR->getCalculation(), "YearBonus"=>$this->CR->getYearBonus(), "Holiday"=>$this->CR->getHoliday()]);
    }

    public function getKPI_Data() {
        return $this->KPI->get();
    }

    /*
     * Получаю класс RN
     */
    public function getRN() {
        return $this->RN;
    }

    /*
     * Получаю класс CR
     */
    public function getCR() {
        return $this->CR;
    }

    /*
     * Получаю класс TR
     */
    public function getTR() {
        return $this->TR;
    }

    /*
    * Получаю класс MB
    */
    public function getMB() {
        return $this->MB;
    }

    /*
     * Получаю класс AWH
     */
    public function getAwh() {
        return $this->AWH;
    }

    /*
     * Возвращаю объект истории
     */
    public function getPH() {
        return $this->PH;
    }

    /*
     * Получаю рабочий статус
     */
    public function getWorkStatus() {
        return $this->member->getWorkStatus();
    }

    /*
     * Получаю коммерческие данные
     */
    public function getData() {
        $this->MFB->getData();
    }

    /*
     * Изменение корректировок
     */
    public function changeCorrecting($correcting, $changer_id) {
//        if ($this->MFB->getData() == NULL)
//            $this->MFB->fetchWithDateObj($this->date);

        foreach ($correcting as $id => $item) { // Получаю id объекта и значение item
            // Ищем в массиве объектов, id из массива
            $current = current(array_filter($this->MFB->getData(), function ($obj) use ($id) {
                return $obj->getId() == $id;
            }));

            // $item[0] == Bonus
            // $item[1] == BonusNote
            // $item[2] == Correct
            // $item[3] == CorrectNote
            // ОЧЕНЬ КУХОННО!!!!

            // Если существет такой объект, то проверяем равны ли значения в объекте и массиве, если да, то продолжаем поиск.
            if ($current) {
                if (($current->getBonus() == $item[0]) && (($current->getCorrect() == $item[2])) && ($current->getBonusNote() == $item[1]) && (($current->getCorrectNote() == $item[3]))) {
                    continue;
                }
            } else { // Если объекта нету, то проверяем на пустоту значений, если все пусто, то продолжаем
                if ((empty($item[0])) && empty($item[1]) && (empty($item[2])) && empty($item[3])) {
                    continue;
                }
            }

            // Заменяю запятую на точку
            $item[0] = str_replace(",", ".", $item[0]);
            $item[2] = str_replace(",", ".", $item[2]);

            // Добавляю корректировки в БД
            try {
                $this->MFB->addCorrection($id, $item, $changer_id);

            } catch (Exception $e) {
                throw  new Exception($e->getMessage());
            }
        }

        $this->fetch($this->user_id);
    }

    public function changeCorrectingNew($correcting, $changer_id) {
        foreach ($correcting as $id => $item) { // Получаю id объекта и значение item
            // Ищем в массиве объектов, id из массива
            $current = current(array_filter($this->MFB->getData(), function ($obj) use ($id) {
                return $obj->getId() == $id;
            }));

            if (empty($correcting[7]))
                $correcting[7] = NULL;

            // Если существет такой объект, то проверяем равны ли значения в объекте и массиве, если да, то продолжаем поиск.
            if ($current) {
                if (($current->getBonus() == $item[3]) && (($current->getCorrect() == $item[5])) && ($current->getBonusNote() == $item[4]) && (($current->getCorrectNote() == $item[6])) && ($current->getServiceId() == $correcting[7])) {
                    continue;
                }
            } else { // Если объекта нету, то проверяем на пустоту значений, если все пусто, то продолжаем
                if ((empty(trim($item[3])) and $item[3]!=0) && empty(trim($item[5]) and $item[5]!=0) && (empty(trim($item[4]))) && empty(trim($item[6]))) {
                    continue;
                }
            }

            // Заменяю запятую на точку
            $item[3] = str_replace(",", ".", $item[3]);
            $item[5] = str_replace(",", ".", $item[5]);

            // Добавляю корректировки в БД
            try {
                $this->MFB->addCorrectionNew($item, $changer_id);
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }

            if ($current) {
                $current->setBonus($item[3]);
                $current->setBonusNote($item[4]);
                $current->setCorrect($item[5]);
                $current->setCorrectNote($item[6]);
                $current->setServiceId(($item[7]?$item[7]:NULL));
            }

            if ($this->status>1) {
                switch ($this->motivation) {
                    case 1:
                    case 8:
                    case 9:
                        $this->CommercialPartManager[$id]['bonus'] = $item[3];
                        $this->CommercialPartManager[$id]['bonus_note'] = $item[4];
                        $this->CommercialPartManager[$id]['correct'] = $item[5];
                        $this->CommercialPartManager[$id]['correct_note'] = $item[6];
                        break;
                    case 2:
                    case 3:
                        $this->CommercialPartTT[$id]['bonus'] = $item[3];
                        $this->CommercialPartTT[$id]['bonus_note'] = $item[4];
                        $this->CommercialPartTT[$id]['correct'] = $item[5];
                        $this->CommercialPartTT[$id]['correct_note'] = $item[6];
                        break;
                    case 4:
                        $this->CommercialPartStatGroup[$id]['bonus'] = $item[3];
                        $this->CommercialPartStatGroup[$id]['bonus_note'] = $item[4];
                        $this->CommercialPartStatGroup[$id]['correct'] = $item[5];
                        $this->CommercialPartStatGroup[$id]['correct_note'] = $item[6];
                        break;
                    case 5:
                    case 7:
                        $this->CommercialPartService[$id]['bonus'] = $item[3];
                        $this->CommercialPartService[$id]['bonus_note'] = $item[4];
                        $this->CommercialPartService[$id]['correct'] = $item[5];
                        $this->CommercialPartService[$id]['correct_note'] = $item[6];
                        break;
                    case 10:
                    default:
                        break;
                }
            }
        }

//        $this->fetch($this->user_id);
    }

    /*
     * Изменение коммерческого процента
     */
    public function changeCommercialProcent($date, $CommercialCorrectingProcent, $changer_id) {
        if ($CommercialCorrectingProcent != $this->MFB->getCommercialProcent()) {
            $this->MFB->changeCommercialProcent($date, $CommercialCorrectingProcent, $changer_id);
        }
    }

    /*
     * Фильтры для истории полученные из ролей по Административному блоку
     */
    public function setAdministrativeBlockArray($Arr) {
        $this->AdministrativeBlock_Array = $Arr;
    }

    /*
     * Фильтры для истории полученные из ролей по Удержаниям
     */
    public function setRetitiotionBlockArray($Arr) {
        $this->RetitiotionBlock_Array = $Arr;
    }

    /*
     * Фильтры для истории полученные из ролей по Корректировкам
     */
    public function setCorrectingBlockArray($Arr) {
        $this->CorrectingBlock_Array = $Arr;
    }

    /*
     * Устанавилваю объект сотрудника
     */
    public function setMember($member) {
        $this->member = $member;
        $this->user_id = $this->member->getId();
    }

    /*
     * Устанавливаю НДФЛ
     */
    public function setNdfl($ndfl) {
        $this->ndfl = $ndfl;
    }

    /*
     * Устанавливаю ИЛ
     */
    public function setRo($ro) {
        $this->ro = $ro;
    }

    public function setAwhNote($awh_note) {
        $this->awh_note = $awh_note;
    }

    /*
     * Получаю историю по премии
     */
    public function getPremiumHistory() {
        $this->PH = new PayHistory();
        $this->PH->setMotivation($this->member->getMotivation());

        try {
            $this->PH->fetchPayHistory($this->user_id, 1, $this->date);
        } catch (Exception $e) {
            echo $e->getMessage()."<BR>";
        }

        $this->PH->filterAdministrativeBlock($this->AdministrativeBlock_Array);
        $this->PH->filterRetitiotionBlock($this->RetitiotionBlock_Array);
        $this->PH->filterCorrectingBlock($this->CorrectingBlock_Array);
    }

    /*
     * Устанавливаю корерктировки в руб.
     */
    public function setAllCorrectionRubles($c) {
        $this->All_Correction_Rubles = $c;
    }

    /**
     * @param bool $konvert
     */
    public function setKonvert()
    {
        $this->konvert = true;
    }

    public function getBasePercentId() {
        return $this->CPT->getBasepercentId();
    }

    public function getAdvancedPercentId() {
        return $this->CPT->getAdvancedPercentId();
    }

    /**
     * @return int
     */
    public function getAdministrativeLock()
    {
        return $this->Administrative_lock;
    }

    /**
     * @return int
     */
    public function getCommercialLock()
    {
        return $this->Commercial_lock;
    }

    /**
     * @return int
     */
    public function getCorrectingLock()
    {
        return $this->Correcting_lock;
    }

    /**
     * @return int
     */
    public function getRetentionLock()
    {
        return $this->Retention_lock;
    }
    /**
     * @return int
     */
    public function getKpiLock()
    {
        return $this->kpi_lock;
    }

    /**
     * @return int
     */
    public function getAWHLock()
    {
        return $this->AWH_lock;
    }

    public function setPremiumLock() {
        $this->Administrative_lock=1;
        $this->Retention_lock=1;
        $this->Correcting_lock=1;
        $this->AWH_lock=1;
        $this->Commercial_lock = 1;
        $this->kpi_lock = 1;
    }

    function calculateBonusCorrect(&$date) {
        $this->MFB->calculateBonusCorrect($date);
        $this->fetch($this->user_id);
    }

    public function getYearBonus() {
        return $this->year_bonus;
    }

    public function reloadRetentions(){
        $this->RN->fetch($this->user_id, $this->date);
    }

    public function reloadAdministrative(){
        $this->AB->fetch($this->user_id, $this->date);
    }

    public function reloadCorrections(){
        $this->CR->fetch($this->user_id, $this->date);
    }

    public function getAllTransport() {
        return $this->All_Transport_Rubles;
    }

    public function getAllMobile() {
        return $this->All_Mobile_Rubles;
    }

    /**
     * @return null
     */
    public function getAwhExtraholiday()
    {
        return $this->awh_extraholiday;
    }

    public function setAwhExtraholiday($holiday) {
        $this->awh_extraholiday = $holiday;
    }
}