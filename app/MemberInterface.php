<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 19.07.18
 * Time: 11:10
 */

require_once 'Member.php';
require_once 'functions.php';
require_once 'app/IntChat.php';
require_once 'ProductionCalendar.php';
require_once 'AwhInterface.php';

class MemberInterface
{
    private $dbh;
    private $membersList = array();

    /*
     * Добавляем основную информацию по пользователю
     * Фамилия, Имя, Отчество, пол, дата рождения.
     * Направление, Отдел, Должность.
     * @param $array массив с данными
     * @throws Ошибка добавления в БД member
     * @throws Ошибка извлечения из БД member при получении ID
     * @throws Ошибка добавления в БД member_direction
     * @throws Ошибка добавления в БД member_department
     * @throws Ошибка добавления в БД member_position
     * @return int $MemberId возвращает ID сотрудника
     */
    public function AddMembers($arr, $changer_id)
    {
        list($lastname, $name, $middle, $sex, $birthdate, $direction, $department, $position, $nation, $addDate) = $arr;

        $lastname = trim($lastname);
        $name = trim($name);
        $middle = trim($middle);

        if (!$nation || !is_numeric($nation)) {
            $nation = 0;
        }

        if (!$addDate || !$this->is_Date($addDate)) {
            $addDate = date('d.m.Y');
        }

        /*
         * Подключение к БД
         */
        try {
            $this->dbh = dbConnect();
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }

        /*
         *  Расчет отсутствий
         */
        $emp_date = DateTime::createFromFormat("d.m.Y", $addDate);

        /*
         *  Создаю объект производственный календарь
         */
        $PC = new ProductionCalendar();
        /*
         * Получаю рабочие дни из производственного календаря
         */
//        $PC->fetchByDate($emp_date->format("m.Y"));
        $PC->fetchByDateWithDepartment($emp_date->format("m.Y"), $department);
        /*
        * Извлекаю массив с рабочими днями
        */
//        $PC->fetchWorkingArray($emp_date->format("m.Y"));
        $PC->fetchWorkingArrayWithDepartment($emp_date->format("m.Y"), $department);
        /*
        * Получаю массив с рабочими днями
        */
        $working_days_array = $PC->getWDArray();


        $awh_absence = 0;
        $awh_absence_avans = 0;

        /*
         * Расчет отсутствий в премию
         */
        foreach ($working_days_array as $wp) {
            if ($emp_date->format("d") != $wp ){
                $awh_absence += 1;
            } else {
                break;
            }
        }

        /*
         * Получаю первую часть рабочих дней при котором сотрудник получает аванс
         */
        $working_part = array_splice($working_days_array, 0, (ceil(count($working_days_array) / 2 )) );

        /*
         * Расчет отсутствий в аванс
         */
        foreach ($working_part as $wp) {
            if ($emp_date->format("d") != $wp ){
                $awh_absence_avans += 1;
            } else {
                break;
            }
        }

        $working_days_array = $PC->getWDArray();
        if ($awh_absence_avans >= count($working_part) && count($working_days_array) % 2 !== 0) {
            $awh_absence_avans = count($working_days_array) / 2;
        }

        $query = "INSERT INTO member(lastname, name, middle, sex, birthday, employment_date, reasons_for_leaving_id) VALUES (?,?,?,?,STR_TO_DATE(?, '%d.%m.%Y'),STR_TO_DATE(?, '%d.%m.%Y'), 0)";
        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$lastname, $name, $middle, $sex, $birthdate, $addDate])) {
            $error = $sth->errorInfo();
            throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        /*
         *  Получаю id сотрудника
         */
        $MemberId = $this->dbh->lastInsertId();

        /*
         * Добавляю отсутствия
         */
        $AWH = new AwhInterface();
        $AWH->setNewAbsenceByMember($MemberId, $emp_date->format("m.Y"), 1, $awh_absence);
        $AWH->setNewAbsenceByMember($MemberId, $emp_date->format("m.Y"), 2, $awh_absence_avans);

        // Добавляю направдение отдел должность
        $query = "insert into member_jobpost(member_id, direction_id, position_id, department_id, date_s, changer_id) VALUES(?,?,?,?,STR_TO_DATE(?, '%d.%m.%Y'),?)";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$MemberId, $direction, $position, $department, $addDate, $changer_id]);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка. ' . $exception->getMessage());
        }

        /***** Добавляю рабочую информацию, login,1CSklad,1CZik *****/
        $active = 1;

        $sklad_name = NULL;
//        $sklad_name = $name . " " . $lastname;
//        $cnt = 0;
//        while ($this->Testsklad($sklad_name)) {
//            switch ($cnt) {
//                case 0:
//                    $sklad_name .= " " . $middle;
//                    break;
//                case 1:
//                    $sklad_name .= $cnt;
//                    break;
//                case 10:
//                    exit();
//                default:
//                    $sklad_name++;
//                    break;
//            }
//            $cnt++;
//        }

        $login = $this->translit($lastname);
        $login = strtolower($login);

        $cnt = 0;
        while ($this->TestLogin($login)) {
            switch ($cnt) {
                case 0:
                    $login .= "_" . strtolower($this->translit(mb_substr($name, 0, 1)));
                    break;
                case 1:
                    $login .= strtolower($this->translit(mb_substr($middle, 0, 1)));
                    break;
                case 2:
                    $login .= "_" . $cnt;
                    break;
                case 10:
                    exit();
                default:
                    $login++;
                    break;
            }
            $cnt++;
        }

        $query = "insert into member_workstatus (member_id, date_s, changer_id) VALUES (?, NOW(), ?)";
        try {
            $sth_sub = $this->dbh->prepare($query);
            $sth_sub->execute([$MemberId, $changer_id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка добавления workstatus" . $e->getMessage());
        }

        $query = "insert into member_work(login, 1CSklad_name, personnel_number, date, active, member_id) VALUES (?, ?, ?, STR_TO_DATE(?, '%d.%m.%Y'), ?, ?)";

        try {
            $sth_sub = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth_sub->execute([$login, $sklad_name, 0, $addDate, $active, $MemberId])) {
            $error = $sth_sub->errorInfo();
            throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }


        try {
            $this->ChangeNation($MemberId, $nation, $changer_id);
        } catch (Exception $e) {
            echo $e->getMessage() . "<BR>";
        }

        try {
            $this->ChangeMotivation($MemberId, 6, $changer_id);
        } catch (Exception $e) {
            echo $e->getMessage() . "<BR>";
        }

        $query = null;
        $sth = null;
        $this->dbh = null;

        return $MemberId;
    }

    public function deleteUser($member_id)
    {
        try {
            if ($this->dbh == null) { // Если нету подключения, то
                $this->dbh = dbConnect(); // Подключаемся
            }
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }

        $TableList = [
            "member_AdministrativePrize",
            "member_MobileCommunication",
            "member_Transport",
            "member_auth",
            "member_avans",
            "member_awh",
            "member_awh_extraholiday",
            "member_jobpost",
            "member_kpi",
            "member_mails",
            "member_motivation",
            "member_name_archive",
            "member_nation",
            "member_ndfl",
            "member_percent",
            "member_percent_advanced",
            "member_personal_emergency_phones",
            "member_personal_phones",
            "member_procent_avans",
            "member_salary",
            "member_work",
            "member_work_local_phone",
            "member_work_phones",
            "member_workstatus",
            "member_yearbonus_pay",
        ];

        foreach ($TableList as $table) {
            $query = "DELETE FROM ".$table." WHERE member_id = ?";
            try {
                $sth = $this->dbh->prepare($query);
                $sth->execute([$member_id]);
            } catch (PDOException $e) {
                throw new Exception('Ошибка удаления из БД. ' . $e->getMessage());
            }
        }

        $query = "DELETE FROM member WHERE id = ?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$member_id]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка удаления из БД. ' . $e->getMessage());
        }

        return true;
    }

    /*
     * Добавление пользователей из CSV файла с минимальными полями
     * Фамилия, Имя, Отчество, пол, дата рождения, направление, отдел, должность, дата приема на работу.
     */
    public function AddMembersFromCSV($user_array)
    {
        list($lastname, $name, $middle, $maiden, $sex, $birthday, $nation_name, $zik, $direction, $department, $position, $employment_date, $login, $sklad_name, $dismissal_date) = $user_array;
//        var_dump($user_array); echo "<BR>";

        $lastname = trim($lastname);
        $name = trim($name);
        $middle = trim($middle);
        $maiden = trim($maiden);

        if ($sex == "Мужской") {
            $sex = 0;
        } else {
            $sex = 1;
        };

        $employment_date = trim($employment_date);
        if (!preg_match("/^[0-9]{2}\.[0-9]{2}\.[0-9]{4}$/i", $employment_date)) {
            throw new Exception("Не могу сформировать дату приема сотрудника " . $lastname . " " . $name . " " . $middle);
        }

        if (!preg_match("/^[0-9]{2}\.[0-9]{2}\.[0-9]{4}$/i", $birthday) AND ($birthday != null)) {
            throw new Exception("Неверная дата рождения " . $lastname . " " . $name . " " . $middle);
        }

        try {
            $this->dbh = dbConnect();
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }

        /*
         * Проверяем сущестует ли пользователь в БД
         */
        $query = "select * from member where lastname=? and name=? and middle=?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([trim($lastname), trim($name), trim($middle)]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка добавления в БД. ' . $e->getMessage());
        }
        if ($sth->rowCount() > 0) {
            throw new Exception("Сотрудник " . $lastname . " " . $name . " " . $middle . " уже заведен");
        }

        /*****
         * Проверка существования наравления, отдела, должности
         * А так же получение id
         ****/

        /*
         * Получаю ID направления
         */
        $query = "select * from direction where LOWER(name)=?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([strtolower(trim($direction))]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка добавления в БД ' . $e->getMessage());
        }

        $direction_id = $sth->fetch(PDO::FETCH_ASSOC)['id'];
        if ($direction_id == null) {
            throw new Exception($lastname . " " . $name . " " . $middle . "Направление не может быть пустым. Или такого направления не существует.");
        }

        /*
         * Получаю id отдела
         */
        $query = "select * from department where LOWER(name)=?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([strtolower(trim($department))]);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка добавления в БД' . $exception->getMessage());
        }

        $department_id = $sth->fetch(PDO::FETCH_ASSOC)['id'];
        if ($department_id == null) {
            throw new Exception($lastname . " " . $name . " " . $middle . " Отдел не может быть пустым. Или такого отдела не существует.");
        }

        /*
         * Получаю id должности
         */
        $query = "select * from position where LOWER(name)=?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([strtolower(trim($position))]);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка добавления в БД. ' . $exception->getMessage());
        }

        $position_id = $sth->fetch(PDO::FETCH_ASSOC)['id'];
        if ($position_id == null) {
            throw new Exception($lastname . " " . $name . " " . $middle . "Должность не должа быть пустой. Или такой должности не существует.");
        }

        /*
         * Добавляю сотрудника в БД
         */
        $query = "INSERT INTO member (lastname,name,middle,sex,birthday,employment_date) VALUES (?,?,?,?,STR_TO_DATE(?, '%d.%m.%Y'),STR_TO_DATE(?, '%d.%m.%Y'))";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([trim($lastname), trim($name), trim($middle), trim($sex), trim($birthday), $employment_date]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка добавления в БД. ' . $e->getMessage());
        }

        /*
         * Получаю id сотрудника
         */
        $MemberId = $this->dbh->lastInsertId();

        /*
         * Если присутсвует дата увольнения, то ставлю статут увольнения
         */
        if ($dismissal_date != NULL) {
            try {
                $this->LeavingByID($MemberId, 1, $dismissal_date);
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }

        }

        // Добавляю направдение отдел должность
        $query = "insert into member_jobpost(member_id, direction_id, position_id, department_id,date_s) VALUES(?,?,?,?,STR_TO_DATE(?, '%d.%m.%Y'))"; // Дата будет выставлена '0000-00-00'
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$MemberId, $direction_id, $position_id, $department_id, $employment_date]);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка. ' . $exception->getMessage());
        }

        /** Национальность **/
        $query = "select * from nation where LOWER(name)=?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([strtolower(trim($nation_name))]);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        $nation_id = $sth->fetch(PDO::FETCH_ASSOC)['id'];
        if ($nation_id == null) {
            $nation_id = 1;
        }

        if (empty($zik) || !is_numeric($zik)) {
            $zik = null;
        }

        $query = "insert into member_work (login,1CSklad_name,personnel_number,date,active,member_id) VALUES (?,?,?,NOW(),1,?)";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$login, $sklad_name, $zik, $MemberId]);
        } catch (PDOException $exception) {
            throw new Exception($exception->getMessage());
        }

        try {
            $this->ChangeNation($MemberId, $nation_id);
        } catch (Exception $e) {
            echo $e->getMessage() . "<BR>";
        }

        if (!empty($maiden)) {
            try {
                $this->ChangeMaidenName($MemberId, trim($maiden));
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
        }

        $IC = new IntChat();
        try {
            $IC->connect();
            $IC->setUserID($MemberId);
            $IC->getPicture(trim($lastname) . " " . trim($name));
            $IC->close();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        try {
            $this->ChangeMotivation($MemberId, 6);
        } catch (Exception $e) {
            echo $e->getMessage() . "<BR>";
        }

        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }


    /*
     * Добавляем личную информацию
     * Страна, регион, город, область, улица, почтовый индекс
     * @param $array массив с данными
     * @param $member_id ID пользователя
     * @throws Не верный id пользователя
     * @throws Ошибка добавления в БД
     * @return bool true при успешном выполнении кода.
     */
    public function AddPersonalCustom($array, $member_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Ошибка при добавлении личных данных. Неверное значение ID пользователя<BR>');
        }

        /*
         * инициализация статичных переменных
         */
        $current = 1;

        // Удаляем концевые пробелы
        foreach ($array as $key => $value) {
            $array[$key] = trim($array[$key]);
        }

//        list($country,$region,$city,$area,$street,$postindex) = $array; //Разбираем полученный массив

//        try {
//            $this->dbh = dbConnect();
//        } catch (PDOException $exception) {
//            throw new Exception('Can\'t connect to db: '.$exception->getMessage());
//        }
//
//        $query = "INSERT INTO member_personal (country,region,city,area,street,postindex,date,current,member_id) VALUES (?,?,?,?,?,?,NOW(),?,?)";
//        try {
//            $sth = $this->dbh->prepare($query);
//        } catch (PDOException $exception) {
//            throw new Exception('Ошибка prepare'.$exception->getMessage());
//        }
//
//        if (!$sth->execute([$country,$region,$city,$area,$street,$postindex,$current,$member_id])) {
//            $error = $sth->errorInfo();
//            throw new Exception('Возника ошибка при добавлении персональных данных у пользователя с ID: '.$member_id.' <BR> Код ошибки: '.$error[1].'.<br> Текст ошибки: '.$error[2]."<BR>");
//        }

        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }

    /*
     * Добавляю комментарий на испытательный срок для сотрудника
     */
    public function AddProbationComment($member_id, $note)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверный ID сотрудника: ' . $member_id);
        }

        try {
            if ($this->dbh == null) { // Если нету подключения, то
                $this->dbh = dbConnect(); // Подключаемся
            }
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }

        $query = "select * from probation where member_id = ?";
        $sth = $this->dbh->prepare($query);
        $sth->execute([$member_id]); // Выполняю запрос

        if ($sth->rowCount() > 0) {
            $query = "delete * from probation where member_id = ?";
            $sth = $this->dbh->prepare($query);
            $sth->execute([$member_id]); // Выполняю запрос

            $query = "update probation set note=? where member_id=?";
            try {
                $sth = $this->dbh->prepare($query);
                $sth->execute([$note, $member_id]);
            } catch (PDOException $e) {
                throw new Exception($e->getMessage());
            }
        } else {
            $query = "INSERT INTO probation (note, member_id) VALUES (?,?)";
            $sth = $this->dbh->prepare($query);
            if (!$sth->execute([$note, $member_id])) { // Выполняю запрос
                $error = $sth->errorInfo();
                throw new Exception('Возника ошибка при добавлении в БД для пользователя с ID: ' . $member_id . ' <BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
            }
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        return true;
    }

    /*
     * Добавляет в БД сотруднику личный(персональный) телефон.
     * Один телефон одному сотруднику, не пакетная обработка
     * @param $phone телефнон
     * @param $member_id ID пользователя
     * @throws Не верный id пользователя
     * @throws Ошибка добавления в БД
     * @return bool true при успешном выполнении кода.
     */
    public function AddPersonalPhone($phone, $member_id, $changer_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверный ID сотрудника: ' . $member_id);
        }

        if (!is_numeric($phone)) { //Проверяем что это число
            throw new Exception('Неверный номер телефона: ' . $phone);
        }

        if (!preg_match("/^[0-9]{10,10}+$/", $phone)) {  // Должно быть 10 цифр
            throw new Exception('Неверный формат телефона: ' . $phone);
        }

        /*
         * инициализация статичных переменных
         */
        $active = 1;
        $note = ""; // Без заметок! Пока.
        $is_mobile = 0; // Не мобильный

        try {
            $this->dbh = dbConnect(); // подключаемся к серверу
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }
        $query = "INSERT INTO member_personal_phones (phone,date,is_mobile,active,note,member_id, changer_id) VALUES (?,NOW(),?,?,?,?,?)";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$phone, $is_mobile, $active, $note, $member_id, $changer_id])) { // Выполняю запрос
            $error = $sth->errorInfo();
            throw new Exception('Возника ошибка при добавлении в БД для пользователя с ID: ' . $member_id . ' <BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }

    /*
     * Добавляет в БД сотрудников личные(персональные) телефоны.
     * @param $array массив с данными
     * @param $member_id ID пользователя
     * @throws Не верный id пользователя
     * @throws Ошибка добавления в БД
     * @return bool true при успешном выполнении кода.
     */
    public function AddPersonalPhones($array, $member_id, $changer_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Ошибка при добавлении личных телефонов. Неверное значение ID пользователя<BR>');
        }

        /*
         * инициализация статичных переменных
         */
        $active = 1;
        $note = "";

        try {
            $this->dbh = dbConnect(); // подключаемся к серверу
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }
        $query = "INSERT INTO member_personal_phones (phone,date,active,note,member_id, changer_id) VALUES (?,NOW(),?,?,?,?)";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        foreach ($array as $value) {
            if (is_numeric($value)) { // номер телефона должен быть numeric! иначе false
                if (preg_match("/^[0-9]{10,10}+$/", $value)) {  // Должно быть 10 цифр
                    if (!$sth->execute([$value, $active, $note, $member_id, $changer_id])) { // Выполняю запрос
                        $error = $sth->errorInfo();
                        throw new Exception('Ошибка при дробавлянии личных телефонов. Возника ошибка при добавлении в БД для пользователя с ID: ' . $member_id . ' <BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
                    }
                }
            } else {
                throw new Exception('Ошибка при дробавлянии личных телефонов. Неверное значение у номера телефона.');
            }
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }

    /*
     * Добавляет в БД сотруднику личный(персональный) мобильный телефон.
     * Один телефон одному сотруднику, не пакетная обработка
     * @param $phone телефнон
     * @param $member_id ID пользователя
     * @throws Не верный id пользователя
     * @throws Ошибка добавления в БД
     * @return bool true при успешном выполнении кода.
     */
    public function AddMobilePersonalPhone($phone, $member_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверный ID сотрудника: ' . $member_id);
        }

        if (!is_numeric($phone)) { //Проверяем что это число
            throw new Exception('Неверный номер телефона: ' . $phone);
        }

        if (!preg_match("/^[0-9]{10,10}+$/", $phone)) {  // Должно быть 10 цифр
            throw new Exception('Неверный формат телефона: ' . $phone);
        }

        /*
         * инициализация статичных переменных
         */
        $active = 1;
        $note = ""; // Без заметок! Пока.
        $is_mobile = 1; // Мобильный

        try {
            $this->dbh = dbConnect(); // подключаемся к серверу
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }
        $query = "INSERT INTO member_personal_phones (phone,date,is_mobile,active,note,member_id) VALUES (?,NOW(),?,?,?,?)";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$phone, $is_mobile, $active, $note, $member_id])) { // Выполняю запрос
            $error = $sth->errorInfo();
            throw new Exception('Возника ошибка при добавлении в БД для пользователя с ID: ' . $member_id . ' <BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }

    /*
     * Личные данные. Добавляет аварийный телефон в профиль сотрудника.
     * @param $array массив с данными (телефон и комментарий)
     * @param $member_id ID пользователя
     * @throws Не верный id пользователя
     * @throws Ошибка добавления в БД
     * @throws Ошибка номера телефона
     * @return bool true при успешном выполнении кода.
     */
    public function AddEmergancyPhone($array, $member_id, $changer_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверное значение ID пользователя: ' . $member_id);
        }

        $phone = $array['phone'];
        $note = $array['name'];

        if (!is_numeric($phone)) { //Проверяем что это число
            throw new Exception('Неверный номер телефона: ' . $phone);
        }

        if (!preg_match("/^[0-9]{10,10}+$/", $phone)) {  // Должно быть 10 цифр
            throw new Exception('Неверный формат телефона: ' . $phone);
        }

        /*
         * инициализация статичных переменных
         */
        $active = 1;

        try {
            $this->dbh = dbConnect(); // подключаемся к серверу
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }
        $query = "INSERT INTO member_personal_emergency_phones (phone,date,active,note,member_id, changer_id) VALUES (?,NOW(),?,?,?,?)";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$phone, $active, $note, $member_id, $changer_id])) { // Выполняю запрос
            $error = $sth->errorInfo();
            throw new Exception('Возника ошибка при добавлении в БД для пользователя с ID: ' . $member_id . ' <BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }

    /*
     * Личные данные. Добавляет аварийные телефоны в профиль сотрудника.
     * @param $array массив с данными
     * @param $member_id ID пользователя
     * @throws Не верный id пользователя
     * @throws Ошибка добавления в БД
     * @throws Ошибка номера телефона
     * @return bool true при успешном выполнении кода.
     */
    public function AddEmergancyPhones($array, $member_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверное значение ID пользователя: ' . $member_id);
        }

        /*
         * инициализация статичных переменных
         */
        $active = 1;

        try {
            $this->dbh = dbConnect(); // подключаемся к серверу
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }
        $query = "INSERT INTO member_personal_emergency_phones (phone,date,active,note,member_id) VALUES (?,NOW(),?,?,?)";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        // Нужно ли проверять $value['comment'] по REGEX ?
        foreach ($array as $value) {
            if (is_numeric($value['phone'] && preg_match("/^[0-9]{10,10}+$/", $value['phone']))) { // номер телефона должен быть numeric! иначе false, должно быть 10 цифр
                if (!$sth->execute([trim($value['phone']), $active, trim($value['comment']), $member_id])) { // Выполняю запрос
                    $error = $sth->errorInfo();
                    throw new Exception('Ошибка при дробавлянии личных аварийных телефонов. Возника ошибка при добавлении в БД для пользователя с ID: ' . $member_id . ' <BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
                }
            } else {
                throw new Exception('Ошиька при добавлении аварийных телефонов. В номере телефона: ' . $value['phone']);
            }
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }

    /*
     * Добавление почтовые адреса в профиль сотрудника.
     * @param $array массив с данными (array['email'], array['comment'])
     * @param $member_id ID пользователя
     * @throws Не верный id пользователя
     * @throws Ошибка добавления в БД
     * @throws Ошибка в адресе
     * @return bool true при успешном выполнении кода.
     */
    public function AddEMail($array, $member_id, $changer_id)
    {

        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверное значение ID пользователя: ' . $member_id);
        }

        if (!filter_var(trim($array['email']), FILTER_VALIDATE_EMAIL)) { // Проводим валидацию почтового адреса
            throw new Exception('Неверный формат почтового ящика: ' . $array['email']);
        }

        /*
         * инициализация статичных переменных
         */
        $active = 1;

        try {
            $this->dbh = dbConnect();
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }

        $query = "INSERT INTO member_mails (email,note,date,active,member_id, changer_id) VALUES (?,?,NOW(),?,?,?)";
        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([trim($array['email']), trim($array['comment']), $active, $member_id, $changer_id])) {
            throw new Exception('Ошибка добавления в БД. Код ошибки: ' . $sth->errorInfo()[1] . '.<br> Текст ошибки: ' . $sth->errorInfo()[2]);
        }

        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }


    /*
      * Добавляет в БД сотруднику рабочий телефон.
      * Один телефон одному сотруднику
      * @param $phone телефнон
      * @param $member_id ID пользователя
      * @throws Не верный id пользователя
      * @throws Ошибка добавления в БД
      * @return bool true при успешном выполнении кода.
      */
    public function AddWorkPhone($phone, $member_id, $changer_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверный ID сотрудника: ' . $member_id);
        }

        if (!is_numeric($phone)) { //Проверяем что это число
            throw new Exception('Неверный номер телефона: ' . $phone);
        }

        if (!preg_match("/^[0-9]{10,10}+$/", $phone)) {  // Должно быть 10 цифр
            throw new Exception('Неверный формат телефона: ' . $phone);
        }

        /*
         * инициализация статичных переменных
         */
        $active = 1;
        $is_mobile = 0;
        $note = ""; // Без заметок! Пока.

        try {
            $this->dbh = dbConnect(); // подключаемся к серверу
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }
        $query = "INSERT INTO member_work_phones (phone, date, active, note, is_mobile, member_id, changer_id) VALUES (?,NOW(),?,?,?,?,?)";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$phone, $active, $note, $is_mobile, $member_id, $changer_id])) { // Выполняю запрос
            $error = $sth->errorInfo();
            throw new Exception('Возника ошибка при добавлении в БД рабочих телефонов. Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }

    /*
      * Добавляет в БД сотруднику мобильный рабочий телефон.
      * Один телефон одному сотруднику
      * @param $phone телефнон
      * @param $member_id ID пользователя
      * @throws Не верный id пользователя
      * @throws Ошибка добавления в БД
      * @return bool true при успешном выполнении кода.
      */
    public function AddMobileWorkPhone($phone, $member_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверный ID сотрудника: ' . $member_id);
        }

        if (!is_numeric($phone)) { //Проверяем что это число
            throw new Exception('Неверный номер телефона: ' . $phone);
        }

        if (!preg_match("/^[0-9]{10,10}+$/", $phone)) {  // Должно быть 10 цифр
            throw new Exception('Неверный формат телефона: ' . $phone);
        }

        /*
         * инициализация статичных переменных
         */
        $active = 1;
        $is_mobile = 1;
        $note = ""; // Без заметок! Пока.

        try {
            $this->dbh = dbConnect(); // подключаемся к серверу
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }
        $query = "INSERT INTO member_work_phones (phone, date, active, note, is_mobile, member_id) VALUES (?,NOW(),?,?,?,?)";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$phone, $active, $note, $is_mobile, $member_id])) { // Выполняю запрос
            $error = $sth->errorInfo();
            throw new Exception('Возника ошибка при добавлении в БД рабочих телефонов. Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }

    /*
     * Добавляет в БД сотруднику местный рабочий телефон.
     * Один телефон одному сотруднику, не пакетная обработка
     * @param $phone телефнон
     * @param $member_id ID пользователя
     * @throws Не верный id пользователя
     * @throws Ошибка добавления в БД
     * @return bool true при успешном выполнении кода.
     */
    public function AddWorkLocal($phone, $member_id, $changer_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверный ID сотрудника: ' . $member_id);
        }

        if (!is_numeric($phone)) { //Проверяем что это число
            throw new Exception('Неверный номер телефона: ' . $phone);
        }

        if (!preg_match("/^[0-9]{4,4}+$/", $phone)) {  // Должно быть 4 цифры
            throw new Exception('Неверный формат телефона: ' . $phone);
        }

        /*
         * инициализация статичных переменных
         */
        $active = 1;
        $note = ""; // Без заметок! Пока.

        try {
            $this->dbh = dbConnect(); // подключаемся к серверу
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }
        $query = "INSERT INTO member_work_local_phone (phone,date,active,member_id, changer_id) VALUES (?,NOW(),?,?,?)";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$phone, $active, $member_id, $changer_id])) { // Выполняю запрос
            $error = $sth->errorInfo();
            throw new Exception('Возника ошибка при добавлении в БД для пользователя с ID: ' . $member_id . ' <BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }

    /*
     * Обновление рабочих реквизитов
     */
    public function ChangeJobpost($id, $department, $direction, $position, $date, $changer_id)
    {
        if (!is_numeric($id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($department) || !is_numeric($direction) || !is_numeric($position)) {
            throw new Exception("Некорректные данные");
        }

        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT * FROM member_jobpost where member_id=? and active=1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $version = $sth->fetch(PDO::FETCH_ASSOC)['version'];
            $version++;

            $query = "update member_jobpost SET active=0, date_e=STR_TO_DATE(?, '%d.%m.%Y') where member_id=? and active=1";

            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$date, $id]);
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO' . $e->getMessage());
            }
        }

        $query = "insert into member_jobpost (member_id, direction_id, position_id, department_id, date_s, active, version, changer_id) VALUES (?,?,?,?,STR_TO_DATE(?, '%d.%m.%Y'),1,?,?)";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id, $direction, $position, $department, $date, $version, $changer_id]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO. ' . $e->getMessage());
        }
    }

    public function updateJobPost($id, $department, $direction, $position, $date, $changer_id)
    {
        if (!is_numeric($id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($department) || !is_numeric($direction) || !is_numeric($position)) {
            throw new Exception("Некорректные данные");
        }

        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT * FROM member_jobpost where member_id=? and active=1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $version = $sth->fetch(PDO::FETCH_ASSOC)['version'];
            $version++;

            $query = "update member_jobpost SET direction_id=?, position_id=?, department_id=?, changer_id=? where member_id=? and active=1";

            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$direction, $position, $department, $changer_id, $id]);
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO' . $e->getMessage());
            }
        } else {
            $query = "insert into member_jobpost (member_id, direction_id, position_id, department_id, date_s, active, version, changer_id) VALUES (?,?,?,?,STR_TO_DATE(?, '%d.%m.%Y'),1,?,?)";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$id, $direction, $position, $department, $date, $version, $changer_id]);
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO. ' . $e->getMessage());
            }
        }
    }

    // Оклад
    public function ChangeSalary($user_id, $salary, $changer_id)
    {
        if (!is_numeric($user_id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($salary)) {
            throw new Exception("Оклад не число");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT * FROM member_salary where member_id=? and active=1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO' . $sth->errorInfo()[2]);
        };

        if (!$sth->execute([$user_id])) {
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }

        if ($sth->rowCount() > 0) {
            $query = "update member_salary SET active=0, date_e=NOW(), changer_id=? where member_id=? and active=1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$changer_id, $user_id]);
            } catch (PDOException $e) {
                throw new Exception("Не могу выполнить зарос к БД" . $e->getMessage());
            }

            if ($sth->rowCount() == 0) {
                throw new Exception('Не могу обновить строки' . $sth->errorInfo()[2]);
            }
        }

        $query = "insert into member_salary (member_id, date_s, salary, changer_id) VALUES (?, NOW(), ?,?)";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$user_id, $salary, $changer_id]);
        } catch (PDOException $e) {
            throw new Exception('Не могу добавить.' . $e->getMessage());
        }
    }

    public function ChangeSalarywithDate($user_id, $salary, $changer_id, $date)
    {
        if (!is_numeric($user_id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($salary)) {
            throw new Exception("Оклад не число");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT * FROM member_salary where member_id=? and active=1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO' . $sth->errorInfo()[2]);
        };

        if (!$sth->execute([$user_id])) {
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }

        $lastdate = $sth->fetch()['date_s'];

        if (strtotime($lastdate) >  strtotime($date)) {
            throw new Exception("Изменение в окладе. Последняя дата изменения была ".$lastdate.". Новая дата меньше последней даты изменения, что невозможно.");
        }

        if ($sth->rowCount() > 0) {
            $query = "update member_salary SET active=0, date_e=STR_TO_DATE(?,'%d.%m.%Y'), changer_id=? where member_id=? and active=1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$date, $changer_id, $user_id]);
            } catch (PDOException $e) {
                throw new Exception("Не могу выполнить зарос к БД" . $e->getMessage());
            }

            if ($sth->rowCount() == 0) {
                throw new Exception('Не могу обновить строки' . $sth->errorInfo()[2]);
            }
        }

        $query = "insert into member_salary (member_id, date_s, salary, changer_id) VALUES (?, STR_TO_DATE(?,'%d.%m.%Y'), ?,?)";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$user_id, $date, $salary, $changer_id]);
        } catch (PDOException $e) {
            throw new Exception('Не могу добавить.' . $e->getMessage());
        }
    }

    // Административная премия
    public function ChangeAdministrativePrize($user_id, $AdministrativePrize, $Note, $changer_id)
    {
        if (!is_numeric($user_id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($AdministrativePrize)) {
            throw new Exception("Оклад не число");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT * FROM member_AdministrativePrize where member_id=? and active=1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO' . $sth->errorInfo()[2]);
        };

        if (!$sth->execute([$user_id])) {
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }

        $count = $sth->rowCount();
        if ($count > 0) {
            $query = "update member_AdministrativePrize SET active=0, date_e=NOW(), changer_id=? where member_id=? and active=1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$changer_id, $user_id]);
            } catch (PDOException $e) {
                throw new Exception("Не могу выполнить зарос к БД" . $e->getMessage());
            }

            if ($sth->rowCount() == 0) {
                throw new Exception('Не могу обновить строки' . $sth->errorInfo()[2]);
            }
        }

        $query = "insert into member_AdministrativePrize (member_id, date_s, AdministrativePrize, Note, changer_id) VALUES (?, NOW(), ?, ?, ?)";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$user_id, $AdministrativePrize, $Note, $changer_id]);
        } catch (PDOException $e) {
            throw new Exception('Не могу добавить.' . $e->getMessage());
        }

        $query = null;
        $sth = null;
    }

    // Административная премия
    public function ChangeAdministrativePrizeWithDate($user_id, $AdministrativePrize, $Note, $date, $changer_id)
    {
        if (!is_numeric($user_id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($AdministrativePrize)) {
            throw new Exception("Оклад не число");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT * FROM member_AdministrativePrize where member_id=? and active=1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO' . $sth->errorInfo()[2]);
        };

        if (!$sth->execute([$user_id])) {
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }

        $lastdate = $sth->fetch()['date_s'];

        if (strtotime($lastdate) >  strtotime($date)) {
            throw new Exception("Изменение в Административной премии. Последняя дата изменения была ".$lastdate.". Новая дата меньше последней даты изменения, что невозможно.");
        }

        $count = $sth->rowCount();
        if ($count > 0) {
            $query = "update member_AdministrativePrize SET active=0, date_e=STR_TO_DATE(?,'%d.%m.%Y'), changer_id=? where member_id=? and active=1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$date, $changer_id, $user_id]);
            } catch (PDOException $e) {
                throw new Exception("Не могу выполнить зарос к БД" . $e->getMessage());
            }

            if ($sth->rowCount() == 0) {
                throw new Exception('Не могу обновить строки' . $sth->errorInfo()[2]);
            }

        };
        $query = "insert into member_AdministrativePrize (member_id, date_s, AdministrativePrize, Note, changer_id) VALUES (?, STR_TO_DATE(?, '%d.%m.%Y'), ?, ?, ?)";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$user_id, $date, $AdministrativePrize, $Note, $changer_id]);
        } catch (PDOException $e) {
            throw new Exception('Не могу добавить.' . $e->getMessage());
        }

        $query = null;
        $sth = null;
    }

    // Мобильная связь
    public function ChangeMobileCommunication($user_id, $MobileCommunication, $MobileFormula, $changer_id)
    {
        if (!is_numeric($user_id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($MobileCommunication)) {
            throw new Exception("Оклад не число");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT * FROM member_MobileCommunication where member_id=? and active=1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO' . $sth->errorInfo()[2]);
        };

        if (!$sth->execute([$user_id])) {
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }

        $count = $sth->rowCount();
        if ($count > 0) {
            $query = "update member_MobileCommunication SET active=0, date_e=NOW(), changer_id=? where member_id=? and active=1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$changer_id, $user_id]);
            } catch (PDOException $e) {
                throw new Exception("Не могу выполнить зарос к БД" . $e->getMessage());
            }

            if ($sth->rowCount() == 0) {
                throw new Exception('Не могу обновить строки' . $sth->errorInfo()[2]);
            }

            $query = "insert into member_MobileCommunication (member_id, date_s, MobileCommunication, formula, changer_id) VALUES (?, NOW(), ?, ?, ?)";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$user_id, $MobileCommunication, $MobileFormula, $changer_id]);
            } catch (PDOException $e) {
                throw new Exception('Не могу добавить.' . $e->getMessage());
            }
        } else { // Если еше не заносили данных, то добавляю с data_s по умолчанию 0000-00-00 //////////// ИСПАВИТЬ В ПРОДАКШЕНЕ
            $query = "insert into member_MobileCommunication (member_id, date_s, MobileCommunication, formula, changer_id) VALUES (?, NOW(), ?, ?, ?)";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$user_id, $MobileCommunication, $MobileFormula, $changer_id]);
            } catch (PDOException $e) {
                throw new Exception('Не могу добавить.' . $e->getMessage());
            }
        }

        $query = null;
        $sth = null;
    }

    public function ChangeMobileCommunicationWithDate($user_id, $MobileCommunication, $date, $changer_id)
    {
        if (!is_numeric($user_id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($MobileCommunication)) {
            throw new Exception("Оклад не число");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT * FROM member_MobileCommunication where member_id=? and active=1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO' . $sth->errorInfo()[2]);
        };

        if (!$sth->execute([$user_id])) {
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }

        $lastdate = $sth->fetch()['date_s'];

        if (strtotime($lastdate) >  strtotime($date)) {
            throw new Exception("Изменение в выплате за мобильную связь. Последняя дата изменения была ".$lastdate.". Новая дата меньше последней даты изменения, что невозможно.");
        }

        $count = $sth->rowCount();
        if ($count > 0) {
            $query = "update member_MobileCommunication SET active=0, date_e=STR_TO_DATE(?, '%d.%m.%Y'), changer_id=? where member_id=? and active=1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$date, $changer_id, $user_id]);
            } catch (PDOException $e) {
                throw new Exception("Не могу выполнить зарос к БД" . $e->getMessage());
            }

            if ($sth->rowCount() == 0) {
                throw new Exception('Не могу обновить строки' . $sth->errorInfo()[2]);
            }
        };

        $query = "insert into member_MobileCommunication (member_id, date_s, MobileCommunication, changer_id) VALUES (?, STR_TO_DATE(?, '%d.%m.%Y'), ?, ?)";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$user_id, $date, $MobileCommunication, $changer_id]);
        } catch (PDOException $e) {
            throw new Exception('Не могу добавить.' . $e->getMessage());
        }

        $query = null;
        $sth = null;
    }

    /*
     * обновление базового процента
     * @param id
     * @param procent
     * @return true
     */
    public function ChangeBaseProcent($user_id, $procent, $changer_id)
    {
        if (!is_numeric($user_id)) {
            throw new Exception("Неверный id пользовтаеля");
        }

        if (!is_numeric($procent)) {
            throw new Exception("Ошибка в базовом проценте");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from member_percent where member_id=? and active=1";
        $sth = $this->dbh->prepare($query);
        try {
            $sth->execute([$user_id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $count = $sth->rowCount();

        if ($count > 0) {
            $query = "UPDATE member_percent SET active=0, date_e=NOW(), changer_id=? where member_id=? and active=1";
            $sth = $this->dbh->prepare($query);
            try {
                $sth->execute([$changer_id, $user_id]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }

            $query = "insert into member_percent (percent, active, member_id, date_s, changer_id) VALUES (?,1,?, NOW(),?);";
            $sth = $this->dbh->prepare($query);
            try {
                $sth->execute([$procent, $user_id, $changer_id]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }
        } else if ($count == 0) { // Если еше не заносили данных, то добавляю с data_s по умолчанию 0000-00-00 //////////// ИСПАВИТЬ В ПРОДАКШЕНЕ
            $query = "insert into member_percent (percent, active, member_id, date_s, changer_id) VALUES (?,1,?, NOW(),?);";
            $sth = $this->dbh->prepare($query);
            try {
                $sth->execute([$procent, $user_id, $changer_id]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }
        }

        $sth = null;
        return true;
    }

    public function ChangeBrandProcent($user_id, $procent, $changer_id)
    {
        if (!is_numeric($user_id)) {
            throw new Exception("Неверный id пользовтаеля");
        }

        if (!is_numeric($procent)) {
            throw new Exception("Ошибка в базовом проценте");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from member_brend_percent where member_id=? and active=1";
        $sth = $this->dbh->prepare($query);
        try {
            $sth->execute([$user_id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $count = $sth->rowCount();

        if ($count > 0) {
            $query = "UPDATE member_brend_percent SET active=0, date_e=NOW(), changer_id=? where member_id=? and active=1";
            $sth = $this->dbh->prepare($query);
            try {
                $sth->execute([$changer_id, $user_id]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }

            $query = "insert into member_brend_percent (percent, active, member_id, date_s, changer_id) VALUES (?,1,?, NOW(),?);";
            $sth = $this->dbh->prepare($query);
            try {
                $sth->execute([$procent, $user_id, $changer_id]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }
        } else if ($count == 0) { // Если еше не заносили данных, то добавляю с data_s по умолчанию 0000-00-00 //////////// ИСПАВИТЬ В ПРОДАКШЕНЕ
            $query = "insert into member_brend_percent (percent, active, member_id, date_s, changer_id) VALUES (?,1,?, NOW(),?);";
            $sth = $this->dbh->prepare($query);
            try {
                $sth->execute([$procent, $user_id, $changer_id]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }
        }

        $sth = null;
        return true;
    }

    public function ChangeBaseProcentWithDate($user_id, $procent, $date, $changer_id)
    {
        if (!is_numeric($user_id)) {
            throw new Exception("Неверный id пользовтаеля");
        }

        if (!is_numeric($procent)) {
            throw new Exception("Ошибка в базовом проценте");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from member_percent where member_id=? and active=1";
        $sth = $this->dbh->prepare($query);
        try {
            $sth->execute([$user_id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $lastdate = $sth->fetch()['date_s'];

        if (strtotime($lastdate) >  strtotime($date)) {
            throw new Exception("Изменение в базовом проценте. Последняя дата изменения была ".$lastdate.". Новая дата меньше последней даты изменения, что невозможно.");
        }

        $count = $sth->rowCount();

        if ($count > 0) {
            $query = "UPDATE member_percent SET active=0, date_e=STR_TO_DATE(?, '%d.%m.%Y'), changer_id=? where member_id=? and active=1";
            $sth = $this->dbh->prepare($query);
            try {
                $sth->execute([$date, $changer_id, $user_id]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }
        };

        $query = "insert into member_percent (percent, active, member_id, date_s, changer_id) VALUES (?,1,?, STR_TO_DATE(?,'%d.%m.%Y'),?);";
        $sth = $this->dbh->prepare($query);
        try {
            $sth->execute([$procent, $user_id, $date, $changer_id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
        return true;
    }

    /*
     * Изменение базового процента
     * @param id
     * @param procent
     * @return true
     */
    public function ChangeMotivation($user_id, $motivation_id, $changer_id)
    {
        if (!is_numeric($user_id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($motivation_id)) {
            throw new Exception("Неверный пол");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        // Ищем в таблице данные по пользователю
        $query = "select * from member_motivation where member_id=? and active=1";
        $sth = $this->dbh->prepare($query);
        try {
            $sth->execute([$user_id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        // Получаю количество строк
        $count = $sth->rowCount();

        // Если количество строк больше 0, то данные уже были занесены и их нужно обновить
        if ($count > 0) {
            // обновляю мотивацию в БД, выставляю активность в 0, дату окончания текущий день и указываем id пользователя
            $query = "UPDATE member_motivation SET active=0, date_e=NOW(), changer_id=? where member_id=? and active=1";
            // Подготавливаем запрос
            $sth = $this->dbh->prepare($query);
            try {
                $sth->execute([$changer_id, $user_id]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }

            // Добавляем данные
            $query = "insert into member_motivation (motivation_id, active, member_id, date_s, changer_id) VALUES (?,1,?,NOW(),?);";
            $sth = $this->dbh->prepare($query);
            try {
                $sth->execute([$motivation_id, $user_id, $changer_id]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }
        } else if ($count == 0) { // Если еше не заносили данных, то добавляю с data_s по умолчанию 0000-00-00 //////////// ИСПАВИТЬ В ПРОДАКШЕНЕ
            $query = "insert into member_motivation (motivation_id, active, member_id, changer_id, date_s) VALUES (?,1,?,?, NOW());";
            $sth = $this->dbh->prepare($query);
            try {
                $sth->execute([$motivation_id, $user_id, $changer_id]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }
        }


        $query = "UPDATE member_percent_advanced SET active=0, changer_id=?, date_e=NOW() WHERE member_id=? and active=1";
        $sth = $this->dbh->prepare($query);
        try {
            $sth->execute([$changer_id, $user_id]);
        } catch (PDOException $e) {
            throw new Exception("ChangeMotivation. Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
        return true;
    }

    public function ChangeMotivationWithDate($user_id, $motivation_id, $date, $changer_id)
    {
        if (!is_numeric($user_id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($motivation_id)) {
            throw new Exception("Неверный пол");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        // Ищем в таблице данные по пользователю
        $query = "select * from member_motivation where member_id=? and active=1";
        $sth = $this->dbh->prepare($query);
        try {
            $sth->execute([$user_id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        // Получаю количество строк
        $count = $sth->rowCount();

        // Если количество строк больше 0, то данные уже были занесены и их нужно обновить
        if ($count > 0) {
            // обновляю мотивацию в БД, выставляю активность в 0, дату окончания текущий день и указываем id пользователя
            $query = "UPDATE member_motivation SET active=0, date_e=STR_TO_DATE(?, '%d.%m.%Y'), changer_id=? where member_id=? and active=1";
            // Подготавливаем запрос
            $sth = $this->dbh->prepare($query);
            try {
                $sth->execute([$date, $changer_id, $user_id]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }
        };

        $query = "insert into member_motivation (motivation_id, active, member_id, date_s, changer_id) VALUES (?,1,?, STR_TO_DATE(?, '%d.%m.%Y'),?);";
        $sth = $this->dbh->prepare($query);
        try {
            $sth->execute([$motivation_id, $user_id, $date, $changer_id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
        return true;
    }

    /*
     * Смена пола
     * @param id
     * @param sex
     * @return true
     */
    public function ChangeSex($id, $sex)
    {
        if (!is_numeric($id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($sex)) {
            throw new Exception("Неверный пол");
        }

        $this->dbh = dbConnect();
        $query = "UPDATE member SET sex=? where id=?";

        $sth = $this->dbh->prepare($query);
        try {
            $sth->execute([$sex, $id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
        $this->dbh = null;
        return true;
    }

    /*
     * Смена национальности
     * @param nation_name
     * @param id
     * @return true
     */
    public function ChangeNation($id, $nation, $changer_id)
    {
        if (!is_numeric($id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($nation)) {
            throw new Exception("Неверный пол");
        }

        $this->dbh = dbConnect();
        $query = "UPDATE member_nation SET end_date=NOW(),active=0, changer_id=? where member_id=? and active=1";
//        $query = "UPDATE member_nation SET nation_id=? where id=?";

        $sth = $this->dbh->prepare($query);
        try {
            $sth->execute([$changer_id, $id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $query = "INSERT INTO member_nation (start_date,active,member_id,nation_id, changer_id) VALUES(NOW(),1,?,?,?)";
        $sth = $this->dbh->prepare($query);
        try {
            $sth->execute([$id, $nation, $changer_id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
        $this->dbh = null;
        return true;

    }

    /*
     * Смена даты рождения
     * @param id
     * @param sex
     * @return true
     */
    public function ChangeBirthday($id, $birthday)
    {
        if (!is_numeric($id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
//        if (!is_numeric($sex))  {
//            throw new Exception("Неверный пол");
//        }

        $this->dbh = dbConnect();
        $query = "UPDATE member SET birthday=STR_TO_DATE(?, '%d.%m.%Y') where id=?";

        $sth = $this->dbh->prepare($query);
        try {
            $sth->execute([$birthday, $id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
        $this->dbh = null;
        return true;
    }


    public function ChangeMaidenName($id, $name)
    {
        $this->dbh = dbConnect();
        $query = "UPDATE member SET maiden_name=? where id=?";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $e) {
            throw new Exception("Ошибка prepare" . $e->getMessage());
        }

        try {
            $sth->execute([$name, $id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
        $this->dbh = null;
        return true;
    }

    /*
     * Смена имени фамилии отчества
     */
    public function ChangeFullName($id, $arr, $changer_id)
    {
        if (!is_numeric($id)) {
            throw new Exception("Неверный id пользовтаеля");
        }

        if (strlen($arr['lastname']) == 0 || strlen($arr['name']) == 0 || strlen($arr['middle']) == 0) {
            throw new Exception("Неверный данные пользовтаеля");
        }

        if (strlen($arr['maiden']) == 0) {
            $arr['maiden'] = NULL;
        }

        $arr['lastname'] = trim($arr['lastname']);
        $arr['name'] = trim($arr['name']);
        $arr['middle'] = trim($arr['middle']);
        if ($arr['maiden'] != NULL) {
            $arr['maiden'] = trim($arr['maiden']);
        }

        $this->dbh = dbConnect();
        $query = "INSERT INTO member_name_archive (lastname,name,middle,maiden_name,date,member_id) SELECT member.lastname, member.name, member.middle, member.maiden_name, NOW() as date,member.id FROM member WHERE id = ?";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $e) {
            throw new Exception("Ошибка prepare" . $e->getMessage());
        }

        try {
            $sth->execute([$id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $last_id = $this->dbh->lastInsertId();

        $query = "UPDATE member_name_archive SET changer_id=? where id=?";
        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $e) {
            throw new Exception("Ошибка prepare" . $e->getMessage());
        }

        try {
            $sth->execute([$changer_id, $last_id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }


        $query = "UPDATE member SET lastname=?, name=?, middle=?, maiden_name=? where id=?";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $e) {
            throw new Exception("Ошибка prepare" . $e->getMessage());
        }

        try {
            $sth->execute([$arr['lastname'], $arr['name'], $arr['middle'], $arr['maiden'], $id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
        $this->dbh = null;
        return true;
    }

    /*
     * Смена login
     * @param id
     * @param login
     * @return true
     */
    public function ChangeLogin($id, $login)
    {
        if (!is_numeric($id)) {
            throw new Exception("Неверный id пользовтаеля");
        }

        $this->dbh = dbConnect();
        $query = "UPDATE member_work SET login=? where member_id=?";

        $sth = $this->dbh->prepare($query);

        try {
            $sth->execute([$login, $id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
        $this->dbh = null;
        return true;
    }

    /*
     * Смена worklocalphone
     * @param id
     * @param sklad
     * @return true
     */
    public function ChangeWLocalPhone($id, $phone, $changer_id)
    {
        if (!is_numeric($id)) {
            throw new Exception("Неверный id пользовтаеля");
        }

        if (!is_numeric($phone) && strlen($phone) > 0) {
            throw new Exception("Неверный номер телефона");
        }

        if (strlen($phone) == 0) {
            $phone = NULL;
        }

        $this->dbh = dbConnect();


        $query = "UPDATE member_work_local_phone SET active=0, changer_id=? where member_id=?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$changer_id, $id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $query = "INSERT INTO member_work_local_phone(phone,date,active,member_id, changer_id) VALUES (?,NOW(),1,?,?)";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$phone, $id, $changer_id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
        $this->dbh = null;
        return true;
    }

    /*
     * Смена skladname
     * @param id
     * @param sklad
     * @return true
     */
    public function ChangeSklad($id, $sklad)
    {
        if (!is_numeric($id)) {
            throw new Exception("Неверный id пользовтаеля");
        }

        $this->dbh = dbConnect();
        $query = "UPDATE member_work SET 1CSklad_name=? where member_id=?";

        $sth = $this->dbh->prepare($query);

        try {
            $sth->execute([$sklad, $id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
        $this->dbh = null;
        return true;
    }

    /*
    * Смена zik
    * @param id
    * @param number
    * @return true
    */
    public function ChangeZik($id, $number)
    {
        if (!is_numeric($id)) {
            throw new Exception("Неверный id пользовтаеля");
        }

        $this->dbh = dbConnect();
        $query = "UPDATE member_work SET personnel_number=? where member_id=?";

        $sth = $this->dbh->prepare($query);

        try {
            $sth->execute([$number, $id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
        $this->dbh = null;
        return true;
    }

    /*
    * Смена метки увольнения
    * @param id
    * @param метка
    * @return true
    */
    public function ChangeDismissalMark($id, $value)
    {
        if (!is_numeric($id)) {
            throw new Exception("Неверный id пользовтаеля");
        }

        if (!is_numeric($value)) {
            throw new Exception("Неверное значение флага");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "UPDATE member SET dismissal_mark=? where id=?";

        $sth = $this->dbh->prepare($query);

        try {
            $sth->execute([$value, $id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
        $this->dbh = null;
        return true;
    }


    /*
     * Удаляет из БД почту
     * Один телефон одному сотруднику, не пакетная обработка
     * @param $phone телефнон
     * @param $member_id ID пользователя
     * @throws Не верный id пользователя
     * @throws Ошибка добавления в БД
     * @return bool true при успешном выполнении кода.
     */
    public function DeleteEmail($id, $member_id, $changer_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверный ID сотрудника: ' . $member_id);
        }
//        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) { // Проводим валидацию почтового адреса
//            throw new Exception('Неверный email: '.$member_id);
//        }

        /*
         * инициализация статичных переменных
         */
        try {
            $this->dbh = dbConnect(); // подключаемся к серверу
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }
        $query = "UPDATE member_mails SET active=0, changer_id=? where id=? AND member_id=?";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$changer_id, $id, $member_id])) { // Выполняю запрос
            $error = $sth->errorInfo();
            throw new Exception('Возника ошибка при обновлении в БД для пользователя с ID: ' . $member_id . ' <BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }

    /*
     * Удаляет из БД личный(персональный) телефон.
     * Один телефон одному сотруднику, не пакетная обработка
     * @param $phone телефнон
     * @param $member_id ID пользователя
     * @throws Не верный id пользователя
     * @throws Ошибка добавления в БД
     * @return bool true при успешном выполнении кода.
     */
    public function DeletePersonalPhone($phone, $member_id, $changer_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверный ID сотрудника: ' . $member_id);
        }

        if (!is_numeric($phone)) { //Проверяем что это число
            throw new Exception('Неверный номер телефона: ' . $phone);
        }

        if (!preg_match("/^[0-9]{10,10}+$/", $phone)) {  // Должно быть 10 цифр
            throw new Exception('Неверный формат телефона: ' . $phone);
        }

        /*
         * инициализация статичных переменных
         */
        try {
            $this->dbh = dbConnect(); // подключаемся к серверу
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }
        $query = "UPDATE member_personal_phones SET active=0,changer_id=? where  is_mobile=0 AND phone=? AND member_id=?";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$changer_id, $phone, $member_id])) { // Выполняю запрос
            $error = $sth->errorInfo();
            throw new Exception('Возника ошибка при обновлении в БД для пользователя с ID: ' . $member_id . ' <BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }

    /*
     * Удаляет из БД личный(персональный) мобильный телефон.
     * Один телефон одному сотруднику, не пакетная обработка
     * @param $phone телефнон
     * @param $member_id ID пользователя
     * @throws Не верный id пользователя
     * @throws Ошибка добавления в БД
     * @return bool true при успешном выполнении кода.
     */
    public function DeletePersonalMobilePhone($phone, $member_id, $changer_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверный ID сотрудника: ' . $member_id);
        }

        if (!is_numeric($phone)) { //Проверяем что это число
            throw new Exception('Неверный номер телефона: ' . $phone);
        }

        if (!preg_match("/^[0-9]{10,10}+$/", $phone)) {  // Должно быть 10 цифр
            throw new Exception('Неверный формат телефона: ' . $phone);
        }

        /*
         * инициализация статичных переменных
         */
        try {
            $this->dbh = dbConnect(); // подключаемся к серверу
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }
        $query = "UPDATE member_personal_phones SET active=0, changer_id=? where is_mobile=1 AND phone=? AND member_id=?";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$changer_id, $phone, $member_id])) { // Выполняю запрос
            $error = $sth->errorInfo();
            throw new Exception('Возника ошибка при обновлении в БД для пользователя с ID: ' . $member_id . ' <BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }

    /*
     * Удаляет из БД рабочий мобильный телефон.
     * Один телефон одному сотруднику, не пакетная обработка
     * @param $phone телефнон
     * @param $member_id ID пользователя
     * @throws Не верный id пользователя
     * @throws Ошибка добавления в БД
     * @return bool true при успешном выполнении кода.
     */
    public function DeleteMobileWorkPhone($phone, $member_id, $changer_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверный ID сотрудника: ' . $member_id);
        }

        if (!is_numeric($phone)) { //Проверяем что это число
            throw new Exception('Неверный номер телефона: ' . $phone);
        }

        if (!preg_match("/^[0-9]{10,10}+$/", $phone)) {  // Должно быть 10 цифр
            throw new Exception('Неверный формат телефона: ' . $phone);
        }

        /*
         * инициализация статичных переменных
         */
        try {
            $this->dbh = dbConnect(); // подключаемся к серверу
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }
        $query = "UPDATE member_work_phones SET active=0, changer_id=? where is_mobile=1 AND phone=? AND member_id=?";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$changer_id, $phone, $member_id])) { // Выполняю запрос
            $error = $sth->errorInfo();
            throw new Exception('Возника ошибка при обновлении в БД для пользователя с ID: ' . $member_id . ' <BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }

    /*
     * Удаляет из БД рабочий телефон.
     * Один телефон одному сотруднику, не пакетная обработка
     * @param $phone телефнон
     * @param $member_id ID пользователя
     * @throws Не верный id пользователя
     * @throws Ошибка добавления в БД
     * @return bool true при успешном выполнении кода.
     */
    public function DeleteWorkPhone($phone, $member_id, $changer_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверный ID сотрудника: ' . $member_id);
        }

        if (!is_numeric($phone)) { //Проверяем что это число
            throw new Exception('Неверный номер телефона: ' . $phone);
        }

        if (!preg_match("/^[0-9]{10,10}+$/", $phone)) {  // Должно быть 10 цифр
            throw new Exception('Неверный формат телефона: ' . $phone);
        }

        /*
         * инициализация статичных переменных
         */
        try {
            $this->dbh = dbConnect(); // подключаемся к серверу
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }
        $query = "UPDATE member_work_phones SET active=0, changer_id=? where  is_mobile=0 AND phone=? AND member_id=?";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$changer_id, $phone, $member_id])) { // Выполняю запрос
            $error = $sth->errorInfo();
            throw new Exception('Возника ошибка при обновлении в БД для пользователя с ID: ' . $member_id . ' <BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }

    /*
     * Удаляет из БД аварийный телефон.
     * Один телефон одному сотруднику, не пакетная обработка
     * @param $phone телефнон
     * @param $member_id ID пользователя
     * @throws Не верный id пользователя
     * @throws Ошибка добавления в БД
     * @return bool true при успешном выполнении кода.
     */
    public function DeleteEmergencyPhone($phone, $member_id, $changer_id)
    {
        if ($member_id == null || !is_numeric($member_id)) { // проверяем что id число и не null
            throw new Exception('Неверный ID сотрудника: ' . $member_id);
        }

        if (!is_numeric($phone)) { //Проверяем что это число
            throw new Exception('Неверный номер телефона: ' . $phone);
        }

        if (!preg_match("/^[0-9]{10,10}+$/", $phone)) {  // Должно быть 10 цифр
            throw new Exception('Неверный формат телефона: ' . $phone);
        }

        /*
         * инициализация статичных переменных
         */
        try {
            $this->dbh = dbConnect(); // подключаемся к серверу
        } catch (PDOException $exception) {
            throw new Exception('Can\'t connect to db: ' . $exception->getMessage());
        }
        $query = "UPDATE member_personal_emergency_phones SET active=0, changer_id=? where phone=? AND member_id=?";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$changer_id, $phone, $member_id])) { // Выполняю запрос
            $error = $sth->errorInfo();
            throw new Exception('Возника ошибка при обновлении в БД для пользователя с ID: ' . $member_id . ' <BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        /*
         * Очистка неиспользуемых переменных
         */
        $query = null;
        $sth = null;
        $this->dbh = null;

        return true;
    }


    /*
     * Увольнение пользователя
     * @param id пользоветля
     * @param reason_id - id причины увольнения
     */
    public function LeavingByID($id, $reason_id, $date, $note, $changer_id)
    {
        if (!is_numeric($id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($reason_id) || ($reason_id == 0)) {
            throw new Exception("Неверная причина увольнения");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if (strtotime($this->GetDateForLeaving($id)) > strtotime($date)) {
            throw new Exception("Пользователь заведен датой раньше чем пытаемся уволить. ID: " . $id);
        }

        $query = "UPDATE member SET dismissal_date=STR_TO_DATE(?, '%d.%m.%Y'),reasons_for_leaving_id=? where id=?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$date, $reason_id, $id]);
        } catch (Exception $e) {
            echo $e->getMessage() . "<BR>";
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        // Отключаем внутренний рабочий телефон
        $query = "UPDATE member_work_local_phone SET active=0, changer_id=? where member_id=?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$changer_id, $id]);
        } catch (PDOException $e) {
            echo $e->getMessage() . "<BR>";
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $query = "UPDATE member_work SET login=NULL, 1CSklad_name=NULL, personnel_number=NULL, date=STR_TO_DATE(?, '%d.%m.%Y') where member_id=?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$date, $id]);
        } catch (PDOException $e) {
            echo $e->getMessage() . "<BR>";
            throw new Exception("Ошибка в SQL запросе LeavingByID" . $e->getMessage());
        }

        // Выставляю workstatus
        $this->ChangeWorkStatusWithDate($id, $note, 4, $changer_id, $date);

        $sth = null;

        return true;
    }

    /*
     * Обновление финансовой ведомости
     * при изменении рабочего статуса сотрудника
     */
    public function checkPayout($id, $date) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from report_status where department_id = (SELECT department_id from member_jobpost where member_id = ? and active = 1) and status_id < 4 and active = 1";

        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
        $date = DateTime::createFromFormat('d.m.Y', $date);

        foreach ($raw as $item) {
            $date_vedomost = DateTime::createFromFormat("Y-m-d", $item['date']);
            switch ($item['type_of_payment']) {
                case 1:
                    $date_vedomost->modify("+5 day"); // потому что день 00 и ставит месяц предыдущий
                    $date_vedomost->modify("+1 month"); // выдача в следующем месяце
                    break;
                case 2:
                    $date_vedomost->modify("+20 day");
                    break;
                default:
                    continue 2;
                    break;
            }

            if ($date_vedomost > $date) {
                $query = "UPDATE financial_payout set active = 0 where member_id = ? and date = ? and type_of_payment = ?";
                try {
                    $sth = $this->dbh->prepare($query);
                    $sth->execute([$id, $item['date'], $item['type_of_payment']]);
                } catch (PDOException $e) {
                    throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
                }
            } else {
                continue;
            }
        }

        $sth = null;
    }

    public function disableInPayout($member_id, $date, $type_of_payment) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "UPDATE financial_payout set active = 0 where member_id = ? and date = STR_TO_DATE(?, '%m.%Y') and type_of_payment = ?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$member_id, $date, $type_of_payment]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
    }

    public function enableInPayout($member_id, $date, $type_of_payment) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "UPDATE financial_payout set active = 1 where member_id = ? and date = STR_TO_DATE(?, '%m.%Y') and type_of_payment = ?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$member_id, $date, $type_of_payment]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        $sth = null;
    }

    /*
     * Изменение рабочего статуса у пользователя
     * @param id пользоветля
     * @param note - заметка
     * @param status - статус
     */
    public function ChangeWorkStatus($id, $note, $status, $changer_id, $date=NULL)
    {
        if (!is_numeric($id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($status) || ($status == 0)) {
            throw new Exception("Неверный статус");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        // Выставляю workstatus
        $query = "select * from member_workstatus where active=1 AND member_id=?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            try {
                if (is_null($date)) {
                    $query = "UPDATE member_workstatus SET active=0, date_e=NOW(), changer_id=? where member_id=?";
                    $sth = $this->dbh->prepare($query);
                    $sth->execute([$changer_id, $id]);

                    $query = "INSERT INTO member_workstatus(member_id, date_s, status, note, active, changer_id) VALUES (?,NOW(),?,?,?,?)";
                    $sth = $this->dbh->prepare($query);
                    $sth->execute([$id, $status, $note, 1, $changer_id]);
                } else {
                    $query = "UPDATE member_workstatus SET active=0, date_e=STR_TO_DATE(?, '%d.%m.%Y'), changer_id=? where member_id=?";
                    $sth = $this->dbh->prepare($query);
                    $sth->execute([$date, $changer_id, $id]);

                    $query = "INSERT INTO member_workstatus(member_id, date_s, status, note, active, changer_id) VALUES (?,STR_TO_DATE(?, '%d.%m.%Y'),?,?,?,?)";
                    $sth = $this->dbh->prepare($query);
                    $sth->execute([$id, $date, $status, $note, 1, $changer_id]);
                }
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }
        } else {
            try {
            if (is_null($date)) {
                $query = "INSERT INTO member_workstatus(member_id, date_s, status, note, active, changer_id) VALUES (?,NOW(),?,?,1,?)";
                $sth = $this->dbh->prepare($query);
                $sth->execute([$id, $status, $note, $changer_id]);
            } else {
                $query = "INSERT INTO member_workstatus(member_id, date_s, status, note, active, changer_id) VALUES (?,STR_TO_DATE(?, '%d.%m.%Y'),?,?,1,?)";
                $sth = $this->dbh->prepare($query);
                $sth->execute([$id, $date, $status, $note, $changer_id]);
            }

            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }
        }

        $query = "UPDATE member SET dismissal_date='9999-01-01' where id = ?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

//        $query = "UPDATE financial_payout SET active=1 where member_id=?";
//        try {
//            $sth = $this->dbh->prepare($query);
//            $sth->execute([$id]);
//        } catch (PDOException $e) {
//            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
//        }

        $sth = null;
        return true;
    }

    public function ChangeWorkStatusWithDate($id, $note, $status, $changer_id, $date)
    {
        if (!is_numeric($id)) {
            throw new Exception("Неверный id пользовтаеля");
        }
        if (!is_numeric($status) || ($status == 0)) {
            throw new Exception("Неверный статус");
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        // Выставляю workstatus
        $query = "select * from member_workstatus where active=1 AND member_id=?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$id]);
        } catch (PDOException $e) {
            throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $query = "UPDATE member_workstatus SET active=0, date_e=STR_TO_DATE(?,'%d.%m.%Y'), changer_id=? where member_id=? AND active=1";
            try {
                $sth = $this->dbh->prepare($query);
                $sth->execute([$date, $changer_id, $id]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }

            $query = "INSERT INTO member_workstatus(member_id, date_s, status, note, active, changer_id) VALUES (?,STR_TO_DATE(?,'%d.%m.%Y'),?,?,?,?)";
            try {
                $sth = $this->dbh->prepare($query);
                $sth->execute([$id, $date, $status, $note, 1, $changer_id]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }
        } else {
            $query = "INSERT INTO member_workstatus(member_id, date_s, status, note, active, changer_id) VALUES (?,STR_TO_DATE(?,'%d.%m.%Y'),?,?,1,?)";
            try {
                $sth = $this->dbh->prepare($query);
                $sth->execute([$id, $date, $status, $note, $changer_id]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в SQL запросе" . $e->getMessage());
            }
        }

        $sth = null;
        return true;
    }

    /*
     * Максимальная дата
     */
    public function GetDateForLeaving($id)
    {
        $dbh = dbConnect();

        $query = "select DATE_FORMAT(employment_date,'%d.%m.%Y') as date from member where id=? LIMIT 1";
        $sth = $dbh->prepare($query);
        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };
        $sth->execute([$id]);
        $m = $sth->fetch()['date'];

//        $query = "select DATE_FORMAT(start_date,'%d.%m.%Y') as date from member_department where current=1 AND member_id=? LIMIT 1";
//        $sth = $dbh->prepare($query);
//        if (!$sth) {
//            throw new Exception('Ошибка в PDO');
//        };
//        $sth->execute([$id]);
//        $d = $sth->fetch()['date'];

//        $query = "select DATE_FORMAT(start_date,'%d.%m.%Y') as date from member_direction where current=1 AND member_id=? LIMIT 1";
//        $sth = $dbh->prepare($query);
//        if (!$sth) {
//            throw new Exception('Ошибка в PDO');
//        };
//        $sth->execute([$id]);
//        $dr = $sth->fetch()['date'];

//        $query = "select DATE_FORMAT(start_date,'%d.%m.%Y') as date from member_position where current=1 AND member_id=? LIMIT 1";
//        $sth = $dbh->prepare($query);
//        if (!$sth) {
//            throw new Exception('Ошибка в PDO');
//        };
//        $sth->execute([$id]);
//        $p = $sth->fetch()['date'];

        $sth = null;
        $dbh = null;

        return $m;
    }

    /*
     * Заполнение полей массива объектов this->memberList
     */
    private function setMemberValue($rawresult, $all_data=1) {
        $this->membersList = array();
        foreach ($rawresult as $value) {
            $Member = new Member();
            $Member->setAll($value);

            $query_subquery = "select t1.*,CONCAT(t2.lastname, ' ', t2.name) as changer_name from vacation_extension as t1
            LEFT JOIN member as t2 ON (t1.changer_id = t2.id)
            where member_id=?";
            try {
                $sth_sub = $this->dbh->prepare($query_subquery);
                $sth_sub->execute([$value['id']]);
            } catch (Exception $e) {
                throw new Exception("Ошибка в получении направления." . $e->getMessage());
            }
            $raw = $sth_sub->fetchAll(PDO::FETCH_ASSOC);
//            $aReturn = [];
//            foreach ($raw as $item) {
//                $ext_date = explode("-", $item['vacation_extension_date']);
//                $aReturn[$ext_date[0]] = $item;
//            }
            $Member->setVacationExtensionDate($raw);

            /*
            * Получаю текущеее направление, отдел, должность
            */
            $query_subquery = "select 
            t1.direction_id, t2.name as direction_name, 
            t1.department_id,t3.name as department_name,
            t1.position_id,t4.name as position_name,
            DATE_FORMAT(t1.date_s,'%d.%m.%Y') as date
            from member_jobpost as t1 
            LEFT JOIN direction as t2 ON(t1.direction_id = t2.id)
            LEFT JOIN department as t3 ON(t1.department_id = t3.id)
            LEFT JOIN position as t4 ON(t1.position_id = t4.id)
            where member_id=? and t1.active=1 LIMIT 1";

            try {
                $sth_sub = $this->dbh->prepare($query_subquery);
                $sth_sub->execute([$value['id']]);
            } catch (Exception $e) {
                throw new Exception("Ошибка в получении направления." . $e->getMessage());
            }
            $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

            //Направление
            $direction['id'] = $raw['direction_id'];
            $direction['name'] = $raw['direction_name'];
            $direction['date'] = $raw['date'];

            // Отдел
            $department['id'] = $raw['department_id'];
            $department['name'] = $raw['department_name'];
            $department['date'] = $raw['date'];

            // Должность
            $position['id'] = $raw['position_id'];
            $position['name'] = $raw['position_name'];
            $position['date'] = $raw['date'];

            $Member->setDepartment($department);
            $Member->setDirection($direction);
            $Member->setPosition($position);

            if ($all_data==1) {
                /*
                 * probation
                 */
                $query_subquery = "select note from probation where member_id=?";
                try {
                    $sth_sub = $this->dbh->prepare($query_subquery);
                    $sth_sub->execute([$value['id']]);
                } catch (Exception $e) {
                    throw new Exception("Ошибка в получении рабочих телефонов." . $e->getMessage());
                }
                $probation = $sth_sub->fetch();
                if ($probation)
                    $Member->setProbation($probation[0]);

                /*
                * получаю рабочий телефон и мобильный рабочий номер
                */
                $query_subquery = "select phone,is_mobile from member_work_phones where active=1 and member_id=?";
                try {
                    $sth_sub = $this->dbh->prepare($query_subquery);
                    $sth_sub->execute([$value['id']]);
                } catch (Exception $e) {
                    throw new Exception("Ошибка в получении рабочих телефонов." . $e->getMessage());
                }
                $wp = array(); // work phone получаю массив рабочих телефонов
                $wm = array(); // work mobile получаю массив рабочих мобильных телефонов
                foreach ($sth_sub->fetchAll(PDO::FETCH_ASSOC) as $item) {
                    if ($item['is_mobile'] == 0) {
                        array_push($wp, $item);
                    } else {
                        array_push($wm, $item);
                    }
                }
                $Member->setWorkphone($wp);
                $Member->setWorkmobilephone($wm);

                /*
                * получаю добавочный рабочий номер
                */
                $query_subquery = "select phone from member_work_local_phone where active=1 and member_id=?";
                try {
                    $sth_sub = $this->dbh->prepare($query_subquery);
                    $sth_sub->execute([$value['id']]);
                } catch (Exception $e) {
                    throw new Exception("Ошибка в получении рабочих телефонов." . $e->getMessage());
                }
                $Member->setWorklocalphone($sth_sub->fetch(PDO::FETCH_ASSOC));

                /*
                * получаю личные номера телефона и мобильного
                */
                $query_subquery = "select phone,is_mobile from member_personal_phones where active=1 and member_id=?";
                try {
                    $sth_sub = $this->dbh->prepare($query_subquery);
                    $sth_sub->execute([$value['id']]);
                } catch (Exception $e) {
                    throw new Exception("Ошибка в получении личных телефонов." . $e->getMessage());
                }
                $pp = array(); // personal phone получаю массив личных телефонов
                $pm = array(); // persoanl mobile получаю массив личных мобильных телефонов
                foreach ($sth_sub->fetchAll(PDO::FETCH_ASSOC) as $item) {
                    if ($item['is_mobile'] == 0) {
                        array_push($pp, $item);
                    } else {
                        array_push($pm, $item);
                    }
                }
                $Member->setPersonalphone($pp);
                $Member->setPersonalmobilephone($pm);

                /*
                 * Добавляю почту
                 */
                $query_subquery = "select id,email,note from member_mails where active=1 and member_id=?";
                try {
                    $sth_sub = $this->dbh->prepare($query_subquery);
                    $sth_sub->execute([$value['id']]);
                } catch (Exception $e) {
                    throw new Exception("Ошибка в получении направления." . $e->getMessage());
                }

                if ($sth_sub->rowCount() > 0) {
                    $Member->setEmails($sth_sub->fetchAll(PDO::FETCH_ASSOC));
                }

                /*
                * получаю аварийные телефоны
                 */
                $query_subquery = "select phone,note from member_personal_emergency_phones where active=1 and member_id=?";
                try {
                    $sth_sub = $this->dbh->prepare($query_subquery);
                    $sth_sub->execute([$value['id']]);
                } catch (Exception $e) {
                    throw new Exception("Ошибка в получении аварийных телефонов." . $e->getMessage());
                }
                $Member->setEmergencyphone($sth_sub->fetchAll(PDO::FETCH_ASSOC));


                /*
                 * Получаю причину увольнения
                 *
                 */

                $query_subquery = "SELECT t1.id, t1.reasons_for_leaving_id, t2.name as reason_name,t3.note
            FROM member as t1
            LEFT JOIN reasons_for_leaving as t2 ON (t1.reasons_for_leaving_id = t2.id)
            LEFT JOIN member_workstatus as t3 ON (t1.id=t3.member_id and t3.status=4 and t3.active=1)
            WHERE
            t1.id=?";
                try {
                    $sth_sub = $this->dbh->prepare($query_subquery);
                    $sth_sub->execute([$value['id']]);
                } catch (Exception $e) {
                    throw new Exception("Ошибка в получении причины увольнения." . $e->getMessage());
                }
                $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);
                if ($raw) {
                    $Member->setReasonLeaving($raw['reason_name']);
                    $Member->setReasonLeavingId($raw['reasons_for_leaving_id']);
                    $Member->setReasonLeavingNote($raw['note']);
                }

                /*
                 * Получаю рабочие данные
                */
                $query_subquery = "select * from member_work where active=1 and member_id=?";
                try {
                    $sth_sub = $this->dbh->prepare($query_subquery);
                    $sth_sub->execute([$value['id']]);
                } catch (Exception $e) {
                    throw new Exception("Ошибка в получении аварийных телефонов." . $e->getMessage());
                }
                $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);
                $Member->setLogin($raw['login']);
                $Member->setSkladName($raw['1CSklad_name']);
                $Member->setPersonalNumber($raw['personnel_number']);

                /*
                 * Названии нации
                 *
                 */
                $query_subquery = "SELECT t1.nation_id,t2.name FROM member_nation as t1 JOIN nation as t2 ON (t1.nation_id = t2.id) where t1.member_id=? and t1.active=1 LIMIT 1";
                try {
                    $sth_sub = $this->dbh->prepare($query_subquery);
                    $sth_sub->execute([$value['id']]);
                } catch (Exception $e) {
                    throw new Exception("Ошибка в получении причины увольнения." . $e->getMessage());
                }
                $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

                $Member->setNationName($raw['name']);
                $Member->setNation($raw['nation_id']);

                /* Рабочий статус */
                $query_subquery = "select status,note,DATE_FORMAT(date_s, '%d.%m.%Y') as date_s from member_workstatus where member_id=? and active=1";
                try {
                    $sth_sub = $this->dbh->prepare($query_subquery);
                    $sth_sub->execute([$value['id']]);
                } catch (Exception $e) {
                    throw new Exception("Ошибка получения данных. " . $e->getMessage());
                }

                if ($sth_sub->rowCount() > 1)
                    throw new Exception("Много активных записей в базе");

                $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

                if (($raw) && ($sth_sub->rowCount() == 1)) {
                    $Member->setWorkstatus($raw['status']);
                    $Member->setWorkstatusDate($raw['date_s']);
                    $Member->setWorkstatusNote($raw['note']);
                };
            }

            /* Базовый процент */

            $query_subquery = "select percent from member_percent where member_id=? and active=1";
            try {
                $sth_sub = $this->dbh->prepare($query_subquery);
                $sth_sub->execute([$value['id']]);
            } catch (Exception $e) {
                throw new Exception("Ошибка получения данных. " . $e->getMessage());
            }
            $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);
            if ($raw) {
                $Member->setBasePercent($raw['percent']);
            }

            $query_subquery = "select percent from member_brend_percent where member_id=? and active=1";
            try {
                $sth_sub = $this->dbh->prepare($query_subquery);
                $sth_sub->execute([$value['id']]);
            } catch (Exception $e) {
                throw new Exception("Ошибка получения данных. " . $e->getMessage());
            }
            $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);
            if ($raw) {
                $Member->setBrandpercent($raw['percent']);
            }

            /* мотивация процент */

            // Получаю текущую мотивацию пользователя!!!!
            $query_subquery = "select motivation_id from member_motivation where member_id=? and active=1";
            try {
                $sth_sub = $this->dbh->prepare($query_subquery);
                $sth_sub->execute([$value['id']]);
            } catch (Exception $e) {
                throw new Exception("Ошибка получения данных. " . $e->getMessage());
            }
            $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

            if (($raw) && ($sth_sub->rowCount() > 0)) {
                $Member->setMotivation($raw['motivation_id']);
            } else {
                $Member->setMotivation(-1);
            }

            /*Оклад*/
            $query_subquery = "select salary from member_salary where member_id=? and active=1";
            try {
                $sth_sub = $this->dbh->prepare($query_subquery);
                $sth_sub->execute([$value['id']]);
            } catch (Exception $e) {
                throw new Exception("Ошибка получения данных. " . $e->getMessage());
            }
            $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

            if (($raw) && ($sth_sub->rowCount() > 0)) {
                $Member->setSalary($raw['salary']);
            } else {
                $Member->setSalary(0);
            }

            /* Административная премия */
            $query_subquery = "select AdministrativePrize,Note from member_AdministrativePrize where member_id=? and active=1";
            try {
                $sth_sub = $this->dbh->prepare($query_subquery);
                $sth_sub->execute([$value['id']]);
            } catch (Exception $e) {
                throw new Exception("Ошибка получения данных. " . $e->getMessage());
            }
            $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

            if (($raw) && ($sth_sub->rowCount() > 0)) {
                $Member->setAdministrativePrize($raw['AdministrativePrize']);
                $Member->setAdministrativePrizeNote($raw['Note']);
            } else {
                $Member->setAdministrativePrize(0);
            }

            /* Мобильная связь */
            $query_subquery = "select MobileCommunication, formula from member_MobileCommunication where member_id=? and active=1";
            try {
                $sth_sub = $this->dbh->prepare($query_subquery);
                $sth_sub->execute([$value['id']]);
            } catch (Exception $e) {
                throw new Exception("Ошибка получения данных. " . $e->getMessage());
            }
            $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

            if (($raw) && ($sth_sub->rowCount() > 0)) {
                $Member->setMobileCommunication($raw['MobileCommunication']);
                $Member->setMobileFormula($raw['formula']);
            } else {
                $Member->setMobileCommunication(null);
                $Member->setMobileFormula(0);
            }

            /* Мобильная связь */
            $query_subquery = "select summ, formula from member_Transport where member_id=? and active=1";
            try {
                $sth_sub = $this->dbh->prepare($query_subquery);
                $sth_sub->execute([$value['id']]);
            } catch (Exception $e) {
                throw new Exception("Ошибка получения данных. " . $e->getMessage());
            }
            $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

            if (($raw) && ($sth_sub->rowCount() > 0)) {
                $Member->setTransport($raw['summ']);
                $Member->setTransportFormula($raw['formula']);
            } else {
                $Member->setTransport(null);
                $Member->setTransportFormula(0);
            }

            /*
            * Проверяю может ли получать аванс данный сотрудник
            */
            $query_subquery = "select * from member_avans where member_id=? and active=1 order by id desc LIMIT 1";
            try {
                $sth_sub = $this->dbh->prepare($query_subquery);
                $sth_sub->execute([$value['id']]);
            } catch (Exception $e) {
                throw new Exception("Ошибка получения данных. " . $e->getMessage());
            }
            $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

            if (($raw) && ($sth_sub->rowCount() > 0)) {
                $Member->setAvansPay($raw['bool']);
            };

            /*
         * Получаю процент от оклада для аванса
         */
            $query_subquery = "select * from member_procent_avans where member_id=? and active=1 order by id desc LIMIT 1";
            try {
                $sth_sub = $this->dbh->prepare($query_subquery);
                $sth_sub->execute([$value['id']]);
            } catch (Exception $e) {
                throw new Exception("Ошибка получения данных. " . $e->getMessage());
            }
            $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

            if (($raw) && ($sth_sub->rowCount() > 0)) {
                $Member->setAvansProcent($raw['procent']);
            };

            /*
         * Очисления в годовой бонус
         */
            $query_subquery = "select summ,type,note from member_yearbonus_pay where member_id=? and active=1";
            try {
                $sth_sub = $this->dbh->prepare($query_subquery);
                $sth_sub->execute([$value['id']]);
            } catch (Exception $e) {
                throw new Exception("Ошибка получения данных. " . $e->getMessage());
            }
            $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

            if (($raw) && ($sth_sub->rowCount() > 0)) {
                $Member->setYearbonusPay($raw['summ']);
                $Member->setYearbonusType($raw['type']);
                $Member->setYearbonusNote($raw['note']);
            }

            $query_subquery = null;
            $sth_sub = null;

            array_push($this->membersList, $Member);
        }
    }

    /*
     * Получает массив объектов пользователей
     * с данными с БД
     * @return true при успешном выполенении.
     * select member.*,member_department.department_id,member_direction.direction_id,member_position.position_id from member,member_department,member_direction,member_position where member.id=1 AND member.id=member_department.member_id=member_direction.member_id=member_position.member_id
     * Нужен ли массив объектов???
     * $all_members - Выборка всех пользователей или без уволенных
     * $emp_date - Выбрать пользователей, которые приняты на работу не позже data + 1 месяц
     * Например, строим ведомость за 02.2019, значит сотрудник может быть принят до 03.2019.
     */
    public function fetchMembers($all_members = true, $emp_date = null, $sortbyname = false)
    {
        $this->dbh = dbConnect();
        if ($all_members) {
            $query = "select *,DATE_FORMAT(employment_date,'%d.%m.%Y') AS employment_date, DATE_FORMAT(birthday,'%d.%m.%Y') AS birthday,DATE_FORMAT(dismissal_date,'%d.%m.%Y') AS dismissal_date 
            from member";
            if (!is_null($emp_date)) {
                $time = strtotime("01." . $emp_date);
                $final = date("d.m.Y", strtotime("+1 month", $time));
                $query .= " where employment_date< STR_TO_DATE('" . $final . "', '%d.%m.%Y')";
            }
        } else {
            $query = "select *,DATE_FORMAT(employment_date,'%d.%m.%Y') AS employment_date, DATE_FORMAT(birthday,'%d.%m.%Y') AS birthday,DATE_FORMAT(dismissal_date,'%d.%m.%Y') AS dismissal_date
            from member
            where dismissal_date = '9999-01-01'";
            if (!is_null($emp_date)) {
                $time = strtotime("01." . $emp_date);
                $final = date("d.m.Y", strtotime("+1 month", $time));
                $query .= " AND employment_date< STR_TO_DATE('" . $final . "', '%d.%m.%Y')";

            }
        }

        if ($sortbyname) {
            $query .= " order by lastname, name, middle";
        }

        $sth = $this->dbh->prepare($query);

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute();

        if ($sth->rowCount()==0) {
            throw new Exception("Нет данных из БД.");
        }

        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $this->setMemberValue($rawresult);

        return true;
    }

    /*
     * Список пользователей по отделу из Фикс Ведомости
     */
    public function fetchMembersbyDepartment($department_id){
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        $this->membersList = [];

        if (is_array($department_id)) {
            $in  = str_repeat('?,', count($department_id) - 1) . '?';
        }

        if (is_array($department_id)) {
            $query = "SELECT * FROM member where id IN (select member_id from member_jobpost where active=1 and department_id IN (".$in."))";
        } else {
            $query = "SELECT * FROM member where id IN (select member_id from member_jobpost where active=1 and department_id=?)";
        }
        try {
            $sth = $this->dbh->prepare($query);
            if (is_array($department_id)) {
                $sth->execute($department_id);
            } else {
                $sth->execute([$department_id]);
            }
        } catch (Exception $e) {
            echo $e->getMessage()."<BR>";
        }

        if ($sth->rowCount()==0) {
            throw new Exception("Нет данных из БД.");
        }


        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);
        $this->setMemberValue($rawresult);

        return true;
    }

    /*
     * Используется в финансовой ведоимости
     */
    public function fetchMembersFinancialbyDepartment($d, $type_of_payment, $department_id){
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        $this->membersList = [];

        if (is_array($department_id))
            $in  = str_repeat('?,', count($department_id) - 1) . '?';

        if (is_array($department_id)) {
            $query = "SELECT * FROM member where id IN (select member_id from financial_payout where active=1 and department_id IN (".$in.") and date=STR_TO_DATE(?, '%m.%Y') and type_of_payment=?)";
        } else {
            $query = "SELECT * FROM member where id IN (select member_id from financial_payout where active=1 and department_id=? and date=STR_TO_DATE(?, '%m.%Y') and type_of_payment=?)";
        }

        try {
            $sth = $this->dbh->prepare($query);
            if (is_array($department_id)) {
                $param = array_merge($department_id,[$d], [$type_of_payment]);
                $sth->execute($param);
            } else {
                $sth->execute([$department_id, $d, $type_of_payment]);
            }
        } catch (Exception $e) {
            echo $e->getMessage()."<BR>";
        }

        if ($sth->rowCount()==0) {
            throw new Exception("Нет данных из БД.");
        }
        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);
        $this->setMemberValue($rawresult, 0);
        return true;
    }

    /*
     * ================  DEPRECATED ================
     * Получает массив объектов пользователей
     * с данными с БД
     * @return true при успешном выполенении.
     * select member.*,member_department.department_id,member_direction.direction_id,member_position.position_id from member,member_department,member_direction,member_position where member.id=1 AND member.id=member_department.member_id=member_direction.member_id=member_position.member_id
     * Нужен ли массив объектов???
     * $all_members - Выборка всех пользователей или без уволенных
     * $emp_date - Выбрать пользователей, которые приняты на работу не позже data + 1 месяц
     * Например, строим ведомость за 02.2019, значит сотрудник может быть принят до 03.2019.
     */
    public function fetchMembersFinancial($d, $type_of_payment)
    {
        $this->dbh = dbConnect();
        $date = $d;
        switch ($type_of_payment) {
            case 1:
                $dt = DateTime::createFromFormat("d.m.Y", "05." . $date);
                $dt->modify("+1 month");
                $date = $dt->format("d.m.Y");

                $query = "select *,DATE_FORMAT(employment_date,'%d.%m.%Y') AS employment_date, DATE_FORMAT(birthday,'%d.%m.%Y') AS birthday,
                  DATE_FORMAT(dismissal_date,'%d.%m.%Y') AS dismissal_date  from member 
                  WHERE employment_date <= STR_TO_DATE(?, '%d.%m.%Y') AND   
                   (dismissal_date >= STR_TO_DATE(?, '%d.%m.%Y'))";

                break;
            case 2:
            case 3:
                $date = "20." . $date;

                $query = "select *,DATE_FORMAT(employment_date,'%d.%m.%Y') AS employment_date, DATE_FORMAT(birthday,'%d.%m.%Y') AS birthday,
                  DATE_FORMAT(dismissal_date,'%d.%m.%Y') AS dismissal_date from member 
                  WHERE employment_date < STR_TO_DATE(?, '%d.%m.%Y') AND   
                   (dismissal_date >= STR_TO_DATE(?, '%d.%m.%Y'))";
                break;
            default:
                break;
        }
        $sth = $this->dbh->prepare($query);

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        switch ($type_of_payment) {
            case 1:
            case 3:
                $sth->execute([$date, $date]);
                break;
            case 2:
                $sth->execute([$date, $date]);
        }

        if ($sth->rowCount()==0) {
            throw new Exception("Нет данных из БД.");
        }

        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $this->setMemberValue($rawresult);

        return true;
    }

    /*
     * Получение данных о пользователе по ID
     * @param id пользователя
     * @return class Member
     */
    public function fetchMemberByID($id)
    {
        $this->dbh = dbConnect();

        /*
         * Получаем из БД member все значения.
         * Дату поступления на работу и дату рождения формируем как нам удобно читать
         */
        $query = "select *, 
                        DATE_FORMAT(employment_date,'%d.%m.%Y') AS employment_date, 
                        DATE_FORMAT(birthday,'%d.%m.%Y') AS birthday,
                        DATE_FORMAT(dismissal_date,'%d.%m.%Y') AS dismissal_date 
                  from member where id=?";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$id])) {
            throw new Exception("Не могу выполнить зарос к БД");
        }
        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        if (!isset($rawresult[0])) {
            throw new Exception("Нет данных из БД.");
        }

        $this->setMemberValue($rawresult);

        return $this->membersList[0];
    }

    /*
     * Возвращает массиво объектов с сотрудниками.
     * @throws Если нету сотрудников в БД
     * @return массив объектов
     */
    public function GetMembers()
    {
        if (empty($this->membersList)) {
            throw new Exception("Нет заведенных сотрудников");
        }
        return $this->membersList;
    }


    public function getMotivation($id)
    {
        /*
         * МОТИВАЦИЯ ПРИМЕНЯЕТСЯ с текщушего МЕСЯЦА
         * Дата передается в формате %m.%Y
        */

        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        // Формируем строку запроса к таблице member_motivation, где указываем id пользователя и что нужны строки которые от 0000-00-00 до такушей даты и date_s от заданной даты до конца периода, сортируем до id и выводим одну строку более позднюю

//        $query = "SELECT motivation_id FROM member_motivation where
//        member_id=? AND
//        (date_s > '0000-00-00' AND date_s <= STR_TO_DATE(?,'%m.%Y')) AND
//        (date_e > STR_TO_DATE(?,'%m.%Y') AND  date_e <= '9999-01-01')
//        order by id desc LIMIT 1";

        $query = "SELECT motivation_id FROM member_motivation where 
        member_id=? and active = 1 
        order by id desc LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
            return $raw['motivation_id'];
        } else {
            return null;
        }
    }

    public function getMotivationByDate($id, $date)
    {
        /*
         * МОТИВАЦИЯ ПРИМЕНЯЕТСЯ с текщушего МЕСЯЦА
         * Дата передается в формате %m.%Y
        */

        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        // Приводим к нормальной дате
        $date = "01.".$date;

        // Формируем строку запроса к таблице member_motivation, где указываем id пользователя и что нужны строки которые от 0000-00-00 до такушей даты и date_s от заданной даты до конца периода, сортируем до id и выводим одну строку более позднюю

//        $query = "SELECT motivation_id FROM member_motivation where
//        member_id=? AND
//        (date_s > '0000-00-00' AND date_s <= STR_TO_DATE(?,'%m.%Y')) AND
//        (date_e > STR_TO_DATE(?,'%m.%Y') AND  date_e <= '9999-01-01')
//        order by id desc LIMIT 1";

        $query = "SELECT motivation_id FROM member_motivation where 
        member_id=? and 
        (date_s BETWEEN '0000-00-00' AND DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'), INTERVAL 1 MONTH ) ) and (date_e BETWEEN DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'), INTERVAL 1 MONTH) AND '9999-01-01') 
        order by id desc LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id, $date, $date]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
            return $raw['motivation_id'];
        } else {
            return null;
        }
    }

    public function getWorkStatusByDate($id, $date)
    {
        /*
         * Рабочий статус выставляется сразу, поэтому нужно отследить до конца месяца
         * Дата в формате %m.%Y
         */

        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }
        $date_1 = "01." . $date;
        $date_2 = "31." . $date;

        /*
         * Формируем строку запроса к таблице member_motivation,
         * где указываем id пользователя и что нужны строки которые от 0000-00-00 до такушей даты и
         * date_s от заданной даты до конца периода,
         * сортируем до id и выводим одну строку более позднюю
         */
        $query = "SELECT status FROM member_workstatus where 
        member_id=? and 
        (date_s BETWEEN '0000-00-00' AND STR_TO_DATE(?,'%d.%m.%Y')) and (date_e BETWEEN STR_TO_DATE(?,'%d.%m.%Y') AND '9999-01-01') 
        order by id desc LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id, $date_2, $date_1]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() == 1) { // Если число строк > 0
            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
            return $raw['status'];
        } else {
            return 1; // Если ничего нету, значит сотрудник работает.
        }
    }

    /**
     * Получение рабочих реквизитов на заданную дату и заданного пользователя
     */
    public  function getJobPost($member_id){
        /*
        * Дата в формате %m.%Y
        */
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }
        $query_subquery = "select 
        t1.direction_id, t2.name as direction_name, 
        t1.department_id,t3.name as department_name,
        t1.position_id,t4.name as position_name,
        DATE_FORMAT(t1.date_s,'%d.%m.%Y') as date
        from member_jobpost as t1 
        LEFT JOIN direction as t2 ON(t1.direction_id = t2.id)
        LEFT JOIN department as t3 ON(t1.department_id = t3.id)
        LEFT JOIN position as t4 ON(t1.position_id = t4.id)
        where member_id=? and 
        t1.active = 1
        LIMIT 1";

        try {
            $sth_sub = $this->dbh->prepare($query_subquery);
            $sth_sub->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в получении направления." . $e->getMessage());
        }
        $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

        return $raw;
    }

    public  function getAllJobPost($member_id){
        /*
        * Дата в формате %m.%Y
        */
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }
        $query_subquery = "select 
        t1.direction_id, t2.name as direction_name, 
        t1.department_id,t3.name as department_name,
        t1.position_id,t4.name as position_name,
        DATE_FORMAT(t1.date_s,'%d.%m.%Y') as date
        from member_jobpost as t1 
        LEFT JOIN direction as t2 ON(t1.direction_id = t2.id)
        LEFT JOIN department as t3 ON(t1.department_id = t3.id)
        LEFT JOIN position as t4 ON(t1.position_id = t4.id)
        where member_id=?";

        try {
            $sth_sub = $this->dbh->prepare($query_subquery);
            $sth_sub->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в получении направления." . $e->getMessage());
        }
        $raw = $sth_sub->fetchAll(PDO::FETCH_ASSOC);

        return $raw;
    }

    public function getJobPostByDate($member_id, $date)
    {
        /*
         * Дата в формате %m.%Y
         */
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $date = "01.".$date;

        /*
         * Получаю текущеее направление, отдел, должность
         */
//        $query_subquery = "select
//        t1.direction_id, t2.name as direction_name,
//        t1.department_id,t3.name as department_name,
//        t1.position_id,t4.name as position_name,
//        DATE_FORMAT(t1.date_s,'%d.%m.%Y') as date
//        from member_jobpost as t1
//        LEFT JOIN direction as t2 ON(t1.direction_id = t2.id)
//        LEFT JOIN department as t3 ON(t1.department_id = t3.id)
//        LEFT JOIN position as t4 ON(t1.position_id = t4.id)
//        where
//        member_id=? AND
//        (t1.date_s >= '0000-00-00' AND t1.date_s < STR_TO_DATE(?,'%d.%m.%Y')) AND
//        (t1.date_e > STR_TO_DATE(?,'%d.%m.%Y') AND  t1.date_e <= '9999-01-01')
//        LIMIT 1";

        $query_subquery = "select 
        t1.direction_id, t2.name as direction_name, 
        t1.department_id,t3.name as department_name,
        t1.position_id,t4.name as position_name,
        DATE_FORMAT(t1.date_s,'%d.%m.%Y') as date
        from member_jobpost as t1 
        LEFT JOIN direction as t2 ON(t1.direction_id = t2.id)
        LEFT JOIN department as t3 ON(t1.department_id = t3.id)
        LEFT JOIN position as t4 ON(t1.position_id = t4.id)
        where member_id=? and 
        (t1.date_s BETWEEN '0000-00-00' AND DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'), INTERVAL 1 MONTH) ) and (t1.date_e BETWEEN DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'), INTERVAL 1 MONTH) AND '9999-01-01') 
        LIMIT 1";

        try {
            $sth_sub = $this->dbh->prepare($query_subquery);
            $sth_sub->execute([$member_id, $date, $date]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в получении направления." . $e->getMessage());
        }
        $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

        return $raw;
    }


    public function getSalary($member_id)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        /*
         * Получаю текущеее направление, отдел, должность
         */
        $query_subquery = "select * 
        from member_salary 
        where member_id=? AND 
        active = 1
        LIMIT 1";
        try {
            $sth_sub = $this->dbh->prepare($query_subquery);
            $sth_sub->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в получении направления." . $e->getMessage());
        }
        $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

        return $raw;
    }

    /**
     * Получение рабочих реквизитов на заданную дату и заданного пользователя
     */
    public function getSalaryByDate($member_id, $date)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        /*
         * Привожу дату к нормальному виду
         */
        $date = "01.".$date;

        /*
         * Получаю текущеее направление, отдел, должность
         */
        $query_subquery = "select * 
        from member_salary 
        where member_id=? AND 
        (date_s BETWEEN '0000-00-00' AND DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'), INTERVAL 1 MONTH ) ) and (date_e BETWEEN DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'), INTERVAL 1 MONTH) AND '9999-01-01')  
        LIMIT 1";
        try {
            $sth_sub = $this->dbh->prepare($query_subquery);
            $sth_sub->execute([$member_id, $date,  $date]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в получении направления." . $e->getMessage());
        }
        $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

        return $raw;
    }

    /**
     * Получение отчисления в ГБ на заданную дату и заданного пользователя
     */
    public function getYearBonusPayByDate($member_id, $date)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $date = "01.".$date;

        /*
         * Получаю текущеее направление, отдел, должность
         */
//        $query_subquery = "select *
//        from member_yearbonus_pay
//        where member_id=? AND
//        (date_s > '0000-00-00' AND date_s <= STR_TO_DATE(?,'%d.%m.%Y')) AND
//        (date_e > STR_TO_DATE(?,'%d.%m.%Y') AND  date_e <= '9999-01-01')
//        order by id DESC LIMIT 1";

        $query_subquery = "select * 
        from member_yearbonus_pay 
        where member_id=? and 
        (date_s BETWEEN '0000-00-00' AND DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'), INTERVAL 1 MONTH) ) and (date_e BETWEEN DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'), INTERVAL 1 MONTH) AND '9999-01-01') 
        order by id DESC LIMIT 1";

        try {
            $sth_sub = $this->dbh->prepare($query_subquery);
            $sth_sub->execute([$member_id, $date, $date]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в получении направления." . $e->getMessage());
        }
        $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

        return $raw;
    }

    public function getYearBonusPay($member_id)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }



        $query_subquery = "select * 
        from member_yearbonus_pay 
        where member_id=? and 
        active = 1 
        order by id DESC LIMIT 1";

        try {
            $sth_sub = $this->dbh->prepare($query_subquery);
            $sth_sub->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в получении направления." . $e->getMessage());
        }
        $raw = $sth_sub->fetch(PDO::FETCH_ASSOC);

        return $raw;
    }

    /*
     * Изменение чекбокса "Продление отпуска до"
     */
    public function changeVacationExtensionDate($member_id, $vacation_extension_date, $vacation_extension_note, $changer_id)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }
        $query = "INSERT INTO vacation_extension (member_id, vacation_extension_date, vacation_extension_note, changer_id, change_date) VALUES (?,STR_TO_DATE(?,'%Y-%m-%d'),?,?,NOW())";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        $sth->execute([$member_id, $vacation_extension_date, $vacation_extension_note, $changer_id]); // Выполняем запрос
    }

    /*
     * Удаление чекбокса "Продление отпуска до"
     */
    public function deleteVacationExtensionDate($id)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }
        $query = "DELETE FROM vacation_extension where id=?";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        $sth->execute([$id]); // Выполняем запрос
    }

    /*
     * Изменение чекбокса "Отпуск не сгорает"
     */
//    public function changeVacationNotExpire($member_id, $vacation_not_expire, $vacation_not_expire_date)
//    {
//        $vacation_not_expire_date = htmlspecialchars($vacation_not_expire_date);
//        if ($this->dbh == null) { // Если нету подключения, то
//            $this->dbh = dbConnect(); // Подключаемся
//        }
//
//        if ($vacation_not_expire && empty($vacation_not_expire)) {
//            return false;
//        }
//
//        if ($vacation_not_expire) {
//            $query = "UPDATE member SET vacation_not_expire=?, vacation_not_expire_date=STR_TO_DATE(?,'%Y-%m-%d') where id=?";
//            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
//            $sth->execute([$vacation_not_expire, $vacation_not_expire_date, $member_id]); // Выполняем запрос
//        } else {
//            $query = "UPDATE member SET vacation_not_expire=?, vacation_not_expire_date=NULL where id=?";
//            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
//            $sth->execute([$vacation_not_expire, $member_id]); // Выполняем запрос
//        }
//    }

    /*
     * Изменение чекбокса "Не считать УРВ"
     */
    public function changeNotCountUrv($member_id, $not_count_urv)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "UPDATE member SET not_count_urv=? where id=?";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        $sth->execute([$not_count_urv, $member_id]); // Выполняем запрос
    }

    /*
     * Изменение чекбокса "Не выплачивать аванс сотруднику"
     */
    public function changeAvansPay($member_id, $status, $changer_id)
    {

        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "select * from member_avans where member_id=? and active=1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $query_sub = "UPDATE member_avans SET active=0,date_e=NOW(), changer_id=? where member_id=? and active=1";
            $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
            $sth_sub->execute([$changer_id, $member_id]); // Выполняем запрос
        }

        $query_sub = "INSERT INTO member_avans (member_id, bool, date_s, date_e, active, changer_id) VALUES  (?,?,NOW(),'9999-01-01',1,?)";
        $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
        $sth_sub->execute([$member_id, $status, $changer_id]); // Выполняем запрос

        $sth = null;
        $sth_sub = null;
    }

    /*
     * Изменение чекбокса "Не выплачивать аванс сотруднику"
     */
    public function changeTransport($member_id, $summ, $formula, $changer_id)
    {

        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "select * from member_Transport where member_id=? and active=1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $query_sub = "UPDATE member_Transport SET active=0, date=NOW(), changer_id=? where member_id=? and active=1";
            $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
            $sth_sub->execute([$changer_id, $member_id]); // Выполняем запрос
        }

        $query_sub = "INSERT INTO member_Transport (member_id, summ, formula, date, active, changer_id) VALUES  (?,?,?,NOW(),1,?)";
        $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
        $sth_sub->execute([$member_id, $summ, $formula, $changer_id]); // Выполняем запрос

        $sth = null;
        $sth_sub = null;
    }

    public function changeAvansPayWithDate($member_id, $status, $changer_id, $date)
    {

        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "select * from member_avans where member_id=? and active=1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        $lastdate = $sth->fetch()['date_s'];

        if (strtotime($lastdate) >  strtotime($date)) {
            throw new Exception("Последняя дата изменения была ".$lastdate.". Новая дата меньше последней даты изменения, что невозможно.");
        }

        if ($sth->rowCount() > 0) {
            $query_sub = "UPDATE member_avans SET active=0, date_e=STR_TO_DATE(?,'%d.%m.%Y'), changer_id=? where member_id=? and active=1";
            $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
            $sth_sub->execute([$date, $changer_id, $member_id]); // Выполняем запрос
        }

        $query_sub = "INSERT INTO member_avans (member_id, bool, date_s, date_e, active, changer_id) VALUES  (?,?,STR_TO_DATE(?,'%d.%m.%Y'),'9999-01-01',1,?)";
        $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
        $sth_sub->execute([$member_id, $status, $date, $changer_id]); // Выполняем запрос

        $sth = null;
        $sth_sub = null;
    }

    /*
     * Изменение процента от оклада для Аванса
     */
    public function changeAvansProcent($member_id, $procent, $changer_id)
    {

        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "select * from member_procent_avans where member_id=? and active=1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $query_sub = "UPDATE member_procent_avans SET active=0, date_e=NOW(), changer_id=? where member_id=? and active=1";
            $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
            $sth_sub->execute([$changer_id, $member_id]); // Выполняем запрос
        }


        $query_sub = "INSERT INTO member_procent_avans (member_id, procent, date_s, date_e, active, changer_id) VALUES  (?,?,NOW(),'9999-01-01',1,?)";
        $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
        $sth_sub->execute([$member_id, $procent, $changer_id]); // Выполняем запрос
        $sth = null;
        $sth_sub = null;
    }

    public function changeAvansProcentWithDate($member_id, $procent, $changer_id, $date)
    {

        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "select * from member_procent_avans where member_id=? and active=1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        $lastdate = $sth->fetch()['date_s'];

        if (strtotime($lastdate) >  strtotime($date)) {
            throw new Exception("Последняя дата изменения была ".$lastdate.". Новая дата меньше последней даты изменения, что невозможно.");
        }

        if ($sth->rowCount() > 0) {
            $query_sub = "UPDATE member_procent_avans SET active=0, date_e=STR_TO_DATE(?,'%d.%m.%Y'), changer_id=? where member_id=? and active=1";
            $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
            $sth_sub->execute([$date, $changer_id, $member_id]); // Выполняем запрос
        }

        $query_sub = "INSERT INTO member_procent_avans (member_id, procent, date_s, date_e, active, changer_id) VALUES  (?,?,STR_TO_DATE(?, '%d.%m.%Y'),'9999-01-01',1,?)";
        $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
        $sth_sub->execute([$member_id, $procent, $date, $changer_id]); // Выполняем запрос

        $sth = null;
        $sth_sub = null;
    }


    /*
     * Изменение отчисления в ГБ
     */
    public function changeYearBonusPay($member_id, $type, $summ, $changer_id, $note)
    {

        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        if ($type == 2 && $summ > 100) {
            throw  new Exception("Процент не может быть больше 100%");
        }

        if ($type == 0) {
            $query_sub = "UPDATE member_yearbonus_pay SET active=0, date_e=NOW(), changer_id=?, note=? where member_id=? and active=1";
            $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
            $sth_sub->execute([$changer_id, $note, $member_id]); // Выполняем запрос
            $sth_sub = null;
        } else {
            $query = "select * from member_yearbonus_pay where member_id=? and active=1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$member_id]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO ' . $e->getMessage());
            }

            if ($sth->rowCount() > 0) {
                $query_sub = "UPDATE member_yearbonus_pay SET active=0, date_e=NOW(), changer_id=?, note=? where member_id=? and active=1";
                $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
                $sth_sub->execute([$changer_id, $note, $member_id]); // Выполняем запрос
            }

            $query_sub = "INSERT INTO member_yearbonus_pay (member_id, type, summ, note, date_s, date_e, active, changer_id) VALUES  (?,?,?,?,NOW(),'9999-01-01',1,?)";
            $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
            $sth_sub->execute([$member_id, $type, $summ, $note, $changer_id]); // Выполняем запрос
            $sth = null;
            $sth_sub = null;
        }
    }

    public function changeYearBonusPayWithDate($member_id, $type, $summ, $changer_id, $note, $date)
    {

        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        if ($type == 2 && $summ > 100) {
            throw  new Exception("Процент не может быть больше 100%");
        }

        if ($type == 0) {
            $query_sub = "UPDATE member_yearbonus_pay SET active=0, date_e=STR_TO_DATE(?,'%d.%m.%Y'), changer_id=? where member_id=? and active=1";
            $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
            $sth_sub->execute([$date, $changer_id, $member_id]); // Выполняем запрос
            $sth_sub = null;
        } else {
            $query = "select * from member_yearbonus_pay where member_id=? and active=1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$member_id]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO ' . $e->getMessage());
            }

            $lastdate = $sth->fetch()['date_s'];

            if (strtotime($lastdate) >  strtotime($date)) {
                throw new Exception("Последняя дата изменения была ".$lastdate.". Новая дата меньше последней даты изменения, что невозможно.");
            }

            if ($sth->rowCount() > 0) {
                $query_sub = "UPDATE member_yearbonus_pay SET active=0, date_e=STR_TO_DATE(?,'%d.%m.%Y'), changer_id=? where member_id=? and active=1";
                $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
                $sth_sub->execute([$date, $changer_id, $member_id]); // Выполняем запрос
            }

            $query_sub = "INSERT INTO member_yearbonus_pay (member_id, type, summ, note, date_s, date_e, active, changer_id) VALUES  (?,?,?,?,STR_TO_DATE(?,'%d.%m.%Y'),'9999-01-01',1,?)";
            $sth_sub = $this->dbh->prepare($query_sub); // Подготавливаем запрос
            $sth_sub->execute([$member_id, $type, $summ, $note, $date, $changer_id]); // Выполняем запрос
            $sth = null;
            $sth_sub = null;
        }
    }

    public function getMembersFinanceInfo()
    {

        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT t1.id, t1.name, t1.lastname, t1.middle,
	    t4.id as department_id, t4.name as department_name,
	    t3.id as position_id, t3.name as position_name,
        t6.salary as salary,
        t5.percent as base_percent,
        t7.AdministrativePrize as AdministrativePrize,
        t8.MobileCommunication as MobileCommunication
	    FROM member as t1
        LEFT JOIN member_jobpost as t2 ON (t1.id = t2.member_id and t2.active = 1)
        LEFT JOIN position as t3 ON (t2.position_id = t3.id)
        LEFT JOIN department as t4 ON (t2.department_id = t4.id)
        LEFT JOIN member_percent as t5 ON (t1.id = t5.member_id and t5.active = 1)
        LEFT JOIN member_salary as t6 ON (t1.id = t6.member_id and t6.active = 1)
        LEFT JOIN member_AdministrativePrize as t7 ON (t1.id = t7.member_id and t7.active = 1)
        LEFT JOIN member_MobileCommunication as t8 ON (t1.id = t8.member_id and t8.active = 1)
        where t1.dismissal_date='9999-01-01 00:00:00'
        order by t4.id, t3.id;";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute(); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        }

        return false;
    }

    public function getSalaryDates($member_id)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT DATE_FORMAT(date_s,'%d.%m.%Y') as date_s FROM member_salary where member_id=? and active = 1 LIMIT 1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $raw = current($sth->fetch());
        }

        if (!isset($raw)) {
            $query = "SELECT DATE_FORMAT(employment_date,'%d.%m.%Y') as employment_date FROM member where id=? LIMIT 1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$member_id]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO ' . $e->getMessage());
            }

            if ($sth->rowCount() > 0) {
                $raw = current($sth->fetch());
            }
        }

        if (!isset($raw)) {
            $raw = "00.00.0000";
        }

        return $raw;
    }

    public function empDateModify($member_id, $emp_date_old, $emp_date) {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "UPDATE member SET employment_date=STR_TO_DATE(? ,'%Y-%m-%d') where id=?";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$emp_date, $member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка! Не могу обновить дату приема на работу ' . $e->getMessage());
        }

        /*
         * Jobpost table
         */
        $query = "select * from member_jobpost where member_id=? and active=1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка! Получения данных с таблицы member_jobpost ' . $e->getMessage());
        }

        if ( $sth->rowCount() != 0 ) {
            $query = "UPDATE member_jobpost SET date_s=STR_TO_DATE(?, '%Y-%m-%d') WHERE member_id = ? and active = 1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$emp_date, $member_id]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка! Не могу обновить данные в таблице member_jobpost ' . $e->getMessage());
            }
        }


        /*
         * member_work table
         */
        $query = "select * from member_work where member_id=? and active=1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка! Получения данных с таблицы member_work ' . $e->getMessage());
        }

        if ( $sth->rowCount() != 0 ) {
            $query = "UPDATE member_work SET date=STR_TO_DATE(?, '%Y-%m-%d') WHERE member_id = ? and active = 1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$emp_date, $member_id]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка! Не могу обновить данные в таблице member_work ' . $e->getMessage());
            }
        }

        /*
         * member_workstatus table
         */
        $query = "select * from member_workstatus where member_id=? and active=1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка! Получения данных с таблицы member_work ' . $e->getMessage());
        }

        if ( $sth->rowCount() != 0 ) {
            $query = "UPDATE member_workstatus SET date_s=STR_TO_DATE(?, '%Y-%m-%d') WHERE member_id = ? and active=1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$emp_date, $member_id]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка! Не могу обновить данные в таблице member_work ' . $e->getMessage());
            }
        }

        /*
         * member_motivation table
         */
        $query = "select * from member_motivation where member_id=? and active=1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка! Получения данных с таблицы member_work ' . $e->getMessage());
        }

        if ( $sth->rowCount() != 0 ) {
            $query = "UPDATE member_motivation SET date_s=STR_TO_DATE(?, '%Y-%m-%d') WHERE member_id = ? and active = 1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$emp_date, $member_id]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка! Не могу обновить данные в таблице member_work ' . $e->getMessage());
            }
        }

        /*
         * member_salary table
         */
        $query = "select * from member_salary where member_id=? and active=1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка! Получения данных с таблицы member_work ' . $e->getMessage());
        }

        if ( $sth->rowCount() != 0 ) {
            $query = "UPDATE member_salary SET date_s=STR_TO_DATE(?, '%Y-%m-%d') WHERE member_id = ? and active = 1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$emp_date, $member_id]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка! Не могу обновить данные в таблице member_work ' . $e->getMessage());
            }
        }

        /*
         * AWH
         */
        $date_old = DateTime::createFromFormat("Y-m-d", $emp_date_old);
        $date_new = DateTime::createFromFormat("Y-m-d", $emp_date);

        /*
         *  Создаю объект производственный календарь
         */
        $PC = new ProductionCalendar();
        $AWH = new AwhInterface();

        /*
         * Если разный месяц, то меняю/удаляю старые значения
         */
        if ($date_old->format("m") < $date_new->format("m")) {
            /*
             * Получаю рабочие дни из производственного календаря
             */
            $PC->fetchByDate($date_old->format("m.Y"));
            /*
            * Извлекаю массив с рабочими днями
            */
            $PC->fetchWorkingArray($date_old->format("m.Y"));
            /*
            * Получаю массив с рабочими днями
            */
            $working_days_array = $PC->getWDArray();
            $AWH->setNewAbsenceByMember($member_id, $date_old->format("m.Y"), 1, count($working_days_array));
            $AWH->setNewAbsenceByMember($member_id, $date_old->format("m.Y"), 2, (count($working_days_array)/2));
            unset($working_days_array);
        } else if ($date_old->format("m") > $date_new->format("m")) {
            $AWH->setNewAbsenceByMember($member_id, $date_old->format("m.Y"), 1, 0);
            $AWH->setNewAbsenceByMember($member_id, $date_old->format("m.Y"), 2, 0);
        }

        /*
        * Получаю рабочие дни из производственного календаря
        */
        $PC->fetchByDate($date_new->format("m.Y"));

        /*
        * Извлекаю массив с рабочими днями
        */
        $PC->fetchWorkingArray($date_new->format("m.Y"));

        /*
        * Получаю массив с рабочими днями
        */
        $working_days_array = $PC->getWDArray();

        $awh_absence = 0;
        $awh_absence_avans = 0;

        /*
         * Расчет отсутствий в премию
         */
        foreach ($working_days_array as $wp) {
            if ($date_new->format("d") != $wp ){
                $awh_absence += 1;
            } else {
                break;
            }
        }

        /*
         * Получаю первую часть рабочих дней при котором сотрудник получает аванс
         */
        $working_part = array_splice($working_days_array, 0, (ceil(count($working_days_array) / 2 )) );

        /*
         * Расчет отсутствий в аванс
         */
        foreach ($working_part as $wp) {
            if ($date_new->format("d") != $wp ){
                $awh_absence_avans += 1;
            } else {
                break;
            }
        }

        $working_days_array = $PC->getWDArray();
        if ($awh_absence_avans >= count($working_part) && count($working_days_array) % 2 !== 0) {
            $awh_absence_avans = count($working_days_array) / 2;
        }

        /*
        * Добавляю отсутствия
        */
        $AWH->setNewAbsenceByMember($member_id, $date_new->format("m.Y"), 1, $awh_absence);
        $AWH->setNewAbsenceByMember($member_id, $date_new->format("m.Y"), 2, $awh_absence_avans);

    }

    /*
     * Перевод кириллицы в латиницу
     */
    private function translit($str)
    {
        $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
        $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
        return str_replace($rus, $lat, $str);
    }

    /*
     * @param login
     * @return true если логина нету и false если login есть
     */
    private function TestLogin($login)
    {
        $dbh = dbConnect();

        $query = "select * from member_work where login=?";

        try {
            $sth = $dbh->prepare($query);
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("PDO Error" . $e->getMessage());
        }


        $sth->execute([$login]);
        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $dbh = null;

        if (count($rawresult) == 0) {
            return false;
        }

        return true;

    }

    /*
     * @param name
     * @return true если логина нету и false если login есть
     */
    private function Testsklad($name)
    {
        $dbh = dbConnect();

        $query = "select * from member_work where 1CSklad_name=?";

        try {
            $sth = $dbh->prepare($query);
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("PDO Error" . $e->getMessage());
        }


        $sth->execute([$name]);
        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $dbh = null;

        if (count($rawresult) == 0) {
            return false;
        }

        return true;

    }

    private function is_Date($str)
    {
        return is_numeric(strtotime($str));
    }

    /*
     * Если запись в ведомости активна, то возвращаю true
     */
    public function checkActivePayout($member_id, $date, $type_of_payment) {
        $dbh = dbConnect();

        $query = "select active from financial_payout where member_id=? and date = STR_TO_DATE(?, '%m.%Y') and type_of_payment = ? and active = 1";

        try {
            $sth = $dbh->prepare($query);
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("PDO Error" . $e->getMessage());
        }


        $sth->execute([$member_id, $date, $type_of_payment]);

        if ($sth->rowCount() > 0 ){
            return true;
        }

        $sth = null;

        return false;
    }

    public function getProfitByYear($member_id, $motivation, $year) {
        $dbh = dbConnect();

        switch ($motivation) {
            case 1:
                $aReturn = [];
                if ($year <= 2021) {
                    $q = "select tt.date,tt.member_id, tt.statgroup_id, tt.department_id, p, b, c, (p-b+c) as f FROM 
                    (SELECT t1.date,t1.member_id,t1.statgroup_id, 
                      t1.department_id, IFNULL(SUM(profit*25),0) as p, IFNULL(SUM(bonus*25),0) as b, IFNULL(SUM(correct*25),0) as c 
                    FROM zp.data_manager as t1
                    LEFT JOIN correcting_manager as t2 ON (t1.id = t2.data_manager_id and t2.active=1)
                    WHERE t1.member_id=? and t1.date>=STR_TO_DATE(?, '%Y-%m-%d') and t1.date<STR_TO_DATE(?, '%Y-%m-%d') GROUP BY t1.date
                    ) as tt
                    order by tt.date";

                    try {
                        $sth = $dbh->prepare($q);
                    } catch (PDOException $e) {
                        echo $e->getMessage();
                        throw new Exception("PDO Error" . $e->getMessage());
                    }

                    if ($year == 2021) {
                        $sth->execute([$member_id, $year . "-01-00", $year . "-04-31"]);
                    } else {
                        $sth->execute([$member_id, $year . "-01-00", $year . "-12-31"]);
                    }
                    $aReturn1 = $sth->fetchAll(PDO::FETCH_ASSOC);

                    $q = "SELECT tt.date,
                             tt.member_id,
                             tt.statgroup_id,
                             tt.department_id, p
                     FROM (SELECT
                        t1.date,
                        t1.member_id,
                        NULL as statgroup_id,
                        t1.department_id,
                        SUM(t1.profit) as profit,
                        IFNULL(SUM(t1.profit),0) as p
                    FROM data as t1
                    WHERE
                        t1.member_id=? AND
                        t1.date>=STR_TO_DATE(?, '%Y-%m-%d') AND
                        t1.date<=STR_TO_DATE(?, '%Y-%m-%d')
                    GROUP BY month(t1.date) ORDER BY t1.id) as tt
                    order by tt.date";

                    try {
                        $sth = $dbh->prepare($q);
                    } catch (PDOException $e) {
                        echo $e->getMessage();
                        throw new Exception("PDO Error" . $e->getMessage());
                    }

                    if ($year == 2021) {
                        $sth->execute([$member_id, $year . "-01-00", $year . "-04-31"]);
                    } else {
                        $sth->execute([$member_id, $year . "-01-00", $year . "-12-31"]);
                    }
                    $aReturn2 = $sth->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($aReturn2 as $key => $item) {
                        $q = "select IFNULL(SUM(sum),0) as b 
                            FROM bonus 
                            WHERE member_id=? and department_id=? and date=? and is_active=1 GROUP BY date";
                        try {
                            $sth = $dbh->prepare($q);
                        } catch (PDOException $e) {
                            echo $e->getMessage();
                            throw new Exception("PDO Error" . $e->getMessage());
                        }
                        $sth->execute([$member_id, $item['department_id'], $str = substr_replace($item['date'], '00', strlen($item['date']) - 2)]);
                        $rtn = $sth->fetch(PDO::FETCH_ASSOC);
                        $aReturn2[$key]['b'] = $rtn['b'];

                        $q = "select IFNULL(SUM(sum),0) as b 
                            FROM correct 
                            WHERE member_id=? and department_id=? and date=? and is_active=1 GROUP BY date";
                        try {
                            $sth = $dbh->prepare($q);
                        } catch (PDOException $e) {
                            echo $e->getMessage();
                            throw new Exception("PDO Error" . $e->getMessage());
                        }
                        $sth->execute([$member_id, $item['department_id'], $str = substr_replace($item['date'], '00', strlen($item['date']) - 2)]);
                        $rtn = $sth->fetch(PDO::FETCH_ASSOC);
                        $aReturn2[$key]['c'] = $rtn['c'];

                        $aReturn2[$key]['f'] = $aReturn2[$key]['p'] - $aReturn2[$key]['b'] + $aReturn2[$key]['c'];
                    }

                    $aReturn = array_merge($aReturn1, $aReturn2);
                }

                if ($year >= 2021) {
                    $q = "SELECT data.id, DATE_FORMAT(data.date, '%Y-%m-00') as date, 
                        SUM(data.profit) as p,         
                        data.department_id,
                        dp.namesklad as department_name
                    FROM data
                    JOIN department as dp ON(department_id = dp.id) 
                    where member_id=? AND date>=STR_TO_DATE(?, '%Y-%m-%d') AND date<=STR_TO_DATE(?, '%Y-%m-%d') 
                    GROUP BY  MONTH(data.date), data.department_id order by id";

                    try {
                        $sth = $dbh->prepare($q);
                    } catch (PDOException $e) {
                        echo $e->getMessage();
                        throw new Exception("PDO Error" . $e->getMessage());
                    }
                    if ($year == 2021) {
                        $sth->execute([$member_id, $year . "-05-00", $year . "-12-31"]);
                    } else {
                        $sth->execute([$member_id, $year . "-01-00", $year . "-12-31"]);
                    }
                    $aReturn3 = $sth->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($aReturn3 as $key => $item) {
                        $q = "select IFNULL(SUM(sum),0) as b 
                            FROM bonus 
                            WHERE member_id=? and department_id=? and date=? and is_active=1 GROUP BY date";
                        try {
                            $sth = $dbh->prepare($q);
                        } catch (PDOException $e) {
                            echo $e->getMessage();
                            throw new Exception("PDO Error" . $e->getMessage());
                        }

                        $sth->execute([$member_id, $item['department_id'], $item['date']]);
                        $rtn = $sth->fetch(PDO::FETCH_ASSOC);
                        $aReturn3[$key]['b'] = $rtn['b'];

                        $q = "select IFNULL(SUM(sum),0) as c 
                            FROM correcting 
                            WHERE member_id=? and department_id=? and date=? and is_active=1 GROUP BY date";
                        try {
                            $sth = $dbh->prepare($q);
                        } catch (PDOException $e) {
                            echo $e->getMessage();
                            throw new Exception("PDO Error" . $e->getMessage());
                        }
                        $sth->execute([$member_id, $item['department_id'], $item['date']]);
                        $rtn = $sth->fetch(PDO::FETCH_ASSOC);
                        $aReturn3[$key]['c'] = $rtn['c'];

                        $aReturn3[$key]['f'] = $aReturn3[$key]['p'] - $aReturn3[$key]['b'] + $aReturn3[$key]['c'];
                    }

                    $aR = array_merge($aReturn, $aReturn3);
                    $aReturn = $aR;
                }

                return $aReturn;

                break;
            case 2:
                $q = "select procents from member_percent_advanced where member_id = ? and type=2 and active = 1 LIMIT 1";

                try {
                    $sth = $dbh->prepare($q);
                } catch (PDOException $e) {
                    echo $e->getMessage();
                    throw new Exception("PDO Error" . $e->getMessage());
                }
                $sth->execute([$member_id]);
                $aReturn = [];
                if ($sth->rowCount() > 0) {
                    $r = $sth->fetch()[0];
                    $departments = array();
                    foreach (unserialize($r) as $k=>$item) {
                        if (empty($item))
                            continue;
                        $departments[] = $k;
                    }

                    if ($year <= 2021) {
                        $in = str_repeat('?,', count($departments) - 1) . '?';
                        $q = "
                        SELECT date, department_id, name, p, b, c, (p-b+c) as f FROM ( 
                            SELECT t1.date, t1.department_id, t3.name, IFNULL(SUM(profit*25),0) as p, 
                            IFNULL( SUM(bonus*25), 0) as b,
                            IFNULL( SUM(correct*25), 0) as c
                        FROM zp.data_TT as t1 
                        LEFT JOIN correcting_tt as t2 ON (t1.id = t2.data_TT_id and t2.active=1 and t2.member_id=?) 
                        LEFT JOIN department as t3 ON (t1.department_id = t3.id) 
                        WHERE t1.department_id IN (" . $in . ") and 
                                t1.date >= STR_TO_DATE(?, '%Y-%m-%d') and t1.date < STR_TO_DATE(?, '%Y-%m-%d')
                        GROUP BY t1.date,t1.department_id ) as f";

                        try {
                            $sth = $dbh->prepare($q);
                        } catch (PDOException $e) {
                            echo $e->getMessage();
                            throw new Exception("PDO Error" . $e->getMessage());
                        }
                        if ($year == 2021) {
                            $params = array_merge([$member_id], $departments, [$year . "-01-00", $year . "-04-31"]);
                        } else {
                            $params = array_merge([$member_id], $departments, [$year . "-01-00", $year . "-12-31"]);
                        }
                        $sth->execute($params);
                        $aReturn1 = $sth->fetchAll(PDO::FETCH_ASSOC);

                        $q = "SELECT f.date, 
                                 f.department_id,
                                 f.name,
                                 f.p
                                FROM 
                                (SELECT t1.date, t1.department_id, dp.name, member_id, IFNULL(SUM(t1.profit),0) as p  
                                    FROM data as t1 
                                LEFT JOIN department as dp ON (t1.department_id = dp.id) 
                                WHERE t1.department_id IN (" . $in . ") AND
                                    t1.date>=STR_TO_DATE(?, '%Y-%m-%d') AND t1.date<=STR_TO_DATE(?, '%Y-%m-%d')
                                GROUP BY month(t1.date) ORDER BY t1.id) as f
                                ";

                        try {
                            $sth = $dbh->prepare($q);
                        } catch (PDOException $e) {
                            echo $e->getMessage();
                            throw new Exception("PDO Error" . $e->getMessage());
                        }

                        if ($year == 2021) {
                            $params = array_merge($departments, [$year . "-01-00", $year . "-04-31"]);
                        } else {
                            $params = array_merge($departments, [$year . "-01-00", $year . "-12-31"]);
                        }

                        $sth->execute($params);
                        $aReturn2 = $sth->fetchAll(PDO::FETCH_ASSOC);

                        foreach ($aReturn2 as $key => $item) {
                            $q = "select IFNULL(SUM(sum),0) as b 
                            FROM bonus 
                            WHERE member_id=? and department_id=? and date=? and is_active=1 GROUP BY date";
                            try {
                                $sth = $dbh->prepare($q);
                            } catch (PDOException $e) {
                                echo $e->getMessage();
                                throw new Exception("PDO Error" . $e->getMessage());
                            }
                            $sth->execute([$member_id, $item['department_id'], $str = substr_replace($item['date'], '00', strlen($item['date']) - 2)]);
                            $rtn = $sth->fetch(PDO::FETCH_ASSOC);
                            $aReturn2[$key]['b'] = $rtn['b'];

                            $q = "select IFNULL(SUM(sum),0) as b 
                            FROM correct 
                            WHERE member_id=? and department_id=? and date=? and is_active=1 GROUP BY date";
                            try {
                                $sth = $dbh->prepare($q);
                            } catch (PDOException $e) {
                                echo $e->getMessage();
                                throw new Exception("PDO Error" . $e->getMessage());
                            }
                            $sth->execute([$member_id, $item['department_id'], $str = substr_replace($item['date'], '00', strlen($item['date']) - 2)]);
                            $rtn = $sth->fetch(PDO::FETCH_ASSOC);
                            $aReturn2[$key]['c'] = $rtn['c'];

                            $aReturn2[$key]['f'] = $aReturn2[$key]['p'] - $aReturn2[$key]['b'] + $aReturn2[$key]['c'];
                        }
                        $aReturn = array_merge($aReturn1, $aReturn2);
                    }

                    if ($year >= 2021) {
                        $in = str_repeat('?,', count($departments) - 1) . '?';
                        $q = "SELECT DATE_FORMAT(date, '%Y-%m-00') as date, 
                                SUM(t1.profit) as p, 
                                t1.department_id, 
                                dp.namesklad as name 
                                FROM data as t1 
                                LEFT JOIN department as dp ON (t1.department_id = dp.id) 
                                WHERE t1.date>=STR_TO_DATE(?, '%Y-%m-%d') AND t1.date<=STR_TO_DATE(?, '%Y-%m-%d') AND 
                                t1.department_id IN (".$in.") 
                                GROUP BY MONTH(t1.date), t1.department_id 
                                order by MONTH(t1.date), dp.namesklad";

                        try {
                            $sth = $dbh->prepare($q);
                        } catch (PDOException $e) {
                            echo $e->getMessage();
                            throw new Exception("PDO Error" . $e->getMessage());
                        }
                        if ($year == 2021) {
                            $params = array_merge([$year . "-05-00", $year . "-12-31"], $departments);
                        } else {
                            $params = array_merge([$year . "-01-00", $year . "-12-31"], $departments);
                        }

                        $sth->execute($params);

                        $aReturn3 = $sth->fetchAll(PDO::FETCH_ASSOC);

                        foreach ($aReturn3 as $key => $item) {
                            $q = "select IFNULL(SUM(sum),0) as b 
                            FROM bonus 
                            WHERE member_id=? and department_id=? and date=? and is_active=1 GROUP BY date";
                            try {
                                $sth = $dbh->prepare($q);
                            } catch (PDOException $e) {
                                echo $e->getMessage();
                                throw new Exception("PDO Error" . $e->getMessage());
                            }

                            $sth->execute([$member_id, $item['department_id'], $item['date']]);
                            $rtn = $sth->fetch(PDO::FETCH_ASSOC);
                            $aReturn3[$key]['b'] = $rtn['b'];

                            $q = "select IFNULL(SUM(sum),0) as c 
                            FROM correcting 
                            WHERE member_id=? and department_id=? and date=? and is_active=1 GROUP BY date";
                            try {
                                $sth = $dbh->prepare($q);
                            } catch (PDOException $e) {
                                echo $e->getMessage();
                                throw new Exception("PDO Error" . $e->getMessage());
                            }
                            $sth->execute([$member_id, $item['department_id'], $item['date']]);
                            $rtn = $sth->fetch(PDO::FETCH_ASSOC);
                            $aReturn3[$key]['c'] = $rtn['c'];

                            $aReturn3[$key]['f'] = $aReturn3[$key]['p'] - $aReturn3[$key]['b'] + $aReturn3[$key]['c'];
                        }

                        $aR = array_merge($aReturn, $aReturn3);
                        $aReturn = $aR;

                    }

                    return $aReturn;
                }

                break;
            case 4:
                $q = "select procents from member_percent_advanced where member_id = ? and type=1 and active = 1 LIMIT 1";

                try {
                    $sth = $dbh->prepare($q);
                } catch (PDOException $e) {
                    echo $e->getMessage();
                    throw new Exception("PDO Error" . $e->getMessage());
                }
                $sth->execute([$member_id]);
                if ($sth->rowCount() > 0) {
                    $r = $sth->fetch()[0];
                    $statgroups = array();
                    foreach (unserialize($r) as $k => $item) {
                        $statgroups[] = $k;
                    }

                    $in = str_repeat('?,', count($statgroups) - 1) . '?';
                    $q = "select tt.date, tt.statgroup_id, tt.name, p, b, c, (p-b+c) as f FROM  
                    (SELECT t1.date,t1.statgroup_id, t3.name,
                     IFNULL(SUM(profit),0) as p, IFNULL(SUM(bonus),0) as b, IFNULL(SUM(correct),0) as c 
                        FROM zp.data_statgroup as t1
                        LEFT JOIN correcting_statgroup as t2 ON (t1.id = t2.data_statgroup_id and t2.active=1)
                        LEFT JOIN statgroup as t3 ON (t1.statgroup_id = t3.id)
                        WHERE t1.statgroup_id IN (".$in.") and t1.date >= STR_TO_DATE(?, '%Y-%m-%d') and t1.date < STR_TO_DATE(?, '%Y-%m-%d') GROUP BY t1.date
                    ) as tt
                    order by tt.date";

                    try {
                        $sth = $dbh->prepare($q);
                    } catch (PDOException $e) {
                        echo $e->getMessage();
                        throw new Exception("PDO Error" . $e->getMessage());
                    }
                    $params = array_merge($statgroups, [$year."-01-00", $year."-12-31"]);
                    $sth->execute($params);
                    $aReturn1 = $sth->fetchAll(PDO::FETCH_ASSOC);


                    $aReturn = array_merge($aReturn1);
                    return $aReturn;
                }
                break;
            case 8:
                /*
                 * Чтобы не вводить новую переменную, использую member_id как department_id
                 */
                $q = "
                SELECT data_service.id, date,
                SUM(profit) as p, (SELECT 0) as b, (SELECT 0) as c,SUM(profit) as f
                FROM data_service
                        JOIN statgroup as st ON(statgroup_id = st.id)
                        JOIN department as dp ON(department_id = dp.id) 
                        where member_id IN (
                            select t1.id from member as t1
                            LEFT JOIN member_jobpost as t2 ON (t1.id = t2.member_id)
                            where t1.dismissal_date='9999-01-01' and t2.department_id=?) 
                        and date>=STR_TO_DATE(?,'%Y-%m-%d') and date<STR_TO_DATE(?,'%Y-%m-%d') group by date
                ";

                try {
                    $sth = $dbh->prepare($q);
                } catch (PDOException $e) {
                    echo $e->getMessage();
                    throw new Exception("PDO Error" . $e->getMessage());
                }

                $sth->execute([$member_id, $year."-01-00", $year."-12-31"]);
                return $sth->fetchAll(PDO::FETCH_ASSOC);
                break;
            default:
                break;
        }

        return NULL;

    }

    public function getWindowManagers() {
        $dbh = dbConnect();

        $q = "SELECT t1.* FROM zp.member as t1
LEFT JOIN member_jobpost as t2 on (t1.id=t2.member_id)
WHERE t2.active=1 and t2.position_id=43 and t1.dismissal_date = '9999-01-01 00:00:00'";
        try {
            $sth = $dbh->prepare($q);
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("PDO Error" . $e->getMessage());
        }

        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
}
