<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 13.08.18
 * Time: 13:45
 */

require_once  "functions.php";
require_once "Member.php";

class MemberHistory extends Member
{
    private $history; // История изменения ФИО
    private $nation_history;
    private $Percent = null; // Ассоциативный Массив с кастомными процентами.

    /**
     * @param mixed $history
     */
    public function setHistory($history)
    {
        $this->history = $history;
    }

    /**
     * @return mixed
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * @param mixed $nation_history
     */
    public function setNationHistory($nation_history)
    {
        $this->nation_history = $nation_history;
    }

    /**
     * @return mixed
     */
    public function getNationHistory()
    {
        return $this->nation_history;
    }

    /**
     * @param array $Percent
     */
    public function setPercent($Percent)
    {
        $this->Percent = $Percent;
    }

    /**
     * @return array
     */
    public function getPercent()
    {
        return $this->Percent;
    }


}