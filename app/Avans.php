<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 27.02.19
 * Time: 13:41
 */

require_once "app/MemberInterface.php";
//require_once 'app/AdministrativeBlock.php';
//require_once 'app/Retention.php';
require_once 'app/ProductionCalendar.php';
require_once 'app/AwhInterface.php';
require_once 'app/PayRollInterface.php';
require_once "app/Unscheduled.php";


class Avans
{
    /*
    * Дата
    */
    protected $date;
    /*
     * По какому пользователю получаю премию
     */
    protected $user_id;

    /*
     * Объект сотрудника
     */
    protected $member = NULL;

    /*
     *  Итого по административному блоку
     */
    protected $All_Administrative_Rubles = 0;
    /*
     *  Итого по удержаниям рублей
     */
    protected $All_Hold_Rubles = 0;

    /*
     * Пропуски в руб.
     */
    protected $All_Absence_Rub = 0;
    /*
     * Весь выплаченный аванс + аванс 2
     */
    protected $All_Salary = 0;

    /*
     * Выдана премия или нет
     */
    protected $payment_made = 0;

    /*
     * Административный блок
     */
    protected $AB= null;

    /*
     * Удержания
     */
    protected $RN = null;

    /*
     * УРВ
     */

    /*
     * Объект урв
     */
    protected $AWH=null;

    /*
     * Отсутсвтия
     */
    protected $awh_absence = null;
    protected $awh_new_absence = null;
    /*
     * Отпуск
     */
    protected $awh_holiday = null;
    /*
     * Болень
     */
    protected $awh_disease = null;
    /*
     * Заметка
     */
    private $awh_note = null;

    /*
     * Количество рабочих дней
     */
    protected $working_days = 0;

    /*
     * Функция извлечения всех данных для расчета
     * Необходим member и user_id
     */
    public function fetch($user_id) {
        /*
         * **************    Обнуляю переменные
         */
        $this->awh_holiday = 0;
        $this->awh_disease = 0;
        $this->awh_absence = 0;
        $this->awh_new_absence = 0;
        $this->All_Absence_Rub = 0;
        $this->All_Salary = 0;
        $this->All_Administrative_Rubles = 0;
        $this->All_Hold_Rubles = 0;

        /*
         * Присваиваю id
         */
        $this->user_id = $user_id;


        // Создаем объект MemberInterface
        $MI = new MemberInterface();

        if (is_null($this->member)) {
            /*
             * Получаю основные данные по пользователю
             */
            $this->member = $MI->fetchMemberByID($this->user_id);
        }

        if ($this->member->getAvansPay() == 0) {
            throw new Exception("Данный сотрудник не получает аванс.");
        }

        $Us = new Unscheduled();
        if ($Us->checkForAvans($this->user_id, $this->date) == true) {
            return false;
        }

//        $SL = $MI->getSalaryByDate($this->user_id, $this->date);

//        $this->member->setSalary($SL['salary']);

//        $JP = $MI->getJobPostByDate($this->user_id,$this->date);
//        if ($JP) {
//            $this->member->setDepartment(["id"=>$JP["department_id"], "name"=>$JP["department_name"]]);
//        }



        // Только из премии вычитается!
//        if (!is_null($this->member->getYearbonusPay()) || $this->member->getYearbonusPay() != 0 ){
//            $this->member->setSalary($this->member->getSalary()-$this->member->getYearbonusPay());
//        }

        // ******************************* ИНИЦИАЛИЗАЦИЯ ОСНОВНЫХ ОБЪЕКТОВ (по табам)
        /*
         * Таб Административный объект
         */
//        $this->AB = new AdministrativeBlock();
        /*
         * Тип выплат аванс
         */
//        $this->AB->setTypeOfPayment(2);

        /*
         * Таб Удержания
         */
//        $this->RN = new Retention();
        /*
         * Тип выплат аванс
         */
//        $this->RN->setTypeOfPayment(2);

        /*
         * Создаю объект ведомости
         */
        $PI = new PayRollInterface();

        /*
         *  Выдана ли премия или нет
         */
//        $this->payment_made = $PI->checkPaid($this->user_id, $this->date, 2);
        $RS = new ReportStatus();

        $Status = $RS->getFiltredStatus($this->date,3, $this->member->getDepartment()['id'])['status_id'];

        if ($this->payment_made == null) {
            if ($Status > 1) {
                $this->payment_made = true;
            } else {
                $this->payment_made = false;
            }
        } else {
            $this->payment_made = false;
        }


            /*
             *  Получаю данные по административному блоку
             */
//        $this->AB->fetch($this->user_id, $this->date);
            /*
             *  Получаю данные по удержаниям
             */
//        $this->RN->fetch($this->user_id, $this->date);

            /*
             *  Создаю объект производственный календарь
             */
            $PC = new ProductionCalendar();
            /*
             * Получаю рабочие дни из производственного календаря
             */
//        $PC->fetchByDate($this->date);
            $PC->fetchByDateWithDepartment($this->date, $this->member->getDepartment()['id']);

            /*
            * Извлекаю массив с рабочими днями
            */
//        $PC->fetchWorkingArray($this->date);
            $PC->fetchWorkingArrayWithDepartment($this->date, $this->member->getDepartment()['id']);
            /*
            * Получаю массив с рабочими днями
            */
            $working_days_array = $PC->getWDArray();
            /*
             * Получаю первую часть рабочих дней при котором сотрудник получает аванс
             */
            $working_part = array_splice($working_days_array, 0, (ceil(count($working_days_array) / 2)));


            /*
             * Разбираю дату приема на работу
             */
            if ($this->member->getEmploymentDate()) {
                $emp_date_arr = explode(".", $this->member->getEmploymentDate());
            }

            /*
             *  Создаю объект УРВ
             */
            $this->AWH = new AwhInterface();
            /*
             *  Получаю данные  по отсутствиям по премии
             */
            $member_awh = $this->AWH->getAwhByMember($this->date, 2, $this->user_id);

            if ($member_awh) {
                $this->awh_new_absence = $member_awh['new_absence'];
                $this->awh_absence = $member_awh['absence'];
                $this->awh_holiday = $member_awh['holiday'];
                $this->awh_disease = $member_awh['disease'];
                $this->awh_note = $member_awh['note'];
            };

            /*
             * Если приянт в этом месяце и при этом ПЕРСОНАЛ не подал данные по отсутствиям, то пробую разобрать самому
             */
            if (($emp_date_arr[1] . "." . $emp_date_arr[2] == $this->date && $emp_date_arr[0] != 1) && $this->awh_new_absence == 0) {
                foreach ($working_part as $wp) {
                    if ($emp_date_arr[0] != $wp) {
                        $this->awh_new_absence += 1;
                    } else {
                        break;
                    }
                }

                $this->AWH->setNewAbsenceByMember($this->user_id, $this->date, 2, $this->awh_new_absence);
            }

            /*
             *  Если пустое значение, то считаю что их нет
             */
            if (is_null($this->awh_absence)) {
                $this->awh_absence = 0;
            }
            if (is_null($this->awh_holiday)) {
                $this->awh_holiday = 0;
            }
            if (is_null($this->awh_disease)) {
                $this->awh_disease = 0;
            }

            /*
             *  Если мы получили количество рабочих дней в этом месяце, то расчитываем пропуски
             */
            if (!empty($PC->get()["working_days"])) {
                // Расчитываю сумму отсутствия в рублях по формуле ( КОЛИЧЕСТВО_ПРОПУСКОВ * (ОКЛАД / КОЛИЧЕСТВО_РАБОЧИХ_ДНЕЙ))
//            var_dump($PC->get()["working_days"]);
                $this->All_Absence_Rub = ($this->awh_absence + $this->awh_new_absence) * (($this->member->getSalary()) / $PC->get()["working_days"]); // пропуск * За один день.
            } else {
                $this->All_Absence_Rub = 0;
            }

            /*
             * Количество рабочих дней
             */
            $this->working_days = $PC->get()["working_days"];

            /*
             * Расчет аванса (Оклад * % / 100)
             */
        if ($this->payment_made == true) {
            $raw = $PI->getFinancialPayout($this->user_id, $this->date, 2);
            $this->member->setSalary($raw['Salary']);

            /*
             *  Если мы получили количество рабочих дней в этом месяце, то расчитываем пропуски
             */
            if (!empty($PC->get()["working_days"])) {
                // Расчитываю сумму отсутствия в рублях по формуле ( КОЛИЧЕСТВО_ПРОПУСКОВ * (ОКЛАД / КОЛИЧЕСТВО_РАБОЧИХ_ДНЕЙ))
//            var_dump($PC->get()["working_days"]);
                $this->All_Absence_Rub = ($this->awh_absence + $this->awh_new_absence) * (($this->member->getSalary()) / $PC->get()["working_days"]); // пропуск * За один день.
            } else {
                $this->All_Absence_Rub = 0;
            }

            $this->All_Salary = $raw['Total'];
        } else {
            $this->All_Salary = ($this->member->getSalary() * $this->member->getAvansProcent() / 100);
        }
            /*
             * Извлекаю данные и заношу в переменные
             */
//        $this->All_Administrative_Rubles = $this->AB->getSumm();
//        $this->All_Hold_Rubles = $this->RN->getSumm();
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getPaymentMade()
    {
        return $this->payment_made;
    }

    /*
     * Получаю итоговую сумму
     */
    public function getSummary() {
        // Аванс + Административная_премия - Удержания - Опоздания
        return $this->All_Salary + $this->All_Administrative_Rubles - $this->All_Hold_Rubles - $this->All_Absence_Rub;
    }

    /*
     * Получаю класс AB
     */
//    public function getAB() {
//        return $this->AB;
//    }

    /*
     * Получаю класс RN
     */
//    public function getRN() {
//        return $this->RN;
//    }

    /*
     * Получаю класс AWH
     */
    public function getAwh() {
        if (is_null($this->AWH)) {
            $this->AWH = new AwhInterface();
        }
        return $this->AWH;
    }

    /*
     * Получаю рабочий статус
     */
    public function getWorkStatus() {
        return $this->member->getWorkStatus();
    }

    /*
     * Устанавилваю объект сотрудника
     */
    public function setMember($member) {
        $this->member = $member;
        $this->user_id = $this->member->getId();
    }

    /**
     * @return int
     */
    public function getWorkingDays()
    {
        return $this->working_days;
    }


    /**
     * @return int
     */
    public function getAllHoldRubles()
    {
        return $this->All_Hold_Rubles;
    }

    /**
     * @return int
     */
    public function getAllSalary()
    {
        return $this->All_Salary;
    }

    /**
     * @return int
     */
    public function getAllAbsenceRub()
    {
        return $this->All_Absence_Rub;
    }

    /**
     * @return int
     */
    public function getAllAdministrativeRubles()
    {
        return $this->All_Administrative_Rubles;
    }

    /*
     * Устанавливаю опоздания
     */
    public function getAbsence() {
        return $this->awh_absence;
    }

    public function getNewAbsence() {
        return $this->awh_new_absence;
    }
    /*
     * Получаю выходные
     */
    public function getHoliday() {
        return $this->awh_holiday;
    }

    /*
     * Получаю болезни
     */
    public function getDisease() {
        return $this->awh_disease;
    }

    public function getAwhNote() {
        return $this->awh_note;
    }

    public function setAwhNote($awh_note) {
        $this->awh_note = $awh_note;
    }
};