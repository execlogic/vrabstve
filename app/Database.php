<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 16.08.18
 * Time: 14:42
 */

class Database
{
    private $dbh = null;
    private $sth;
    private $type;
    private $host;
    private $port;
    private $dbname;
    private $db_user;
    private $db_password;
    private $db_options;

    public function __construct()
    {
        $config = include("config/config.php");

        $this->type = $config['db_type'];
        $this->host = $config['db_host'];
        $this->port = $config['db_port'];
        $this->dbname = $config['db_name'];
        $this->db_user = $config['db_user'];
        $this->db_password = $config['db_password'];
        $this->db_options = $config['db_options'];
    }

    public function dbConnect( $errors = true ) {
        try {
            $this->dbh = new PDO(
                $this->db_type .
                ':host=' . $this->db_host .
                ';port=' . $this->db_port .
                ';dbname=' . $this->db_name,
                $this->db_user,
                $this->db_password,
                $this->db_options
            );
        } catch (PDOException $e) {
            if ( $errors === true ) {
                echo "\nPDO::errorInfo():\n";
                print $e->getMessage();

            }
        }
    }

    public function prepare($query) {
        try {
            $this->sth = $this->dbh->prepare($query);
        }catch (PDOException $e) {
            echo "\nPDO::errorInfo():\n";
            print $e->getMessage();
            $this->sth = null;
            return false;
        }
        return true;
    }

    public function execute($parametrs) {
        try {
            $this->sth->execute([$parametrs]);
        } catch (PDOException $e) {
            print $e->getMessage();
            return false;
        }
        return true;
    }

    public function fetch() {
        return $this->sth->fetch();
    }

    public function fetchAll() {
        return $this->sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function close(){
        $this->dbh = null;
    }
}