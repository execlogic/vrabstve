<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 27.02.19
 * Time: 16:33
 */

require_once 'app/Avans.php';
require_once "app/MemberInterface.php";
require_once 'app/CustomPercent.php';

require_once "app/MemberFinanceBlock.php";
require_once 'app/DataItem.php';
require_once 'app/DataManager.php';
require_once 'app/ErrorInterface.php';
require_once 'app/AdministrativeBlock.php';
require_once 'app/Retention.php';
require_once 'app/Correction.php';

require_once 'app/ProductionCalendar.php';
require_once 'app/AwhInterface.php';

require_once 'app/PayRollInterface.php';

require_once 'app/TransportBlock.php';
require_once 'app/MobileBlock.php';


class Avans2 extends Avans
{
    /*
    * Коммеческий блок для НДФЛ и ИЛ
    */
    private $MFB = NULL;

    /*
    * НДФЛ + ИЛ
    */
    private $ndfl = 0;
    private $ro = 0;

    /*
    * Блок корректировок
    */
    private $CR = null;

    /*
     * Административный блок
     */
    protected $AB= null;

    /*
     * Удержания
     */
    protected $RN = null;

    /*
     * Транспорт
     */
    private $TR = null;

    /*
     * Связь
     */
    private $MB = null;


    /*
     * Корректировки в рублях
     */
    private $All_Correction_Rubles = 0;

    /*
     * Итого по Транспорту
     */
    private $All_Transport_Rubles = 0;

    /*
     * Итого по Связь
     */
    private $All_Mobile_Rubles = 0;


    /*
    * Отчисления в годовой бонус
    */
    private $year_bonuspay = 0;
    /*
     * Заметка
     */
    private $awh_note = null;
    private $awh_extraholiday = null;

    /*
     * мотивация пользователя
     */
    private $motivation;

    /*
    * Lock в зависимости от статуса ведомости
    */
    private $Administrative_lock = 0;
    private $Retention_lock = 0;
    private $Correcting_lock = 0;
    private $AWH_lock = 0;

    /*
     * Функция извлечения всех данных для расчета
     * Необходим member и user_id
     */
    public function fetch($user_id, $payment_made=null) {
        /*
         * **************    Обнуляю переменные
         */
        $this->awh_holiday = 0;
        $this->awh_disease = 0;
        $this->awh_absence = 0;
        $this->awh_new_absence = 0;
        $this->All_Absence_Rub = 0;
        $this->All_Salary = 0;
        /*
         *  Итого по административному блоку
         */
        $this->All_Administrative_Rubles = 0;
        /*
         *  Итого по удержаниям рублей
         */
        $this->All_Hold_Rubles = 0;
        /*
         *  Корректировка в рублях
         */
        $this->All_Correction_Rubles = 0;
        /*
         * НДФЛ
         */
        $this->ndfl = 0;
        /*
         * ИЛ
         */
        $this->ro = 0;

        /*
         * Присваиваю id
         */
        $this->user_id = $user_id;


        // Создаем объект MemberInterface
        $MI = new MemberInterface();

        if (is_null($this->member)) {
            /*
             * Получаю основные данные по пользователю
             */
            $this->member = $MI->fetchMemberByID($this->user_id);
        }

        /*
         * Получаю мотивацию в переменую (зачем?)
         */
        $this->motivation = $MI->getMotivation($this->user_id);
        /*
         * Если у пользователя не выбрана мотивация, то считаем что мотивация произвольная
         */
        if ($this->member->getMotivation() == null) {
            $this->member->setMotivation(6);
        }

        // ******************************* ИНИЦИАЛИЗАЦИЯ ОСНОВНЫХ ОБЪЕКТОВ (по табам)
        /*
         * Таб Административный объект
         */
        $this->AB = new AdministrativeBlock();
        /*
         * Тип выплат аванс
         */
        $this->AB->setTypeOfPayment(3);

        /*
         * Таб Удержания
         */
        $this->RN = new Retention();
        /*
         * Тип выплат аванс
         */
        $this->RN->setTypeOfPayment(3);

        /*
         * Таб Корректировки
         */
        $this->CR = new Correction();
        /*
        * Тип выплат премия
        */
        $this->CR->setTypeOfPayment(3);

        /*
        * Транспорт
        */
        $this->TR = new TransportBlock();
        $this->TR->getTransport($this->date, 3, $this->user_id);
        $this->All_Transport_Rubles = $this->TR->getSumm();

        /*
         * Связь
         */
        $this->MB = new MobileBlock();
        $this->MB->getMobile($this->date, 3, $this->user_id);
        $this->All_Mobile_Rubles = $this->MB->getSumm();

        /*
         * Создаю объект ведомости
         */
        $PI = new PayRollInterface();
        /*
         * Выдана ли премия или нет
         */
        $RS = new ReportStatus();
        $Status = $RS->getFiltredStatus($this->date,3, $this->member->getDepartment()['id'])['status_id'];

        if ($Status >= 4) {
            $this->payment_made = true;
            $this->Administrative_lock=1;
            $this->Retention_lock=1;
            $this->Correcting_lock=1;
            $this->AWH_lock=1;
            $this->Commercial_lock = 1;
            $this->kpi_lock = 1;

        }

        if ($Status > 1) {
            $this->MFB = new MemberFinanceBlock($this->member);
            /*
            *  Получаю данные по административному блоку
            */
            $this->AB->fetch($this->user_id, $this->date);
            /*
             *  Получаю данные по удержаниям
             */
            $this->RN->fetch($this->user_id, $this->date);
            /*
             *  Получаю данные по корректировкам
             */
            $this->CR->fetch($this->user_id, $this->date);

            $raw = $PI->getFinancialPayout($this->user_id, $this->date, 3);
            $this->motivation = $raw['motivation'];
            $this->ndfl = $raw['ndfl'];
            $this->ro = $raw['ro'];

            /*
            *  Создаю объект УРВ
            */
            $this->AWH = new AwhInterface();
            $this->setAwhExtraholiday($this->AWH->getExtraHoliday($this->date, $this->user_id));

            $member_awh = $this->AWH->getAwhByMember($this->date, 3, $this->user_id); //Премия
            $this->awh_new_absence = $member_awh['new_absence'];
            $this->awh_absence = $member_awh['absence'];
            $this->awh_holiday = $member_awh['holiday'];
            $this->awh_disease = $member_awh['disease'];
            $this->awh_note = $member_awh['note'];

            $this->All_Absence_Rub = $raw['absence'];
            $this->All_Salary = $raw['PrepaidExpense'];

            /*
             * Административный блок
             */
            $this->All_Administrative_Rubles = $raw['AdministrativePremium'];

            /*
             * Удержания
             */
            $this->All_Hold_Rubles = $raw['Hold'];

            /*
             * Корректировки
             */

            $this->All_Correction_Rubles = $raw['Correcting'];

            $this->member->setYearbonusPay($raw['YearBonusPay']);

        } else {
            /*
             * Коммерческий блок с бонусами и корректировками. В Зависимости от мотивации получаю данные их разных таблиц.
             */
            $this->MFB = new MemberFinanceBlock($this->member); // Получение данных наценки в зависимости от мотивации, передаю Объект с данными по пользователю

            /*
             *  Получаю данные по административному блоку
             */
            $this->AB->fetch($this->user_id, $this->date);
            /*
             *  Получаю данные по удержаниям
             */
            $this->RN->fetch($this->user_id, $this->date);
            /*
             *  Получаю данные по корректировкам
             */
            $this->CR->fetch($this->user_id, $this->date);

            /*
             *  Извлекаю NDFL за дату по пользователю переданному как объект
             */
            $this->MFB->fetch_NdflByDate($this->date);
            /*
             *  получаю NDFL в массив
             */
            $member_ndfl = $this->MFB->getNdfl();
            $this->ndfl = $member_ndfl['ndfl'];
            $this->ro = $member_ndfl['ro'];

            /*
             *  Если null значение административной премии и есть в профиле, то выставляю из профиля
             */
            if (is_null($this->AB->getAdministrativePrize())) {
                $this->AB->setAdministrativePrizeFin($this->user_id);
            }

            /*
             *  Если null значение Мобильной связи и есть в профиле, то выставляю из профиля
             */
            if (is_null($this->MB->getSumm())) {
                $this->MB->setSumm($this->member->getMobileCommunication());
            }

            if (is_null($this->TR->getSumm())) {
                $this->TR->setSumm($this->member->getTransport());
            }

            /*
             *  Создаю объект производственный календарь
             */
            $PC = new ProductionCalendar();
            /*
             * Получаю рабочие дни из производственного календаря
             */
            $PC->fetchByDateWithDepartment($this->date, $this->member->getDepartment()['id']);


            /*
             *  Создаю объект УРВ
             */
            $this->AWH = new AwhInterface();
            /*
             *  Получаю данные  по отсутствиям по премии
             */
            $this->setAwhExtraholiday($this->AWH->getExtraHoliday($this->date, $this->user_id));
//        $this->member_awh = $this->AWH->getAwhByMember($this->date, 1, $this->user_id); //Премия
            $member_awh = $this->AWH->getAwhByMember($this->date, 1, $this->user_id); //Премия
            $this->awh_new_absence = $member_awh['new_absence'];
            $this->awh_absence = $member_awh['absence'];
            $this->awh_holiday = $member_awh['holiday'];
            $this->awh_disease = $member_awh['disease'];
            $this->awh_note = $member_awh['note'];

            /*
            * Разбираю дату приема на работу
            */
            if ($this->member->getEmploymentDate()) {
                if (strpos($this->member->getEmploymentDate(), "-") != false) {
                    $emp_date_arr = explode("-", $this->member->getEmploymentDate());
                } else {
                    $emp_date_arr = explode(".", $this->member->getEmploymentDate());
                }
            }

            if (($emp_date_arr[1] . "." . $emp_date_arr[2] == $this->date && $emp_date_arr[0] != 1) && $this->awh_new_absence == 0) {
                $PC->fetchWorkingArrayWithDepartment($this->date, $this->member->getDepartment()['id']);
                $working_days_array = $PC->getWDArray();

                foreach ($working_days_array as $wp) {
                    if ($emp_date_arr[0] != $wp) {
                        $this->awh_new_absence += 1;
                    } else {
                        break;
                    }
                }

                $this->AWH->setNewAbsenceByMember($this->user_id, $this->date, 1, $this->awh_new_absence);
            }

            /*
             *  Если пустое значение, то считаю что их нет
             */
            if (is_null($this->awh_holiday)) {
                $this->awh_holiday = 0;
            }
            if (is_null($this->awh_disease)) {
                $this->awh_disease = 0;
            }
            $vse_otsutstviya = $this->awh_absence + $this->awh_new_absence + $this->awh_holiday + $this->awh_disease + $this->awh_extraholiday;
            $this->working_days = $PC->get()["working_days"];

            /*
             * Расчет Транспортных расходов исходя из рабочих дней
             */
            if ($this->TR->getSumm() <= 0  && $this->member->getTransport()>0) {
                if ($this->member->getTransportFormula() == 1) {
                    $this->TR->setSumm($this->member->getTransport());
                    $this->TR->recountCompensationFAL($vse_otsutstviya, $this->working_days);
                    $this->All_Transport_Rubles = $this->TR->getSumm();
                } else {
                    $this->TR->setTransport($this->date, 3, $this->user_id,$this->member->getTransport(), "", -1);
                    $this->All_Transport_Rubles = $this->TR->getSumm();
                }
            } else {
                if ($this->member->getTransportFormula() == 1) {
                    if ($this->TR->isAuto($this->date, 3, $this->user_id) == true) {
                        $this->TR->setSumm($this->member->getTransport());
                        $this->TR->recountCompensationFAL($vse_otsutstviya, $this->working_days);
                    }
                }
            }

            /*
             * Расчет Мобильной свззи исходя из рабочих дней
             */
            if ($this->MB->getSumm() <= 0 && $this->member->getMobileCommunication()>0 ) {
                if ($this->member->getMobileFormula() == 1) {
                    $this->MB->setSumm($this->member->getMobileCommunication());
                    $this->MB->recountMobile($vse_otsutstviya, $this->working_days);
                    $this->All_Mobile_Rubles = $this->MB->getSumm();
                } else {
                    $this->MB->setMobile($this->date, 3, $this->user_id, $this->member->getMobileCommunication(), "", -1);
                    $this->All_Mobile_Rubles = $this->MB->getSumm();
                }
            } else {
                if ($this->member->getMobileFormula() == 1) {
                    if ($this->MB->isAuto($this->date, 3, $this->user_id) == true) {
                        $this->MB->setSumm($this->member->getMobileCommunication());
                        $this->MB->recountMobile($vse_otsutstviya, $this->working_days);
                    }
                }
            }

            /*
             *  Если мы получили количество рабочих дней в этом месяце, то расчитываем пропуски
             */
            if ((!empty($PC->get()["working_days"])) && (!is_null($this->awh_absence) || !is_null($this->awh_new_absence)) ) {
                /*
                 *  Расчитываю сумму отсутствия в рублях по формуле ( КОЛИЧЕСТВО_ПРОПУСКОВ * (ОКЛАД / КОЛИЧЕСТВО_РАБОЧИХ_ДНЕЙ))
                 */
                $this->All_Absence_Rub = ($this->awh_absence + $this->awh_new_absence) * ($this->member->getSalary() / $PC->get()["working_days"]); // пропуск * За один день.
            } else {
                $this->All_Absence_Rub = 0;
            }

            $PrepaidExpenseArray = $PI->getPrepaidExpense($this->user_id, $this->date);

            /*
             *  Выплаченный аванс
             */
            if (isset($PrepaidExpenseArray)) {
                $this->All_Salary = $PrepaidExpenseArray['Total']; // Выплаченный аванс
            } else {
                $this->All_Salary = 0;
            }

            /*
             *  Если пустота, то ставлю 0
             */
            if (empty($this->All_Salary)) {
                $this->All_Salary = 0;
            }

            /*
             * Извлекаю данные и заношу в переменные
             */
            /*
             * Административный блок
             */
            $this->All_Administrative_Rubles = $this->AB->getSumm();
            /*
             * Удержания
             */
            $this->All_Hold_Rubles = $this->RN->getSumm();
            /*
             * Корректировки
             */
            $this->All_Correction_Rubles = $this->CR->getSumm();

            /*
             * Отчисления в годовой бонус
             */
            $Raw = $MI->getYearBonusPay($this->member->getId());
            $this->member->setYearbonusType($Raw['type']);
            $this->member->setYearbonusPay($Raw['summ']);

            switch ($this->member->getYearbonusType()) {
                case 1:
                    $this->year_bonuspay = $this->member->getYearbonusPay();
                    break;
                case 2:
                    $tmp = explode(".", $this->date);
                    if ($tmp[0] >= 3 && $tmp[0] <= 11) {
                        $this->year_bonuspay = ($this->member->getSalary() + $this->All_Administrative_Rubles) * $this->member->getYearbonusPay();
                    }
                    break;
            }
        }
    }

    /*
     * Получаю итоговую сумму
     */
    public function getSummary() {
        // Оклад - НДФЛ - ИЛ - АВАНС - Опоздания + Административная_премия - Удержания + Корректировки
        return $this->getSalary() - $this->ndfl - $this->ro - $this->All_Salary - $this->All_Absence_Rub + $this->All_Administrative_Rubles - $this->All_Hold_Rubles + $this->All_Correction_Rubles - $this->year_bonuspay + $this->All_Mobile_Rubles + $this->All_Transport_Rubles;
    }

    /*
     * Получаю класс CR
     */
    public function getCR() {
        return $this->CR;
    }

    /*
     * Получаю класс AB
     */
    public function getAB() {
        return $this->AB;
    }

    /*
     * Получаю класс AB
     */
    public function getRN() {
        return $this->RN;
    }

    /*
    * Получаю класс TR
    */
    public function getTR() {
        return $this->TR;
    }

    /*
    * Получаю класс MB
    */
    public function getMB() {
        return $this->MB;
    }

    /*
     * Получаю НДФЛ
     */
    public function getNdfl() {
        return $this->ndfl;
    }

    /*
     * Получаю ИЛ
     */
    public function getRo() {
        return $this->ro;
    }

    /**
     * @return int
     */
    public function getAllCorrectionRubles()
    {
        return $this->All_Correction_Rubles;
    }

    /**
     * @return int
     */
    public function getSalary()
    {
        return $this->member->getSalary();
    }

    public function getYearBonusPay() {
        return $this->year_bonuspay;
    }

    /*
     * Устанавливаю опоздания
     */
    public function setAbsence($absence) {
        $this->awh_absence = $absence;
    }
    /*
     * Устанавливаю выходные
     */
    public function setHoliday($holiday) {
        $this->awh_holiday = $holiday;
    }

    public function getAB_Data() {
        return (["Promotions"=>$this->AB->getPromotions(), "AdministrativePrize"=>$this->AB->getAdministrativePrize(), "Training"=>$this->AB->getTraining(), "PrizeAnotherDepartment"=>$this->AB->getPrizeAnotherDepartment(), "Defect"=>$this->AB->getDefect(), "Sunday"=>$this->AB->getSunday(), "NotLessThan"=>$this->AB->getNotLessThan(), "MobileCommunication"=>$this->MB->getSumm(), "Overtime"=>$this->AB->getOvertime(),  "CompensationFAL"=>$this->TR->getSumm()]);
    }

    public function getRN_Data() {
        return (["LateWork"=>$this->RN->getLateWork(), "Internet"=>$this->RN->getInternet(), "CachAccounting"=>$this->RN->getCachAccounting(), "Receivables"=>$this->RN->getReceivables(),  "Credit"=>$this->RN->getCredit(), "Other"=>$this->RN->getOther()]);
    }

    public function getCR_Data() {
        return (["BonusAdjustment"=>$this->CR->getBonusAdjustment(), "Calculation"=>$this->CR->getCalculation(), "YearBonus"=>$this->CR->getYearBonus()]);
    }

    public function getAllTransport() {
        return $this->All_Transport_Rubles;
    }

    public function getAllMobile() {
        return $this->All_Mobile_Rubles;
    }
    /**
     * @return mixed
     */
    public function getMotivation()
    {
        return $this->motivation;
    }

    /**
     * @return int
     */
    public function getAdministrativeLock()
    {
        return $this->Administrative_lock;
    }
    /**
     * @return int
     */
    public function getCorrectingLock()
    {
        return $this->Correcting_lock;
    }

    /**
     * @return int
     */
    public function getRetentionLock()
    {
        return $this->Retention_lock;
    }
    /**
     * @return int
     */
    public function getAWHLock()
    {
        return $this->AWH_lock;
    }

    /**
     * @return null
     */
    public function getAwhExtraholiday()
    {
        return $this->awh_extraholiday;
    }

    public function setAwhExtraholiday($holiday) {
        $this->awh_extraholiday = $holiday;
    }

    public function getYearBonus() {
        return $this->year_bonus;
    }

    /*
     * Устанавливаю НДФЛ
     */
    public function setNdfl($ndfl) {
        $this->ndfl = $ndfl;
    }

    /*
     * Устанавливаю ИЛ
     */
    public function setRo($ro) {
        $this->ro = $ro;
    }

}