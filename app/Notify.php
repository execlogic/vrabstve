<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 16.01.19
 * Time: 16:09
 */

require_once 'functions.php';

class Notify
{
    private $dbh = null;

    public function getCount($member_id)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT count(*) as count FROM notify where member_id=?";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос


            if ($sth->rowCount() > 0) {
                $count = $sth->fetch(PDO::FETCH_ASSOC)['count'];
                return $count;
            } else {
                return 0;
            }

        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }
    }

    public function getMessages($member_id)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT * FROM notify where member_id=?";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос


            if ($sth->rowCount() > 0) {
                $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
                return $raw;
            }
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        return null;
    }

    public function cleanNotify($member_id)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "delete from notify where member_id=?";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос


            if ($sth->rowCount() > 0) {
                return true;
            }
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        return false;
    }

    public function addNotify($member_id, $subject, $message)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "INSERT into notify (member_id, subject, message, date) VALUES (?, ?, ?, NOW())";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id, $subject, $message]); // Выполняем запрос
            if ($sth->rowCount() == 0 ){
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}