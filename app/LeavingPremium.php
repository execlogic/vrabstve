<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 21.01.19
 * Time: 17:57
 */

require_once "app/functions.php";

require_once "app/MemberInterface.php";
require_once 'app/CustomPercent.php';

require_once 'app/ProductionCalendar.php';
require_once 'app/AwhInterface.php';
require_once 'app/PayRollInterface.php';
require_once 'app/PayRoll.php';
require_once 'app/ReportStatus.php';
require_once "app/EnvelopeInterface.php";

require_once "app/MemberFinanceBlock.php";
require_once 'app/DataItem.php';
require_once 'app/DataManager.php';
require_once 'app/ErrorInterface.php';
require_once 'app/AdministrativeBlock.php';
require_once 'app/Retention.php';
require_once 'app/Correction.php';

require_once 'app/Notify.php';
require_once "admin/RoleInterface.php";

require_once 'app/Premium.php';

class LeavingPremium
{
    private $dbh = null;
    private $PayRollArray = array();

    public function checkStatus($member_id) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from report_status where member_id = ? and type_of_payment = 4 and status_id>1 and active = 1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) { // Если число строк > 0
            return true;
        } else {
            return false;
        }
    }

    public function getPremium() {
        $this->PayRollArray = array();

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        /*
        * Создаю объект интерфейса ведомости
        */
        $PI = new PayRollInterface();
        /*
        * Создаю объект MemberInterface
        */
        $MI = new MemberInterface();


        $query = "select t1.*,DATE_FORMAT(t1.date, '%m.%Y') as date,t2.name,t2.lastname,t2.middle,t3.summ,t3.note,t4.name as status_name
          FROM report_status as t1
          LEFT JOIN member as t2 ON (t1.member_id = t2.id)
          LEFT JOIN unscheduled as t3 ON (t1.unscheduled_id=t3.id)
          LEFT JOIN status as t4 on (t1.status_id = t4.id)
          WHERE t1.type_of_payment = 4 
          AND t1.active=1  AND t1.status_id < 6";


        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute(); // Выполняю запрос
        } catch (PDOException $e) {
            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
        }

        /*
         *  Если количество строк больше 0, то расчитываю сотрудника
         */
        if ($sth->rowCount() > 0) {
            $rawLeaving = $sth->fetchAll();

            foreach ($rawLeaving as $item) {
                $m = $item['date'];
                $user_id = $item['member_id'];
                $report_id = $item['id'];
                $status_name = $item;
                try {
                    $member = $MI->fetchMemberByID($user_id);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }

                $PR = new Premium();
                $PR->setMember($member);
                $PR->setDate($m);

                $PR->fetch($user_id, true);

                $PayRoll = new PayRoll();
                $PayRoll->setDate($m); // Дата
                $PayRoll->setTypeOfPayment(4); // Тип выплаты
                $PayRoll->setId($member->getId()); // Пользовательский ID

                $PayRoll->setDirection($member->getDirection()); // Напраление
                $PayRoll->setDepartment($member->getDepartment()); // Отдел
                $PayRoll->setPosition($member->getPosition()); // Должность


                $PayRoll->setFio($member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()); // ФИО
                $PayRoll->setSalary($member->getSalary()); // Оклад
                $PayRoll->setKPI($PR->getKPISumm());

                $PayRoll->setNDFL($PR->getNdfl()); // НДФЛ
                $PayRoll->setRO($PR->getRo()); // ИЛ
                $PayRoll->setAdvancePayment($PR->getAllSalary()); // АВАНС

                $PayRoll->setAwhdata(["absence" => ($PR->getAbsence() + $PR->getNewAbsence()), "holiday" => $PR->getHoliday(), "disease" => $PR->getDisease()]);
                $PayRoll->setAdministrativeData($PR->getAB_Data());
                $PayRoll->setRetentionData($PR->getRN_Data());
                $PayRoll->setCorrectionData($PR->getCR_Data());
                $PayRoll->setTransport($PR->getAllTransport());
                $PayRoll->setMobile($PR->getAllMobile());

                $PayRoll->setAbsences($PR->getAllAbsenceRub()); // Отсутствия
                if ($member->getMotivation() == 7) {
                    $PayRoll->setCommercial(0); // Коммерческая премия
                } else {
                    $PayRoll->setCommercial($PR->getAllMarkupRubles()); // Коммерческая премия
                }
                $PayRoll->setAdministrative($PR->getAllAdministrativeRubles()); // Административная премия
                $PayRoll->setHold($PR->getAllHoldRubles()); // Удержания
                $PayRoll->setCorrecting($PR->getAllCorrectionRubles()); // Корректировки
                $PayRoll->setKPI($PR->getKPISumm());
                $PayRoll->setData($PR->getCommercialJson());

                $PayRoll->setYearbonusPay($PR->getYearBonusPay());

                $PayRoll->setBaseprocentId($PR->getBasepercentId());
                $PayRoll->setAdvancedProcentId($PR->getAdvancedPercentId());
                $PayRoll->setMotivation($member->getMotivation());
                $PayRoll->Calculation();

                $PayRoll->setStatusName($status_name['status_name']);
                $PayRoll->setStatusId($status_name['status_id']);

                //Добавляю в массив объектов
                $this->PayRollArray[$report_id] = $PayRoll;


            }

            return $this->PayRollArray;
        }
    }

    public function getPremiumCacher() {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT t1.id, t1.date, t1.type_of_payment, t1.status_id, t4.name as status_name, t3.lastname, t3.name, t3.middle, t2.Total
        FROM report_status as t1
        LEFT JOIN financial_payout as t2 ON (t1.member_id = t2.member_id and t2.type_of_payment=4 and t2.active=1)
        LEFT JOIN member as t3 on (t1.member_id = t3.id)
        LEFT JOIN status as t4 on (t1.status_id = t4.id)
        where t1.type_of_payment = 4 and t1.status_id = 4 and t1.active = 1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute(); // Выполняю запрос
        } catch (PDOException $e) {
            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
        }

        if ($sth->rowCount() > 0) {
            $raw = $sth->fetchAll();
            return $raw;
        }

        return NULL;
    }

    public function Cansel($ID) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from report_status where id = ? LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$ID]); // Выполняю запрос
        } catch (PDOException $e) {
            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
        }

        if ($sth->rowCount() > 0) {
            $rawData = $sth->fetch(PDO::FETCH_ASSOC);
//            $UnscheduledID = $rawData["unscheduled_id"];
//            $user_id = $rawData["member_id"];
            if ($rawData['status_id'] > 1) {
//                $query = "DELETE from report_status where id=?";
//            } else {
                $status = $rawData['status_id'] - 1;
                $query = "UPDATE report_status SET status_id=? where id=?";
//            }

                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$status, $ID]); // Выполняю запрос
                } catch (PDOException $e) {
                    echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                }

                if ($sth->rowCount() > 0) {
                    return true;
                }
            }
        }

        return false;
    }

    /*
     * Инкрементирую статус ведомости
     */
    public function IncStatus($ID)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        /*
        * Создаю объект статуса ведомости
        */
        $RS = new ReportStatus();

        /*
         * Объект ролей
         */
        $RI = new RoleInterface();

        /*
         * Получаю ID статуса
         */
        $status = $RS->getStatusByID($ID)['status_id'];

        if ($status > 1 || $status < 6) {

            $status++;

            /*
             * Если статус == 3, то добавляю в ведомость
             */
            if ($status == 3) {
                try {
                    $this->AddToPayment((int)$ID);
                }catch (Exception $e) {
                    echo $e->getMessage()."<BR>";
                }
            }

            /*
             * Инкрементирую статус
             */
            $RS->ChangeStatusLeavingByID($ID, $status);

            switch ($status) {
                /*
                * С НЛ на кассира
                */
                case 4:
                    $query = "SELECT DISTINCT(member_id) FROM zp.Roles where RoleSettings_id=3 OR RoleSettings_id=5 OR RoleSettings_id=6;";
                    try {
                        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                        $sth->execute();
                    } catch (PDOException $e) {
                        throw new Exception($e->getMessage());
                    }
                    $rawDirectors = $sth->fetchAll(PDO::FETCH_ASSOC);
                    break;
                /*
                 * С руководителя на НЛ, c Кассира на НЛ и с НЛ в расходы
                 */
                case 3:
                case 5:
                case 6:
                    $query = "SELECT DISTINCT(member_id) FROM zp.Roles where RoleSettings_id=5 OR RoleSettings_id=6;";
                    try {
                        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                        $sth->execute();
                    } catch (PDOException $e) {
                        throw new Exception($e->getMessage());
                    }
                    $rawDirectors = $sth->fetchAll(PDO::FETCH_ASSOC);
            }

            /*
            * Уведомление пользователей
            */
            $NF = new Notify();
//
            switch ($status) {
                case 3:
                    $subj = "Расчет сотрудника";
                    $msg = "Отвественные директора проверку закончили";
                    break;
                case 4:
                    $subj = "Расчет сотрудника";
                    $msg = "Ведомость передана кассиру";
                    break;
                case 5:
                    $subj = "Расчет сотрудника";
                    $msg = "Выдано";
                    break;
                case 6:
                    $subj = "Расчет сотрудника";
                    $msg = "Ведомость закрыта";
                    break;
            }

            foreach ($rawDirectors as $item) {
                if (!is_null($item['member_id'])) {
                    $NF->addNotify($item['member_id'], $subj, $msg);
                }
            }
        }
    }


    /*
    * Метод класса добавляет данные в FIX таблицу.
    * Выполняется когда статус меняется с 5 на 6
    */
    public function AddToPayment($ID) {
        $PI = new PayRollInterface();
        try {
            $this->getPremium();

            $PI->AddToFinancialPayout($this->PayRollArray[$ID]);
        } catch (Exception $e) {
            echo $e->getMessage()."<BR>";
        }
    }

}