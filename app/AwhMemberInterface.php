<?php
require_once 'functions.php';
require_once 'AwhInterface.php';
require_once 'MemberInterface.php';
require_once 'ProductionCalendar.php';

class AwhMemberInterface
{
    private $dbh;
    private $debug = false;
    /*
     * Месяц когда сгорают отпуска
     */
    public $HOLIDAY_LAST_YEAR = 4;
    /*
     * Максимальное количество дней отпуска
     */
    static $MAX_HOLIDAY = 20;
    /*
     * Максимальное количество дней болезни
     */
    static $MAX_DISEASE = 10;

    /*
     *
     */
    private $member_id = NULL;

    private $date = NULL;

    /*
     * Отпуск в этом году
     */
    private $holiday = 0;

    /*
     * Сумма отпуска за предыдущий год
     */
//    private $prevSumHoliday = 0;

    /*
     * Остаток болезни
     */
    public $diseaseBalance = 0;

    /*
     * Остаток отпуска
     */

    public $holidayBalance = 0;

    /*
     * Количество дней отпуска в текущем году
     */
    public $holidayDays = 0;

    /*
     * Данные УРВ
     */
    public $aData = [];

    /*
     *  Остаток отпуска на начало года
     */
    public $vacation_balance = 0;

    /*
    * Сумма отпуска за первые 4 месяца
    */
    public $sum_first_holiday = 0;

    /*
    * Сумма отпуска за период 4 по 12 месяц
    */
    public $sum_second_holiday = 0;
    
    /*
     * Расчет остатка отпуска
     */
    public $holiday_balance_calculation = 0;

    /*
    * Суммарное количество дней за свой счет
    */
    public $sumAbsence = 0;
    /*
     * Суммарное количество дней отпуска
     */
    public $sumHoliday = 0;
    /*
     * Суммарное количество дней болезни
     */
    public $sumDisease = 0;

    public $vacation_balance_calculation = 0;
    /*
     * Исключаем эти должности из отчетов
     */
    static public $positionBanList = [5, 15, 34, 36, 49];

    /*
    * Остаток отпуска на начало года
    */
    public function getVacationBalance($member_id, $year) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        $q = "select * from vacation_balance where date='".$year."-01-01' and member_id = ? LIMIT 1";
        $sth = $this->dbh->prepare($q);
        $sth->execute([$member_id]);

        if ($sth->rowCount()>0) {
            $raw = $sth->fetch(PDO::FETCH_ASSOC);
            return $raw['holiday'];
        }

        return false;
    }

    /*
     * Соханить остаток в базе
     */
    public function setVacationBalance($member_id, $year, $holiday) {

    }

    /*
     * Подсчет отпусков
     */
    public function calculateHoliday($member_id, $date) {
        if (is_null($this->date)) {
            $this->date = $date;
        }

        if (is_null($this->member_id)) {
            $this->member_id = $member_id;
        }

        $AWH = new AwhInterface();
        $MI = new MemberInterface();
        $aDate = explode("-", $date);
        if (count($aDate)>1) {
            $date = $aDate[0];
        }

        $member = $MI->fetchMemberByID($member_id);

        if ($member->getNotCountUrv()==1)
            return [];

        if (in_array($member->getPosition()['id'], self::$positionBanList)) {
            return [];
        }

        /*
         * Если текущая должность кладовщик или гибщик
         */
        $awh_absence_sklad = 0;
        if (in_array($member->getPosition()['id'], [13, 4])) {
            $member_jobpost = $MI->getAllJobPost($member_id);
            /*
             * То смотрим предыдущую должность, если они были, и есть в бан листе,  то дату приема заменяем датой перевода
             * и от нее считаем УРВ
             */
            if ( count($member_jobpost)>1 && in_array( $member_jobpost[(count($member_jobpost))-2]['position_id'], self::$positionBanList )) {
                $employment_date = DateTime::createFromFormat("d.m.Y", $member_jobpost[count($member_jobpost)-1]['date']);
            } else {
                $employment_date = DateTime::createFromFormat("d.m.Y", $member->getEmploymentDate());
            }

            $PC = new ProductionCalendar();
            /*
             * Получаю рабочие дни из производственного календаря
             */
            $PC->fetchByDateWithDepartment($employment_date->format("m.Y"), $member->getDepartment());
            /*
            * Извлекаю массив с рабочими днями
            */
            $PC->fetchWorkingArrayWithDepartment($employment_date->format("m.Y"), $member->getDepartment());
            /*
            * Получаю массив с рабочими днями
            */
            $working_days_array = $PC->getWDArray();
            /*
             * Расчет отсутствий в премию
             */
            foreach ($working_days_array as $wp) {
                if ($employment_date->format("d") != $wp ){
                    $awh_absence_sklad += 1;
                } else {
                    break;
                }
            }
            if ($this->debug)
                echo "Отсутствия склад: " . $awh_absence_sklad ."<BR>";
        } else {
            $employment_date = DateTime::createFromFormat("d.m.Y", $member->getEmploymentDate());
        }

        if ($member->GetId()==66) {
            $employment_date = DateTime::createFromFormat("d.m.Y", "24.01.2022");
        }

        if ($employment_date->format("Y") > $date) {
            return [];
        }

        $aData = $AWH->getAwhByMemberYear($member_id, $date);
        if (count($aData)>0)
            array_multisort(array_column($aData, 'date'), SORT_ASC, $aData);

        $dismissalDate = NULL;
        if ($member->getDismissalDate() != "01.01.9999") {
            $dismissalDate = DateTime::createFromFormat("d.m.Y", $member->getDismissalDate());
        }

        $leaving_days_of_absence = NULL;

        foreach ($aData as $key=>$value) {
            $current = explode("-", $value['date']);
            if ($employment_date->format("Y") == $current[0] && $employment_date->format("m") > $current[1]){
                unset($aData[$key]);
            }
            if (count($aDate)>1 && $current[1]>$aDate[1])
                break;

            if (!is_null($dismissalDate) && $dismissalDate->format("Y")==$current[0] && $dismissalDate->format("m")==$current[1]) {
                $PC = new ProductionCalendar();
                $PC->fetchWorkingArray($current[1].".".$current[0]);

                $work_days = $PC->getWDArray(); // раб. дни в этом месяце
                $count_work_days = count($work_days);
                $tmp = 0;
                foreach ($work_days as $work_key=>$day) {
                    if ($day == $dismissalDate->format("d")) {
                        $tmp = $work_key;
                        break;
                    }
                }
                $work_days = array_slice($work_days, 0, ($tmp+1) );
                if ( $value['absence'] >= ($count_work_days - count($work_days))  ) {
                    $leaving_days_of_absence = ($count_work_days - count($work_days));
                    $aData[$key]['absence'] = $value['absence'] - ($count_work_days - count($work_days));
                    $value['absence'] = $aData[$key]['absence'];
                }
            }
            $this->sumAbsence += $value['absence'];
            $this->sumHoliday += $value['holiday'];
            $this->sumDisease += $value['disease'];
        }
        if ($this->debug)
            echo "Болезни: ". $this->sumDisease. "<br>";

        $this->aData = $aData;

        /*
         * Получаю данные за предыдущий год и считаю сумму отпусков
         */
//        $prevYearAwh = $AWH->getAwhByMemberYear($member_id, ($date-1) );
//        $this->prevSumHoliday = 0;
//        foreach ($prevYearAwh as $item) {
//            $this->prevSumHoliday += $item['holiday'];
//        }

        /*
         * Получаю остаток отпуска
         */
        $this->vacation_balance = $AWH->getVacationBalance($member_id, $date);

        /*
         * Остаток отпуска за предыдущий год
         */
        $vacation_balance_calculation = $this->vacation_balance;
        if ($this->debug)
            echo "Остаток отпуска за предыдущий год: ".$this->vacation_balance."<BR>";

        /*
         * Живые люди
         */
        if ($member->getDismissalDate() == "01.01.9999" || $date<$dismissalDate->format("Y")) {
            if ($date < date("Y") || (date("m") == 12 && date("d") >= 20)) {
                $date_now = DateTime::createFromFormat("d.m.Y", "31.12." . $date);
            } else {
                $date_now = new DateTime("first day of this month");
            }
        } else {
            /*
             * Уволенные
             */
            if ($employment_date->format("Y") == $date) {
                $date_now = DateTime::createFromFormat("d.m.Y", $member->getDismissalDate());
            } else {
//                $PC = new ProductionCalendar();
//                $PC->fetchWorkingArray("01.".$date);
//                $work_days = $PC->getWDArray();
                try {
                    $employment_date = new DateTime("01.01." . $date);
//                    $employment_date = new DateTime($work_days[0] . ".01." . $date);
                } catch (Exception $e) {
                    $employment_date = new DateTime("01.01." . $date);
                }
                $date_now = DateTime::createFromFormat("d.m.Y", $member->getDismissalDate());
            }
        }

        if ($employment_date->format("Ymd") > $date_now->format("Ymd")) {
            return [];
        }
        if ($this->debug)
            echo "Дата начала отсчета: ".$employment_date->format("Y-m-d")."<BR>";
        $interval = $employment_date->diff($date_now);

        /*
         * Остаток отпуска на дату(следующий год)
         */
        if ($interval->y>0 || ($interval->y==0 && $interval->m>=6) ) {
            $this->holiday_balance_calculation = self::$MAX_HOLIDAY;
        } else {
            $this->holiday_balance_calculation = 0;
        }
        /*
         * Если остаток предыдущего года меньше 0 (отрицательное число)
         * то остаток отпуска на этот год уменьшаем на остаток прошлого года
         */
        if ($vacation_balance_calculation<0) {
            $this->holiday_balance_calculation += $vacation_balance_calculation;
        }

        if ($this->debug)
            echo "Остаток отпуска: " . $this->holiday_balance_calculation."<BR>";
        $calculateVacation = true;
        /*
         * Отпуск действует на следующий год
         * НЕ ПЕРЕЗАТИРАЕТ В ФОРМУЛЕ, ВСЁ ОК
         */
        if ($employment_date->format("Y")==($date-1) && $employment_date->format("m")>=7) {
            if ($this->debug)
                echo "Отпуск за пред.год.: ".$this->vacation_balance."<BR>";
            $this->holiday_balance_calculation += $this->vacation_balance;
            $calculateVacation = false;
        }
        if ($this->debug)
            echo "Остаток отпуска " . $this->holiday_balance_calculation."<BR>";
//        if ($member->getVacationNotExpire() && $member->getVacationNotExpireDate()) {
//            $vacation_not_expire_date = explode("-", $member->getVacationNotExpireDate());
//            if ($date == $vacation_not_expire_date[0]) {
//                $this->holiday_balance_calculation += $this->vacation_balance;
//                $calculateVacation = false;
//            }
//        }
        $aVacationExtensionDate = $member->getVacationExtensionDate();
//        $HOLIDAY_LAST_YEAR = $this->HOLIDAY_LAST_YEAR;
        if ($aVacationExtensionDate) {
            foreach ($aVacationExtensionDate as $item) {
                $vacation_extention_date = explode("-", $item['vacation_extension_date']);
                if ($vacation_extention_date[0]==$date) {
                    $HOLIDAY_LAST_YEAR = $vacation_extention_date[1];
                    $this->HOLIDAY_LAST_YEAR = $HOLIDAY_LAST_YEAR;
                }
            }
        }


        $formula = function () use ($AWH, $employment_date, $interval, $member_id, $member, $leaving_days_of_absence, $dismissalDate, $date, $date_now, $awh_absence_sklad) {
            $PC = new ProductionCalendar();
            if (!is_null($dismissalDate) && ($dismissalDate->format("Y") == $date) ) {
                if ($this->debug)
                    echo "Обнаружена дата увольнения ".$dismissalDate->format("Y-m-d")."<br>";
                $PC->fetchByDateWithDepartment($dismissalDate->format("m.Y"), $member->getDepartment()['id']);
            } else {
                if ($employment_date->format("Y") < $date) {
                    if ($this->debug)
                        echo "Дата а там разберемся: 01.".$date."<BR>";
                    $PC->fetchByDateWithDepartment("01.".$date, $member->getDepartment()['id']);
                } else {
                    $PC->fetchByDateWithDepartment($employment_date->format("m.Y"), $member->getDepartment()['id']);
                }
            }

            $working_days_month_employment = $PC->get()["working_days"];
            if ($this->debug) {
                echo "Кол-во раб. дней в месяце: " . $working_days_month_employment . "<BR>";
                echo $employment_date->format("Y") . " < " . $date . "<BR>";
            }

            if ($employment_date->format("Y") < $date) {
                $member_awh = $AWH->getAwhByMember("01.".$date, 1, $member_id);
                $employment_date = DateTime::createFromFormat("d.m.Y","01.01.".$date);
                $interval = $employment_date->diff($date_now);
                if ($this->debug)
                    echo "Текущая дата: " . $date_now->format("Y-m-d")."<BR>";
            } else {
                $member_awh = $AWH->getAwhByMember($employment_date->format("m.Y"), 1, $member_id);
            }
            if (!is_null($leaving_days_of_absence)) {
                $member_awh['new_absence'] = $leaving_days_of_absence;
            }

            if ($interval->d > 0) {
                $this->holidayDays = round((1.66 * $interval->m + (1.66 / $working_days_month_employment * ($working_days_month_employment - $member_awh['new_absence'] - $awh_absence_sklad))), 2);
                if ($this->debug)
                    echo "(1.66 * " . $interval->m . " + (1.66 / " . $working_days_month_employment . " * (" . $working_days_month_employment . " - " . $member_awh['new_absence'] . " - " . $awh_absence_sklad . ")) = ".$this->holidayDays." <br>";
            } else {
                $this->holidayDays = round((1.66 * $interval->m), 2);
                if ($this->debug)
                    echo "1.66 * ".$interval->m."= ".$this->holidayDays."<BR>";
            }

            if ($employment_date->format("Y")==($date-1)) {
                $start_date = DateTime::createFromFormat("d.m.Y","01.01.".$date);
                $diseaseInterval = $employment_date->diff($start_date);
            } else {
                $diseaseInterval = $interval;
            }

            if ($diseaseInterval->d > 0) {
                $this->diseaseBalance = round((0.83 * $interval->m + (0.83 / $working_days_month_employment * ($working_days_month_employment - $member_awh['new_absence'] - $awh_absence_sklad))) - $this->sumDisease, 2);
                if ($this->debug)
                    echo "Формула для больняков: (0.83 * ".$interval->m." + (0.83 / ".$working_days_month_employment." * (".$working_days_month_employment." - ".$member_awh['new_absence']." - " . $awh_absence_sklad."))) - ".$this->sumDisease." = ".$this->diseaseBalance."<BR>";
            } else {
                $this->diseaseBalance = round((0.83 * $interval->m) - $this->sumDisease, 2);
//                echo "(0.83 * ".$interval->m.") - ".$this->sumDisease." = ".$this->diseaseBalance."<BR>";
            }
        };
        if ($this->debug) {
            echo "<b>Интервал между датами:</b> " . $employment_date->format("Y-m-d") . " и " . $date_now->format("Y-m-d")."<br>";
            echo "Интервал: " . $interval->y . " " . $interval->m . " " . $interval->d . " " . "<BR>";
        }
        if ($interval->y>0 || ($interval->y==0 && $interval->m>=6) ) {
            if ( $employment_date->format("Y") == $date ) {
                $formula();
                $this->holiday_balance_calculation = $this->holidayDays;
                if ($this->debug)
                    echo "Осталось больничных(формула): ".$this->diseaseBalance."<BR>";
            } else {
                $this->holidayDays = self::$MAX_HOLIDAY;
                $this->diseaseBalance = self::$MAX_DISEASE - $this->sumDisease;
                if ($this->debug)
                    echo "Осталось больничных: ".$this->diseaseBalance."<BR>";
            }
        } else {
            $formula();
            if ($this->debug) {
                echo "Возможные дни отпуска: " . $this->holidayDays . "<BR>";
                echo "Остаток отп: " . $this->holiday_balance_calculation . " <BR>";
                echo "Осталось больничных(формула2): ".$this->diseaseBalance."<BR>";
            }
            $this->holiday_balance_calculation += $this->holidayDays;
        }

        /*
         * Если сумма отсутствий > 0,
         * то пересчитываем дни отпуска
         */
        if ($date < 2022) {
            if ($this->sumAbsence > 3) {
                $this->holiday_balance_calculation -= round( (1.66 / 21 * $this->sumAbsence), 2);
                $this->holidayDays -= round( (1.66 / 21 * $this->sumAbsence), 2);
                $this->diseaseBalance = round( ($this->diseaseBalance - (0.83 / 21 * $this->sumAbsence)), 2);
            }
        } else {
            if ($this->sumAbsence > 0) {
                if ($this->debug)
                    echo "За свой счет: "."1.66 / 21 * ".$this->sumAbsence." = ".round( (1.66 / 21 * $this->sumAbsence), 2)."<BR>";
                $this->holiday_balance_calculation -= round( (1.66 / 21 * $this->sumAbsence), 2);
                $this->holidayDays -= round( (1.66 / 21 * $this->sumAbsence), 2);
                $this->diseaseBalance = round( ($this->diseaseBalance - (0.83 / 21 * $this->sumAbsence)), 2);
            }
        }
        if ($this->debug)
            echo " До перебора месяцев: " . $vacation_balance_calculation . " " . $this->holiday_balance_calculation . "<BR>";
        $current  = [];
        foreach ($aData as $value) {
            $current = explode("-", $value['date']);
            if (count($aDate)>1 && $current[1]>$aDate[1])
                break;

            /*
             * Если полученный месяц меньше или равен $this->HOLIDAY_LAST_YEAR
             * то считаем сумму первой половины годы
             * а так же остаток отпуска
             */
            if ((int)$current[1] <= $this->HOLIDAY_LAST_YEAR && $calculateVacation) {
                $this->sum_first_holiday += $value['holiday'];
                /*
                 * Считаю остаток отпуска
                 * Если с прошлого года остались неотгуленные дни
                 * то за первый период отнимаю из отатка дней прошлого года
                 */
                if ($vacation_balance_calculation>0) {

                    $vacation_balance_calculation -= $value['holiday'];
                    /*
                     * Если получилось отрицательно число, то вычитаю из общего количества путем прибавление
                     * т.к. отрицательно число и ставлю 0
                     */
                    if ($vacation_balance_calculation < 0 ) {
                        $this->holiday_balance_calculation += $vacation_balance_calculation;
                        $vacation_balance_calculation = 0;
                    }
                } else {
                    /*
                     * Иначе отнимаю от количества неотгуленных дней в этом году
                     */
                    $this->holiday_balance_calculation -= $value['holiday'];
                }
            } else {
                if ((int)$current[1] <= $this->HOLIDAY_LAST_YEAR) {
                    $this->sum_first_holiday += $value['holiday'];
                } else {
                    $this->sum_second_holiday += $value['holiday'];
                }
                /*
                 * Считаю отпуска за второю половину года
                 * так же отнимаю дни из общего отпуска
                 */
                if (!$calculateVacation) {
                    if ($vacation_balance_calculation > 0) {
                        $vacation_balance_calculation -= $value['holiday'];

                        if ($vacation_balance_calculation < 0 ) {
                            $vacation_balance_calculation = 0;
                        }
                    }
                }
                $this->holiday_balance_calculation -= $value['holiday'];
            }

            if ($this->debug)
                echo $current[1]. " " . $vacation_balance_calculation . " " . $this->holiday_balance_calculation . "<BR>";

            /*
             * Если не использовали остаток за предыдущий год, при условии, что пришел в передыдущем году с 7го месяца
             * то в текущем декабре остаток сгорает
             */
            if ((int)$current[1]==12 && !$calculateVacation && $vacation_balance_calculation>0) {
                $this->holiday_balance_calculation -= $vacation_balance_calculation;
            }
        }
        /*
         * Если НЕ (дата приема == прошлый год и месяц >=7)
         * А дата текущая == выбранной дате, при этом текущий месяц <= отсечеке и еще остался остаток за прошлый год
         * то он не пока еще не сгорел прибавляем его к остатку отпуска на 01.дата_текущего_месяца
         */
        if (!($employment_date->format("Y")==(date("Y")-1) && $employment_date->format("m")>=7)) {
            if (date("Y") == $date && date("m") <= $this->HOLIDAY_LAST_YEAR && $vacation_balance_calculation > 0) {
                $this->holiday_balance_calculation += $vacation_balance_calculation;
            }
        }
        if ($vacation_balance_calculation<0) {
            $this->vacation_balance_calculation = 0;
        } else {
            if ($current[1]>=$this->HOLIDAY_LAST_YEAR && $calculateVacation) {
                $this->vacation_balance_calculation = $vacation_balance_calculation;
            } else {
                $this->vacation_balance_calculation = 0;
            }
        }
    }

    public function save() {
        global $db;
        if (is_null($this->date)) {
            return false;
        }
        if (is_null($this->member_id)) {
            return false;
        }

        $aDate = explode("-", $this->date);
        if (count($aDate)>1) {
            $date = $aDate[0];
        } else {
            $date = $this->date;
        }
        /*
         * следующий год
         */
        $date++;

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $q = "select * from vacation_balance where date=? and member_id=?";
        $sth = $this->dbh->prepare($q);
        $sth->execute([$date ."-01-01", $this->member_id]);

        if ($sth->rowCount()>0) {
            $raw = $sth->fetch(PDO::FETCH_ASSOC);
            if ($raw['holiday'] == round($this->holiday_balance_calculation,2))
                return true;

            $q = "UPDATE vacation_balance SET holiday=? where date=STR_TO_DATE(?, '%Y-%m-%d') and member_id=?";
            $sth = $this->dbh->prepare($q);
            $sth->execute([round($this->holiday_balance_calculation, 2), $date . "-01-01", $this->member_id]);
        } else {
            $q = "INSERT INTO vacation_balance (date, member_id, holiday) VALUES (?, ?, ?)";
            $sth = $this->dbh->prepare($q);
            $sth->execute([$date . "-01-01", $this->member_id, round($this->holiday_balance_calculation,2)]);
        }
    }
}