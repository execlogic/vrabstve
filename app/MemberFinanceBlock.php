<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 14.09.18
 * Time: 13:26
 */

require_once 'app/functions.php';
require_once 'MemberInterface.php';
require_once 'app/DataItem.php';
require_once 'app/DataManager.php';
require_once 'app/CustomPercent.php';
require_once 'app/WindowPercent.php';

class MemberFinanceBlock
{
    private $dbh;
    private $Nacenka = array();
    private $data = array();
    private $data_otdel = array();
    private $data_manager = array();
    private $data_tt = array();
    private $data_service = array();
    private $data_service2 = array();
    private $data_statgroup = array();
    private $data_estimator = [];
    private $Ndfl = array();
    private $CommercialProcent = 1;
    private $CommercialProcentNote;
    private $Member;
    private $FixPayment = 0;

    private $date_now;

    public function __construct(&$Member)
    {
        $this->Member = $Member;
        $this->date_now = new DateTime();
    }

    // Используется в профиле сотрудника depricated
    public function fetch()
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if ($this->Member->getMotivation() == 1) {
            $query = "SELECT data_manager.date,data_manager.profit,data_manager.statgroup_id,st.name as statgroup_name,dp.namesklad as department_name FROM data_manager JOIN statgroup as st ON(statgroup_id = st.id) JOIN department as dp ON(department_id = dp.id) where member_id=? order by date LIMIT 50";

            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if (!$sth->execute([$this->Member->getId()])) {
                throw new Exception("Не могу выполнить зарос к БД");
            }

            $this->Nacenka = $sth->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    // Устаревший метод отключил depricated 02.10.2018
//    public function fetchWithDate($date)
//    {
//        if ($this->dbh == null) {
//            $this->dbh = dbConnect();
//        }
//
//        if ($this->Member->getMotivation() == 1) {
//            $query = "SELECT data_manager.date,data_manager.profit,data_manager.statgroup_id,st.name as statgroup_name,dp.namesklad as department_name FROM data_manager JOIN statgroup as st ON(statgroup_id = st.id) JOIN department as dp ON(department_id = dp.id) where member_id=? AND data_manager.date=STR_TO_DATE(?, '%m.%Y') order by data_manager.id";
//
//            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
//
//            if (!$sth) {
//                throw new Exception('Ошибка в PDO');
//            };
//
//            if (!$sth->execute([$this->Member->getId(), $date])) {
//                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
//                throw new Exception("Не могу выполнить зарос к БД");
//            }
//
//            $this->Nacenka = $sth->fetchAll(PDO::FETCH_ASSOC);
//        }
//    }
    public function fetchOtdelData($date)
    {
        if (count($this->data_otdel) > 0) {
            $this->data_otdel = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select data_service.id, date, member_id, statgroup_id, st.name as statgroup_name,  sales, const_price, profit,data_service.department_id,dp.namesklad as department_name 
        from data_service
        JOIN statgroup as st ON(statgroup_id = st.id)
        JOIN department as dp ON(department_id = dp.id) 
        where member_id IN (
        select t1.id from member as t1
        LEFT JOIN member_jobpost as t2 ON (t1.id = t2.member_id)
        where t2.department_id=?) and date=STR_TO_DATE(?,'%m.%Y')";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$this->Member->getDepartment()['id'], $date])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД data_service ");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
                $DataItem->setStatgroupId($value['statgroup_id']);
            }

            if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
                $DataItem->setStatgroupName($value['statgroup_name']);
            }

            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
                $DataItem->setDepartmentId($value['department_id']);
            }
            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
                $DataItem->setDepartmentName($value['department_name']);
            }

            if (isset($value['bonus']) && !is_null($value['bonus'])) {
                $DataItem->setBonus($value['bonus']);
            }
            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                $DataItem->setBonusNote($value['bonus_note']);
            }
            if (isset($value['correct']) && !is_null($value['correct'])) {
                $DataItem->setCorrect($value['correct']);
            }
            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                $DataItem->setCorrectNote($value['correct_note']);
            }

            array_push($this->data_manager, $DataItem);
        }

//        $query ="select date, member_id, statgroup_id, st.name as statgroup_name,
//            sales, const_price, profit,
//            data_manager.department_id,dp.namesklad as department_name
//            from data_manager
//            JOIN statgroup as st ON(statgroup_id = st.id)
//            JOIN department as dp ON(department_id = dp.id)
//            where member_id IN (
//                select t1.id from member as t1
//                LEFT JOIN member_jobpost as t2 ON (t1.id = t2.member_id)
//                where t1.dismissal_date='9999-01-01' and t2.department_id=?
//                )
//            and date=STR_TO_DATE(?,'%m.%Y')";
//
//        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
//
//        if (!$sth) {
//            throw new Exception('Ошибка в PDO');
//        };
//
//        if (!$sth->execute([$this->Member->getDepartment()['id'], $date])) {
//            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
//            throw new Exception("Не могу выполнить зарос к БД");
//        }
//
//        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
//
//        foreach ($raw as $value) {
//            $DataItem = new DataManager();
//            $DataItem->setId($value['id']);
//            $DataItem->setDate($value['date']);
//            $DataItem->setProfit($value['profit']);
//
//            if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
//                $DataItem->setStatgroupId($value['statgroup_id']);
//            }
//
//            if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
//                $DataItem->setStatgroupName($value['statgroup_name']);
//            }
//
//            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
//                $DataItem->setDepartmentId($value['department_id']);
//            }
//            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
//                $DataItem->setDepartmentName($value['department_name']);
//            }
//
//            if (isset($value['bonus']) && !is_null($value['bonus'])) {
//                $DataItem->setBonus($value['bonus']);
//            }
//            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
//                $DataItem->setBonusNote($value['bonus_note']);
//            }
//            if (isset($value['correct']) && !is_null($value['correct'])) {
//                $DataItem->setCorrect($value['correct']);
//            }
//            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
//                $DataItem->setCorrectNote($value['correct_note']);
//            }
//
//            array_push($this->data_manager, $DataItem);
//        }

        $sth = null;
    }

    public function fetchOtdelDataNew($date)
    {
        if (count($this->data_otdel) > 0) {
            $this->data_otdel = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        if ($this->date_now->format("Y-m-d") < '2023-04-01') {
            $query = "SELECT data.id, date, member_id, 
                    sales, const_price, profit,
                    data.department_id, dp.namesklad as department_name, CONCAT(mb.lastname,' ', mb.name)
                  FROM data 
	              JOIN department as dp ON(department_id = dp.id) 
	              LEFT JOIN member as mb on (service_id = mb.id)
                  WHERE service_id IN ( 
                    select t1.id from member as t1 
                    LEFT JOIN member_jobpost as t2 ON (t1.id = t2.member_id) 
                    where t2.department_id=?) AND
	                date>=STR_TO_DATE(?, '%d.%m.%Y') AND 
                    date<=STR_TO_DATE(?, '%d.%m.%Y') and mb.dismissal_date >= STR_TO_DATE(?, '%d.%m.%Y') order by id";

//            echo "SELECT data.id, date, member_id,
//                    sales, const_price, profit,
//                    data.department_id, dp.namesklad as department_name, CONCAT(mb.lastname,' ', mb.name)
//                  FROM data
//	              JOIN department as dp ON(department_id = dp.id)
//	              LEFT JOIN member as mb on (service_id = mb.id)
//                  WHERE service_id IN (
//                    select t1.id from member as t1
//                    LEFT JOIN member_jobpost as t2 ON (t1.id = t2.member_id)
//                    where t2.department_id=".$this->Member->getDepartment()['id'].") AND
//	                date>=STR_TO_DATE("."01.".$date.", '%d.%m.%Y') AND
//                    date<=STR_TO_DATE("."31.".$date.", '%d.%m.%Y') and mb.dismissal_date >= STR_TO_DATE("."01.".$date.", '%d.%m.%Y') order by id<BR>";
        } else {
            $query = "SELECT data.id, date, member_id, 
                    sales, const_price, profit,
                    data.department_id, dp.namesklad as department_name, CONCAT(mb.lastname,' ', mb.name)
                  FROM data 
	              JOIN department as dp ON(department_id = dp.id) 
	              LEFT JOIN member as mb on (service2_id = mb.id)
                  WHERE service2_id IN ( 
                    select t1.id from member as t1 
                    LEFT JOIN member_jobpost as t2 ON (t1.id = t2.member_id) 
                    where t2.department_id=?) AND
	                date>=STR_TO_DATE(?, '%d.%m.%Y') AND 
                    date<=STR_TO_DATE(?, '%d.%m.%Y') and mb.dismissal_date >= STR_TO_DATE(?, '%d.%m.%Y') order by id";

//            echo "SELECT data.id, date, member_id,
//                    sales, const_price, profit,
//                    data.department_id, dp.namesklad as department_name, CONCAT(mb.lastname,' ', mb.name)
//                  FROM data
//	              JOIN department as dp ON(department_id = dp.id)
//	              LEFT JOIN member as mb on (service2_id = mb.id)
//                  WHERE service2_id IN (
//                    select t1.id from member as t1
//                    LEFT JOIN member_jobpost as t2 ON (t1.id = t2.member_id)
//                    where t2.department_id=".$this->Member->getDepartment()['id'].") AND
//	                date>=STR_TO_DATE("."01.".$date.", '%d.%m.%Y') AND
//                    date<=STR_TO_DATE("."31.".$date.", '%d.%m.%Y') and mb.dismissal_date >= STR_TO_DATE("."01.".$date.", '%d.%m.%Y') order by id<BR>";
        }



        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };
        if (!$sth->execute([$this->Member->getDepartment()['id'], "01.".$date, "31.".$date, "01.".$date])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД data_service ");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        /*
         * Получаю бонусы и корректировки
         */
        foreach ($raw as $key=>$item) {
            /*
             * Корретировки
             */
            $query = "SELECT * from correcting WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and is_active=1 LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if (!$sth->execute([$date, $item['department_id'], $this->Member->getId()])) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД");
            }

            $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($correcting_raw)) {
                $raw[$key]['correct'] = $correcting_raw['sum'];
                $raw[$key]['correct_note'] = $correcting_raw['note'];
            }

            /*
             *  Бонусы
             */

            $query = "SELECT * from bonus WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and is_active=1 LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if (!$sth->execute([$date, $item['department_id'], $this->Member->getId()])) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД");
            }

            $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($correcting_raw)) {
                $raw[$key]['bonus'] = $correcting_raw['sum'];
                $raw[$key]['bonus_note'] = $correcting_raw['note'];
            }

        }

        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
                $DataItem->setStatgroupId($value['statgroup_id']);
            }

            if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
                $DataItem->setStatgroupName($value['statgroup_name']);
            }

            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
                $DataItem->setDepartmentId($value['department_id']);
            }
            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
                $DataItem->setDepartmentName($value['department_name']);
            }

            if (isset($value['bonus']) && !is_null($value['bonus'])) {
                $DataItem->setBonus($value['bonus']);
            }
            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                $DataItem->setBonusNote($value['bonus_note']);
            }
            if (isset($value['correct']) && !is_null($value['correct'])) {
                $DataItem->setCorrect($value['correct']);
            }
            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                $DataItem->setCorrectNote($value['correct_note']);
            }

            array_push($this->data_manager, $DataItem);
        }

        $sth = null;
    }

    public function fetchManagerData($date)
    {
        if (count($this->data_manager) > 0) {
            $this->data_manager = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT data_manager.id, 
	    data_manager.date, 
        data_manager.profit, 
        data_manager.statgroup_id,
        st.name as statgroup_name,
        data_manager.department_id,
        dp.namesklad as department_name, 
        correct.bonus,
        correct.bonus_note,
        correct.correct,
        correct.correct_note
        FROM data_manager 
        JOIN statgroup as st ON(statgroup_id = st.id) 
        JOIN department as dp ON(department_id = dp.id) 
        LEFT JOIN correcting_manager as correct ON (data_manager.id = correct.data_manager_id and correct.active=1) 
        where member_id=? AND data_manager.date=STR_TO_DATE(?, '%m.%Y') order by data_manager.id";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };


        if (!$sth->execute([$this->Member->getId(), $date])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
                $DataItem->setStatgroupId($value['statgroup_id']);
            }

            if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
                $DataItem->setStatgroupName($value['statgroup_name']);
            }

            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
                $DataItem->setDepartmentId($value['department_id']);
            }
            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
                $DataItem->setDepartmentName($value['department_name']);
            }

            if (isset($value['bonus']) && !is_null($value['bonus'])) {
                $DataItem->setBonus($value['bonus']);
            }
            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                $DataItem->setBonusNote($value['bonus_note']);
            }
            if (isset($value['correct']) && !is_null($value['correct'])) {
                $DataItem->setCorrect($value['correct']);
            }
            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                $DataItem->setCorrectNote($value['correct_note']);
            }

            array_push($this->data_manager, $DataItem);
        }

        $sth = null;
    }

    /**
     * Получаю данные по мотивации менеджер
     * @param $date
     * @throws Exception
     */
    public function fetchManagerDataNew($date)
    {
        if (count($this->data_manager) > 0) {
            $this->data_manager = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

//        $query ="
//        SELECT data.id,
//	    data.date,
//        SUM(data.profit) as profit,
//        data.department_id,
//        dp.namesklad as department_name, data.service_id
//	FROM data
//	JOIN department as dp ON(department_id = dp.id)
//	where member_id=? AND date>=STR_TO_DATE(?, '%d.%m.%Y') AND date<=STR_TO_DATE(?, '%d.%m.%Y') GROUP BY data.service_id, data.department_id order by id
//        ";
        $query ="
        SELECT data.id, 
	    data.date, 
        data.profit as profit,         
        data.department_id,
        dp.namesklad as department_name, data.service_id
	FROM data
	JOIN department as dp ON(department_id = dp.id) 
	where member_id=? AND date>=STR_TO_DATE(?, '%d.%m.%Y') AND date<=STR_TO_DATE(?, '%d.%m.%Y')
        ";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };


        if (!$sth->execute([$this->Member->getId(), '01.'.$date,'31.'.$date])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $WP = new WindowPercent();
        $window_data = [];
        foreach ($WP->get($this->Member->getId()) as $item) {
            $window_data[] = $item['window_id'];
        }
        
        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
        $sales = [];
        foreach ($raw as $item) {
            if ($window_data && $item['service_id'] && in_array($item['service_id'], $window_data)) {
                if (isset($sales[$item['department_id']."_".$item['service_id']])) {
                    $sales[$item['department_id']."_".$item['service_id']]['profit'] += $item['profit'];
                } else {
                    $sales[$item['department_id']."_".$item['service_id']] = $item;
                    $sales[$item['department_id']."_".$item['service_id']]['department_name'] .= ' (окна)';
                }
            } else {
                if (isset($sales[$item['department_id']])) {
                    $sales[$item['department_id']]['profit'] += $item['profit'];
                } else {
                    $item['service_id'] = null;
                    $sales[$item['department_id']] = $item;
                }
            }
        }
        $raw = $sales;

        foreach ($raw as $key=>$item) {
            /*
             * Корретировки
             */
            if ($item['service_id']) {
                $query = "SELECT * from correcting WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and service_id=? and is_active=1 LIMIT 1";
            } else {
                $query = "SELECT * from correcting WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and service_id is null and is_active=1 LIMIT 1";
            }


            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if ($item['service_id']) {
                $args = [$date, $item['department_id'], $this->Member->getId(), $item['service_id']];
            } else {
                $args = [$date, $item['department_id'], $this->Member->getId()];
            }

            if (!$sth->execute($args)) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД correcting manager");
            }

            $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($correcting_raw)) {
                $raw[$key]['correct'] = $correcting_raw['sum'];
                $raw[$key]['correct_note'] = $correcting_raw['note'];
            }

            /*
             *  Бонусы
             */

            if ($item['service_id']) {
                $query = "SELECT * from bonus WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and service_id=? and is_active=1 LIMIT 1";
            } else {
                $query = "SELECT * from bonus WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and service_id is null and is_active=1 LIMIT 1";
            }
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if ($item['service_id']) {
                $args = [$date, $item['department_id'], $this->Member->getId(), $item['service_id']];
            } else {
                $args = [$date, $item['department_id'], $this->Member->getId()];
            }

            if (!$sth->execute($args)) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД bonus");
            }

            $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($correcting_raw)) {
                $raw[$key]['bonus'] = $correcting_raw['sum'];
                $raw[$key]['bonus_note'] = $correcting_raw['note'];
            }

        }

        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
                $DataItem->setStatgroupId($value['statgroup_id']);
            }

            if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
                $DataItem->setStatgroupName($value['statgroup_name']);
            }

            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
                $DataItem->setDepartmentId($value['department_id']);
            }
            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
                $DataItem->setDepartmentName($value['department_name']);
            }

            if (isset($value['bonus']) && !is_null($value['bonus'])) {
                $DataItem->setBonus($value['bonus']);
            }
            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                $DataItem->setBonusNote($value['bonus_note']);
            }
            if (isset($value['correct']) && !is_null($value['correct'])) {
                $DataItem->setCorrect($value['correct']);
            }
            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                $DataItem->setCorrectNote($value['correct_note']);
            }
            if (isset($value['service_id']) && (!is_null($value['service_id']))) {
                $DataItem->setServiceId($value['service_id']);
            }

            array_push($this->data_manager, $DataItem);
        }

        $sth = null;
    }

    public function getBonusCorrect(&$item) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        /*
         * Корретировки
         */
        $query = "SELECT * from correcting WHERE date=STR_TO_DATE(?, '%Y-%m') and department_id=? and member_id=? and is_active=1 LIMIT 1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };
        if (!$sth->execute([$item['date'], $item['department'], $this->Member->getId()])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
        if (!empty($correcting_raw)) {
            $item['correct'] = $correcting_raw['sum'];
            $item['correct_note'] = $correcting_raw['note'];
        }

        /*
         *  Бонусы
         */

        $query = "SELECT * from bonus WHERE date=STR_TO_DATE(?, '%Y-%m') and department_id=? and member_id=? and is_active=1 LIMIT 1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$item['date'], $item['department'], $this->Member->getId()])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
        if (!empty($correcting_raw)) {
            $item['bonus'] = $correcting_raw['sum'];
            $item['bonus_note'] = $correcting_raw['note'];
        }
    }

    /**
     * Получаю данные по мотивации сметчик
     * @param $date
     * @throws Exception
     */
    public function fetchEstimatorDataNew($date)
    {
        if (count($this->data_estimator) > 0) {
            $this->data_estimator = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query ="
        SELECT data.id, data.date, SUM(data.profit) as profit, data.service_id, concat(mb.lastname,' ', mb.name, ' ', mb.middle) as service_name
        FROM data JOIN member as mb ON(service_id = mb.id) 
        where service_id in (select service_manager_id from estimator_data where manager_id=".$this->Member->getId().") AND date>=STR_TO_DATE('01.".$date."', '%d.%m.%Y') AND date<=STR_TO_DATE('31.".$date."', '%d.%m.%Y') 
        GROUP BY data.service_id order by service_id
        ";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };
        if (!$sth->execute()) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
                $DataItem->setStatgroupId($value['statgroup_id']);
            }

            if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
                $DataItem->setStatgroupName($value['statgroup_name']);
            }

            if (isset($value['service_id']) && (!is_null($value['service_id']))) {
                $DataItem->setDepartmentId($value['service_id']);
            }
            if (isset($value['service_name']) && (!is_null($value['service_name']))) {
                $DataItem->setDepartmentName($value['service_name']);
            }

            array_push($this->data_estimator, $DataItem);
        }

        $sth = null;
    }

    /**
     * Получаю данные от бренд-менеджер Шумилов
     * @param $date
     * @throws Exception
     */
    public function fetchServiceBrandDataNew($date)
    {
        if (count($this->data_statgroup) > 0) {
            $this->data_statgroup = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query ="
        SELECT data.id, 
            data.date, 
            SUM(data.profit) as profit,         
            data.department_id,
            dp.namesklad as department_name
        FROM data
        JOIN department as dp ON(department_id = dp.id) 
        where brand_id=227 and service2_id=? AND date>=STR_TO_DATE(?, '%d.%m.%Y') AND date<=STR_TO_DATE(?, '%d.%m.%Y') GROUP BY  data.department_id order by id
        ";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$this->Member->getID(), "01." . $date, "31." . $date])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
                $DataItem->setStatgroupId($value['statgroup_id']);
            }

            if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
                $DataItem->setStatgroupName($value['statgroup_name']);
            }

            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
                $DataItem->setDepartmentId($value['department_id']);
            }
            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
                $DataItem->setDepartmentName($value['department_name']);
            }

            if (isset($value['bonus']) && !is_null($value['bonus'])) {
                $DataItem->setBonus($value['bonus']);
            }
            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                $DataItem->setBonusNote($value['bonus_note']);
            }
            if (isset($value['correct']) && !is_null($value['correct'])) {
                $DataItem->setCorrect($value['correct']);
            }
            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                $DataItem->setCorrectNote($value['correct_note']);
            }

            array_push($this->data_statgroup, $DataItem);
        }
    }

    public function fetchServiceData($date)
    {
        if (count($this->data_service) > 0) {
            $this->data_service = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT data_service.id,
	    data_service.date,
        data_service.profit,
        data_service.statgroup_id,
        st.name as statgroup_name,
        data_service.department_id,
        dp.namesklad as department_name
        FROM data_service
        JOIN statgroup as st ON(statgroup_id = st.id)
        JOIN department as dp ON(department_id = dp.id)
        WHERE member_id=? AND data_service.date=STR_TO_DATE(?, '%m.%Y') order by data_service.id";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };


        if (!$sth->execute([$this->Member->getId(), $date])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
                $DataItem->setStatgroupId($value['statgroup_id']);
            }

            if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
                $DataItem->setStatgroupName($value['statgroup_name']);
            }

            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
                $DataItem->setDepartmentId($value['department_id']);
            }
            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
                $DataItem->setDepartmentName($value['department_name']);
            }

            if (isset($value['bonus']) && !is_null($value['bonus'])) {
                $DataItem->setBonus($value['bonus']);
            }
            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                $DataItem->setBonusNote($value['bonus_note']);
            }
            if (isset($value['correct']) && !is_null($value['correct'])) {
                $DataItem->setCorrect($value['correct']);
            }
            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                $DataItem->setCorrectNote($value['correct_note']);
            }

            array_push($this->data_service, $DataItem);
        }

        $sth = null;
    }

    /**
     * Получаю данные для мотивации сервис-менеджер
     * @param $date
     * @throws Exception
     */
    public function fetchServiceDataNew($date)
    {
        if (count($this->data_service) > 0) {
            $this->data_service = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT t1.id,
	    t1.date,
        SUM(t1.profit) as profit,
        t1.department_id,
        dp.namesklad as department_name
        FROM data as t1
        JOIN department as dp ON(t1.department_id = dp.id)
        WHERE t1.service_id=? AND t1.date>=STR_TO_DATE(?, '%d.%m.%Y') AND t1.date<=STR_TO_DATE(?, '%d.%m.%Y') GROUP BY t1.department_id order by t1.id";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };


        if (!$sth->execute([$this->Member->getId(), "01.".$date, "31.".$date])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        /*
         * Получаю бонусы и корректировки
         */
        foreach ($raw as $key=>$item) {
            /*
             * Корретировки
             */
            $query = "SELECT * from correcting WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and is_active=1 LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if (!$sth->execute([$date, $item['department_id'], $this->Member->getId()])) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД");
            }

            $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($correcting_raw)) {
                $raw[$key]['correct'] = $correcting_raw['sum'];
                $raw[$key]['correct_note'] = $correcting_raw['note'];
            }

            /*
             *  Бонусы
             */

            $query = "SELECT * from bonus WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and is_active=1 LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if (!$sth->execute([$date, $item['department_id'], $this->Member->getId()])) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД");
            }

            $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($correcting_raw)) {
                $raw[$key]['bonus'] = $correcting_raw['sum'];
                $raw[$key]['bonus_note'] = $correcting_raw['note'];
            }

        }


        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
                $DataItem->setStatgroupId($value['statgroup_id']);
            }

            if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
                $DataItem->setStatgroupName($value['statgroup_name']);
            }

            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
                $DataItem->setDepartmentId($value['department_id']);
            }
            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
                $DataItem->setDepartmentName($value['department_name']);
            }

            if (isset($value['bonus']) && !is_null($value['bonus'])) {
                $DataItem->setBonus($value['bonus']);
            }
            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                $DataItem->setBonusNote($value['bonus_note']);
            }
            if (isset($value['correct']) && !is_null($value['correct'])) {
                $DataItem->setCorrect($value['correct']);
            }
            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                $DataItem->setCorrectNote($value['correct_note']);
            }

            array_push($this->data_service, $DataItem);
        }

        $sth = null;
    }

    /**
     * Получаю данные для мотивации услуги сервис 2
     * Товары
     * @param $date
     * @throws Exception
     */
    public function fetchUsligiDataNew($date)
    {
        if (count($this->data_service2) > 0) {
            $this->data_service2 = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT t1.id,
	    t1.date,
        SUM(t1.profit) as profit,
        t1.department_id,
        dp.namesklad as department_name
        FROM data as t1
        JOIN department as dp ON(t1.department_id = dp.id)
        WHERE t1.service2_id=? AND t1.date>=STR_TO_DATE(?, '%d.%m.%Y') AND t1.date<=STR_TO_DATE(?, '%d.%m.%Y') GROUP BY t1.department_id order by t1.id";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };


        if (!$sth->execute([$this->Member->getId(), "01.".$date, "31.".$date])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        /*
         * Получаю бонусы и корректировки
         */
        foreach ($raw as $key=>$item) {
            /*
             * Корретировки
             */
            $query = "SELECT * from correcting WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and is_active=1 LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if (!$sth->execute([$date, $item['department_id'], $this->Member->getId()])) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД");
            }

            $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($correcting_raw)) {
                $raw[$key]['correct'] = $correcting_raw['sum'];
                $raw[$key]['correct_note'] = $correcting_raw['note'];
            }

            /*
             *  Бонусы
             */

            $query = "SELECT * from bonus WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and is_active=1 LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if (!$sth->execute([$date, $item['department_id'], $this->Member->getId()])) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД");
            }

            $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($correcting_raw)) {
                $raw[$key]['bonus'] = $correcting_raw['sum'];
                $raw[$key]['bonus_note'] = $correcting_raw['note'];
            }

        }


        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
                $DataItem->setStatgroupId($value['statgroup_id']);
            }

            if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
                $DataItem->setStatgroupName($value['statgroup_name']);
            }

            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
                $DataItem->setDepartmentId($value['department_id']);
            }
            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
                $DataItem->setDepartmentName($value['department_name']);
            }

            if (isset($value['bonus']) && !is_null($value['bonus'])) {
                $DataItem->setBonus($value['bonus']);
            }
            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                $DataItem->setBonusNote($value['bonus_note']);
            }
            if (isset($value['correct']) && !is_null($value['correct'])) {
                $DataItem->setCorrect($value['correct']);
            }
            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                $DataItem->setCorrectNote($value['correct_note']);
            }

            array_push($this->data_service2, $DataItem);
        }
//        echo "<pre>"; var_dump($this->data_service2); echo "</pre>";
        $sth = null;
    }

    public function calculateBonusCorrect(&$date)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if (count($this->data_tt) == 0) {
            return false;
//            $this->fetchTTData($date);
        }

        if ($this->Member->getMotivation() == 2) {
            $q = "select department_id from Roles where member_id=? and RoleSettings_id=2";
            $sth = $this->dbh->prepare($q); // Подготавливаем запрос
            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };
            if (!$sth->execute([$this->Member->getId()])) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД");
            }
            $d = [];

            foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $item) {
                $d[] = $item['department_id'];
            }

            foreach ($this->data_tt as $DI) {
                if (!in_array($DI->getDepartmentId() , $d))
                    continue;

                $dt = DateTime::createFromFormat("d.m.Y", "01." . $date);
                if ($dt >= DateTime::createFromFormat("Y-m-d", '2019-11-01')) {
                    $query = "SELECT SUM(t2.bonus) as bonus, SUM(t2.correct) as correct FROM zp.data_manager as t1
                        LEFT JOIN correcting_manager as t2 on (t1.id = t2.data_manager_id)
                        where t1.date = STR_TO_DATE(?, '%m.%Y') and department_id=? and t2.active = 1";

                    $sth_sub = $this->dbh->prepare($query); // Подготавливаем запрос
                    if (!$sth_sub)
                        throw new Exception('Ошибка в PDO');

                    if (!$sth_sub->execute([$date, $DI->getDepartmentId()])) {
                        echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                        throw new Exception("Не могу выполнить зарос к БД");
                    }

                    $raw_summ = current($sth_sub->fetchAll(PDO::FETCH_ASSOC));

                    $q = "select * from correcting_tt where data_TT_id = ? and member_id = ? LIMIT 1";
                    $sth_sub = $this->dbh->prepare($q); // Подготавливаем запрос
                    if (!$sth_sub)
                        throw new Exception('Ошибка в PDO');

                    if (!$sth_sub->execute([ $DI->getId(), $this->Member->getId()])) {
                        echo "xxxx Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                        throw new Exception("Не могу выполнить зарос к БД");
                    }

                    if ($sth_sub->rowCount()==0) {
                        $query = "INSERT INTO correcting_tt (bonus, bonus_note, correct, correct_note, date, member_id, data_TT_id, changer_id) VALUES (?,'',?,'', NOW(),?,?, '-1')";
                        $sth_sub = $this->dbh->prepare($query); // Подготавливаем запрос

                        if (!$sth_sub)
                            throw new Exception('Ошибка в PDO');

                        if (!$sth_sub->execute([$raw_summ['bonus'], $raw_summ['correct'], $this->Member->getId(), $DI->getId()])) {
                            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                            throw new Exception("Не могу выполнить зарос к БД");
                        }
                    } else {
                        $r = $sth_sub->fetch(PDO::FETCH_ASSOC);
                        $query = "UPDATE correcting_tt SET bonus=?, correct=? where id=?";
                        $sth_sub = $this->dbh->prepare($query); // Подготавливаем запрос
                        if (!$sth_sub) {
                            throw new Exception('Ошибка в PDO');
                        };

                        if (!$sth_sub->execute([ $raw_summ['bonus'], $raw_summ['correct'], $r['id'] ])) {
                            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                            throw new Exception("Не могу выполнить зарос к БД");
                        }
                    }

                    $DI->setBonus($raw_summ['bonus']);
                    $DI->setCorrect($raw_summ['correct']);

                    $sth_sub = null;
                }
            }
        }
    }

    public function fetchTTData($date) {
        if (count($this->data_tt) > 0) {
            $this->data_tt = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $this->fetchCommercialProcent($date);

        $query = "SELECT data_TT.id, 
	    data_TT.date, 
        data_TT.profit, 
        data_TT.department_id,
        dp.namesklad as department_name,
        correct.bonus,
        correct.bonus_note,
        correct.correct,
        correct.correct_note
        FROM data_TT 
        JOIN department as dp ON(department_id = dp.id) 
        LEFT JOIN correcting_tt as correct ON (data_TT.id = correct.data_TT_id and correct.active=1 and correct.member_id=? )
        where data_TT.date=STR_TO_DATE(?, '%m.%Y') order by department_name";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$this->Member->getId(), $date])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
                $DataItem->setStatgroupId($value['statgroup_id']);
            }

            if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
                $DataItem->setStatgroupName($value['statgroup_name']);
            }

            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
                $DataItem->setDepartmentId($value['department_id']);
            }
            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
                $DataItem->setDepartmentName($value['department_name']);
            }

            if (isset($value['bonus']) && !is_null($value['bonus'])) {
                $DataItem->setBonus($value['bonus']);
            }
            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                $DataItem->setBonusNote($value['bonus_note']);
            }
            if (isset($value['correct']) && !is_null($value['correct'])) {
                $DataItem->setCorrect($value['correct']);
            }
            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                $DataItem->setCorrectNote($value['correct_note']);
            }

            array_push($this->data_tt, $DataItem);
        }

        $sth = null;
    }

    /**
     * Получаю данные мотивации от торговой точки
     * @param $date
     * @throws Exception
     */
    public function fetchTTDataNew($date) {
        if (count($this->data_tt) > 0) {
            $this->data_tt = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $this->fetchCommercialProcent($date);

        $CPT = new CustomPercent();
        $CPT->fetchv3byDate($this->Member->GetId(), $date);
        $advanced_percent = $CPT->get();

        if (!$advanced_percent) {
            throw new Exception("Не установлены проценты на ТТ");
        }
        $departments = array_keys($advanced_percent);
        $in = str_repeat('?,', count($departments) - 1) . '?';

        $query = "SELECT t1.profit as profit,
        t1.department_id,
        dp.namesklad as department_name,
        t1.service_id
        FROM data as t1
        JOIN department as dp ON (t1.department_id = dp.id)
        WHERE t1.date>=STR_TO_DATE(?, '%d.%m.%Y') AND t1.date<=STR_TO_DATE(?, '%d.%m.%Y') AND t1.department_id IN ($in)
        order by dp.namesklad";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };
        if (!$sth->execute(array_merge(["01.".$date, "31.".$date],$departments))) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД data");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        $WP = new WindowPercent();
        $window_data = [];
        foreach ($WP->get($this->Member->getId()) as $item) {
            $window_data[] = $item['window_id'];
        }

        $sales = [];
        foreach ($raw as $item) {
            if ($window_data && $item['service_id'] && in_array($item['service_id'], $window_data)) {
                if (isset($sales[$item['department_id']."_".$item['service_id']])) {
                    $sales[$item['department_id']."_".$item['service_id']]['profit'] += $item['profit'];
                } else {
                    $sales[$item['department_id']."_".$item['service_id']] = $item;
                    $sales[$item['department_id']."_".$item['service_id']]['department_name'] .= ' (окна)';
                }
            } else {
                if (isset($sales[$item['department_id']])) {
                    $sales[$item['department_id']]['profit'] += $item['profit'];
                } else {
                    $item['service_id'] = null;
                    $sales[$item['department_id']] = $item;
                }
            }
        }
        $raw = $sales;

        /*
         * Получаю бонусы и корректировки
         */
        foreach ($raw as $key=>$item) {
            /*
             * Корретировки
             */
            if ($item['service_id']) {
                $query = "SELECT * from correcting WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and service_id=? and member_id=? and is_active=1 LIMIT 1";
            } else {
                $query = "SELECT * from correcting WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and service_id is null and member_id=? and is_active=1 LIMIT 1";
            }

            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if ($item['service_id']) {
                $args = [$date, $item['department_id'], $item['service_id'], $this->Member->getId()];
            } else {
                $args = [$date, $item['department_id'], $this->Member->getId()];
            }

            if (!$sth->execute($args)) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД correcting");
            }

            $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($correcting_raw)) {
                $raw[$key]['correct'] = $correcting_raw['sum'];
                $raw[$key]['correct_note'] = $correcting_raw['note'];
            }

            /*
             *  Бонусы
             */
            if ($item['service_id']) {
                $query = "SELECT * from bonus WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and service_id=? and is_active=1 LIMIT 1";
            } else {
                $query = "SELECT * from bonus WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and service_id is null and is_active=1 LIMIT 1";
            }


            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if ($item['service_id']) {
                $args = [$date, $item['department_id'], $this->Member->getId(), $item['service_id']];
            } else {
                $args = [$date, $item['department_id'], $this->Member->getId()];
            }

            if (!$sth->execute($args)) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД bonus");
            }

            $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($correcting_raw)) {
                $raw[$key]['bonus'] = $correcting_raw['sum'];
                $raw[$key]['bonus_note'] = $correcting_raw['note'];
                $raw[$key]['service_id'] = $correcting_raw['service_id'];
            }

        }

        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
                $DataItem->setDepartmentId($value['department_id']);
            }
            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
                $DataItem->setDepartmentName($value['department_name']);
            }

            if (isset($value['bonus']) && !is_null($value['bonus'])) {
                $DataItem->setBonus($value['bonus']);
            }
            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                $DataItem->setBonusNote($value['bonus_note']);
            }
            if (isset($value['correct']) && !is_null($value['correct'])) {
                $DataItem->setCorrect($value['correct']);
            }
            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                $DataItem->setCorrectNote($value['correct_note']);
            }
            if (isset($value['service_id']) && (!is_null($value['service_id']))) {
                $DataItem->setServiceId($value['service_id']);
            }

            array_push($this->data_tt, $DataItem);
        }

        $sth = null;
    }

    /**
     * Получаю данные мотивации всех тороговых точк
     * @param $date
     * @throws Exception
     */
    public function fetchTTDataAllNew($date) {
        if (count($this->data_tt) > 0) {
            $this->data_tt = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $this->fetchCommercialProcent($date);

        $query = "SELECT SUM(t1.profit) as profit, 
        t1.department_id,
        dp.namesklad as department_name
        FROM data as t1 
        JOIN department as dp ON (t1.department_id = dp.id) 
        WHERE t1.date>=STR_TO_DATE(?, '%d.%m.%Y') AND t1.date<=STR_TO_DATE(?, '%d.%m.%Y')
        GROUP BY t1.department_id order by dp.namesklad";


        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };
        if (!$sth->execute(["01.".$date, "31.".$date])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        /*
         * Получаю бонусы и корректировки
         */
        foreach ($raw as $key=>$item) {
            /*
             * Корретировки
             */
            if ($item['service_id']) {
                $query = "SELECT * from correcting WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and service_id=? and member_id=? and is_active=1 LIMIT 1";
            } else {
                $query = "SELECT * from correcting WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and service_id is null and member_id=? and is_active=1 LIMIT 1";
            }

            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if ($item['service_id']) {
                $args = [$date, $item['department_id'], $item['service_id'], $this->Member->getId()];
            } else {
                $args = [$date, $item['department_id'], $this->Member->getId()];
            }

            if (!$sth->execute($args)) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД correcting");
            }

            $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($correcting_raw)) {
                $raw[$key]['correct'] = $correcting_raw['sum'];
                $raw[$key]['correct_note'] = $correcting_raw['note'];
            }

            /*
             *  Бонусы
             */
            if ($item['service_id']) {
                $query = "SELECT * from bonus WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and service_id=? and is_active=1 LIMIT 1";
            } else {
                $query = "SELECT * from bonus WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and service_id is null and is_active=1 LIMIT 1";
            }


            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if ($item['service_id']) {
                $args = [$date, $item['department_id'], $this->Member->getId(), $item['service_id']];
            } else {
                $args = [$date, $item['department_id'], $this->Member->getId()];
            }

            if (!$sth->execute($args)) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД bonus");
            }

            $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($correcting_raw)) {
                $raw[$key]['bonus'] = $correcting_raw['sum'];
                $raw[$key]['bonus_note'] = $correcting_raw['note'];
                $raw[$key]['service_id'] = $correcting_raw['service_id'];
            }

        }

        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
                $DataItem->setDepartmentId($value['department_id']);
            }
            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
                $DataItem->setDepartmentName($value['department_name']);
            }

            if (isset($value['bonus']) && !is_null($value['bonus'])) {
                $DataItem->setBonus($value['bonus']);
            }
            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                $DataItem->setBonusNote($value['bonus_note']);
            }
            if (isset($value['correct']) && !is_null($value['correct'])) {
                $DataItem->setCorrect($value['correct']);
            }
            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                $DataItem->setCorrectNote($value['correct_note']);
            }
            if (isset($value['service_id']) && (!is_null($value['service_id']))) {
                $DataItem->setServiceId($value['service_id']);
            }

            array_push($this->data_tt, $DataItem);
        }

        $sth = null;
    }

    public function fetchStatGroupData($date)
    {
        if (count($this->data_statgroup) > 0) {
            $this->data_statgroup = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT data_statgroup.id, 
	   data_statgroup.date, 
       data_statgroup.profit, 
       data_statgroup.statgroup_id,
       st.name as statgroup_name,
       correct.bonus,
       correct.bonus_note,
       correct.correct,
       correct.correct_note
       FROM data_statgroup 
       JOIN statgroup as st ON(statgroup_id = st.id) 
       LEFT JOIN correcting_statgroup as correct ON (data_statgroup.id = correct.data_statgroup_id and correct.active=1 and correct.member_id=? )
       where data_statgroup.date=STR_TO_DATE(?, '%m.%Y') order by data_statgroup.id";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };


        if (!$sth->execute([$this->Member->getId(), $date])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
                $DataItem->setStatgroupId($value['statgroup_id']);
            }

            if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
                $DataItem->setStatgroupName($value['statgroup_name']);
            }

            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
                $DataItem->setDepartmentId($value['department_id']);
            }
            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
                $DataItem->setDepartmentName($value['department_name']);
            }

            if (isset($value['bonus']) && !is_null($value['bonus'])) {
                $DataItem->setBonus($value['bonus']);
            }
            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                $DataItem->setBonusNote($value['bonus_note']);
            }
            if (isset($value['correct']) && !is_null($value['correct'])) {
                $DataItem->setCorrect($value['correct']);
            }
            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                $DataItem->setCorrectNote($value['correct_note']);
            }

            array_push($this->data_statgroup, $DataItem);
        }
        $sth = null;
    }

    /**
     * Получаю данные для мотивации бренд-менеджер
     * @param $date
     * @throws Exception
     */
    public function fetchStatGroupDataNew($date)
    {
        if (count($this->data_statgroup) > 0) {
            $this->data_statgroup = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query ="
        SELECT data.id, 
            data.date, 
            SUM(data.profit) as profit,         
            data.department_id,
            dp.namesklad as department_name
        FROM data
        JOIN department as dp ON(department_id = dp.id) 
        where brand_id=? AND date>=STR_TO_DATE(?, '%d.%m.%Y') AND date<=STR_TO_DATE(?, '%d.%m.%Y') GROUP BY  data.department_id order by id
        ";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };


        if (!$sth->execute([$this->Member->getId(), "01." . $date, "31." . $date])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        /*
         * Получаю бонусы и корректировки
         */
        foreach ($raw as $key=>$item) {
            /*
             * Корретировки
             */
            $query = "SELECT * from correcting WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and is_active=1 LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if (!$sth->execute([$date, $item['department_id'], $this->Member->getId()])) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД");
            }

            $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($correcting_raw)) {
                $raw[$key]['correct'] = $correcting_raw['sum'];
                $raw[$key]['correct_note'] = $correcting_raw['note'];
            }

            /*
             *  Бонусы
             */

            $query = "SELECT * from bonus WHERE date=STR_TO_DATE(?, '%m.%Y') and department_id=? and member_id=? and is_active=1 LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            if (!$sth->execute([$date, $item['department_id'], $this->Member->getId()])) {
                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
                throw new Exception("Не могу выполнить зарос к БД");
            }

            $correcting_raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($correcting_raw)) {
                $raw[$key]['bonus'] = $correcting_raw['sum'];
                $raw[$key]['bonus_note'] = $correcting_raw['note'];
            }

        }

        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
                $DataItem->setStatgroupId($value['statgroup_id']);
            }

            if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
                $DataItem->setStatgroupName($value['statgroup_name']);
            }

            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
                $DataItem->setDepartmentId($value['department_id']);
            }
            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
                $DataItem->setDepartmentName($value['department_name']);
            }

            if (isset($value['bonus']) && !is_null($value['bonus'])) {
                $DataItem->setBonus($value['bonus']);
            }
            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                $DataItem->setBonusNote($value['bonus_note']);
            }
            if (isset($value['correct']) && !is_null($value['correct'])) {
                $DataItem->setCorrect($value['correct']);
            }
            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                $DataItem->setCorrectNote($value['correct_note']);
            }

            array_push($this->data_statgroup, $DataItem);
        }

        $sth = null;
    }

    public function fetchServiceDataPeriod($date)
    {
        if (count($this->data_service) > 0) {
            $this->data_service = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT data_manager.id, 
	    data_manager.date, 
        SUM(data_manager.profit) as profit, 
        data_manager.statgroup_id,
        st.name as statgroup_name,
        data_manager.department_id,
        dp.namesklad as department_name 
        FROM data_manager 
        JOIN statgroup as st ON(statgroup_id = st.id) 
        JOIN department as dp ON(department_id = dp.id)  
        WHERE service_id=? AND data_manager.date BETWEEN DATE_FORMAT(SYSDATE() , '%Y-01-01 00:00:00') AND NOW() group by statgroup_id order by statgroup_id";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };


        if (!$sth->execute([$this->Member->getId()])) {
            echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        foreach ($raw as $value) {
            $DataItem = new DataManager();
            $DataItem->setId($value['id']);
            $DataItem->setDate($value['date']);
            $DataItem->setProfit($value['profit']);

            if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
                $DataItem->setStatgroupId($value['statgroup_id']);
            }

            if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
                $DataItem->setStatgroupName($value['statgroup_name']);
            }

            if (isset($value['department_id']) && (!is_null($value['department_id']))) {
                $DataItem->setDepartmentId($value['department_id']);
            }
            if (isset($value['department_name']) && (!is_null($value['department_name']))) {
                $DataItem->setDepartmentName($value['department_name']);
            }

            if (isset($value['bonus']) && !is_null($value['bonus'])) {
                $DataItem->setBonus($value['bonus']);
            }
            if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                $DataItem->setBonusNote($value['bonus_note']);
            }
            if (isset($value['correct']) && !is_null($value['correct'])) {
                $DataItem->setCorrect($value['correct']);
            }
            if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                $DataItem->setCorrectNote($value['correct_note']);
            }

            array_push($this->data_service, $DataItem);
        }

        $sth = null;
    }

    // Получаю массив объектов данных в зависимости от мотивации пользователя
    public function fetchWithDateObj($date)
    {
        if (count($this->data) > 0) {
            $this->data = array();
        }

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $test_date = DateTime::createFromFormat("d.m.Y", "01.".$date);
        if ($test_date->format("Y-m-d")<'2021-05-01')
            $older_format = 1;
        else
            $older_format = 0;
        unset($test_date);

        $motivation = $this->Member->getMotivation();
        switch ($motivation) {
            case 1:
                if ($older_format)
                    $this->fetchManagerData($date);
                else
                    $this->fetchManagerDataNew($date);
                break;
            case 2:
                if ($older_format)
                    $this->fetchTTData($date);
                else
                    $this->fetchTTDataNew($date);
                break;
            case 3:
                if ($older_format)
                    $this->fetchTTData($date);
                else
                    $this->fetchTTDataAllNew($date);
                break;
            case 4:
                if ($older_format)
                    $this->fetchStatGroupData($date);
                else
                    $this->fetchStatGroupDataNew($date);
                break;
            case 5:
                if ($older_format)
                    $this->fetchServiceData($date);
                else
                    $this->fetchServiceDataNew($date);
                break;
            case 6:
                $this->data = 0;
                break;
            case  7:
                if ($older_format)
                    $this->fetchServiceData($date);
                else {
                    if ($this->date_now->format("Y-m-d") >= '2023-04-24') {
                        $this->fetchUsligiDataNew($date);
                    } else {
                        $this->fetchServiceDataNew($date);
                    }
                }
                break;
            case 8:
                if ($older_format)
                    $this->fetchOtdelData($date);
                else
                    $this->fetchOtdelDataNew($date);
                break;
            case 9:
                $this->fetchEstimatorDataNew($date);
                break;
            case 10:
                $this->fetchUsligiDataNew($date);
                $this->fetchServiceBrandDataNew($date);
                break;
            default:
                throw new Exception("У пользователя не выбрана мотивация");
                break;
        }
//        if (($this->Member->getMotivation() > 0) && ($this->Member->getMotivation() < 6)) {
//            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
//
//            if (!$sth) {
//                throw new Exception('Ошибка в PDO');
//            };
//
//
//            if (!$sth->execute([$this->Member->getId(), $date])) {
//                echo "Error info: " . $sth->errorInfo()[2] . " Error code: " . $sth->errorCode() . "<BR>";
//                throw new Exception("Не могу выполнить зарос к БД");
//            }
//
//            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
//
//            foreach ($raw as $value) {
//                $DataItem = new DataManager();
//                $DataItem->setId($value['id']);
//                $DataItem->setDate($value['date']);
//                $DataItem->setProfit($value['profit']);
//
//                if (isset($value['statgroup_id']) && (!is_null($value['statgroup_id']))) {
//                    $DataItem->setStatgroupId($value['statgroup_id']);
//                }
//
//                if (isset($value['statgroup_name']) && (!is_null($value['statgroup_name']))) {
//                    $DataItem->setStatgroupName($value['statgroup_name']);
//                }
//
//                if (isset($value['department_id']) && (!is_null($value['department_id']))) {
//                    $DataItem->setDepartmentId($value['department_id']);
//                }
//                if (isset($value['department_name']) && (!is_null($value['department_name']))) {
//                    $DataItem->setDepartmentName($value['department_name']);
//                }
//
//                if (isset($value['bonus']) && !is_null($value['bonus'])) {
//                    $DataItem->setBonus($value['bonus']);
//                }
//                if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
//                    $DataItem->setBonusNote($value['bonus_note']);
//                }
//                if (isset($value['correct']) && !is_null($value['correct'])) {
//                    $DataItem->setCorrect($value['correct']);
//                }
//                if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
//                    $DataItem->setCorrectNote($value['correct_note']);
//                }
//
//                array_push($this->data, $DataItem);
//            }
//        };
        if (count($this->data_manager) > 0) {
            $this->data = $this->data_manager;
        }

        if (count($this->data_tt) > 0) {
            $this->data = $this->data_tt;
        }

        if (count($this->data_statgroup) > 0) {
            $this->data = $this->data_statgroup;
        }

        if (count($this->data_service) > 0) {
            $this->data = $this->data_service;
        }

    }

    public function getData()
    {
        return $this->data;
    }

    public function getDataManager()
    {
        return $this->data_manager;
    }

    /**
     * Установить дату из Json
     * @param $json
     * @param $motivation
     * @param $date
     * @throws Exception
     */
    public function setDataFromJson($json, $motivation, $date) {
        $f = function ($data, $motivation) use($date) {
            $this->data=array();
            foreach ($data as $key => $value) {
                $this->getBonusCorrect($data[$key]);
                $DataItem = new DataManager();
                isset($value['id']) ? $DataItem->setId($value['id']) : NULL;
                $value['date'] ? $DataItem->setDate($value['date']) : NULL;
                $value['profit'] ? $DataItem->setProfit($value['profit']) : NULL;
                $value['statgroup_id'] ? $DataItem->setStatgroupId($value['statgroup_id']) : NULL;
                $value['statgroup_name'] ? $DataItem->setStatgroupName($value['statgroup_name']) : NULL;
                $value['department'] ? $DataItem->setDepartmentId($value['department']) : NULL;
                $value['department_name'] ? $DataItem->setDepartmentName($value['department_name']) : NULL;
                $value['procent'] ? $DataItem->setBaseprocent($value['procent']) : NULL;

                if (isset($value['bonus']) && !is_null($value['bonus'])) {
                    $DataItem->setBonus($value['bonus']);
                }
                if (isset($value['bonus_note']) && !is_null($value['bonus_note'])) {
                    $DataItem->setBonusNote($value['bonus_note']);
                }
                if (isset($value['correct']) && !is_null($value['correct'])) {
                    $DataItem->setCorrect($value['correct']);
                }
                if (isset($value['correct_note']) && !is_null($value['correct_note'])) {
                    $DataItem->setCorrectNote($value['correct_note']);
                }
                array_push($this->data, $DataItem);
            }

            switch ($motivation) {
                case 1:
                case 8:
                    $this->data_manager = $this->data;
                    break;
                case 2:
                case 3:
                    $this->fetchCommercialProcent($date);
                    $this->data_tt = $this->data;
                    break;
                case 4:
                    $this->data_statgroup = $this->data;
                    break;
                case 5:
                    $this->data_service = $this->data;
                    break;
                case 7:
                    if ($this->date_now->format("Y-m-d") >= '2023-04-24') {
                        $this->data_service2 = $this->data;
                    }  else {
                        $this->data_service = $this->data;
                    }
                    break;
                case 6:
                    $this->data = 0;
                    break;
                case 9:
                    $this->data_estimator = $this->data;
                    break;
            }
        };

        if ($motivation == 10) {
            $f($json['data']['service'], 7);
            $f($json['data']['brand'], 4);
        } else {
            $f($json['data'], $motivation);
        }
    }

    /**
     * @return array
     */
    public function getDataEstimator()
    {
        return $this->data_estimator;
    }

    public function getDataTT()
    {
        return $this->data_tt;
    }

    public function getDataService()
    {
        return $this->data_service;
    }
    public function getDataService2()
    {
        return $this->data_service2;
    }

    public function getDataStatGroup()
    {
        return $this->data_statgroup;
    }

    public function get()
    {
        return $this->Nacenka;
    }

    /**
     * Получение NDFL (скорее всего не будет использоваться)
     * @throws Exception
     */
    public function fetch_NDFL()
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT date,ndfl,ro FROM member_ndfl where member_id=? order by date LIMIT 20";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$this->Member->getId()])) {
            throw new Exception("Не могу выполнить зарос к БД fetch_NDFL");
        }

        $this->Ndfl = $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Получение NDFL по заданной дате (копия)
     * @param $date
     * @throws Exception
     */
    public function fetch_NdflByDate($date)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT date, ndfl,ro FROM member_ndfl where member_id=? and date=STR_TO_DATE(?, '%m.%Y') LIMIT 1";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$this->Member->getId(), $date])) {
            throw new Exception("Не могу выполнить зарос к БД fetch_NdflByDate");
        }

        $this->Ndfl = $sth->fetch(PDO::FETCH_ASSOC);
    }

    public function getNdfl()
    {
        return $this->Ndfl;
    }

    /**
     * Добавляю бонусы И корректировки к коммерческому блоку
     * @param $id
     * @param $array
     * @param $changer_id
     * @throws Exception
     */
    public function addCorrection($id, $array, $changer_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if ((!is_numeric($array[0])) && (!is_numeric($array[2]))) {
            throw new Exception('Не число');
        }

        if (!is_numeric($array[0])) {
            $array[0] = 0;
        }
        if (!is_numeric($array[2])) {
            $array[2] = 0;
        }

        switch ($this->Member->getMotivation()) {
            case 1:
                // Проверка что такая строка уже есть
                $query = "SELECT * FROM correcting_manager where data_manager_id=? and active=1";
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос

                if (!$sth) {
                    throw new Exception('Ошибка в PDO');
                };

                if (!$sth->execute([$id])) {
                    throw new Exception("Не могу выполнить зарос к БД");
                }

                if ($sth->rowCount() > 0) {
                    $query = "update correcting_manager SET active=0, date=NOW() where data_manager_id=? and active=1";
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    if (!$sth) {
                        throw new Exception('Ошибка в PDO');
                    };
                    if (!$sth->execute([$id])) {
                        throw new Exception("Не могу выполнить зарос к БД");
                    }

                    if ($sth->rowCount() == 0) {
                        throw new Exception('Не могу обновить строки');
                    }
                }

                $query = "insert into correcting_manager (bonus, bonus_note, correct, correct_note, data_manager_id, changer_id, date) VALUES (?,?,?,?,?,?, NOW())";
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                if (!$sth) {
                    throw new Exception('Ошибка в PDO');
                };
                if (!$sth->execute([$array[0], $array[1], $array[2], $array[3], $id, $changer_id])) {
                    var_dump($sth->errorInfo());
                    throw new Exception("Не могу выполнить зарос к БД");
                }

                if ($sth->rowCount() == 0) {
                    throw new Exception('Не могу добавить строки');
                }
                break;
            case 2:
            case 3:
                // Проверка что такая строка уже есть
                $query = "SELECT * FROM correcting_tt where data_tt_id=? and member_id=? and active=1";
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос

                if (!$sth) {
                    throw new Exception('Ошибка в PDO');
                };

                if (!$sth->execute([$id, $this->Member->getId()])) {
                    throw new Exception("Не могу выполнить зарос к БД");
                }

                if ($sth->rowCount() > 0) {
                    $query = "update correcting_tt SET active=0, date=NOW() where data_tt_id=? and member_id=?  and active=1";
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    if (!$sth) {
                        throw new Exception('Ошибка в PDO');
                    };
                    if (!$sth->execute([$id, $this->Member->getId()])) {
                        throw new Exception("Не могу выполнить зарос к БД");
                    }

                    if ($sth->rowCount() == 0) {
                        throw new Exception('Не могу обновить строки');
                    }
                }

                $query = "insert into correcting_tt (bonus, bonus_note, correct, correct_note, data_tt_id, member_id, changer_id, date) VALUES (?,?,?,?,?,?,?, NOW())";
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                if (!$sth) {
                    throw new Exception('Ошибка в PDO');
                };
                if (!$sth->execute([$array[0], $array[1], $array[2], $array[3], $id, $this->Member->getId(), $changer_id])) {
                    var_dump($sth->errorInfo());
                    throw new Exception("Не могу выполнить зарос к БД");
                }

                if ($sth->rowCount() == 0) {
                    throw new Exception('Не могу добавить строки');
                }
                break;
            case 4:
                // Проверка что такая строка уже есть
                $query = "SELECT * FROM correcting_statgroup where data_statgroup_id=? and member_id=? and active=1";
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос

                if (!$sth) {
                    throw new Exception('Ошибка в PDO');
                };

                if (!$sth->execute([$id, $this->Member->getId()])) {
                    throw new Exception("Не могу выполнить зарос к БД");
                }

                if ($sth->rowCount() > 0) {
                    $query = "update correcting_statgroup SET active=0, date=NOW() where data_statgroup_id=? and member_id=? and active=1";
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    if (!$sth) {
                        throw new Exception('Ошибка в PDO');
                    };
                    if (!$sth->execute([$id, $this->Member->getId()])) {
                        throw new Exception("Не могу выполнить зарос к БД");
                    }

                    if ($sth->rowCount() == 0) {
                        throw new Exception('Не могу обновить строки');
                    }
                }

                $query = "insert into correcting_statgroup (bonus, bonus_note, correct, correct_note, data_statgroup_id, member_id, changer_id, date) VALUES (?,?,?,?,?,?,?, NOW())";
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                if (!$sth) {
                    throw new Exception('Ошибка в PDO');
                };
                if (!$sth->execute([$array[0], $array[1], $array[2], $array[3], $id, $this->Member->getId(), $changer_id])) {
                    var_dump($sth->errorInfo());
                    throw new Exception("Не могу выполнить зарос к БД");
                }

                if ($sth->rowCount() == 0) {
                    throw new Exception('Не могу добавить строки');
                }
                break;
            default:
                break;
        }
    }


    public function addCorrectionNew($array, $changer_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

//        if ((!is_numeric($array[3])) && (!is_numeric($array[5]))) {
//            echo "$array[3] =====  $array[5]<BR>";
//            throw new Exception('Не число');
//        }

        /*
         * Бонусы
         */
        if (!is_numeric($array[3])) {
            $array[3] = 0;
        }

        /*
         * Корректировки
         */
        if (!is_numeric($array[5])) {
            $array[5] = 0;
        }

        if ($array[7]) {
            $query = "SELECT * FROM correcting where member_id=? and department_id=? and date=STR_TO_DATE(?,'%m.%Y') and service_id=? and is_active=1";
        } else {
            $query = "SELECT * FROM correcting where member_id=? and department_id=? and date=STR_TO_DATE(?,'%m.%Y') and service_id is NULL and is_active=1";
        }

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if ($array[7]) {
            $args = [$array[0], $array[1], $array[2], $array[7]];
        } else {
            $args = [$array[0], $array[1], $array[2]];
        }

        if (!$sth->execute($args)) {
            throw new Exception("Не могу выполнить зарос к БД correcting: " . $sth->errorInfo()[0] );
        }

        if ($sth->rowCount() > 0) {
            if ($array[7]) {
                $query = "update correcting SET is_active=0, updated_at=NOW() where member_id=? and department_id=? and date=STR_TO_DATE(?,'%m.%Y') and service_id=? and is_active=1";
            } else {
                $query = "update correcting SET is_active=0, updated_at=NOW() where member_id=? and department_id=? and date=STR_TO_DATE(?,'%m.%Y') and service_id is null and is_active=1";
            }


            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            }

            if ($array[7]) {
                $args = [$array[0], $array[1], $array[2], $array[7]];
            } else {
                $args = [$array[0], $array[1], $array[2]];
            }

            if (!$sth->execute($args)) {
                throw new Exception("Не могу выполнить зарос на обновление строк в БД correcting. "  . $sth->errorInfo()[2]);
            }

            if ($sth->rowCount() == 0) {
                throw new Exception('Не могу обновить строки');
            }
        }
        if ($array[7]) {
            $query = "INSERT INTO correcting (date, created_at, updated_at, member_id, department_id, is_active, sum, note, changer_id, service_id) 
                    VALUES (STR_TO_DATE(?,'%m.%Y'), NOW(), NOW(), ?, ?, 1, ?, ?, ?, ?)";
        } else {
            $query = "INSERT INTO correcting (date, created_at, updated_at, member_id, department_id, is_active, sum, note, changer_id, service_id) 
                    VALUES (STR_TO_DATE(?,'%m.%Y'), NOW(), NOW(), ?, ?, 1, ?, ?, ?, NULL)";
        }

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if ($array[7]) {
            $args = [$array[2], $array[0], $array[1], $array[5], $array[6], $changer_id, $array[7]];
        } else {
            $args = [$array[2], $array[0], $array[1], $array[5], $array[6], $changer_id];
        }

        if (!$sth->execute($args)) {
            echo $changer_id."<BR>";
            echo $query."<BR>";
            echo "<pre>".print_r($args,true)."</pre><br>";
            throw new Exception("Не могу выполнить зарос к БД correcting. " . $sth->errorInfo()[2]);
        }

        if ($sth->rowCount() == 0) {
            throw new Exception('Не могу добавить строки');
        }

        /*
         *  ============ Бонусы
         */
        if ($array[7]) {
            $query = "SELECT * FROM bonus where member_id=? and department_id=? and date=STR_TO_DATE(?,'%m.%Y') and service_id=?  and is_active=1";
        } else {
            $query = "SELECT * FROM bonus where member_id=? and department_id=? and date=STR_TO_DATE(?,'%m.%Y') and service_id is null and is_active=1";
        }
//        var_dump($array); echo "<BR>";
//        var_dump($array[7]); echo "<BR>";
//        echo "bonus: ".$query."<BR>";
//        exit(0);

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };
        if ($array[7]) {
            $args = [$array[0], $array[1], $array[2], $array[7]];
        } else {
            $args = [$array[0], $array[1], $array[2]];
        }

        if (!$sth->execute($args)) {
            throw new Exception("Не могу выполнить зарос к БД");
        }

        if ($sth->rowCount() > 0) {
            if ($array[7]) {
                $query = "UPDATE bonus SET is_active=0, updated_at=NOW() where member_id=? and department_id=? and date=STR_TO_DATE(?,'%m.%Y') and service_id=? and is_active=1";
            } else {
                $query = "UPDATE bonus SET is_active=0, updated_at=NOW() where member_id=? and department_id=? and date=STR_TO_DATE(?,'%m.%Y') and service_id is null and is_active=1";
            }

            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            }

            if ($array[7]) {
                $args = [$array[0], $array[1], $array[2], $array[7]];
            } else {
                $args = [$array[0], $array[1], $array[2]];
            }

            if (!$sth->execute($args)) {
                throw new Exception("Не могу выполнить зарос на обновление строк в БД bonus. "  . $sth->errorInfo()[2]);
            }

            if ($sth->rowCount() == 0) {
                throw new Exception('Не могу обновить строки');
            }
        }

        if ($array[7]) {
            $query = "insert into bonus (date, created_at, updated_at, member_id, department_id, is_active, sum, note, changer_id, service_id) VALUES (STR_TO_DATE(?, '%m.%Y'), NOW(), NOW(), ?, ?, 1, ?, ?, ?, ?)";
        } else {
            $query = "insert into bonus (date, created_at, updated_at, member_id, department_id, is_active, sum, note, changer_id, service_id) VALUES (STR_TO_DATE(?, '%m.%Y'), NOW(), NOW(), ?, ?, 1, ?, ?, ?, NULL)";
        }
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        }

        if ($array[7]) {
            $args = [$array[2], $array[0], $array[1], $array[3], $array[4], $changer_id, $array[7]];
        } else {
            $args = [$array[2], $array[0], $array[1], $array[3], $array[4], $changer_id];
        }

        if (!$sth->execute($args)) {
            var_dump($sth->errorInfo());
            throw new Exception("Не могу выполнить зарос к БД bonus. " . $sth->errorInfo()[2] );
        }

        if ($sth->rowCount() == 0) {
            throw new Exception('Не могу добавить строки');
        }

    }

    /**
     * @param int $CommercialProcent
     */
    public function setCommercialProcent($CommercialProcent)
    {
        $this->CommercialProcent = $CommercialProcent;
    }

    /**
     * @return int
     */
    public function getCommercialProcent()
    {
        return $this->CommercialProcent;
    }

    public function fetchCommercialProcent($date)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from correcting_tt_procent where member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1 LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$this->Member->getId(), $date]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $raw = $sth->fetch(PDO::FETCH_ASSOC);
            if (!empty($raw['procent'])) {
                $this->CommercialProcent = $raw['procent'];
                $this->CommercialProcentNote = $raw['procent_note'];
            }
        }
    }

    public function changeCommercialProcent($date, $procent, $changer_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from correcting_tt_procent where member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1 LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$this->Member->getId(), $date]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $query = "update correcting_tt_procent SET active=0 where member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$this->Member->getId(), $date]);
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO' . $e->getMessage());
            }

            if ($sth->rowCount() == 0) {
                throw new Exception('Не могу обновить строки' . $sth->errorInfo()[2]);
            }
        }

        $query = "insert into correcting_tt_procent(member_id, date, procent,active, changer_id, change_date) VALUES (?, STR_TO_DATE(?, '%m.%Y'), ?, 1, ?, NOW())";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$this->Member->getId(), $date, $procent, $changer_id]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }
        $this->CommercialProcent = $procent;
    }
}


class MemberFinanceBlockCorrect
{
    private $dbh;
    private $data = null;

    public function fetchCorrection($date, $member_id, $motivation)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if ($motivation == 1 || $motivation == 4) {
            $query = "SELECT data_manager.id, 
            data_manager.date, 
            data_manager.profit, 
            st.name as statgroup_name,
            correct.correct,
            correct.correct_note FROM data_manager 
            JOIN statgroup as st ON(statgroup_id = st.id) 
            LEFT JOIN correcting_manager as correct ON (data_manager.id = correct.data_manager_id and correct.active=1) 
            where member_id=? AND data_manager.date=STR_TO_DATE(?, '%m.%Y') and correct is NOT NULL order by data_manager.id";


            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };
            if (!$sth->execute([$member_id, $date])) {
                throw new Exception("Не могу выполнить зарос к БД");
            }
            $this->data = $sth->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}