<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.08.18
 * Time: 13:42
 */

class IntChat
{
    /*
     * PDO подключение к БД
     */
    private $dbh = null;
    /*
     * Директоря куда будет происходить запись
     */
    private $target_dir = "/";
    /*
     * ID пользователя
     */
    private $userid;
    /*
     * Название файла назначения
     */
    private $target_file;

    /*
     * Подключение к БД
     */
    public function connect($errors = true)
    {
        /*
         * Настройки подключения к БД
         * Пока вшил без конфигов
         */
        try {
            $this->dbh = new PDO(
                'pgsql' .
                ':host=192.168.82.200' .
                ';port=5432' .
                ';dbname=ic5',
                "postgres",
                "secret"
            );
        } catch (PDOException $e) {
            /*
             * Если ловлю ошибку, то вывожу на экран и возвращаю false
             */
            echo $e->getMessage() . "<BR>";
            return false;
        }
        return true;
    }

    /*
     * Устанавливаю ID пользователя,
     * исходя из него определяю структуру папок и название файлов
     */
    public function setUserID($id)
    {
        /*
         * Устанавливаю id
         */
        $this->userid = $id;
        /*
         * Получаю директорию
         */
        $this->target_dir = "images/" . $id . "/";
        /*
         * Определяю полное название файла
         */
        $this->target_file = "images/" . $id . "/photo.jpg";
    }


    /*
     * Получаю картинку
     */
    public function getPicture($username)
    {
        /*
         * Обнуляю переменную $rawImage
         */
        $this->rawImage = null;

        /*
         * Проверяю подключение
         */
        if ($this->dbh == NULL) {
            throw  new Exception("Error");
        }

        /*
         * Если переданное имя нуль или пустой, то выкидываю ошибку
         */
        if (is_null($username) || empty($username)) {
            throw  new Exception("Error in username");
        }

        /*
         * Формирую SQL запрос к БД, мы должны получить валидных пользователей и ожидаем только одну запись
         */
        $query = "select picture from ic_users where username=? LIMIT 1";

        /*
         * Подготавливаю и выполняю запрос, передаю $username
         */
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$username]);
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("PDO ERROR: " . $e->getMessage());
        }

        /*
         * Получаю данные и переменную
         */
        $raw = $sth->fetch(PDO::FETCH_ASSOC);

        /*
         * Очищаю память
         */
        $sth = null;

        /*
         * Если пусто, то выдаю false, иначе
         */
        if (!empty($raw["picture"])) {
            /*
             * декодирую из base64, то что получили из БД
             */
            $this->rawImage = base64_decode($raw["picture"]);

            /*
             * Проверяю существование файла, если false, то
             */
            if (!$this->CheckExist()) {
                /*
                 * Удаляю файл, если не получается удалить швыряю exception
                 */
                if (!unlink($this->target_file)) {
                    throw new Exception('Не могу заменить файл');
                }
            }

            /*
             * Проверяю папку назначения, если ее нету, то создаю
             */
            $this->CheckTargetDir();
            /*
             * Записываю картинку
             */
            $this->writeImage();
            /*
             * Делаю resize для фото
             */
            $this->ResizeImage(200, "photo.jpg", 280);
            /*
             * Делаю resize для иконки в правом верхнем углу
             */
            $this->ResizeImage();
        } else {
            return false;
        }
    }


    /*
     * Запись картинки на диск
     */
    private function writeImage()
    {
        /*
         * Проверяю что в папку можно записать
         */
        if (is_writable($this->target_dir)) {
            /*
             * Открываем $filename в режиме "записи в конец"
             */
            if (!$handle = fopen($this->target_file, 'a')) {
                throw  new Exception("Не могу открыть файл ($this->target_file)");
            }

            /*
             *  Записываем rawImage в наш открытый файл.
             */
            if (fwrite($handle, $this->rawImage) === FALSE) {
                throw  new Exception("Не могу произвести запись в файл  ($this->target_file)");
            }
            /*
             * и закрываем дискриптор файла
             */
            fclose($handle);

        } else {
            throw  new Exception("Файл  ($this->target_file) недоступен для записи");
        }
    }

    /*
     * Функция проверяет существует такой файл или нет
     * если существует то возвращает false, иначе true
     */
    private function CheckExist()
    {
        if (file_exists($this->target_file)) {
            return false;
        }
        return true;
    }

    /*
     * Проверяет существование папки назначения
     * Если папки не существует, то создает ее
     */
    private function CheckTargetDir()
    {
        if (file_exists($this->target_dir)) {
            if (!is_dir($this->target_dir)) {
                if (is_writable($this->target_dir)) {
                    mkdir($this->target_dir);
                } else {
                    throw new Exception("Не могу создать папку: " . $this->target_dir);
                }
            }
        } else {
            mkdir($this->target_dir);
        }
        return true;
    }

    /*
     * Закрываю подключение к БД
     */
    public function close()
    {
        $this->dbh = null;
    }

    /*
     * Изменение размера и кроп картинки
     */
    private function ResizeImage($new_w = 150, $imgname = "photo-160x160.jpg", $limit_h = 150)
    {
        /*
         * Определяю переменную $imagick по пути $this->target_file
         */
        $imagick = new Imagick(realpath($this->target_file));

        /*
         * Получаю длинну и высоту картинки
         */
        $w = $imagick->getImageWidth();
        $h = $imagick->getImageHeight();


        /*
         * Если больший размер по длинне, то
         */
        if ($w > $h) {
            /*
             * Высоту выставляю в $limit_h
             * А длинну расчитываю по формуле ИСХОДНАЯ_ДЛИННА * НОВАЯ_ВЫСОТА / ИСХОДНАЯ ВЫСОТА
             */
            $resize_w = $w * $limit_h / $h;
            $resize_h = $limit_h;
        } else {
            /*
             * Иначе ...
             * Ширину выставляю в новое значение
             * а высоту расчитываю по формуле ИСХОДНАЯ_Высота * НОВАЯ_Ширина / ИСХОДНАЯ_Ширина
             */
            $resize_w = $new_w;
            $resize_h = $h * $new_w / $w;
        }

        /*
         * Если высота оказалась меньше, то пересчитваю по формуле 1
         */
        if ($resize_h < $limit_h) {

            $resize_w = $w * $limit_h / $h;
            $resize_h = $limit_h;
        }

        /*
         * Делаю resize картинки
         */
        $imagick->resizeImage($resize_w, $resize_h, Imagick::FILTER_LANCZOS, 0.9);
        /*
         * Обрезаю картинку, стараюсь не обрезать центр, делаю отступ по X
         */
        $imagick->cropImage($new_w, $limit_h, ($resize_w - $new_w) / 2, 0);
        /*
         * Записываю картинку в указанный файл
         */
        $imagick->writeImage($this->target_dir . "/" . $imgname);

        return true;
    }

}
