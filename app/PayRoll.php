<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 08.10.18
 * Time: 18:25
 */

class PayRoll
{
    private $id;

    private $rowId;
    private $flyCalculation=0;

    private $fio;
    private $email;
    private $direction;
    private $department;
    private $position;
    private $salary = 0; // Оклад

    private $Advance_payment = 0; // Выплаченный аванс

    private $Absences = 0; //Отсутствия УРВ
    private $AbsencesSum = 0;
    private $Holiday = 0; // Отпуска
    private $Disease = 0;
    private $NDFL = 0; // НДФЛ
    private $RO = 0; // ИЛ
    private $Commercial = 0; // Коммерческая премия
    private $KPI = 0;
    private $Administrative = 0; // Администраитивная премия
    private $Mobile = 0;
    private $Transport = 0;
    private $Hold = 0; // Удержания
    private $Correcting = 0; // Корректировки
    private $date;
    private $type_of_payment = 1;
    private $motivation = null;
    private $statusName = null; // для расчета пользователя
    private $statusId = null;
    private $summ=0;
    private $fullsumm=0;

    private $awhdata = array();
    private $administrativeData = array();
    private $retentionData = array();
    private $correctionData = array();
    private $KPIData = array();

    private $yearbonus_pay = 0;

    private $department_id;
    private $direction_id;
    private $position_id;

    private $baseprocent_id = NULL;
    private $advanced_procent_id = NULL;

    private $unscheduled_id = NULL;

    public $order = 0;

    private $data = NULL;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @param mixed $flyCalculation
     */
    public function setFlyCalculation($flyCalculation)
    {
        $this->flyCalculation = $flyCalculation;
    }

    /**
     * @return int
     */
    public function getFlyCalculation()
    {
        return $this->flyCalculation;
    }


    /**
     * @param mixed $rowId
     */
    public function setRowId($rowId)
    {
        $this->rowId = $rowId;
    }

    /**
     * @return mixed
     */
    public function getRowId()
    {
        return $this->rowId;
    }

    /*
     * Количество рабочих дней
     */
    private $workingdays = 0;

    /**
     * @param int $Fullsum
     */
    public function setFullSumm($fullsumm)
    {
        $this->fullsumm = $fullsumm;
    }

    /**
     * @return int
     */
    public function getFullSumm()
    {
        return $this->fullsumm;
    }

    /**
     * @param int $Transport
     */
    public function setTransport($Transport)
    {
        $this->Transport = $Transport;
    }

    /**
     * @param int $Mobile
     */
    public function setMobile($Mobile)
    {
        $this->Mobile = $Mobile;
    }

    /**
     * @return int
     */
    public function getTransport()
    {
        return $this->Transport;
    }

    /**
     * @return int
     */
    public function getMobile()
    {
        return $this->Mobile;
    }

    /**
     * @param int $workingdays
     */
    public function setWorkingdays($workingdays)
    {
        $this->workingdays = $workingdays;
    }

    /**
     * @return int
     */
    public function getWorkingdays()
    {
        return $this->workingdays;
    }

    /**
     * @param null $unscheduled_id
     */
    public function setUnscheduledId($unscheduled_id)
    {
        $this->unscheduled_id = $unscheduled_id;
    }

    /**
     * @return null
     */
    public function getUnscheduledId()
    {
        return $this->unscheduled_id;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param int $AbsencesDays
     */
    public function setAbsencesDays($AbsencesDays)
    {
        $this->Absences = $AbsencesDays;
    }

    /**
     * @return int
     */
    public function getAbsencesDays()
    {
        return $this->Absences;
    }
    /**
     * @param int $Disease
     */
    public function setDisease($Disease)
    {
        $this->Disease = $Disease;
    }

    /**
     * @param int $Holiday
     */
    public function setHoliday($Holiday)
    {
        $this->Holiday = $Holiday;
    }

    /**
     * @return int
     */
    public function getHoliday()
    {
        return $this->Holiday;
    }

    /**
     * @return int
     */
    public function getDisease()
    {
        return $this->Disease;
    }

    /**
     * @param array $KPIData
     */
    public function setKPIData($KPIData)
    {
        $this->KPIData = $KPIData;
    }


    /**
     * @return array
     */
    public function getKPIData()
    {
        return $this->KPIData;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        if (isset($position['id'])) {
            $this->position_id = $position['id'];
        }
        $this->position = $position;
    }

    /**
     * @param mixed $position_id
     */
    public function setPositionId($position_id)
    {
        $this->position_id = $position_id;
    }

    /**
     * @param mixed $direction_id
     */
    public function setDirectionId($direction_id)
    {
        $this->direction_id = $direction_id;
    }

    /**
     * @param mixed $department_id
     */
    public function setDepartmentId($department_id)
    {
        $this->department_id = $department_id;
    }

    /**
     * @param mixed $advanced_procent_id
     */
    public function setAdvancedProcentId($advanced_procent_id)
    {
        $this->advanced_procent_id = $advanced_procent_id;
    }

    /**
     * @param mixed $baseprocent_id
     */
    public function setBaseprocentId($baseprocent_id)
    {
        $this->baseprocent_id = $baseprocent_id;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return mixed
     */
    public function getPositionId()
    {
        return $this->position_id;
    }

    /**
     * @return mixed
     */
    public function getDirectionId()
    {
        return $this->direction_id;
    }

    /**
     * @return mixed
     */
    public function getDepartmentId()
    {
        return $this->department_id;
    }

    /**
     * @return mixed
     */
    public function getAdvancedProcentId()
    {
        return $this->advanced_procent_id;
    }

    /**
     * @return mixed
     */
    public function getBaseprocentId()
    {
        return $this->baseprocent_id;
    }

    /**
     * @param int $KPI
     */
    public function setKPI($KPI)
    {
        $this->KPI = $KPI;
    }

    /**
     * @return int
     */
    public function getKPI()
    {
        return $this->KPI;
    }

    /**
     * @param int $yearbonus_pay
     */
    public function setYearbonusPay($yearbonus_pay)
    {
        $this->yearbonus_pay = $yearbonus_pay;
    }

    /**
     * @return int
     */
    public function getYearbonusPay()
    {
        return $this->yearbonus_pay;
    }

    /**
     * @param array $correctionData
     */
    public function setCorrectionData($correctionData)
    {
        $this->correctionData = $correctionData;
    }

    /**
     * @return array
     */
    public function getCorrectionData()
    {
        return $this->correctionData;
    }


    /**
     * @param array $retentionData
     */
    public function setRetentionData($retentionData)
    {
        $this->retentionData = $retentionData;
    }

    /**
     * @return array
     */
    public function getRetentionData()
    {
        return $this->retentionData;
    }

    /**
     * @param array $administrativeData
     */
    public function setAdministrativeData($administrativeData)
    {
        $this->administrativeData = $administrativeData;
    }

    /**
     * @return array
     */
    public function getAdministrativeData()
    {
        return $this->administrativeData;
    }

    /**
     * @return array
     */
    public function getAwhdata()
    {
        return $this->awhdata;
    }

    /**
     * @param array $awhdata
     */
    public function setAwhdata($awhdata)
    {
        if (isset($awhdata['new_absence'])) {
            $awhdata['absence'] += $awhdata['new_absence'];
        }

        $this->awhdata = $awhdata;

//        if (isset($awhdata['disease'])) {
            $this->Disease = $awhdata['disease'];
//        } elseif (isset($awhdata['Disease'])){
//            $this->Disease = $awhdata['Disease'];
//        }

//        if (isset($awhdata['holiday'])) {
            $this->Holiday = $awhdata['holiday'];
//        } elseif (isset($awhdata['Holiday'])) {
//            $this->Holiday = $awhdata['Holiday'];
//        }

//        if (isset($awhdata['absence'])) {
            $this->Absences = $awhdata['absence'];

//        } elseif (isset($awhdata['Absence'])) {
//            $this->Absences = $awhdata['Absence'];
//        }
    }


    /**
     * @param null $statusId
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;
    }

    /**
     * @param null $statusName
     */
    public function setStatusName($statusName)
    {
        $this->statusName = $statusName;
    }

    /**
     * @return null
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * @return null
     */
    public function getStatusName()
    {
        return $this->statusName;
    }


    /**
     * @param null $motivation
     */
    public function setMotivation($motivation)
    {
        $this->motivation = $motivation;
    }

    /**
     * @return null
     */
    public function getMotivation()
    {
        return $this->motivation;
    }


    /**
     * @return int
     */
    public function getTypeOfPayment()
    {
        return $this->type_of_payment;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param int $type_of_payment
     */
    public function setTypeOfPayment($type_of_payment)
    {
        $this->type_of_payment = $type_of_payment;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param mixed $department
     */
    public function setDepartment($department)
    {
        if (isset($department['id'])) {
            $this->department_id = $department['id'];
        }
        $this->department = $department;
    }


    /**
     * @param mixed $direction
     */
    public function setDirection($direction)
    {
        if (isset($direction['id'])) {
            $this->direction_id = $direction['id'];
        }
        $this->direction = $direction;
    }

    /**
     * @return mixed
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param mixed $fio
     */
    public function setFio($fio)
    {
        $this->fio = $fio;
    }

    /**
     * @param mixed $salary
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $Absences
     */
    public function setAbsences($Absences)
    {
        $this->AbsencesSum = $Absences;
    }

    /**
     * @param mixed $Administrative
     */
    public function setAdministrative($Administrative)
    {
        $this->Administrative = $Administrative;
    }

    /**
     * @param mixed $Advance_payment
     */
    public function setAdvancePayment($Advance_payment)
    {
        $this->Advance_payment = $Advance_payment;
    }

    /**
     * @param mixed $Commercial
     */
    public function setCommercial($Commercial)
    {
        $this->Commercial = $Commercial;
    }

    /**
     * @param mixed $Correcting
     */
    public function setCorrecting($Correcting)
    {
        $this->Correcting = $Correcting;
    }

    /**
     * @param mixed $Hold
     */
    public function setHold($Hold)
    {
        $this->Hold = $Hold;
    }

    /**
     * @param mixed $RO
     */
    public function setRO($RO)
    {
        $this->RO = $RO;
    }

    /**
     * @param mixed $NDFL
     */
    public function setNDFL($NDFL)
    {
        $this->NDFL = $NDFL;
    }

    /**
     * @return mixed
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getAbsences()
    {
        return $this->AbsencesSum;
    }

    /**
     * @return mixed
     */
    public function getAdministrative()
    {
        return $this->Administrative;
    }

    /**
     * @return mixed
     */
    public function getAdvancePayment()
    {
        return $this->Advance_payment;
    }

    /**
     * @return mixed
     */
    public function getCommercial()
    {
        return $this->Commercial;
    }

    /**
     * @return mixed
     */
    public function getCorrecting()
    {
        return $this->Correcting;
    }

    /**
     * @return mixed
     */
    public function getHold()
    {
        return $this->Hold;
    }

    /**
     * @return mixed
     */
    public function getRO()
    {
        return $this->RO;
    }

    /**
     * @return mixed
     */
    public function getNDFL()
    {
        return $this->NDFL;
    }

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }

    public function setSumm($a){
        $this->summ = $a;
    }

    public function Calculation() //Расчет
    {
        switch ($this->type_of_payment) {
            case 1: // Премия
            case 3: // Аванас 2
            case 4:
                // Оклад - Отсутствия - НДФЛ - ИЛ - Выплаченный_Аванс + Коммерческая_премия + Административная_Премия - Удержания + Корректировки
                $this->summ = ($this->salary - $this->AbsencesSum - $this->NDFL - $this->RO - $this->Advance_payment + $this->Commercial + $this->KPI + $this->Administrative - $this->Hold + $this->Mobile + $this->Transport + $this->Correcting - $this->yearbonus_pay);
                break;
            case 2:
                // (Оклад / 2) -  Отсутствия + Администратинвая_премичя - Удержания
                $this->summ = ($this->Advance_payment - $this->AbsencesSum + $this->Administrative - $this->Hold);
                break;
            case 5:
                $this->summ = ($this->Commercial + $this->Administrative - $this->Hold + $this->Correcting);
                break;
        };
    }

    public function getSumm() {
        return $this->summ;
    }
}