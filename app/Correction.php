<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 25.09.18
 * Time: 13:56
 */

require_once 'app/functions.php';

class Correction
{
    private $dbh;
//                                  !!!!! ДОБАВИТЬ ПРОВЕРКУ type_of_payment в методах класса !!!!! !!!!!
    /***
     * @var type_of_payment
     * Тип выплат
     * 1 - премия
     * 2 - аванс
     * 3 - аванс2
     * 4-  рассчет
     * 5 - внеплановая выплата
     */
    private $type_of_payment;

    /*
     * Корректировка бонуса
     */
    private $BonusAdjustment = NULL; // 1
    private $BonusAdjustment_Note = NULL;

    /*
     * Расчет
     */
    private $Calculation = NULL; // 2
    private $Calculation_Note = NULL;


    /*
     * Годовой бонус, по новому стилю
     */
    private $YearBonus = NULL; // 3
    private $YearBonus_Note = NULL;

    /*
     * корректировка отпуска при увольнении
     */
    private $Holiday = NULL; // 4
    private $HolidayNote = NULL;

    /*
     * корректировка болезней при увольнении
     */
    private $Hospital = NULL; // 5
    private $HospitalNote = NULL;

    private $BonusAdjustment_lock = 0; // 1
    private $Calculation_lock = 0; // 2
    private $YearBonus_lock = 0; // 3
    private $Holiday_lock = 0; // 4
    private $Hospital_lock = 0; // 5

    public function setLockAll() {
        $this->BonusAdjustment_lock = 1;
        $this->Calculation_lock = 1;
        $this->YearBonus_lock = 1;
        $this->Holiday_lock = 1;
        $this->Hospital_lock = 1;
    }


    /**
     * @return int
     */
    public function getBonusAdjustmentLock()
    {
        return $this->BonusAdjustment_lock;
    }

    /**
     * @return int
     */
    public function getCalculationLock()
    {
        return $this->Calculation_lock;
    }

    /**
     * @return int
     */
    public function getHolidayLock()
    {
        return $this->Holiday_lock;
    }

    /**
     * @return int
     */
    public function getHospitalLock()
    {
        return $this->Hospital_lock;
    }

    /**
     * @return int
     */
    public function getYearBonusLock()
    {
        return $this->YearBonus_lock;
    }

    /**
     * @return null
     */
    public function getHoliday()
    {
        return $this->Holiday;
    }

    /**
     * @return null
     */
    public function getHolidayNote()
    {
        return $this->HolidayNote;
    }

    /**
     * @return null
     */
    public function getHospital()
    {
        return $this->Hospital;
    }

    /**
     * @return null
     */
    public function getHospitalNote()
    {
        return $this->HospitalNote;
    }

    /**
     * @param null $YearBonus
     */
    public function setYearBonus($YearBonus)
    {
        $this->YearBonus = $YearBonus;
    }

    /**
     * @param null $YearBonus_Note
     */
    public function setYearBonusNote($YearBonus_Note)
    {
        $this->YearBonus_Note = $YearBonus_Note;
    }

    /**
     * @return null
     */
    public function getYearBonus()
    {
        return $this->YearBonus;
    }

    /**
     * @return null
     */
    public function getYearBonusNote()
    {
        return $this->YearBonus_Note;
    }

    /**
     * @return type_of_payment
     */
    public function getTypeOfPayment()
    {
        return $this->type_of_payment;
    }

    /**
     * @param type_of_payment $type_of_payment
     */
    public function setTypeOfPayment($type_of_payment)
    {
        $this->type_of_payment = $type_of_payment;
    }

    /**
     * @param null $AnnualBonus
     */
    public function setAnnualBonus($AnnualBonus)
    {
        $this->AnnualBonus = $AnnualBonus;
    }

    /**
     * @param null $AnnualBonus_Note
     */
    public function setAnnualBonusNote($AnnualBonus_Note)
    {
        $this->AnnualBonus_Note = $AnnualBonus_Note;
    }

    /**
     * @param null $BonusAdjustment
     */
    public function setBonusAdjustment($BonusAdjustment)
    {
        $this->BonusAdjustment = $BonusAdjustment;
    }

    /**
     * @param null $BonusAdjustment_Note
     */
    public function setBonusAdjustmentNote($BonusAdjustment_Note)
    {
        $this->BonusAdjustment_Note = $BonusAdjustment_Note;
    }

    /**
     * @param null $Calculation
     */
    public function setCalculation($Calculation)
    {
        $this->Calculation = $Calculation;
    }

    /**
     * @param null $Сalculation_Note
     */
    public function setCalculationNote($Calculation_Note)
    {
        $this->Calculation_Note = $Calculation_Note;
    }

    /**
     * @return null
     */
    public function getAnnualBonus()
    {
        return $this->AnnualBonus;
    }

    /**
     * @return null
     */
    public function getAnnualBonusNote()
    {
        return $this->AnnualBonus_Note;
    }

    /**
     * @return null
     */
    public function getBonusAdjustment()
    {
        return $this->BonusAdjustment;
    }

    /**
     * @return null
     */
    public function getBonusAdjustmentNote()
    {
        return $this->BonusAdjustment_Note;
    }

    /**
     * @return null
     */
    public function getCalculation()
    {
        return $this->Calculation;
    }

    /**
     * @return null
     */
    public function getCalculationNote()
    {
        return $this->Calculation_Note;
    }




    /*
    * Получение данных по удержаниям при указанном ID пользователя и дате
    */
    public function fetch($id, $date)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $this->BonusAdjustment = NULL;
        $this->Calculation = NULL;
        $this->YearBonus = NULL;
        $this->Holiday = NULL;
        $this->Hospital = NULL;
        $this->BonusAdjustment_Note = NULL;
        $this->Calculation_Note = NULL;
        $this->YearBonus_Note = NULL;
        $this->HolidayNote = NULL;
        $this->Hospital_Note = NULL;

        try {
            $query = "select * from correcting_block where member_id=? and active=1 and type_of_payment=? and date=STR_TO_DATE(?,\"%m.%Y\")";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id, $this->type_of_payment, $date]);
            if ($sth->rowCount() > 0) {
                foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $item) {
                    switch ($item['type']) {
                        case 1:
                            $this->BonusAdjustment = $item['summ'];
                            $this->BonusAdjustment_Note = $item['note'];
                            break;
                        case 2:
                            $this->Calculation = $item['summ'];
                            $this->Calculation_Note = $item['note'];
                            break;
                        case 3:
                            $this->YearBonus = $item['summ'];
                            $this->YearBonus_Note = $item['note'];
                            break;
                        case 4:
                            $this->Holiday = $item['summ'];
                            $this->HolidayNote = $item['note'];
                            break;
                        case 5:
                            $this->Hospital = $item['summ'];
                            $this->HospitalNote = $item['note'];
                            break;
                    }
                }
            } else {
                /*
                 * Если выставлена тип выплаты в премию, то проверяю есть ли в авансе
                 */
                if ($this->type_of_payment == 1) { // Если тип выплаты 1(премия)
                    $query = "select * from correcting_block where member_id=? and active=1 and type_of_payment=2 and date=STR_TO_DATE(?,\"%m.%Y\")";
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$id, $date]);

                    if ($sth->rowCount() > 0) {
                        foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $item) {
                            switch ($item['type']) {
                                case 1:
                                    $this->BonusAdjustment = $item['summ'];
                                    $this->BonusAdjustment_Note = $item['note'];
                                    break;
                                case 2:
                                    $this->Calculation = $item['summ'];
                                    $this->Calculation_Note = $item['note'];
                                    break;
                                case 3:
                                    $this->YearBonus = $item['summ'];
                                    $this->YearBonus_Note = $item['note'];
                                    break;
                            }
                        }
                    }
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД retention_block. " . $e->getMessage());
        }

    }

    public function fetchFromPayout($id,$date) {
        $this->BonusAdjustment = NULL;
        $this->Calculation = NULL;
        $this->YearBonus = NULL;
        $this->Holiday = NULL;
        $this->Hospital = NULL;
        $this->BonusAdjustment_Note = NULL;
        $this->Calculation_Note = NULL;
        $this->YearBonus_Note = NULL;
        $this->HolidayNote = NULL;
        $this->HospitalNote = NULL;

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        // Получаю бонус по проведенным акциям
        try {
            $query = "select CorrectionData from financial_payout_ext where member_id=? and type_of_payment=1 and date=STR_TO_DATE(?,\"%m.%Y\") LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id, $date]);

            $raw = current($sth->fetchAll(PDO::FETCH_ASSOC));
            if ($raw) {
                foreach (unserialize($raw["CorrectionData"]) as $key=>$item) {
                    $this->$key = $item;
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }
    }

    /*
    * Получение данных по определенному типу, в зависимости от даты
    * Требуется для группового заведения данных
    */
    public function fetchAllByDate($date, $type)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        // Получаю бонус по проведенным акциям
        try {
            /*
             * Создаю SQL Запрос
             */
            $query = "select * from correcting_block where type=? and active=1 and type_of_payment=? and date=STR_TO_DATE(?,\"%m.%Y\")";
            /*
             * Подготавливаем запрос
             */
            $sth = $this->dbh->prepare($query);
            /*
             * Выполняю запрос
             */
            $sth->execute([$type, $this->type_of_payment, $date]);

            /*
             * Если количество строк > 0, то получаю данные из БД
             */
            if ($sth->rowCount() > 0) {
                /*
                 * Получаю все из БД
                 */
                $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
                /*
                 * Очищаю память
                 */
                $sth = null;


                $AllData = array();
                foreach ($raw as $item) {
                    $AllData[$item['member_id']] = $item;
                }
                $raw = null;

                return $AllData;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }
        return NULL;
    }


    /*
   * Добавление данных в БД
   * Необходимые параметры: ID пользователя, дата, тип, сумма, заметка.
   */
    public function addByType($id, $date, $type, $summ, $note, $changer_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if (is_null($this->type_of_payment)) {
            throw new Exception("Не выставлен тип выплаты!");
        }

        $dateNow = new DateTime();

        try {
            $query = "SELECT * FROM correcting_block where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and correcting_block.lock=1 and type_of_payment=?";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$type, $id, $date, $this->type_of_payment]);
            } catch (PDOException $e) {
                throw new Exception("Не могу выполнить зарос к БД" . $e->getMessage());
            }

            if ($sth->rowCount() > 0) {
                throw new Exception("Запись заблокирована, изменение невозможно");
                return false;
            }


            $query = "SELECT * FROM correcting_block where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1 and type_of_payment=?";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$type, $id, $date, $this->type_of_payment]);

            if ($sth->rowCount() > 0) {
                $query = "update correcting_block SET active=0 where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1  and correcting_block.lock=0 and type_of_payment=?";
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$type, $id, $date, $this->type_of_payment]);

                if ($sth->rowCount() == 0) {
                    throw new Exception('Не могу обновить строки: ' . $sth->errorInfo()[2]);
                }
            }

            $query = "insert into correcting_block (member_id, date, summ, note, active, change_date,type_of_payment, type, changer_id) VALUES (?, STR_TO_DATE(?, '%m.%Y'), ?, ?, 1, NOW(),?,?,?)";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id, $date, $summ, $note, $this->type_of_payment, $type, $changer_id]);

            # Если мы меняем в аванс2 в декабре месяце, то должны добавить/обновить в премию значения!
            if ($dateNow->format("m") == 12 && $this->type_of_payment == 3) {
                # Ищу активные записи по в премии
                $query = "SELECT * FROM correcting_block where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1 and type_of_payment=?";
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$type, $id, $date, 1]);

                # Если запись существует, то ставлю active=0
                if ($sth->rowCount() > 0) {
                    $query = "update correcting_block SET active=0 where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1  and correcting_block.lock=0 and type_of_payment=?";
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$type, $id, $date, 1]);
                }

                # Добавляю новые значения
                $query = "insert into correcting_block (member_id, date, summ, note, active, change_date,type_of_payment, type, changer_id) VALUES (?, STR_TO_DATE(?, '%m.%Y'), ?, ?, 1, NOW(),?,?,?)";
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$id, $date, $summ, $note, 1, $type, $changer_id]);
            }
        } catch (PDOException $e) {
            throw new Exception("Не могу выполнить зарос к БД" . $e->getMessage());
        }

        switch ($type) {
            case 1:
                $this->BonusAdjustment = $summ;
                $this->BonusAdjustment_Note = $note;
                break;
            case 2:
                $this->Calculation = $summ;
                $this->Calculation_Note = $note;
                break;
            case 3:
                $this->YearBonus = $summ;
                $this->YearBonus_Note = $note;
                break;
            case 4:
                $this->Holiday = $summ;
                $this->HolidayNote = $note;
                break;
            case 5:
                $this->Hospital = $summ;
                $this->HospitalNote = $note;
                break;
        }
    }


    public function getSumm()
    {
        if ($this->YearBonus == NULL) {
            return $this->Calculation + $this->BonusAdjustment;
        } else {
            return $this->Calculation + $this->BonusAdjustment + $this->YearBonus;
        }
    }

    public function getSummEnvelope()
    {
        if ($this->YearBonus == NULL) {
            return $this->Calculation + $this->BonusAdjustment;
        } else {
            return $this->Calculation + $this->BonusAdjustment + ($this->YearBonus % 100000);
        }
    }

    public function getYearBonusSumm() {
        if ($this->YearBonus != NULL) {
            return intdiv($this->YearBonus , 100000)*100000;
        }
    }

    public function getAllData(){
        $data = array();
        $data[] = array("summ"=>$this->BonusAdjustment,"note"=>$this->BonusAdjustment_Note);
        $data[] = array("summ"=>$this->Calculation,"note"=>$this->Calculation_Note);
        $data[] = array("summ"=>$this->YearBonus,"note"=>$this->YearBonus_Note);

        return $data;
    }
}