<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 13.09.18
 * Time: 11:40
 */

require_once 'app/functions.php';
require_once 'app/ErrorInterface.php';

/***
 * Class DataParserInterface
 * Парсинг строк и добавление в массив
 */
class DataParserInterface
{
    protected $dbh;
    protected $data = array();
    protected $data_tt = array();
    protected $data_stat = array();
    protected $data_service = array();

    protected $members;
    protected $statgroup;
    protected $departments;
    private $date; //Дата

    private $_monthsList = array(
        "Январь"=>"01","Февраль"=>"02","Март"=>"03",
        "Апрель"=>"04","Май"=>"05", "Июнь"=>"06",
        "Июль"=>"07","Август"=>"08","Сентябрь"=>"09",
        "Октябрь"=>"10","Ноябрь"=>"11","Декабрь"=>"12");

    public function __construct(&$members, &$statgroup, &$departments)
    {
        /*
         * Присваиваем основные переменные в конструкторе класса
         */

        $this->members = $members;
        $this->statgroup=$statgroup;
        $this->departments=$departments;

        if (!$this->dbh) {
            $this->dbh = dbConnect();
        }
    }

    /*
     * Получаю дату
     */
    public function getDate() {
        return $this->date;
    }

    /*
     * Парсер csv файла
     */
    public function parser($array, &$Error)
    { //Передаю массив и формирую объект
        list($data_all, $name,$statgroup,$departments,$service,$sale, $const_price, $profit, $profit_procent) = $array;

        // Получаю дату из файла
        list($month, $year) = explode(" ",$data_all);
        $year += 2000; // отдает только 2 последние цифры, добавляем 2к
        $nmonth = $this->_monthsList[$month];
        $date = $year."-".$nmonth."-00";
        if (empty($this->date)) {
            $this->date = $date;
        }

        /*
         * Преобразую строку с , в строку с .
         * Т.к. расчет ведем с точкой
         */
        $sale = str_replace(',','.',$sale);
        $const_price = str_replace(',','.',$const_price);
        $profit = str_replace(',','.',$profit);
        $profit_procent = str_replace(',','.',$profit_procent);

        /*
         * Деление на 0 1С показывает как !!!!
         */
        $profit_procent = str_replace("!!!!","0",$profit_procent);

        /*
         * Ищем ID менеджера сервиса в массиве объектов
         */
        $result = current(array_filter($this->members, function ($object) use ($name) {
            return $object->getSkladName() == $name;
        }));
        if ($result) {
            $member_id = $result->getId();
        } else {
            $member_id = "";
            $Error->add("Не обнаружен пользователь: '".$name."'");
        }

        /*
         * Ищем ID менеджера сервиса в массиве объектов
         */
        if ($service!="< не выбран >") {
            $result = current(array_filter($this->members, function ($object) use ($service) { //Менеджер сервиса
                return $object->getSkladName() == $service;
            }));
            if ($result) {
                $service_id = $result->getId();
            } else {
                $service_id = "";
                $Error->add("Не обнаружен пользователь(сервис): '" . $service . "'");
            }
        } else {
            $service = "";
            $service_id = "";
        }

        /*
         * Ищем ID статгруппы
         */
        $result = null;
        $result = current(array_filter($this->statgroup, function ($object) use ($statgroup) {
            return $object->getName() == $statgroup;
        }));
        if ($result) {
            $statgroup_id = $result->getId();
        } else {
            $Error->add("Не обнаружена статгруппа: '".$statgroup."'");
        }

        /*
         * Ищем ID ТТ
         */
        $result = null;
        $result = current(array_filter($this->departments, function ($object) use ($departments) {
            return $object->getNamesklad() == $departments;
        }));

        if ($result) {
            $department_id = $result->getId();
        } else {
            $Error->add("Не обнаружена ТТ: '".$departments."'");
        }

        //*********************   Менеджер   *********************
        // Если пустой ID значит пользователя не нашли и можно игнорировать
        if (isset($member_id) && !empty($member_id) && isset($statgroup_id) && !empty($statgroup_id) && isset($department_id) && !empty($department_id) && isset($service_id)) {
            /// Проверяем что в массиве объектов уже есть данная пользователя
            $mbr = current(array_filter($this->data, function ($obj) use ($member_id, $statgroup_id, $department_id, $service_id, $date) {
                return $obj->getDate() == $date && $obj->getDepartmentId() == $department_id && $obj->getId() == $member_id && $obj->getStatgroupId() == $statgroup_id;
            }));

            if (!$mbr) {
                $DS = new DataManager();
                $DS->setName($name);
                $DS->setId($member_id);
                $DS->setStatgroupId($statgroup_id);
                $DS->setStatgroupName($statgroup);
                $DS->setDepartmentId($department_id);
                $DS->setDepartmentName($departments);
                $DS->setDate($date);
                $DS->setSale($sale);
                $DS->setConstPrice($const_price);
                $DS->setProfit($profit);
                if ($const_price != 0) {
                    $DS->setProfitProcent($profit / $const_price * 100);
                } else {
                    $DS->setProfitProcent(0);
                }
                array_push($this->data, $DS); // Все данные
            } else {
                $mbr->setSale($mbr->getSale() + $sale);
                $mbr->setConstprice($mbr->getConstprice() + $const_price);
                $mbr->setProfit($mbr->getProfit() + $profit);
                if ($mbr->getConstprice() != 0 ) {
                    $mbr->setProfitProcent($mbr->getProfit() / $mbr->getConstprice() * 100);
                } else {
                    $mbr->setProfitProcent(0);
                }
            }
        }

        //*********************   TT   *********************
        /// Проверяем что в массиве объектов уже есть данная ТТ
        if ((isset($department_id) && !empty($department_id))) {
            $tt = current(array_filter($this->data_tt, function ($obj) use ($department_id,$date) {
                return $obj->getId() == $department_id && $obj->getDate() == $date;
            }));

            if (!$tt) { //Если нету, то добавляем
                $DTT = new DataItem();
                $DTT->setDate($date);
                $DTT->setName($departments);
                $DTT->setId($department_id);
                $DTT->setSale($sale);
                $DTT->setConstprice($const_price);
                $DTT->setProfit($profit);
                if ($const_price != 0) {
                    $DTT->setProfitProcent($profit / $const_price * 100);
                } else {
                    $DTT->setProfitProcent(0);
                }

                array_push($this->data_tt, $DTT);
            } else { //Иначе плюсуем продажи итд
                $tt->setSale($tt->getSale() + $sale);
                $tt->setConstprice($tt->getConstprice() + $const_price);
                $tt->setProfit($tt->getProfit() + $profit);
                if ($tt->getConstprice() != 0) {
                    $tt->setProfitProcent($tt->getProfit() / $tt->getConstprice() * 100);
                } else {
                    $tt->setProfitProcent(0);
                }
            }
        }

        //*********************   StatGroup   *********************
        /// Проверяем что в массиве объектов уже есть данные по статгруппе
        if (isset($statgroup_id) && !empty($statgroup_id)) {
            $stat = current(array_filter($this->data_stat, function ($obj) use ($statgroup_id, $date) {
                return $obj->getId() == $statgroup_id && $obj->getDate() == $date;
            }));

            if (!$stat) { //Если нету, то добавляем
                $DST = new DataItem();
                $DST->setDate($date);
                $DST->setName($statgroup);
                $DST->setId($statgroup_id);
                $DST->setSale($sale);
                $DST->setConstprice($const_price);
                $DST->setProfit($profit);
                if ($const_price != 0) {
                    $DST->setProfitProcent($profit / $const_price * 100);
                } else {
                    $DST->setProfitProcent(0);
                }

                array_push($this->data_stat, $DST);
            } else { //Иначе плюсуем продажи итд
                $stat->setSale($stat->getSale() + $sale);
                $stat->setConstprice($stat->getConstprice() + $const_price);
                $stat->setProfit($stat->getProfit() + $profit);
                if ($stat->getConstprice() != 0) {
                    $stat->setProfitProcent($stat->getProfit() / $stat->getConstprice() * 100);
                } else {
                    $stat->setProfitProcent(0);
                }
            }
        }

        //*********************   Service   *********************
        //Если service_id пустой, то в БД не нашли пользователя
        if (isset($service_id) && !empty($service_id)) {
            /// Проверяем что в массиве объектов уже есть данная Service
            $srv = current(array_filter($this->data_service, function ($obj) use ($service_id, $date) {
                return $obj->getId() == $service_id && $obj->getDate() == $date;
            }));

            if (!$srv) { //Если нету, то добавляем
                $DSRV = new DataItem();
                $DSRV->setDate($date);
                $DSRV->setName($service);
                $DSRV->setId($service_id);
                $DSRV->setSale($sale);
                $DSRV->setConstprice($const_price);
                $DSRV->setProfit($profit);
                if ($const_price != 0) {
                    $DSRV->setProfitProcent($profit / $const_price * 100);
                } else {
                    $DSRV->setProfitProcent(0);
                }

                array_push($this->data_service, $DSRV);
            } else { //Иначе плюсуем продажи итд
                $srv->setSale($srv->getSale() + $sale);
                $srv->setConstprice($srv->getConstprice() + $const_price);
                $srv->setProfit($srv->getProfit() + $profit);
                if ($srv->getConstprice() != 0 ) {
                    $srv->setProfitProcent($srv->getProfit() / $srv->getConstprice() * 100);
                } else {
                    $srv->setProfitProcent(0);
                }
            }
        }
    }

    public function count(){
        return count($this->data);
    }

    public function count_tt(){
        return count($this->data_tt);
    }

    public function count_stat(){
        return count($this->data_stat);
    }

    public function count_service(){
        return count($this->data_service);
    }

    public function get() {
        return $this->data;
    }

    public function get_tt() {
        return $this->data_tt;
    }

    public function get_stat() {
        return $this->data_stat;
    }

    public function get_service() {
        return $this->data_service;
    }

    public function add(&$dataObj, &$Error) {
        $query = "select * from data_manager where date=? and member_id=? and statgroup_id=? and department_id=? order by id";
        $sth = $this->dbh->prepare($query);
        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };
        $sth->execute([$dataObj->getDate(), $dataObj->getId(), $dataObj->getStatgroupId(), $dataObj->getDepartmentId()]);

        if ($sth->rowCount() == 0) {
            $query = "insert into data_manager (date,member_id,statgroup_id,department_id,sales,const_price,profit,profit_procent) VALUES (?,?,?,?,?,?,?,?)";
            $sth = $this->dbh->prepare($query);
            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            $sth->execute([$dataObj->getDate(), $dataObj->getId(), $dataObj->getStatgroupId(), $dataObj->getDepartmentId(), $dataObj->getSale(), $dataObj->getConstPrice(), $dataObj->getProfit(), $dataObj->getProfitProcent()]);
            if ($sth->rowCount() == 0) {
                $Error->add("Не могу добавить запись в БД: " . $dataObj->getDate() . " " . $dataObj->getName() . " " . $dataObj->getStatgroupName() . " " . $dataObj->getDepartmentName());
            }
        } else {
            $Error->add("Такая запись уже присутсвует в БД: ".$dataObj->getDate() . " " . $dataObj->getName() . " " . $dataObj->getStatgroupName() . " " . $dataObj->getDepartmentName());
        }

        $sth = null;
    }

    public function add_tt(&$dataObj, &$Error) {
        $query = "select * from data_TT where date=? and department_id=? order by id";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$dataObj->getDate(),$dataObj->getId()]);
        if ($sth->rowCount()>0) {
            $Error->add("TT. Такая запись уже присутсвует: ".$dataObj->getDate()." ".$dataObj->getName());
        } else {
            $query = "insert into data_TT (date,department_id,sales,const_price,profit,profit_procent) VALUES (?,?,?,?,?,?)";
            $sth = $this->dbh->prepare($query);
            if(!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            $sth->execute([$dataObj->getDate(),$dataObj->getId(),$dataObj->getSale(),$dataObj->getConstPrice(),$dataObj->getProfit(),$dataObj->getProfitProcent()]);
            if ($sth->rowCount()==0) {
                $Error->add("Не могу добавить запись в БД: ".$dataObj->getDate()." ".$dataObj->getName());
            }
        }

        $sth = null;
    }

    public function add_statgroup(&$dataObj, &$Error) {
        $query = "select * from data_statgroup where date=? and statgroup_id=? order by id";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$dataObj->getDate(),$dataObj->getId()]);
        if ($sth->rowCount()>0) {
            $Error->add("StatGroup. Такая запись уже присутсвует: ".$dataObj->getDate()." ".$dataObj->getName());
        } else {
            $query = "insert into data_statgroup (date,statgroup_id,sales,const_price,profit,profit_procent) VALUES (?,?,?,?,?,?)";
            $sth = $this->dbh->prepare($query);
            if(!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            $sth->execute([$dataObj->getDate(),$dataObj->getId(),$dataObj->getSale(),$dataObj->getConstPrice(),$dataObj->getProfit(),$dataObj->getProfitProcent()]);
            if ($sth->rowCount()==0) {
                $Error->add("Не могу добавить запись в БД: ".$dataObj->getDate()." ".$dataObj->getName());
            }
        }

        $sth = null;
    }

    public function add_service(&$dataObj, &$Error) {
        $query = "select * from data_service where date=? and member_id=? order by id";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$dataObj->getDate(),$dataObj->getId()]);
        if ($sth->rowCount()>0) {
            $Error->add("Service: Такая запись уже присутсвует: ".$dataObj->getDate()." ".$dataObj->getName());
        } else {
            $query = "insert into data_service (date,member_id,sales,const_price,profit,profit_procent) VALUES (?,?,?,?,?,?)";
            $sth = $this->dbh->prepare($query);
            if(!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            $sth->execute([$dataObj->getDate(),$dataObj->getId(),$dataObj->getSale(),$dataObj->getConstPrice(),$dataObj->getProfit(),$dataObj->getProfitProcent()]);
            if ($sth->rowCount()==0) {
                $Error->add("Не могу добавить запись в БД: ".$dataObj->getDate()." ".$dataObj->getName());
            }
        }

        $sth = null;
    }

    public function addUpdate(&$dataObj, &$Error) {
        $query = "select * from data_manager where date=? and member_id=? and statgroup_id=? and department_id=? order by id";
        $sth = $this->dbh->prepare($query);
        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$dataObj->getDate(), $dataObj->getId(), $dataObj->getStatgroupId(), $dataObj->getDepartmentId()]);

        if ($sth->rowCount() == 0) {

            $query = "insert into data_manager (date,member_id,statgroup_id,department_id,sales,const_price,profit,profit_procent) VALUES (?,?,?,?,?,?,?,?)";
            $sth = $this->dbh->prepare($query);
            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            $sth->execute([$dataObj->getDate(), $dataObj->getId(), $dataObj->getStatgroupId(), $dataObj->getDepartmentId(), $dataObj->getSale(), $dataObj->getConstPrice(), $dataObj->getProfit(), $dataObj->getProfitProcent()]);
            if ($sth->rowCount() == 0) {
                $Error->add("Не могу добавить запись в БД: " . $dataObj->getDate() . " " . $dataObj->getName() . " " . $dataObj->getStatgroupName() . " " . $dataObj->getDepartmentName());
            }
        } else {
//            $Error->add("Такая запись уже присутсвует в БД: ".$dataObj->getDate() . " " . $dataObj->getName() . " " . $dataObj->getStatgroupName() . " " . $dataObj->getDepartmentName());

            $raw = current($sth->fetchAll());
            if ($raw['sales'] != $dataObj->getSale() || $raw['sales'] != $dataObj->getConstPrice() || $raw['profit'] != $dataObj->getProfit() || $raw['profit_procent'] != $dataObj->getProfitProcent()) {
//                echo "UPDATE<BR>";
                $query = "UPDATE data_manager SET sales=?, const_price=?, profit=?, profit_procent=? where date=? AND member_id=? AND statgroup_id=? AND department_id=?";
                $sth = $this->dbh->prepare($query);
                if (!$sth) {
                    throw new Exception('Ошибка в PDO');
                };

                $sth->execute([$dataObj->getSale(), $dataObj->getConstPrice(), $dataObj->getProfit(), $dataObj->getProfitProcent(), $dataObj->getDate(), $dataObj->getId(), $dataObj->getStatgroupId(), $dataObj->getDepartmentId()]);
            }
//            if ($dataObj->getId()==225) {
//                echo $dataObj->getSale().", ".$dataObj->getConstPrice().", ".$dataObj->getProfit().", ".$dataObj->getProfitProcent().", ".$dataObj->getDate().", ".$dataObj->getId().", ".$dataObj->getStatgroupId().", ".$dataObj->getDepartmentId();
//            }
        }

        $sth = null;
    }

    public function addUpdate_tt(&$dataObj, &$Error) {
        $query = "select * from data_TT where date=? and department_id=? order by id";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$dataObj->getDate(),$dataObj->getId()]);
        if ($sth->rowCount()>0) {
//            $Error->add("TT. Такая запись уже присутсвует: ".$dataObj->getDate()." ".$dataObj->getName());
            $raw = current($sth->fetchAll());
            if ($raw['sales'] != $dataObj->getSale() || $raw['sales'] != $dataObj->getConstPrice() || $raw['profit'] != $dataObj->getProfit() || $raw['profit_procent'] != $dataObj->getProfitProcent()) {
                $query = "UPDATE data_TT SET sales=?,const_price=?,profit=?,profit_procent=? where date=? AND department_id=?";
                $sth = $this->dbh->prepare($query);
                if (!$sth) {
                    throw new Exception('Ошибка в PDO');
                };

                $sth->execute([$dataObj->getSale(), $dataObj->getConstPrice(), $dataObj->getProfit(), $dataObj->getProfitProcent(), $dataObj->getDate(), $dataObj->getId()]);
            }
        } else {
            $query = "insert into data_TT (date,department_id,sales,const_price,profit,profit_procent) VALUES (?,?,?,?,?,?)";
            $sth = $this->dbh->prepare($query);
            if(!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            $sth->execute([$dataObj->getDate(),$dataObj->getId(),$dataObj->getSale(),$dataObj->getConstPrice(),$dataObj->getProfit(),$dataObj->getProfitProcent()]);
            if ($sth->rowCount()==0) {
                $Error->add("Не могу добавить запись в БД: ".$dataObj->getDate()." ".$dataObj->getName());
            }
        }

        $sth = null;
    }

    public function addUpdate_statgroup(&$dataObj, &$Error) {
        $query = "select * from data_statgroup where date=? and statgroup_id=? order by id";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$dataObj->getDate(),$dataObj->getId()]);
        if ($sth->rowCount()>0) {
//            $Error->add("StatGroup. Такая запись уже присутсвует: ".$dataObj->getDate()." ".$dataObj->getName());
            $raw = current($sth->fetchAll());
            if ($raw['sales'] != $dataObj->getSale() || $raw['sales'] != $dataObj->getConstPrice() || $raw['profit'] != $dataObj->getProfit() || $raw['profit_procent'] != $dataObj->getProfitProcent()) {
                $query = "UPDATE data_statgroup SET sales=?,const_price=?,profit=?,profit_procent=? where date=? AND statgroup_id=?";
                $sth = $this->dbh->prepare($query);
                if (!$sth) {
                    throw new Exception('Ошибка в PDO');
                };

                $sth->execute([$dataObj->getSale(), $dataObj->getConstPrice(), $dataObj->getProfit(), $dataObj->getProfitProcent(), $dataObj->getDate(), $dataObj->getId()]);
            }
        } else {
            $query = "insert into data_statgroup (date,statgroup_id,sales,const_price,profit,profit_procent) VALUES (?,?,?,?,?,?)";
            $sth = $this->dbh->prepare($query);
            if(!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            $sth->execute([$dataObj->getDate(),$dataObj->getId(),$dataObj->getSale(),$dataObj->getConstPrice(),$dataObj->getProfit(),$dataObj->getProfitProcent()]);
            if ($sth->rowCount()==0) {
                $Error->add("Не могу добавить запись в БД: ".$dataObj->getDate()." ".$dataObj->getName());
            }
        }

        $sth = null;
    }

    public function addUpdate_service(&$dataObj, &$Error) {
        $query = "select * from data_service where date=? and member_id=? order by id";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$dataObj->getDate(),$dataObj->getId()]);

        if ($sth->rowCount()>0) {
//            $Error->add("Service: Такая запись уже присутсвует: ".$dataObj->getDate()." ".$dataObj->getName());
            $raw = current($sth->fetchAll());
            if ($raw['sales'] != $dataObj->getSale() || $raw['sales'] != $dataObj->getConstPrice() || $raw['profit'] != $dataObj->getProfit() || $raw['profit_procent'] != $dataObj->getProfitProcent()) {
                $query = "UPDATE data_service SET sales=?,const_price=?,profit=?,profit_procent=? where date=? AND member_id=?";
                $sth = $this->dbh->prepare($query);
                if (!$sth) {
                    throw new Exception('Ошибка в PDO');
                };

                $sth->execute([$dataObj->getSale(), $dataObj->getConstPrice(), $dataObj->getProfit(), $dataObj->getProfitProcent(), $dataObj->getDate(), $dataObj->getId()]);
            }
        } else {
            $query = "insert into data_service (date,member_id,sales,const_price,profit,profit_procent) VALUES (?,?,?,?,?,?)";
            $sth = $this->dbh->prepare($query);
            if(!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            $sth->execute([$dataObj->getDate(),$dataObj->getId(),$dataObj->getSale(),$dataObj->getConstPrice(),$dataObj->getProfit(),$dataObj->getProfitProcent()]);
            if ($sth->rowCount()==0) {
                $Error->add("Не могу добавить запись в БД: ".$dataObj->getDate()." ".$dataObj->getName());
            }
        }

        $sth = null;
    }

    public function del() {
        if (empty($this->date)) {
            throw  new Exception("Сначала нужно выполнить метод parse");
        }
        $query = "delete from data_manager where date=?";
        $sth = $this->dbh->prepare($query);
        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };
        $sth->execute([$this->date]);
    }

    public function del_tt() {
        if (empty($this->date)) {
            throw  new Exception("Сначала нужно выполнить метод parse");
        }

        $query = "delete from data_TT where date=?";
        $sth = $this->dbh->prepare($query);
        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };
        $sth->execute([$this->date]);
    }

    public function del_statgroup() {
        if (empty($this->date)) {
            throw  new Exception("Сначала нужно выполнить метод parse");
        }

        $query = "delete from data_statgroup where date=?";
        $sth = $this->dbh->prepare($query);
        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };
        $sth->execute([$this->date]);
    }

    public function del_service() {
        if (empty($this->date)) {
            throw  new Exception("Сначала нужно выполнить метод parse");
        }

        $query = "delete from data_service where date=?";
        $sth = $this->dbh->prepare($query);
        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };
        $sth->execute([$this->date]);
    }

}