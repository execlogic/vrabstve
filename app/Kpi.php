<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 31.05.19
 * Time: 12:58
 */

class KpiData
{
    private $id = NULL;
    private $number = NULL;
    private $summ = NULL;
    private $note = NULL;

    private $changer_id = NULL;
    private $changer_date = NULL;

    private $default = false;
    private $Lock = 0;

    /**
     * @param int $Lock
     */
    public function setLock()
    {
        $this->Lock = 1;
    }

    /**
     * @return int
     */
    public function getLock()
    {
        return $this->Lock;
    }

    /**
     * @param bool $default
     */
    public function setDefault($default)
    {
        $this->default = $default;
    }

    public function getDefault()
    {
        return $this->default;
    }


    /**
     * @param null $changer_date
     */
    public function setChangerDate($changer_date)
    {
        $this->changer_date = $changer_date;
    }

    /**
     * @param null $changer_id
     */
    public function setChangerId($changer_id)
    {
        $this->changer_id = $changer_id;
    }

    /**
     * @return null
     */
    public function getChangerDate()
    {
        return $this->changer_date;
    }

    /**
     * @return null
     */
    public function getChangerId()
    {
        return $this->changer_id;
    }


    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @param mixed $summ
     */
    public function setSumm($summ)
    {
        $this->summ = $summ;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return mixed
     */
    public function getSumm()
    {
        return $this->summ;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

}

class Kpi
{
    private $KpiData = array();
    private $dbh;

    /*
     * Получает значения из таблицы по-умолчанию для сотрудника
     * при условии что изменения были в диапозоне дат
     */
    public function fetchWithDate($member_id, $date)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT * FROM member_kpi where 
        member_id=? and 
        (date_s BETWEEN '0000-00-00' AND DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'),INTERVAL 1 MONTH)) and (date_e BETWEEN DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'),INTERVAL 1 MONTH) AND '9999-01-01') 
        order by id desc";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id, $date, $date]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC); // возвращаем извлеченные строки

            foreach ($raw as $item) {
                $KD = new KpiData();
                $KD->setId($item['id']);
                $KD->setNumber($item['num']);
                $KD->setSumm($item['summ']);
                $KD->setNote($item['note']);
                $KD->setDefault(true);

                array_push($this->KpiData, $KD);
            }
        };
    }


    /*
     * Метод получает данные из таблицы значений по-умолчанию для сотрудника
     */
    public function fetch($member_id)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $this->KpiData = [];

        $query = "SELECT * FROM member_kpi where 
        member_id=? and 
        active = 1 
        order by id desc";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
            foreach ($raw as $item) {
                $KD = new KpiData();
                $KD->setId($item['id']);
                $KD->setNumber($item['num']);
                $KD->setSumm($item['summ']);
                $KD->setNote($item['note']);
                $KD->setDefault(true);

                array_push($this->KpiData, $KD);
            }
        };
    }

    /*
     * Получить данные для ведомости
     * Данный метод получает данные из значений по умолчанию
     * А потом получает данные из таблицы с измененными значениями
     */
    public function fetchPayment($member_id, $date, $type_of_payment)
    {

        /*
         * Получаю значения по-умолчанию
         */
        $this->fetchWithDate($member_id, "01." . $date);

        /*
         * Проверяю есть ли подключение к БД
         */
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        /*
         * Составляю SQL запрос
         */
        $query = "SELECT * FROM kpi_block where 
        member_id=? and 
        date = STR_TO_DATE(?, '%m.%Y') AND 
        type_of_payment = ? and
        active = 1 
        order by id desc";

        /*
         * Выполняю его
         */
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id, $date, $type_of_payment]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        /*
         * Если количество строк больше чем 0, значит данные присутствуют в БД
         */
        if ($sth->rowCount() > 0) { // Если число строк > 0
            /*
             * Получаю все строки из БД
             */
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
            /*
             * Разбираю строки
             */
            foreach ($raw as $item) {
                /*
                 * Пытаюсь найти данные в массиве данных
                 */
                $cr_kpi = current(array_filter($this->KpiData, function ($k) use ($item) {
                    return ($k->getNumber() == $item["num"]);
                }));

                /*
                 * Если такой объект в массиве данных существует
                 */
                if ($cr_kpi) {
                    /*
                     * Устанавливаю значения через методы объекта
                     */
                    $cr_kpi->setId($item['id']);

                    $cr_kpi->setNumber($item['num']);
                    $cr_kpi->setSumm($item['summ']);
                    $cr_kpi->setNote($item['note']);

                    $cr_kpi->setChangerId($item['changer_id']);
                    $cr_kpi->setChangerDate($item['change_date']);
                    $cr_kpi->setDefault(false);
                } else {
                    /*
                     * Иначе создаю новый объект и добавляю его в массив
                     */
                    $KD = new KpiData();
                    $KD->setId($item['id']);

                    $KD->setNumber($item['num']);
                    $KD->setSumm($item['summ']);
                    $KD->setNote($item['note']);

                    $KD->setChangerId($item['changer_id']);
                    $KD->setChangerDate($item['change_date']);
                    $KD->setDefault(false);
                    array_push($this->KpiData, $KD);
                }
            }
        };
    }

    public function fetchPayment2($member_id, $date, $type_of_payment)
    {

        /*
         * Получаю значения по-умолчанию
         */
        $this->fetch($member_id);

        /*
         * Проверяю есть ли подключение к БД
         */
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        /*
         * Составляю SQL запрос
         */
        $query = "SELECT * FROM kpi_block where 
        member_id=? and 
        date = STR_TO_DATE(?, '%m.%Y') AND 
        type_of_payment = ? and
        active = 1 
        order by id desc";

        /*
         * Выполняю его
         */
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id, $date, $type_of_payment]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        /*
         * Если количество строк больше чем 0, значит данные присутствуют в БД
         */
        if ($sth->rowCount() > 0) { // Если число строк > 0
            /*
             * Получаю все строки из БД
             */
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
            /*
             * Разбираю строки
             */
            foreach ($raw as $item) {
                /*
                 * Пытаюсь найти данные в массиве данных
                 */
                $cr_kpi = current(array_filter($this->KpiData, function ($k) use ($item) {
                    return ($k->getNumber() == $item["num"]);
                }));

                /*
                 * Если такой объект в массиве данных существует
                 */
                if ($cr_kpi) {
                    /*
                     * Устанавливаю значения через методы объекта
                     */
                    $cr_kpi->setId($item['id']);

                    $cr_kpi->setNumber($item['num']);
                    $cr_kpi->setSumm($item['summ']);
                    $cr_kpi->setNote($item['note']);

                    $cr_kpi->setChangerId($item['changer_id']);
                    $cr_kpi->setChangerDate($item['change_date']);
                    $cr_kpi->setDefault(false);
                } else {
                    /*
                     * Иначе создаю новый объект и добавляю его в массив
                     */
                    $KD = new KpiData();
                    $KD->setId($item['id']);

                    $KD->setNumber($item['num']);
                    $KD->setSumm($item['summ']);
                    $KD->setNote($item['note']);

                    $KD->setChangerId($item['changer_id']);
                    $KD->setChangerDate($item['change_date']);
                    $KD->setDefault(false);
                    array_push($this->KpiData, $KD);
                }
            }
        };
    }

    public function get()
    {
        return $this->KpiData;
    }

    public function getByNum($num)
    {
        $cr = current(array_filter($this->KpiData, function ($k) use ($num) {
            return $k->getNumber() == $num;
        }));

//        var_dump($cr);
        return $cr;
    }

    /*
     * Обновление и/или добавление данных значения по-умолчанию в профиле пользователя
     */
    public function update($member_id, $item, $changer_id)
    {
        /*
         * Проверяю подключение
         */
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        /*
         * Если нету данных, то пытаюсь получить актуальные (active == 1)
         */
//        if (count($this->KpiData) == 0) {
        $this->fetch($member_id);
//        }

        /*
         * Проверяю значения, если summ не число, то выходим игнорируя
         */
        if (!is_numeric($item['summ'])) {
            return 0;
        }

        /*
         * Вырезаем потенциально опасные моменты из заметки
         */
        $item['note'] = strip_tags($item['note']);
        $item['note'] = htmlspecialchars($item['note']);

        /*
         * Ищу в массиве объектов, объект который пронумерован как в передаваемом $item
         */
        $cr_kpi = current(array_filter($this->KpiData, function ($k) use ($item) {
            return ($k->getNumber() == $item["num"]);
        }));

        /*
         * Если объект существует
         */
        if ($cr_kpi) {
            /*
             * Если сумма != передаваемой сумме ИЛИ заметка != передаваемой заметке, то обновляем значения
             */
            if ($cr_kpi->getSumm() != $item['summ'] || $cr_kpi->getNote() != $item['note']) {
                $query = "select * from member_kpi where member_id=? and date=STR_TO_DATE(?, '%m.%Y') and num=? and lock=1";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$member_id, $item['date'], $item['num']]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }

                if ($sth->rowCount() > 0) { // Если число строк > 0
                    throw new Exception("Запись заблокирована");
                    return false;
                }

                /*
                 * Выставляю активность == 0, через update
                 */
                $query = "update member_kpi set active=0, date_e=NOW(), changer_id=? where id=?";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$changer_id, $item['id']]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }

                /*
                 * Если количество обновленных строк > 0, то добавляю новые данные
                 */
                if ($sth->rowCount() > 0) { // Если число строк > 0
                    $query = "insert into member_kpi (member_id, num, summ, note, date_s, changer_id) VALUE(?, ?, ?, ?, NOW(), ?)";
                    try {
                        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                        $sth->execute([$member_id, $item['num'], $item['summ'], $item['note'], $changer_id]); // Выполняем запрос
                    } catch (PDOException $e) {
                        throw new Exception('Ошибка в PDO ' . $e->getMessage());
                    }

                    /*
                     * Выставляю измененные значения в найденный объект
                     */
                    $cr_kpi->setSumm($item['summ']);
                    $cr_kpi->setNote($item['note']);
                }
            }
            /*
             * Если объект не существует
             * и сумма в передаваемом значении не пустая, то добавляю строку
             */
        } else if ($item['summ'] != "") {
            $query = "insert into member_kpi (member_id, num, summ, note, date_s, changer_id) VALUE(?, ?, ?, ?, NOW(), ?)";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$member_id, $item['num'], $item['summ'], $item['note'], $changer_id]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO ' . $e->getMessage());
            }

            /*
             * Создаю новый объект и добавляю в массив данных
             */
            $KD = new KpiData();
            $KD->setId($item['id']);

            $KD->setNumber($item['num']);
            $KD->setSumm($item['summ']);
            $KD->setNote($item['note']);

            $KD->setDefault(true);
            array_push($this->KpiData, $KD);
        }
    }

    /*
     * Обновление значений по-умолчанию, с установленной даты
     */
    public function updateWithDate($member_id, $item, $date)
    {
        /*
        * Проверяю подключение
        */
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        /*
         * Если нету данных, то пытаюсь получить актуальные (active == 1)
         */
        if (count($this->KpiData) == 0) {
            $this->fetch($member_id);
        }

        /*
         * Проверяю значения, если summ не число, то выходим игнорируя
         */
        if (!is_numeric($item['summ'])) {
            return 0;
        }

        /*
         * Вырезаем потенциально опасные моменты из заметки
         */
        $item['note'] = strip_tags($item['note']);
        $item['note'] = htmlspecialchars($item['note']);

        /*
         * Ищу в массиве объектов, объект который пронумерован как в передаваемом $item
         */
        $cr_kpi = current(array_filter($this->KpiData, function ($k) use ($item) {
            return ($k->getNumber() == $item["num"]);
        }));

        /*
         * Если объект существует
         */
        if ($cr_kpi) {
            /*
            * Если сумма != передаваемой сумме ИЛИ заметка != передаваемой заметке, то обновляем значения
            */
            if ($cr_kpi->getSumm() != $item['summ'] || $cr_kpi->getNote() != $item['note']) {
                $query = "update member_kpi set active=0, date_e=STR_TO_DATE(?, '%d.%m.%Y') where id=?";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$date, $item['id']]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }

                if ($sth->rowCount() > 0) { // Если число строк > 0
                    $query = "insert into member_kpi (member_id,num,summ,note,date_s) VALUE(?,?,?,?,STR_TO_DATE(?, '%d.%m.%Y'))";
                    try {
                        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                        $sth->execute([$member_id, $item['num'], $item['summ'], $item['note'], $date]); // Выполняем запрос
                    } catch (PDOException $e) {
                        throw new Exception('Ошибка в PDO ' . $e->getMessage());
                    }

                    /*
                    * Выставляю измененные значения в найденный объект
                    */
                    $cr_kpi->setSumm($item['summ']);
                    $cr_kpi->setNote($item['note']);
                }

            }
            /*
             * Если объект не существует
             * и сумма в передаваемом значении не пустая, то добавляю строку
             */
        } else if ($item['summ'] != "") {
            $query = "insert into member_kpi (member_id, num, summ, note, date_s) VALUE(?, ?, ?, ?, STR_TO_DATE(?, '%d.%m.%Y'))";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$member_id, $item['num'], $item['summ'], $item['note'], $date]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO ' . $e->getMessage());
            }

            /*
             * Создаю новый объект и добавляю в массив данных
             */
            $KD = new KpiData();
            $KD->setId($item['id']);

            $KD->setNumber($item['num']);
            $KD->setSumm($item['summ']);
            $KD->setNote($item['note']);

            $KD->setChangerId($item['changer_id']);
            $KD->setChangerDate($item['change_date']);
            $KD->setDefault(false);
            array_push($this->KpiData, $KD);
        }
    }

    /*
     * Обновление значений в ведомости премии
     */
    public function updateWithDateToPayment($member_id, $item, $date, $type_of_payment, $changer_id)
    {
        /*
        * Проверяю подключение
        */
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

//        if (count($this->KpiData) == 0) {
        $this->fetch($member_id);
        $this->fetchPayment2($member_id, $date, $type_of_payment);
//        }

        /*
         * Проверяю значения, если summ не число, то выходим игнорируя
         */
        if (!is_numeric($item['summ'])) {
            return 0;
        }

        /*
         * Вырезаем потенциально опасные моменты из заметки
         */
        $item['note'] = strip_tags($item['note']);
        $item['note'] = htmlspecialchars($item['note']);

        /*
         * Ищу в массиве объектов, объект который пронумерован как в передаваемом $item
         */
        $cr_kpi = current(array_filter($this->KpiData, function ($k) use ($item) {
            return ($k->getNumber() == $item["num"]);
        }));

        /*
         * Если объект существует
         */
        if ($cr_kpi) {
            /*
            * Если сумма != передаваемой сумме ИЛИ заметка != передаваемой заметке, то обновляем значения
            */

            if ($cr_kpi->getSumm() != $item['summ'] || $cr_kpi->getNote() != $item['note']) {
                /*
                 * Обновляю активность и меняю дату изменения на текущюю, так же выставляю кто делал изменение данной записи.
                 * При условии что мы передаем дату, тип ведомости, номер позиции и строка должна быть активна
                 */
                $query = "update kpi_block set active=0, change_date=NOW(), changer_id=? where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and num=? and active=1 and member_id=?";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$changer_id, $date, $type_of_payment, $item['num'],$member_id]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }


                /*
                 * Если это было значение по-умолчанию, то просто добавляю строку
                 */
                $query = "insert into kpi_block (member_id, num, summ, note, date, type_of_payment, change_date, changer_id) VALUES(?, ?, ?, ?, STR_TO_DATE(?, '%m.%Y'), ?, NOW(), ?)";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$member_id, $item['num'], $item['summ'], $item['note'], $date, $type_of_payment, $changer_id]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }

                /*
                 * И меняю значения существующего объекта
                 */
                $cr_kpi->setDefault(false);
                $cr_kpi->setSumm($item['summ']);
                $cr_kpi->setNote($item['note']);
            }
        } else if ($item['summ'] != "") {
            /*
             * Если объект не был найден и сумма не пустота, то добавляю строку в БД
             */
            $query = "insert into kpi_block (member_id, num, summ, note, date, type_of_payment, change_date, changer_id) VALUES(?, ?, ?, ?, STR_TO_DATE(?, '%m.%Y'), ?, NOW(), ?)";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$member_id, $item['num'], $item['summ'], $item['note'], $date, $type_of_payment, $changer_id]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO ' . $e->getMessage());
            }

            /*
             * А так же добавляю объект в массив объектов
             */
            $KD = new KpiData();
            $KD->setId($item['id']);

            $KD->setNumber($item['num']);
            $KD->setSumm($item['summ']);
            $KD->setNote($item['note']);

            $KD->setDefault(false);

            array_push($this->KpiData, $KD);
        }
    }

    public function fetchAllMemberKPI() {
        /*
        * Проверяю подключение
        */
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        /*
         * Составляю SQL запрос
         */
        $query = "SELECT * FROM member_kpi where 
        active = 1
        order by member_id, num
        ";

        /*
         * Выполняю его
         */
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute(); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        $raw = $sth->fetchAll();

        return $raw;
    }

    public function fetchAllByDate($date) {
        /*
        * Проверяю подключение
        */
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $this->KpiData = [];
        /*
         * Составляю SQL запрос
         */
        $query = "SELECT * FROM kpi_block WHERE 
        date = STR_TO_DATE(?, '%m.%Y') AND 
        type_of_payment = 1 and
        active = 1
        order by member_id, num
        ";

        $AllData = array();

        /*
         * Выполняю его
         */
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        /*
         * Если количество строк больше чем 0, значит данные присутствуют в БД
         */
        if ($sth->rowCount() > 0) { // Если число строк > 0
            /*
             * Получаю все строки из БД
             */
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

            /*
             * Получаю данные из профиля
             */
            $MemberKPI = $this->fetchAllMemberKPI();
            $t1 = array();
            $t2 = array();
            foreach ($MemberKPI as $item) {
                $t1[$item['member_id']."_".$item['num']] = $item;
            }

            foreach ($raw as $item) {
                $t2[$item['member_id']."_".$item['num']] = $item;
            }

            $result = array_merge($t1, $t2);

            /*
             * Разбираю строки
             */
            foreach ($result as $item) {
                /*
                 * Иначе создаю новый объект и добавляю его в массив
                 */
                if (!isset($AllData[$item['member_id']])) {
                    $AllData[$item['member_id']] = array();
                }

                $KD = new KpiData();
                $KD->setId($item['id']);

                $KD->setNumber($item['num']);
                $KD->setSumm($item['summ']);
                $KD->setNote($item['note']);

                if (isset($item['changer_id'])) {
                    $KD->setChangerId($item['changer_id']);
                }
                if (isset($item['changer_date'])) {
                    $KD->setChangerDate($item['change_date']);
                }
                $KD->setDefault(false);

                array_push($this->KpiData, $KD);
                array_push($AllData[$item['member_id']], $KD);
            }
        };

        return $AllData;
    }
}