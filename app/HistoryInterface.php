<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 13.08.18
 * Time: 13:50
 */


require_once 'functions.php';
require_once 'MemberHistory.php';

require_once 'DepartmentInterface.php';
require_once 'StatgroupInterface.php';

class HistoryInterface
{
    private $dbh;
    public static $COLOR_DISABLED = "#6b6b6b";
    public static $COLOR_ENABLED = "black";

    public function getKPI($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "SELECT t1.*, CONCAT(t2.lastname, ' ', t2.name, ' ', t2.middle) as changer_name from member_kpi as t1
        LEFT JOIN member as t2 ON (t1.changer_id = t2.id)
        WHERE member_id=?";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getKPI класса HistoryInterface " . $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getAdvancedProcent($member_id, $member_motivation) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "SELECT t1.*, CONCAT(t2.lastname, ' ', t2.name, ' ', t2.middle) as changer_name FROM member_percent_advanced as t1
        LEFT JOIN member as t2 ON (t1.changer_id = t2.id)
        WHERE member_id=?";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getAdvancedProcent класса HistoryInterface" . $e->getMessage());
        }
        if ($sth->rowCount()>0) {
            $raws = $sth->fetchAll(PDO::FETCH_ASSOC);
            $Data = [];
            foreach ($raws as $raw)
            {
                if ($raw['date_e'] == '9999-01-01')
                    $raw['date_e'] = '';

                $datas = unserialize($raw['procents']);
                if (!$datas)
                    return [];

                switch ($raw['type'])
                {
                    case 1:
                        $SG = new StatgroupInterface();
                        try {
                            $SG->fetch();
                            $StatGroups = $SG->get();
                        } catch (Exception $e) {
                            throw new Exception("Ошибка получения статгрупп, в методе getAdvancedProcent класса HistoryInterface " . $e->getMessage());
                            die();
                        }

                    foreach ($datas as $statgroup_id=>$procent) {
                        $member_statgroup = current(array_filter($StatGroups, function ($s) use ($statgroup_id) {
                            return $s->getId() == $statgroup_id;
                        }));
                        $name = "Неизвестная статгруппа";
                        if (isset($member_statgroup))
                            $name = $member_statgroup->getName();

                        $Data[] = [
                            'type'         => $raw['type'],
                            'date_s'       => $raw['date_s'],
                            'date_e'       => $raw['date_e'],
                            'changer_id'   => $raw['changer_id'],
                            'changer_name' => $raw['changer_name'],
                            'change_date'  => $raw['change_date'],
                            'active'       => $raw['active'],
                            'name'         => $name,
                            'procent'      => floatval($procent),
                        ];
                    }

                        break;
                    case 2:
                        $DI = new DepartmentInterface();
                        try {
                            $DI->fetchDepartments();
                            $Departments = $DI->GetDepartments();
                        } catch (Exception $e) {
                            throw new Exception("Ошибка получения отделов, в методе getAdvancedProcent класса HistoryInterface " . $e->getMessage());
                            die();
                        }

                        foreach ($datas as $department_id=>$procent) {
                            $member_department = current(array_filter($Departments, function ($d) use ($department_id) {
                                return $d->getId() == $department_id;
                            }));
                            $name = "Неизвестная ТТ";
                            if (isset($member_department))
                                $name = $member_department->getNamesklad();

                            $Data[] = [
                                'type'         => $raw['type'],
                                'date_s'       => $raw['date_s'],
                                'date_e'       => $raw['date_e'],
                                'changer_id'   => $raw['changer_id'],
                                'changer_name' => $raw['changer_name'],
                                'change_date'  => $raw['change_date'],
                                'active'       => $raw['active'],
                                'name'         => $name,
                                'procent'      => floatval($procent),
                            ];
                        }
                        break;

                }
            }
            return $Data;
        } else {
            return [];
        }
    }

    public function getFio($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "select t1.*, t2.lastname as changer_last, t2.name as changer_name, t2.middle as changer_middle 
        FROM member_name_archive as t1
            LEFT JOIN member as t2 ON (t1.changer_id = t2.id)
        where t1.member_id=?";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getFio класса HistoryInterface " . $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getEmails($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "select t1.email, t1.note, t1.date, t1.changer_id, t2.name as changer_name, t2.lastname as changer_last, t2.middle as changer_middle 
        from member_mails as t1
        LEFT JOIN member as t2 ON (t1.changer_id = t2.id)
        where t1.member_id=?";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getEmails класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getNations($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "SELECT t1.*, t2.name, t3.name as changer_name, t3.lastname as changer_last, t3.middle as changer_middle
        FROM member_nation as t1 
        JOIN nation as t2 ON (t1.nation_id = t2.id)
        LEFT JOIN member as t3 ON (t1.changer_id = t3.id) 
        where t1.member_id=?";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getNations класса HistoryInterface ". $e->getMessage());
        }

        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getPersonalPhones($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "select t1.phone,t1.note,t1.is_mobile, t1.date, t1.changer_id, t1.active,
        t2.name as changer_name,t2.lastname as changer_last, t2.middle as changer_middle
        from member_personal_phones as t1
        LEFT JOIN member as t2 ON (t1.changer_id=t2.id)
        where t1.member_id=? and is_mobile = 0";

        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getPersonalPhones класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getPersonalMobilePhones($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "select t1.phone,t1.note,t1.is_mobile, t1.date, t1.changer_id, t1.active,
        t2.name as changer_name,t2.lastname as changer_last, t2.middle as changer_middle
        from member_personal_phones as t1
        LEFT JOIN member as t2 ON (t1.changer_id=t2.id)
        where t1.member_id=? and is_mobile != 0";

        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getPersonalMobilePhones класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getEmergencyPhones($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "select t1.phone, t1.note, t1.date, t1.changer_id, t1.active,
        t2.name as changer_name,t2.lastname as changer_last, t2.middle as changer_middle 
        from member_personal_emergency_phones as t1
        LEFT JOIN member as t2 ON (t1.changer_id = t2.id) 
        where t1.member_id=?";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getEmergencyPhones класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getSalary($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "SELECT t1.*, t2.name as changer_name, t2.lastname as changer_last, t2.middle as changer_middle
        FROM member_salary as t1 
        LEFT JOIN member as t2 ON (t1.changer_id = t2.id) 
        where t1.member_id=?";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getSalary класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getProcentAvans($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "SELECT t1.*, t2.name as changer_name, t2.lastname as changer_last, t2.middle as changer_middle
        FROM member_procent_avans as t1 
        LEFT JOIN member as t2 ON (t1.changer_id = t2.id) 
        where t1.member_id=?";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getProcentSalary класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getMemberAvans($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "SELECT t1.*, t2.name as changer_name, t2.lastname as changer_last, t2.middle as changer_middle
        FROM member_avans as t1 
        LEFT JOIN member as t2 ON (t1.changer_id = t2.id) 
        where t1.member_id=? ";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getMemberAvans класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getProcent($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "SELECT t1.*, t2.name as changer_name, t2.lastname as changer_last, t2.middle as changer_middle
        FROM member_percent as t1 
        LEFT JOIN member as t2 ON (t1.changer_id = t2.id) 
        where t1.member_id=? ";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getProcent класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getAdministrativePrize($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "SELECT t1.*, t2.name as changer_name, t2.lastname as changer_last, t2.middle as changer_middle
        FROM member_AdministrativePrize as t1 
        LEFT JOIN member as t2 ON (t1.changer_id = t2.id) 
        where t1.member_id=?";

        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getAdministrativePrize класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getMobileCommunication($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "SELECT t1.*, t2.name as changer_name, t2.lastname as changer_last, t2.middle as changer_middle
        FROM member_MobileCommunication as t1 
        LEFT JOIN member as t2 ON (t1.changer_id = t2.id) 
        where t1.member_id=?";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getMobileCommunication класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getTransport($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "SELECT t1.*, t2.name as changer_name, t2.lastname as changer_last, t2.middle as changer_middle
        FROM member_Transport as t1 
        LEFT JOIN member as t2 ON (t1.changer_id = t2.id) 
        where t1.member_id=?";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getMobileCommunication класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getYearBonus($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "SELECT t1.*, t2.name as changer_name, t2.lastname as changer_last, t2.middle as changer_middle
        FROM member_yearbonus_pay as t1 
        LEFT JOIN member as t2 ON (t1.changer_id = t2.id) 
        where t1.member_id=?";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getMobileCommunication класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getMotivation($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "SELECT t1.*, 
          t2.name as changer_name, t2.lastname as changer_last, t2.middle as changer_middle,
          t3.name as motivation_name
        FROM member_motivation as t1 
        LEFT JOIN member as t2 ON (t1.changer_id = t2.id) 
        LEFT JOIN motivation as t3 ON (t1.motivation_id = t3.id)
        where t1.member_id=?";

        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getMotivation класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getWorkMobilePhone($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "select t1.phone, t1.note, t1.is_mobile, t1.date, t1.changer_id, t1.active,
        t2.name as changer_name,t2.lastname as changer_last, t2.middle as changer_middle
        FROM member_work_phones as t1
        LEFT JOIN member as t2 ON (t1.changer_id=t2.id)
        WHERE t1.member_id=? and t1.is_mobile = 0";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getWorkMobilePhone класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getWorkPhone($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "select t1.phone, t1.note, t1.is_mobile, t1.date, t1.changer_id, t1.active,
        t2.name as changer_name,t2.lastname as changer_last, t2.middle as changer_middle
        FROM member_work_phones as t1
            LEFT JOIN member as t2 ON (t1.changer_id=t2.id)
        WHERE t1.member_id=? and t1.is_mobile != 0";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getWorkPhone класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getWorkLocalPhone($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "select phone, changer_id, t1.date, active  from member_work_local_phone where member_id=?";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getWorkLocalPhone класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    public function getWork($member_id) : array
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }
        $q = "select 
		t1.member_id,t5.name,t5.lastname,t5.middle,
        t1.direction_id, t2.name as direction_name, 
        t1.department_id,t3.name as department_name,
        t1.position_id,t4.name as position_name,
        t1.date_s,
        t1.date_e, 
        t1.changer_id,t6.name as changer_name,t6.lastname as changer_last,t6.middle as changer_middle, t1.active
        from member_jobpost as t1 
        LEFT JOIN direction as t2 ON(t1.direction_id = t2.id)
        LEFT JOIN department as t3 ON(t1.department_id = t3.id)
        LEFT JOIN position as t4 ON(t1.position_id = t4.id)
        RIGHT JOIN member as t5 ON(t1.member_id= t5.id)
        LEFT JOIN member as t6 ON (t1.changer_id = t6.id)
        where t1.member_id=? order by t1.id";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в методе getWork класса HistoryInterface ". $e->getMessage());
        }
        if ($sth->rowCount() > 0)
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }
}