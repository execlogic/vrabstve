<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 20.08.18
 * Time: 17:40
 */

require_once 'functions.php';

class AwhInterface
{
    private $dbh;
    private $Ndfl = array();
    private $AWHlock = 0;
    static $HOLIDAY_LAST_YEAR = 4;

    /**
     * @param int $AWHlock
     */
    public function setLock()
    {
        $this->AWHlock = 1;
    }

    /**
     * @return int
     */
    public function getLock()
    {
        return $this->AWHlock;
    }

    public function addAwh($array) {
        list($date, $absence, $holiday, $disease, $user_id, $type_of_payment) = $array;
//        echo "$date, $absence, $holiday, $disease, $user_id, $type_of_payment <BR>";

        $this->dbh=dbConnect();

        $query = "select t1.*,t2.lastname,t2.name,t2.middle from member_awh as t1 JOIN member as t2 ON (t1.member_id=t2.id) where date=STR_TO_DATE(?, '%Y-%m') AND member_id=? AND type_of_payment=?";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$date, $user_id,$type_of_payment])) {
            throw new Exception("Не могу выполнить зарос к БД addAwh");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
        if (count($raw)>0) {
//            echo "Данные по пользователю \"".$raw[0]['lastname']. " ".$raw[0]['name']."\" уже добавлены БД";
            throw new Exception("Данные по пользователю \"".$raw[0]['lastname']. " ".$raw[0]['name']."\" уже добавлены БД");
        }


        /*
         * Добавляю данные в AWH
         */
        $query = "INSERT INTO member_awh (date,absence,holiday,disease,member_id,type_of_payment) VALUES (STR_TO_DATE(?, '%Y-%m'), ?, ?, ?, ?, ?)";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$date, $absence, $holiday, $disease, $user_id, $type_of_payment])) {
            throw new Exception("Не могу выполнить зарос к БД addAwh");
        }

    }

    public function setAbsenceByMember($user_id, $date, $type_of_payment, $absence) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from member_awh where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and member_id=? LIMIT 1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        $sth->execute([$date,$type_of_payment,$user_id]);

        if ($sth->rowCount() == 1) {
            $query = "UPDATE member_awh SET absence=? where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and member_id=?";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$absence,$date,$type_of_payment,$user_id]);
        } else {
            $query = "INSERT INTO member_awh (date,type_of_payment, member_id, absence, disease, holiday) VALUES (STR_TO_DATE(?,'%m.%Y'),?,?,?,0,0)";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date,$type_of_payment,$user_id,$absence]);
        }
    }

    public function setNewAbsenceByMember($user_id, $date, $type_of_payment, $absence) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select new_absence from member_awh where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and member_id=? LIMIT 1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        $sth->execute([$date, $type_of_payment, $user_id]);

        if ($sth->rowCount() == 1) {
            $query = "UPDATE member_awh SET new_absence=? where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and member_id=?";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$absence, $date, $type_of_payment, $user_id]);
        } else {
            $query = "INSERT INTO member_awh (date, type_of_payment, member_id, new_absence, absence, disease, holiday) VALUES (STR_TO_DATE(?,'%m.%Y'),?,?,?,0,0,0)";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date, $type_of_payment, $user_id, $absence]);
        }
    }

    public function setHolidayByMember($user_id, $date, $type_of_payment, $holiday) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from member_awh where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and member_id=? LIMIT 1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        $sth->execute([$date, $type_of_payment, $user_id]);

        if ($sth->rowCount() == 1) {
            $query = "UPDATE member_awh SET holiday=? where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and member_id=?";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$holiday, $date, $type_of_payment, $user_id]);
        } else {
            $query = "INSERT INTO member_awh (date, type_of_payment, member_id, holiday, absence, disease) VALUES (STR_TO_DATE(?,'%m.%Y'),?,?,?,0,0)";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date, $type_of_payment, $user_id, $holiday]);
        }
    }

    public function setDiseaseByMember($user_id, $date, $type_of_payment, $disease) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from member_awh where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and member_id=? LIMIT 1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        $sth->execute([$date, $type_of_payment, $user_id]);

        if ($sth->rowCount() == 1) {

            $query = "UPDATE member_awh SET disease=? where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and member_id=?";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$disease, $date, $type_of_payment, $user_id]);
        } else {
            $query = "INSERT INTO member_awh (date,type_of_payment, member_id, disease, holiday, absence) VALUES (STR_TO_DATE(?,'%m.%Y'),?,?,?,0,0)";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date, $type_of_payment, $user_id, $disease]);
        }
    }

    public function getAwh($date, $type_of_payment) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }


        /*
         * Добавляю данные в AWH
         */
//        $query = "SELECT t1.*,t2.lastname,t2.name,t2.middle FROM zp.member_awh as t1 JOIN member as t2 ON (t1.member_id=t2.id) where t1.date = STR_TO_DATE(?, '%Y-%m') AND t1.type_of_payment=?;";
        $query = "SELECT t1.*,t2.lastname,t2.name,t2.middle,t2.employment_date,t3.department_id, t4.name as department_name
        FROM zp.member_awh as t1 
        JOIN member as t2 ON (t1.member_id=t2.id)
        JOIN member_jobpost as t3 ON (t2.id=t3.member_id AND t3.active=1) 
        JOIN department as t4 ON (t3.department_id = t4.id)
        where t1.date = STR_TO_DATE(?, '%Y-%m') AND t1.type_of_payment=?";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$date,$type_of_payment])) {
            throw new Exception("Не могу выполнить зарос к БД getAwh");
        }

        $rawResult = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $rawResult;
    }

    public function getAwhJournal($date, $type_of_payment, $departmnet_id) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        /*
         * Добавляю данные в AWH
         */
        $query = "SELECT t1.*,t2.lastname,t2.name,t2.middle,t2.employment_date,t3.department_id, t4.name as department_name
        FROM zp.member_awh as t1 
        JOIN member as t2 ON (t1.member_id=t2.id)
        JOIN member_jobpost as t3 ON (t2.id=t3.member_id AND t3.active=1) 
        JOIN department as t4 ON (t3.department_id = t4.id)
        where t1.date = STR_TO_DATE(?, '%Y-%m') AND t1.type_of_payment=?";

        if ($departmnet_id != 0 ) {
            $query .= " AND t3.department_id=?";
        }

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if ($departmnet_id != 0) {
            if (!$sth->execute([$date, $type_of_payment, $departmnet_id])) {
                throw new Exception("Не могу выполнить зарос к БД getAwhJournal");
            }
        } else {
            if (!$sth->execute([$date, $type_of_payment])) {
                throw new Exception("Не могу выполнить зарос к БД getAwhJournal");
            }
        }

        $rawResult = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $rawResult;
    }

    public function getAwhAndNDFLJournal($date, $type_of_payment, $departmnet_id) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        /*
         * Добавляю данные в AWH
         */
        switch ($type_of_payment) {
            case 1:
            case 3:
                $query = "SELECT t1.*,t2.lastname,t2.name,t2.middle,t2.employment_date,t3.department_id, t4.name as department_name, t5.ndfl, t5.ro, t6.status as workstatus
            FROM zp.member_awh as t1 
            JOIN member as t2 ON (t1.member_id=t2.id)
            JOIN member_jobpost as t3 ON (t2.id=t3.member_id AND t3.active=1) 
            JOIN department as t4 ON (t3.department_id = t4.id)
            JOIN member_ndfl as t5 on (t1.member_id = t5.member_id and t1.date = t5.date)
            LEFT JOIN member_workstatus as t6 ON (t1.member_id = t6.member_id and t6.active=1) 
            where t1.date = STR_TO_DATE(?, '%Y-%m') AND t1.type_of_payment=? and (t6.status=1 or t6.status is NULL)";
                break;
            case 2:
                $query = "SELECT t1.*,t2.lastname,t2.name,t2.middle,t2.employment_date,t3.department_id, t4.name as department_name, t6.status as workstatus
            FROM zp.member_awh as t1 
            JOIN member as t2 ON (t1.member_id=t2.id)
            JOIN member_jobpost as t3 ON (t2.id=t3.member_id AND t3.active=1) 
            JOIN department as t4 ON (t3.department_id = t4.id)
            LEFT JOIN member_workstatus as t6 ON (t1.member_id = t6.member_id and t6.active=1) 
            where t1.date = STR_TO_DATE(?, '%Y-%m') AND t1.type_of_payment=? and (t6.status=1 or t6.status is NULL)";
                break;
        }

        if ($departmnet_id != 0 ) {
            $query .= " AND t3.department_id=?";
        }

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if ($departmnet_id != 0) {
            if (!$sth->execute([$date, $type_of_payment, $departmnet_id])) {
                throw new Exception("Не могу выполнить зарос к БД getAwhJournal");
            }
        } else {
            if (!$sth->execute([$date, $type_of_payment])) {
                throw new Exception("Не могу выполнить зарос к БД getAwhJournal");
            }
        }

        $rawResult = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $rawResult;
    }

    public function getAwhByDate($date) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }


        /*
         * Добавляю данные в AWH
         */
        $query = "SELECT t1.*,t2.lastname,t2.name,t2.middle FROM zp.member_awh as t1 JOIN member as t2 ON (t1.member_id=t2.id) where t1.date = STR_TO_DATE(?, '%m.%Y');";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$date])) {
            throw new Exception("Не могу выполнить зарос к БД getAwhByDate");
        }

        $rawResult = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $rawResult;
    }

    public function getAwhByMemberYear($member_id,$date) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        /*
         * Добавляю данные в AWH
         */
        $query = "SELECT t1.* FROM zp.member_awh as t1 where t1.date > STR_TO_DATE(?, '%Y') and t1.date < STR_TO_DATE(?, '%Y') and member_id=? and type_of_payment=1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$date, ($date+1),$member_id])) {
            throw new Exception("Не могу выполнить зарос к БД getAwhByMemberYear");
        }

        $rawResult = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $rawResult;
    }

    public function getNdflByMemberYear($member_id,$date) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        /*
         * Добавляю данные в AWH
         */
        $query = "SELECT t1.* FROM zp.member_ndfl as t1 where t1.date > STR_TO_DATE(?, '%Y') and t1.date < STR_TO_DATE(?, '%Y') and member_id=?";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$date, ($date+1),$member_id])) {
            throw new Exception("Не могу выполнить зарос к БД getNdflByMemberYear");
        }

        $rawResult = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $rawResult;
    }

    public function fetchFromPayout($id ,$date, $type_of_payment) {

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        // Получаю бонус по проведенным акциям
        try {
            $query = "select AwhData from financial_payout_ext where member_id=? and type_of_payment=? and date=STR_TO_DATE(?,\"%m.%Y\") LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id, $date,$type_of_payment]);

            $raw = current($sth->fetchAll(PDO::FETCH_ASSOC));
            if ($raw) {
                return unserialize($raw["AwhData"]);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД fetchFromPayout " . $sth->errorInfo()[2]);
        }
    }

    public function getAwhByMember($date, $type_of_payment, $member_id) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        if ($type_of_payment == 4 || $type_of_payment == 3)
            $type_of_payment = 1;
        /*
         * Добавляю данные в AWH
         */
        $query = "SELECT t1.new_absence, t1.absence, t1.holiday, t1.disease, t1.note FROM zp.member_awh as t1 where t1.date = STR_TO_DATE(?, '%m.%Y') AND t1.type_of_payment=? AND member_id=?;";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$date,$type_of_payment,$member_id])) {
            throw new Exception("Не могу выполнить зарос к БД getAwhByMember");
        }

        $rawResult = $sth->fetch(PDO::FETCH_ASSOC);
        return $rawResult;
    }

    // Получение NDFL (скорее всего не будет использоваться)
    public function fetch_NDFL()
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT date,ndfl,ro FROM member_ndfl where member_id=? order by date LIMIT 20";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$this->Member->getId()])) {
            throw new Exception("Не могу выполнить зарос к БД fetch_NDFL");
        }

        $this->Ndfl = $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    // Получение NDFL по заданной дате (копия)
    public function fetch_NdflByDate($date, $user_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT date,ndfl,ro FROM member_ndfl where member_id=? and date=STR_TO_DATE(?, '%m.%Y') LIMIT 1";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$user_id, $date])) {
            throw new Exception("Не могу выполнить зарос к БД fetch_NdflByDate");
        }

        $this->Ndfl = $sth->fetch(PDO::FETCH_ASSOC);
    }

    public function getNdfl()
    {
        return $this->Ndfl;
    }

    public function setNDFLByMember($user_id, $date, $ndfl) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from member_ndfl where date=STR_TO_DATE(?,'%m.%Y') and member_id=? LIMIT 1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        $sth->execute([$date, $user_id]);

        if ($sth->rowCount() == 1) {
            $query = "UPDATE member_ndfl SET ndfl=? where date=STR_TO_DATE(?,'%m.%Y') and member_id=?";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$ndfl, $date, $user_id]);
        } else {
            $query = "INSERT INTO member_ndfl (date, member_id, ndfl, ro) VALUES (STR_TO_DATE(?,'%m.%Y'),?,?,0)";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date,  $user_id, $ndfl]);
        }
    }

    public function setROByMember($user_id, $date, $ro) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from member_ndfl where date=STR_TO_DATE(?,'%m.%Y') and member_id=? LIMIT 1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        $sth->execute([$date, $user_id]);

        if ($sth->rowCount() == 1) {

            $query = "UPDATE member_ndfl SET ro=? where date=STR_TO_DATE(?,'%m.%Y') and member_id=?";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$ro, $date, $user_id]);
        } else {
            $query = "INSERT INTO member_ndfl (date, member_id, ro, ndfl) VALUES (STR_TO_DATE(?,'%m.%Y'),?,?,0)";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date,  $user_id, $ro]);
        }
    }

    public function setNoteByMember($user_id, $date, $type_of_payment, $note) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from member_awh where date=STR_TO_DATE(?,'%m.%Y') and member_id=? and type_of_payment=? LIMIT 1";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        $sth->execute([$date, $user_id, $type_of_payment]);

        if ($sth->rowCount() == 1) {
            $query = "UPDATE member_awh SET note=? where date=STR_TO_DATE(?,'%m.%Y') and member_id=? and type_of_payment=?";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$note, $date, $user_id, $type_of_payment]);
        } else {
            $query = "INSERT INTO member_awh (date, member_id, type_of_payment, absence, holiday, disease, note) VALUES (STR_TO_DATE(?,'%m.%Y'),?,?,0,0,0,?)";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date,  $user_id, $type_of_payment, $note]);
        }
    }

    public function getAwhNewMember($d1,$d2) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select t1.*,t3.name as department_name from member as t1
        LEFT JOIN member_jobpost as t2 ON (t1.id = t2.member_id and t2.active = 1)
        LEFT JOIN department as t3 ON (t2.department_id = t3.id)
        where employment_date>=STR_TO_DATE(?,'%Y-%m-%d') and employment_date<STR_TO_DATE(?,'%Y-%m-%d')";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        $sth->execute([$d1, $d2]);
        if ($sth->rowCount() > 0) {
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
            return $raw;
        } else {
            return NULL;
        }
    }

    public function setExtraHoliday($date, $member_id, $holiday, $changer_id) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        try {
            $q = "select * from member_awh_extraholiday where is_active = 1 and date=STR_TO_DATE(?,'%m.%Y') and member_id = ? LIMIT 1";
            $sth = $this->dbh->prepare($q);
            $sth->execute([$date, $member_id]);
            if ($sth->rowCount() > 0) {
                $raw = $sth->fetch(PDO::FETCH_ASSOC);
                if ($holiday == $raw['holiday']) {
                    return true;
                }

                $q = "UPDATE member_awh_extraholiday SET is_active = 0, changer_id=?, change_date=NOW() where is_active = 1 and date=STR_TO_DATE(?,'%m.%Y') and member_id = ?";
                $sth = $this->dbh->prepare($q);
                $sth->execute([$changer_id, $date, $member_id]);
            }
            $q = "INSERT INTO member_awh_extraholiday (date, member_id, holiday, changer_id, change_date) VALUES (STR_TO_DATE(?,'%m.%Y'), ?, ?, ?, NOW())";
            $sth = $this->dbh->prepare($q);
            $sth->execute([$date, $member_id, $holiday, $changer_id]);
        } catch (Exception $e) {
            echo $e->getMessage()."<BR>";
        }

        return true;
    }

    public function getExtraHoliday($date, $member_id) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        $q = "select * from member_awh_extraholiday where is_active = 1 and date=STR_TO_DATE(?,'%m.%Y') and member_id = ? LIMIT 1";
        $sth = $this->dbh->prepare($q);
        $sth->execute([$date, $member_id]);
        if ($sth->rowCount()>0) {
            $raw = $sth->fetch(PDO::FETCH_ASSOC);
            return $raw['holiday'];
        }

        return false;
    }

    /*
     * Остаток отпуска на начало года
     */
    public function getVacationBalance($member_id, $year) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        $q = "select * from vacation_balance where date='".$year."-01-01' and member_id = ? LIMIT 1";
        $sth = $this->dbh->prepare($q);
        $sth->execute([$member_id]);

        if ($sth->rowCount()>0) {
            $raw = $sth->fetch(PDO::FETCH_ASSOC);
            return $raw['holiday'];
        }

        return false;
    }
}