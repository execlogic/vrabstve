<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 13.08.18
 * Time: 15:43
 */

require_once 'Nation.php';
require_once 'functions.php';

class NationInterface {
    private $nationList = array();
    private $dbh;

    public function fetchNations() {
        $this->dbh=dbConnect();
        $query = "select * from nation order by id";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute();
        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $this->dbh = null;

        foreach ($rawresult as $value) {
            $Nation = new Nation();
            $Nation->setId($value['id']);
            $Nation->setName($value['name']);
            array_push($this->nationList,$Nation);
        }
    }

    public function GetNations() {
        if (empty($this->nationList)) {
            throw new Exception("Пустой список отделов");
        }
        return $this->nationList;
    }

    /**
     * @return bool
     * @throws Exception
     * Отображает Отделы
     */
    public function ShowNations() {
        if (empty($this->nationList)) {
            throw new Exception("Пустой список наций");
            return false;
        }

        echo "<table class='table table-bordered'>";
        foreach ($this->nationList as $value ){
            echo "<tr>";
            echo "<td>".$value->getId()."</td><td>".$value->getName()."</td>";
            echo "</tr>";
        }
        echo "</table>";

        return true;
    }

    public function AddNations($name) {
        $this->dbh=dbConnect();

        if (empty($this->dbh)) {
            echo "Error<br>";
        }

        $query = "INSERT INTO nation (name) VALUES (?)";
        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare'.$exception->getMessage());
        }

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$name])) {
            $error = $sth->errorInfo();
            throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: '.$error[1].'.<br> Текст ошибки: '.$error[2]."<BR>");
        }

        $sth = null;
        $this->dbh = null;

        return true;
    }

}