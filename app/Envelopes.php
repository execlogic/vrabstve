<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 19.10.18
 * Time: 17:56
 */

class Envelopes
{
    private $name;
    private $member_id;
    private $envelope_id;
    private $summ;
    private $old_summ = 0;
    private $status = 0;
    private $modify = 0;

    /**
     * @param int $modify
     */
    public function setModify($modify)
    {
        $this->modify = $modify;
    }


    /**
     * @return int
     */
    public function getModify()
    {
        return $this->modify;
    }

    /**
     * @param mixed $old_summ
     */
    public function setOldSumm()
    {
        $this->old_summ = $this->summ;
    }

    /**
     * @return mixed
     */
    public function getOldSumm()
    {
        return $this->old_summ;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getMemberId()
    {
        return $this->member_id;
    }

    /**
     * @param mixed $member_id
     */
    public function setMemberId($member_id)
    {
        $this->member_id = $member_id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $envelope_id
     */
    public function setEnvelopeId($envelope_id)
    {
        $this->envelope_id = $envelope_id;
    }

    /**
     * @param mixed $summ
     */
    public function setSumm($summ)
    {
        $this->summ = $summ;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getEnvelopeId()
    {
        return $this->envelope_id;
    }

    /**
     * @return mixed
     */
    public function getSumm()
    {
        return $this->summ;
    }
}