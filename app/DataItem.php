<?php
/***
 * Class DataItem
 * Данный класс описывает ТТ, Статгруппы и Службу сервиса
 */

require_once 'app/functions.php';

class DataItem {
    protected $date;
    protected $name;
    protected $id;
    protected $sale;
    protected $const_price;
    protected $profit;
    protected $profit_procent;

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @param mixed $sale
     */
    public function setSale($sale)
    {
        $this->sale = $sale;
    }

    /**
     * @param mixed $profit_procent
     */
    public function setProfitProcent($profit_procent)
    {
        $this->profit_procent = $profit_procent;
    }

    /**
     * @param mixed $profit
     */
    public function setProfit($profit)
    {
        $this->profit = $profit;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $ss
     */
    public function setConstprice($const_price)
    {
        $this->const_price = $const_price;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getProfitProcent()
    {
        return $this->profit_procent;
    }

    /**
     * @return mixed
     */
    public function getProfit()
    {
        return $this->profit;
    }

    /**
     * @return mixed
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getConstprice()
    {
        return $this->const_price;
    }
}