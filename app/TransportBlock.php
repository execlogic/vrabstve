<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 04.03.20
 * Time: 13:16
 */

require_once 'app/functions.php';

class TransportBlock
{
    private $dbh;
    /***
     * @var type_of_payment
     * Тип выплат
     * 1 - премия
     * 2 - аванс
     * 3 - аванс2
     * 4- расчет
     */
    private $member_id=null;
    private $date=null;
    private $type_of_payment = null;
    private $summ = null;
    private $note = null;
    private $changer_id = null;
    private $change_date = null;


    private function migration() {
        $q = "insert into transport_block (member_id,date,type_of_payment,summ,note,changer_id,change_date) 
              (SELECT member_id,date,type_of_payment,summ,note,changer_id,change_date FROM zp.administrative_block where type=10)";
    }

    public function getTransport($date, $type_of_payment, $member_id){
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $this->setMemberId($member_id);
        $this->setDate($date);
        $this->setTypeOfPayment($type_of_payment);
        $this->setSumm(0);
        $this->setNote(NULL);
        $this->setChangerId(NULL);
        $this->setChangeDate(NULL);

//        $query = "SELECT member_id, summ,formula FROM zp.member_Transport where member_id=? and active=1 LIMIT 1";
//        $sth = $this->dbh->prepare($query);
//        $sth->execute([$member_id]);
//        $raw = $sth->fetch(PDO::FETCH_ASSOC);
//        if ($sth->rowCount() > 0) {
//            $this->setSumm($raw['summ']);
//        }

        $q = "select * from transport_block where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and member_id=? and is_active=1 LIMIT 1";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$date, $type_of_payment, $member_id]);

            if ($sth->rowCount()>0) {
                $raw = $sth->fetch(PDO::FETCH_ASSOC);
                $this->setSumm($raw['summ']);
                $this->setNote($raw['note']);
                $this->setChangerId($raw['changer_id']);
                $this->setChangeDate($raw['change_date']);
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }

        return true;
    }

    public function setTransport($date, $type_of_payment, $member_id, $summ, $note, $changer_id, $is_auto=1) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        $dateNow = new DateTime();

        $q = "select * from transport_block where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and member_id = ? and is_active=1 LIMIT 1";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$date, $type_of_payment, $member_id]);

            if ($sth->rowCount()>0) {
                $raw = $sth->fetch(PDO::FETCH_ASSOC);
                if ($raw['summ'] == $summ) {
                    return true;
                }
                $q = "UPDATE transport_block SET is_active=0, change_date=NOW(), changer_id=?, is_auto=? where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and member_id=? and is_active=1";
                $sth = $this->dbh->prepare($q);
                $sth->execute([$changer_id, $is_auto, $date, $type_of_payment, $member_id]);

            };

            $q = "INSERT INTO transport_block (member_id, date, type_of_payment, summ, note, changer_id, change_date, is_auto) VALUE (?, STR_TO_DATE(?,\"%m.%Y\"), ?, ?, ?, ?, NOW(),?)";
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id, $date, $type_of_payment, $summ, $note, $changer_id, $is_auto]);

        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД Transport" . $sth->errorInfo()[2]);
        }

        if ($dateNow->format("m") == 12 && $type_of_payment == 3) {
            $q = "select * from transport_block where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and member_id = ? and is_active=1 LIMIT 1";
            $sth = $this->dbh->prepare($q);
            $sth->execute([$date, 1, $member_id]);
            if ($sth->rowCount()>0) {
                $q = "UPDATE transport_block SET is_active=0, change_date=NOW(), changer_id=?, is_auto=? where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and member_id=? and is_active=1";
                $sth = $this->dbh->prepare($q);
                $sth->execute([$changer_id, $is_auto, $date, 1, $member_id]);
            }
            $q = "INSERT INTO transport_block (member_id, date, type_of_payment, summ, note, changer_id, change_date, is_auto) VALUE (?, STR_TO_DATE(?,\"%m.%Y\"), ?, ?, ?, ?, NOW(),?)";
            $sth = $this->dbh->prepare($q);
            $sth->execute([$member_id, $date, 1, $summ, $note, $changer_id, $is_auto]);
        }
        $this->summ = $summ;

        return true;
    }

    public function recountCompensationFAL($otsutstviya, $work_days, $changer_id=-1, $is_auto=1) {
        $this->summ = round(($this->summ / $work_days * ($work_days-$otsutstviya)),2);
        $this->setTransport($this->date, $this->type_of_payment, $this->member_id, $this->summ, $this->note, $changer_id, $is_auto);
    }

    public function fetchAllByDate($date, $type_of_payment) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        $AllData=array();
        $q = "select * from transport_block where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and is_active=1";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$date, $type_of_payment]);
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
            foreach ($raw as $item) {
                $AllData[$item['member_id']] = $item;
            }
            return $AllData;
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }

        return false;
    }

    public function isAuto($date, $type_of_payment, $member_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $q = "select * from transport_block where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=? and member_id = ? and is_active=1 and is_auto=1 LIMIT 1";
        try {
            $sth = $this->dbh->prepare($q);
            $sth->execute([$date, $type_of_payment, $member_id]);
            if ($sth->rowCount()>0) {
                return true;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД Transport" . $sth->errorInfo()[2]);
        }

        return false;
    }


    /**
     * @return null
     */
    public function getChangerId()
    {
        return $this->changer_id;
    }

    /**
     * @return null
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @return null
     */
    public function getSumm()
    {
        return $this->summ;
    }

    /**
     * @return null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return null
     */
    public function getChangeDate()
    {
        return $this->change_date;
    }

    /**
     * @return type_of_payment
     */
    public function getMemberId()
    {
        return $this->member_id;
    }

    /**
     * @return null
     */
    public function getTypeOfPayment()
    {
        return $this->type_of_payment;
    }

    /**
     * @param null $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @param null $type_of_payment
     */
    public function setTypeOfPayment($type_of_payment)
    {
        $this->type_of_payment = $type_of_payment;
    }

    /**
     * @param type_of_payment $member_id
     */
    public function setMemberId($member_id)
    {
        $this->member_id = $member_id;
    }

    /**
     * @param null $changer_id
     */
    public function setChangerId($changer_id)
    {
        $this->changer_id = $changer_id;
    }

    /**
     * @param null $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @param null $summ
     */
    public function setSumm($summ)
    {
        $this->summ = $summ;
    }

    /**
     * @param null $change_date
     */
    public function setChangeDate($change_date)
    {
        $this->change_date = $change_date;
    }

}