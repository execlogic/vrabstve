<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 14.09.18
 * Time: 10:14
 */
require_once 'functions.php';

class CustomPercent
{
    private $dbh;
    private $Percent = array(); // Ассоциативный Массив с кастомными процентами.
    private $AdvancedPercent_id = NULL;
    private $hash = NULL; // Хеш ассоциативного массива
    private $baseprercent;
    private $basepercent_id = NULL;

    private $brand_prercent;
    private $brand_prercent_id = NULL;

    private $motivation;

    public function fetchBasePercent($member_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        $query_subquery = "select id, percent from member_percent where member_id=? and active=1";
        try {
            $sth = $this->dbh->prepare($query_subquery);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в получении причины увольнения." . $e->getMessage());
        }
        $raw = $sth->fetch(PDO::FETCH_ASSOC);
        if ($raw) {
            $this->baseprercent = $raw['percent'];
            $this->basepercent_id = $raw['id'];
        }
    }

    public function fetchBrendPercent($member_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query_subquery = "select id, percent from member_brend_percent where member_id=? and active=1";
        try {
            $sth = $this->dbh->prepare($query_subquery);
            $sth->execute([$member_id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в получении причины увольнения." . $e->getMessage());
        }
        $raw = $sth->fetch(PDO::FETCH_ASSOC);
        if ($raw) {
            $this->brand_prercent = $raw['percent'];
            $this->brand_prercent_id = $raw['id'];
        }
    }

    public function fetchBasePercentbyId($id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        $query_subquery = "select id, percent from member_percent where id=?";
        try {
            $sth = $this->dbh->prepare($query_subquery);
            $sth->execute([$id]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в получении причины увольнения." . $e->getMessage());
        }
        $raw = $sth->fetch(PDO::FETCH_ASSOC);
        if ($raw) {
            $this->baseprercent = $raw['percent'];
            $this->basepercent_id = $raw['id'];
        }
    }

    public function fetchBasePercentByDate($member_id,$date)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $date = "01.".$date;

        $query_subquery = "select percent from member_percent WHERE 
        member_id=? AND 
        (date_s BETWEEN '0000-00-00' AND DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'), INTERVAL 1 MONTH) ) and (date_e BETWEEN DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'),INTERVAL 1 MONTH ) AND '9999-01-01') 
        order by id desc LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query_subquery);
            $sth->execute([$member_id, $date, $date]);
        } catch (Exception $e) {
            throw new Exception("Ошибка в получении причины увольнения." . $e->getMessage());
        }

        $raw = $sth->fetch(PDO::FETCH_ASSOC);
        if ($raw) {
            $this->baseprercent = $raw['percent'];
        }
    }

    /**
     * @param mixed $brand_prercent
     */
    public function setBrandPrercent($brand_prercent)
    {
        $this->brand_prercent = $brand_prercent;
    }

    /**
     * @return mixed
     */
    public function getBrandPrercent()
    {
        return $this->brand_prercent;
    }

    public function setBasePercent($basepercent)
    {
        $this->baseprercent = $basepercent;
    }

    /**
     * @return mixed
     */
    public function getBasepercentId()
    {
        return $this->basepercent_id;
    }

    public function getBasePercent()
    {
        return $this->baseprercent;
    }

    public function fetchMotivation($member_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select motivation_id from member_motivation where member_id=? and active=1 LIMIT 1";
        $sth = $this->dbh->prepare($query);
        $sth->execute([$member_id]);

        $this->motivation = $sth->fetch()['motivation_id'];
        if (is_null($this->motivation) && !$this->motivation)
            throw new Exception("Не могу получить мотивацию пользователя");

//        if (($this->motivation == 1 ) || ($this->motivation == 4 ) || ($this->motivation == 5) || ($this->motivation == 7)) {
//            $this->motivation = $sth->fetch()['motivation_id'];
//        } else {
//            throw new Exception("Не могу получить мотивацию пользователя");
//        }
    }

    public function setMotivation($motivation)
    {
        $this->motivation = $motivation;
    }




    public function fetch($member_id)
    {
        if (!is_numeric($member_id)) {
            throw new Exception("Неверный id пользователя");
        }
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if (!isset($this->motivation)) {
            $this->fetchMotivation($member_id);
        }

        if (($this->motivation == 1) || ($this->motivation == 4) || ($this->motivation == 5) || ($this->motivation == 7)) {
            $query = "select * from member_cp_statgroup where member_id=? and active=1 order by statgroup_id";
        } else if (($this->motivation == 2) || ($this->motivation == 3)) {
            $query = "select * from member_cp_department where member_id=? and active=1 order by department_id";
        } else {
            throw new Exception("Данный пользователеь имеет мотивацию без кастомных процентов");
        }

        $sth = $this->dbh->prepare($query);

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$member_id]);
        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $this->dbh = null;

        foreach ($rawresult as $value) {
            if (($this->motivation == 1 ) || ($this->motivation == 4 )  || ($this->motivation == 5) || ($this->motivation == 7)) {
                $this->Percent[$value['statgroup_id']] = $value['percent'];
            } else if (($this->motivation == 2) || ($this->motivation == 3)) {
                $this->Percent[$value['department_id']] = $value['percent'];
            }

        }
    }

    public function fetchv2($member_id, $date = null)
    {
//        $date = "01.".$date;
        if (($this->motivation == 1) || ($this->motivation == 2)  || ($this->motivation == 3)  || ($this->motivation == 4) || ($this->motivation == 5) || ($this->motivation == 7)) {
            if (!is_numeric($member_id)) {
                throw new Exception("Неверный id пользователя");
            }
            if ($this->dbh == null) {
                $this->dbh = dbConnect();
            }

            if (!isset($this->motivation)) {
                $this->fetchMotivation($member_id);
            }

            if (($this->motivation == 1) || ($this->motivation == 4) || ($this->motivation == 5) || ($this->motivation == 7) || $this->motivation == 8) {
                $query = "select * from member_cp_statgroup where 
                member_id=? and active = 1
                order by statgroup_id";
            } else if (($this->motivation == 2) || ($this->motivation == 3)) {
                $query = "select * from member_cp_department where 
                member_id=? and active = 1 
                order by department_id";
            } else {
                throw new Exception("Данный пользователеь имеет мотивацию без кастомных процентов");
            }

            $sth = $this->dbh->prepare($query);

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

//            $sth->execute([$member_id, $date, $date]);
            $sth->execute([$member_id]);
            $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

            $sth = null;
            $this->dbh = null;

            foreach ($rawresult as $value) {
                if (($this->motivation == 1) || ($this->motivation == 4) || ($this->motivation == 5) || ($this->motivation == 7) || $this->motivation == 8) {
                    $this->Percent[$value['statgroup_id']] = $value['percent'];
                } else if (($this->motivation == 2) || ($this->motivation == 3)) {
                    $this->Percent[$value['department_id']] = $value['percent'];
                }
            }
        }
    }


    public function fetchById($id) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if (!is_numeric($id)){
            return false;
        }

        $query = "select id, procents, hash from member_percent_advanced where id=? LIMIT 1";
        $sth = $this->dbh->prepare($query);

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };
        $sth->execute([$id]);
        if ($sth->rowCount() > 0) {
            $rawresult = $sth->fetch(PDO::FETCH_ASSOC);

            $this->hash = $rawresult['hash'];
            $this->AdvancedPercent_id = $rawresult['id'];

            $sth = null;
            $this->dbh = null;

            foreach (unserialize($rawresult['procents']) as $key => $value) {
                $this->Percent[$key] = $value;
            }

            return true;
        } else {
            return false;
        }
    }

    /*
     * Извлечение из новой таблицы с хешами и типом данных
     */
    public function fetchv3($member_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if (!isset($this->motivation)) {
            $this->fetchMotivation($member_id);
        }
        if (($this->motivation != 6) ) {
            if (!is_numeric($member_id)) {
                throw new Exception("Неверный id пользователя");
            }

            /*
         * Тип
         * 1 - статгурппы
         * 2 - направления
         */
            $type = 1;

            if (($this->motivation == 1 ) || ($this->motivation == 4 ) || ($this->motivation == 5) || ($this->motivation == 7) || ($this->motivation == 8) || ($this->motivation == 9)) {
                $type = 1;
            } else if (($this->motivation == 2) || ($this->motivation == 3)) {
                $type = 2;
            } else if ($this->motivation == 10) {
                return true;
            } else {
                throw new Exception("Ошибка мотивации fetchv3<br>");
            }

            $query = "select id, procents, hash from member_percent_advanced where member_id=? and type=? and active=1 LIMIT 1";
            $sth = $this->dbh->prepare($query);

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            $sth->execute([$member_id, $type]);
//            echo sprintf("select id, procents, hash from member_percent_advanced where member_id=%s and type=%s and active=1 LIMIT 1", $member_id, $type)."<BR>";
            if ($sth->rowCount() > 0) {
                $rawresult = $sth->fetch(PDO::FETCH_ASSOC);

                $this->hash = $rawresult['hash'];
                $this->AdvancedPercent_id = $rawresult['id'];

                $sth = null;
                $this->dbh = null;

                foreach (unserialize($rawresult['procents']) as $key => $value) {
                    $this->Percent[$key] = $value;
                }

                return true;
            } else {
                return false;
            }
        }
    }

    public function fetchv3byDate($member_id, $date)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if (!isset($this->motivation)) {
            $this->fetchMotivation($member_id);
        }
        if (($this->motivation != 6) ) {
            if (!is_numeric($member_id)) {
                throw new Exception("Неверный id пользователя");
            }

            /*
         * Тип
         * 1 - статгурппы
         * 2 - направления
         */
            $type = 1;

            if (($this->motivation == 1 ) || ($this->motivation == 4 ) || ($this->motivation == 5) || ($this->motivation == 7) || ($this->motivation == 8) || ($this->motivation == 9)) {
                $type = 1;
            } else if (($this->motivation == 2) || ($this->motivation == 3)) {
                $type = 2;
            } else if ($this->motivation == 10) {
                return true;
            } else {
                throw new Exception("Ошибка мотивации fetchv3<br>");
            }
//            echo sprintf("select * from member_percent_advanced where member_id=? and type=? and date_s>STR_TO_DATE(01.".$date.",'%%d.%%m.%%Y') and date_e>STR_TO_DATE(01.".$date.",'%%d.%%m.%%Y') order by id desc LIMIT 1", $member_id, $type)."<BR>";
            $d = DateTime::createFromFormat("d.m.Y", "01.".$date);
            $d->modify("+1 month");
            $query = "select id, procents, hash from member_percent_advanced where member_id=? and type=? and date_s<STR_TO_DATE(".$d->format("m.Y").",'%m.%Y') and date_e>STR_TO_DATE(".$d->format("m.Y").",'%m.%Y') order by id desc LIMIT 1";
            $sth = $this->dbh->prepare($query);

            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            };

            $sth->execute([$member_id, $type]);

            if ($sth->rowCount() > 0) {
                $rawresult = $sth->fetch(PDO::FETCH_ASSOC);
                $this->hash = $rawresult['hash'];
                $this->AdvancedPercent_id = $rawresult['id'];

                $sth = null;
                $this->dbh = null;

                foreach (unserialize($rawresult['procents']) as $key => $value) {
                    $this->Percent[$key] = $value;
                }

                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * @return null
     */
    public function getAdvancedPercentId()
    {
        return $this->AdvancedPercent_id;
    }

    public function get()
    {
        return $this->Percent;
    }

    public function count() {
        return count($this->Percent);
    }

    public function add($item_id, $item_value, $member_id, $changer_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if (!isset($this->motivation)) {
            $this->fetchMotivation($member_id);
        }

        // Проверка наличия в БД
        if (($this->motivation == 1 ) || ($this->motivation == 4 ) || ($this->motivation == 5) || ($this->motivation == 7) || ($this->motivation == 8)) {
            $query = "select * from member_cp_statgroup where member_id=? and active=1 and statgroup_id=? order by id LIMIT 1";
        } else if (($this->motivation == 2) || ($this->motivation == 3)) {
            $query = "select * from member_cp_department where member_id=? and active=1 and department_id=? order by id LIMIT 1";
        } else {
            throw new Exception("Ошибка мотивации");
        }

        $sth = $this->dbh->prepare($query);

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$member_id, $item_id]);
        $count = $sth->rowCount();


        if ($count > 0) {
            if (($this->motivation == 1 ) || ($this->motivation == 4 ) || ($this->motivation == 5) || ($this->motivation == 7) || ($this->motivation == 8)) {
                $query = "update member_cp_statgroup SET active=0, date_e=NOW(),changer_id=? where member_id=? and statgroup_id=?";
            } else if (($this->motivation == 2) || ($this->motivation == 3)) {
                $query = "update member_cp_department SET active=0, date_e=NOW(),changer_id=? where member_id=? and department_id=?";
            } else {
                throw new Exception("Ошибка мотивации");
            }
            $sth = $this->dbh->prepare($query);
            $sth->execute([$changer_id, $member_id, $item_id]);
        }

        if (($this->motivation == 1 ) || ($this->motivation == 4 ) || ($this->motivation == 5) || ($this->motivation == 7) || ($this->motivation == 8)) {
            $query = "insert into member_cp_statgroup (percent, member_id, statgroup_id, date_s, changer_id) VALUES (?,?,?,NOW(),?)";
        } else if (($this->motivation == 2) || ($this->motivation == 3)) {
            $query = "insert into member_cp_department (percent, member_id, department_id, date_s, changer_id) VALUES (?,?,?,NOW(),?)";
        } else {
            throw new Exception("Ошибка мотивации");
        }

        $sth = $this->dbh->prepare($query);
        $sth->execute([$item_value, $member_id, $item_id, $changer_id]);
//        echo "обновлено: ".$sth->rowCount()."<BR>";

        $sth = null;
        $this->dbh = null;
    }


    public function add2($array, $member_id, $changer_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

//        var_dump($array); echo  "<BR>";

        foreach ($array as $key=>$value) {
            if (!is_numeric($key)) {
                throw  new Exception("Расширенные проценты. Ошибка в типе данных ключ: ". $key);
                return 1;
            }

            if ($value=="") {
                unset($array[$key]);
                continue;
            }

            if (strpos($value,',')) {
                $value = str_replace(",", ".", $value);  // Мало ли с запятой ввели
                $array[$key] = $value;
            }

            if (!is_numeric($value)) {
                throw  new Exception("Расширенные проценты. Ошибка в типе данных значение: ". $value);
                return 1;
            }
        }

        $serializeProcent = serialize($array);
        $Hash = hash('md5', $serializeProcent);


        if ($this->hash == $Hash) {
            /*
             * Хеши совпадают, т.е. изменений не было.
             */
            return 0;
        }

        if (!isset($this->motivation)) {
            $this->fetchMotivation($member_id);
        }

        /*
         * Тип
         * 1 - статгурппы
         * 2 - направления
         */
        $type = 1;

        if (($this->motivation == 1 ) || ($this->motivation == 4 ) || ($this->motivation == 5) || ($this->motivation == 7) || ($this->motivation == 8)) {
            $type = 1;
        } else if (($this->motivation == 2) || ($this->motivation == 3)) {
            $type = 2;
        } else {
            throw new Exception("Ошибка мотивации");
        }

        $query = "select hash from member_percent_advanced where member_id = ? and type=? and active=1 LIMIT 1";

        $sth = $this->dbh->prepare($query);

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$member_id, $type]);
        $count = $sth->rowCount();



        if ($count > 0) {
            /*
             * Если хеши не совпадают, то это значения обновлены
             */
            if ($sth->fetch(PDO::FETCH_ASSOC)['hash'] != $Hash) {
                $query = "update member_percent_advanced SET active=0, date_e=NOW(),changer_id=?, change_date=NOW() where member_id=? and type=? and active=1";
                $sth = $this->dbh->prepare($query);
                $sth->execute([$changer_id, $member_id, $type]);

                $query = "insert into member_percent_advanced (member_id, type, procents, hash, date_s, changer_id) VALUES (?, ?, ?, ?, NOW(), ?)";
                $sth = $this->dbh->prepare($query);
                $sth->execute([$member_id, $type, $serializeProcent, $Hash, $changer_id]);
            }
        } else {
            /*
             * Значения еще не были занесены, заносим их
             */

            $query = "insert into member_percent_advanced (member_id, type, procents, hash, date_s, changer_id) VALUES (?, ?, ?, ?, NOW(), ?)";
            $sth = $this->dbh->prepare($query);
            $sth->execute([$member_id, $type, $serializeProcent, $Hash, $changer_id]);
        }

        unset($this->Percent);

        foreach (unserialize($serializeProcent) as $key=>$value) {
            $this->Percent[$key] = $value;
        }

        $this->hash = $Hash;

        $sth = null;
    }

    public function addWithDate($item_id, $item_value, $member_id, $changer_id, $date)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if ($item_value > 100) {
            throw new Exception("Процент должен быть от 0 до 100");
        }

        if (!isset($this->motivation)) {
            $this->fetchMotivation($member_id);
        }

        // Проверка наличия в БД
        if (($this->motivation == 1 ) || ($this->motivation == 4 ) || ($this->motivation == 5) || ($this->motivation == 7)) {
            $query = "select * from member_cp_statgroup where member_id=? and active=1 and statgroup_id=? order by id LIMIT 1";
        } else if (($this->motivation == 2) || ($this->motivation == 3)) {
            $query = "select * from member_cp_department where member_id=? and active=1 and department_id=? order by id LIMIT 1";
        } else {
            throw new Exception("Ошибка мотивации");
        }

        $sth = $this->dbh->prepare($query);

        if (!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute([$member_id, $item_id]);
        $count = $sth->rowCount();


        if ($count > 0) {
            if (($this->motivation == 1 ) || ($this->motivation == 4 ) || ($this->motivation == 5) || ($this->motivation == 7)) {
                $query = "update member_cp_statgroup SET active=0, date_e=STR_TO_DATE(?, '%d.%m.%Y'),changer_id=? where member_id=? and statgroup_id=? and active=1";
            } else if (($this->motivation == 2) || ($this->motivation == 3)) {
                $query = "update member_cp_department SET active=0, date_e=STR_TO_DATE(?, '%d.%m.%Y'),changer_id=? where member_id=? and department_id=? and active=1";
            } else {
                throw new Exception("Ошибка мотивации");
            }
            $sth = $this->dbh->prepare($query);
            $sth->execute([$date, $changer_id, $member_id, $item_id]);
        }

        if (($this->motivation == 1 ) || ($this->motivation == 4 ) || ($this->motivation == 5) || ($this->motivation == 7)) {
            $query = "insert into member_cp_statgroup (percent, member_id, statgroup_id, date_s, changer_id) VALUES (?,?,?,STR_TO_DATE(?, '%d.%m.%Y'),?)";
        } else if (($this->motivation == 2) || ($this->motivation == 3)) {
            $query = "insert into member_cp_department (percent, member_id, department_id, date_s, changer_id) VALUES (?,?,?,STR_TO_DATE(?, '%d.%m.%Y'),?)";
        } else {
            throw new Exception("Ошибка мотивации");
        }

        $sth = $this->dbh->prepare($query);
        $sth->execute([$item_value, $member_id, $item_id, $date, $changer_id]);
//        echo "обновлено: ".$sth->rowCount()."<BR>";

        $sth = null;
        $this->dbh = null;
    }
}