<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 15.11.18
 * Time: 15:57
 */


require_once 'app/functions.php';
require_once 'app/Envelopes.php';
require_once 'app/ReportStatus.php';


class EnvelopeInterface
{
    private $dbh;

    private $MAX_ENVELOPE_SUMM = 50000;
    private $MIN_ENVELOPE_SUMM = 5000;
    private $MAX_Envelope_NumSize = 4;

    private $rawData = array();
    private $TypeOfPayment = 1;
    private $date;


    private $SQLEnvData = array(); // Данные конвертов из SQL
    private $SQLMembersSumm = array(); // Сумма всех конвертов по пользователю ['MEMBER_ID'] = SUMM

    private $FinalEnvData;

    private $RS;
    private $ReportStatus;

    public $filter = false;

    public function setMaxEnvSumm($a)
    {
        $this->MAX_ENVELOPE_SUMM = $a;
    }

    public function setMinEnvSumm($a)
    {
        $this->MIN_ENVELOPE_SUMM = $a;
    }

    public function setMaxEnvNumSize($a)
    {
        $this->MAX_Envelope_NumSize = $a;
    }

    public function setRawData($data)
    {
        $this->rawData = $data;
        if (count($this->rawData) > 0) {
            $this->roundSumm();
        }
    }

    public function setEnvData(&$EnvData) {
        $this->FinalEnvData = &$EnvData;
    }

    public function setDateType($date, $type)
    {
        $this->date = $date;
        $this->TypeOfPayment = $type;

        $this->RS = new ReportStatus(); // Статус ведомости.
        $this->ReportStatus = $this->RS->getStatus($this->date, $this->TypeOfPayment);
    }

    private function roundSumm()
    {
        // Округление до 10
        foreach ($this->rawData as $item) {
            if ($item->getSumm() <= 0) { //Если сумма <= 0 то не обрабатываем данные
                continue;
            } else {
//                $item->setSumm(round($item->getSumm())); // округляю сотые и десятые
                $item->setSumm((int)$item->getSumm());

                $ostatok = ($item->getSumm() % 10); // Получаю остаток от деления на 10
                if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
                    $item->setSumm($item->getSumm() - $ostatok + 10);
                } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
                    $item->setSumm($item->getSumm() - $ostatok);
                };
            }
        }
    }

    public function fetch()
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT SUM(summ) as summ, member_id FROM envelope where date=STR_TO_DATE(?, '%m.%Y') and type_of_payment=? group by member_id"; //Получаю ВСЮ сумму, member_id, зная тип и дату, группирую по id пользователя
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$this->date, $this->TypeOfPayment]); // Выполняю запрос
        } catch (PDOException $e) {
            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
        }
        if ($sth->rowCount() > 0) { // Если количество строк > 0, значит данные есть
            foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $item) { // Добавляю данные в массив SQLMemberSumm
                $this->SQLMembersSumm[$item['member_id']] = $item['summ'];
            }
        }

        $query = "SELECT COUNT(*) as total FROM zp.envelope where type_of_payment=? AND date=STR_TO_DATE(?,'%m.%Y') group by member_id order by total desc limit 1"; // Проверяю максимальное количество конвертов в БД.
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$this->TypeOfPayment, $this->date]); // Выполняю запрос
        } catch (PDOException $e) {
            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
        }
        if ($sth->rowCount() > 0) {
            $raw = $sth->fetch();
            if ($raw['total'] > $this->MAX_Envelope_NumSize) {
                $this->MAX_Envelope_NumSize = $raw['total'];
            }
        }

        $query = "select * from envelope where type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y')"; // Получаю ранее выданные конверты из БД
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$this->TypeOfPayment, $this->date]); // Выполняю запрос
        } catch (PDOException $e) {
            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
        }

        if ($sth->rowCount() > 0) { // Если количество строк больше > 0 значит данные есть
            $SQLRaw = $sth->fetchAll(PDO::FETCH_ASSOC); // Извлекаю данные и добавляю в массив объектов

            foreach ($SQLRaw as $item) {
                $Env = new Envelopes(); // Создаю объект конверта
                $Env->setSumm($item['summ']); // Устанавливаю остаток от суммы
                $Env->setOldSumm();
                $Env->setMemberId($item['member_id']); // ID пользователя
                $Env->setModify($item['modify']); // Были ли исправления
//        $Env->setName(); // ФИО
                $Env->setEnvelopeId($item['envelope_id']); // Номер конверта
                $this->SQLEnvData[] = $Env; // Добавляю в  массив данных
            }
        }
    }

    public function Calculation()
    {
        $rtn = 0;
        if (count($this->SQLEnvData) == 0) { // Если в БД ничего нету, то количество элементов в массиве == 0 и нам нужно создать конверты на лету.
            // Делаю копию массива объектов, для того чтобы его можно было ломать в разборе
            $tmpDataArray = array_map(function ($object) {
                return clone $object;
            }, $this->rawData);

            $EnvData = array();

            $EnvelopeNumber = 0; // Номер конверта
            $EnvelopeSize = 0;

            for ($i = 0; $i < $this->MAX_Envelope_NumSize; $i++) { // Максимум конвертов на сотрудника
                foreach ($tmpDataArray as $item) {
                    if ($item->getSumm() <= 0) { // если сумма в ведомости <=0 тогда не выводим
                        continue;
                    }

                    $EnvelopeNumber++; // Расчет номера конверта
                    if (($this->MAX_Envelope_NumSize > 3) && ($i == ($this->MAX_Envelope_NumSize - 2)) && ($item->getSumm() > (2 * $this->MAX_ENVELOPE_SUMM))) { // Если Сумма в 4ом конверте > чем максимально возможная сумма
                        $OSTATOK = ($item->getSumm() % $this->MAX_ENVELOPE_SUMM); // То расчитываю остаток
                        if ($OSTATOK == 0) {
                            $OSTATOK = $this->MAX_ENVELOPE_SUMM;
                        }

                        if (($item->getSumm() - $OSTATOK) > $this->MAX_ENVELOPE_SUMM) {
                            $OSTATOK += $this->MAX_ENVELOPE_SUMM;
                        }

                        $Env = new Envelopes(); // Создаю объект конверта
                        $Env->setSumm($OSTATOK); // Устанавливаю остаток от суммы
                        $Env->setMemberId($item->getId()); // ID пользователя
                        $Env->setName($item->getFio()); // ФИО
                        $Env->setEnvelopeId($EnvelopeNumber); // Номер конверта
                        $item->setSumm($item->getSumm() - $OSTATOK); // Из суммы вычитаю остаток

                        $EnvData[] = $Env; // Добавляю в  массив данных
                    } else if ($i == ($this->MAX_Envelope_NumSize - 1)) {

                        $Env = new Envelopes(); // Создаю объект конверта
                        $Env->setSumm($item->getSumm()); // Устанавливаю ВСЕ от суммы
                        $Env->setMemberId($item->getId()); // ID пользователя
                        $Env->setName($item->getFio()); // ФИО
                        $Env->setEnvelopeId($EnvelopeNumber); // Номер конверта
                        $item->setSumm(0); // Сумму выставляю в 0
                        $EnvData[] = $Env; // Добавляю в  массив данных
                    } else {
                        if (($item->getSumm() <= $this->MAX_ENVELOPE_SUMM)) { // Если меньше Максимальной суммы

                            $Env = new Envelopes(); // Создаю объект
                            $Env->setSumm($item->getSumm()); //Устанавливаю сумму конверта
                            $Env->setMemberId($item->getId()); // Устанавливаю ID пользователя
                            $Env->setName($item->getFio()); // Устанавливаю ФИО
                            $Env->setEnvelopeId($EnvelopeNumber); // Установка номера конверта

                            $item->setSumm(0); // Установить сумму в 0.

                            $EnvData[] = $Env; // Добавить в массив
                        } else if ($item->getSumm() > $this->MAX_ENVELOPE_SUMM) { // Если больше максимальной суммы
                            $Env = new Envelopes(); // Создаю объект
                            $Env->setSumm($this->MAX_ENVELOPE_SUMM); // Устанавливаю МАКСИМАЛЬНУЮ сумму конверта
                            $Env->setMemberId($item->getId()); // Устанавливаю ID пользователя
                            $Env->setName($item->getFio()); // Устанавливаю ФИО
                            $Env->setEnvelopeId($EnvelopeNumber);  // Установка номера конверта

                            $item->setSumm($item->getSumm() - $this->MAX_ENVELOPE_SUMM); // Вычитаю максимальную сумму

                            if ($item->getSumm() < $this->MIN_ENVELOPE_SUMM) { // Если остаток суммы меньше MIN_ENVELOPE_SUMM
                                $Env->setSumm($Env->getSumm() + $item->getSumm());  // То выдаем в том же конверте
                                $item->setSumm(0); // Сумму по ведомости ставлю в 0
                            }

                            $EnvData[] = $Env;
                        }
                    }
                };
            }
            $tmpDataArray = null; // Очищаю память от tmp массива объектов
            $this->FinalEnvData = $EnvData;
        } else { // Если данные есть в SQL, то провожу проверку, нету ли измененных данных
            $this->FinalEnvData = $this->SQLEnvData;
            $EnvelopeNumber = $this->FinalEnvData[count($this->FinalEnvData) - 1]->getEnvelopeId(); // Получаю последний номер конверта

            foreach ($this->SQLMembersSumm as $member_id => $summ) {
                $current = current(array_filter($this->rawData, function ($obj) use ($member_id) { // Ищу RAW данные(данные из ведомости) по пользователю,
                    return ($obj->getId() == $member_id); // ID пользователя в ведомости в конвертах совпадает, для сравнения ИТОГО суммы с SQL данными
                }));

                if ($current) { // В ведомости такой пользователь есть, если нету значи пользователь написал заявление на увольнение.

                    if ($current->getSumm() == $summ || $this->ReportStatus['status_id']==6) { // Суммы == значит, ничего не изменилось
                        continue;
                    } else if ($summ > $current->getSumm()) { // Если в БД сумма меньше чем по ведомости, например внесли удержания
                        $rtn = 1;
                        echo $current->getFio() . "<BR>";
                        echo "Сумма > чем в ведомости<BR>";
                        $RAZNICA = $summ - $current->getSumm(); // Получаю разницу в сумме
                        echo "Разница " . $RAZNICA . "<BR>";

                        $member_array = array_filter($this->FinalEnvData, function ($obj) use ($member_id) { // Ищу все конверты в SQL данных
                            return ($obj->getMemberId() == $member_id); // где ID пользователя в ведомости в конвертах совпадает
                        });

                        $list_keys = array(); // Получаю массив с ключами массива, где находятся конверты пользователя
                        echo "Массив с ключами: <BR>";
                        foreach ($member_array as $key => $value) {
                            $list_keys[] = $key; // Добавляю ключи в массив
                            echo "Key: " . $key . "<BR>";
                        }

                        if ($RAZNICA >= $summ) { // Если полученная разница больше чем сумма по конвертам, то удаляю эти конверты
                            echo "Разница >= ВСЕЙ суммы, удаляю все конверты<br>";
                            foreach ($list_keys as $value) {
                                $this->FinalEnvData[$value] = null; // Удаляю данные из основного массива
                            }
                        } else {
                            echo "Перебираю конверты " . count($list_keys) . " эл.<br>";

                            for ($i = count($list_keys) - 1; $i > 0, $RAZNICA > 0; $i--) { // Перебираем конверты с конца
                                echo "Проверяю эл. массива: " . $list_keys[$i] . "<BR>";
                                if ($this->FinalEnvData[$list_keys[$i]]->getSumm() >= $RAZNICA) {
                                    echo "В конверте " . $this->FinalEnvData[$list_keys[$i]]->getEnvelopeId() . " сумма >= чем в \"разнице\" вычитаю все из конверта, разницу обнуляю <BR>";

                                    $this->FinalEnvData[$list_keys[$i]]->setSumm($this->FinalEnvData[$list_keys[$i]]->getSumm() - $RAZNICA);
                                    $this->FinalEnvData[$list_keys[$i]]->setStatus(1); // Ставлю статуст что было изменение
                                    $RAZNICA = 0;
                                } else {
                                    echo "В конверте < чем в \"разнице\" вычитаю сумму конверта из \"разницы\", конверт " . $list_keys[$i] . " обнуляю <BR>";
                                    $RAZNICA -= $this->FinalEnvData[$list_keys[$i]]->getSumm();
//                            unset($this->FinalEnvData[$list_keys[$i]]);
                                    $this->FinalEnvData[$list_keys[$i]]->setSumm(0);
                                    $this->FinalEnvData[$list_keys[$i]]->setStatus(2);
                                }
                            }
                        }
//                sort($this->FinalEnvData);
                        echo "<BR>";
                    } else if ($summ < $current->getSumm()) { // Сумма в SQL < меньше чем в ведомости, т.е. поступили новые данные и начислили еще денег
                        $rtn = 1;
                        echo $current->getFio() . "<BR>";
                        echo "Сумма ведомости > чем в SQL <BR>";
                        $RAZNICA = $current->getSumm() - $summ; // Получаю разницу в сумме
                        echo "Разница " . $RAZNICA . "<BR>";

                        $member_array = array_filter($this->FinalEnvData, function ($obj) use ($member_id) { // Ищу все конверты в SQL данных
                            return ($obj->getMemberId() == $member_id); // где ID пользователя в ведомости в конвертах совпадает
                        });

                        $list_keys = array(); // Получаю массив с ключами массива, где находятся конверты пользователя
                        echo "Массив с ключами: <BR>";
                        foreach ($member_array as $key => $value) {
                            $list_keys[] = $key; // Добавляю ключи в массив
                            echo "Key: " . $key . "<BR>";
                        }

                        echo "Количество конвертов: " . count($list_keys) . "<BR>";

                        if (count($list_keys) == $this->MAX_Envelope_NumSize) { // Если количество конвертов == максимальному количеству, ([предпоследний конверт] + RAZNICA) > MAX_SIZE,
                            echo "Количество конвертов == MAX_Envelope_NumSize<BR>";
                            // то добавляю в последний MAX_SIZE, и разницу в пердпоследний
                            if (($this->FinalEnvData[$list_keys[count($list_keys) - 2]]->getSumm() + $RAZNICA) > $this->MAX_ENVELOPE_SUMM) { // Если сумма предпоследнего элемента + РАЗНИЦА > чем Максимально возможная сумма
                                echo "Сумма передпоследнего + РАЗНИЦА > MAX размер в конверте<BR>";
                                $SUMM = ($this->FinalEnvData[$list_keys[count($list_keys) - 2]]->getSumm() + $this->FinalEnvData[$list_keys[count($list_keys) - 1]]->getSumm() + $RAZNICA); // Получаю сумму 2х предпоследних конвертов и РАЗНИЦЫ
                                echo "Сумма 2х последний эл-тов + RAZNICA: " . $SUMM . "<BR>";
                                $OSTATOK = ($SUMM % $this->MAX_ENVELOPE_SUMM); // Получаю остаток от деления на Максимальный размер конверта
                                $this->FinalEnvData[$list_keys[count($list_keys) - 2]]->setSumm($OSTATOK); // Устанавливаю остаток в предпоследний конверт
                                $this->FinalEnvData[$list_keys[count($list_keys) - 1]]->setSumm($SUMM - $OSTATOK); // Устанавливаю новую сумму в последний конверт
                                $this->FinalEnvData[$list_keys[count($list_keys) - 1]]->setStatus(1);
                                $this->FinalEnvData[$list_keys[count($list_keys) - 2]]->setStatus(1);
                                $RAZNICA = 0; // Ставлю разницу == 0
                            } else { // Иначе добавляю остаток в предпоследний конверт
                                echo "Сумма передпоследнего + РАЗНИЦА < MAX размер в конверте<BR>";
                                $this->FinalEnvData[$list_keys[count($list_keys) - 2]]->setSumm($this->FinalEnvData[$list_keys[count($list_keys) - 2]]->getSumm() + $RAZNICA); // Устанавливаю остаток в предпоследний конверт
                                $this->FinalEnvData[$list_keys[count($list_keys) - 2]]->setStatus(1);
                                $RAZNICA = 0; // Ставлю разницу == 0
                            }
                        } else { // Если это не максимально возможное количество конвертов
                            echo "Количество конвертов != MAX_Envelope_NumSize<BR>";
                            echo "Номер последнего конверта: " . $EnvelopeNumber . "<BR>";

                            if ($this->FinalEnvData[$list_keys[count($list_keys) - 1]]->getSumm() + $RAZNICA <= $this->MAX_ENVELOPE_SUMM) { // Если сумма последнего конверта  и $RAZNICA <= максимально возможной суммы в конверте
                                echo "Сумма последнего конверта <= $this->MAX_ENVELOPE_SUMM<BR>";
                                $this->FinalEnvData[$list_keys[count($list_keys) - 1]]->setSumm($this->FinalEnvData[$list_keys[count($list_keys) - 1]]->getSumm() + $RAZNICA); // Добавляю сумму в этот же конверт
                                $this->FinalEnvData[$list_keys[count($list_keys) - 1]]->setStatus(1);
                                $RAZNICA = 0;
                            } else { // Если больше
                                echo "Сумма последнего конверта > $this->MAX_ENVELOPE_SUMM<BR>";
                                if ((count($list_keys) == ($this->MAX_Envelope_NumSize - 1)) && (($this->FinalEnvData[$list_keys[count($list_keys) - 1]]->getSumm() + $RAZNICA) >= $this->MAX_ENVELOPE_SUMM)) { // Если это предпоследний конверт и разница > чем максимально возможная сумма
                                    echo "Если колчество конвертов == предпоследнему возможнуому конверту, а разница > максимальной сумме в конверте<BR>";
                                    $SUMM = $this->FinalEnvData[$list_keys[count($list_keys) - 1]]->getSumm() + $RAZNICA; // Получаю сумму последнего конверта  и разницу
                                    $OSTATOK = ($SUMM % $this->MAX_ENVELOPE_SUMM); // То расчитываю остаток
                                    $this->FinalEnvData[$list_keys[count($list_keys) - 1]]->setSumm($OSTATOK); // Устанавливаю остаток в последний элемент
                                    $this->FinalEnvData[$list_keys[count($list_keys) - 1]]->setStatus(1);

                                    $EnvelopeNumber++;
                                    $MEMBER_ID = $this->FinalEnvData[$list_keys[count($list_keys) - 1]]->getMemberId(); // Получаю ID пользователя
                                    echo "ID пользователя: " . $MEMBER_ID . " <BR>";

                                    $Env = new Envelopes(); // Создаю объект конверта
                                    $Env->setSumm($SUMM - $OSTATOK); // Устанавливаю остаток от суммы
                                    $Env->setMemberId($MEMBER_ID); // ID пользователя
                                    $Env->setEnvelopeId($EnvelopeNumber); // Номер конверта
                                    $Env->setStatus(1);
                                    $RAZNICA = 0; // Из суммы вычитаю остаток
                                    $this->FinalEnvData[] = $Env; // Добавляю в  массив данных
                                } else {
                                    echo "Еще не предпоследний и не последний конверт <BR>";
                                    $OSTATOK = $this->MAX_ENVELOPE_SUMM - $this->FinalEnvData[$list_keys[count($list_keys) - 1]]->getSumm(); // Получаю разницу между максимальным количеством в конверте и суммой в конверте
                                    $this->FinalEnvData[$list_keys[count($list_keys) - 1]]->setSumm($this->MAX_ENVELOPE_SUMM); // В Последний конверт выставляю максимальную сумму
                                    $RAZNICA -= $OSTATOK; // Получаю сумму

                                    $MEMBER_ID = $this->FinalEnvData[$list_keys[count($list_keys) - 1]]->getMemberId(); // Получаю ID пользователя
                                    echo "ID пользователя: " . $MEMBER_ID . " <BR>";

                                    for ($i = (count($list_keys) - 1); $i < $this->MAX_Envelope_NumSize, $RAZNICA > 0; $i++) { // Максимум конвертов на сотрудника
                                        $EnvelopeNumber++; // Расчет номера конверта
                                        if (($this->MAX_Envelope_NumSize > 3) && ($i == ($this->MAX_Envelope_NumSize - 2)) && ($RAZNICA > (2 * $this->MAX_ENVELOPE_SUMM))) { // Если Сумма в 4ом конверте > чем максимально возможная сумма
                                            $OSTATOK = ($RAZNICA % $this->MAX_ENVELOPE_SUMM); // То расчитываю остаток
                                            $Env = new Envelopes(); // Создаю объект конверта
                                            $Env->setSumm($OSTATOK); // Устанавливаю остаток от суммы
                                            $Env->setMemberId($MEMBER_ID); // ID пользователя
                                            $Env->setEnvelopeId($EnvelopeNumber); // Номер конверта
                                            $Env->setStatus(1);
                                            $RAZNICA -= $OSTATOK; // Из суммы вычитаю остаток
                                            $this->FinalEnvData[] = $Env; // Добавляю в  массив данных
                                        } else if ($i == ($this->MAX_Envelope_NumSize - 1)) {
                                            $Env = new Envelopes(); // Создаю объект конверта
                                            $Env->setSumm($RAZNICA); // Устанавливаю ВСЕ от суммы
                                            $Env->setMemberId($MEMBER_ID); // ID пользователя
                                            $Env->setEnvelopeId($EnvelopeNumber); // Номер конверта
                                            $RAZNICA = 0; // Сумму выставляю в 0
                                            $this->FinalEnvData[] = $Env; // Добавляю в  массив данных
                                        } else {
                                            if (($RAZNICA < $this->MAX_ENVELOPE_SUMM)) { // Если меньше Максимальной суммы
                                                $Env = new Envelopes(); // Создаю объект
                                                $Env->setSumm($RAZNICA); //Устанавливаю сумму конверта
                                                $Env->setMemberId($MEMBER_ID); // Устанавливаю ID пользователя
                                                $Env->setEnvelopeId($EnvelopeNumber); // Установка номера конверта
                                                $Env->setStatus(1);

                                                $RAZNICA = 0; // Установить сумму в 0.

                                                $this->FinalEnvData[] = $Env; // Добавить в массив
                                            } else if ($RAZNICA > $this->MAX_ENVELOPE_SUMM) { // Если больше максимальной суммы
                                                $Env = new Envelopes(); // Создаю объект
                                                $Env->setSumm($this->MAX_ENVELOPE_SUMM); // Устанавливаю МАКСИМАЛЬНУЮ сумму конверта
                                                $Env->setMemberId($MEMBER_ID); // Устанавливаю ID пользователя
                                                $Env->setEnvelopeId($EnvelopeNumber);  // Установка номера конверта
                                                $Env->setStatus(1);

                                                $RAZNICA -= $this->MAX_ENVELOPE_SUMM; // Вычитаю максимальную сумму

                                                if ($RAZNICA < $this->MIN_ENVELOPE_SUMM) { // Если остаток суммы меньше MIN_ENVELOPE_SUMM
                                                    $Env->setSumm($Env->getSumm() + $RAZNICA);  // То выдаем в том же конверте
                                                    $RAZNICA = 0; // Сумму по ведомости ставлю в 0
                                                }
                                                $this->FinalEnvData[] = $Env;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else { // Помечен на увольнение или уволен, убираем из ведомости
                    $rtn = 1;
                    $member_array = array_filter($this->FinalEnvData, function ($obj) use ($member_id) { // Ищу все конверты в SQL данных
                        return ($obj->getMemberId() == $member_id); // где ID пользователя в ведомости в конвертах совпадает
                    });

                    foreach ($member_array as $key => $value) {
                        $this->FinalEnvData[$key]->setSumm(0);
                        $this->FinalEnvData[$key]->setStatus(2);
                        $this->FinalEnvData[$key]->setModify(1);
                    }
                }
            }
        }

        return $rtn;
    }

    public function CatcherCalc() {
        $this->FinalEnvData = $this->SQLEnvData;
    }

    public function Confirm()
    {
        if (is_null($this->dbh)) {
            $this->dbh = dbConnect();
        }

        $this->dbh->beginTransaction();
        if (count($this->SQLEnvData) == 0) { // Если еще не заносили ничего в БД
//            foreach ($this->rawData as $item) { //Перебираю ведомость, получаю id сотрудников, для поиска в объектах конвертов
//                $current = array_filter($this->FinalEnvData, function ($obj) use ($item) {
//                    return $obj->getMemberId() == $item->getId(); // Ищем все конверты где ID пользователя в ведомости в конвертах совпадает
//                });

//                if ($current) {
                    foreach ($this->FinalEnvData as $env_item) {
                        $query = "select * from envelope where member_id=? and envelope_id=? and type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y')";
                        try {
                            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                            $sth->execute([$env_item->getMemberId(), $env_item->getEnvelopeId(), $this->TypeOfPayment, $this->date]);
                        } catch (PDOException $e) {
                            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                        }

                        if ($sth->rowCount() == 0) {
                            $query = "insert into envelope (member_id,envelope_id,summ,date,type_of_payment) VALUES (?,?,?,STR_TO_DATE(?,'%m.%Y'),?)";
                            try {
                                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                                $sth->execute([$env_item->getMemberId(), $env_item->getEnvelopeId(), $env_item->getSumm(), $this->date, $this->TypeOfPayment]);
                            } catch (PDOException $e) {
                                echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                            }
                        }
                    }
//                } else {
//                    continue;
//                }
//            }
        } else {
            $current = array_filter($this->FinalEnvData, function ($obj) {
                return $obj->getStatus() > 0; // Ищем все конверты где ID пользователя в ведомости в конвертах совпадает
            });

            if ($current) {
                foreach ($current as $env_item) {
                    $query = "select * from envelope where member_id=? and envelope_id=? and type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y')";
                    try {
                        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                        $sth->execute([$env_item->getMemberId(), $env_item->getEnvelopeId(), $this->TypeOfPayment, $this->date]);
                    } catch (PDOException $e) {
                        echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                    }

                    if ($sth->rowCount() == 0) {
                        $query = "insert into envelope (member_id,envelope_id,summ,date,type_of_payment) VALUES (?,?,?,STR_TO_DATE(?,'%m.%Y'),?)";
                        try {
                            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                            $sth->execute([$env_item->getMemberId(), $env_item->getEnvelopeId(), $env_item->getSumm(), $this->date, $this->TypeOfPayment]);
                        } catch (PDOException $e) {
                            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                        }
                    } else {
                        $query = "update envelope SET summ=?, modify=1 where member_id=? AND envelope_id=? AND date=STR_TO_DATE(?,'%m.%Y') AND type_of_payment=?";
                        try {
                            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                            $sth->execute([$env_item->getSumm(), $env_item->getMemberId(), $env_item->getEnvelopeId(), $this->date, $this->TypeOfPayment]);
                        } catch (PDOException $e) {
                            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
                        }
                    }
                }
            }
        }
        $this->dbh->commit();
    }

    public function get() {
        return $this->FinalEnvData;
    }

    public function count() {
        return count($this->FinalEnvData);
    }

    public function countSqlData() {
        return count($this->SQLEnvData);
    }

    public function getMaxEnvSumm() {
        return $this->MAX_ENVELOPE_SUMM;
    }

    public function getMinEnvSumm() {
        return $this->MIN_ENVELOPE_SUMM;
    }

    public function getMaxEnvNumSize() {
        return $this->MAX_Envelope_NumSize;
    }

    public function getTypeOfPayment() {
        return $this->TypeOfPayment;
    }
    public function getDate() {
        return $this->date;
    }

    static function dropItems($date, $TypeOfPayment) {
        $dbh = dbConnect();
        $query = "DELETE FROM zp.envelope where date=STR_TO_DATE(?,'%m.%Y') and type_of_payment=?";
        try {
            $sth = $dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date, $TypeOfPayment]); // Выполняю запрос
        } catch (PDOException $e) {
            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
        }
    }
}