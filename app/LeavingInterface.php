<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 07.08.18
 * Time: 10:09
 */

require_once 'functions.php';

class LeavingInterface
{

    private $reasonsList = array();
    private $dbh;

    public function fetchReasons() {
        $this->dbh=dbConnect();
        $query = "select * from reasons_for_leaving order by id";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute();
        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $this->dbh = null;

        $this->reasonsList = $rawresult;
    }

    public function GetReasons() {
        if (empty($this->reasonsList)) {
            throw new Exception("Пустой список причин.");
        }
        return $this->reasonsList;
    }

    public function AddReason($name) {
        $this->dbh=dbConnect();

        if (empty($this->dbh)) {
            echo "Error<br>";
        }

        $query = "INSERT INTO reasons_for_leaving (name) VALUES (?)";
        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare'.$exception->getMessage());
        }

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$name])) {
            $error = $sth->errorInfo();
            throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: '.$error[1].'.<br> Текст ошибки: '.$error[2]."<BR>");
        }

        $sth = null;
        $this->dbh = null;

        return true;
    }




}