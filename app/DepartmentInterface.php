<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 18.07.18
 * Time: 12:56
 */

require_once 'Department.php';
require_once 'functions.php';

class DepartmentInterface
{
    private $departmentList = array();
    private $dbh;

    public function fetchDepartments() {
        if (!empty($this->departmentList))
            $this->departmentList = array();

        $this->dbh=dbConnect();
//        $query = "select * from department order by id";
        $query = "select * from department order by namesklad,name";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute();
        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $this->dbh = null;

        foreach ($rawresult as $value) {
            $d_array = explode(",", $value['direction_id']);

            foreach ($d_array as $d_id) {
                $Department = new Department();
                $Department->setId($value['id']);
                $Department->setName($value['name']);
                $Department->setIdsklad($value['idsklad']);
                $Department->setNamesklad($value['namesklad']);
                $Department->setDirectionId(array($d_id));
                array_push($this->departmentList, $Department);
            }
        }

//        foreach ($rawresult as $value) {
//            $Department = new Department();
//            $Department->setId($value['id']);
//            $Department->setName($value['name']);
//            $Department->setIdsklad($value['idsklad']);
//            $Department->setNamesklad($value['namesklad']);
//            $Department->setDirectionId(explode(",",$value['direction_id']));
//            array_push($this->departmentList,$Department);
//        }
    }

    public function fetchActiveDepartments() {
        $this->dbh=dbConnect();
        $query = "select * from department where active=1 order by id";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute();
        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $this->dbh = null;

        foreach ($rawresult as $value) {
            $d_array = explode(",",$value['direction_id']);

            foreach ($d_array as $d_id) {
                $Department = new Department();
                $Department->setId($value['id']);
                $Department->setName($value['name']);
                $Department->setIdsklad($value['idsklad']);
                $Department->setNamesklad($value['namesklad']);
                $Department->setDirectionId(array($d_id));
                array_push($this->departmentList,$Department);
            }
        }

//        foreach ($rawresult as $value) {
//            $Department = new Department();
//            $Department->setId($value['id']);
//            $Department->setName($value['name']);
//            $Department->setIdsklad($value['idsklad']);
//            $Department->setNamesklad($value['namesklad']);
//            $Department->setDirectionId(explode(",",$value['direction_id']));
//            array_push($this->departmentList,$Department);
//        }
    }


    public function GetDepartments() {
        if (empty($this->departmentList)) {
            throw new Exception("Пустой список отделов");
        }
        return $this->departmentList;
    }

    /**
     * @return bool
     * @throws Exception
     * Отображает Отделы
     */
    public function ShowDepartments() {
        if (empty($this->departmentList)) {
            throw new Exception("Пустой список отделов");
            return false;
        }

        echo "<table class='table table-bordered'>";
        foreach ($this->departmentList as $value ){
            echo "<tr>";
            echo "<td>".$value->getId()."</td><td>".$value->getName()."</td>";
            echo "</tr>";
        }
        echo "</table>";

        return true;
    }

    public function AddDepartments($name) {
        $this->dbh=dbConnect();

        if (empty($this->dbh)) {
            echo "Error<br>";
        }

        $query = "INSERT INTO department (name) VALUES (?)";
        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare'.$exception->getMessage());
        }

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$name])) {
            $error = $sth->errorInfo();
            throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: '.$error[1].'.<br> Текст ошибки: '.$error[2]."<BR>");
        }

        $sth = null;
        $this->dbh = null;

        return true;
    }


    public function AddDepartmentSingle($name, $direction_id, $sklad_id, $sklad_name) {

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if (is_null($sklad_id)) {
            $sklad_id = 0000000000;
        }

        $query = "select * from department where name = ?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$name]);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare'.$exception->getMessage());
        }

        if ($sth->rowCount() > 0 ) {
            throw new Exception("Отдел с таким названием уже существует");
        } else {
            $query = "INSERT INTO department (name, idsklad, namesklad, direction_id) VALUES (?,?,?,?)";
            try {
                $sth = $this->dbh->prepare($query);
                $sth->execute([$name, $sklad_id, $sklad_name, $direction_id]);
                $department_id = $this->dbh->lastInsertId();
            } catch (PDOException $exception) {
                throw new Exception('Ошибка prepare' . $exception->getMessage());
            }
        }

        $sth = null;
        $this->dbh = null;

        return true;
    }

    public function UpdateDepartmentSingle($id, $name, $direction_id, $sklad_id, $sklad_name) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from department where id = ?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$id]);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare'.$exception->getMessage());
        }

        if ($sth->rowCount() > 0 ) {
            $query = "Update department SET name = ?, idsklad = ?, namesklad = ?, direction_id = ? where id = ?";
//            $query = "INSERT INTO department (name, idsklad, namesklad, direction_id) VALUES (?,?,?,?)";
            try {
                $sth = $this->dbh->prepare($query);
                $sth->execute([$name, $sklad_id, $sklad_name, $direction_id, $id]);
            } catch (PDOException $exception) {
                throw new Exception('Ошибка prepare' . $exception->getMessage());
            }
        } else {
            throw new Exception("Отдел с таким названием уже существует");
        }

        $sth = null;
        $this->dbh = null;

        return true;
    }

    public function findById($id) {
        if (count($this->departmentList)==0) {
            $this->fetchDepartments();
        }
        $member_departmnet = current(array_filter($this->departmentList, function ($d) use ($id) {
            return $d->getId() == $id;
        }));

        if (isset($member_departmnet) && $member_departmnet!=false) {
            return $member_departmnet;
        } else {
            $Department = new Department();
            $Department->setId($id);
            $Department->setName("None");
            $Department->setIdsklad("None");
            return $Department;
        }

    }
}

