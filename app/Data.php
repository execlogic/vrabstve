<?php
require_once 'app/functions.php';

class Data
{
    protected $dbh;
    public function getMakeupDepartment($start_date, $end_date) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT t2.name, department_id, SUM(profit) as sum FROM zp.data 
LEFT JOIN department as t2 on (data.department_id = t2.id)
where date>=? and date<=? group by department_id order by t2.name";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$start_date, $end_date]);
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO' . $e->getMessage());
        }

        return $sth->fetchAll();
    }
}