<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 21.09.18
 * Time: 11:50
 */

require_once 'app/functions.php';


class AdministrativeBlock
{
    private $dbh;
    /***
     * @var type_of_payment
     * Тип выплат
     * 1 - премия
     * 2 - аванс
     * 3 - аванс2
     * 4- расчет
     */
    private $type_of_payment = null;

    private $Promotions = NULL;
    private $AdministrativePrize = NULL;
    private $Training = NULL;
    private $PrizeAnotherDepartment = NULL;
    private $Defect = NULL;
    private $Sunday = NULL;
    private $NotLessThan = NULL;
//    private $MobileCommunication = NULL;
//    private $MobileCommunicationFormula = NULL;
    private $Overtime = NULL;
    private $CompensationFAL = NULL;
    private $CompensationFALFormula = NULL;


    private $Promotions_note = NULL;
    private $AdministrativePrize_note = NULL;
    private $Training_note = NULL;
    private $PrizeAnotherDepartment_note = NULL;
    private $Defect_note = NULL;
    private $Sunday_note = NULL;
    private $NotLessThan_note = NULL;
//    private $MobileCommunication_note = NULL;
    private $Overtime_note = NULL;
    private $CompensationFAL_note = NULL;

    private $Promotions_lock = 0;
    private $AdministrativePrize_lock = 0;
    private $Training_lock = 0;
    private $PrizeAnotherDepartment_lock = 0;
    private $Defect_lock = 0;
    private $Sunday_lock = 0;
    private $NotLessThan_lock = 0;
//    private $MobileCommunication_lock = 0;
    private $Overtime_lock = 0;
    private $CompensationFAL_lock = 0;

    public function setLockAll() {
        $this->Promotions_lock = 1;
        $this->AdministrativePrize_lock = 1;
        $this->Training_lock = 1;
        $this->PrizeAnotherDepartment_lock = 1;
        $this->Defect_lock = 1;
        $this->Sunday_lock = 1;
        $this->NotLessThan_lock = 1;
//        $this->MobileCommunication_lock = 1;
        $this->Overtime_lock = 1;
//        $this->CompensationFAL_lock = 1;
    }

    /**
     * @return null
     */
    public function getCompensationFALFormula()
    {
        return $this->CompensationFALFormula;
    }

    /**
     * @return null
     */
//    public function getMobileCommunicationFormula()
//    {
//        return $this->MobileCommunicationFormula;
//    }

    /**
     * @param null $CompensationFALFormula
     */
//    public function setCompensationFALFormula($CompensationFALFormula)
//    {
//        $this->CompensationFALFormula = $CompensationFALFormula;
//    }

    /**
     * @param null $MobileCommunicationFormula
     */
//    public function setMobileCommunicationFormula($MobileCommunicationFormula)
//    {
//        $this->MobileCommunicationFormula = $MobileCommunicationFormula;
//    }

    /**
     * @return int
     */
    public function getAdministrativePrizeLock()
    {
        return $this->AdministrativePrize_lock;
    }

    /**
     * @return int
     */
    public function getCompensationFALLock()
    {
        return $this->CompensationFAL_lock;
    }

    /**
     * @return int
     */
    public function getDefectLock()
    {
        return $this->Defect_lock;
    }

    /**
     * @return int
     */
//    public function getMobileCommunicationLock()
//    {
//        return $this->MobileCommunication_lock;
//    }

    /**
     * @return int
     */
    public function getNotLessThanLock()
    {
        return $this->NotLessThan_lock;
    }

    /**
     * @return int
     */
    public function getOvertimeLock()
    {
        return $this->Overtime_lock;
    }

    /**
     * @return int
     */
    public function getPrizeAnotherDepartmentLock()
    {
        return $this->PrizeAnotherDepartment_lock;
    }

    /**
     * @return int
     */
    public function getPromotionsLock()
    {
        return $this->Promotions_lock;
    }

    /**
     * @return int
     */
    public function getSundayLock()
    {
        return $this->Sunday_lock;
    }

    /**
     * @return int
     */
    public function getTrainingLock()
    {
        return $this->Training_lock;
    }

    /**
     * @param type_of_payment $type_of_payment
     */
    public function setTypeOfPayment($type_of_payment)
    {
        $this->type_of_payment = $type_of_payment;
    }

    /**
     * @return type_of_payment
     */
    public function getTypeOfPayment()
    {
        return $this->type_of_payment;
    }

    /**
     * @param mixed $AdministrativePrize
     */
    public function setAdministrativePrize($AdministrativePrize)
    {
        $this->AdministrativePrize = $AdministrativePrize;
    }

    /**
     * @param mixed $AdministrativePrize_note
     */
    public function setAdministrativePrizeNote($AdministrativePrize_note)
    {
        $this->AdministrativePrize_note = $AdministrativePrize_note;
    }

    /**
     * @param mixed $dbh
     */
    public function setDbh($dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param mixed $Defect
     */
    public function setDefect($Defect)
    {
        $this->Defect = $Defect;
    }

    /**
     * @param mixed $Defect_note
     */
    public function setDefectNote($Defect_note)
    {
        $this->Defect_note = $Defect_note;
    }

    /**
     * @param mixed $MobileCommunication
     */
//    public function setMobileCommunication($MobileCommunication)
//    {
//        $this->MobileCommunication = $MobileCommunication;
//    }

    /**
     * @param mixed $MobileCommunication_note
     */
//    public function setMobileCommunicationNote($MobileCommunication_note)
//    {
//        $this->MobileCommunication_note = $MobileCommunication_note;
//    }

    /**
     * @param mixed $NotLessThan
     */
    public function setNotLessThan($NotLessThan)
    {
        $this->NotLessThan = $NotLessThan;
    }

    /**
     * @param mixed $NotLessThan_note
     */
    public function setNotLessThanNote($NotLessThan_note)
    {
        $this->NotLessThan_note = $NotLessThan_note;
    }

    /**
     * @param mixed $Overtime
     */
    public function setOvertime($Overtime)
    {
        $this->Overtime = $Overtime;
    }

    /**
     * @param mixed $Overtime_note
     */
    public function setOvertimeNote($Overtime_note)
    {
        $this->Overtime_note = $Overtime_note;
    }

    /**
     * @param mixed $PrizeAnotherDepartment
     */
    public function setPrizeAnotherDepartment($PrizeAnotherDepartment)
    {
        $this->PrizeAnotherDepartment = $PrizeAnotherDepartment;
    }

    /**
     * @param mixed $PrizeAnotherDepartment_note
     */
    public function setPrizeAnotherDepartmentNote($PrizeAnotherDepartment_note)
    {
        $this->PrizeAnotherDepartment_note = $PrizeAnotherDepartment_note;
    }

    /**
     * @param mixed $Promotions
     */
    public function setPromotions($Promotions)
    {
        $this->Promotions = $Promotions;
    }

    /**
     * @param mixed $Promotions_note
     */
    public function setPromotionsNote($Promotions_note)
    {
        $this->Promotions_note = $Promotions_note;
    }

    /**
     * @param mixed $Sunday
     */
    public function setSunday($Sunday)
    {
        $this->Sunday = $Sunday;
    }

    /**
     * @param mixed $Sunday_note
     */
    public function setSundayNote($Sunday_note)
    {
        $this->Sunday_note = $Sunday_note;
    }

    /**
     * @param mixed $Training
     */
    public function setTraining($Training)
    {
        $this->Training = $Training;
    }

    /**
     * @param mixed $Training_note
     */
    public function setTrainingNote($Training_note)
    {
        $this->Training_note = $Training_note;
    }

    /**
     * @param mixed $CompensationFAL
     */
    public function setCompensationFAL($CompensationFAL)
    {
        $this->CompensationFAL = $CompensationFAL;
    }

    /**
     * @param mixed $CompensationFAL_note
     */
    public function setCompensationFALNote($CompensationFAL_note)
    {
        $this->CompensationFAL_note = $CompensationFAL_note;
    }

    /**
     * @return mixed
     */
    public function getAdministrativePrize()
    {
        return $this->AdministrativePrize;
    }

    /**
     * @return mixed
     */
    public function getAdministrativePrizeNote()
    {
        return $this->AdministrativePrize_note;
    }

    /**
     * @return mixed
     */
    public function getDbh()
    {
        return $this->dbh;
    }

    /**
     * @return mixed
     */
    public function getDefect()
    {
        return $this->Defect;
    }

    /**
     * @return mixed
     */
    public function getDefectNote()
    {
        return $this->Defect_note;
    }

    /**
     * @return mixed
     */
//    public function getMobileCommunication()
//    {
//        return $this->MobileCommunication;
//    }

    /**
     * @return mixed
     */
//    public function getMobileCommunicationNote()
//    {
//        return $this->MobileCommunication_note;
//    }

    /**
     * @return mixed
     */
    public function getNotLessThan()
    {
        return $this->NotLessThan;
    }

    /**
     * @return mixed
     */
    public function getNotLessThanNote()
    {
        return $this->NotLessThan_note;
    }

    /**
     * @return mixed
     */
    public function getOvertime()
    {
        return $this->Overtime;
    }

    /**
     * @return mixed
     */
    public function getOvertimeNote()
    {
        return $this->Overtime_note;
    }

    /**
     * @return mixed
     */
    public function getPrizeAnotherDepartment()
    {
        return $this->PrizeAnotherDepartment;
    }

    /**
     * @return mixed
     */
    public function getPrizeAnotherDepartmentNote()
    {
        return $this->PrizeAnotherDepartment_note;
    }

    /**
     * @return mixed
     */
    public function getPromotions()
    {
        return $this->Promotions;
    }

    /**
     * @return mixed
     */
    public function getPromotionsNote()
    {
        return $this->Promotions_note;
    }

    /**
     * @return mixed
     */
    public function getSunday()
    {
        return $this->Sunday;
    }

    /**
     * @return mixed
     */
    public function getSundayNote()
    {
        return $this->Sunday_note;
    }

    /**
     * @return mixed
     */
    public function getTraining()
    {
        return $this->Training;
    }

    /**
     * @return mixed
     */
    public function getTrainingNote()
    {
        return $this->Training_note;
    }

    /**
     * @return mixed
     */
    public function getCompensationFAL()
    {
        return $this->CompensationFAL;
    }

    /**
     * @return mixed
     */
    public function getCompensationFALNote()
    {
        return $this->CompensationFAL_note;
    }

    /*
     * Получение данных по определенному типу, в зависимости от даты
     * Требуется для группового заведения данных
     */
    public function fetchAllByDate($date, $type)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        // Получаю бонус по проведенным акциям
        try {
            $AllData = array();

            if ($type==2) {
                $query = "SELECT member_id, AdministrativePrize as summ, Note as note FROM zp.member_AdministrativePrize where active=1";
                /*
                 * Подготавливаем запрос
                 */
                $sth = $this->dbh->prepare($query);
                /*
                 * Выполняю запрос
                 */
                $sth->execute();
                $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
                foreach ($raw as $item) {
                    $AllData[$item['member_id']] = $item;
                }
            }

            /*
             * Создаю SQL Запрос
             */
            $query = "select * from administrative_block where type=? and active=1 and type_of_payment=? and date=STR_TO_DATE(?,\"%m.%Y\")";
            /*
             * Подготавливаем запрос
             */
            $sth = $this->dbh->prepare($query);
            /*
             * Выполняю запрос
             */
            $sth->execute([$type, $this->type_of_payment, $date]);

            /*
             * Если количество строк > 0, то получаю данные из БД
             */
            if ($sth->rowCount() > 0) {
                /*
                 * Получаю все из БД
                 */
                $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
                /*
                 * Очищаю память
                 */
                $sth = null;



                foreach ($raw as $item) {
                    $AllData[$item['member_id']] = $item;
                }
                $raw = null;

                return $AllData;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }
        return NULL;
    }


    /*
    * Получение всех данных по административному блок при указанном ID пользователя и дате
    */
    public function fetch($id, $date)
    {
        $this->Promotions = NULL;
        $this->AdministrativePrize = NULL;
        $this->Training = NULL;
        $this->PrizeAnotherDepartment = NULL;
        $this->Defect = NULL;
        $this->Sunday = NULL;
        $this->NotLessThan = NULL;
//        $this->MobileCommunication = NULL;
//        $this->MobileCommunicationFormula = NULL;
        $this->Overtime = NULL;
//        $this->CompensationFAL = NULL;
//        $this->CompensationFALFormula = NULL;

        $this->Promotions_note = NULL;
        $this->AdministrativePrize_note = NULL;
        $this->Training_note = NULL;
        $this->PrizeAnotherDepartment_note = NULL;
        $this->Defect_note = NULL;
        $this->Sunday_note = NULL;
        $this->NotLessThan_note = NULL;
//        $this->MobileCommunication_note = NULL;
        $this->Overtime_note = NULL;
//        $this->CompensationFAL_note = NULL;

        $this->Promotions_lock = 0;
        $this->AdministrativePrize_lock = 0;
        $this->Training_lock = 0;
        $this->PrizeAnotherDepartment_lock = 0;
        $this->Defect_lock = 0;
        $this->Sunday_lock = 0;
        $this->NotLessThan_lock = 0;
//        $this->MobileCommunication_lock = 0;

        $this->Overtime_lock = 0;
//        $this->CompensationFAL_lock = 0;

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        // Получаю бонус по проведенным акциям
        try {
            if ($this->type_of_payment != 2) {
                $query = "SELECT member_id, AdministrativePrize as summ, Note as note FROM zp.member_AdministrativePrize where member_id=? and active=1";
                $sth = $this->dbh->prepare($query);
                $sth->execute([$id]);
                $raw = $sth->fetch(PDO::FETCH_ASSOC);
                if ($sth->rowCount() > 0) {
                    $this->AdministrativePrize = $raw['summ'];
                    $this->AdministrativePrize_note = $raw['note'];
                }

//                $query = "SELECT member_id, MobileCommunication as summ,formula FROM zp.member_MobileCommunication where member_id=? and active=1 LIMIT 1";
//                $sth = $this->dbh->prepare($query);
//                $sth->execute([$id]);
//                $raw = $sth->fetch(PDO::FETCH_ASSOC);
//                if ($sth->rowCount() > 0) {
//                    $this->MobileCommunication = $raw['summ'];
//                    $this->MobileCommunication_note = '';
//                }

//                $query = "SELECT member_id, summ,formula FROM zp.member_Transport where member_id=? and active=1 LIMIT 1";
//                $sth = $this->dbh->prepare($query);
//                $sth->execute([$id]);
//                $raw = $sth->fetch(PDO::FETCH_ASSOC);
//                if ($sth->rowCount() > 0) {
//                    $this->CompensationFAL = $raw['summ'];
//                    $this->CompensationFAL_note = '';
//                }
            }

            $query = "select * from administrative_block where member_id=? and active=1 and type_of_payment=? and date=STR_TO_DATE(?,\"%m.%Y\")";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id, $this->type_of_payment, $date]);

            if ($sth->rowCount() > 0) {
                foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $item) {
                    switch ($item['type']) {
                        case 1:
                            $this->Promotions = $item['summ'];
                            $this->Promotions_note = $item['note'];
                            $this->Promotions_lock = $item['lock'];
                            break;
                        case 2:
                            $this->AdministrativePrize = $item['summ'];
                            $this->AdministrativePrize_note = $item['note'];
                            $this->AdministrativePrize_lock = $item['lock'];
                            break;
                        case 3:
                            $this->Training = $item['summ'];
                            $this->Training_note = $item['note'];
                            $this->Training_lock = $item['lock'];
                            break;
                        case 4:
                            $this->PrizeAnotherDepartment = $item['summ'];
                            $this->PrizeAnotherDepartment_note = $item['note'];
                            $this->PrizeAnotherDepartment_lock = $item['lock'];
                            break;
                        case 5:
                            $this->Defect = $item['summ'];
                            $this->Defect_note = $item['note'];
                            $this->Defect_lock = $item['lock'];
                            break;
                        case 6:
                            $this->Sunday = $item['summ'];
                            $this->Sunday_note = $item['note'];
                            $this->Sunday_lock = $item['lock'];
                            break;
                        case 7:
                            $this->NotLessThan = $item['summ'];
                            $this->NotLessThan_note = $item['note'];
                            $this->NotLessThan_lock = $item['lock'];
                            break;
                        case 8:
//                            $this->MobileCommunication = $item['summ'];
//                            $this->MobileCommunication_note = $item['note'];
//                            $this->MobileCommunication_lock = $item['lock'];
                            break;
                        case 9:
                            $this->Overtime = $item['summ'];
                            $this->Overtime_note = $item['note'];
                            $this->Overtime_lock = $item['lock'];
                            break;
                        case 10:
//                            $this->CompensationFAL = $item['summ'];
//                            $this->CompensationFAL_note = $item['note'];
//                            $this->CompensationFAL_lock = $item['lock'];
                            break;
                    }
                }
            } else {
                /*
                 * Если выставлена тип выплаты в премию, то проверяю есть ли в авансе
                 */
                if ($this->type_of_payment == 1) { // Если тип выплаты 1(премия)
                    $query = "select * from administrative_block where member_id=? and active=1 and type_of_payment=2 and date=STR_TO_DATE(?,\"%m.%Y\") LIMIT 1";
                    $sth_avans = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth_avans->execute([$id, $date]);
                    if ($sth_avans->rowCount() > 0) {
                        foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $item) {
                            switch ($item['type']) {
                                case 1:
                                    $this->Promotions = $item['summ'];
                                    $this->Promotions_note = $item['note'];
                                    break;
                                case 2:
                                    $this->AdministrativePrize = $item['summ'];
                                    $this->AdministrativePrize_note = $item['note'];
                                    break;
                                case 3:
                                    $this->Training = $item['summ'];
                                    $this->Training_note = $item['note'];
                                    break;
                                case 4:
                                    $this->PrizeAnotherDepartment = $item['summ'];
                                    $this->PrizeAnotherDepartment_note = $item['note'];
                                    break;
                                case 5:
                                    $this->Defect = $item['summ'];
                                    $this->Defect_note = $item['note'];
                                    break;
                                case 6:
                                    $this->Sunday = $item['summ'];
                                    $this->Sunday_note = $item['note'];
                                    break;
                                case 7:
                                    $this->NotLessThan = $item['summ'];
                                    $this->NotLessThan_note = $item['note'];
                                    break;
                                case 8:
//                                    $this->MobileCommunication = $item['summ'];
//                                    $this->MobileCommunication_note = $item['note'];
                                    break;
                                case 9:
                                    $this->Overtime = $item['summ'];
                                    $this->Overtime_note = $item['note'];
                                    break;
                                case 10:
//                                    $this->CompensationFAL = $item['summ'];
//                                    $this->CompensationFAL_note = $item['note'];
                                    break;
                            }
                        }
                    }
                    $sth_avans = null;
                    $raw_avans = null;
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }
    }

    public function fetchFromPayout($id,$date) {
        $this->Promotions = NULL;
        $this->AdministrativePrize = NULL;
        $this->Training = NULL;
        $this->PrizeAnotherDepartment = NULL;
        $this->Defect = NULL;
        $this->Sunday = NULL;
        $this->NotLessThan = NULL;
//        $this->MobileCommunication = NULL;
        $this->Overtime = NULL;
//        $this->CompensationFAL = NULL;

        $this->Promotions_note = NULL;
        $this->AdministrativePrize_note = NULL;
        $this->Training_note = NULL;
        $this->PrizeAnotherDepartment_note = NULL;
        $this->Defect_note = NULL;
        $this->Sunday_note = NULL;
        $this->NotLessThan_note = NULL;
//        $this->MobileCommunication_note = NULL;
        $this->Overtime_note = NULL;
//        $this->CompensationFAL_note = NULL;


        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        // Получаю бонус по проведенным акциям
        try {
            $query = "select AdministrativeData from financial_payout_ext where member_id=? and type_of_payment=1 and date=STR_TO_DATE(?,\"%m.%Y\") LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id, $date]);

            $raw = current($sth->fetchAll(PDO::FETCH_ASSOC));
            if ($raw) {
                foreach (unserialize($raw["AdministrativeData"]) as $key=>$item) {
                    $this->$key = $item;
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }
    }

    /*
    * Добавление данных в БД
    * Необходимые параметры: ID пользователя, дата, тип, сумма, заметка.
    */
    public function addByType($id, $date, $type, $summ, $note,$changer_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if (is_null($this->type_of_payment)) {
            throw new Exception("Не выставлен тип выплаты!");
        }

        $dateNow = new DateTime();

        $query = "SELECT * FROM administrative_block where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and administrative_block.lock=1 and type_of_payment=?";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$type, $id, $date, $this->type_of_payment]);
        } catch (PDOException $e) {
            throw new Exception("Не могу выполнить зарос к БД" . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            throw new Exception("Запись заблокирована, изменение невозможно");
            return false;
        }

        $query = "SELECT * FROM administrative_block where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1 and type_of_payment=?";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$type, $id, $date, $this->type_of_payment]);
        } catch (PDOException $e) {
            throw new Exception("Не могу выполнить зарос к БД" . $e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $query = "update administrative_block SET active=0 where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1 and administrative_block.lock=0 and type_of_payment=?";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$type, $id, $date, $this->type_of_payment]);
            } catch (PDOException $e) {
                throw new Exception("Ошибка в PDO. Не могу выполнить запрос добавления в БД administrative_block. " . $e->getMessage());
            }

            if ($sth->rowCount() == 0) {
                throw new Exception('Не могу обновить строки: ' . $sth->errorInfo()[2]);
            }
        }

        $query = "insert into administrative_block (member_id, date, summ, note, active, change_date,type_of_payment, type,changer_id) VALUES (?, STR_TO_DATE(?, '%m.%Y'), ?, ?, 1, NOW(),?,?,?)";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id, $date, $summ, $note, $this->type_of_payment, $type, $changer_id]);
        } catch (PDOException $e) {
            throw new Exception('Не могу добавить в БД administrative_block.' . $e->getMessage());
        }

        # Если мы меняем в аванс2 в декабре месяце, то должны добавить/обновить в премию значения!
        if ($dateNow->format("m") == 12 && $this->type_of_payment == 3) {
            # Ищу активные записи по в премии
            $query = "SELECT * FROM administrative_block where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1 and type_of_payment=1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$type, $id, $date]);
            } catch (PDOException $e) {
                throw new Exception("Не могу выполнить зарос к БД" . $e->getMessage());
            }

            # Если запись существует, то ставлю active=0
            if ($sth->rowCount() > 0) {
                $query = "update administrative_block SET active=0 where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1 and administrative_block.lock=0 and type_of_payment=1";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$type, $id, $date]);
                } catch (PDOException $e) {
                    throw new Exception("Ошибка в PDO. Не могу выполнить запрос добавления в БД administrative_block. " . $e->getMessage());
                }
            }
            # Добавляю новые значения
            $query = "insert into administrative_block (member_id, date, summ, note, active, change_date,type_of_payment, type,changer_id) VALUES (?, STR_TO_DATE(?, '%m.%Y'), ?, ?, 1, NOW(),?,?,?)";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$id, $date, $summ, $note, 1, $type, $changer_id]);
            } catch (PDOException $e) {
                throw new Exception('Не могу добавить в БД administrative_block.' . $e->getMessage());
            }
        }

        switch ($type) {
            case 1:
                $this->Promotions = $summ;
                $this->Promotions_note = $note;
                break;
            case 2:
                $this->AdministrativePrize = $summ;
                $this->AdministrativePrize_note = $note;
                break;
            case 3:
                $this->Training = $summ;
                $this->Training_note = $note;
                break;
            case 4:
                $this->PrizeAnotherDepartment = $summ;
                $this->PrizeAnotherDepartment_note = $note;
                break;
            case 5:
                $this->Defect = $summ;
                $this->Defect_note = $note;
                break;
            case 6:
                $this->Sunday = $summ;
                $this->Sunday_note = $note;
                break;
            case 7:
                $this->NotLessThan = $summ;
                $this->NotLessThan_note = $note;
                break;
            case 8:
//                $this->MobileCommunication = $summ;
//                $this->MobileCommunication_note = $note;
                break;
            case 9:
                $this->Overtime = $summ;
                $this->Overtime_note = $note;
                break;
            case 10:
//                $this->CompensationFAL = $summ;
//                $this->CompensationFAL_note = $note;
                break;
        }
    }

    /*
     * Получение суммы всех переменных классов
     */
    public function getSumm()
    {
        return
            $this->Overtime + $this->NotLessThan + $this->Sunday +
            $this->Defect + $this->PrizeAnotherDepartment + $this->AdministrativePrize +
            $this->Promotions + $this->Training;
    }

    /*
     *  Нужно для проверки на нулевые значения в ведомости
     */
    public function checkNull()
    {

        if (
            is_null($this->Promotions) &&
            is_null($this->AdministrativePrize) &&
            is_null($this->Training) &&
            is_null($this->PrizeAnotherDepartment) &&
            is_null($this->Defect) &&
            is_null($this->Sunday) &&
            is_null($this->NotLessThan) &&
            is_null($this->Overtime)
        ) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Установка Административной премии на пользователя
     */
    public function setAdministrativePrizeByDate($id, $date)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        // Привожу дату в нормальный вид со днем!
        $date = "01.".$date;

        /*
         * Формируем строку запроса к таблице member_AdministrativePrize,
         * где указываем id пользователя и что нужны строки которые от 0000-00-00 до такушей даты и date_s от заданной даты до конца периода,
         * сортируем до id и выводим одну строку более позднюю
         */
//        $query = "SELECT * FROM member_AdministrativePrize where member_id=? AND
//        (date_s > '0000-00-00' AND date_s <= STR_TO_DATE(?,'%d.%m.%Y')) AND
//        (date_e > STR_TO_DATE(?,'%d.%m.%Y') AND  date_e <= '9999-01-01')
//        order by id desc LIMIT 1";

        $query = "SELECT * FROM member_AdministrativePrize where 
        member_id=? and 
        (date_s BETWEEN '0000-00-00' AND DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'),INTERVAL 1 MONTH)) and (date_e BETWEEN DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'),INTERVAL 1 MONTH) AND '9999-01-01') 
        order by id desc LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id, $date, $date]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
//            var_dump($raw);
            $this->AdministrativePrize = $raw['AdministrativePrize'];
            $this->AdministrativePrize_note = $raw['Note'];
        };
    }

    public function setAdministrativePrizeFin($id)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }
        /*
         * Формируем строку запроса к таблице member_AdministrativePrize,
         * где указываем id пользователя и что нужны строки которые от 0000-00-00 до такушей даты и date_s от заданной даты до конца периода,
         * сортируем до id и выводим одну строку более позднюю
         */
        $query = "SELECT * FROM member_AdministrativePrize where 
        member_id=? and 
        active = 1 
        order by id desc LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
//            var_dump($raw);
            $this->AdministrativePrize = $raw['AdministrativePrize'];
            $this->AdministrativePrize_note = $raw['Note'];
        };
    }

    /*
     * Установка премии за мобильную связь на пользователя
     */
//    public function setMobileCommunicationByDate($id, $date)
//    {
//        if ($this->dbh == null) { // Если нету подключения, то
//            $this->dbh = dbConnect(); // Подключаемся
//        }
//
//        $date = "01.".$date;
//
//        // Формируем строку запроса к таблице member_MobileCommunication, где указываем id пользователя и что нужны строки которые от 0000-00-00 до такушей даты и date_s от заданной даты до конца периода, сортируем до id и выводим одну строку более позднюю
//        $query = "SELECT * FROM member_MobileCommunication where
//        member_id=? and
//        (date_s BETWEEN '0000-00-00' AND DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'),INTERVAL 1 MONTH) ) and (date_e BETWEEN DATE_ADD(STR_TO_DATE(?,'%d.%m.%Y'),INTERVAL 1 MONTH) AND '9999-01-01')
//        order by id desc LIMIT 1";
//
////        $query = "SELECT * FROM member_MobileCommunication where
////        member_id=? AND
////        (date_s > '0000-00-00' AND date_s <= STR_TO_DATE(?,'%m.%Y')) AND
////        (date_e > STR_TO_DATE(?,'%m.%Y') AND  date_e <= '9999-01-01')
////        order by id desc LIMIT 1";
//        try {
//            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
//            $sth->execute([$id, $date, $date]); // Выполняем запрос
//        } catch (PDOException $e) {
//            throw new Exception('Ошибка в PDO ' . $e->getMessage());
//        }
//
//        if ($sth->rowCount() > 0) { // Если число строк > 0
//            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
//            $this->MobileCommunication = $raw['MobileCommunication'];
//        };
//    }

//    public function setMobileCommunicationFin($id)
//    {
//        if ($this->dbh == null) { // Если нету подключения, то
//            $this->dbh = dbConnect(); // Подключаемся
//        }
//
//        // Формируем строку запроса к таблице member_MobileCommunication, где указываем id пользователя и что нужны строки которые от 0000-00-00 до такушей даты и date_s от заданной даты до конца периода, сортируем до id и выводим одну строку более позднюю
//        $query = "SELECT * FROM member_MobileCommunication where
//        member_id=? and active = 1
//        order by id desc LIMIT 1";
//
////        $query = "SELECT * FROM member_MobileCommunication where
////        member_id=? AND
////        (date_s > '0000-00-00' AND date_s <= STR_TO_DATE(?,'%m.%Y')) AND
////        (date_e > STR_TO_DATE(?,'%m.%Y') AND  date_e <= '9999-01-01')
////        order by id desc LIMIT 1";
//        try {
//            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
//            $sth->execute([$id]); // Выполняем запрос
//        } catch (PDOException $e) {
//            throw new Exception('Ошибка в PDO ' . $e->getMessage());
//        }
//
//        if ($sth->rowCount() > 0) { // Если число строк > 0
//            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
//            $this->MobileCommunication = $raw['MobileCommunication'];
//            $this->MobileCommunicationFormula = $raw['formula'];
//        };
//    }

//    public function setCompensationFALFin($id)
//    {
//        if ($this->dbh == null) { // Если нету подключения, то
//            $this->dbh = dbConnect(); // Подключаемся
//        }
//
//        // Формируем строку запроса к таблице member_MobileCommunication, где указываем id пользователя и что нужны строки которые от 0000-00-00 до такушей даты и date_s от заданной даты до конца периода, сортируем до id и выводим одну строку более позднюю
//        $query = "SELECT * FROM member_Transport where
//        member_id=? and active = 1
//        order by id desc LIMIT 1";
//
//        try {
//            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
//            $sth->execute([$id]); // Выполняем запрос
//        } catch (PDOException $e) {
//            throw new Exception('Ошибка в PDO ' . $e->getMessage());
//        }
//
//        if ($sth->rowCount() > 0) { // Если число строк > 0
//            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
//            $this->CompensationFAL = $raw['summ'];
//            $this->CompensationFALFormula = $raw['summ'];
//        };
//    }

//    public function recountCompensationFAL($otsutstviya, $work_days) {
//        $this->CompensationFAL = round(($this->CompensationFAL / $work_days * ($work_days-$otsutstviya)),2);
//    }

//    public function recountMobile($otsutstviya, $work_days) {
//        $this->MobileCommunication = round(($this->MobileCommunication / $work_days * ($work_days-$otsutstviya)),2);
//    }

    public function getAllData(){
        $data = array();
        $data[] = array("summ"=>$this->Promotions,"note"=>$this->Promotions_note);
        $data[] = array("summ"=>$this->AdministrativePrize,"note"=>$this->AdministrativePrize_note);
        $data[] = array("summ"=>$this->Training,"note"=>$this->Training_note);
        $data[] = array("summ"=>$this->PrizeAnotherDepartment,"note"=>$this->PrizeAnotherDepartment_note);
        $data[] = array("summ"=>$this->Defect,"note"=>$this->Defect_note);
        $data[] = array("summ"=>$this->Sunday,"note"=>$this->Sunday_note);
        $data[] = array("summ"=>$this->NotLessThan,"note"=>$this->NotLessThan_note);
//        $data[] = array("summ"=>$this->MobileCommunication,"note"=>$this->MobileCommunication_note);
        $data[] = array("summ"=>$this->Overtime,"note"=>$this->Overtime_note);
//        $data[] = array("summ"=>$this->CompensationFAL,"note"=> $this->CompensationFAL_note);

        return $data;
    }
}
