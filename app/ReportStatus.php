<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.10.18
 * Time: 12:34
 */

require_once 'functions.php';
require_once "admin/RoleInterface.php";
require_once "DepartmentInterface.php";
require_once "Notify.php";

class ReportStatus
{
    private $dbh;

    public function getStatusList()
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT * FROM status";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute(); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }
        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
            return $raw;
        } else {
            return NULL;
        }
    }

    /*
     * Получить текущий статус
     */
    public function getStatus($date, $type_of_payment)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

//        $query = "SELECT status_id FROM report_status where type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y') LIMIT 1";
        $query = "SELECT MIN(t1.status_id) as status_id,t2.name FROM zp.report_status as t1 LEFT JOIN status as t2 ON(t1.status_id = t2.id) where t1.type_of_payment=? and t1.date=STR_TO_DATE(?,'%m.%Y') LIMIT 1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$type_of_payment, $date]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }
        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
            return $raw;
        } else {
            return array("status_id" => 0, "name" => "Нету данных");
        }
    }

    public function getFiltredStatus($date, $type_of_payment, $filter)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT t1.status_id as status_id,t2.name 
        FROM zp.report_status as t1 
        LEFT JOIN status as t2 ON(t1.status_id = t2.id) 
        where 
          t1.type_of_payment=? and 
          t1.date=STR_TO_DATE(?,'%m.%Y') and 
          find_in_set(cast(t1.department_id  as char), ?) 
        order by t1.status_id LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$type_of_payment, $date, $filter]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
            return $raw;
        } else {
            return array("status_id" => 0, "name" => "Нету данных");
        }
    }

    /*
     * Статус Расчет, от имени пользователя
     */
    public function getStatusMemberLeaving($date, $member_id)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

//        $query = "SELECT status_id FROM report_status where type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y') LIMIT 1";
        $query = "SELECT t1.status_id,t2.name FROM zp.report_status as t1 LEFT JOIN status as t2 ON(t1.status_id = t2.id) where t1.type_of_payment=4 and t1.date=STR_TO_DATE(?,'%m.%Y') 
        and t1.member_id=? and active = 1
        LIMIT 1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date, $member_id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }
        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
            return $raw;
        } else {
            return array("status_id" => 1, "name" => "Внесение данных");
        }
    }

    /*
     * Статус внеплановой выплаты, от имени пользователя
     */
    public function getStatusMember($date, $type_of_payment, $member_id, $id)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

//        $query = "SELECT status_id FROM report_status where type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y') LIMIT 1";
        $query = "SELECT t1.status_id,t2.name FROM zp.report_status as t1 LEFT JOIN status as t2 ON(t1.status_id = t2.id) where t1.type_of_payment=? and t1.date=STR_TO_DATE(?,'%m.%Y') 
        and t1.member_id=? and t1.unscheduled_id = ? 
        LIMIT 1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$type_of_payment, $date, $member_id, $id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }
        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
            return $raw;
        } else {
            return array("status_id" => 1, "name" => "Внесение данных");
        }
    }

    public function getStatusByID($ID) {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT t1.status_id,t2.name FROM zp.report_status as t1 
        LEFT JOIN status as t2 ON(t1.status_id = t2.id) 
        where t1.id=?
        LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$ID]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }
        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
            return $raw;
        } else {
            return null;
        }
    }

    /*
     * Изменение статуса внеплановой выплаты по ID
     */
    public function ChangeMemberStatusByID($ID, $status) {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "select t1.id as report_status_id, t1.member_id, t1.unscheduled_id, t1.status_id, t1.active, t2.date, t2.summ, t2.note
                  from report_status as t1
                  LEFT JOIN unscheduled as t2 ON (t1.unscheduled_id = t2.id)
                  where t2.id=? LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$ID]); // Выполняю запрос
        } catch (PDOException $e) {
            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
        }

        if ($sth->rowCount() > 0) {
            $Status_ID = $sth->fetch()['report_status_id'];

            $query = "UPDATE report_status SET status_id=? where id=?";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$status, $Status_ID]); // Выполняю запрос
            } catch (PDOException $e) {
                echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
            }
        }

        return true;
    }

    /*
     * Статус внеплановой ведомости по ID
     */
    public function getMemberStatusById($ID) {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "select t1.id as status_id, t1.member_id, t1.unscheduled_id, t1.status_id, t1.active, t2.date, t2.summ, t2.note
                  from report_status as t1
                  LEFT JOIN unscheduled as t2 ON (t1.unscheduled_id = t2.id)
                  where t2.id=? LIMIT 1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$ID]); // Выполняю запрос
        } catch (PDOException $e) {
            echo 'Ошибка в PDO' . $e->getMessage() . "<BR>";
        }

        if ($sth->rowCount() > 0) {
            $raw = $sth->fetch();
            return $raw ['status_id'];
        }

        return false;
    }

    public function InitReportStatus($date,$type_of_payment, $ChangeMember=-2) {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        /*
         * Иначе, добавляем новую строку с статусом в основную таблицу
         */
        $DI = new DepartmentInterface();
        $DI->fetchActiveDepartments();
        $Departments = $DI->GetDepartments();

        foreach ($Departments as $department) {
            $query = "SELECT * FROM report_status as t1 where t1.type_of_payment=? and t1.date=STR_TO_DATE(?,'%m.%Y') and department_id=?";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$type_of_payment, $date, $department->getId()]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO ' . $e->getMessage());
            }

            if ($sth->rowCount() > 0) {
                echo "Ведомость уже инициализирована<BR>";
            } else {
                $query = "INSERT INTO report_status (date, type_of_payment, department_id, status_id, change_date, change_member) VALUES (STR_TO_DATE(?, '%m.%Y'), ?, ?, ?, NOW(), ?)";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$date, $type_of_payment, $department->getId(), 1, $ChangeMember]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }
            }
        }
    }

    /*
     * Изменть статус ведомости
     */
    public function ChangeStatus($date, $type_of_payment, $status, $change_id)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT * FROM report_status as t1 where t1.type_of_payment=? and t1.date=STR_TO_DATE(?,'%m.%Y')";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$type_of_payment, $date]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        /*
         * Если количество строк > 0, то выполняем обноление статуса
         */
        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
            foreach ($raw as $item) {
                $query = "UPDATE report_status SET status_id=?, change_date=NOW(), change_member=? where date=STR_TO_DATE(?, '%m.%Y') AND type_of_payment=? and department_id=?";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$status, $change_id, $date, $type_of_payment, $item['department_id']]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }
            }

        } else {
            /*
             * Иначе, добавляем новую строку с статусом в основную таблицу
             */

            $DI = new DepartmentInterface();
            $DI->fetchActiveDepartments();
            $Departments = $DI->GetDepartments();

            foreach ($Departments as $department) {
                $query = "INSERT INTO report_status (date, type_of_payment, department_id, status_id, change_date, change_member) VALUES (STR_TO_DATE(?, '%m.%Y'), ?, ?, ?, NOW(), ?)";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$date, $type_of_payment, $department->getId(), 1, $change_id]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }
            }

        }

        /*
         * Уведомление пользователей
         */
        $NF = new Notify();
//        $members_array = $RI->getMemberListStatus($status);

//        if ($status > $OldStatus) {
//            switch ($status) {
//                case 1:
//                    $subj = "Стадия 1";
//                    $msg = "Данные добавлены";
//                    break;
//                case 2:
//                    $subj = "Стадия 2";
//                    $msg = "Данные руководителями заполены";
//                    break;
//                case 3:
//                    $subj = "Стадия 3";
//                    $msg = "Отвественные директора проверку закончили";
//                    break;
//                case 4:
//                    $subj = "Стадия 4";
//                    $msg = "Ведомость передана кассиру";
//                    break;
//                case 5:
//                    $subj = "Стадия 5";
//                    $msg = "Выдано";
//                    break;
//                case 6:
//                    $subj = "Стадия 6";
//                    $msg = "Ведомость закрыта";
//                    break;
//            }
//
////            foreach ($members_array as $item) {
////                if (!is_null($item['member_id'])) {
////                    $NF->addNotify($item['member_id'], $subj, $msg);
////                }
////            }
//        }
    }

    /*
     * Изменение статуса при внеплановой выплате.
     */
    public function ChangeStatusMember($member_id, $date, $type_of_payment, $status, $id)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        if ($type_of_payment == 4) {
            $query = "SELECT * FROM report_status as t1 where t1.type_of_payment=? and t1.date=STR_TO_DATE(?,'%m.%Y') and member_id=? LIMIT 1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$type_of_payment, $date, $member_id]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO ' . $e->getMessage());
            }

            if ($sth->rowCount() > 0) { // Если число строк > 0
                $query = "UPDATE report_status SET status_id=? where date=STR_TO_DATE(?, '%m.%Y') AND type_of_payment=? and member_id=?";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$status, $date, $type_of_payment, $member_id]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }
            } else {
                $query = "INSERT INTO report_status (date, type_of_payment, status_id, member_id, unscheduled_id) VALUES (STR_TO_DATE(?, '%m.%Y'),?,?,?,?)";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$date, $type_of_payment, $status, $member_id, NULL]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }
            }
        } else {
            $query = "SELECT * FROM report_status as t1 where t1.type_of_payment=? and t1.date=STR_TO_DATE(?,'%m.%Y') and member_id=? and unscheduled_id=? LIMIT 1";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$type_of_payment, $date, $member_id, $id]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO ' . $e->getMessage());
            }

            if ($sth->rowCount() > 0) { // Если число строк > 0
                $query = "UPDATE report_status SET status_id=? where date=STR_TO_DATE(?, '%m.%Y') AND type_of_payment=? and member_id=? and unscheduled_id=?";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$status, $date, $type_of_payment, $member_id, $id]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }
            } else {
                $query = "INSERT INTO report_status (date, type_of_payment, status_id, member_id, unscheduled_id) VALUES (STR_TO_DATE(?, '%m.%Y'),?,?,?,?)";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$date, $type_of_payment, $status, $member_id, $id]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }
            }
        }
    }

    /*
     * Изменение статуса при расчете сотрудника
     */
    public function ChangeStatusLeavingByID($id,$status)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT * FROM report_status as t1 where id=? LIMIT 1";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) { // Если число строк > 0
            $query = "UPDATE report_status SET status_id=? where id=?";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$status, $id]); // Выполняем запрос
            } catch (PDOException $e) {
                throw new Exception('Ошибка в PDO ' . $e->getMessage());
            }
        }

        return false;
    }


    /*
     * Получение журнала со всеми статусами за текущий год
     */
    public function getJournal($date=null)
    {
        if (is_null($date)) {
            $date = date("Y");
        }

        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT DATE_FORMAT(t1.date,\"%m.%Y\") as date,DATE_FORMAT(t1.date, '%m') as date_month,t1.type_of_payment, t3.name, MIN(t1.status_id) as status_id, t2.name as status_name FROM report_status as t1 
        LEFT JOIN status as t2 ON(t1.status_id = t2.id) 
        LEFT JOIN payment as t3 ON(t1.type_of_payment = t3.id) 
        where t1.date>STR_TO_DATE(?,'%Y') group by t1.date, t1.type_of_payment";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }
        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
            return $raw;
        };
    }

    /*
     * Получение статуса Кассира
     */
    public function getCatcherStatus()
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT *,DATE_FORMAT(date,\"%m.%Y\") as date FROM zp.report_status where status_id=4 and (type_of_payment = 1  or type_of_payment = 2 or type_of_payment = 3) LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute(); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
            return $raw;
        };

        return NULL;
    }

    /*
     * Получение статуса Кассира
     */
    public function getCatcherPaymentStatus()
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }

        $query = "SELECT *,DATE_FORMAT(date,\"%m.%Y\") as date FROM zp.report_status where status_id=4 and (type_of_payment = 4  or type_of_payment = 5) and active = 1 LIMIT 1";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute(); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        if ($sth->rowCount() > 0) { // Если число строк > 0
            $raw = $sth->fetch(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
            return $raw;
        };

        return NULL;
    }

    /*
     * Проверка статуса по дате и типу выплаты
     * При типе 1 (премия) проверяю есть ли данные в data_manager, member_awh, member_ndfl, за текущую дату
     * при типе 2 (аванса) проверяю данные member_awh
     * при типе 2 (аванса) проверяю данные member_awh и member_ndfl
     * Возвращаю true или false в зависимости от наличия данных, в дальнейшем это используется при добавлении строки статуса в БД
     */
    public function checkStatus($date, $type_of_payments)
    {
        if ($this->dbh == null) { // Если нету подключения, то
            $this->dbh = dbConnect(); // Подключаемся
        }


        switch ($type_of_payments) {
            case 1:
                $data = false;
                $awh = false;
                $ndfl = false;

                $query = "select * from data_manager where date=STR_TO_DATE(?, '%m.%Y') LIMIT 1";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$date]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }

                if ($sth->rowCount() > 0) {
                    $data = true;
                }

                $query = "select * from member_awh where date=STR_TO_DATE(?, '%m.%Y') and type_of_payment=? LIMIT 1";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$date, $type_of_payments]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }

                if ($sth->rowCount() > 0) {
                    $awh = true;
                }

                $query = "select * from member_ndfl where date=STR_TO_DATE(?, '%m.%Y') LIMIT 1";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$date]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }

                if ($sth->rowCount() > 0) {
                    $ndfl = true;
                }

                if ($ndfl && $data && $awh) {
                    return true;
                } else {
                    return false;
                }

                break;
            case 2:
                $query = "select * from member_awh where date=STR_TO_DATE(?, '%m.%Y') and type_of_payment=? LIMIT 1";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$date, $type_of_payments]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }

                if ($sth->rowCount() > 0) {
                    return true;
                }

                return false;
                break;
            case 3:
                $awh = false;
                $ndfl = false;

                $query = "select * from member_awh where date=STR_TO_DATE(?, '%m.%Y') and type_of_payment=1 LIMIT 1";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$date]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }

                if ($sth->rowCount() > 0) {
                    $awh = true;
                }

                $query = "select * from member_ndfl where date=STR_TO_DATE(?, '%m.%Y') LIMIT 1";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$date]); // Выполняем запрос
                } catch (PDOException $e) {
                    throw new Exception('Ошибка в PDO ' . $e->getMessage());
                }

                if ($sth->rowCount() > 0) {
                    $ndfl = true;
                }

                if ($ndfl && $awh) {
                    return true;
                } else {
                    return false;
                }

                break;
        }
        return false;
    }

    /*
     * Кто тупит и на каком этапе
     */
    public function whoBrake($date, $type_of_payment)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT t3.id, t3.name, t1.status_id, t2.name as status_name, t1.change_date, t1.change_member, CONCAT(t4.lastname,\" \", t4.name,\" \", t4.middle) as change_member_name
              FROM report_status as t1
              LEFT JOIN status as t2 on (t1.status_id = t2.id)
              LEFT JOIN department as t3 on (t3.id = t1.department_id)
              LEFT JOIN member as t4 on (t4.id = t1.change_member)
              WHERE 
                  t1.date=STR_TO_DATE(?,'%m.%Y') AND
                  t1.type_of_payment=?
                  order by t3.name";

        $sth = $this->dbh->prepare($query);
        $sth->execute([$date, $type_of_payment]);
        if ($sth->rowCount() > 0) {
            $raw = $sth->fetchAll(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
            return $raw;
        } else {
            $query = 'select t1.id,
                        t1.name,
                        (select 0) as status_id,
                        (select "нет данных") as status_name,
                        CURDATE() as change_date,
                        (select -2) as change_member,
                        (select NULL) as change_member_name
                    from department as t1
                    where t1.active=1';
            $sth = $this->dbh->prepare($query);
            $sth->execute([$date, $type_of_payment]);
            if ($sth->rowCount() > 0) {
                $raw = $sth->fetchAll(PDO::FETCH_ASSOC); // возвращаем извлеченные строки
                return $raw;
            }
        }

        return false;

    }

    /*
     * Была ли нажата кнопка следующей стадии до этого по отделу
     */
    public function inChecked($date, $type_of_payments, $department_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }


        /*
         * Если был передан 0 в отделе, то мы считаем что это все отделы, проверяем есть ли строки в которых не поставили check
         */
        if ($department_id == 0) {
            $query = "
            SELECT t1.id,t1.date,t1.status_id,t1.active,t2.department_id,t2.check
            FROM report_status  as t1
            LEFT JOIN report_substatus as t2 ON (t1.id = t2.report_status_id AND t1.status_id = t2.status)
            WHERE 
	            t1.date=STR_TO_DATE(?,'%m.%Y') AND
                t1.type_of_payment=? AND
                t2.check = 0
            ";

            $sth = $this->dbh->prepare($query);
            $sth->execute([$date, $type_of_payments]);
            if ($sth->rowCount() == 0) {
                /*
                 * Если количество строк == 0, то или нету доступа, или кнопка уже нажата
                 */
                return false;
            } else {
                /*
                 * Если вернулись строки, значит еще не прожата кнопка check
                 */
                return true;
            }

        } else {
            $query = "
            SELECT t1.id,t1.date,t1.status_id,t1.active,t2.department_id,t2.check
            FROM report_status  as t1
            LEFT JOIN report_substatus as t2 ON (t1.id = t2.report_status_id AND t1.status_id = t2.status)
            WHERE 
	            t1.date=STR_TO_DATE(?,'%m.%Y') AND
                t1.type_of_payment=? AND
                t2.department_id=? AND 
                t2.check = 0
            LIMIT 1
            ";

            $sth = $this->dbh->prepare($query);
            $sth->execute([$date, $type_of_payments, $department_id]);
            if ($sth->rowCount() == 0) {
                /*
                 * Если количество строк == 0, то или нету доступа, или кнопка уже нажата
                 */
                return false;
            } else {
                /*
                 * Если вернулись строки, значит еще не прожата кнопка check
                 */
                return true;
            }
        }
    }


    /*
     * Изменение подстатуса
     */
    public function ChangeSubStatus($date, $type_of_payments, $department_id, $changer_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        /*
         * Если все отделы, то:
         * получаю ID и статус
         * Выполняю обновление ВСЕХ записией устанавливая check на 1
         */
        if ($department_id == 0) {

            $query = "select id, status_id from report_status where date=STR_TO_DATE(?,'%m.%Y') AND type_of_payment=?";
            $sth = $this->dbh->prepare($query);
            $sth->execute([$date, $type_of_payments]);

            if ($sth->rowCount() > 0) {
                $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
                foreach ($raw as $item) {
                    $id = $item['id']; // ID основой записи
                    $status = $item['status_id']; // Номер статуса
                    $status++;

                    $query = "UPDATE report_status SET status=?, change_member=?, change_date=NOW() where id=?";
                    $sth = $this->dbh->prepare($query);
                    $sth->execute([$status, $changer_id, $id]);

                    if (isset($sth->errorInfo()[3])) {
                        echo $sth->errorCode()[3]."<BR>";
                    }
                    if ($sth->rowCount() == 0) {
                        throw new Exception("Не могу обновить статус");
                    }
                }

                return true;
            } else {
                return false;
            }
        } else {
            /*
             * Если задан конкретный ID отдела, то обновляю только его
             */
            $query = "select id,status_id from report_status where date=STR_TO_DATE(?,'%m.%Y') AND type_of_payment=? AND department_id=? LIMIT 1";
            $sth = $this->dbh->prepare($query);
            $sth->execute([$date, $type_of_payments, $department_id]);

            if ($sth->rowCount() > 0) {
                $raw = $sth->fetch(PDO::FETCH_ASSOC);
                $id = $raw['id']; // ID основой записи
                $status = $raw['status_id']; // Номер статуса
                $status++;

                $query = "UPDATE report_status SET status_id=?, change_member=?, change_date=NOW() WHERE id=?";
                $sth = $this->dbh->prepare($query);
                $sth->execute([$status, $changer_id, $id]);

                if (isset($sth->errorInfo()[3])) {
                    echo $sth->errorCode()[3]."<BR>";
                }

                if ($sth->rowCount() == 0) {
                    return false;
                }

                return true;
            } else {
                return false;
            }
        }
    }

    /*
     * Проверяем все ли check в подстатусах  выставлены в 1, если да, то возвращаю true
     */
    public function CheckAllSubStatus($date, $type_of_payments)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select status_id from report_status where date=STR_TO_DAT(?, '%m.%Y') and type_of_payment=? group by status_id";

        /*
         * Подготовка запроса
         */
        $sth = $this->dbh->prepare($query);
        /*
         * Выполняю запрос
         */
        $sth->execute([$date, $type_of_payments]);

        if ($sth->rowCount()>1) {
            return false;
        };

        return true;
    }


    /*
     * Декрементируем подстатусы
     */
    public function BackSubStatus($date, $type_of_payments, $ids, $change_id) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        if (is_array($ids)) {
            foreach ($ids as $k) {
                $query = "select status_id from report_status where date=STR_TO_DATE(?,'%m.%Y') AND type_of_payment=? AND department_id=? LIMIT 1";
                $sth = $this->dbh->prepare($query);
                $sth->execute([$date, $type_of_payments, $k]);
                $status = $sth->fetch()['status_id'];
                if ($status != 1) {
                    $status--;
                    $query = "UPDATE report_status SET status_id=?, change_date=NOW(), change_member=? where date=STR_TO_DATE(?,'%m.%Y') AND type_of_payment=? AND department_id=?";
                    $sth = $this->dbh->prepare($query);
                    $sth->execute([$status, $change_id, $date, $type_of_payments, $k]);
                }

                if ($status == 1) {
                    $query = "DELETE FROM financial_payout where type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y') and department_id = ?";
                    $sth = $this->dbh->prepare($query);
                    $sth->execute([$type_of_payments, $date , $k]);
                }
            }
        } else {
            $query = "select id, status_id, department_id from report_status where date=STR_TO_DATE(?,'%m.%Y') AND type_of_payment=?";
            $sth = $this->dbh->prepare($query);
            $sth->execute([$date, $type_of_payments]);
            $raw = $sth->fetchAll();
            foreach ($raw as $item) {
                $status = $item['status_id'];
                if ($status != 1) {
                    $status--;
                    $query = "UPDATE report_status SET status_id=?, change_date=NOW(), change_member=? where id=?";
                    $sth = $this->dbh->prepare($query);
                    $sth->execute([$status, $change_id, $item['id']]);
                }
                if ($status == 1) {
                    $query = "DELETE FROM financial_payout where type_of_payment=? and date=STR_TO_DATE(?,'%m.%Y')";
                    $sth = $this->dbh->prepare($query);
                    $sth->execute([$type_of_payments, $date]);
                }
            }
        }
        return false;
    }

    /*
     * Метод требуется для определения есть находится ли ведомость на стадии НЛ
     * Если статус ведомости >= 3 и < 5(выплачено), то увольнять пользователя нельзя
     * Уволить может только НЛ
     */
    public static function getReportStatusLastMonth()
    {
        $dbh = dbConnect();
        $date = new DateTime();
        $date->modify("-1 month");

        $query = "SELECT MIN(t1.status_id) as status_id
        FROM zp.report_status as t1 
        WHERE 
            t1.date>=STR_TO_DATE(?,'%m.%Y') and 
            t1.date<=STR_TO_DATE(?,'%m.%Y') and 
            type_of_payment=1 
        HAVING
	        MIN(t1.status_id)>=3 and 
	        MIN(t1.status_id)<5";

        try {
            $sth = $dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date->format("m.Y"), date("m.Y")]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        $premium_row_count = $sth->rowCount();

        $query = "SELECT MIN(t1.status_id) as status_id
        FROM zp.report_status as t1 
        WHERE 
            t1.date>=STR_TO_DATE(?,'%m.%Y') and 
            t1.date<=STR_TO_DATE(?,'%m.%Y') and 
            type_of_payment=2 
        HAVING
	        MIN(t1.status_id)>=3 and 
	        MIN(t1.status_id)<5";

        try {
            $sth = $dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date->format("m.Y"), date("m.Y")]); // Выполняем запрос
        } catch (PDOException $e) {
            throw new Exception('Ошибка в PDO ' . $e->getMessage());
        }

        $salary_row_count = $sth->rowCount();

        return ($salary_row_count || $premium_row_count?true:false);
    }
}
