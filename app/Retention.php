<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 24.09.18
 * Time: 11:20
 */

require_once 'app/functions.php';

class Retention
{
    private $dbh;
    //                                  !!!!! ДОБАВИТЬ ПРОВЕРКУ type_of_payment в методах класса !!!!! !!!!!

    /***
     * @var type_of_payment
     * Тип выплат
     * 1 - премия
     * 2 - аванс
     * 3 - аванс2
     * 4- рассчет
     */
    private $type_of_payment;


    private $LateWork = NULL; // 1
    private $Schedule = NULL; // 2
    private $Internet = NULL; // 3
    private $CachAccounting = NULL; // 4
    private $Receivables = NULL; // 5
    private $Credit = NULL; // 6
    private $Other = NULL;// 7


    private $LateWork_Note = NULL;
    private $Schedule_Note = NULL;
    private $Internet_Note = NULL;
    private $CachAccounting_Note = NULL;
    private $Receivables_Note = NULL;
    private $Credit_Note = NULL;
    private $Other_Note = NULL;


    private $LateWork_lock = 0; // 1
    private $Schedule_lock = 0; // 2
    private $Internet_lock = 0; // 3
    private $CachAccounting_lock = 0; // 4
    private $Receivables_lock = 0; // 5
    private $Credit_lock = 0; // 6
    private $Other_lock = 0;// 7

    public function setLockAll() {
        $this->LateWork_lock = 1;
        $this->Schedule_lock = 1;
        $this->Internet_lock = 1;
        $this->CachAccounting_lock = 1;
        $this->Receivables_lock = 1;
        $this->Credit_lock = 1;
        $this->Other_lock = 1;
    }


    /**
     * @return int
     */
    public function getCachAccountingLock()
    {
        return $this->CachAccounting_lock;
    }

    /**
     * @return int
     */
    public function getCreditLock()
    {
        return $this->Credit_lock;
    }

    /**
     * @return int
     */
    public function getInternetLock()
    {
        return $this->Internet_lock;
    }

    /**
     * @return int
     */
    public function getLateWorkLock()
    {
        return $this->LateWork_lock;
    }

    /**
     * @return int
     */
    public function getOtherLock()
    {
        return $this->Other_lock;
    }

    /**
     * @return int
     */
    public function getReceivablesLock()
    {
        return $this->Receivables_lock;
    }

    /**
     * @return int
     */
    public function getScheduleLock()
    {
        return $this->Schedule_lock;
    }

    /**
     * @param null $Credit
     */
    public function setCredit($Credit)
    {
        $this->Credit = $Credit;
    }

    /**
     * @return null
     */
    public function getCredit()
    {
        return $this->Credit;
    }

    /**
     * @param null $Credit_Note
     */
    public function setCreditNote($Credit_Note)
    {
        $this->Credit_Note = $Credit_Note;
    }

    /**
     * @return null
     */
    public function getCreditNote()
    {
        return $this->Credit_Note;
    }


    /**
     * @return type_of_payment
     */
    public function getTypeOfPayment()
    {
        return $this->type_of_payment;
    }

    /**
     * @param type_of_payment $type_of_payment
     */
    public function setTypeOfPayment($type_of_payment)
    {
        $this->type_of_payment = $type_of_payment;
    }

    /**
     * @param null $LateWork
     */
    public function setLateWork($LateWork)
    {
        $this->LateWork = $LateWork;
    }

    /**
     * @param null $Schedule
     */
    public function setSchedule($Schedule)
    {
        $this->Schedule = $Schedule;
    }

    /**
     * @param null $Internet
     */
    public function setInternet($Internet)
    {
        $this->Internet = $Internet;
    }

    /**
     * @param null $CachAccounting
     */
    public function setCachAccounting($CachAccounting)
    {
        $this->CachAccounting = $CachAccounting;
    }

    /**
     * @param null $Receivables
     */
    public function setReceivables($Receivables)
    {
        $this->Receivables = $Receivables;
    }

    /**
     * @param null $Other
     */
    public function setOther($Other)
    {
        $this->Other = $Other;
    }

    /**
     * @param null $Other_Note
     */
    public function setOtherNote($Other_Note)
    {
        $this->Other_Note = $Other_Note;
    }


    /**
     * @param null $LateWork_Note
     */
    public function setLateWorkNote($LateWork_Note)
    {
        $this->LateWork_Note = $LateWork_Note;
    }

    /**
     * @param null $Schedule_Note
     */
    public function setScheduleNote($Schedule_Note)
    {
        $this->Schedule_Note = $Schedule_Note;
    }

    /**
     * @param null $Internet_Note
     */
    public function setInternetNote($Internet_Note)
    {
        $this->Internet_Note = $Internet_Note;
    }

    /**
     * @param null $CachAccounting_Note
     */
    public function setCachAccountingNote($CachAccounting_Note)
    {
        $this->CachAccounting_Note = $CachAccounting_Note;
    }

    /**
     * @param null $Receivables_Note
     */
    public function setReceivablesNote($Receivables_Note)
    {
        $this->Receivables_Note = $Receivables_Note;
    }

    /**
     * @return null
     */
    public function getCachAccounting()
    {
        return $this->CachAccounting;
    }

    /**
     * @return null
     */
    public function getCachAccountingNote()
    {
        return $this->CachAccounting_Note;
    }

    /**
     * @return null
     */
    public function getInternet()
    {
        return $this->Internet;
    }

    /**
     * @return null
     */
    public function getInternetNote()
    {
        return $this->Internet_Note;
    }

    /**
     * @return null
     */
    public function getLateWork()
    {
        return $this->LateWork;
    }

    /**
     * @return null
     */
    public function getLateWorkNote()
    {
        return $this->LateWork_Note;
    }

    /**
     * @return null
     */
    public function getOther()
    {
        return $this->Other;
    }

    /**
     * @return null
     */
    public function getOtherNote()
    {
        return $this->Other_Note;
    }

    /**
     * @return null
     */
    public function getReceivables()
    {
        return $this->Receivables;
    }

    /**
     * @return null
     */
    public function getReceivablesNote()
    {
        return $this->Receivables_Note;
    }

    /**
     * @return null
     */
    public function getSchedule()
    {
        return $this->Schedule;
    }

    /**
     * @return null
     */
    public function getScheduleNote()
    {
        return $this->Schedule_Note;
    }

    /*
    * Получение данных по определенному типу, в зависимости от даты
    * Требуется для группового заведения данных
    */
    public function fetchAllByDate($date, $type)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        // Получаю бонус по проведенным акциям
        try {
            /*
             * Создаю SQL Запрос
             */
            $query = "select * from retention_block where type=? and active=1 and type_of_payment=? and date=STR_TO_DATE(?,\"%m.%Y\")";
            /*
             * Подготавливаем запрос
             */
            $sth = $this->dbh->prepare($query);
            /*
             * Выполняю запрос
             */
            $sth->execute([$type, $this->type_of_payment, $date]);

            /*
             * Если количество строк > 0, то получаю данные из БД
             */
            if ($sth->rowCount() > 0) {
                /*
                 * Получаю все из БД
                 */
                $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
                /*
                 * Очищаю память
                 */
                $sth = null;


                $AllData = array();
                foreach ($raw as $item) {
                    $AllData[$item['member_id']] = $item;
                }
                $raw = null;

                return $AllData;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }
        return NULL;
    }

    /*
     * Получение данных по удержаниям при указанном ID пользователя и дате
     */
    public function fetch($id, $date)
    {
        $this->LateWork = NULL; // 1
        $this->Schedule = NULL; // 2
        $this->Internet = NULL; // 3
        $this->CachAccounting = NULL; // 4
        $this->Receivables = NULL; // 5
        $this->Credit = NULL; // 6
        $this->Other = NULL; // 7


        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        try {
            $query = "select * from retention_block where member_id=? and active=1 and type_of_payment=? and date=STR_TO_DATE(?,\"%m.%Y\")";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id, $this->type_of_payment, $date]);
            if ($sth->rowCount() > 0) {
                foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $item) {
                    switch ($item['type']) {
                        case 1:
                            $this->LateWork = $item['summ'];
                            $this->LateWork_Note = $item['note'];
                            break;
                        case 2:
                            $this->Schedule = $item['summ'];
                            $this->Schedule_Note = $item['note'];
                            break;
                        case 3:
                            $this->Internet = $item['summ'];
                            $this->Internet_Note = $item['note'];
                            break;
                        case 4:
                            $this->CachAccounting = $item['summ'];
                            $this->CachAccounting_Note = $item['note'];
                            break;
                        case 5:
                            $this->Receivables = $item['summ'];
                            $this->Receivables_Note = $item['note'];
                            break;
                        case 6:
                            $this->Credit = $item['summ'];
                            $this->Credit_Note = $item['note'];
                            break;
                        case 7:
                            $this->Other = $item['summ'];
                            $this->Other_Note = $item['note'];
                            break;
                    }
                }
            } else {
                /*
                 * Если выставлена тип выплаты в премию, то проверяю есть ли в авансе
                 */
                if ($this->type_of_payment == 1) { // Если тип выплаты 1(премия)
                    $query = "select * from retention_block where member_id=? and active=1 and type_of_payment=2 and date=STR_TO_DATE(?,\"%m.%Y\")";
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$id, $date]);

                    if ($sth->rowCount() > 0) {
                        foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $item) {
                            switch ($item['type']) {
                                case 1:
                                    $this->LateWork = $item['summ'];
                                    $this->LateWork_Note = $item['note'];
                                    break;
                                case 2:
                                    $this->Schedule = $item['summ'];
                                    $this->Schedule_Note = $item['note'];
                                    break;
                                case 3:
                                    $this->Internet = $item['summ'];
                                    $this->Internet_Note = $item['note'];
                                    break;
                                case 4:
                                    $this->CachAccounting = $item['summ'];
                                    $this->CachAccounting_Note = $item['note'];
                                    break;
                                case 5:
                                    $this->Receivables = $item['summ'];
                                    $this->Receivables_Note = $item['note'];
                                    break;
                                case 6:
                                    $this->Credit = $item['summ'];
                                    $this->Credit_Note = $item['note'];
                                    break;
                                case 7:
                                    $this->Other = $item['summ'];
                                    $this->Other_note = $item['note'];
                                    break;
                            }
                        }
                    }
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД retention_block. " . $e->getMessage());
        }
    }


    public function fetchFromPayout($id,$date) {
        $this->LateWork = NULL; // 1
        $this->Schedule = NULL; // 2
        $this->Internet = NULL; // 3
        $this->CachAccounting = NULL; // 4
        $this->Receivables = NULL; // 5
        $this->Credit = NULL; // 6
        $this->Other = NULL; // 7

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        // Получаю бонус по проведенным акциям
        try {
            $query = "select RetentionData from financial_payout_ext where member_id=? and type_of_payment=1 and date=STR_TO_DATE(?,\"%m.%Y\") LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id, $date]);

            $raw = current($sth->fetchAll(PDO::FETCH_ASSOC));
            if ($raw) {
                foreach (unserialize($raw["RetentionData"]) as $key=>$item) {
                    $this->$key = $item;
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД" . $sth->errorInfo()[2]);
        }
    }

    /*
    * Добавление данных в БД
    * Необходимые параметры: ID пользователя, дата, тип, сумма, заметка.
    */
    public function addByType($id, $date, $type, $summ, $note,$changer_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $dateNow = new DateTime();

        if (is_null($this->type_of_payment)) {
            throw new Exception("Не выставлен тип выплаты!");
        }
        try {
            $query = "SELECT * FROM retention_block where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and retention_block.lock=1 and type_of_payment=?";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$type, $id, $date, $this->type_of_payment]);
            } catch (PDOException $e) {
                throw new Exception("Не могу выполнить зарос к БД" . $e->getMessage());
            }

            if ($sth->rowCount() > 0) {
                throw new Exception("Запись заблокирована, изменение невозможно");
                return false;
            }

            $query = "SELECT * FROM retention_block where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1 and type_of_payment=?";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$type, $id, $date, $this->type_of_payment]);

            if ($sth->rowCount() > 0) {
                $query = "update retention_block SET active=0 where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1 and retention_block.lock=0 and type_of_payment=?";
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$type, $id, $date, $this->type_of_payment]);

                if ($sth->rowCount() == 0) {
                    throw new Exception('Не могу обновить строки: ' . $sth->errorInfo()[2]);
                }
            }

            $query = "insert into retention_block (member_id, date, summ, note, active, change_date,type_of_payment, type, changer_id) VALUES (?, STR_TO_DATE(?, '%m.%Y'), ?, ?, 1, NOW(),?,?,?)";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id, $date, $summ, $note, $this->type_of_payment, $type, $changer_id]);

            # Если мы меняем в аванс2 в декабре месяце, то должны добавить/обновить в премию значения!
            if ($dateNow->format("m") == 12 && $this->type_of_payment == 3) {
                # Ищу активные записи по в премии
                $query = "SELECT * FROM retention_block where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1 and type_of_payment=?";
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$type, $id, $date, 1]);

                # Если запись существует, то ставлю active=0
                if ($sth->rowCount() > 0) {
                    $query = "update retention_block SET active=0 where type=? and member_id=? and date=STR_TO_DATE(?, '%m.%Y') and active=1 and retention_block.lock=0 and type_of_payment=?";
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute([$type, $id, $date, 1]);
                }

                # Добавляю новые значения
                $query = "insert into retention_block (member_id, date, summ, note, active, change_date,type_of_payment, type, changer_id) VALUES (?, STR_TO_DATE(?, '%m.%Y'), ?, ?, 1, NOW(),?,?,?)";
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$id, $date, $summ, $note, 1, $type, $changer_id]);
            }
        } catch (PDOException $e) {
            throw new Exception("Не могу выполнить зарос к БД" . $e->getMessage());
        }

        switch ($type) {
            case 1:
                $this->LateWork = $summ;
                $this->LateWork_Note = $note;
                break;
            case 2:
                $this->Schedule = $summ;
                $this->Schedule_Note = $note;
                break;
            case 3:
                $this->Internet = $summ;
                $this->Internet_Note = $note;
                break;
            case 4:
                $this->CachAccounting = $summ;
                $this->CachAccounting_Note = $note;
                break;
            case 5:
                $this->Receivables = $summ;
                $this->Receivables_Note = $note;
                break;
            case 6:
                $this->Credit = $summ;
                $this->Credit_Note = $note;
                break;
            case 7:
                $this->Other = $summ;
                $this->Other_Note = $note;
                break;
        }
    }


    /*
     * Сумма всех переменных класса
     */
    public function getSumm()
    {
        return $this->LateWork + $this->Schedule + $this->Internet + $this->CachAccounting + $this->Receivables + $this->Credit + $this->Other;
    }

    /*
     * Проверка на пустоту
     */
    public function checkNull()
    {
        if (is_null($this->LateWork) &&
            is_null($this->Schedule) &&
            is_null($this->Internet) &&
            is_null($this->CachAccounting) &&
            is_null($this->Receivables) &&
            is_null($this->Other)) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllData(){
        $data = array();
        $data[] = array("summ"=>$this->LateWork,"note"=>$this->LateWork_Note);
        $data[] = array("summ"=>$this->Schedule,"note"=>$this->Schedule_Note);
        $data[] = array("summ"=>$this->Internet,"note"=>$this->Internet_Note);
        $data[] = array("summ"=>$this->CachAccounting,"note"=>$this->CachAccounting_Note);
        $data[] = array("summ"=>$this->Receivables,"note"=>$this->Receivables_Note);
        $data[] = array("summ"=>$this->Credit,"note"=>$this->Credit_Note);
        $data[] = array("summ"=>$this->Other,"note"=>$this->Other_Note);

        return $data;
    }

}
