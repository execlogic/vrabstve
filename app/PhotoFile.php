<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 18.07.18
 * Time: 13:51
 */

class PhotoFile
{
    private $target_dir = "/";
    private $target_file;
    private $uploadOk=true;
    private $FormName;
    private $FileType;
    private $maxfileSize = 15000000;
    private $Format = array("jpg","jpeg");

    public function setFormName($name) {
        $this->FormName = $name;
    }

    /**
     * @param string $target_dir
     */
    public function setTargetDir($target_dir)
    {
        $this->target_dir = $target_dir;
        $this->CheckTargetDir();
    }

    public function Upload(){
        if (empty($this->FormName)) {
            throw new Exception("Нужно задать имя формы<br>");
        }

        $this->target_file = $this->target_dir."original_image.jpg";

        $this->FileType = strtolower(pathinfo($this->target_file,PATHINFO_EXTENSION));
        if (!$this->CheckExist()) {
            if (!unlink($this->target_file)) {
                throw new Exception('Не могу заменить файл');
            }
        }

        if (!$this->Size()) {
            throw new Exception("Sorry, your file is too large.");
        }

        if (!$this->CheckFormat()) {
            throw new Exception("Sorry, only ".implode(' && ',$this->Format)." files are allowed.");
        }

        if ($this->uploadOk==false) {
            echo "Sorry, your file was not uploaded.";
        } else {
            if (!move_uploaded_file($_FILES[$this->FormName]["tmp_name"], $this->target_file)) {
                throw  new Exception("Sorry, there was an error uploading your file.");
            }

            // Если оригинал смогли залить, то ресайзим картинку
            $this->ResizeImage();
            $this->ResizeImage(240,"chat.jpg",320);
            $this->ResizeImage(105,"mail.jpg",110);
        }
    }

    private function Size() {
        if ($_FILES[$this->FormName]["size"] > $this->maxfileSize) {
            $this->uploadOk = false;
            return false;
        }
        return true;
    }

    private function CheckFormat() {
        if (!in_array($this->FileType,$this->Format)) {
            $this->uploadOk = false;
            return false;
        }

        return true;
    }

    private function CheckExist() {
        // Check if file already exists
        if (file_exists($this->target_file)) {
            return false;
        }
        return true;
    }

    private function ShowInfo() {
        // Выводим информацию о загруженном файле:
        echo "<h3>Информация о загруженном на сервер файле: </h3>";
        echo "<p><b>Оригинальное имя загруженного файла: ".$_FILES[$this->FormName]['name']."</b></p>";
        echo "<p><b>Mime-тип загруженного файла: ".$_FILES[$this->FormName]['type']."</b></p>";
        echo "<p><b>Размер загруженного файла в байтах: ".$_FILES[$this->FormName]['size']."</b></p>";
        echo "<p><b>Временное имя файла: ".$_FILES[$this->FormName]['tmp_name']."</b></p>";
    }

    public function ReturnFileName() {
        if ($this->uploadOk == true && $this->target_file ) {
            return $this->target_file;
        }
        return false;
    }

    public function DeleteFile() {
        if(!file_exists($this->target_file) || !is_readable($this->target_file))
            return false;

        if (unlink($this->target_file)) {
            $this->target_file = null;
            $this->uploadOk = false;
            return true;
        } else {
            return false;
        }
    }

    public function CheckTargetDir() {
        if (!is_dir($this->target_dir)){
            mkdir($this->target_dir);
        }

        return true;
    }

    private function crop($src, array $rect)
    {
        $dest = imagecreatetruecolor($rect['width'], $rect['height']);
        imagecopy(
            $dest,
            $src,
            0,
            0,
            $rect['x'],
            $rect['y'],
            $rect['width'],
            $rect['height']
        );

        return $dest;
    }

    // 200x280
    private function ResizeImage($new_w=200,$imgname="photo.jpg",$limit_h=280) {
        $original_image = imagecreatefromjpeg ( $this->target_file);
        $w = imagesx($original_image);
        $h = imagesy($original_image);

//        $new_w = 200;
        $new_h=$new_w * ($h/$w);

//        $new_h=$new_w * ($h/$w);
//        $new_w=$new_h * ($w/$h);

        $im2 = ImageCreateTrueColor($new_w, $new_h);
        imagecopyResampled($im2, $original_image, 0, 0, 0, 0, $new_w, $new_h, $w, $h);

        if ($new_h>$limit_h) {
//            $im2 = imagecrop($im2,['x'=>0,'y'=>0, 'width'=>$new_w, 'height'=>$limit_h]);
            $im2 = $this->crop($im2,['x'=>0,'y'=>0, 'width'=>$new_w, 'height'=>$limit_h]);
        }

        imagejpeg($im2,$this->target_dir."/".$imgname);
        imagedestroy($im2);
        imagedestroy($original_image);

        return true;
    }
}