<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 13.02.19
 * Time: 16:33
 */

require_once 'functions.php';

class PayHistory
{
    private $dbh = null;
    private $CommercialBlok = array();
    private $AdministrativeBlock = array();
    private $RetentionBlock = array();
    private $CorrectingBlock = array();
    private $CommercialCorrectingProcent = array();
    private $KPIBlock = array();
    private $motivation = null;

    /**
     * @param mixed $motivation
     */
    public function setMotivation($motivation)
    {
        $this->motivation = $motivation;
    }



    public function fetchPayHistory($member_id, $TypeOfPayment, $date) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $this->CommercialBlok = array();
        $this->AdministrativeBlock = array();
        $this->RetentionBlock = array();
        $this->CorrectingBlock = array();
        $this->CommercialCorrectingProcent = array();
        $this->KPIBlock = array();

        $query = "SELECT t1.*, t2.name as changer_name, t2.lastname as changer_last, t2.middle as changer_middle
        FROM zp.administrative_block as t1
        LEFT JOIN member as t2 ON (t1.changer_id=t2.id)
        WHERE member_id=? and type_of_payment=? and date=STR_TO_DATE(?, '%m.%Y')";
        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$member_id, $TypeOfPayment, $date])) {
            $error = $sth->errorInfo();
            throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        $this->AdministrativeBlock = $sth->fetchAll(PDO::FETCH_ASSOC);

        $query = "SELECT t1.*, t2.name as changer_name, t2.lastname as changer_last, t2.middle as changer_middle
        FROM zp.retention_block as t1
        LEFT JOIN member as t2 ON (t1.changer_id=t2.id) 
        WHERE member_id=? and type_of_payment=? and date=STR_TO_DATE(?, '%m.%Y')";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$member_id, $TypeOfPayment, $date])) {
            $error = $sth->errorInfo();
            throw new Exception('Ошибка обращения к таблице retention_block.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        $this->RetentionBlock = $sth->fetchAll(PDO::FETCH_ASSOC);

        $query = "SELECT t1.*, t2.name as changer_name, t2.lastname as changer_last, t2.middle as changer_middle
        FROM zp.correcting_block as t1
        LEFT JOIN member as t2 ON (t1.changer_id=t2.id) 
        WHERE member_id=? and type_of_payment=? and date=STR_TO_DATE(?, '%m.%Y')";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$member_id, $TypeOfPayment, $date])) {
            $error = $sth->errorInfo();
            throw new Exception('Ошибка обращения к таблице correcting_block.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        $this->CorrectingBlock = $sth->fetchAll(PDO::FETCH_ASSOC);

        if ($this->motivation == 2) {
            $query = "SELECT t1.*, t2.name as changer_name, t2.lastname as changer_last, t2.middle as changer_middle
            FROM zp.correcting_tt_procent as t1
            LEFT JOIN member as t2 ON (t1.changer_id=t2.id) 
            WHERE member_id=? and date=STR_TO_DATE(?, '%m.%Y')";

            try {
                $sth = $this->dbh->prepare($query);
            } catch (PDOException $exception) {
                throw new Exception('Ошибка prepare' . $exception->getMessage());
            }

            if (!$sth->execute([$member_id, $date])) {
                $error = $sth->errorInfo();
                throw new Exception('Ошибка обращения к таблице correcting_tt_procent.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
            }

            $this->CommercialCorrectingProcent = $sth->fetchAll(PDO::FETCH_ASSOC);
        }

        if ($this->motivation != null && $this->motivation != 6 && $this->motivation != 10 ) {
            switch ($this->motivation) {
                case 1:
                    $query = "SELECT  
                    t2.name as object,
                    t3.bonus,
                    t3.bonus_note,
                    t3.correct,
                    t3.correct_note,
                    t3.changer_id,
                    t3.date,
                    t4.name as changer_name, t4.lastname as changer_last, t4.middle as changer_middle
                    FROM data_manager as t1
                    JOIN statgroup as t2 ON(t1.statgroup_id = t2.id) 
                    LEFT JOIN correcting_manager as t3 ON (t1.id = t3.data_manager_id  )
                    LEFT JOIN member as t4 ON (t3.changer_id = t4.id)
                    where t1.member_id=? and t1.date=STR_TO_DATE(?, '%m.%Y') and (t3.bonus IS NOT NULL || t3.correct IS NOT NULL) order by t1.id";

                    break;
                case 2:
                case 3:
                    $query = "SELECT  
                    t2.namesklad as object,
                    t3.bonus,
                    t3.bonus_note,
                    t3.correct,
                    t3.correct_note,
                    t3.changer_id,
                    t3.date,
                    t4.name as changer_name, t4.lastname as changer_last, t4.middle as changer_middle
                    FROM data_TT as t1
                    JOIN department as t2 ON(t1.department_id = t2.id) 
                    LEFT JOIN correcting_tt as t3 ON (t1.id = t3.data_TT_id and t3.member_id=? )
                    LEFT JOIN member as t4 ON (t3.changer_id = t4.id)
                    where t1.date=STR_TO_DATE(?, '%m.%Y') and (t3.bonus IS NOT NULL || t3.correct IS NOT NULL) order by t1.id";

                    break;

                case 4:
                    $query = "SELECT  
                    t2.name as object,
                    t3.bonus,
                    t3.bonus_note,
                    t3.correct,
                    t3.correct_note,
                    t3.changer_id,
                    t3.date,
                    t4.name as changer_name, t4.lastname as changer_last, t4.middle as changer_middle
                    FROM data_statgroup as t1
                    JOIN statgroup as t2 ON(t1.statgroup_id = t2.id) 
                    LEFT JOIN correcting_statgroup as t3 ON (t1.id = t3.data_statgroup_id and t3.member_id=? )
                    LEFT JOIN member as t4 ON (t3.changer_id = t4.id)
                    where t1.date=STR_TO_DATE(?, '%m.%Y') and (t3.bonus IS NOT NULL || t3.correct IS NOT NULL) order by t1.id";
                    break;

                case 5:
                case 7:
                    $query="SELECT  t3.bonus, t3.bonus_note, t3.correct, t3.correct_note, t3.changer_id, t3.date,
                    t4.name as changer_name, t4.lastname as changer_last, t4.middle as changer_middle
                    FROM data_service as t1
                    LEFT JOIN correcting_service as t3 ON (t1.id = t3.data_service_id )
                    LEFT JOIN member as t4 ON (t3.changer_id = t4.id)
                    where t1.member_id=? and t1.date=STR_TO_DATE(?, '%m.%Y') and (t3.bonus IS NOT NULL || t3.correct IS NOT NULL) order by t1.id";
                    break;
                default:
                    break;
            }

            if ($this->motivation != null && $this->motivation != 6 && $this->motivation != 8 && $this->motivation != 9 && $this->motivation!=10 ) {
                try {
                    $sth = $this->dbh->prepare($query);
                } catch (PDOException $exception) {
                    throw new Exception('Ошибка prepare' . $exception->getMessage());
                }

                if (!$sth->execute([$member_id, $date])) {
                    $error = $sth->errorInfo();
                    throw new Exception('Ошибка обращения к таблице коммерческого блока.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
                }

                $this->CommercialBlok = $sth->fetchAll(PDO::FETCH_ASSOC);
            }
        }


        $query = "SELECT t1.*, CONCAT(t2.lastname, ' ', t2.name, ' ', t2.middle) as changer_name
        FROM zp.kpi_block as t1
        LEFT JOIN member as t2 ON (t1.changer_id=t2.id) 
        WHERE member_id=? and type_of_payment=? and date=STR_TO_DATE(?, '%m.%Y')";

        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare' . $exception->getMessage());
        }

        if (!$sth->execute([$member_id, $TypeOfPayment, $date])) {
            $error = $sth->errorInfo();
            throw new Exception('Ошибка обращения к таблице kpi_block.<BR> Код ошибки: ' . $error[1] . '.<br> Текст ошибки: ' . $error[2] . "<BR>");
        }

        $this->KPIBlock = $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function filterAdministrativeBlock($Filter) {
        foreach ($this->AdministrativeBlock as $key=>$value) {
            $type = $value['type'];
            if ($Filter[$type] == 0 ){
                unset($this->AdministrativeBlock[$key]);
            }
        }
    }

    public function filterRetitiotionBlock($Filter) {
        foreach ($this->RetentionBlock as $key=>$value) {
            $type = $value['type'];
            if ($Filter[$type] == 0 ){
                unset($this->RetentionBlock[$key]);
            }
        }
    }

    public function filterCorrectingBlock($Filter) {
        foreach ($this->CorrectingBlock as $key=>$value) {
            $type = $value['type'];

            if (isset($Filter[$type]) && $Filter[$type] == 0 ){
                unset($this->CorrectingBlock[$key]);
            }
        }
    }

    /**
     * @return array
     */
    public function getAdministrativeBlock() {
        return $this->AdministrativeBlock;
    }

    /**
     * @return array
     */
    public function getRetentionBlock()
    {
        return $this->RetentionBlock;
    }

    /**
     * @return array
     */
    public function getCommercialBlok()
    {
        return $this->CommercialBlok;
    }

    public function getKPIBlok()
    {
        return $this->KPIBlock;
    }

    /**
     * @return array
     */
    public function getCorrectingBlock()
    {
        return $this->CorrectingBlock;
    }

    /**
     * @return array
     */
    public function getCommercialCorrectingProcent()
    {
        return $this->CommercialCorrectingProcent;
    }

}