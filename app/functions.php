<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.07.18
 * Time: 12:09
 */

function dbConnect( $errors = true ) {
    $config = include($_SERVER['DOCUMENT_ROOT']."/config/config.php");
    $dbh = null;
    try {
        $dbh = new PDO(
            $config['db_type'] .
            ':host=' . $config['db_host'] .
            ';port=' . $config['db_port'] .
            ';dbname=' . $config['db_name'],
            $config['db_user'],
            $config['db_password'],
            $config['db_options']
        );
    }
    catch (PDOException $e) {
        if ( $errors === true ) {
            echo "\nPDO::errorInfo():\n";
            print $e->getMessage();

        }
    }
    return $dbh;
}
?>