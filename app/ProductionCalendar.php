<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 26.09.18
 * Time: 14:39
 */

require_once 'app/functions.php';

class ProductionCalendar
{
    private $dbh;
    private $Calendar=array();
    private $working_days_array = array();

    public function fetch() {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        try {
            $query = "select *,DATE_FORMAT(date,'%m.%Y') as date from ProductionCalendar";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute();
            $this->Calendar = $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД".$sth->errorInfo()[2]);
        }
    }

    public function fetchYear($year) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        try {
            $query = "select *, DATE_FORMAT(date,'%m.%Y') as date from ProductionCalendar where date between STR_TO_DATE(?,'%m.%Y') and STR_TO_DATE(?,'%m.%Y')";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute(["01.".$year, "12.".$year,]);
            $this->Calendar = $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД".$sth->errorInfo()[2]);
        }
    }

    public function fetchByDate($date) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        try {
            $query = "select date, working_days from ProductionCalendar where date=STR_TO_DATE(?,\"%m.%Y\") LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date]);
            $this->Calendar = $sth->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД".$sth->errorInfo()[2]);
        }
    }

    public function fetchByDateWithDepartment($date, $department_id) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        try {
            $query = "select date, working_days from ProductionCalendar where date=STR_TO_DATE(?,\"%m.%Y\") and department_id=? LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date, $department_id]);
            if ($sth->rowCount()>0) {
                $this->Calendar = $sth->fetch(PDO::FETCH_ASSOC);
            } else {
                $this->fetchByDate($date);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД".$sth->errorInfo()[2]);
        }
    }

    public function get() {
        return $this->Calendar;
    }

    public function fetchWorkingArray($date) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        try {
            $query = "select wd_array from ProductionCalendar where date=STR_TO_DATE(?,\"%m.%Y\") LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date]);
            $raw = current($sth->fetch(PDO::FETCH_ASSOC));

            $this->working_days_array = unserialize($raw);
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД".$sth->errorInfo()[2]);
        }
    }

    public function fetchWorkingArrayWithDepartment($date, $department_id) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        try {
            $query = "select wd_array from ProductionCalendar where date=STR_TO_DATE(?,\"%m.%Y\") and department_id=? LIMIT 1";
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$date, $department_id]);
            if ($sth->rowCount()>0) {
                $raw = current($sth->fetch(PDO::FETCH_ASSOC));

                $this->working_days_array = unserialize($raw);
            } else {
                $this->fetchWorkingArray($date);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new Exception("Не могу выполнить зарос к БД".$sth->errorInfo()[2]);
        }
    }

    public function getWDArray() {
        return $this->working_days_array;
    }

}