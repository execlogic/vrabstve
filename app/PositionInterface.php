<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 18.07.18
 * Time: 18:42
 */

require_once 'Position.php';
require_once 'functions.php';

class PositionInterface
{
    private $positionList = array();
    private $dbh;

    public function fetchPositions() {
        $this->dbh=dbConnect();
        $query = "select * from position order by id";

        $sth = $this->dbh->prepare($query);

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        $sth->execute();
        $rawresult = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $this->dbh = null;

        foreach ($rawresult as $value) {
            $Position = new Position();
            $Position->setId($value['id']);
            $Position->setName($value['name']);
            $Position->setDepartmentId(explode(",", $value['department_id']));
            array_push($this->positionList,$Position);
        }
    }

    public function GetPositions() {
        if (empty($this->positionList)) {
            throw new Exception("Пустой список должностей");
        }
        return $this->positionList;
    }

    /**
     * @return bool
     * @throws Exception
     * Отображает Отделы
     */
    public function ShowPositions() {
        if (empty($this->positionList)) {
            throw new Exception("Пустой список должностей");
            return false;
        }

        echo "<table class='table table-bordered'>";
        foreach ($this->positionList as $value ){
            echo "<tr>";
            echo "<td>".$value->getId()."</td><td>".$value->getName()."</td>";
            echo "</tr>";
        }
        echo "</table>";

        return true;
    }

    public function AddPositions($name) {
        $this->dbh=dbConnect();

        if (empty($this->dbh)) {
            echo "Error<br>";
        }

        $query = "INSERT INTO position (name) VALUES (?)";
        try {
            $sth = $this->dbh->prepare($query);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare'.$exception->getMessage());
        }

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$name])) {
            $error = $sth->errorInfo();
            throw new Exception('Ошибка добавления в БД.<BR> Код ошибки: '.$error[1].'.<br> Текст ошибки: '.$error[2]."<BR>");
        }

        $sth = null;
        $this->dbh = null;

        return true;
    }

    public function AddPositionSingle($name,$department_id) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from position where name = ?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$name]);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare'.$exception->getMessage());
        }

        if ($sth->rowCount() > 0 ) {
            throw new Exception("Отдел с таким названием уже существует");
        } else {
            $query = "INSERT INTO position (name,department_id) VALUES (?,?)";
            try {
                $sth = $this->dbh->prepare($query);
                $sth->execute([$name,$department_id]);
            } catch (PDOException $exception) {
                throw new Exception('Ошибка БД' . $exception->getMessage());
            }
        }

        $sth = null;
        $this->dbh = null;

        return true;
    }

    public function UpdatePositionSingle($id, $name,$department_id) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from position where id = ?";
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute([$id]);
        } catch (PDOException $exception) {
            throw new Exception('Ошибка prepare'.$exception->getMessage());
        }

        if ($sth->rowCount() > 0 ) {
            $query = "UPDATE position  SET name=?, department_id = ? where id = ?";
            try {
                $sth = $this->dbh->prepare($query);
                $sth->execute([$name,$department_id,$id]);
            } catch (PDOException $exception) {
                throw new Exception('Ошибка БД' . $exception->getMessage());
            }
        } else {
            throw new Exception("Отдела с таким названием не существует");
        }

        $sth = null;
        $this->dbh = null;

        return true;
    }

    public function findById($id) {
        if (count($this->positionList)==0) {
            $this->fetchPositions();
        }
        $member_position = current(array_filter($this->positionList, function ($d) use ($id) {
            return $d->getId() == $id;
        }));

        if (isset($member_position)) {
            return $member_position;
        } else {
            $Position = new Position();
            $Position->setId($id);
            $Position->setName("None");
            return NULL;
        }
    }
}