<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 20.08.18
 * Time: 17:40
 */

require_once 'functions.php';

class NdflInterface
{
    private $dbh = null;

    public function add($array) {
        list($date, $ndfl, $ro, $user_id) = $array;
//        var_dump($array);

        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select t1.*,t2.lastname,t2.name,t2.middle from member_ndfl as t1 JOIN member as t2 ON (t1.member_id=t2.id) where date=STR_TO_DATE(?, '%Y-%m') AND member_id=?";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$date, $user_id])) {
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
        if (count($raw)>0) {
            throw new Exception("Данные по пользователю \"".$raw[0]['lastname']. " ".$raw[0]['name']."\" уже добавлены БД");
        }


        /*
         * Добавляю данные в NDFL
         */
        $query = "INSERT INTO member_ndfl (date,ndfl,ro,member_id) VALUES (STR_TO_DATE(?, '%Y-%m'), ?, ?, ?)";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$date, $ndfl, $ro, $user_id])) {
            throw new Exception("Не могу выполнить зарос к БД");
        }

    }

    public function get($date) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        /*
         * Добавляю данные в NDFL
         */
        $query = "SELECT t1.*,t2.lastname,t2.name,t2.middle FROM zp.member_ndfl as t1 JOIN member as t2 ON (t1.member_id=t2.id) where t1.date = STR_TO_DATE(?, '%Y-%m');";
        $sth = $this->dbh->prepare($query); // Подготавливаем запрос

        if(!$sth) {
            throw new Exception('Ошибка в PDO');
        };

        if (!$sth->execute([$date])) {
            throw new Exception("Не могу выполнить зарос к БД");
        }

        $rawResult = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $rawResult;
    }

}