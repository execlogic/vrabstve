<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 13.09.18
 * Time: 11:39
 */
require_once 'app/DataItem.php';
require_once 'app/functions.php';

/**
 * Class DataManager
 * Данный класс описывает менеджера
 */
class DataManager extends DataItem {
    private $statgroup_id;
    private $statgroup_name;
    private $department_id;
    private $department_name;

    private $correct;
    private $correct_note;

    private $bonus;
    private $bonus_note;

    private $baseprocent;

    private $service_id;

    /**
     * @return mixed
     */
    public function getServiceId()
    {
        return $this->service_id;
    }

    /**
     * @param mixed $service_id
     */
    public function setServiceId($service_id)
    {
        $this->service_id = $service_id;
    }

    /**
     * @return mixed
     */
    public function getBaseprocent()
    {
        return $this->baseprocent;
    }

    /**
     * @param mixed $baseprocent
     */
    public function setBaseprocent($baseprocent)
    {
        $this->baseprocent = $baseprocent;
    }

    /**
     * @param mixed $department_id
     */
    public function setDepartmentId($department_id)
    {
        $this->department_id = $department_id;
    }

    /**
     * @param mixed $department_name
     */
    public function setDepartmentName($department_name)
    {
        $this->department_name = $department_name;
    }

    /**
     * @param mixed $statgroup_id
     */
    public function setStatgroupId($statgroup_id)
    {
        $this->statgroup_id = $statgroup_id;
    }

    /**
     * @param mixed $statgroup_name
     */
    public function setStatgroupName($statgroup_name)
    {
        $this->statgroup_name = $statgroup_name;
    }

    /**
     * @return mixed
     */
    public function getDepartmentId()
    {
        return $this->department_id;
    }

    /**
     * @return mixed
     */
    public function getDepartmentName()
    {
        return $this->department_name;
    }

    /**
     * @return mixed
     */
    public function getStatgroupId()
    {
        return $this->statgroup_id;
    }

    /**
     * @return mixed
     */
    public function getStatgroupName()
    {
        return $this->statgroup_name;
    }

    /**
     * @param mixed $bonus
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;
    }

    /**
     * @param mixed $bonus_note
     */
    public function setBonusNote($bonus_note)
    {
        $this->bonus_note = $bonus_note;
    }

    /**
     * @param mixed $correct
     */
    public function setCorrect($correct)
    {
        $this->correct = $correct;
    }

    /**
     * @param mixed $correct_note
     */
    public function setCorrectNote($correct_note)
    {
        $this->correct_note = $correct_note;
    }

    /**
     * @return mixed
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @return mixed
     */
    public function getBonusNote()
    {
        return $this->bonus_note;
    }

    /**
     * @return mixed
     */
    public function getCorrect()
    {
        return $this->correct;
    }

    /**
     * @return mixed
     */
    public function getCorrectNote()
    {
        return $this->correct_note;
    }

}
