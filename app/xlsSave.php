<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 04.02.20
 * Time: 12:14
 */

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/MemberInterface.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/CustomPercent.php';

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/MemberFinanceBlock.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/DataItem.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/DataManager.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/ErrorInterface.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/AdministrativeBlock.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Retention.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Correction.php';

require_once $_SERVER['DOCUMENT_ROOT'] . '/app/ProductionCalendar.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/AwhInterface.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Premium.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Avans.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Avans2.php';

require_once $_SERVER['DOCUMENT_ROOT'] . '/app/DepartmentInterface.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/DirectionInterface.php';

class xlsSave
{
    private $type_of_payment;
    private $date;
    private $date_array = ["январь", "ферваль", "март", "апрель", "май", "июнь", "июль","август","сентябрь","октябрь", "ноябрь", "декабрь"];
    private $dt;
    private $filename;

    public function save(&$PayRollArray) {
        switch ($this->type_of_payment) {
            case 1:
                $xls_type = "Премия";
                $file_t = "premium";
                break;
            case 2:
                $xls_type = "Аванс";
                $file_t  = "avans";
                break;
            case 3:
                $xls_type = "Аванс2";
                $file_t = "avans2";
                break;
            default:
                $xls_type = "---";
                break;
        };

        $this->filename = 'tmp/'.$file_t.'_'.$this->date.'.xls';
        $xlsData = [
            [
                "Месяц",
                "Тип",
                "ФИО",
                "Направление",
                "Отдел",
                "Должность",
                "Оклад",
                "Оклад в день",
                "Отсутствие(дни)",
                "Отсутствие(сумма)",
                "Отпуск",
                "Болезнь",
                "Удержания(руб)",
                "За опоздания",
                "За интернет",
                "Кассовый учет",
                "Дебиторская задолженность",
                "Кредит",
                "Другое",

                "KPI",
                "Административная премия(руб)",
                "Акции",
                "Административная премия",
                "Обучение",
                "Премия другого отдела",
                "Неликвиды и брак",
                "Воскресения",
                "Выплаты не менее",
                "Переработки",

                "Коммерческая премия(руб)",
                "Корректировки(руб)",
                "Корректировка бонуса",
                "Расчет",
                "Годовой бонус",

                "Отчисления в ГБ",
                "Связь",
                "Транспорт",
                "Выплаченный аванс",
                "Количество дней",
                "НДФЛ",
                "ИЛ",
                "Итого(руб)",
                "Проверка",
                "Проверка Округл.",
                "Разница"
            ]
        ];


        $cell = 2;
        foreach ($PayRollArray as $item) {
            $xlsData[] = [
                $this->dt->format("m")." ".$this->date_array[($this->dt->format("m"))-1], //Месяц
                $xls_type, //Тип
                $item->getFio(),  //ФИО
                $item->getDirection()['name'], //Направление
                $item->getDepartment()['name'], //Отдел
                $item->getPosition()['name'], //Отдел
                $item->getSalary(), //Оклад
                ($item->getSalary()/$item->getWorkingdays()),
                $item->getAbsencesDays(), // Отсутствия в днях
                $item->getAbsences(), // Отсутствия сумма
                $item->getHoliday(), // Отпуск
                $item->getDisease(), // Болезнь
                $item->getHold(), // Удержания

                $item->getRetentionData()["LateWork"], //За опоздания
                $item->getRetentionData()["Internet"], //За интернет
                $item->getRetentionData()["CachAccounting"], //Кассовый учет
                $item->getRetentionData()["Receivables"], //Дебиторская задолженность
                $item->getRetentionData()["Credit"], //Кредит
                $item->getRetentionData()["Other"], // Другое

                $item->getKPI(), // KPI
                $item->getAdministrative(), // Административная премия + KPI
                $item->getAdministrativeData()["Promotions"], // "Акции",

                $item->getAdministrativeData()["AdministrativePrize"], //"Административная премия",
                $item->getAdministrativeData()["Training"], //"Обучение",
                $item->getAdministrativeData()["PrizeAnotherDepartment"], //"Премия другого отдела",
                $item->getAdministrativeData()["Defect"], // "Неликвиды и брак",
                $item->getAdministrativeData()["Sunday"], // "Воскресения",
                $item->getAdministrativeData()["NotLessThan"], //"Выплаты не менее",
                $item->getAdministrativeData()["Overtime"], // "Переработки",

                $item->getCommercial(), // Коммерческая премия

                $item->getCorrecting(), //Корректировки
                $item->getCorrectionData()["BonusAdjustment"], // Корректировка бонуса
                $item->getCorrectionData()["Calculation"], // Расчет
                $item->getCorrectionData()["YearBonus"], //Годовой бонус

                $item->getYearbonusPay(), // Отчисления в ГБ
                $item->getMobile(), //Мобильная
                $item->getTransport(), //ГСМ
                $item->getAdvancePayment(), // Выплаченый аванс
                $item->getWorkingdays(), // Количество рабочих дней в месяце
                $item->getNDFL(), // НДФЛ
                $item->getRO(), // ИЛ
                $item->getSumm() // Сумма
            ];
        };

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $spreadsheet->getDefaultStyle()->getFont()->setSize(8);
        $spreadsheet->getDefaultStyle()->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $spreadsheet->getActiveSheet()
            ->fromArray(
                $xlsData,  // The data to set
                NULL        // Array values with this value will not be set
            );
        $sheet->getStyle('A1:M1')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setRGB('615e97');

        $sheet->getStyle('A1:M1')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

        $sheet->getStyle('T1:U1')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setRGB('615e97');

        $sheet->getStyle('T1:U1')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

        $sheet->getStyle('AD1:AE1')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setRGB('615e97');

        $sheet->getStyle('AD1:AE1')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

        $sheet->getStyle('AJ1:AP1')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setRGB('615e97');

        $sheet->getStyle('AJ1:AP1')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

        for ($i=2; $i<=(count($PayRollArray)+1); ++$i) {
            if ($PayRollArray[$i-2]->getSumm() > 0) {
                $sheet->setCellValue('AQ' . $i, '=G' . $i . '-J' . $i . '-M' . $i . '+T' . $i . '+U' . $i . '+AD' . $i . '+AE' . $i . '+AJ' . $i . '+AK' . $i . '-AL' . $i . '-AN' . $i . '-AO' . $i . '-AI' . $i);
            } else {
                $sheet->setCellValue('AQ' . $i, '0');
            }
            $sheet->setCellValue('AR'.$i, '=IF(AQ'.$i.'>0,ROUND(AQ'.$i.',-1),0)');
            $sheet->setCellValue('AS'.$i, '=AP'.$i."-AR".$i);
        }

        $sheet->setAutoFilter('A1:AR1');

//        foreach (range('A', $sheet->getHighestDataColumn()) as $col) {
//            $sheet->getColumnDimension($col)
//                ->setAutoSize(true);
//        }
//        foreach(range('A', $sheet->getHighestDataColumn()) as $columnID) {
        for ($columnID = 'A'; $columnID != $spreadsheet->getActiveSheet()->getHighestColumn(); $columnID++) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xls($spreadsheet);
        $writer->save($this->filename);

        return $this->filename;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
        $this->dt = DateTime::createFromFormat('d.m.Y',"01.".$date);
    }

    /**
     * @param mixed $type_of_payment
     */
    public function setTypeOfPayment($type_of_payment)
    {
        $this->type_of_payment = $type_of_payment;
    }

}