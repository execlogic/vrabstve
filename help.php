<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 01.08.19
 * Time: 12:51
 */
require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Отделы компании</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>

    <div class="content-wrapper">
        <section class="content">

            <!-- Default box -->
            <div class="box box-solid box-default">
                <!-- /.box-header -->
                <div class="box-body">
                    <h5>Техническая документация</h5>
                    <ul>
                        <li><a href="#" data-toggle="modal" data-target="#modal-navigation" > Навигация </a></li>
                        <li><a href="#" data-toggle="modal" data-target="#modal-sotrudnik" > Сотрудники компании </a></li>
                        <li><a href="#" data-toggle="modal" data-target="#modal-addsotrudnik" > Добавление сотрудников </a></li>
                        <li><a href="#" data-toggle="modal" data-target="#modal-profile" > Профиль сотрудника </a></li>
                        <ul>
                            <li><a href="#" data-toggle="modal" data-target="#modal-avanse" > Аванс </a></li>
                            <li><a href="#" data-toggle="modal" data-target="#modal-premiya" > Премия </a></li>
                            <!--                        <li><a href="#" data-toggle="modal" data-target="#modal-avanse" > Аванс2 </a></li>-->
                        </ul>
                        <li><a href="#" data-toggle="modal" data-target="#modal-planovaya" > Плановые выплаты </a></li>

                    </ul>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="modal fade" id="modal-navigation">
                <div class="modal-dialog" style="width: 90%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title">Навигация</h3>
                        </div>
                        <div class="modal-body">
                                <p>
                                    После ввода логина и пароля мы входим в программу. Первая страница это сотрудники компании, на ней мы можем увидеть всех сотрудников работающих в компании.

                                    Окно делится на 2 панели:
                                </p>
                                <ol>
                                    <li> левая - навигация;</li>
                                    <li> правая - данные.</li>
                                </ol>
                                <p>
                                    Рассмотрим левую панель навигации.
                                </p>
                                <img style="width: 100%" src="help/Screenshot_20190605_181549.png" />

                                <p>Ссылки на левой панели выводится в зависимости от доступов.
                                    Основные рабочие ссылки:</p>
                                <ul>
                                    <li>Список пользователей, в котором вы можете найти своего сотрудника, просмотреть его профиль или внести изменения.</li>
                                    <li>Блок отчетов</li>
                                    <li>Плановые выплаты - ведомости аванса, премии за указанную дату.</li>
                                </ul>
                                <img style="width: auto" src="help/Screenshot_20190605_181618.png" />
                                <p>На верхней панели, если нажать на себя, откроется окно в котором можно перейти в свой профиль (не у администратора), а так же выйти из программы.</p>
                                <img style="width: auto" src="help/Screenshot_20190605_181652.png">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-sotrudnik">
                <div class="modal-dialog" style="width: 90%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title">Сотрудники компании</h3>
                        </div>
                        <div class="modal-body">
                            <p>
                                На данной панели выводятся все сотрудники, сортируются в порядке добавления.
                            </p>
                            <img style="width: 100%" src="help/Screenshot_20190605_181737.png" >
                            <p>
                                Так же можно воспользоваться настройкой
                            </p>
                            <img src="help/Screenshot_20190605_181759.png">
                            <p>Которая содержит возможность показывать, так же уволенных сотрудников.</p>
                            <img src="help/Screenshot_20190605_181821.png">

                            <img style="width: 100%" src="help/Screenshot_20190605_181842.png">

                            <p>Справа есть возможность искать по таблице сотрудников.
                                Ищутся все возможные вхождения слова.</p>
                            <img style="width: 100%" src="help/Screenshot_20190605_181920.png">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-profile">
                <div class="modal-dialog" style="width: 90%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title">Профиль сотрудника</h3>
                        </div>
                        <div class="modal-body">
<!--                            <p class="text-red">-->
<!--                                Доступ разрешен, только у администратора системы и отдела персонала.-->
<!--                            </p>-->

                            <img style="width: auto" src="help/Screenshot_20190605_182856.png">

                            <p>
                                Профиль с краткой информацией.
                            </p>

                            <img style="width: auto" src="help/Screenshot_20190606_183442.png">

                            <p>При наличии доступа, в профиле доступны кнопки:</p>
                            <ul>
                                <li>Изменить - корректировка досье сотрудника;</li>
                                <li>История изменений;</li>
                                <li>Тип расчета - расчет аванса, премии из профиля сотрудника.</li>
                            </ul>

                            <img style="width: auto" src="help/Screenshot_20190605_182944.png">

                            <h3> Корректировка</h3>

                            <p>
                                Изменение в профиле сотрудника разделены на несколько вкладок, которые расположены справа.
                                Все внесенные или измененные данные сохраняются, таким образом есть возможность посмотреть кто и когда вносил изменения.

                            </p>

                            <h5>Вкладка Основное</h5>
                            <p>
                                В данной вкладке есть возможность изменить фамилию, имя, отчество сотрудника, итд
                            </p>

                            <ul>
                                <li>Сотрудник</li>
                                <li>Личные данные</li>
                                <li>Работа</li>
                                <li>Финансовые условия</li>
                            </ul>
                            <img style="width: auto" src="help/Screenshot_20190605_183016.png">

                            <h5>Вкладка Личное</h5>
                            <p>В данной вкладке мы можем изменить пол сотруднику(21 век на дворе), дату рождения, национальность.
                                А так же добавить личные телефоны для связи с ним.
                            </p>

                            <img style="width: auto" src="help/Screenshot_20190605_183041.png">

                            <h5>Вкладка Работа</h5>
                            <p>
                                В данной вкладке есть возможность изменения направления, отдела и должности сотрудника, управляет отдел персонала.
                                ИТ-отделом заносятся данные по логину и имени в 1С Склад 7.7.
                                Номер по зарплате и кадрам заносит отдел персонала.
                                Так же имеется возможность занесения рабочих телефонов, это может сделать как сам сотрудник, так и его непосредственный руководитель.
                            </p>

                            <img style="width: auto" src="help/Screenshot_20190605_183116.png">

                            <h5>Вкладка финансовые условия</h5>
                            <p>
                                Одна из основных вкладок это финансовые условия сотрудника.
                                В этой вкладке находятся такие параметры как оклад, базовый процент, отчисления в годовой бонус.
                                А так же премии, которые будут ежемесячно начисляться сотруднику.
                                Руководитель отдела заполняет и следит за актуальностью данных, по своим сотрудникам, самостоятельно.
                                Сотрудник, в свою очередь, может только смотреть, какие значения, на текущий момент, у него выставлены.
                                Данные в этом блоке применяются сразу, при условии что ведомость не была проведена до окончательной стадии("В расходы").
                                Отчисления в годовой бонус - процент или фиксированная сумма которая будет удерживаться с премии для выплаты в по окончанию года.
                            </p>

                            <img style="width: auto" src="help/Screenshot_20190702_132703.png">
                            <p>
                                Ежемесячные начисления Административной премии и Мобильной связи.
                                Данные параметры будут автоматически подставляться в ведомости, если в финансовой ведомости изменить значение,
                                то оно перебьет данные выставленные в профиле.
                            </p>
                            <img style="width: auto" src="help/Screenshot_20190702_133126.png">
                            <p>
                                В зависимости от мотивации (см. ниже), появляется блок "Расширенных финансовых условий".
                                В этом блоке можно более точно выставить процент по таким параметрам, как статгруппы и торговые точки.
                                Расширенные проценты для статгрупп. Используется у менеджера, бренд-менеджера, для более гибкого управления процентами с реализации по статгруппам.
                            </p>

                            <img style="width: auto" src="help/Screenshot_20190702_133246.png">

                            <p>Расширенные проценты по торговым точкам.
                                Используется у руководителя, при этом базовый процент не выставляется, иначе он применится ко всем торговым точкам.
                                Руководителю выставляется процент строго по его торговой точке.
                            </p>

                            <img style="width: auto" src="help/Screenshot_20190702_133829.png">

                            <h5>Блок KPI.</h5>
                            <p>В данном блоке можно добавить премии при достижения поставленных целей.</p>

                            <img style="width: auto" src="help/Screenshot_20190702_134934.png">

                            <h5>Мотивация сотрудника</h5>
                            <p>В данном блоке мы можем выбрать мотивацию у сотрудника, от нее зависит как будет рассчитываться премия.
                                Мы выделили основные мотивации которые присутствуют в компании.
                                В зависимости от мотивации мы сделали следующие формулы:
                            </p>

                            <ul>
                                <li>Мотивация менеджер - установленный процент * на наценку статгруппы (личная)
                                    Используется у всех продающих менеджеров</li>
                                <li>Мотивация от ТТ - установленный процент * на наценку торговой точки
                                    Используется у руководителей</li>
                                <li>Мотивация от всего * коэффициент - установленный процент * сумму с наценки всех торговых точке * коэффициент
                                    Используется в отделе поставок и интернет магазине.</li>
                                <li>Мотивация бренд менеджер - установленный процент * на наценку статгруппы по всем торговым точкам
                                    Используется у бренд менеджеров</li>
                                <li>Мотивация сервис менеджера - установленный процент * сумму наценки со всех сервис менеджеров
                                    Используется в службе сервиса, при продаже услуг.</li>
                                <li>Произвольное - пустое поле для заполнения вручную
                                    Используется у сотрудников, которые не имеют связи с % от продаж.</li>
                                <li>Мотивация Услуги - Мотивация Сервис менеджера без выплаты коммерческой части
                                    Данная мотивация не учитывает коммерческий блок, а только вычисляет его.</li>
                            </ul>
                            <img style="width: auto" src="help/Screenshot_20190605_184054.png">

                            <h5>Роль сотрудника</h5>
                            <p>Это права доступа в текущей программе которые позволяют изменять те или иные параметры.
                                Мы описали основные роли сотрудников:</p>
                            <ul>
                                <li>Персонал - добавление, увольнение сотрудников изменение рабочих реквизитов и телефонов, управление УРВ, а так же добавление НДФЛ и исполнительных листов.</li>
                                <li>Руководитель - позволяет исправлять данные по своим сотрудникам, а так же делать предварительный расчет финансовых выплат.</li>
                                <li>Кассир - получает данные по разбивке на конверты и подтверждает данные по выдаче денежных сумм руководителям.</li>
                                <li>Ответственный директор - сотрудник который выдает конверты и контролирует работу руководителей, проверяет и проводить ведомость до директора.</li>
                                <li>Директор - обладаем максимальными полномочиями по управлению пользователями и финансовыми ведомостями.</li>
                                <li>Root - Все полномочия включая системное администрирование</li>
                                <li>Настройки - данная роль позволяет управлять настройками программы, которые скрыты от остальных, такие как добавление отделов, статгруппы, мотивации итд</li>
                            </ul>

                            <img style="width: auto" src="help/Screenshot_20190605_184148.png">

                            <h5>Последняя вкладка рабочий статус</h5>
                            <p>
                                В данной вкладке сотрудник из отдела персонала, руководитель или директор может выбрать рабочий статус сотрудника.
                                Основные статусы Работает, Сдает дела, Декрет, Увольнение.
                                Расчет премии осуществляется только если сотрудник имеет статус "Работает".
                            </p>

                            <img style="width: auto" src="help/Screenshot_20190605_184215.png">
                            <img style="width: auto" src="help/Screenshot_20190605_184314.png">
                            <p>При увольнении можно выбрать причину увольнения, а так же написать комментарий.</p>
                            <img style="width: auto" src="help/Screenshot_20190605_184358.png">

                            <h5>История</h5>
                            <p>
                                История по сотруднику.
                                Все измененные данные сохраняются. Если сотрудник удалит по себе данные, то в программе это останется.
                            </p>

                            <img style="width: auto" src="help/Screenshot_20190605_184435.png">

                            <p>Сохраняются данные по всем вкладкам, например изменение мотивации.</p>

                            <img style="width: auto" src="help/Screenshot_20190605_184457.png">

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-addsotrudnik">
                <div class="modal-dialog" style="width: 90%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title">Профиль сотрудника</h3>
                        </div>
                        <div class="modal-body">
                            <p class="text-red">
                                Доступ разрешен, только у администратора системы и отдела персонала.
                            </p>
                            <p>
                                Добавление сотрудника разбито на 4 таба, которые разделены логически.
                                Все поля которые отмечены звездочкой являются обязательными для заполнения.
                            </p>

                            <ul>
                                <li>Сотрудник</li>
                                <li>Личные данные</li>
                                <li>Работа</li>
                                <li>Финансовые условия</li>
                            </ul>

                            <img style="width: 100%" src="help/Screenshot_20190605_181957.png">

                            <p>В табе финансовых условий, сотрудник отдела персонала, добавляет оклад на ИСПЫТАТЕЛЬНЫЙ срок, а так же добавляет в комментарии сколько будет оклад после испытательного срока.</p>

                            <img src="help/Screenshot_20190605_182127.png">

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-avans">
                <div class="modal-dialog" style="width: 90%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title">Тип расчета - Аванс</h3>
                        </div>
                        <div class="modal-body">
                            <p>
                                Самый просто тип расчета в котором учитывается оклад и УРВ(учет рабочего времени).

                                Чтобы посмотреть и рассчитать данные по сотруднику, необходимо выбрать дату на которую будет осуществляться расчет
                            </p>

                            <img style="width: 100%" src="help/Screenshot_20190605_185524.png" />

                            <p>Данная страница имеет 2 вкладки, предварительный расчет, в котором выводятся все данные.</p>
                            <img style="width: 100%" src="help/Screenshot_20190605_185607.png" />

                            <p>Вкладка УРВ позволяет изменять данные по УРВ. Устанавливать Отсутствия, Отпуска, Болезни.
                                Если в УРВ не было заполнено, то считается что сотрудник присутствовал все дни.</p>
                            <img style="width: auto" src="help/Screenshot_20190605_185643.png" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-premiya">
                <div class="modal-dialog" style="width: 90%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title">Тип расчета - Премия</h3>
                        </div>
                        <div class="modal-body">
                            <p>
                                Расчет премии, один из сложных блоков который требует внимания и заполнения.
                                Данные в премию заносятся руководителем отдела, а так же директоратом.
                                УРВ заносит отдел персонала.
                                Коммерческий блок выгружается из 1С каждую ночь.

                                Для просмотра или расчета премии необходимо выбрать дату на которую осуществляется расчет.

                                Премия имеет вкладки: Предварительный расчет, Коммерческий блок, KPI, Административный блок, Удержания, Корректировки УРВ и история изменений данных.
                                Коммерческий блок и KPI показывается в зависимости от мотивации сотрудника.
                            </p>

                            <img style="width: 100%" src="help/Screenshot_20190606_171552.png" />

                            <p>Коммерческий блок содержит реализации выгруженные из 1С.
                                В данном блоке есть возможность гибко управлять процентами в текущей выплате.
                                Устанавливать бонусы клиентам, а так же корректировки наценки.</p>

                            <img style="width: 100%" src="help/Screenshot_20190606_172226.png" />
                            <p>Корректировка наценки является знакозависимой и может быть как с положительным знаком, так и с отрицательным, т.е. можно занести -30 у.е так и 30 у.е.
                                При нажатии на кнопку комментирования, можно добавить информацию почему добавлено то или иное значение.</p>
                            <img style="width: 100%" src="help/Screenshot_20190606_172301.png" />

                            <p>Бонусы клиентам заносятся в у.е. и в всегда вычитаются у сотрудника. Например если занести 30 у.е, то эта сумма будет вычитаться в расчете.
                                Так же обладает возможностью комментирования.</p>
                            <img style="width: 100%" src="help/Screenshot_20190606_172555.png" />
                            <img style="width: auto" src="help/Screenshot_20190606_172658.png" />
                            <p>При подведении мышки к заполненному полю будет показан комментарий.</p>
                            <img style="width: auto" src="help/Screenshot_20190606_172717.png" />

                            <h5>KPI</h5>
                            <p>В данном блоке подтягиваются данные из профиля пользователя с возможностью корректировки.
                                Изменение данных в поле которое взято из профиля сотрудника перебивается и считается основным при расчете.</p>
                            <img style="width: 100%" src="help/Screenshot_20190606_172959.png" />

                            <img style="width: auto" src="help/Screenshot_20190606_173052.png" />

                            <h5>Административный блок</h5>
                            <p>Административный блок имеет основные поля такие как Акции, Административная премия, Обучение, Премия другого отдела и др.
                                Данные по Административной премии и Мобильной связи подтягиваются из профиля сотрудника и могут быть переопределены в текущей дате.
                                Все поля имеют возможность заносить комментарий.
                                Если есть несколько административных премий, то они суммируются и в комментарий заносятся данные за что получает сотрудник ту или иную премию.</p>
                            <img style="width: 100%" src="help/Screenshot_20190606_173212.png" />

                            <h5>Удержания</h5>
                            <p>В удержания заносятся данные которые будут вычитаться из премии сотрудника.</p>
                            <img style="width: 100%" src="help/Screenshot_20190606_173319.png" />

                            <h5>Корректировки</h5>
                            <p>Блок корректировок - данные по корректировкам бонуса, расчета и отчисления в годовой бонус.</p>
                            <img style="width: 100%" src="help/Screenshot_20190606_173347.png" />

                            <h5>УРВ</h5>
                            <p>Блок УРВ(учет рабочего времени) заполняется отделом персонала в данном блоке заполняются отсутствия, отпуска и болезни.
                                А так же НДФЛ и исполнительные листы.
                            </p>
                            <img style="width: 100%" src="help/Screenshot_20190606_173439.png" />

                            <h5>История</h5>
                            <p>Блок истории изменений.
                                Отображает данные которые были изменены руководителем во всех вкладках.
                            </p>
                            <img style="width: 100%" src="help/Screenshot_20190606_173529.png" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-planovaya">
                <div class="modal-dialog" style="width: 90%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title">Плановая выплата</h3>
                        </div>
                        <div class="modal-body">
                            <p>
                                Плановые ведомости - основной инструмент при расчете выплаты сотрудникам.
                                На этой странице, в зависимости от роли, выводятся сотрудники и данные по расчету им.

                                Чтобы посмотреть плановую ведомость необходимо выбрать дату и тип выплаты.
                            </p>

                            <img style="width: 100%" src="help/Screenshot_20190606_174435.png" />

                            <p>Ведомость имеет понятия статуса которые разделены на:</p>
                            <ul>
                                <li>Внесение данных - данный статус становится активным когда: в авансе занесены данные по УРВ; премии - когда внесены данные по УРВ, НДФЛ и подгружен коммерческий блок из 1С.
                                    На этом этапе руководители могут вносить данные по сотрудникам в блоках коммерческий, административный, удержания, корректировки.
                                    После внесения данных в руководитель нажимает кнопку внизу ведомости "На проверку отв. директору".
                                    Ведомость переходит на следующий этап когда все руководители отправят данные на проверку.</li>
                                <li>Проверка ответственным директором - на этом этапе ответственный директор проверяет данные которые были внесены руководителями и отправляет на утверждение директоратом.</li>
                                <li>Утверждение директоратом - проверка и внесение данных директором, а так же предварительный расклад по конвертам, после подтверждения ведомость переходит на следующий статус.</li>
                                <li>Отправлено в кассу - на этом этапе кассир выдает из кассы необходимую сумму в конвертах и отправляет на следующую стадию.</li>
                                <li>Выплачено - стадия на которой деньги выданы из кассы, на этой стадии ведомости директор отправляет ведомость в расходы и все данные фиксируются в базе, после данные невозможно изменять, так как считается что они были выплачены сотрудникам.</li>
                            </ul>
                            <img style="width: 100%" src="help/Screenshot_20190606_174554.png" />
                            <p>Список сотрудников с предварительным расчетом данных по ним.
                                При нажатии на ФИО сотрудника вы попадаете в детальный расчет, такой же как если бы перешли из профиля.</p>
                            <img style="width: 100%" src="help/Screenshot_20190606_174625.png">
                            <p>В конце ведомости выводится итоговая сумма, а так же кнопки управления ведомостью.
                                Директорат может вернуть ведомость на предыдущую стадию.</p>
                            <img style="width: 100%" src="help/Screenshot_20190606_174654.png">
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
    <?php require_once 'footer.php'; ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
