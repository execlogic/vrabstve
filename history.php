<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 07.08.18
 * Time: 13:51
 */
require_once 'app/HistoryInterface.php';
require_once 'app/MemberHistory.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";
require_once 'app/Notify.php';
require_once "app/MemberInterface.php";

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();


// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());


if ($_REQUEST['id']) {
    $user_id = $_REQUEST['id'];
    $member_id = $_REQUEST['id'];
    if (!is_numeric($member_id)) {
        echo "Error!";
        die();
    }
    $HI = new HistoryInterface();
    $MI = new MemberInterface();
    $member = $MI->fetchMemberByID($member_id);
} else {
    echo "Нету такого пользователя!";
    exit();
}


$Permission_ChangeMainInfo = 0; // Изменение основного (фото, почта, тел, мобильные тел., аварийный тел.)
$Permission_ChangeWorkPersonal = 0; // Изменение рабочего и личного
$Permission_ChangeFinanceMotivation = 0; // Изменение фин. условий и мотивации
$Permission_ChangeWorkStatus = 0; // Изменение рабочего статуса
$Permission_Settings = 0; // Настройки, в нашем случае ролей
$Permission_ChangeFinanceProbation = 1; // Изменение комментария в начальных условиях

array_filter($Roles, function ($Role) use (&$Permission_Settings) {
    if ($Role->getSettings() == 1) {
        $Permission_Settings = 1;
        return true;
    };
    return false;
});

//$department_list = $member->getDepartment();
//$member_department_id = end($department_list)['id'];

$member_department_id = $member->getDepartment()['id'];

if ($User->getMemberId() != $member_id) {
    if (!array_filter($Roles, function ($Role) use ($member_department_id) {
        return (($Role->getChangeMember() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
    })) {
        header("Location: 404.php");
    }
}

array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ChangeWorkPersonal, &$Permission_ChangeMainInfo) {
    if (($Role->getChangeWorkPersonal() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
        $Permission_ChangeWorkPersonal = 1; // Можно изменять личные данные и рабочие данные
        $Permission_ChangeMainInfo = 1; // Можно изменять почтовые адреса, телефоны итд
        return true;
    }
    return false;
});

array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ChangeFinanceMotivation) {
    if (($Role->getChangeFinanceMotivation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
        $Permission_ChangeFinanceMotivation = 1;
        return true;
    }
    return false;
});

array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ChangeWorkStatus) {
    if (($Role->getChangeWorkStatus() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
        $Permission_ChangeWorkStatus = 1;
        return true;
    }
    return false;
});
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>История</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Profile Styles -->
    <link rel="stylesheet" href="../../css/profile.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/history.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                <a style="color: #333" href="profile.php?id=<?=$member->getId()?>">История изменений в досье сотрудника <?=$member->getLastName()." ".$member->getName()." ".$member->getMiddle()?></a>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Список пользователей</a></li>
                <li><a href="profile.php?id=<?php echo $member_id; ?>">Профиль сотрудника</a></li>
                <li class="active">История</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-9">
                    <div class="tab-content">
                        <!--                                                            Основное-->
                        <div class="tab-pane active" id="tab_1">
                            <?php
                            if (!empty($history = $HI->getFio($member_id))) {
                                echo "";
                                ?>
                                <div class="box box-solid box-default">
                                    <div class="box-body">
                                        <h5>Изменение ФИО</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Фамилия</th>
                                                <th>Девичья фамилия</th>
                                                <th>Имя</th>
                                                <th>Отчество</th>
                                                <th class="col-md-4">Дата изменения</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                if ($value['maiden_name']) {
                                                    echo '<tr><td>' . $value['lastname'] . '</td><td> ' . $value['maiden_name'] . '</td><td> ' . $value['name'] . '</td><td>' . $value['middle'] . '</td><td>' . $value['date'] . '</td><td>' . $ChangerName . '</td></tr>';
                                                } else {
                                                    echo '<tr><td>' . $value['lastname'] . '</td><td></td><td> ' . $value['name'] . '</td><td> ' . $value['middle'] . '</td><td>' . $value['date'] . '</td><td>' . $ChangerName . '</td></tr>';
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if ($member->getEmails()) {
                                ?>
                                <div class="box box-solid box-default">
                                    <div class="box-body">
                                        <h5>Удаленные почтовые адреса</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Email</th>
                                                <th>Заметка</th>
                                                <th>Дата</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($member->getEmails() as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                echo '<tr><td>' . $value['email'] . '</td><td> ' . $value['note'] . '</td><td> ' . $value['date'] . '</td><td>' . $ChangerName . '</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>

                        <!--                                                            Личное-->
                        <div class="tab-pane" id="tab_2">

                            <?php
                            if (!empty($history = $HI->getNations($member_id))) {
                                ?>
                                <div class="box box-solid">
                                    <div class="box-body">
                                        <h5>Изменение национальности</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Национальность</th>
                                                <th>Дата начала</th>
                                                <th>Дата конца</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                if (strpos($value['end_date'],'9999-01-01') !== false)
                                                    $value['end_date'] = '';
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'"><td>' . $value['name'] . '</td><td> ' . $value['start_date'] . '</td><td>' . $value['end_date'] . '</td><td>' . $ChangerName . '</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (!empty($history = $HI->getPersonalPhones($member_id))) {
                                ?>
                                <div class="box box-solid">
                                    <div class="box-body">
                                        <h5>Личные телефоны</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Телефон</th>
                                                <th>Заметка</th>
                                                <th>Дата</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                $str = $value['phone'];
                                                $str = '+7'.'('.substr($str, 0, 3).')'.substr($str, 3, 3).'-'.substr($str, 6, 2).'-'.substr($str, 8, 2);
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'"><td>' . $str . '</td><td> ' . $value['note'] . '</td><td>' . $value['date'] . '</td><td>' . $ChangerName . '</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (!empty($history = $HI->getPersonalMobilePhones($member_id))) {
                                ?>
                                <div class="box box-solid">
                                    <div class="box-body">
                                        <h5>Мобильные телефоны</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Телефон</th>
                                                <th>Заметка</th>
                                                <th>Дата</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                $str = $value['phone'];
                                                $str = '+7'.'('.substr($str, 0, 3).')'.substr($str, 3, 3).'-'.substr($str, 6, 2).'-'.substr($str, 8, 2);
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'"><td>' . $str . '</td><td> ' . $value['note'] . '</td><td>' . $value['date'] . '</td><td>' . $ChangerName . '</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (!empty($history = $HI->getEmergencyPhones($member_id))) {
                                ?>
                                <div class="box box-solid">
                                    <div class="box-body">
                                        <h5>Аварийные телефоны</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Телефон</th>
                                                <th>Заметка</th>
                                                <th>Дата</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                $str = $value['phone'];
                                                $str = '+7'.'('.substr($str, 0, 3).')'.substr($str, 3, 3).'-'.substr($str, 6, 2).'-'.substr($str, 8, 2);
                                                echo '<tr><td>' . $str . '</td><td> ' . $value['note'] . '</td><td>' . $value['date'] . '</td><td>' . $ChangerName . '</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>

                        <!--                                                            Работа-->
                        <div class="tab-pane" id="tab_3">
                            <?php
                            if (!empty($history = $HI->getWork($member_id))) {
                                ?>
                                <div class="box box-solid box-default">
                                    <div class="box-body">
                                        <h5>Направление, отдел, должность </h5>
                                        <table class="table">
                                            <thead>
                                            <th>Направление</th>
                                            <th>Отдел</th>
                                            <th>Должность</th>
                                            <th>Дата начала</th>
                                            <th>Дата окончания</th>
                                            <th>Кто внес изменения</th>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $key => $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                if (strpos($value['date_e'],'9999-01-01') !== false)
                                                    $value['date_e'] = '';
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'">';
                                                echo "<td>" . $value['direction_name'] . "</td><td>" . $value['department_name'] . "</td><td>" . $value['position_name'] . "</td><td>" . $value['date_s'] . "</td><td>" . $value['date_e'] . "</td><td>". $ChangerName."</td>";
                                                echo "</tr>";
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (!empty($history = $HI->getWorkPhone($member_id))) {
                                ?>
                                <div class="box box-solid box-default">
                                    <div class="box-body">
                                        <h5>Рабочий телефон</h5>
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Телефон</th>
                                                <th>Заметка</th>
                                                <th>Дата</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                $str = $value['phone'];
                                                $str = '+7'.'('.substr($str, 0, 3).')'.substr($str, 3, 3).'-'.substr($str, 6, 2).'-'.substr($str, 8, 2);

                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'"><td>' . $str . '</td><td> ' . $value['note'] . '</td><td>' . $value['date'] . '</td><td>' . $ChangerName . '</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (!empty($history = $HI->getWorkMobilePhone($member_id))) {
                                ?>
                                <div class="box box-solid box-default">
                                    <div class="box-body">
                                        <h5>Рабочий мобильный телефон</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Телефон</th>
                                                <th>Заметка</th>
                                                <th>Дата</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                if (strpos($value['date'],'9999-01-01') !== false)
                                                    $value['date'] = '';
                                                $str = $value['phone'];
                                                $str = '+7'.'('.substr($str, 0, 3).')'.substr($str, 3, 3).'-'.substr($str, 6, 2).'-'.substr($str, 8, 2);
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'"><td>' . $str . '</td><td> ' . $value['note'] . '</td><td>' . $value['date'] . '</td><td>' . $ChangerName . '</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (!empty($history = $HI->getWorkLocalPhone($member_id))) {
                                ?>
                                <div class="box box-solid box-default">
                                    <div class="box-body">
                                        <h5>Внутренний номер</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th class="col-md-4">Номер</th>
                                                <th class="col-md-4">Дата изменения</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                if (strpos($value['date'],'9999-01-01') !== false)
                                                    $value['date'] = '';
                                                $str = $value['phone'];
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'"><td>' . $str . '</td><td>' . $value['date'] . '</td><td>' . $ChangerName . '</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>

                        <!--                                                            Фин. условия-->
                        <div class="tab-pane" id="tab_4">
                            <?php
                            if (!empty($history = $HI->getSalary($member_id))) {
                                ?>
                                <div class="box box-solid">
                                    <div class="box-body">
                                        <h5>Оклад</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Оклад</th>
                                                <th>Дата начала</th>
                                                <th>Дата конца</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                if (strpos($value['date_e'],'9999-01-01') !== false)
                                                    $value['date_e'] = '';
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'">';
                                                echo '<td>' . number_format($value['salary'], 2, ",", " ") . '</td>';
                                                echo '<td>' . $value['date_s'] . '</td>';
                                                echo '<td>' . $value['date_e'] . '</td>';
                                                echo '<td>' . $ChangerName . '</td>';
                                                echo '</tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (!empty($history = $HI->getProcentAvans($member_id))) {
                                ?>
                                <div class="box box-solid">
                                    <div class="box-body">
                                        <h5>% Аванса</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>%</th>
                                                <th>Дата начала</th>
                                                <th>Дата конца</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                if (strpos($value['date_e'],'9999-01-01') !== false)
                                                    $value['date_e'] = '';
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'"><td>' . $value['procent'] . '</td><td> ' . $value['date_s'] . '</td><td>' . $value['date_e'] . '</td><td>' . $ChangerName . '</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (!empty($history = $HI->getMemberAvans($member_id))) {
                                ?>
                                <div class="box box-solid">
                                    <div class="box-body">
                                        <h5>Выплата аванса</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Логическое</th>
                                                <th>Дата начала</th>
                                                <th>Дата конца</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($member->getAvansPay() as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                if ($value['bool']==1) {
                                                    $AvPay = "Да";
                                                } else {
                                                    $AvPay = "Нет";
                                                }
                                                if (strpos($value['date_e'],'9999-01-01') !== false)
                                                    $value['date_e'] = '';
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'"><td>' . $AvPay . '</td><td> ' . $value['date_s'] . '</td><td>' . $value['date_e'] . '</td><td>' . $ChangerName . '</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (!empty($history = $HI->getProcent($member_id))) {
                                ?>
                                <div class="box box-solid">
                                    <div class="box-body">
                                        <h5>Базовый процент</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Процент</th>
                                                <th>Дата начала</th>
                                                <th>Дата конца</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                if (strpos($value['date_e'],'9999-01-01') !== false)
                                                    $value['date_e'] = '';
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'">';
                                                echo '<td>' . number_format($value['percent'], 2, ",", " ") . '</td>';
                                                echo '<td>' . $value['date_s'] . '</td>';
                                                echo '<td>' . $value['date_e'] . '</td>';
                                                echo '<td>' . $ChangerName . '</td>';
                                                echo '</tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (!empty($Procents = $HI->getAdvancedProcent($member_id, $member->getMotivation()))) {
                            ?>
                            <div class="box box-solid">
                                <div class="box-body">
                                    <h5>Расширенные финансовые условия</h5>
                                    <table class="table table-hover table-striped">
                                        <thead>
                                        <tr>
                                            <th>Процент</th>
                                            <th>ТТ</th>
                                            <th>Дата начала</th>
                                            <th>Дата конца</th>
                                            <th>Кто внес изменения</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($Procents as $value) {
                                            if ($value['changer_id'] == -1) {
                                                $ChangerName = "Администратор";
                                            } else {
                                                $ChangerName =  $value['changer_name'];
                                            }
                                            if (strpos($value['date_e'],'9999-01-01') !== false)
                                                $value['date_e'] = '';
                                            echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'">';
                                            echo '<td>' . number_format($value['procent'], 2, ",", " ") . '</td>';
                                            echo '<td>'. $value['name'] .'</td>';
                                            echo '<td>' . $value['date_s'] . '</td>';
                                            echo '<td>' . $value['date_e'] . '</td>';
                                            echo '<td>' . $ChangerName . '</td>';
                                            echo '</tr>';
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php
                            }
                            ?>

                            <?php
                            if (!empty($history = $HI->getAdministrativePrize($member_id))) {
                                ?>
                                <div class="box box-solid">
                                    <div class="box-body">
                                        <h5>Административная премия</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Сумма</th>
                                                <th>Дата начала</th>
                                                <th>Дата конца</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                if (strpos($value['date_e'],'9999-01-01') !== false)
                                                    $value['date_e'] = '';
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'"><td>' . $value['AdministrativePrize'] . '</td><td> ' . $value['date_s'] . '</td><td>' . $value['date_e'] . '</td><td>' . $ChangerName . '</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (!empty($history = $HI->getMobileCommunication($member_id))) {
                                ?>
                                <div class="box box-solid">
                                    <div class="box-body">
                                        <h5>Мобильная связь</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Сумма</th>
                                                <th>Дата начала</th>
                                                <th>Дата конца</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                if (strpos($value['date_e'],'9999-01-01') !== false)
                                                    $value['date_e'] = '';
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'">';
                                                echo '<td>' . number_format($value['MobileCommunication'], 2, ",", " ") . '</td>';
                                                echo '<td> ' . $value['date_s'] . '</td>';
                                                echo '<td>' . $value['date_e'] . '</td>';
                                                echo '<td>' . $ChangerName . '</td>';
                                                echo '</tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (!empty($history = $HI->getTransport($member_id))) {
                                ?>
                                <div class="box box-solid">
                                    <div class="box-body">
                                        <h5>Транспортные расходы</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Сумма</th>
                                                <th>Дата начала</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                if (strpos($value['date'],'9999-01-01') !== false)
                                                    $value['date'] = '';
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'">';
                                                echo '<td>' . number_format($value['summ'], 2, ',', ' ') . '</td>';
                                                echo '<td>' . $value['date'] . '</td>';
                                                echo '<td>' . $ChangerName . '</td>';
                                                echo '</tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (!empty($history = $HI->getYearBonus($member_id))) {
                                ?>
                                <div class="box box-solid">
                                    <div class="box-body">
                                        <h5>Годовой бонус</h5>
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Тип</th>
                                                <th>Сумма</th>
                                                <th>Заметка</th>
                                                <th>Дата начала</th>
                                                <th>Дата конца</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                switch ($value['type'])
                                                {
                                                    case 1:
                                                        $Type = 'Константа';
                                                        break;
                                                    case 2:
                                                        $Type = 'Процент (поставки с апр. по нояб.)';
                                                        break;
                                                }
                                                if (strpos($value['date_e'],'9999-01-01') !== false)
                                                    $value['date_e'] = '';
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'">';
                                                echo '<td>' . $Type . '</td>';
                                                echo '<td>' . number_format($value['summ'], 2, ',', ' ') . ($value['type']==1?' р.':' %') . '</td>';
                                                echo '<td>' . $value['note'] . '</td>';
                                                echo '<td>' . $value['date_s'] . '</td>';
                                                echo '<td>' . $value['date_e'] . '</td>';
                                                echo '<td>' . $ChangerName . '</td>';
                                                echo '</tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>

<!--                        Мотивация-->
                        <div class="tab-pane" id="tab_5">
                            <?php
                            if (!empty($history = $HI->getMotivation($member_id))) {
                                ?>
                                <div class="box box-solid">
                                    <div class="box-body">
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Мотивация</th>
                                                <th>Дата начала</th>
                                                <th>Дата конца</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($history as $value) {
                                                if ($value['changer_id'] == -1) {
                                                    $ChangerName = "Администратор";
                                                } else {
                                                    $ChangerName = $value['changer_last'] . " " . $value['changer_name'] . " " . $value['changer_middle'];
                                                }
                                                if (strpos($value['date_e'],'9999-01-01') !== false)
                                                    $value['date_e'] = '';
                                                echo '<tr style="color:'.($value['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'"><td>' . $value['motivation_name'] . '</td><td> ' . $value['date_s'] . '</td><td>' . $value['date_e'] . '</td><td>' . $ChangerName . '</td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>

                        <!--                        KPI-->
                        <div class="tab-pane" id="tab_6">
                            <?php
                            if (!empty($kpi = $HI->getKPI($member_id))) {
                                array_multisort( array_column($kpi, 'active'), SORT_ASC, array_column($kpi, 'num'), SORT_ASC, $kpi);
                                ?>
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h4>KPI</h4>
                                    </div>
                                    <div class="box-body">
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>№</th>
                                                <th>Сумма</th>
                                                <th>Комментарий</th>
                                                <th>Дата начала</th>
                                                <th>Дата конца</th>
                                                <th>Кто внес изменения</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($kpi as $item) {
                                                if (strpos($item['date_e'],'9999-01-01') !== false)
                                                    $item['date_e'] = '';
                                                echo '<tr style="color:'.($item['active']?HistoryInterface::$COLOR_ENABLED:HistoryInterface::$COLOR_DISABLED).'">';
                                                echo "<td>" . ($item['num']+1). "</td>";
                                                echo "<td>" . number_format($item['summ'], 2, ',', ' ') ."</td>";
                                                echo "<td>" . $item['note'] . "</td>";
                                                echo "<td>" . $item['date_s']."</td>";
                                                echo "<td>" .$item['date_e']."</td>";
                                                if (!is_null($item['changer_name']))
                                                    echo "<td>".$item['changer_name']."</td>";
                                                if ($item['changer_id']==-1)
                                                    echo "<td>Администратор</td>";
                                                echo "</tr>";
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <?php if ($Permission_ChangeMainInfo) {
                                ?>
                            <li style="width: 100%;" class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Основное</a></li>
                                <?php
                            }
                            ?>
                            <?php if ($Permission_ChangeWorkPersonal) {
                            ?>
                            <li style="width: 100%;" class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Личное</a></li>
                                <?php
                            }
                            ?>

                            <li style="width: 100%;" class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Работа</a></li>
                            <?php if ($Permission_ChangeFinanceMotivation) {
                                ?>
                            <li style="width: 100%;" class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Финансовые условия</a></li>
                            <li style="width: 100%;" class=""><a href="#tab_6" data-toggle="tab" aria-expanded="false">KPI</a></li>
                            <li style="width: 100%;" class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false">Мотивация</a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
