<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.10.18
 * Time: 18:30
 */

// Время работы скрипта
$start = microtime(true);

require_once 'app/MemberInterface.php';
require_once 'app/AwhInterface.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';
require_once 'app/DepartmentInterface.php';

require_once 'app/ReportStatus.php';
require_once 'app/ProductionCalendar.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

// Настройки
$Permission_ReportAWH = 0;
$RoleDepartmentFilter = array();
foreach ($Roles as $Role) {
    if ($Role->getReportAWH() == 1) {
        $Permission_ReportAWH = 1;

        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[] = $Role->getDepartment();
        }
    }
}

if ($Permission_ReportAWH == 0) {
    header("Location: 404.php");
    exit();
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

$date_m = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
$AWH = new AwhInterface();
$DI = new DepartmentInterface();
$DI->fetchDepartments();

//var_dump($AWH_Data);
$date = date("Y-m");
$AWHData = array();
$type_of_payment = 0;

if (isset($_POST['SendDate']) && isset($_POST['date'])) {
    $date = $_POST['date'];
    $type_of_payment = $_POST['TypeOfPayment'];
    if (isset($_POST['department'])) {
        $department_id = filter_input(INPUT_POST, "department", FILTER_VALIDATE_INT, array("options" => array("default" => 0)));
    }

    $AWHData = $AWH->getAwhAndNDFLJournal($date, $type_of_payment, $department_id);

    if (isset($department_id) && $department_id!=0) {
        foreach ($AWHData as $key=>$value) {
            if ($value['department_id'] != $department_id) {
                unset($AWHData[$key]);
            }
        }
    }

    if (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] != 0) {
        foreach ($AWHData as $key=>$value) {
            if (!in_array($value['department_id'], $RoleDepartmentFilter)) {
                unset($AWHData[$key]);
            }
        }
    }

    usort($AWHData, function ($a,$b) {
        return $a['lastname'] > $b['lastname'];
    });

}
$menu_open = 3;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Журнал УРВ</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        thead th {
            position: sticky;
            top: 0;
            background: white;
        }
    </style>
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse" style="overflow-x: auto !important; height: auto; min-height: 100%;">
<div class="wrapper" style="overflow: unset !important; height: auto; min-height: 100%;">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
                Журнал УРВ + НДФЛ/ИЛ за <?php echo $date; ?>
            </h4>
            <ol class="breadcrumb">
                <li class="active">Журнал УРВ + НДФЛ/ИЛ</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="box box-solid box-plane">
                <div class="box-body">
                    <form class="form-horizontal" method="post">

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Выберите дату: </label>
                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker" name="date" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                           value="<?php echo $date; ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Тип выплаты: </label>
                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-check"></i>
                                    </div>
                                    <select class="form-control" name="TypeOfPayment">
                                        <option <?php if ($type_of_payment == 1) { echo "selected"; } ?> value="1">Премия</option>
                                        <option <?php if ($type_of_payment == 2) { echo "selected"; } ?> value="2">Аванс</option>
                                        <option <?php if ($type_of_payment == 3) { echo "selected"; } ?> value="3">Аванс2</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Отдел: </label>
                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-check"></i>
                                    </div>
                                    <select class="form-control" name="department">
                                        <option value="0">Все отделы</option>
                                        <?php

                                        foreach ($DI->GetDepartments() as $d) {
                                            if (isset($department_id) && $d->getId() ==  $department_id) {
                                                echo "<option value='" . $d->getId() . "'' selected>" . $d->getName() . "</option>";
                                            } else {
                                                echo "<option value='" . $d->getId() . "''>" . $d->getName() . "</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2"><input type="submit" class="btn btn-flat btn-default" name="SendDate" value="Получить данные"></div>
                        </div>

                    </form>
                </div>
            </div>

            <?php
            if (count($AWHData) > 0) {
                ?>
                <div class="box box-solid box-plane">
                    <div class="box-body">
                        <table class="table table-striped table-hover" style="overflow-y: auto" id="DataTable">
                            <thead>
                            <tr>
                                <th>ФИО</th>
                                <th>Отдел</th>
                                <th>Отсутствия</th>
                                <th>Отпуск</th>
                                <th>Болезнь</th>
                                <th>НДФЛ</th>
                                <th>ИЛ</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $absence_sum = 0;
                            $holiday_sum = 0;
                            $disease_sum = 0;
                            $ndfl_sum = 0;
                            $ro_sum = 0;
                            foreach ($AWHData as $value) {
                                $absence_sum +=  ($value['absence'] + $value['new_absence']);
                                $holiday_sum += $value['holiday'];
                                $disease_sum += $value['disease'];
                                $ndfl_sum += $value['ndfl'];
                                $ro_sum += $value['ro'];

                                $dep_id = $value['department_id'];

                                if (!empty($value['new_absence']) && $value['new_absence'] != 0) {
                                    echo "<tr style='background-color: #f9f9fd'>";
                                } else {
                                    echo "<tr>";
                                }
                                echo "<td><a href='profile.php?id=" . $value['member_id'] . "'>" . $value['lastname'] . " " . $value['name'] . "</a></td>";
                                echo "<td>" . $value['department_name'] . "</td>";
                                echo "<td>" . number_format(($value['absence'] + $value['new_absence']), 2, ',', ' ') . "</td>";
                                echo "<td>" . number_format($value['holiday'], 2, ',', ' ') . "</td>";
                                echo "<td>" . number_format($value['disease'], 2, ',', ' ') . "</td>";
                                echo "<td>". number_format($value['ndfl'], 2, ',', ' '). "</td>";
                                echo "<td>". number_format($value['ro'], 2, ',', ' '). "</td>";
                                echo "</tr>";
                            }

                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th><?=number_format($absence_sum, 2, ',', ' ')?></th>
                                <th><?=number_format($holiday_sum, 2, ',', ' ')?></th>
                                <th><?=number_format($disease_sum, 2, ',', ' ')?></th>
                                <th><?=number_format($ndfl_sum, 2, ',', ' ')?></th>
                                <th><?=number_format($ro_sum, 2, ',', ' ')?></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <?php
            }
            ?>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
    $(function () {
        // $('#DataTable').DataTable({
        //     'paging': false,
        //     'lengthChange': false,
        //     'searching': false,
        //     'ordering': true,
        //     'info': false,
        //     'autoWidth': true
        // })


        // Выбор даты
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            format: 'yyyy-mm',
        })
    })
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>


