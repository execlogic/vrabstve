<?php

// Время работы скрипта
$start = microtime(true);

require_once 'app/PayRoll.php';
require_once 'app/PayRollInterface.php';
require_once 'app/functions.php';
require_once "app/MemberInterface.php";
require_once 'app/ErrorInterface.php';
require_once 'app/Correction.php';
require_once 'admin/User.php';
require_once "admin/RoleInterface.php";
require_once 'app/Envelopes.php';
require_once "app/EnvelopeInterface.php";
require_once "app/ReportStatus.php";
require_once 'app/Notify.php';
require_once 'app/PayRollInterface.php';

if (!isset($_SESSION)) {
    session_start();
    $User = $_SESSION['UserObj'];
}

if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

$RS = new ReportStatus();
/*
 * Объект ведомости
 */
$PI = new PayRollInterface();

if (isset($_REQUEST['date'])) {
    /*
     * Выкусываем теги
     */
    $_REQUEST['date'] = strip_tags($_REQUEST['date']);
    /*
     * Экранирую html спец. символы
     */
    $_REQUEST['date'] = htmlspecialchars($_REQUEST['date']);
    /*
    * в дате проверяю существование только цифр, и точки
    */
    $_REQUEST['date'] = preg_replace("/[^0-9.]/i", "", $_REQUEST['date']);


    /*
     * Объявляю переменные дата и тип выплаты
     */
    $date = $_REQUEST['date'];
    $TypeOfPayment = 6;

    $EI = new EnvelopeInterface();
    $EI->setDateType($date, $TypeOfPayment);

    $Status = $RS->getStatus($date, $TypeOfPayment);
    if (!isset($Status['status_id']) || empty($Status['status_id'])) {
        /*
         * Создаю статус ведомости и выставляю в 1
         */
        $RS->ChangeStatus($date, $TypeOfPayment, 1, $User->getMemberId());
        $Status['status_id'] = 1;
    }

    /*
        * Создаю объект MemberInterface
        */
    $Members = new MemberInterface();

    /*
     * Получаю список пользователей с минимальными данными
     */
    $Members->fetchMembers();
    $MemberList = $Members->GetMembers();

    if ($Status['status_id'] == 1) {
        /*
         * Всегда будет 1, т.к. ГБ выдается в премию
         */
        $Datas = $PI->getAllFinancialPayout($date, 1);

        foreach ($Datas as $data) {
            if ($data['Total']>200000) {
                try {
                    $member = $Members->fetchMemberByID($data['member_id']);
                } catch (Exception $e) {
                    echo $e->getMessage()."<BR>";
                    exit(255);
                }
                $sum = $data['Total'] - ($data['Total'] % 100000);

                $PayRoll = new PayRoll();
                $PayRoll->setDate($date); // Дата
                $PayRoll->setTypeOfPayment($TypeOfPayment); // Тип выплаты
                $PayRoll->setId($member->getId()); // Пользовательский ID
                $PayRoll->setDirection($member->getDirection()); // Напраление
                $PayRoll->setDepartment($member->getDepartment()); // Отдел
                $PayRoll->setFio($member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()); // ФИО
                $PayRoll->setCorrecting($sum); // Корректировки
                $PayRoll->setSumm($sum); // Корректировки

                //Добавляю в массив объектов
                $PayRollArray_t[] = $PayRoll;
            }
        }

        /*
         * Перемешиваю
         */
        shuffle($PayRollArray_t);
        $EnvData = array();
        foreach ($PayRollArray_t as $item) {
            if ($item->getCorrecting() == 0) {
                continue;
            }

            if (!isset($PayRollArray)) {
                $PayRollArray[500] = $item;
            } else {
                $PayRollArray[] = $item;
            }
        }

        foreach ($PayRollArray as $key => $PayRoll) {
            $Env = new Envelopes(); // Создаю объект конверта
            /*
             * Вообще нужно брать сумму, но у нас только годовой бонус который находится в корректировках
             * поэтому я буду вытаскивать только корректировки
             */
            $Env->setSumm($PayRoll->getCorrecting()); // Устанавливаю остаток от суммы
            /*
             *  ID пользователя
             */
            $Env->setMemberId($PayRoll->getId());
            /*
             * ФИО
             */
            $Env->setName($PayRoll->getFio());
            /*
             * Ключ массива является номером конверта
             */
            $Env->setEnvelopeId($key); // Номер конверта
            $EnvData[] = $Env; // Добавляю в  массив данных
        }
        /*
         * Устанавливаю данные по конвертам
         */
        $EI->setEnvData($EnvData);

    } else {
        $EI->fetch();
        $EI->CatcherCalc();
    }

    if (isset($_POST['Confirm']) && $Status['status_id'] == 1) {
        /*
         * Сохраняю конверты в БД
         */
        try {
            $EI->Confirm();
        } catch (Exception $e) {
            echo $e->getMessage() . "<BR>";
        }

        /*
         * Устанавливаю статус
         */
        $RS->ChangeStatus($date, $TypeOfPayment, 6, $User->getMemberId());
        header('Location: '.  $_SERVER["REQUEST_URI"]);
        die();
    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Конверты годовой бонус</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
                Годовой бонус
            </h4>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Список пользователей</a></li>
                <li><a href="prepaid_expense.php?date=<?php echo $date; ?>&TypeOfPayment=1">Расчет</a></li>
                <li class="active">Годовой бонус</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="box box-info box-solid">
                <div class="box-body" style="overflow-y: auto">
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ФИО</th>
                            <th>Отдел</th>
                            <th class="col-md-2 text-center">Сумма начисленно</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $count = 0;
                        $FullSumm = 0;
                        foreach ($EI->get() as $item) {
                            $M = current(array_filter($MemberList, function ($member) use ($item) {
                                return $member->getId() == $item->getMemberId();
                            }));

                            $FullSumm += $item->getSumm();// Сумма итого
                            echo "<tr>";
                            echo "<td>" . $item->getEnvelopeId() . "</td>";
                            if ($Status['status_id'] == 1) {
                                echo "<td>" . $item->getName() . "</td>";
                            } else {
                                echo "<td>" . $M->getLastName() . " " . $M->getName() . "</td>";
                            }
                            echo "<td>" . $M->getDepartment()['name'] . "</td>";
                            echo "<td class='text-center'>" . number_format($item->getSumm(), 2, ',', ' ') . "</td>";
                            echo "</tr>";

                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="3">Итого:</th>
                            <th class="text-center"><?php echo number_format($FullSumm, 2, ',', ' '); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

                <div class="box-footer">
                    <form class="form-horizontal" method="post">
                        <?php
                        if ($Status['status_id'] == 1) {
                            ?>
                            <input type="submit" class="btn btn-warning btn-flat" name="Confirm" value="Выплачено">
                            <?php
                        }
                        ?>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


<script>
    $(function () {

        $('#MembersTable').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })
    })
</script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>

