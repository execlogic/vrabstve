<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 31.07.18
 * Time: 13:25
 */

// Время работы скрипта
$start = microtime(true);

//require_once 'app/DepartmentInterface.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());
// Настройки
//if (!array_filter($Roles, function ($Role) {
//    return ($Role->getSettings() == 1);
//})) {
//    header("Location: 404.php");
//}


$raw_data = $RI->getSubordination();
$menu_open = 2;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Список отделов, руководителей и отвественных директоров</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

    <!-- Select2 -->
    <link rel="stylesheet" href="../../bower_components/select2/dist/css/select2.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>

    <div class="content-wrapper">


        <section class="content">
            <?php
            /*
             * Вывод информации об ощибках
             */
            if (!empty($Errors)) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        echo "<p>" . $Errors . "</p>";
                        ?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php
            }
            ?>

            <!-- Default box -->
                <div class="box box-default box-solid">
                    <div class="box-body">
                        <table class="table table-striped table-responsive table-hover">
                            <thead>
                                <th>ID</th>
                                <th>Отдел</th>
                                <th>Руководитель</th>
                                <th>Ответсвенный директор</th>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($raw_data as $item) {

                                echo "<tr><td>".$item['id']."</td><td>".$item['name']."</td>";
                                if (!is_null($item['member_id'])) {
                                    echo "<td><a href='profile.php?id=" . $item['member_id'] . " '>" . $item['member_name'] . "</a></td>";
                                } else {
                                    echo "<td></td>";
                                }
                                if (!is_null($item['director_id'])) {
                                    echo "<td><a href='profile.php?id=" . $item['director_id'] . " '>" . $item['director_name'] . "</a></td>";
                                } else {
                                    echo "<td></td>";
                                }
                                echo "</tr>";
                            }
                            ?>
                            </tbody>
                        </table>

                </div>
        </section>
    </div>
</div>

<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="../dist/js/adminlte.min.js"></script>

<script>
$(function () {
    $('.select2').select2()
})
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
