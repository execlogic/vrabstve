<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 05.10.18
 * Time: 13:55
 */

// Время работы скрипта
$start = microtime(true);

require_once "app/ErrorInterface.php";
require_once "app/MemberInterface.php";
require_once 'app/CustomPercent.php';

require_once "app/MemberFinanceBlock.php";
require_once 'app/DataItem.php';
require_once 'app/DataManager.php';
require_once 'app/ErrorInterface.php';
require_once 'app/AdministrativeBlock.php';
require_once 'app/Retention.php';
require_once 'app/Correction.php';

require_once 'app/ProductionCalendar.php';
require_once 'app/AwhInterface.php';
require_once 'app/ReportStatus.php';

require_once 'app/PayRollInterface.php';
require_once 'app/PayRoll.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once "app/Unscheduled.php";
require_once "app/LeavingPremium.php";

require_once 'app/Notify.php';

session_start();

if (isset($_SESSION['UserObj'])) {
    $User = $_SESSION['UserObj'];
} else {
    header("Location: index.php");
    exit();
}

if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

// Настройки
//if (!array_filter($Roles, function ($Role) {
//    return ($Role->getFinancialStatement() == 1);
//})) {
//    header("Location: 404.php");
//}


if (!array_filter($Roles, function ($Role) {
    return $Role->getId() == 3 || $Role->getId() == 5 || $Role->getId() == 6;
})) {
    header("Location: 404.php");
}
//

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

/*
 * Права доступа
 */

/// ..........................

$RoleDepartmentFilter = array(0);

session_write_close();

$Error = new ErrorInterface();

/*
 * Выставляю дату на текущую
 */
$date = date("m.Y");

/*
*  ******************************* Подключаемся к БД
*/
$dbh = dbConnect();

$Ud = new Unscheduled();
$LP = new LeavingPremium();

/*
 * Внеплановая выплата обработка событий
 */
if (isset($_POST['Success'])) {
    $ID = $_POST['Success'];
    try {
        $Ud->IncStatus($ID);
    }catch (Exception $e) {
        echo $e->getMessage()."<BR>";
    }
}


$raw = $Ud->getData(NULL);

if (count($raw)>0) {
    foreach ($raw as $key => $item) {
        if ($item['status_id'] != 4)
            unset($raw[$key]);
    }
}


//if (count($PayRollArray)>0) {
//    foreach ($PayRollArray as $key => $item) {
//        if ($item->getStatusId() != 4)
//            unset($PayRollArray[$key]);
//    }
//}

if (isset($_POST['SuccessLeaving'])) {
    $ID = $_POST['SuccessLeaving'];

    try {
        $LP->IncStatus($ID);
//        $PayRollArray = $LP->getPremium();
    } catch (Exception $e) {
        echo $e->getMessage()."<BR>";
    }

    unset($_POST);
}

if (isset($_POST['SuccessLeavingAll'])) {
    if (isset($PayRollArray) && count($PayRollArray) > 0) {
        foreach ($PayRollArray as $ID => $item) {
            $LP->IncStatus($ID);
        }
        $PayRollArray = $LP->getPremiumCacher();
    }

    unset($_POST);
}

/*
 * Расчет (Leaving Premium)
 */
$PayRollArray = $LP->getPremiumCacher();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Единоразовая выплата(кассир)</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../bower_components/FixedHeader-3.1.4/css/fixedHeader.bootstrap.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">

</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <?php
            if (isset($raw) && count($raw)>0) {
                ?>
                <div class="box box-solid box-pane" style="overflow-y: auto">
                    <div class="box-header">
                        <h4>Внеплановая выплата</h4>
                    </div>
                    <form class="form-horizontal" method="post">
                        <div class="box-body">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th class="col-sm-1">Дата</th>
                                    <th class="col-sm-1">Тип выплаты</th>
                                    <th class="col-sm-1">Статус</th>
                                    <th class="col-sm-2" style="text-align: center;">Сумма(руб)</th>
                                    <th class="col-sm-2">Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($raw)) {
                                    foreach ($raw as $item) {
                                        if ($item['status_id']!=4)
                                            continue;

                                        if (!isset($item['status_name']) || $item['status_name'] == null) {
                                            $item['status_name'] = "Утверждение директоратом";
                                        }
                                        echo "<tr>";
                                        echo "<td>" . $item['date'] . "</td>";
                                        switch ($item['type_of_payment']) {
                                            case 4:
                                                echo "<td>Расчет</td>";
                                                break;
                                            case 5:
                                                echo "<td>Внеплановая выплата</td>";
                                                break;
                                        }

                                        echo "<td>" . $item['status_name'] . "</td>";
                                        echo "<td title='" . $item['note'] . "' style=\"text-align: center;\">". number_format( $item['summ'], 2, ',', ' '). "</td>";
                                        echo "<td>";
                                        ?>
                                        <div class='btn-group'>
                                            <?php

                                            if (array_filter($Roles, function ($Role) {
                                                return $Role->getId() == 3 || $Role->getId() == 5 || $Role->getId() == 6;
                                            })) {
                                                if ($item['status_id'] != 6) {
                                                    ?>

                                                    <button class='btn btn-sm btn-flat btn-success' name='Success' value='<?php echo $item['unscheduled_id'] ?>'>Подтвердить</button>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                        <?php
                                        echo "</td>";
                                        echo "</tr>";
                                    }
                                }
                                ?>
                                </tbody>
                            </table>


                        </div>

                    </form>
                </div>
                <?php
            }
            ?>

            <?php
            if (isset($PayRollArray) && count($PayRollArray) > 0) {
                ?>
                <div class="box box-solid">
                    <div class="box-header">
                        <h4>Расчет сотрудников</h4>
                    </div>
                    <form class="form-horizontal" method="post">
                        <div class="box-body">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th class="col-sm-1">Дата</th>
                                    <th class="col-sm-1">Тип выплаты</th>
                                    <th class="col-sm-1">ФИО</th>
                                    <th class="col-sm-1">Статус</th>
                                    <th class="col-sm-2" style="text-align: center;">Сумма(руб)</th>
                                    <th class="col-sm-2">Действия</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                // Объявляю переменные для суммирвания колонок
                                $ItogoSumm = 0; // Итого сумма

                                foreach ($PayRollArray as $item) {
                                    $ItogoSumm += $item['Total']; // Суммируем Итого

                                    // Выдерение цветом. Если сумма меньше 0, то помечаю желтым
                                    if ($item['Total'] < 0) {
                                        echo "<tr class='warning'>";
                                    } else {
                                        echo "<tr>";
                                    }

                                    echo "<td>" . $item['date'] . "</td>";


                                    switch ($item['type_of_payment']) {
                                        case 1:
                                            echo "<td>Премия</td>";
                                            break;
                                        case 2:
                                            echo "<td>Аванс</td>";
                                            break;
                                        case 4:
                                            echo "<td>Расчет</td>";
                                            echo "<td>".$item['lastname']." ".$item['name']." ".$item['middle']."</td>";
                                            break;
                                    }

                                    echo "<td>".$item['status_name']."</td>";

                                    // Вывожу сумму
                                    echo "<td style='text-align: center;'>" . number_format($item['Total'], 2, ',', ' ') . "</td>";

                                    echo "<td>";
                                    ?>
                                    <div class='btn-group'>
                                        <button class='btn btn-sm btn-flat btn-success' name='SuccessLeaving' value='<?php echo $item['id'] ?>'>Подтвердить</button>
                                    </div>

                                    <?php
                                    echo "</td>";
                                    echo "</tr>";
                                }
                                ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <!--  Вывожу все просуммированное выше   -->
                                    <th style="font-size: 14px" colspan="4">Итого:</th>
                                    <th colspan="2" style='text-align: left; font-size: 14px;'><?php echo number_format($ItogoSumm, 2, ',', ' '); ?></th>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </form>
                </div>
                <?php
            }
            ?>

            <?php
            if (!isset($PayRollArray) && !isset($raw)) {
                ?>
                <div class="box box-solid">
                    <div class="box-body">
                        <h4>Выплат нет</h4>
                    </div>
                </div>
                <?php
            }
            ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="../../bower_components/FixedHeader-3.1.4/js/dataTables.fixedHeader.js"></script>


<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(function () {
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            format: 'mm.yyyy'
        })
    });

    $(document).ready(function () {
        $('#PayrollTable').DataTable({
            fixedHeader: true,
            paging: false,
            lengthChange: false,
            searching: true,
            ordering: true,
            info: false,
            autoWidth: true
        })
    });
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
