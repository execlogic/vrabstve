<?php
/**
*
* Сравнение и отправка руководителей в tophouse.ru
* Отключение уволенных пользователей
*
**/

require_once $_SERVER["DOCUMENT_ROOT"].'/app/MemberInterface.php';

$MI = new MemberInterface();
$MI->fetchMembers(true);
$aAllMembers = $MI->GetMembers();
$aDepartments = [];
$aMembers = [];

foreach ($aAllMembers as $aMember) {
    
    $aData = [
        'id' => $aMember->getId(),
        'name' => $aMember->getName(),
        'lastname' => $aMember->getLastName(),
        'middle' => $aMember->getMiddle(),
        'direction' => $aMember->getDirection(),
        'department' => $aMember->getDepartment(),
        'position' => $aMember->getPosition(),
        'dismissal_date' => ($aMember->getDismissalDate()=='01.01.9999'?null:$aMember->getDismissalDate()),
        'active' => ($aMember->getDismissalDate()=='01.01.9999'?1:0)
    ];
    if ($aMember->getPosition()['id'] == 29 || $aMember->getPosition()['id']==30 || $aMember->getPosition()['id']==41) {
        if ($aMember->getPosition()['id']==41 && isset($aDepartments[$aMember->getDepartment()['id']])) {

        } else {
            $aDepartments[$aMember->getDepartment()['id']] = $aData;
        }
    }
    $aMembers[$aMember->getLastName(). ' ' . $aMember->getName()] = $aData;
}

$dbh = null;
try {
    $dbh = new PDO(
        'mysql' .
        ':host=89.108.116.57' .
        ';port=3306' .
        ';dbname=db_tophouse_10',
        'dbu_tophouse_2',
        'sD$1sNa[lYi=o8y',
        []
    );
//    $dbh = new PDO(
//        'mysql' .
//        ':host=46.228.10.201' .
//        ';port=3306' .
//        ';dbname=db_tophouse_10',
//        'dbu_tophouse_2',
//        'sD$1sNa[lYi=o8y',
//        []
//    );
} catch (PDOException $e) {
    echo "\nPDO::errorInfo():\n";
    echo print_r($e->getMessage(), true);
    exit(0);
}

$query = 'SELECT * FROM th_admin WHERE new_roles REGEXP "(^|,)login(,|$)" and new_roles not REGEXP "(^|,)price_dealer(,|$)" ORDER BY name ASC';
//$query = 'SELECT * FROM th_admin';

$sth = $dbh->prepare($query);
if (!$sth) {
    throw new Exception('Ошибка в PDO');
}
$sth->execute();
$aWebMemberList = $sth->fetchAll(PDO::FETCH_ASSOC);
$aWebMembers = [];
foreach ($aWebMemberList as $item) {
    $aWebMembers[$item['name']] = $item;
}

foreach ($aWebMemberList as $item) {
    if (isset($aMembers[$item['name']])) {
        if (!$aMembers[$item['name']]['active']) {
            $aRoles = explode(",",$item['new_roles']);
            foreach ($aRoles as $key=>$val) {
                if ($val == 'login') {
                    unset($aRoles[$key]);
                }
            }
            $query = "update th_admin set new_roles=? where id=?";
            $args = [
                implode(",", $aRoles),
                $item['id'],
            ];
            $sth = $dbh->prepare($query);
            if (!$sth) {
                throw new Exception('Ошибка в PDO');
            }
            $sth->execute($args);

            echo "<span style='color: red'>Сотрудник " . $item['name']." уволен - ".$aMembers[$item['name']]['dismissal_date']."</span><br>";
        } else {
            if (!in_array($item['type'],[1,2])){
                continue;
            }
            $department_id = $aMembers[$item['name']]['department']['id'];
            if (isset($aDepartments[$department_id])) {
                $leader = $aDepartments[$department_id];
            } else {
                echo "Не найден руководитель у " . $item['name'] . "<BR>";
                continue;
            }

            $leader_fio = $leader['lastname'] . " " . $leader['name'];

            if (isset($aWebMembers[$leader_fio])) {
                echo "Добавляю руководителя " . $item['name'] . " " . $leader['lastname'] . " " . $leader['name'] . " " . $aWebMembers[$leader_fio]['id'] . "<br>";
                $query = "update th_admin set supervisor=? where id = ?";
                $args = [
                    $aWebMembers[$leader_fio]['id'],
                    $item['id'],
                ];
                $sth = $dbh->prepare($query);
                if (!$sth) {
                    throw new Exception('Ошибка в PDO');
                }
                $sth->execute($args);
            } else {
                echo "<span style='color:green'>Не смог сопоставить руководителя у " . $item['name'] . "</span><BR>";
            }

        }
    } else {
        echo "<span style='color: red'>Нет такой записи " . $item['name']."</span><br>";
    }
}

