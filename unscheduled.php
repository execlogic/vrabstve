<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 14.09.18
 * Time: 18:32
 */

/*
 * Расчет внеплановой выплаты по сотрудникам (персонально)
 *
 */

// Время работы скрипта
$start = microtime(true);
require_once "app/ErrorInterface.php";
require_once "app/MemberInterface.php";
require_once 'app/functions.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';
require_once 'app/Unscheduled.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

/*
 *  Роли и доступы
 */
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

$Error = new ErrorInterface();

if ($_REQUEST['id']) {
    $user_id = $_REQUEST['id'];

    /*
     *  Если не число, то ошибка выходим
     */
    if (!is_numeric($user_id)) {
        echo "Error!";
        header('Location: index.php');
    }


    /*
     *  Создаем объект MemberInterface
     */
    $MI = new MemberInterface();
    /*
     *  Получаю данные по пользователю
     */
    try {
        $member = $MI->fetchMemberByID($user_id);
    } catch (Exception $e) {
        echo $e->getMessage();
        exit();
    }

    /*
     *  Права доступа
     */
    $member_department_id = $member->getDepartment()['id'];

    /*
     * Выставляю текущую дату, если в запросе не будет ['date']
     */
    $date = date("m.Y");

    /*
     *  Выставляю дату
     */
    if (isset($_REQUEST['date'])) {
        /*
         * то выставляю дату с новыми зачениями и начинаю разбор блока
         */
        $date = $_REQUEST['date'];


        /*
         *  ******************************* Подключаемся к БД
         */

        $dbh = dbConnect();

        $Ud = new Unscheduled();
        $chkAvans = $Ud->checkForAvans($user_id, $date);

        // ******************************* Обработка событий
        if (isset($_POST['SendForm'])) {
            if ($_POST['unshedulled_avans_pay'])
                $unshedulled_avans = 1;
            else
                $unshedulled_avans = 0;

//            if ($chkAvans == 1 && $unshedulled_avans == 1){
//                $Error->add("Уже была добавлена выплата с авансом");
//            } else {
                try {
                    $Ud->addUnscheduled($user_id, $_POST['unscheduled'], $_POST['unscheduled_note'], $unshedulled_avans);
                } catch (Exception $e) {
                    echo $e->getMessage() . "<BR>";
                }
//            }

            unset($_POST);
        }

        if (isset($_POST['Сansel'])) {
            $DeleteID = $_POST['Сansel'];
            try {
                $Ud->delUnscheduled($DeleteID);
            }catch (Exception $e) {
                echo $e->getMessage()."<BR>";
            }
        }

        /*
         * Получаю данные из БД
         */

        $raw = $Ud->getDataForTheMonth($date, $user_id);




        /*
         *  Объявляю основные переменные которые будут необходимы при расчете
         */

        /*
         * Итого по административному блоку
         */
        $All_Administrative_Rubles = 0;


        /*
         *  Извлекаю данные и заношу в переменные, наценка будет расчитана ниже
         */

    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Внеплановая выплата</title>
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Расчет внеплановой выплаты
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Список пользователей</a></li>
                <li><a href="profile.php?id=<?php echo $user_id; ?>">Профиль сотрудника</a></li>
                <li class="active">Расчет премии</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php
            /*
                 * Вывод информации об ощибках
                 */
            if ($Error->getCount() > 0) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="TableErr">
                            <thead>
                            <th>Сообщение</th>
                            </thead>
                            <tbody>
                            <?php
                            foreach (array_unique($Error->getErrors()) as $value) {
                                echo "<tr><td>" . $value . "</td><td></td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php
            }
            ?>

            <div class="row">
                <div class="col-md-12">

                    <div class="box box-solid">
                        <div class="box-header">
                            <h3>
                                <?php
                                // Вывожу ФИО
                                if ($member->getMaidenName()) {
                                    echo $member->getLastName() . " ( " . $member->getMaidenName() . " ) " . $member->getName() . " " . $member->getMiddle();
                                } else {
                                    echo $member->getLastName() . " " . $member->getName() . " " . $member->getMiddle();
                                };
                                ?>
                            </h3>
                        </div>
                        <div class="box-body">
                            <form class="form-horizontal" method="post">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Выберите дату: </label>
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="datepicker" name="date" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                                   value="<?php echo $date; ?>" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-2"><input type="submit" class="btn btn-flat btn-default" name="SendDate" value="Получить данные"></div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <?php
                    if (isset($_REQUEST['date'])) {
                        ?>
                        <form method="post" class="form-horizontal">
                            <input type="hidden" name="date" value="<?php echo $date; ?>">
                            <div class="box box-info box-solid">
                                <div class="box-body">

                                    <table class="table table-responsive table-hover">
                                        <thead>
                                        <tr>
                                            <th>Дата</th>
                                            <th>Сумма</th>
                                            <th>Комментарий</th>
                                            <th></th>
                                            <th>Статус</th>
                                            <th>Действия</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if (isset($raw) && count($raw) > 0) {

                                            foreach ($raw as $item) {
                                                if ($item['active'] == 1) {
                                                    if (!isset($item['status_name'])) {
                                                        $item['status_name'] = "Утверждение директоратом";
                                                    }
                                                    echo "<tr>";
                                                        echo "<td>" . $item['date'] . "</td>";
                                                        echo "<td title='" . $item['note'] . "'>" . number_format($item['summ'], 2, ',', ' ') . "</td>";
                                                        echo "<td>".$item['note']."</td>";
                                                        if ($item['avans']==1)
                                                            echo "<td>Не выплачивать аванс</td>";
                                                        else
                                                            echo "<td></td>";
                                                        echo "<td>" . $item['status_name'] . "</td>";
                                                        echo "<td>";
                                                        if ($item['status_id']!=6) {
                                                            echo "<button class='btn btn-sm btn-flat btn-danger' name='Сansel' value='" . $item['id'] . "'>Отменить заявку на выплату</button>";
                                                        }
                                                        echo "</td>";
                                                    echo "</tr>";
                                                } else {
                                                    echo "<tr>";
                                                        echo "<td>" . $item['date'] . "</td>";
                                                        echo "<td title='" . $item['note'] . "'>" . number_format($item['summ'], 2, ',', ' ') . "</td>";
                                                        echo "<td>".$item['note']."</td>";
                                                        echo "<td></td>";
                                                        echo "<td>Заявка отменена</td>";
                                                        echo "<td></td>";
                                                    echo "</tr>";
                                                }
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="box-footer">
                                    <a class="btn btn-sm btn-flat btn-primary" data-toggle="modal" data-target="#modal-default">Добавить выплату</a>
                                </div>
                            </div>

                        </form>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <div class="modal fade" id="modal-default">
                <div style="width: 90%;" class="modal-dialog">
                    <div style="width: 100%;" class="modal-content">
                        <form class="form-horizontal" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="date" value="<?php echo date("m.Y"); ?>">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4>Внеплановая выплата</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Сумма выплаты</label>
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-ruble"></i>
                                            </div>
                                            <input type="text" class="form-control" name="unscheduled"
                                                   id="unscheduled_<?php echo $member->getId(); ?>"
                                                   placeholder="0" title=""
                                                   value="0"
                                                   autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <div class='input-group-addon btn btn-flat' name='unscheduled' id='btn_comment_<?php echo $member->getId(); ?>'>
                                                <i class='fa fa-comment'></i></div>
                                            <textarea name='unscheduled_note'  cols='1' rows='1' class='form-control'></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <div class="checkbox pull-right">
                                            <label>
                                                <input name="unshedulled_avans_pay"  type="checkbox">
                                            </label>
                                        </div>
                                    </div>
                                    <label class="col-sm-4 control-label pull-left" style="text-align: left">Не выплачивать аванс сотруднику</label>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-flat btn-warning pull-right" name="SendForm" value="Сохранить">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
require_once 'footer.php';
?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    // Выбор даты
    $(function () {
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            format: 'mm.yyyy',
            //startDate: <?php //echo '\''.$member->getEmploymentDate().'\''; ?>//,
        })
    });
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>


