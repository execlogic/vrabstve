<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 22.08.18
 * Time: 18:10
 */

// Время работы скрипта
$start = microtime(true);

require_once 'app/ErrorInterface.php';
require_once 'app/MemberInterface.php';
require_once 'app/DepartmentInterface.php';
require_once 'app/Department.php';

require_once 'app/Notify.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

$Errors = new ErrorInterface();

$MI = new MemberInterface();
$MI->fetchMembers(false);
$members = $MI->GetMembers();
$ResultArray = array();

$DI = new DepartmentInterface();
$DI->fetchDepartments();
$DepartmentArray = $DI->GetDepartments();

foreach ($DepartmentArray as $dep) {
    $dep_id = $dep->getId();

    $filtred = array_filter($members, function ($m) use ($dep_id) {
        return $m->getDepartment()['id'] == $dep_id;
    });

    if (count($filtred)>0) {
        foreach ($filtred as $value) {
//            $ResultArray[$dep_id][] = array("name" => $value->getLastName()." ".$value->getName()." ".$value->getMiddle(), "id" => $value->getId());
            $ResultArray[$dep_id][] = $value;
        }
    } else {
        $ResultArray[$dep_id] = null;
    }
}
$menu_open = 3;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Пользователи по отделам</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">

<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">


        <!-- Main content -->
        <section class="content">

            <?php

            /*
                 * Вывод информации об ощибках
                 */
            if ($Errors->getCount() > 0) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="Table3">
                            <thead>
                            <th>Причина</th>
                            <th>Сообщение</th>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($Errors->getErrors() as $value) {
                                echo "<tr><td>" . $value['0'] . "</td><td>" . $value[1] . "</td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php
            }
            ?>

            <div class="box box-solid">
                <div class="box-header"><h4>Пользователи по отделам</h4></div>
                <div class="box-body">
                    <table class="table table-bordered" id="MembersTable">
                        <thead>
                        <th>ID</th>
                        <th>Название отдела</th>
                        <th>Название отдела по 1С</th>
                        <th>ФИО</th>
                        <th>Количество пользователей</th>

                        </thead>
                        <tbody>
                        <?php
                        /*
                         * Итого пользователей
                         */
                        $members_summ = 0;

                        foreach ($ResultArray as $key=>$item) {
                            $current = current(array_filter($DepartmentArray,function ($dep) use ($key) {
                                return $dep->getId() == $key;
                            }));

                            $members_summ += count($item);

                            echo "<tr>";
                            echo "<td>".$key."</td><td ><a href=\"javascript: void(0);\" id=\"expand\" name='".$current->getId()."'>".$current->getName()."</a></td><td>".$current->getNamesklad()."</td><td></td><td><b>".(count($item))."</b></td>";
                            echo "</tr>";

                            if (count($item)>0) {
                                foreach ($item as $user) {
                                    echo "<tr id='" . $current->getId() . "' style=\"display: none\">";
                                    if ($user->getPosition()['id'] == 29 || $user->getPosition()['id'] == 27 || $user->getPosition()['id'] == 8 || $user->getPosition()['id'] == 10 || $user->getPosition()['id'] == 28) {
                                        echo "<td></td><td></td><td></td><td><a href='profile.php?id=" . $user->getId() . "' style='color:rgba(0,0,0,0.7)'><b>" . ($user->getLastName() . " " . $user->getName() . " " . $user->getMiddle()) . "</b></a></td><td></td>";
                                    } else {
                                        echo "<td></td><td></td><td></td><td><a href='profile.php?id=" . $user->getId() . "' style='color:rgba(0,0,0,0.7)'>" . ($user->getLastName() . " " . $user->getName() . " " . $user->getMiddle()) . "</a></td><td></td>";
                                    }
                                    echo "</tr>";
                                }
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th> Всего сотрудников </th>
                            <th></th><th></th>
                            <th><?php echo $members_summ; ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>

<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
    $(document).on("click", "a[id^=expand]", function () {
        var number = $(this).attr("name");
        var boolexp = 0;

        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $("tr[id=" + number + "]").each(function () {
                $(this).hide();
            });
        } else {
            $(this).addClass('active');
            $("tr[id=" + number + "]").each(function () {
                $(this).show();
            });
        }
    })
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
