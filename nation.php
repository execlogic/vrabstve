<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 31.07.18
 * Time: 12:32
 */
require_once 'app/NationInterface.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

// Настройки
if (!array_filter($Roles, function ($Role) {
    return ($Role->getSettings() == 1 || $Role->getAWHEdit() == 1);
})) {
    header("Location: 404.php" );
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

$Nations = new NationInterface();

if (isset($_POST["submit_add"])) {
    if (is_string($_POST['InputNation']) && strlen($_POST['InputNation'])) {
        try {
            $Nations->AddNations($_POST['InputNation']);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

$Nations->fetchNations();
$menu_open = 2;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Национальность</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">

</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>

    <div class="content-wrapper">
        <section class="content">
            <?php
            /*
             * Вывод информации об ощибках
             */
            if (!empty($Errors)) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        echo "<p>" . $Errors . "</p>";
                        ?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php
            }
            ?>

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Национальности</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="MembersTable" class="table table-bordered table-hover table-striped">
                        <thead>
                        <th>#</th>
                        <th>Название</th>
                        </thead>
                        <tbody>
                        <?php
                        try {
                            foreach ($Nations->GetNations() as $value) {
                                echo "<tr>";
                                echo "<td>" . $value->getID() . "</td><td>" . $value->getName() . "</td>";
                                echo "</tr>";
                            }
                        } catch (Exception $e) {
                            echo $e->getMessage();
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a class="btn btn-app" data-toggle="modal" data-target="#modal-default" title="Добавить национальность">
                        <i class="fa fa-plus"></i> Добавить
                    </a>
                </div>

                <div class="modal fade" id="modal-default">
                    <form class="form-horizontal" method="post">
                        <div style="width: 90%;" class="modal-dialog">
                            <div style="width: 100%;" class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title">Добавить национальность</h4>
                                </div>
                                <div class="modal-body">
                                    <form method="post" class="form-horizontal">
                                        <div class="box box-info">
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Название национальности</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" name="InputNation" id="inputNation" placeholder="национальность">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer" style="border:0;">
                                    <a class="btn btn-app pull-left" data-dismiss="modal" title="Закрыть">
                                        <i class="fa fa-times"></i> Закрыть
                                    </a>
                                    <button type="submit" class="btn btn-app btn-primary" name="submit_add"><i class="fa fa-save"></i> Добавить национальность</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </section>
    </div>

    <?php require_once 'footer.php';?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
