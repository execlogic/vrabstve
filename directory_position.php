<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 31.07.18
 * Time: 13:25
 */

require_once 'app/PositionInterface.php';
require_once 'app/FileCSV.php';
require_once 'app/DepartmentInterface.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';


session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

$Positions = new PositionInterface();
$Departments = new DepartmentInterface();
$Departments->fetchDepartments();
$DL = $Departments->GetDepartments();

$Positions->fetchPositions();
$menu_open = 1;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Справочник должностей</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Select2 -->
    <link rel="stylesheet" href="../../bower_components/select2/dist/css/select2.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="css/edit.css">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <div class="content-wrapper">
        <section class="content">
            <?php
            /*
             * Вывод информации об ощибках
             */
            if (!empty($Errors)) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        echo "<p>" . $Errors . "</p>";
                        ?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php
            }
            ?>

            <!-- Default box -->

            <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">Должности в компании</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form method="post">
                        <table id="MembersTable" class="table table-bordered table-hover table-striped">
                            <thead>
                            <th class="col-md-1">#</th>
                            <th class="col-md-2">Название</th>
                            <th class="col-md-8">Отделы</th>
                            </thead>
                            <tbody>
                            <?php
                            try {
                                foreach ($Positions->GetPositions() as $value) {
                                    echo "<tr>";
                                    echo "<td>" . $value->getID() . "</td><td>" . $value->getName() . "</td>";
                                    echo "<td>";
                                    $c = 0;
                                    foreach ($value->getDepartmentId() as $d) {
                                        $c++;
                                        $DObj = current(array_filter($DL, function ($dd) use ($d) {
                                            return $dd->getId() == $d;
                                        }));

                                        if (!$DObj)
                                            continue;

                                        if (count($value->getDepartmentId()) != $c) {
                                            echo $DObj->getName() . ", ";
                                        } else {
                                            echo $DObj->getName() . " ";
                                        }

                                    }
                                    echo "</td>";
                                    echo "</tr>";
                                }
                            } catch (Exception $e) {
                                echo $e->getMessage();
                            }
                            ?>
                            </tbody>
                        </table>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>

        </section>
    </div>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script src="../../bower_components/select2/dist/js/select2.full.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
