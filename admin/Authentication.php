<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 22.10.18
 * Time: 16:32
 */

require_once "app/functions.php";
require_once "admin/User.php";
require_once "admin/RoleInterface.php";

session_start(); //Запускаем сессии

class Authentication
{
    private $dbh;

    private $ldaphost = "192.168.82.19";
    private $ldapport = 389;
    private $ldapUserSuffix = "ou=Users";
    private $ldapSuffix = "dc=thu,dc=local";

    /**
     * Проверяет, авторизован пользователь или нет
     * Возвращает true если авторизован, иначе false
     * @return boolean
     */
    public function isAuth()
    {
        if (isset($_SESSION['UserObj'])) { //Пока не ломаю всё
            return $_SESSION['UserObj']->getAuth();
        } else return false;
    }

    public function getMemberId()
    {
        if (isset($_SESSION['UserObj']) && ($_SESSION['UserObj']->getAuth() == true)) {
            return $_SESSION['UserObj']->getMemberId();
        }
    }

    /**
     * Авторизация пользователя
     * @param string $login
     * @param string $password
     * @return
     **/
    public function auth($login, $password)
    {
        $UserObj = new User();
        $RI = new RoleInterface(); //Roles
        $_SESSION['UserObj'] = &$UserObj; // Назначаю в сессию объект пользователя
        $UserObj->SetAuth(false); // По умолчанию выставляю в false
        $LDAP = 1;


        if (!$LDAP) {
            $hash = hash("sha256", $password);
            echo $hash . "<BR>";
            if ($this->dbh == null) {
                $this->dbh = dbConnect();
            }

            $query = "select member_id from member_auth where login = ? and hash = ? and active = 1 LIMIT 1";
            $sth = $this->dbh->prepare($query);
            $sth->execute([$login, $hash]);
            if ($sth->rowCount() == 0) {
                $sth = null;
                return false;
            } else {
                $id = $sth->fetch(PDO::FETCH_ASSOC)['member_id'];
                echo $id . "<BR>";
                $sth = null;
                $query = "SELECT t1.login,t1.member_id,t2.name,t2.lastname,t2.employment_date,t4.name as position,t5.name as department FROM zp.member_work as t1
                    LEFT JOIN member as t2 ON (t1.member_id=t2.id)
                    LEFT JOIN member_jobpost as t3 on (t1.member_id=t3.member_id)
                    LEFT JOIN position as t4 on (t3.position_id = t4.id)
                    LEFT JOIN department as t5 on (t3.department_id = t5.id)
                    where t2.id=? and t1.active=1 and t2.dismissal_date='9999-01-01' and t3.active=1 LIMIT 1";

                $sth = $this->dbh->prepare($query);
                $sth->execute([$id]);
                $raw = $sth->fetch(PDO::FETCH_ASSOC);

                if (is_null($raw)) {
                    return false;
                } else if ($login == "administrator") {
                    $UserObj->setMemberId(-1);
                    $UserObj->setLogin("administrator");
                    $UserObj->setFN("administrator");
                    $UserObj->setAuth(true);
                    $UserObj->setPosition(1);
                    $UserObj->setDepartment(1); // нужно ли это ????
                    $UserObj->setEmployment("0000-01-01");
                    $UserObj->setRole($RI->getRoles("-1"));
                } else {
                    $UserObj->setMemberId($raw['member_id']);
                    $UserObj->setLogin($raw['login']);
                    $UserObj->setFN($raw['lastname'] . " " . $raw['name']);
                    $UserObj->setAuth(true);
                    $UserObj->setPosition($raw['position']);
                    $UserObj->setDepartment($raw['department']); // нужно ли это ????
                    $UserObj->setEmployment(explode(" ", $raw['employment_date'])[0]);
                    $UserObj->setRole($RI->getRoles($raw['member_id']));
                }

                $raw = null;
                $sth = null;
            }
            return true;

        } else {
            // E8d48gDSWvau
            if ($login != "administrator") {
                /**** LDAP Аутентификация ****/
                try {
                    $ldap = ldap_connect($this->ldaphost, $this->ldapport) or die("Can't connect to ldap server");
                } catch (Exception $e) {
                    echo $e->getMessage() . "<BR>";
                }
                $ldaprdn = 'uid=' . $login . "," . $this->ldapUserSuffix . ",$this->ldapSuffix";

                ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
                ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

                $bind = @ldap_bind($ldap, $ldaprdn, $password);


                if ($bind) {
                    if ($this->dbh == null) {
                        $this->dbh = dbConnect();
                    }

                    $query = "SELECT t1.login,t1.member_id,t2.name,t2.lastname,t2.employment_date,t4.name as position,t5.name as department FROM zp.member_work as t1
                    LEFT JOIN member as t2 ON (t1.member_id=t2.id)
                    LEFT JOIN member_jobpost as t3 on (t1.member_id=t3.member_id)
                    LEFT JOIN position as t4 on (t3.position_id = t4.id)
                    LEFT JOIN department as t5 on (t3.department_id = t5.id)
                    where t1.login=? and t1.active=1 and t2.dismissal_date='9999-01-01' and t3.active=1 LIMIT 1";

                    $sth = $this->dbh->prepare($query);
                    $sth->execute([$login]);
                    $raw = $sth->fetch(PDO::FETCH_ASSOC);

                    if (is_null($raw)) {
                        return false;
                    } else {
                        $UserObj->setMemberId($raw['member_id']);
                        $UserObj->setLogin($raw['login']);
                        $UserObj->setFN($raw['lastname'] . " " . $raw['name']);
                        $UserObj->setAuth(true);
                        $UserObj->setPosition($raw['position']);
                        $UserObj->setDepartment($raw['department']); // нужно ли это ????
                        $UserObj->setEmployment(explode(" ", $raw['employment_date'])[0]);
                        $UserObj->setRole($RI->getRoles($raw['member_id']));
                    }

                    $raw = null;
                    $sth = null;

                    return true;
                } else {
                    return false;
                }
            } else {
                $hash = hash("sha256", $password);
                if ($hash == "6f39535cea6abd731949e54945781676af13269a7dca90b23c1c77624ddfc8a6") {
                    $UserObj->setMemberId(-1);
                    $UserObj->setLogin("administrator");
                    $UserObj->setFN("administrator");
                    $UserObj->setAuth(true);
                    $UserObj->setPosition(1);
                    $UserObj->setDepartment(1); // нужно ли это ????
                    $UserObj->setEmployment("0000-01-01");
                    $UserObj->setRole($RI->getRoles("-1"));
                    return true;
                } else {
                    return false;
                }
            }
        }

        return false;   //Логин и пароль не подошел
    }

    /**
     * Метод возвращает логин авторизованного пользователя
     */
    public function getLogin()
    {
        if ($this->isAuth()) { //Если пользователь авторизован
            return $_SESSION['UserObj']->getLogin(); //Возвращаем логин, который записан в сессию
        }
    }

    public function out()
    {
        $_SESSION = array(); //Очищаем сессию
        session_destroy(); //Уничтожаем
    }
}

?>