<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 24.10.18
 * Time: 14:15
 */

require_once "app/functions.php";
require_once "admin/Role.php";
require_once 'app/ReportStatus.php';

class RoleInterface
{
    private $dbh = NULL;
    private $RoleArray = array();

    public function fetchRole($member_id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $this->RoleArray = array();

        $query = "select t1.id as RoleId,t1.member_id as memberid,t1.department_id as department,t2.*,t3.name as DepartmentName from Roles as t1
        LEFT JOIN RoleSettings as t2 ON (t1.RoleSettings_id = t2.id)
        LEFT JOIN department as t3 ON (t1.department_id = t3.id)
        where t1.member_id = ?";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$member_id]);
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        foreach ($raw as $item) {
            $RoleSettings = new Role();
            $RoleSettings->setRoleName($item['name']);
            $RoleSettings->setRoleId($item['RoleId']);
            $RoleSettings->setId($item['id']);
            $RoleSettings->setFromArray($item['settings']);
            $RoleSettings->setDepartment($item['department']);
            $RoleSettings->setDepartmentName($item['DepartmentName']);

            if ($RoleSettings->getId() == 5) {
                    $RoleSettings->setPrepaid(1);
            }

            if ($RoleSettings->getId() == 3) {
                if ($this->checkPrepaid()) {
                    $RoleSettings->setPrepaid(1);
                }

                if ($this->checkPayment()) {
                    $RoleSettings->setPayment(1);
                }
            }

            array_push($this->RoleArray, $RoleSettings);
        }
    }

    public function fetchRootRole(){
        $this->RoleArray = array();
        $RoleSettings = new Role();

        $RoleSettings->setMemberId(-1);
        $RoleSettings->setDepartment(0);
        $RoleSettings->setId(6);

        $tmp_array = array();
        foreach((array)$RoleSettings as $key=>$value) {
            $k = trim(substr($key, 5));

            if ( $k == 'Id' || $k == "RoleId" || $k == "RoleName" || $k == "member_id" || $k == "Department" || $k == "DepartmentName" ) {
                continue;
            }
            $tmp_array[$k] = 1;

        }
        $RawRole = serialize($tmp_array);

        $RoleSettings->setFromArray($RawRole);
        $RoleSettings->setPrepaid(1);
        $RoleSettings->setPayment(1);
        array_push($this->RoleArray, $RoleSettings);
    }

    public function getRolesByID($roleid)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select t1.*,t2.* 
	    from Roles as t1
	    LEFT JOIN member as t2 ON (t1.member_id = t2.id)
        LEFT JOIN member_workstatus as t3 ON (t2.id=t3.member_id)
	    where t1.RoleSettings_id=? AND (t3.status = 1  OR t3.status IS NULL)";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$roleid]);
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

        /*
         * Получаю данные из БД
         */
        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
        /*
         * Очистка памяти
         */
        $sth = null;

        return $raw;
    }

    public function addDirectorList($id, $list)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from director_list where member_id=?";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id]);
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

        if ($sth->rowCount() > 0) {
            $query = "UPDATE director_list SET departmentlist=? where member_id=?";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$list, $id]);
            } catch (PDOException $e) {
                throw new Exception($e->getMessage());
            }
        } else {
            $query = "INSERT INTO director_list(member_id,departmentlist) VALUES (?,?)";
            try {
                $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                $sth->execute([$id, $list]);
            } catch (PDOException $e) {
                throw new Exception($e->getMessage());
            }
        }

    }

    public function getRoles($member_id)
    {
        if ($member_id == -1 ) {
            $this->fetchRootRole();
        } else {
            $this->fetchRole($member_id);
        }
        return $this->RoleArray;
    }

    public function get()
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from RoleSettings";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute();
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

        return $raw;
    }

    public function add(&$RoleSettings)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $tmp_array = array();
        foreach((array)$RoleSettings as $key=>$value) {
            $k = trim(substr($key, 5));

            if ( $k == 'Id' || $k == "RoleId" || $k == "RoleName" || $k == "member_id" || $k == "Department" || $k == "DepartmentName" ) {
                continue;
            }
            $tmp_array[$k] = $value;

        }
        $settings = serialize($tmp_array);

        $query = "insert into RoleSettings (name,settings) VALUES (?,?)";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([
                $RoleSettings->getRoleName(),
                $settings
            ]);
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

    }


    public function update(&$RoleSettings)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $tmp_array = array();
        foreach((array)$RoleSettings as $key=>$value) {
            $k = trim(substr($key, 5));

            if ( $k == 'Id' || $k == "RoleId" || $k == "RoleName" || $k == "member_id" || $k == "Department" || $k == "DepartmentName" ) {
                continue;
            }
            $tmp_array[$k] = $value;

        }
        $settings = serialize($tmp_array);

        $query = "update RoleSettings SET settings=?,name=? where id=?";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$settings,
                $RoleSettings->getRoleName(),
                $RoleSettings->getId(),
            ]);
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function addToMember($RoleId, $DepartmentId, $MemberId)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "insert into Roles (member_id,RoleSettings_id,department_id) VALUES (?,?,?)";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$MemberId, $RoleId, $DepartmentId]);
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function deleteMemberRole($id)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }
        $query = "delete from Roles where id=?";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id]);
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function checkPrepaid()
    {
        $RS = new ReportStatus(); // Статус ведомости.
        if ($RS->getCatcherStatus() != NULL) {
            return true;
        } else {
            return false;
        }
    }

    public function checkPayment() {
        $RS = new ReportStatus(); // Статус ведомости.
        if ($RS->getCatcherPaymentStatus() != NULL) {
            return true;
        } else {
            return false;
        }
    }

    public function getDirectorList()
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "select * from director_list";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute();
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

        /*
         * Получаю данные из БД
         */
        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
        /*
         * Очистка памяти
         */
        $sth = null;

        return $raw;
    }

    public function getSubordination()
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "
        select 
	        t1.id, t1.name, t2.RoleSettings_id as role_id, t2.member_id, CONCAT(t4.lastname,\" \",t4.name,\" \", t4.middle) as member_name, t3.member_id as director_id, CONCAT(t5.lastname,\" \", t5.name,\" \", t5.middle) as director_name, t6.direction_id
        FROM department as t1
            LEFT JOIN Roles as t2 ON (t1.id = t2.department_id AND t2.RoleSettings_id = 2)
            LEFT JOIN Roles as t3 ON (t1.id = t3.department_id AND t3.RoleSettings_id=4)
            LEFT JOIN member as t4 ON (t2.member_id = t4.id)
            LEFT JOIN member as t5 ON (t3.member_id = t5.id)
            LEFT JOIN member_jobpost as t6 ON (t3.member_id = t6.member_id)
        WHERE
	        t1.active=1
	    GROUP BY id
        ";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute();
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

        /*
         * Получаю данные из БД
         */
        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
        /*
         * Очистка памяти
         */
        $sth = null;

        foreach ($raw as $k=>$v) {
            if (is_null($v['member_id'])) {
                $raw[$k]['member_id'] = $v['director_id'];
                $raw[$k]['member_name'] = $v['director_name'];
            }
        }

        return $raw;
    }

    /*
     * Получаю ID пользователей для рассылки сообщений о статусе ведомости
     */
    public function getMemberListStatus($status)
    {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        switch ($status) {
            case 1:
                $query = "SELECT DISTINCT(member_id) FROM zp.Roles where RoleSettings_id=2 OR RoleSettings_id=4 OR RoleSettings_id=5 OR RoleSettings_id=6;";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute();
                } catch (PDOException $e) {
                    throw new Exception($e->getMessage());
                }
                $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
                break;
            case 2:
                $query = "SELECT DISTINCT(member_id) FROM zp.Roles where RoleSettings_id=4 OR RoleSettings_id=5 OR RoleSettings_id=6;";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute();
                } catch (PDOException $e) {
                    throw new Exception($e->getMessage());
                }
                $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
                break;
            case 4:
                $query = "SELECT DISTINCT(member_id) FROM zp.Roles where RoleSettings_id=3 OR RoleSettings_id=5 OR RoleSettings_id=6;";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute();
                } catch (PDOException $e) {
                    throw new Exception($e->getMessage());
                }
                $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
                break;
            case 3:
            case 5:
            case 6:
                $query = "SELECT DISTINCT(member_id) FROM zp.Roles where RoleSettings_id=5 OR RoleSettings_id=6;";
                try {
                    $sth = $this->dbh->prepare($query); // Подготавливаем запрос
                    $sth->execute();
                } catch (PDOException $e) {
                    throw new Exception($e->getMessage());
                }
                $raw = $sth->fetchAll(PDO::FETCH_ASSOC);

                break;
        }
        return $raw;
    }


    /*
     * Список руководителей начислителей ЗП
     */
    public function getRukList() {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT t2.lastname,t2.name,t2.middle,t2.maiden_name,t3.name as department_name FROM zp.Roles as t1 
        LEFT JOIN member as t2 ON (t1.member_id = t2.id)
        LEFT JOIN department as t3 ON (t1.department_id = t3.id)
        where RoleSettings_id = 2";

        $sth = $this->dbh->prepare($query); // Подготавливаем запрос
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getOtvDirectors() {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT DISTINCT(member_id),t2.lastname, t2.name, t2.middle
        FROM zp.Roles as t1
        LEFT JOIN member as t2 ON (t1.member_id = t2.id)
        WHERE RoleSettings_id=4;";

        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute();
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

        /*
         * Получаю данные из БД
         */
        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
        /*
         * Очистка памяти
         */
        $sth = null;

        return $raw;
    }

    public  function getDepartmnetByMemberId($id) {
        if ($this->dbh == null) {
            $this->dbh = dbConnect();
        }

        $query = "SELECT t1.department_id, t2.direction_id FROM zp.Roles as t1
        LEFT JOIN department as t2 ON (t1.department_id = t2.id)
        WHERE t1.RoleSettings_id=4 and t1.member_id=?";
        try {
            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
            $sth->execute([$id]);
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

        /*
         * Получаю данные из БД
         */
        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
        /*
         * Очистка памяти
         */
        $sth = null;

        return $raw;
    }

//    public  function getDirectionByMemberId($id) {
//        if ($this->dbh == null) {
//            $this->dbh = dbConnect();
//        }
//
//        $query = "SELECT direction_id FROM zp.Roles where RoleSettings_id=4 and member_id=?";
//        try {
//            $sth = $this->dbh->prepare($query); // Подготавливаем запрос
//            $sth->execute([$id]);
//        } catch (PDOException $e) {
//            throw new Exception($e->getMessage());
//        }
//
//        /*
//         * Получаю данные из БД
//         */
//        $raw = $sth->fetchAll(PDO::FETCH_ASSOC);
//        /*
//         * Очистка памяти
//         */
//        $sth = null;
//
//        return $raw;
//    }
}