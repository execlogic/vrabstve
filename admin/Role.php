<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 08.11.18
 * Time: 13:14
 */

class Role
{
    private $RoleId; // ID записи роли

    private $Id = 0; // ID Роли
    private $RoleName="";
    private $member_id = 0; // ID пользователя

    private $Department = 0; // Отдел
    private $DepartmentName = "";

    private $AddUser = 0; // Добавление пользователя

    private $ProfilePayments = 0; //Выплаты в профиле
    private $ProfileAWH = 0; // УРВ в профиле
    private $ProfileWorkInfo = 0; // Рабочая информация
    private $ProfilePersonalInfo = 0; // Личная информация

    private $ChangeMember  = 0; // Изменение пользователя
    private $ChangeWorkPersonal = 0; // Изменение личное, рабочее
    private $ChangeFinanceMotivation = 0; // Изменение финансов и мотивации

    private $ChangeFinanceProbation = 1; // Изменение условий на испытательный срок.

    private $ChangeWorkStatus = 0; // Изменение рабочего статуса

    private $ShowHistory = 0; // Просмотр истории по пользователю
    private $AvansCalculation = 0; // Кнопка расчета аванса
    private $Avans2Calculation = 0; // Кнопка расчета аванса2
    private $PremiyaCalculation = 0; // Кнопка расчета премии
    private $RaschetCalculation = 0; // Кнопка расчета "расчета"

    private $CommercialBlockBonus = 0; // Коммерческий блок Бонусы
    private $CommercialBlockNachenka = 0; // Коммерческий блок наценка
    private $CommercialBlockProcent = 0; // Коммерческий блок корректирующий процент

    private $Promotions = 0; // Административный блок акции
    private $AdministrativePrize = 0; // Административный блок адм. премия
    private $Training = 0; // Административный блок Обучение
    private $PrizeAnotherDepartment = 0; // Административный блок Премия другого отдела
    private $Defect = 0; // Административный блок Неликвиды и брак
    private $Sunday = 0; // Административный блок Воскресения
    private $NotLessThan = 0; // Административный блок Выплаты не менее
    private $MobileCommunication = 0; // Административный блок Мобильная связь
    private $Overtime = 0; // Административный блок Переработки
    private $CompensationFAL = 0; // Административный блок Компенсация ГСМ

    private $LateWork = 0; // Удержания За опоздания
    private $Schedule = 0; // Удержания За график работы
    private $Internet = 0; // Удержания За интернет
    private $CachAccounting = 0; // Удержания Кассовый учет
    private $Receivables = 0; // Удержания Дебиторская задолженность
    private $Credit = 0; // Удержания Кредит
    private $Other = 0; // Удержания Другое

    private $BonusAdjustment = 0; // Корректировки Корректировка бонуса
    private $Calculation = 0; // Корректировки Расчет
    private $YearBonus = 0; // Корректировки годовой бонус

    private $ImportAWH = 0; // Импорт данных УРВ
    private $ImportNDFL = 0; // Импорт данных НДФЛ
    private $Import1C = 0; // Импорт данных 1С7.7

    private $Settings = 0; // Внесение настроек

    private $ReportLeaving = 0; // Отчет уволившихся
    private $ReportTransfer = 0; // Отчет по перешедшим
    private $ReportAWH = 0; // Отчет УРВ всех
    private $ReportMemberAWH = 0; // Отчет УРВ по пользователю
    private $ReportRetetition = 0; // Отчет по удержаниям
    private $ReportCorrecting = 0; // Отчет по корректировкам

    private $FinancialStatement = 0; // Финансовая ведомость (расчет)
    private $ReportJournal = 0;

    private $ShowAdvanceCalculation = 0;
    private $ShowCommercialBlock = 0;
    private $ShowAdministrativeBlock = 0;
    private $ShowRetitiotionBlock = 0;
    private $ShowCorrectingBlock = 0;
    private $ShowAWHBlock = 0; //Показывать и позволять исправлять блок УРВ
    private $AWHEdit = 0; // Изменение в УРВ

    private $Prepaid = 0; // Плановые выплаты
    private $Payment = 0; // Внеплановые выплаты


    /*
     *                     Setters
     */

    /**
     * @param int $AWHEdit
     */
    public function setAWHEdit($AWHEdit)
    {
        $this->AWHEdit = $AWHEdit;
    }

    /**
     * @param int $Payment
     */
    public function setPayment($Payment)
    {
        $this->Payment = $Payment;
    }

    /**
     * @param int $ChangeFinanceProbation
     */
    public function setChangeFinanceProbation($ChangeFinanceProbation)
    {
        $this->ChangeFinanceProbation = $ChangeFinanceProbation;
    }

    /**
     * @param int $Prepaid
     */
    public function setPrepaid($Prepaid)
    {
        $this->Prepaid = $Prepaid;
    }

    /**
     * @return int
     */
    public function getPrepaid()
    {
        return $this->Prepaid;
    }

    /**
     * @param mixed $RoleId
     */
    public function setRoleId($RoleId)
    {
        $this->RoleId = $RoleId;
    }


    /**
     * @param string $DepartmentName
     */
    public function setDepartmentName($DepartmentName)
    {
        $this->DepartmentName = $DepartmentName;
    }

    /**
     * @param int $RaschetCalculation
     */
    public function setRaschetCalculation($RaschetCalculation)
    {
        $this->RaschetCalculation = $RaschetCalculation;
    }


    /**
     * @param string $RoleName
     */
    public function setRoleName($RoleName)
    {
        $this->RoleName = $RoleName;
    }

    /**
     * @param int $ChangeFinanceMotivation
     */
    public function setChangeFinanceMotivation($ChangeFinanceMotivation)
    {
        $this->ChangeFinanceMotivation = $ChangeFinanceMotivation;
    }

    /**
     * @param int $ChangeMember
     */
    public function setChangeMember($ChangeMember)
    {
        $this->ChangeMember = $ChangeMember;
    }

    /**
     * @param int $ChangeWorkPersonal
     */
    public function setChangeWorkPersonal($ChangeWorkPersonal)
    {
        $this->ChangeWorkPersonal = $ChangeWorkPersonal;
    }

    /**
     * @param int $ChangeWorkStatus
     */
    public function setChangeWorkStatus($ChangeWorkStatus)
    {
        $this->ChangeWorkStatus = $ChangeWorkStatus;
    }

    /**
     * @param int $Credit
     */
    public function setCredit($Credit)
    {
        $this->Credit = $Credit;
    }

    /**
     * @param int $AdministrativePrize
     */
    public function setAdministrativePrize($AdministrativePrize)
    {
        $this->AdministrativePrize = $AdministrativePrize;
    }

    /**
     * @param int $MobileCommunication
     */
    public function setMobileCommunication($MobileCommunication)
    {
        $this->MobileCommunication = $MobileCommunication;
    }

    /**
     * @param int $BonusAdjustment
     */
    public function setBonusAdjustment($BonusAdjustment)
    {
        $this->BonusAdjustment = $BonusAdjustment;
    }

    /**
     * @param int $Other
     */
    public function setOther($Other)
    {
        $this->Other = $Other;
    }

    /**
     * @param int $Receivables
     */
    public function setReceivables($Receivables)
    {
        $this->Receivables = $Receivables;
    }

    /**
     * @param int $CachAccounting
     */
    public function setCachAccounting($CachAccounting)
    {
        $this->CachAccounting = $CachAccounting;
    }

    /**
     * @param int $Internet
     */
    public function setInternet($Internet)
    {
        $this->Internet = $Internet;
    }

    /**
     * @param int $Schedule
     */
    public function setSchedule($Schedule)
    {
        $this->Schedule = $Schedule;
    }

    /**
     * @param int $LateWork
     */
    public function setLateWork($LateWork)
    {
        $this->LateWork = $LateWork;
    }

    /**
     * @param int $Training
     */
    public function setTraining($Training)
    {
        $this->Training = $Training;
    }

    /**
     * @param int $Sunday
     */
    public function setSunday($Sunday)
    {
        $this->Sunday = $Sunday;
    }

    /**
     * @param int $Promotions
     */
    public function setPromotions($Promotions)
    {
        $this->Promotions = $Promotions;
    }

    /**
     * @param int $PrizeAnotherDepartment
     */
    public function setPrizeAnotherDepartment($PrizeAnotherDepartment)
    {
        $this->PrizeAnotherDepartment = $PrizeAnotherDepartment;
    }

    /**
     * @param int $Overtime
     */
    public function setOvertime($Overtime)
    {
        $this->Overtime = $Overtime;
    }

    /**
     * @param int $NotLessThan
     */
    public function setNotLessThan($NotLessThan)
    {
        $this->NotLessThan = $NotLessThan;
    }

    /**
     * @param int $Defect
     */
    public function setDefect($Defect)
    {
        $this->Defect = $Defect;
    }

    /**
     * @param int $AddUser
     */
    public function setAddUser($AddUser)
    {
        $this->AddUser = $AddUser;
    }

    /**
     * @param int $Avans2Calculation
     */
    public function setAvans2Calculation($Avans2Calculation)
    {
        $this->Avans2Calculation = $Avans2Calculation;
    }

    /**
     * @param int $AvansCalculation
     */
    public function setAvansCalculation($AvansCalculation)
    {
        $this->AvansCalculation = $AvansCalculation;
    }

    /**
     * @param int $Calculation
     */
    public function setCalculation($Calculation)
    {
        $this->Calculation = $Calculation;
    }

    /**
     * @param int $CommercialBlockBonus
     */
    public function setCommercialBlockBonus($CommercialBlockBonus)
    {
        $this->CommercialBlockBonus = $CommercialBlockBonus;
    }

    /**
     * @param int $CommercialBlockNachenka
     */
    public function setCommercialBlockNachenka($CommercialBlockNachenka)
    {
        $this->CommercialBlockNachenka = $CommercialBlockNachenka;
    }

    /**
     * @param int $CommercialBlockProcent
     */
    public function setCommercialBlockProcent($CommercialBlockProcent)
    {
        $this->CommercialBlockProcent = $CommercialBlockProcent;
    }

    /**
     * @param int $CompensationFAL
     */
    public function setCompensationFAL($CompensationFAL)
    {
        $this->CompensationFAL = $CompensationFAL;
    }

    /**
     * @param int $Department
     */
    public function setDepartment($Department)
    {
        $this->Department = $Department;
    }

    /**
     * @param int $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @param int $Import1C
     */
    public function setImport1C($Import1C)
    {
        $this->Import1C = $Import1C;
    }

    /**
     * @param int $ImportAWH
     */
    public function setImportAWH($ImportAWH)
    {
        $this->ImportAWH = $ImportAWH;
    }

    /**
     * @param int $ImportNDFL
     */
    public function setImportNDFL($ImportNDFL)
    {
        $this->ImportNDFL = $ImportNDFL;
    }

    /**
     * @param int $PremiyaCalculation
     */
    public function setPremiyaCalculation($PremiyaCalculation)
    {
        $this->PremiyaCalculation = $PremiyaCalculation;
    }

    /**
     * @param int $ProfileAWH
     */
    public function setProfileAWH($ProfileAWH)
    {
        $this->ProfileAWH = $ProfileAWH;
    }

    /**
     * @param int $ProfilePayments
     */
    public function setProfilePayments($ProfilePayments)
    {
        $this->ProfilePayments = $ProfilePayments;
    }

    /**
     * @param int $ProfilePersonalInfo
     */
    public function setProfilePersonalInfo($ProfilePersonalInfo)
    {
        $this->ProfilePersonalInfo = $ProfilePersonalInfo;
    }

    /**
     * @param int $ProfileWorkInfo
     */
    public function setProfileWorkInfo($ProfileWorkInfo)
    {
        $this->ProfileWorkInfo = $ProfileWorkInfo;
    }

    /**
     * @param int $ReportAWH
     */
    public function setReportAWH($ReportAWH)
    {
        $this->ReportAWH = $ReportAWH;
    }

    /**
     * @param int $ReportCorrecting
     */
    public function setReportCorrecting($ReportCorrecting)
    {
        $this->ReportCorrecting = $ReportCorrecting;
    }

    /**
     * @param int $ReportLeaving
     */
    public function setReportLeaving($ReportLeaving)
    {
        $this->ReportLeaving = $ReportLeaving;
    }

    /**
     * @param int $ReportMemberAWH
     */
    public function setReportMemberAWH($ReportMemberAWH)
    {
        $this->ReportMemberAWH = $ReportMemberAWH;
    }

    /**
     * @param int $ReportRetetition
     */
    public function setReportRetetition($ReportRetetition)
    {
        $this->ReportRetetition = $ReportRetetition;
    }

    /**
     * @param int $ReportTransfer
     */
    public function setReportTransfer($ReportTransfer)
    {
        $this->ReportTransfer = $ReportTransfer;
    }

    /**
     * @param int $Settings
     */
    public function setSettings($Settings)
    {
        $this->Settings = $Settings;
    }

    /**
     * @param int $ShowHistory
     */
    public function setShowHistory($ShowHistory)
    {
        $this->ShowHistory = $ShowHistory;
    }

    /**
     * @param int $ReportJournal
     */
    public function setReportJournal($ReportJournal)
    {
        $this->ReportJournal = $ReportJournal;
    }

    /**
     * @param int $ShowAdministrativeBlock
     */
    public function setShowAdministrativeBlock($ShowAdministrativeBlock)
    {
        $this->ShowAdministrativeBlock = $ShowAdministrativeBlock;
    }

    /**
     * @param int $ShowAdvanceCalculation
     */
    public function setShowAdvanceCalculation($ShowAdvanceCalculation)
    {
        $this->ShowAdvanceCalculation = $ShowAdvanceCalculation;
    }

    /**
     * @param int $ShowAWHBlock
     */
    public function setShowAWHBlock($ShowAWHBlock)
    {
        $this->ShowAWHBlock = $ShowAWHBlock;
    }

    /**
     * @param int $ShowCommercialBlock
     */
    public function setShowCommercialBlock($ShowCommercialBlock)
    {
        $this->ShowCommercialBlock = $ShowCommercialBlock;
    }

    /**
     * @param int $ShowCorrectingBlock
     */
    public function setShowCorrectingBlock($ShowCorrectingBlock)
    {
        $this->ShowCorrectingBlock = $ShowCorrectingBlock;
    }

    /**
     * @param int $ShowRetitiotionBlock
     */
    public function setShowRetitiotionBlock($ShowRetitiotionBlock)
    {
        $this->ShowRetitiotionBlock = $ShowRetitiotionBlock;
    }

    /**
     * @param int $YearBonus
     */
    public function setYearBonus($YearBonus)
    {
        $this->YearBonus = $YearBonus;
    }

    /*
     * **                    GETTERS
     */


    /**
     * @return int
     */
    public function getPayment()
    {
        return $this->Payment;
    }

    /**
     * @return int
     */
    public function getChangeFinanceProbation()
    {
        return $this->ChangeFinanceProbation;
    }

    /**
     * @return int
     */
    public function getYearBonus()
    {
        return $this->YearBonus;
    }

    /**
     * @return mixed
     */
    public function getRoleId()
    {
        return $this->RoleId;
    }

    /**
     * @return string
     */
    public function getDepartmentName()
    {
        return $this->DepartmentName;
    }

    /**
     * @return int
     */
    public function getShowAdministrativeBlock()
    {
        return $this->ShowAdministrativeBlock;
    }

    /**
     * @return int
     */
    public function getShowAdvanceCalculation()
    {
        return $this->ShowAdvanceCalculation;
    }

    /**
     * @return int
     */
    public function getShowAWHBlock()
    {
        return $this->ShowAWHBlock;
    }

    /**
     * @return int
     */
    public function getShowCommercialBlock()
    {
        return $this->ShowCommercialBlock;
    }

    /**
     * @return int
     */
    public function getShowCorrectingBlock()
    {
        return $this->ShowCorrectingBlock;
    }

    /**
     * @return int
     */
    public function getShowRetitiotionBlock()
    {
        return $this->ShowRetitiotionBlock;
    }


    /**
     * @return int
     */
    public function getRaschetCalculation()
    {
        return $this->RaschetCalculation;
    }

    /**
     * @return string
     */
    public function getRoleName()
    {
        return $this->RoleName;
    }

    /**
     * @return int
     */
    public function getMemberId()
    {
        return $this->member_id;
    }

    /**
     * @return int
     */
    public function getCredit()
    {
        return $this->Credit;
    }

    /**
     * @return int
     */
    public function getDepartment()
    {
        return $this->Department;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @return int
     */
    public function getAdministrativePrize()
    {
        return $this->AdministrativePrize;
    }

    /**
     * @return int
     */
    public function getMobileCommunication()
    {
        return $this->MobileCommunication;
    }

    /**
     * @return int
     */
    public function getBonusAdjustment()
    {
        return $this->BonusAdjustment;
    }

    /**
     * @return int
     */
    public function getSchedule()
    {
        return $this->Schedule;
    }

    /**
     * @return int
     */
    public function getReceivables()
    {
        return $this->Receivables;
    }

    /**
     * @return int
     */
    public function getOther()
    {
        return $this->Other;
    }

    /**
     * @return int
     */
    public function getLateWork()
    {
        return $this->LateWork;
    }

    /**
     * @return int
     */
    public function getInternet()
    {
        return $this->Internet;
    }

    /**
     * @return int
     */
    public function getCachAccounting()
    {
        return $this->CachAccounting;
    }

    /**
     * @return int
     */
    public function getTraining()
    {
        return $this->Training;
    }

    /**
     * @return int
     */
    public function getSunday()
    {
        return $this->Sunday;
    }

    /**
     * @return int
     */
    public function getPromotions()
    {
        return $this->Promotions;
    }

    /**
     * @return int
     */
    public function getPrizeAnotherDepartment()
    {
        return $this->PrizeAnotherDepartment;
    }

    /**
     * @return int
     */
    public function getOvertime()
    {
        return $this->Overtime;
    }

    /**
     * @return int
     */
    public function getNotLessThan()
    {
        return $this->NotLessThan;
    }

    /**
     * @return int
     */
    public function getDefect()
    {
        return $this->Defect;
    }

    /**
     * @return int
     */
    public function getAddUser()
    {
        return $this->AddUser;
    }

    /**
     * @return int
     */
    public function getAvans2Calculation()
    {
        return $this->Avans2Calculation;
    }

    /**
     * @return int
     */
    public function getAvansCalculation()
    {
        return $this->AvansCalculation;
    }

    /**
     * @return int
     */
    public function getCalculation()
    {
        return $this->Calculation;
    }

    /**
     * @return int
     */
    public function getChangeFinanceMotivation()
    {
        return $this->ChangeFinanceMotivation;
    }

    /**
     * @return int
     */
    public function getChangeMember()
    {
        return $this->ChangeMember;
    }

    /**
     * @return int
     */
    public function getChangeWorkPersonal()
    {
        return $this->ChangeWorkPersonal;
    }

    /**
     * @return int
     */
    public function getChangeWorkStatus()
    {
        return $this->ChangeWorkStatus;
    }

    /**
     * @return int
     */
    public function getCommercialBlockBonus()
    {
        return $this->CommercialBlockBonus;
    }

    /**
     * @return int
     */
    public function getCommercialBlockNachenka()
    {
        return $this->CommercialBlockNachenka;
    }

    /**
     * @return int
     */
    public function getCommercialBlockProcent()
    {
        return $this->CommercialBlockProcent;
    }

    /**
     * @return int
     */
    public function getCompensationFAL()
    {
        return $this->CompensationFAL;
    }

    /**
     * @return int
     */
    public function getImport1C()
    {
        return $this->Import1C;
    }

    /**
     * @return int
     */
    public function getImportAWH()
    {
        return $this->ImportAWH;
    }

    /**
     * @return int
     */
    public function getImportNDFL()
    {
        return $this->ImportNDFL;
    }

    /**
     * @return int
     */
    public function getPremiyaCalculation()
    {
        return $this->PremiyaCalculation;
    }

    /**
     * @return int
     */
    public function getProfileAWH()
    {
        return $this->ProfileAWH;
    }

    /**
     * @return int
     */
    public function getProfilePayments()
    {
        return $this->ProfilePayments;
    }

    /**
     * @return int
     */
    public function getProfilePersonalInfo()
    {
        return $this->ProfilePersonalInfo;
    }

    /**
     * @return int
     */
    public function getProfileWorkInfo()
    {
        return $this->ProfileWorkInfo;
    }

    /**
     * @return int
     */
    public function getReportAWH()
    {
        return $this->ReportAWH;
    }

    /**
     * @return int
     */
    public function getReportCorrecting()
    {
        return $this->ReportCorrecting;
    }

    /**
     * @return int
     */
    public function getReportLeaving()
    {
        return $this->ReportLeaving;
    }

    /**
     * @return int
     */
    public function getReportMemberAWH()
    {
        return $this->ReportMemberAWH;
    }

    /**
     * @return int
     */
    public function getReportRetetition()
    {
        return $this->ReportRetetition;
    }

    /**
     * @return int
     */
    public function getReportTransfer()
    {
        return $this->ReportTransfer;
    }

    /**
     * @return int
     */
    public function getSettings()
    {
        return $this->Settings;
    }

    /**
     * @return int
     */
    public function getShowHistory()
    {
        return $this->ShowHistory;
    }

    /**
     * @return int
     */
    public function getFinancialStatement()
    {
        return $this->FinancialStatement;
    }

    /**
     * @param int $member_id
     */
    public function setMemberId($member_id)
    {
        $this->member_id = $member_id;
    }


    /**
     * @return int
     */
    public function getReportJournal()
    {
        return $this->ReportJournal;
    }

    /**
     * @param int $FinancialStatement
     */
    public function setFinancialStatement($FinancialStatement)
    {
        $this->FinancialStatement = $FinancialStatement;
    }

    /**
     * @return int
     */
    public function getAWHEdit()
    {
        return $this->AWHEdit;
    }

    public function setFromArray($array) {
        $array = unserialize($array);
        if ($array) {
            foreach ($array as $name => $value) {
//            echo $name." ".$value."<BR>";
                $func = "set" . $name;
                $this->$func($value);
            }
        }
    }

}