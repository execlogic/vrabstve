<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 22.10.18
 * Time: 16:35
 */

class User
{
    private $member_id;
    private $login; // Логин пользователя
    private $Role = array(); // SuperUser, руководитель, пользователь
    private $Auth; // Аутентифицирован ли пользователь
    private $FN;
    private $department;
    private $position;
    private $employment;


    /**
     * @return mixed
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param mixed $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }


    /**
     * @param mixed $employment
     */
    public function setEmployment($employment)
    {
        $this->employment = $employment;
    }

    /**
     * @return mixed
     */
    public function getEmployment()
    {
        return $this->employment;
    }



    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }



    /**
     * @param mixed $FN
     */
    public function setFN($FN)
    {
        $this->FN = $FN;
    }

    /**
     * @return mixed
     */
    public function getFN()
    {
        return $this->FN;
    }

    /**
     * @param mixed $member_id
     */
    public function setMemberId($member_id)
    {
        $this->member_id = $member_id;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @param mixed $Auth
     */
    public function setAuth($Auth)
    {
        $this->Auth = $Auth;
    }

    /**
     * @param mixed $Role
     */
    public function setRole($Role)
    {
        $this->Role = $Role;
    }

    /**
     * @return mixed
     */
    public function getMemberId()
    {
        return $this->member_id;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function getAuth()
    {
        return $this->Auth;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->Role;
    }

}