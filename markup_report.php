<?php
// Время работы скрипта
$start = microtime(true);

require_once "app/ErrorInterface.php";
require_once 'app/MemberInterface.php';
require_once 'admin/User.php';
require_once "admin/RoleInterface.php";
require_once "app/DirectionInterface.php";
require_once "app/Position.php";

require_once 'app/Notify.php';

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

session_start();
if (isset($_SESSION['UserObj'])) {
    $User = $_SESSION['UserObj'];
} else {
    header("Location: index.php");
}

if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

/*
 * Массив с отделами к которым разрешен доступ
 */

$RoleDepartmentFilter = array();

foreach ($Roles as $Role) {
    switch ($Role->getId()) {
        case 2:
        case 4:
            if (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] == 0) {
                continue 2;
            }

            // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
            if ($Role->getDepartment() == 0) {
                $RoleDepartmentFilter = array();
            } else {
                /*
                 * Иначе добавляю в конец массива значение об отделе
                 */
                $RoleDepartmentFilter[] = $Role->getDepartment();
            }
            break;
        case 5:
        case 6:
            $RoleDepartmentFilter = array();
            break 2;
        default:
            break;
    }
}

session_write_close();

$Error = new ErrorInterface();

/*
 * Создаю объект MemberInterface
 */
$MI = new MemberInterface();

$DI = new DepartmentInterface();
$DI->fetchActiveDepartments();
$Departments = $DI->GetDepartments();

$FilterArray = array();

usort($Departments, function ($a, $b) {
    return $a->getName() > $b->getName();
});
/*
 * Исключаю отделы по правам доступа
 */
$tmp_Direction = array();
foreach ($Departments as $k => $d) {
    if ((count($RoleDepartmentFilter) > 0) && (!in_array($d->getId(), $RoleDepartmentFilter))) {
        $tmp_Direction[] = $d->getDirectionId()[0];
        unset($Departments[$k]);
    }
}
$date = date("Y");

if ($_POST) {
    if (isset($_POST['date'])) {
        $date = $_POST['date'];
    } else {
        $date = date("Y");
    }
    $dItems = [];
    if (isset($_POST['DepartmentFilter']))
        $DepartmentFilter = $_POST['DepartmentFilter'];


    if (isset($DepartmentFilter)) {
        $MI->fetchMembersbyDepartment($DepartmentFilter);
    } else {
        $departmentItems = [];
        foreach ($Departments as $department) {
            $departmentItems[] = $department->getId();
        }

        $MI->fetchMembersbyDepartment($departmentItems);
    }

    foreach ($MI->GetMembers() as $member) {
        if ($member->getMotivation()==2 && $member->getWorkStatus()!=1)
            continue;
        $profits = [];
        switch ($member->getMotivation()) {
            case 1:
            case 2:
            case 4:
                $profits = $MI->getProfitByYear($member->getId(), $member->getMotivation(), $date);
                break;
            case 8:
                $profits = $MI->getProfitByYear($member->getDepartment()['id'], $member->getMotivation(), $date);
                break;
            default:
                break;
        }
        $DepartmentId = $member->getDepartment()['id'];
        $DepartmentName = $member->getDepartment()['name'];

        $PositionId = $member->getPosition()['id'];
        $PositionName = $member->getPosition()['name'];

        if (!isset($dItems[$DepartmentId])) {
            $dItems[$DepartmentId] = [
                'name' => $member->getDepartment()['name']
            ];
        }
        if (!isset($dItems[$DepartmentId][$member->getId()]) && !empty($profits)) {
            $dItems[$DepartmentId]['members'][$member->getId()] = [
                'is_leader' => $member->getMotivation()==2?1:0,
                'name' => $member->GetLastName() . " " . $member->GetName(). " " . $member->GetMiddle(),
                'data' => [],
            ];
        }

        foreach ($profits as $item) {
            $item_date = explode("-",$item['date']);
            if ($item['p'] == 0 && $item['b'] == 0 && $item['c'] == 0)
                continue;

            if (isset($dItems[$DepartmentId]['members'][$member->getId()]['data'][$item_date[1].".".$item_date[0]])) {
                $dItems[$DepartmentId]['members'][$member->getId()]['data'][$item_date[1].".".$item_date[0]]['name'] .= (isset($item['name']) ? "<br>".$item['name'] : "");
                $dItems[$DepartmentId]['members'][$member->getId()]['data'][$item_date[1].".".$item_date[0]]['p'] += $item['p'];
                $dItems[$DepartmentId]['members'][$member->getId()]['data'][$item_date[1].".".$item_date[0]]['b'] += $item['b'];
                $dItems[$DepartmentId]['members'][$member->getId()]['data'][$item_date[1].".".$item_date[0]]['c'] += $item['c'];
                $dItems[$DepartmentId]['members'][$member->getId()]['data'][$item_date[1].".".$item_date[0]]['f'] += $item['f'];
            } else {
                $dItems[$DepartmentId]['members'][$member->getId()]['data'][$item_date[1] . "." . $item_date[0]] = [
                    'name' => (isset($item['name']) ? $item['name'] : $DepartmentName),
                    'position' => $PositionName,
                    'p' => $item['p'],
                    'b' => $item['b'],
                    'c' => $item['c'],
                    'f' => $item['f'],
                ];
            }
        }
    }

    foreach (array_keys($dItems) as $dDepartment) {
        usort($dItems[$dDepartment]['members'], function ($a, $b) {
            return $a['is_leader'] < $b['is_leader'];
        });
    }

    usort($dItems, function ($a, $b){
        return $a['name'] > $b['name'];
    });

    if (isset($_POST['Excel'])) {
        try {
            if ($date < 2021) {
                $xlsData = [
                    [
                        "Отдел",
                        "ФИО",
                        "Должность",
                        "Год",
                        "Месяц",
                        "Наценка (y.e)",
                        "Бонусы (y.e)",
                        "Корректировки (y.e)",
                        "Итого (y.e)",
                    ]
                ];
            } else {
                $xlsData = [
                    [
                        "Отдел",
                        "ФИО",
                        "Должность",
                        "Год",
                        "Месяц",
                        "Наценка (руб.)",
                        "Бонусы (руб.)",
                        "Корректировки (руб.)",
                        "Итого (руб.)",
                    ]
                ];
            }

            foreach ($dItems as $departmnet=>$dDepartment) {
                $d_p = 0;
                $d_b = 0;
                $d_c = 0;
                $d_f = 0;
                $xlsData[] = [$dDepartment['name']];
                foreach ($dDepartment['members'] as $member_id => $dItem) {
                    if (empty($dItem['data']))
                        continue;
                    $p = 0;
                    $b = 0;
                    $c = 0;
                    $f = 0;
                    $member_position = NULL;
                    foreach ($dItem['data'] as $date => $item) {
                        $p += $item['p'];
                        $b += $item['b'];
                        $c += $item['c'];
                        $f += $item['f'];
                        $date_array = explode(".", $date);
                        $member_position = $item['position'];
                        $xlsData[] = [
                            str_replace( "<br>", "\n", $item['name']),
                            $dItem['name'],
                            $item['position'],
                            (int)$date_array[1],
                            (int)$date_array[0],
                            $item['p'],
                            $item['b'],
                            $item['c'],
                            $item['f'],
                        ];
                    }
                    if ($dItem['is_leader']) {
                        $d_p = $p;
                        $d_b = $b;
                        $d_c = $c;
                        $d_f = $f;
                    }
                    $xlsData[] = [
                        'Итого: '.$dItem['name'],
                        $dItem['name'],
                        $member_position,
                        NULL,
                        NULL,
                        $p,
                        $b,
                        $c,
                        $f,
                    ];
                }
                $xlsData[] = [
                    'Итого: '.$dDepartment['name'],
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    $d_p,
                    $d_b,
                    $d_c,
                    $d_f,
                ];
            }

            $filename = 'tmp/markup_report_'.date("YmdHis").'.xls';
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $spreadsheet->getDefaultStyle()->getFont()->setSize(8);
//            $spreadsheet->getDefaultStyle()->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $spreadsheet->getActiveSheet()->getStyle('F:I')->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            $spreadsheet->getActiveSheet()
                ->fromArray(
                    $xlsData,  // The data to set
                    NULL        // Array values with this value will not be set
            );
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xls($spreadsheet);
            $writer->save($filename);

            // заставляем браузер показать окно сохранения файла
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($filename));
            header('Content-Transfer-Encoding: binary');
            // читаем файл и отправляем его пользователю
            exit(readfile($filename));

        } catch (Exception $e) {
            echo $e->getMessage()."<BR>";
            exit(0);
        }
    }
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Наценка по отделам</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- Select2 -->
    <link rel="stylesheet" href="../../bower_components/select2/dist/css/select2.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
                Наценка по отделам
            </h4>
            <ol class="breadcrumb">
                <li class="active">Наценка по отделам</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="box box-solid box-plane">
                <div class="box-body">
                    <form class="form-horizontal" method="post">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Выберите дату c: </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker" name="date" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                           value="<?php echo $date; ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Фильтр по отделу: </label>
                            <div class="col-sm-7">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-group"></i>
                                    </div>
                                    <select name="DepartmentFilter[]" class="form-control select2" multiple="multiple" style="width: 100%;border-radius: unset;color:black;" data-placeholder="Выберите отдел">
                                        <?php
                                        foreach ($Departments as $d) {
                                            foreach ($d->getDirectionId() as $dir) {
                                                if (isset($DepartmentFilter)) {
                                                    $result = current(array_filter($DepartmentFilter, function ($D) use ($d) {
                                                        return $D == $d->getId();
                                                    }));

                                                    if ($result != false) {
                                                        echo "<option class='" . $dir . "' value=" . $d->getId() . " selected>" . $d->getName() . "</option>";
                                                    } else {
                                                        echo "<option class='" . $dir . "' value=" . $d->getId() . ">" . $d->getName() . "</option>";
                                                    }
                                                } else {
                                                    echo "<option class='" . $dir . "' value=" . $d->getId() . ">" . $d->getName() . "</option>";
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <div class="col-sm-6"><input type="submit" class="btn btn-flat btn-default pull-right" name="SendDate" value="Получить данные"></div>
                            <div class="col-sm-6"><input type="submit" class="btn btn-flat btn-success pull-left" name="Excel" value="Сохранить в XLS"></div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="box box-solid box-plane">
                <div class="box-body">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Отдел</th>
                            <th>ФИО</th>
                            <th>Должность</th>
                            <th>Год</th>
                            <th>Месяц</th>
                            <th>Наценка <?=($date<2021)?"(y.e)":"(руб.)"?></th>
                            <th>Бонусы <?=($date<2021)?"(y.e)":"(руб.)"?></th>
                            <th>Корректировки <?=($date<2021)?"(y.e)":"(руб.)"?></th>
                            <th>Итого <?=($date<2021)?"(y.e)":"(руб.)"?></th>
                        </tr>
                        </thead>
                        <tbody>
                    <?php
                    foreach ($dItems as $departmnet=>$dDepartment) {
                        $d_p = 0;
                        $d_b = 0;
                        $d_c = 0;
                        $d_f = 0;
                        echo "<tr><th colspan='9' class='text-center'><h5 style='font-weight: bold'>".$dDepartment['name']."</h5></th></tr>";
                        foreach ($dDepartment['members'] as $member_id => $dItem) {
                            if (empty($dItem['data']))
                                continue;
                            $p = 0;
                            $b = 0;
                            $c = 0;
                            $f = 0;
                            foreach ($dItem['data'] as $date => $item) {
                                $p += $item['p'];
                                $b += $item['b'];
                                $c += $item['c'];
                                $f += $item['f'];
                                $date_array = explode(".", $date);
                                echo "<tr>";
                                echo "<td>" . $item['name'] . "</td>"; // Отдел
                                echo "<td>" . $dItem['name'] . "</td>";
                                echo "<td>" . $item['position'] . "</td>";
                                echo "<td>" . $date_array[1] . "</td>";
                                echo "<td>" . $date_array[0] . "</td>";
                                echo "<td>" . number_format($item['p'], 2, ",", " ") . "</td>";
                                echo "<td>" . number_format($item['b'], 2, ",", " ") . "</td>";
                                echo "<td>" . number_format($item['c'], 2, ",", " ") . "</td>";
                                echo "<td>" . number_format($item['f'], 2, ",", " ") . "</td>";
                                echo "</tr>";
                            }
                            if ($dItem['is_leader']) {
                                $d_p = $p;
                                $d_b = $b;
                                $d_c = $c;
                                $d_f = $f;
                            }
                            echo "<tr>";
                            echo "<th>Итого: ".$dItem['name']." </th><th></th><th></th><th></th><th></th>";
                            echo "<th>" . number_format($p, 2, ",", " ") . "</th>";
                            echo "<th>" . number_format($b, 2, ",", " ") . "</th>";
                            echo "<th>" . number_format($c, 2, ",", " ") . "</th>";
                            echo "<th>" . number_format($f, 2, ",", " ") . "</th>";
                            echo "</tr>";
                        }

                        echo "<tr style='background-color: #d9edf7!important;'>";
                        echo "<th><h5 style='font-weight: bold'>Итого ".$dDepartment['name'].": </h5></th><th></th><th></th><th></th><th></th>";
                        echo "<th><h5 style='font-weight: bold'>" . number_format($d_p, 2, ",", " ") . "</h5></th>";
                        echo "<th><h5 style='font-weight: bold'>" . number_format($d_b, 2, ",", " ") . "</h5></th>";
                        echo "<th><h5 style='font-weight: bold'>" . number_format($d_c, 2, ",", " ") . "</h5></th>";
                        echo "<th><h5 style='font-weight: bold'>" . number_format($d_f, 2, ",", " ") . "</h5></th>";
                        echo "</tr>";
                        echo "<tr><td colspan='9'>&nbsp;</td></tr>";
                    }
                    ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<script src="../../bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
    $(function () {
        $('.select2').select2()
    })

    $(function () {
        // Выбор даты
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "years",
            minViewMode: "years",
            format: 'yyyy',
        })
    })
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
