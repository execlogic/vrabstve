<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 05.10.18
 * Time: 13:55
 */

// Время работы скрипта
$start = microtime(true);

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/ErrorInterface.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/MemberInterface.php";

require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Notify.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/RoleInterface.php";

require_once $_SERVER['DOCUMENT_ROOT'] . '/app/PayRollInterface.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/PayRoll.php';

require_once $_SERVER['DOCUMENT_ROOT'] . '/app/DepartmentInterface.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/DirectionInterface.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/AwhInterface.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/ReportStatus.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Premium.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Avans.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Avans2.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/TransportBlock.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/MobileBlock.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/AdministrativeBlock.php';

function roundSumm($summ)
{
    // Округление до 10
    if ($summ > 0) { //Если сумма <= 0 то не обрабатываем данные
        //$summ = round($summ); // округляю сотые и десятые
        $summ = (int)$summ;

        $ostatok = ($summ % 10); // Получаю остаток от деления на 10
        if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
            $summ = $summ - $ostatok + 10;
        } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
            $summ = $summ - $ostatok;
        };
    } else {
        $summ = 0;
    }
    return $summ;
}

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());


/*
 * Права доступа
 */

$ShowExtended = 0;
$ShowLeaving = 0;

if (isset($_REQUEST['ShowExtended'])) {
    $ShowExtended = 1;
}

/*
 *  Если у пользользователя не стоит доступ в FinancialStatement, то выкидываем его в 404
 */
if (!array_filter($Roles, function ($Role) {
    return ($Role->getFinancialStatement() == 1);
})) {
    header("Location: 404.php");
    exit();
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

/*
 *  Настройки
 */


/*
 * Массив с отделами к которым разрешен доступ
 */

$RoleDepartmentFilter = array();
$is_director = 0;

foreach ($Roles as $Role) {
    if ($Role->getId() == 2 || $Role->getId() == 4) {
        if (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[] = $Role->getDepartment();
        }
    }
    if ($Role->getId() == 5) {
        $is_director = 1;
        $RoleDepartmentFilter = array(0);
        break;
    }
}


function addToPayRoll($date, $TypeOfPayment, $member, &$PayRollArray)
{
    $user_id = $member->getId();
    if ($TypeOfPayment == 1) {
        $PR = new Premium();
        $PR->setMember($member);
        $PR->setDate($date->format("m.Y"));
        try {
            $PR->fetch($member->getId());
        } catch (Exception $e) {
            var_dump($e);
        }
        $CR = $PR->getCR();
        /*
        * Если выставлен хоть у кого-то годовой бонус и он больше 100к, то
        * выставляю флаг что есть годовой бонус.
        */
        if ($CR->getYearBonus() > 100000) {
            $YearBonusStatus = true;
        }
    } else if ($TypeOfPayment == 2) {
        $AV = new Avans();
        $AV->setMember($member);
        $AV->setDate($date->format("m.Y"));
        $AV->fetch($user_id);
    } else if ($TypeOfPayment == 3) {
        $AV2 = new Avans2();
        $AV2->setMember($member);
        $AV2->setDate($date->format("m.Y"));
        $AV2->fetch($user_id);
    }

    if ($TypeOfPayment == 1 || $TypeOfPayment == 3) {
        $TR = new TransportBlock();
        $MB = new MobileBlock();
    }

    // Создаю объект ведомости и заполняю его
    $PayRoll = new PayRoll();
    $PayRoll->setDate($date->format("m.Y")); // Дата
    $PayRoll->setFlyCalculation(1);
    $PayRoll->setRowId(PHP_INT_MAX);
    $PayRoll->setTypeOfPayment($TypeOfPayment); // Тип выплаты
    $PayRoll->setId($user_id); // Пользовательский ID

    $PayRoll->setDirection($member->getDirection()); // Напраление
    $PayRoll->setDepartment($member->getDepartment()); // Отдел
    $PayRoll->setPosition($member->getPosition()); // Должность


    $PayRoll->setFio($member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()); // ФИО
    $PayRoll->setSalary($member->getSalary()); // Оклад

    if ($TypeOfPayment == 1) {
        $PayRoll->setNDFL($PR->getNdfl()); // НДФЛ
        $PayRoll->setRO($PR->getRo()); // ИЛ
    } else if ($TypeOfPayment == 3) {
        $PayRoll->setNDFL($AV2->getNdfl()); // НДФЛ
        $PayRoll->setRO($AV2->getRo()); // ИЛ
    }

//            $PayRoll->setAdvancePayment($All_Salary); // АВАНС
    if ($TypeOfPayment == 1) {
        $PayRoll->setAdvancePayment($PR->getAllSalary()); // АВАНС

        $TR->getTransport($date->format("m.Y"), $TypeOfPayment, $user_id);
        $PayRoll->setTransport($TR->getSumm());
        $MB->getMobile($date->format("m.Y"), $TypeOfPayment, $user_id);
        $PayRoll->setMobile($MB->getSumm());
    } else if ($TypeOfPayment == 2) {
        $PayRoll->setAdvancePayment($AV->getAllSalary());
    } else if ($TypeOfPayment == 3) {
        $PayRoll->setAdvancePayment($AV2->getAllSalary());
        $TR->getTransport($date->format("m.Y"), $TypeOfPayment, $user_id);
        $PayRoll->setTransport($TR->getSumm());
        $MB->getMobile($date->format("m.Y"), $TypeOfPayment, $user_id);
        $PayRoll->setMobile($MB->getSumm());
    };

    if ($TypeOfPayment == 1) {
        $PayRoll->setAwhdata(["absence" => $PR->getAbsence(), "holiday" => $PR->getHoliday(), "disease" => $PR->getDisease()]);
        $PayRoll->setAdministrativeData($PR->getAB_Data());
        $PayRoll->setRetentionData($PR->getRN_Data());
        $PayRoll->setCorrectionData($PR->getCR_Data());

        $PayRoll->setAbsences($PR->getAllAbsenceRub()); // Отсутствия
        if ($member->getMotivation() == 7) {
            $PayRoll->setCommercial(0); // Коммерческая премия
        } else {
            $PayRoll->setCommercial($PR->getAllMarkupRubles()); // Коммерческая премия
        }
        $PayRoll->setAdministrative($PR->getAllAdministrativeRubles()); // Административная премия
        $PayRoll->setHold($PR->getAllHoldRubles()); // Удержания
        $PayRoll->setCorrecting($PR->getAllCorrectionRubles()); // Корректировки
        $PayRoll->setKPI($PR->getKPISumm());

        $PayRoll->setYearbonusPay($PR->getYearBonusPay());

        $PayRoll->setBaseprocentId($PR->getBasepercentId());
        $PayRoll->setAdvancedProcentId($PR->getAdvancedPercentId());
    } else if ($TypeOfPayment == 2) {
        $PayRoll->setAwhdata(["absence" => $AV->getAbsence(), "holiday" => $AV->getHoliday(), "disease" => $AV->getDisease()]);

        $PayRoll->setAbsences($AV->getAllAbsenceRub()); // Отсутствия
        $PayRoll->setCommercial(0); // Коммерческая премия
        $PayRoll->setAdministrative($AV->getAllAdministrativeRubles()); // Административная премия
        $PayRoll->setHold($AV->getAllHoldRubles()); // Удержания
        $PayRoll->setCorrecting(0); // Корректировки
    } else if ($TypeOfPayment == 3) {
        $PayRoll->setAwhdata(["absence" => $AV2->getAbsence(), "holiday" => $AV2->getHoliday(), "disease" => $AV2->getDisease()]);
        $PayRoll->setAbsences($AV2->getAllAbsenceRub()); // Отсутствия
        $PayRoll->setCommercial(0); // Коммерческая премия
        $PayRoll->setAdministrative($AV2->getAllAdministrativeRubles()); // Административная премия
        $PayRoll->setHold($AV2->getAllHoldRubles()); // Удержания
        $PayRoll->setCorrecting($AV2->getAllCorrectionRubles()); // Корректировки
    }

    if ($item['type_of_payment'] == 1 || $item['type_of_payment'] == 3) {
        /*
            * Таб Административный объект
            */
        $AB = new AdministrativeBlock();
        /*
         * Тип выплат премия
         */
        $AB->setTypeOfPayment($TypeOfPayment);
        $AB->fetch($user_id, $date->format("m.Y"));
        $PayRoll->setAdministrativeData(["Promotions" => $AB->getPromotions(), "AdministrativePrize" => $AB->getAdministrativePrize(), "Training" => $AB->getTraining(), "PrizeAnotherDepartment" => $AB->getPrizeAnotherDepartment(), "Defect" => $AB->getDefect(), "Sunday" => $AB->getSunday(), "NotLessThan" => $AB->getNotLessThan(), "Overtime" => $AB->getOvertime()]);
        unset($AB);

        $CR = new Correction();
        $CR->setTypeOfPayment($TypeOfPayment);
        $CR->fetch($user_id, $date->format("m.Y"));
        $PayRoll->setCorrectionData(["BonusAdjustment" => $CR->getBonusAdjustment(), "Calculation" => $CR->getCalculation(), "YearBonus" => $CR->getYearBonus()]);
        unset($CR);

        $RN = new Retention();
        $RN->setTypeOfPayment($TypeOfPayment);
        $RN->fetch($user_id, $date->format("m.Y"));
        $PayRoll->setRetentionData(["LateWork" => $RN->getLateWork(), "Internet" => $RN->getInternet(), "CachAccounting" => $RN->getCachAccounting(), "Receivables" => $RN->getReceivables(), "Credit" => $RN->getCredit(), "Other" => $RN->getOther()]);
        unset($RN);

    }

    $PayRoll->setMotivation($member->getMotivation());
    $PayRoll->Calculation();
    $PayRoll->setSumm(roundSumm($PayRoll->getSumm()));

    //Добавляю в массив объектов
    $PayRollArray[] = $PayRoll;
}

$Error = new ErrorInterface();
$DirectorsData = $RI->getSubordination();

/*
 * Создаю объект MemberInterface
 */
$Members = new MemberInterface();
if (isset($_POST['ShowLeaving'])) {
    $ShowLeaving = 1;
}

$DI = new DepartmentInterface();
$DI->fetchDepartments();
$Departments = $DI->GetDepartments();

$DR = new DirectionInterface();
$DR->fetchDirections();
$Directions = $DR->GetDirections();

/*
 * Создаю объект статуса ведомости
 */
$RS = new ReportStatus();

/*
 * Получаю список пользователей с минимальными данными
 * Только пользователей которые работают, уволенных не берем
 */
if (isset($_POST['ShowLeaving']))
    $ShowLeaving = 1;

if ($ShowLeaving == 0) {
    $Members->fetchMembers(false);
} else {
    $Members->fetchMembers();
}

$MemberList = $Members->GetMembers();

$AWH = new AwhInterface();
//}


/*
 * Удаляю лишние отделы, из-за роли
 */
if ($is_director == 0) {
    foreach ($Departments as $k => $d) {
        if (count($RoleDepartmentFilter) != 0) {
            if (!in_array($d->getId(), $RoleDepartmentFilter)) {
                unset($Departments[$k]);
            }
        }
    }
}
/*
 * Удаляю лишних сотрудников, из-за роли
 */
if ($is_director == 0) {
    foreach ($MemberList as $k => $m) {
        if (count($RoleDepartmentFilter) != 0) {
            if (!in_array($m->getDepartment()["id"], $RoleDepartmentFilter)) {
                unset($MemberList[$k]);
            }
        }
    }
}

$DirectorList = array();
foreach ($DirectorsData as $d) {
    if ($is_director == 0 && (count($RoleDepartmentFilter) != 0) && !in_array($d['id'], $RoleDepartmentFilter)) {
        continue;
    }

    $DirectorList[$d["director_id"]] = array("id" => $d["id"], "director_id" => $d["director_id"], "director_name" => $d["director_name"]);
}


/*
* Создам массив для данных
*/
if (isset($_POST['PushButton_1'])) {
    /*
   * Создам массив для данных
   */
    $PayRollArray = array();
    $TypeOfPayment = $_POST['type_of_payment'];
    /*
    * Создаю объект интерфейса ведомости
    */
    $PI = new PayRollInterface();
}

$dates[0] = date("Y-m");
$dates[1] = date("Y-m");

if (isset($_POST['date']) && isset($_POST['date_2'])) {
//    $dates = explode("-", $_POST['daterangepicker']);

//    $date_picker = $dates;

//    for ($i=0; $i<count($dates); $i++) {
//        $t = explode("/", trim($dates[$i]));
//        $dates[$i] = $t[1]."-".$t[0];
//    }

    $dates[0] = $_POST['date'];
    $dates[1] = $_POST['date_2'];

    $t1 = strtotime($dates[0]);
    $t2 = strtotime($dates[1]);
    if ($t1 > $t1) {
        echo "Дата начала периода не может быть больше чем дата конца периода<BR>";
    }
}
if (isset($_POST['DepartmentNot'])) {
    $DepartmentNot = 1;
} else {
    $DepartmentNot = 0;
}

if (isset($_POST['DirectionNot'])) {
    $DirectionNot = 1;
} else {
    $DirectionNot = 0;
}

if (!isset($_['DirectorID']) && !isset($_POST['DirectionID']) && !isset($_POST['DepartmentId']) && !isset($_POST['MemberId']) && isset($_POST['PushButton_1'])) {
    $AllDate = true;
}

if (isset($_POST['DirectorID']) || isset($AllDate)) {
    /*
     * Получаю данные по направлению и отдела по ID выдающего директора
     */

    if (!isset($AllDate)) {
        foreach ($_POST['DirectorID'] as $Dir) {
            foreach ($DirectorsData as $d) {
                if ($d['director_id'] == $Dir) {
                    $DepartmetnsList[] = $d["id"];
                }
            }
        }
    } else {
        foreach ($DirectorsData as $d) {
            $DepartmetnsList[] = $d["id"];
        }
    }

    foreach ($MemberList as $k => $m) {
        if (!in_array($m->getDepartment()['id'], $DepartmetnsList)) {
            unset($MemberList[$k]);
        }
    }

    $DepartmetnsList = array_unique($DepartmetnsList);

    foreach ($Departments as $k => $v) {
        if (!in_array($v->getId(), $DepartmetnsList)) {
            unset($Departments[$k]);
        }
    }
    foreach ($Departments as $value) {
        $DirectionList[] = current($value->getDirectionId());
    }

    $DirectionList = array_unique($DirectionList);

    foreach ($Directions as $k => $v) {
        if (!in_array($v->getId(), $DirectionList)) {
            unset($Directions[$k]);
        }
    }

    if ((!isset($_POST['DepartmentId'])) && (!isset($_POST['MemberId']))) {
        foreach ($DepartmetnsList as $DepartmentId) {
            $Data = $PI->getDepartmentFinancialPayout($DepartmentId, $TypeOfPayment, $dates[0] . "-00", $dates[1] . "-00");
            if (!is_null($Data)) {
                foreach ($Data as $item) {
                    $member = $Members->fetchMemberByID($item['member_id']);

                    if (isset($_POST['DirectionId'])) {
                        if (!$DirectionNot) {
                            if (!in_array($item['direction_id'], $_POST['DirectionId'])) {
                                continue;
                            }
                        } else {
                            if (in_array($item['direction_id'], $_POST['DirectionId'])) {
                                continue;
                            }
                        }
                    }

                    $PayRoll = new PayRoll(); //Создаем объект
                    $PayRoll->setDate($item['date']); // устанавливаем дату
                    $PayRoll->setTypeOfPayment($item['type_of_payment']);// Тип выплаты
                    $PayRoll->setId($item['member_id']); // Получаю id пользователя

                    switch ($item['type_of_payment']) {
                        case 1:
                            $PayRoll->order = 3;
                            break;
                        case 2:
                            $PayRoll->order = 1;
                            break;
                        case 3:
                            $PayRoll->order = 2;
                            break;
                        default:
                            $PayRoll->order = $item['type_of_payment'];
                            break;
                    }

                    if (isset($item['direction_id'])) {
                        $member_direction = current(array_filter($Directions, function ($d) use ($item) {
                            return $d->getId() == $item['direction_id'];
                        }));
                    }

                    if (isset($item['department_id'])) {
                        $member_departmnet = current(array_filter($Departments, function ($d) use ($item) {
                            return $d->getId() == $item['department_id'];
                        }));
                    }

                    if (!$member_departmnet)
                        continue;

                    if (isset($item['direction_id'])) {
                        $PayRoll->setDirection(array("name" => $member_direction->getName())); // Получаю Направление
                    } else {
                        $PayRoll->setDirection("None"); // Получаю Направление
                    }


                    if (isset($item['department_id'])) {
                        $PayRoll->setDepartment(array("name" => $member_departmnet->getName())); // Получаю отдел
                    } else {
                        $PayRoll->setDepartment("None"); // Получаю отдел
                    }

                    $PayRoll->setFio($member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()); // Устанавливаю ФИО
                    $PayRoll->setSalary($item['Salary']); // получаю данные по окладу
                    $PayRoll->setNDFL($item['ndfl']); // Получаю НДФЛ
                    $PayRoll->setRO($item['ro']); // Получаю ИЛ
                    $PayRoll->setAdvancePayment($item['PrepaidExpense']); // Получаю выплаченный аванс
                    $PayRoll->setAbsences($item['absence']); // Отсутствия
                    $PayRoll->setCommercial($item['CommercialPremium']); // Устанавливаю коммерческую премию
                    $PayRoll->setKPI($item['Kpi']); // Устанавливаю Административную премию
                    $PayRoll->setAdministrative($item['AdministrativePremium']); // Устанавливаю Административную премию
                    $PayRoll->setHold($item['Hold']); // Устанавливаю удержания
                    $PayRoll->setCorrecting($item['Correcting']); //Устанавливаю корректировки
                    $PayRoll->setYearbonusPay($item['YearBonusPay']); // Отчисления в ГБ
                    $PayRoll->setMobile($item['Mobile']);
                    $PayRoll->setTransport($item['Transport']);

                    if ($item['type_of_payment'] == 1 || $item['type_of_payment'] == 3) {
                        /*
                            * Таб Административный объект
                            */
                        $AB = new AdministrativeBlock();
                        /*
                         * Тип выплат премия
                         */
                        $AB->setTypeOfPayment($item['type_of_payment']);
                        $AB->fetch($item['member_id'], $item['date']);
                        $PayRoll->setAdministrativeData(["Promotions" => $AB->getPromotions(), "AdministrativePrize" => $AB->getAdministrativePrize(), "Training" => $AB->getTraining(), "PrizeAnotherDepartment" => $AB->getPrizeAnotherDepartment(), "Defect" => $AB->getDefect(), "Sunday" => $AB->getSunday(), "NotLessThan" => $AB->getNotLessThan(), "Overtime" => $AB->getOvertime()]);
                        unset($AB);

                        $CR = new Correction();
                        $CR->setTypeOfPayment($item['type_of_payment']);
                        $CR->fetch($item['member_id'], $item['date']);
                        $PayRoll->setCorrectionData(["BonusAdjustment" => $CR->getBonusAdjustment(), "Calculation" => $CR->getCalculation(), "YearBonus" => $CR->getYearBonus()]);
                        unset($CR);

                        $RN = new Retention();
                        $RN->setTypeOfPayment($item['type_of_payment']);
                        $RN->fetch($item['member_id'], $item['date']);
                        $PayRoll->setRetentionData(["LateWork" => $RN->getLateWork(), "Internet" => $RN->getInternet(), "CachAccounting" => $RN->getCachAccounting(), "Receivables" => $RN->getReceivables(), "Credit" => $RN->getCredit(), "Other" => $RN->getOther()]);
                        unset($RN);

                    }

                    $TR = new TransportBlock();
                    $MB = new MobileBlock();
                    $TR->getTransport($item['date'], $TypeOfPayment, $item['member_id']);
                    $PayRoll->setTransport($TR->getSumm());
                    $MB->getMobile($item['date'], $TypeOfPayment, $item['member_id']);
                    $PayRoll->setMobile($MB->getSumm());

                    $PayRoll->setMotivation($member->getMotivation());
                    $PayRoll->setSumm($item['Total']);
//                    $PayRoll->Calculation();

                    if (isset($item['AwhData']) && !is_null($item['AwhData'])) {
                        $PayRoll->setAwhdata(unserialize($item['AwhData']));
                    } else {
                        $PayRoll->setAwhdata($AWH->getAwhByMember($item['date'], $item['type_of_payment'], $item['member_id']));
                    }

//            $PayRoll->setAbsencesDays($awh_disease);
//            $PayRoll->setDisease($awh_disease);
//            $PayRoll->setHoliday($awh_holiday);

                    if (isset($item['AdministrativeData']) && !is_null($item['AdministrativeData'])) {
                        $PayRoll->setAdministrativeData(unserialize($item['AdministrativeData']));
                    }
//        var_dump(unserialize($item['AdministrativeData']));
                    if (isset($item['RetentionData']) && !is_null($item['RetentionData'])) {
                        $PayRoll->setRetentionData(unserialize($item['RetentionData']));
                    }

                    if (isset($item['CorrectionData']) && !is_null($item['CorrectionData'])) {
                        $PayRoll->setCorrectionData(unserialize($item['CorrectionData']));
                    }

                    $PayRollArray[] = $PayRoll; // Добавляю в массив объектов
                }
            }
        }
    }
    usort($PayRollArray, function ($a, $b) {
        if ($a->getDepartment()['name'] == $b->getDepartment()['name']) {
            if ($a->getFio() == $b->getFio()) {
                if (strtotime("01.".$a->getDate()) == strtotime("01.".$b->getDate())) {
                    return ($a->order>$b->order) ? 1 : -1;
                };
                return (strtotime("01.".$a->getDate()) > strtotime("01.".$b->getDate())) ? 1 : -1;
            }

            return ($a->getFio() > $b->getFio()) ? 1 : -1;
        }

        return ($a->getDepartment()['name'] > $b->getDepartment()['name']) ? 1 : -1;
    });
}

if (isset($_POST['DirectionId'])) {
    $DirectionId = $_POST['DirectionId'];

    foreach ($Departments as $k => $v) {
        if (!$DirectionNot) {
            if (!in_array(current($v->getDirectionId()), $DirectionId)) {
                unset($Departments[$k]);
            }
        } else {
            if (in_array(current($v->getDirectionId()), $DirectionId)) {
                unset($Departments[$k]);
            }
        }
    }

    foreach ($MemberList as $k => $m) {
        if (!$DirectionNot) {
            if (!in_array($m->getDirection()['id'], $DirectionId)) {
                unset($MemberList[$k]);
            }
        } else {
            if (in_array($m->getDirection()['id'], $DirectionId)) {
                unset($MemberList[$k]);
            }
        }
    }
}

if (isset($_POST['DepartmentId'])) {

    foreach ($MemberList as $k => $m) {

        if (isset($_POST['DirectionId'])) {
            if (!$DirectionNot) {
                if (!in_array($item['direction_id'], $_POST['DirectionId'])) {
                    continue;
                }
            } else {
                if (in_array($item['direction_id'], $_POST['DirectionId'])) {
                    continue;
                }
            }
        }

        if ($DepartmentNot == 0) {
            if (!in_array($m->getDepartment()['id'], $_POST['DepartmentId'])) {
                unset($MemberList[$k]);
            }
        } else {
            if (in_array($m->getDepartment()['id'], $_POST['DepartmentId'])) {
                unset($MemberList[$k]);
            }
        }
    }

    $Department_Filter = array();

    if ($DepartmentNot == 1) {
        foreach ($Departments as $d) {
            if (!in_array($d->getId(), $_POST['DepartmentId'])) {
                $Department_Filter[] = $d->getId();
            }
        }
    } else {
        $Department_Filter = $_POST['DepartmentId'];
    }

    if (!isset($_POST['MemberId'])) {
        foreach ($Department_Filter as $DepartmentId) {
            $Data = $PI->getDepartmentFinancialPayout($DepartmentId, $TypeOfPayment, $dates[0] . "-00", $dates[1] . "-00");
            if (!is_null($Data)) {
                foreach ($Data as $item) {
                    $member = $Members->fetchMemberByID($item['member_id']);

                    $PayRoll = new PayRoll(); //Создаем объект
                    $PayRoll->setDate($item['date']); // устанавливаем дату
                    $PayRoll->setTypeOfPayment($item['type_of_payment']);// Тип выплаты
                    switch ($item['type_of_payment']) {
                        case 1:
                            $PayRoll->order = 3;
                            break;
                        case 2:
                            $PayRoll->order = 1;
                            break;
                        case 3:
                            $PayRoll->order = 2;
                            break;
                        default:
                            $PayRoll->order = $item['type_of_payment'];
                            break;
                    }
                    $PayRoll->setId($item['member_id']); // Получаю id пользователя


                    if (isset($item['direction_id'])) {
                        $member_direction = current(array_filter($Directions, function ($d) use ($item) {
                            return $d->getId() == $item['direction_id'];
                        }));
                    }

                    if (isset($item['department_id'])) {
                        $member_departmnet = current(array_filter($Departments, function ($d) use ($item) {
                            return $d->getId() == $item['department_id'];
                        }));
                    }

                    if (!$member_departmnet)
                        continue;

                    if (isset($item['direction_id'])) {
                        $PayRoll->setDirection(array("name" => $member_direction->getName())); // Получаю Направление
                    } else {
                        $PayRoll->setDirection("None"); // Получаю Направление
                    }

                    if (isset($item['department_id'])) {
                        $PayRoll->setDepartment(array("name" => $member_departmnet->getName())); // Получаю отдел
                    } else {
                        $PayRoll->setDepartment("None"); // Получаю отдел
                    }

                    $PayRoll->setFio($member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()); // Устанавливаю ФИО
                    $PayRoll->setSalary($item['Salary']); // получаю данные по окладу
                    $PayRoll->setNDFL($item['ndfl']); // Получаю НДФЛ
                    $PayRoll->setRO($item['ro']); // Получаю ИЛ
                    $PayRoll->setAdvancePayment($item['PrepaidExpense']); // Получаю выплаченный аванс
                    $PayRoll->setAbsences($item['absence']); // Отсутствия
                    $PayRoll->setCommercial($item['CommercialPremium']); // Устанавливаю коммерческую премию
                    $PayRoll->setKPI($item['Kpi']); // Устанавливаю Административную премию
                    $PayRoll->setAdministrative($item['AdministrativePremium']); // Устанавливаю Административную премию

                    if ($item['type_of_payment'] == 1 || $item['type_of_payment'] == 3) {
                        /*
                            * Таб Административный объект
                            */
                        $AB = new AdministrativeBlock();
                        /*
                         * Тип выплат премия
                         */
                        $AB->setTypeOfPayment($item['type_of_payment']);
                        $AB->fetch($item['member_id'], $item['date']);
                        $PayRoll->setAdministrativeData(["Promotions" => $AB->getPromotions(), "AdministrativePrize" => $AB->getAdministrativePrize(), "Training" => $AB->getTraining(), "PrizeAnotherDepartment" => $AB->getPrizeAnotherDepartment(), "Defect" => $AB->getDefect(), "Sunday" => $AB->getSunday(), "NotLessThan" => $AB->getNotLessThan(), "Overtime" => $AB->getOvertime()]);
                        unset($AB);

                        $CR = new Correction();
                        $CR->setTypeOfPayment($item['type_of_payment']);
                        $CR->fetch($item['member_id'], $item['date']);
                        $PayRoll->setCorrectionData(["BonusAdjustment" => $CR->getBonusAdjustment(), "Calculation" => $CR->getCalculation(), "YearBonus" => $CR->getYearBonus()]);
                        unset($CR);

                        $RN = new Retention();
                        $RN->setTypeOfPayment($item['type_of_payment']);
                        $RN->fetch($item['member_id'], $item['date']);
                        $PayRoll->setRetentionData(["LateWork" => $RN->getLateWork(), "Internet" => $RN->getInternet(), "CachAccounting" => $RN->getCachAccounting(), "Receivables" => $RN->getReceivables(), "Credit" => $RN->getCredit(), "Other" => $RN->getOther()]);
                        unset($RN);

                    }

                    $PayRoll->setHold($item['Hold']); // Устанавливаю удержания
                    $PayRoll->setCorrecting($item['Correcting']); //Устанавливаю корректировки
                    $PayRoll->setYearbonusPay($item['YearBonusPay']); // Отчисления в ГБ
                    $PayRoll->setMotivation($member->getMotivation());
                    $PayRoll->setSumm($item['Total']);
//                    $PayRoll->Calculation();
                    $TR = new TransportBlock();
                    $MB = new MobileBlock();
                    $TR->getTransport($item['date'], $TypeOfPayment, $item['member_id']);
                    $PayRoll->setTransport($TR->getSumm());
                    $MB->getMobile($item['date'], $TypeOfPayment, $item['member_id']);
                    $PayRoll->setMobile($MB->getSumm());

                    if (isset($item['AwhData']) && !is_null($item['AwhData'])) {
                        $PayRoll->setAwhdata(unserialize($item['AwhData']));
                    } else {
                        $PayRoll->setAwhdata($AWH->getAwhByMember($item['date'], $item['type_of_payment'], $item['member_id']));
                    }

                    if (isset($item['AdministrativeData']) && !is_null($item['AdministrativeData'])) {
                        $PayRoll->setAdministrativeData(unserialize($item['AdministrativeData']));
                    }
//        var_dump(unserialize($item['AdministrativeData']));
                    if (isset($item['RetentionData']) && !is_null($item['RetentionData'])) {
                        $PayRoll->setRetentionData(unserialize($item['RetentionData']));
                    }

                    if (isset($item['CorrectionData']) && !is_null($item['CorrectionData'])) {
                        $PayRoll->setCorrectionData(unserialize($item['CorrectionData']));
                    }

                    $PayRollArray[] = $PayRoll; // Добавляю в массив объектов
                }
            }
        }
    }

    usort($PayRollArray, function ($a, $b) {
        if ($a->getDepartment()['name'] == $b->getDepartment()['name']) {
            if ($a->getFio() == $b->getFio()) {
                if (strtotime("01.".$a->getDate()) == strtotime("01.".$b->getDate())) {
//                    return ($a->getRowId()>$b->getRowId()) ? 1 : -1;
                    return ($a->order>$b->order) ? 1 : -1;
                };
                return (strtotime("01.".$a->getDate()) > strtotime("01.".$b->getDate())) ? 1 : -1;
            }

            return ($a->getFio() > $b->getFio()) ? 1 : -1;
        }

        return ($a->getDepartment()['name'] > $b->getDepartment()['name']) ? 1 : -1;
    });
}

if (isset($_POST['MemberId'])) {
    foreach ($_POST['MemberId'] as $MemberId) {
        $begin = DateTime::createFromFormat("Y-m-d", $dates[0] . "-01");
        $end = DateTime::createFromFormat("Y-m-d", $dates[1] . "-01");

        $MemberData = $PI->getMemberFinancialPayout($MemberId, $TypeOfPayment, $dates[0] . "-00", $dates[1] . "-00");

        $member = current(array_filter($MemberList, function ($m) use ($MemberId) {
            return $m->getId() == $MemberId;
        }));

        if ($TypeOfPayment != 0) {
            for ($i = $begin; $i <= $end; $i->modify('+1 month')) {
                if (is_array($MemberData)) {
                    $return = current(array_filter($MemberData, function ($MD) use ($i) {
                        return $MD["date"] == $i->format("m.Y");
                    }));
                } else {
                    $return = false;
                }
                //    if ()
//        return false;

                if ($return == false && $member->getDismissalDate() == "01.01.9999") {
                    addToPayRoll($i, $TypeOfPayment, $member, $PayRollArray);
                }
            }
        } else {
            $date_now=new DateTime(date('Y-m-d H:i:s'));
            $date_now->modify('first day of this month');
            $date_max = clone $date_now;
            $date_now->modify('-1 month');
            $date_max->modify('+1 month');


            for ($i = $begin; $i <= $end; $i->modify('+1 month')) {
                $return = current(array_filter($MemberData, function ($MD) use ($i) {
                    return $MD["date"] == $i->format("m.Y") && $MD['type_of_payment'] == 1;
                }));

                if ($i<$date_now || $i > $date_max) {
                    continue;
                }

                if ($return == false && $member->getDismissalDate() == "01.01.9999") {
                    addToPayRoll($i, 1, $member, $PayRollArray);
                }

                $return = current(array_filter($MemberData, function ($MD) use ($i) {
                    return $MD["date"] == $i->format("m.Y") && $MD['type_of_payment'] == 2;
                }));

                if ($return == false && $member->getDismissalDate() == "01.01.9999") {
                    addToPayRoll($i, 2, $member, $PayRollArray);
                }
            }
        }

        if (is_null($MemberData)) {
            continue;
        }

        foreach ($MemberData as $item) {
            /*
            * Если присутствует в FIX таблице, то вывожу данные
            */

            $PayRoll = new PayRoll(); //Создаем объект
            $PayRoll->setRowId($item['id']);
            $PayRoll->setDate($item['date']); // устанавливаем дату
            $PayRoll->setTypeOfPayment($item['type_of_payment']);// Тип выплаты
            $PayRoll->setId($item['member_id']); // Получаю id пользователя

            switch ($item['type_of_payment']) {
                case 1:
                    $PayRoll->order = 3;
                    break;
                case 2:
                    $PayRoll->order = 1;
                    break;
                case 3:
                    $PayRoll->order = 2;
                    break;
                default:
                    $PayRoll->order = $item['type_of_payment'];
                    break;
            }

            if (isset($item['direction_id'])) {
                $member_direction = current(array_filter($Directions, function ($d) use ($item) {
                    return $d->getId() == $item['direction_id'];
                }));
            }

            if (isset($item['department_id'])) {
                $member_departmnet = current(array_filter($Departments, function ($d) use ($item) {
                    return $d->getId() == $item['department_id'];
                }));
            }

            if (!$member_departmnet) {
                $DepartmentsAll = $DI->GetDepartments();
                if (isset($item['department_id'])) {
                    $member_departmnet = current(array_filter($DepartmentsAll, function ($d) use ($item) {
                        return $d->getId() == $item['department_id'];
                    }));
                }
            }

            if (isset($item['direction_id'])) {
                $PayRoll->setDirection(["name" => $member_direction->getName()]); // Получаю Направление
            } else {
                $PayRoll->setDirection("None"); // Получаю Направление
            }

            if (isset($item['department_id']) && $member_departmnet) {
                $PayRoll->setDepartment(["name" => $member_departmnet->getName()]); // Получаю отдел
            } else {
                $PayRoll->setDepartment("None"); // Получаю отдел
            }


            $PayRoll->setFio($member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()); // Устанавливаю ФИО
            $PayRoll->setSalary($item['Salary']); // получаю данные по окладу
            $PayRoll->setNDFL($item['ndfl']); // Получаю НДФЛ
            $PayRoll->setRO($item['ro']); // Получаю ИЛ
            $PayRoll->setAdvancePayment($item['PrepaidExpense']); // Получаю выплаченный аванс
            $PayRoll->setAbsences($item['absence']); // Отсутствия
            $PayRoll->setCommercial($item['CommercialPremium']); // Устанавливаю коммерческую премию
            $PayRoll->setKPI($item['Kpi']); // Устанавливаю Административную премию
            $PayRoll->setAdministrative($item['AdministrativePremium']); // Устанавливаю Административную премию
            $PayRoll->setHold($item['Hold']); // Устанавливаю удержания
            $PayRoll->setCorrecting($item['Correcting']); //Устанавливаю корректировки
            $PayRoll->setYearbonusPay($item['YearBonusPay']); // Отчисления в ГБ
            $PayRoll->setMotivation($member->getMotivation());
            $PayRoll->setSumm($item['Total']);
            $TR = new TransportBlock();
            $MB = new MobileBlock();
            $TR->getTransport($item['date'], $TypeOfPayment, $item['member_id']);
            $PayRoll->setTransport($TR->getSumm());
            $MB->getMobile($item['date'], $TypeOfPayment, $item['member_id']);
            $PayRoll->setMobile($MB->getSumm());

            if (isset($item['AwhData']) && !is_null($item['AwhData'])) {
                $PayRoll->setAwhdata(unserialize($item['AwhData']));
            } else {
                $PayRoll->setAwhdata($AWH->getAwhByMember($item['date'], $item['type_of_payment'], $item['member_id']));
            }

            if ($item['type_of_payment'] == 1 || $item['type_of_payment'] == 3) {
                /*
                    * Таб Административный объект
                    */
                $AB = new AdministrativeBlock();
                /*
                 * Тип выплат премия
                 */
                $AB->setTypeOfPayment($item['type_of_payment']);
                $AB->fetch($item['member_id'], $item['date']);
                $PayRoll->setAdministrativeData(["Promotions" => $AB->getPromotions(), "AdministrativePrize" => $AB->getAdministrativePrize(), "Training" => $AB->getTraining(), "PrizeAnotherDepartment" => $AB->getPrizeAnotherDepartment(), "Defect" => $AB->getDefect(), "Sunday" => $AB->getSunday(), "NotLessThan" => $AB->getNotLessThan(), "Overtime" => $AB->getOvertime()]);
                unset($AB);

                $CR = new Correction();
                $CR->setTypeOfPayment($item['type_of_payment']);
                $CR->fetch($item['member_id'], $item['date']);
                $PayRoll->setCorrectionData(["BonusAdjustment" => $CR->getBonusAdjustment(), "Calculation" => $CR->getCalculation(), "YearBonus" => $CR->getYearBonus()]);
                unset($CR);

                $RN = new Retention();
                $RN->setTypeOfPayment($item['type_of_payment']);
                $RN->fetch($item['member_id'], $item['date']);
                $PayRoll->setRetentionData(["LateWork" => $RN->getLateWork(), "Internet" => $RN->getInternet(), "CachAccounting" => $RN->getCachAccounting(), "Receivables" => $RN->getReceivables(), "Credit" => $RN->getCredit(), "Other" => $RN->getOther()]);
                unset($RN);

            }

            $PayRollArray[] = $PayRoll; // Добавляю в массив объектов
        }

    }

    usort($PayRollArray, function ($a, $b) {
        if ($a->getFio() > $b->getFio()) {
            return 1;
        }
        if ($a->getFio() < $b->getFio()) {
            return -1;
        }

        if ($a->getFio() == $b->getFio()) {
            if (strtotime("01.".$a->getDate()) == strtotime("01.".$b->getDate())) {
                return ($a->order>$b->order) ? 1 : -1;
            };
            return (strtotime("01.".$a->getDate()) > strtotime("01.".$b->getDate())) ? 1 : -1;
        }
    });
}
$menu_open = 3;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ведомость плановых выплат по сотруднику</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- daterange picker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-daterangepicker/daterangepicker.css">

    <!-- Select2 -->
    <link rel="stylesheet" href="../../bower_components/select2/dist/css/select2.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse" style="overflow-x: auto !important; ">
<div class="wrapper" style="overflow-x: unset !important; overflow-y: unset !important;">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">

            <div class="box box-info box-widget box-solid">
                <form class="form-horizontal" method="post">

                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Тип выплаты: </label>
                            <div class="col-sm-8">
                                <select class="form-control" name="type_of_payment">
                                    <option <?php if ((isset($_POST['type_of_payment'])) && ($_POST['type_of_payment'] == 0)) {
                                        echo "selected";
                                    } ?> value="0">Все выплаты
                                    </option>
                                    <option <?php if ((isset($_POST['type_of_payment'])) && ($_POST['type_of_payment'] == 1)) {
                                        echo "selected";
                                    } ?> value="1">Премия
                                    </option>
                                    <option <?php if ((isset($_POST['type_of_payment'])) && ($_POST['type_of_payment'] == 2)) {
                                        echo "selected";
                                    } ?> value="2">Аванс
                                    </option>
                                    <option <?php if ((isset($_POST['type_of_payment'])) && ($_POST['type_of_payment'] == 3)) {
                                        echo "selected";
                                    } ?> value="3">Аванс2
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Диапазон дат:</label>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker" name="date" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                           value="<?php echo $dates[0]; ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker_2" name="date_2" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                           value="<?php echo $dates[1]; ?>" autocomplete="off">
                                </div>
                            </div>

<!--                            <label class="col-sm-2 control-label">Показывать уволенных:</label>-->
<!--                            <div class="col-sm-2">-->
<!--                                <div class="checkbox">-->
<!--                                    <label>-->
<!--                                        <input name="ShowLeaving" type="checkbox" --><?php //if (isset($ShowLeaving) && ($ShowLeaving == 1)) echo "checked"; ?><!-- >-->
<!--                                    </label>-->
<!--                                </div>-->
<!--                            </div>-->

                                <div class="col-sm-2">
                                    <div class="checkbox">
                                        <label name="ShowLeaving">
                                            <input name="ShowLeaving" type="checkbox" <?php if (isset($ShowLeaving) && ($ShowLeaving == 1)) echo "checked"; ?> >
                                            <b>Показывать уволенных</b>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="checkbox">
                                        <label name="ShowExtendedLabel">
                                            <input name="ShowExtended" type="checkbox" <?php if ($ShowExtended == 1) echo "checked"; ?> >
                                            <b>Расширенные данные</b>
                                        </label>
                                    </div>
                                </div>
                            <!-- /.input group -->
                        </div>

                        <div class="box box-default box-solid" style="border-radius: 0px; box-shadow: unset;">
                            <div class="box-header" style="background-color: #f2f2f2 !important; height: 40px; padding-top: 3px; padding-bottom: 0px;">
                                <h5 data-widget="collapse">Фильтры</h5>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Выдающий директор: </label>
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-check"></i>
                                            </div>
                                            <select class="form-control select2" multiple="multiple" data-placeholder="Выбор отделов" id="DirectorID" style="width: 100%;border-radius: unset;color:black;" name="DirectorID[]">
                                                <?php
                                                foreach ($DirectorList as $d) {
                                                    if (isset($_POST['DirectorID']) && in_array($d["director_id"], $_POST['DirectorID'])) {
                                                        echo "<option value='" . $d["director_id"] . "' selected>" . $d["director_name"] . "</option>";
                                                    } else {
                                                        echo "<option value='" . $d["director_id"] . "' >" . $d["director_name"] . "</option>";
                                                    }
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Направление: </label>
                                    <div class="col-sm-7">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-check"></i>
                                            </div>
                                            <select class="form-control select2" multiple="multiple" data-placeholder="Выбор отделов" id="DirectionId" style="width: 100%;border-radius: unset;color:black;" name="DirectionId[]">
                                                <?php
                                                foreach ($Directions as $d) {


                                                    if (isset($_POST['DirectionId']) && in_array($d->getId(), $_POST['DirectionId'])) {
                                                        echo "<option value='" . $d->getId() . "' selected>" . $d->getName() . "</option>";
                                                    } else {
                                                        echo "<option value='" . $d->getId() . "' >" . $d->getName() . "</option>";
                                                    }
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="checkbox">
                                            <label>
                                                <input name="DirectionNot" type="checkbox" <?php if (isset($DirectionNot) && ($DirectionNot == 1)) echo "checked"; ?> > Кроме
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Отдел: </label>
                                    <div class="col-sm-7">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-check"></i>
                                            </div>
                                            <select class="form-control select2" multiple="multiple" data-placeholder="Выбор отделов" style="width: 100%;border-radius: unset;color:black;" name="DepartmentId[]">
                                                <?php
                                                foreach ($Departments as $d) {


                                                    if (isset($_POST['DepartmentId']) && in_array($d->getId(), $_POST['DepartmentId'])) {
                                                        echo "<option value='" . $d->getId() . "' selected>" . $d->getName() . "</option>";
                                                    } else {
                                                        echo "<option value='" . $d->getId() . "' >" . $d->getName() . "</option>";
                                                    }
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="checkbox">
                                            <label>
                                                <input name="DepartmentNot" type="checkbox" <?php if (isset($DepartmentNot) && ($DepartmentNot == 1)) echo "checked"; ?> > Кроме
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Сотрудник: </label>
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-check"></i>
                                            </div>
                                            <select class="form-control select2" multiple="multiple" data-placeholder="Выбор сотрудников" style="width: 100%;border-radius: unset;color:black;" name="MemberId[]">
                                                <?php
                                                foreach ($MemberList as $m) {

                                                    if ($m->getDismissalDate() != "01.01.9999") {
                                                        $DismissalData = " (Уволен: " . $m->getDismissalDate() . ") ";
                                                    } else {
                                                        $DismissalData = "";
                                                    }

                                                    if (isset($_POST['MemberId']) && in_array($m->getId(), $_POST['MemberId'])) {
                                                        echo "<option value='" . $m->getId() . "' selected>" . $m->getLastName() . " " . $m->getName() . " " . $m->getMiddle() . (!empty($m->getMaidenName())?" (".$m->getMaidenName().")":"") . " [" . $m->getDepartment()['name'] . "] " . $DismissalData . "</option>";
                                                    } else {
                                                        echo "<option value='" . $m->getId() . "' >" . $m->getLastName() . " " . $m->getName() . " " . $m->getMiddle() . (!empty($m->getMaidenName())?" (".$m->getMaidenName().")":"") . " [" . $m->getDepartment()['name'] . "] " . $DismissalData . "</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Группировка по: </label>
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-check"></i>
                                            </div>
                                            <select class="form-control" data-placeholder="Без группировки" style="width: 100%;border-radius: unset;color:black;" name="groupby">
                                                <option value="0" <?php if (isset($_POST['groupby']) && ($_POST['groupby'] == 1)) echo "selected"; ?> >Без группировки</option>
                                                <option value="1" <?php if (isset($_POST['groupby']) && ($_POST['groupby'] == 1)) echo "selected"; ?> >Сотруднику</option>
                                                <option value="2" <?php if (isset($_POST['groupby']) && ($_POST['groupby'] == 2)) echo "selected"; ?> >Отделу</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="box-footer text-center">
                                <div class="col-sm-12"><input type="submit" class="btn btn-flat btn-primary" name="PushButton_1" value="Получить данные"></div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>


            <?php
            if (isset($PayRollArray) && count($PayRollArray) > 0) {
                ?>

                <div class="box box-solid box-pane box-widget" style="width: fit-content">
                    <div class="box-body">
                        <table class="table table-striped table-hover" id="info-table" style="overflow-y: auto">
                            <thead>
                            <tr>
                                <th>ФИО</th>
                                <th>Направление</th>
                                <th>Отдел</th>
                                <th>Дата</th>
                                <th>Тип выплаты</th>

                                <th>Оклад</th>
                                <th>Выплаченный аванс</th>

                                <th style='background-color: #627aa7'>Отсутствия (дни)</th>
                                <th style='background-color: #627aa7'>Отпуск (дни)</th>
                                <th style='background-color: #627aa7'>Болезнь (дни)</th>

                                <th>Отсутствия</th>
                                <th>НДФЛ</th>
                                <th>ИЛ</th>

                                <th>Коммерческая <br>премия(руб)</th>
                                <th>KPI</th>
                                <th>Административная <br>премия(руб)</th>

                                <?php
                                if ($ShowExtended == 1) {
                                    echo "<th style='background-color: #627aa7'>Акции</th>";
                                    echo "<th style='background-color: #627aa7'>Административная премия</th>";
                                    echo "<th style='background-color: #627aa7'>Обучение</th>";
                                    echo "<th style='background-color: #627aa7'>Премия другого отдела</th>";
                                    echo "<th style='background-color: #627aa7'>Неликвиды и брак</th>";
                                    echo "<th style='background-color: #627aa7'>Воскресения</th>";
                                    echo "<th style='background-color: #627aa7'>Выплаты не менее</th>";

                                    echo "<th style='background-color: #627aa7'>Переработки</th>";

                                }
                                ?>

                                <th>Мобильная связь</th>
                                <th>Компенсация ГСМ</th>
                                <th>Удержания(руб)</th>

                                <?php
                                if ($ShowExtended == 1) {
                                    echo "<th style='background-color: #627aa7'>За опоздания</th>";
                                    echo "<th style='background-color: #627aa7'>За интернет</th>";
                                    echo "<th style='background-color: #627aa7'>Кассовый учет</th>";
                                    echo "<th style='background-color: #627aa7'>Дебиторская задолженность</th>";
                                    echo "<th style='background-color: #627aa7'>Кредит</th>";
                                    echo "<th style='background-color: #627aa7'>Другое</th>";
                                }
                                ?>

                                <th>Корректировки(руб)</th>
                                <?php
                                if ($ShowExtended == 1) {
                                    echo "<th style='background-color: #627aa7'>Корректировка бонуса</th>";
                                    echo "<th style='background-color: #627aa7'>Расчет</th>";
                                    echo "<th style='background-color: #627aa7'>Годовой бонус</th>";
                                }
                                ?>

                                <th>Отчисления в ГБ(руб)</th>

                                <th>Итого(руб)</th>
                                <?php
                                if ($ShowExtended == 1) {
                                    echo "<th style='background-color: #627aa7'>Сумма за месяц</th>";
                                }
                                ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            // Объявляю переменные для суммирвания колонок
                            $BankSumm = 0; // Итого сумма в банк

                            $ItogoSumm = 0; // Итого сумма
                            $AvansSumm = 0; // Сумма аванса
                            $OtsutstviyaSumm = 0; // Сумма за отсутсвия

                            $AbsencesSumm = 0; // Отсутствия дни
                            $HolidaySumm = 0; // Сумма за отпус
                            $DiseaseSumm = 0; // Сумма за болезнь

                            $NDFLSumm = 0; // Сумма НДФЛ
                            $ILSumm = 0; // Сумма ИЛ
                            $CommersSumm = 0; // Сумма коммерческой премии
                            $KPISumm = 0; // Сумма по KPI
                            $AdmSumm = 0; // Сумма административной премии
                            $YderzSumm = 0;  // Сумма удержаний
                            $CorrectSumm = 0; // Сумма коррекции
                            $MobileSum = 0;
                            $GSMSum = 0;

                            $YBSumm = 0; // Сумма ГБ
                            $SumMounth = 0; // Cумма выплат за месяц

                            if ($ShowExtended == 1) {
                                $SummAkcii = 0;
                                $SummAdmPremia = 0;
                                $SummObuchenie = 0;
                                $SummPrDrOtd = 0;
                                $SummBrak = 0;
                                $SummVoskr = 0;
                                $SummViplaty = 0;
                                $SummPererab = 0;
                                $SummOpozdaniya = 0;
                                $SummInternet = 0;
                                $SummKassoviyUchet = 0;
                                $SummDebetorka = 0;
                                $SummKredit = 0;
                                $SummDrugoe = 0;
                                $SummKorrekBonus = 0;
                                $SummRaschet = 0;
                                $SummGodovoyBonus = 0;
                            }

                            $Name = NULL;
                            $tmp_date = NULL;
                            foreach ($PayRollArray as $item) {
                                if (!isset($_POST['groupby']) || $_POST['groupby'] == 1) {
                                    $Next_Name = $item->getFio();
                                } else if ($_POST['groupby'] == 2) {
                                    $Next_Name = $item->getDepartment()['name'];
                                }
                                if ($Name != NULL && $Name != $Next_Name) {
                                    ?>
                                    <tr>
                                        <!--  Вывожу все просуммированное выше   -->
                                        <th style="background-color: #fbfbfb; font-weight: bold; font-size: 12px; border: 0px" colspan="6">Итого:</th>
                                        <td style='background-color: #fbfbfb; font-weight: bold; text-align: right;'><?php echo number_format($AvansSumm, 2, ',', ' '); ?></td>

                                        <td style='background-color: #fbfbfb; font-weight: bold;' colspan='3'></td>
                                        <td style='background-color: #fbfbfb; font-weight: bold; text-align: right;'><?php echo number_format($OtsutstviyaSumm, 2, ',', ' '); ?></td>

                                        <td style='background-color: #fbfbfb; font-weight: bold; text-align: right;'><?php echo number_format($NDFLSumm, 2, ',', ' '); ?></td>
                                        <td style='background-color: #fbfbfb; font-weight: bold; text-align: right;'><?php echo number_format($ILSumm, 2, ',', ' '); ?></td>
                                        <td style='background-color: #fbfbfb; font-weight: bold; text-align: right;'><?php echo number_format($CommersSumm, 2, ',', ' '); ?></td>
                                        <td style='background-color: #fbfbfb; font-weight: bold; text-align: right;'><?php echo number_format($KPISumm, 2, ',', ' '); ?></td>
                                        <td style='background-color: #fbfbfb; font-weight: bold; text-align: right;'><?php echo number_format($AdmSumm, 2, ',', ' '); ?></td>
                                        <td></td>
                                        <td></td>
                                        <!--                                    <td colspan='10'></td>-->
                                        <td style='background-color: #fbfbfb; font-weight: bold; text-align: right;'><?php echo number_format($YderzSumm, 2, ',', ' '); ?></td>
                                        <!--                                    <td colspan='6'></td>-->
                                        <td style='background-color: #fbfbfb; font-weight: bold; text-align: right;'><?php echo number_format($CorrectSumm, 2, ',', ' '); ?></td>
                                        <td style='background-color: #fbfbfb; font-weight: bold; text-align: right;'><?php echo number_format($YBSumm, 2, ',', ' '); ?></td>
                                        <!--                                    <td colspan='3'></td>-->
                                        <td style='background-color: #fbfbfb; font-weight: bold; text-align: right;'><?php echo number_format($ItogoSumm, 2, ',', ' '); ?></td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: #fff;">&nbsp;</td>
                                    </tr>
                                    <?php
                                    $BankSumm = 0; // Итого сумма в банк
                                    $ItogoSumm = 0; // Итого сумма
                                    $AvansSumm = 0; // Сумма аванса
                                    $OtsutstviyaSumm = 0; // Сумма за отсутсвия
                                    $NDFLSumm = 0; // Сумма НДФЛ
                                    $ILSumm = 0; // Сумма ИЛ
                                    $CommersSumm = 0; // Сумма коммерческой премии
                                    $KPISumm = 0; // Сумма по KPI
                                    $AdmSumm = 0; // Сумма административной премии
                                    $MobileSum = 0;
                                    $GSMSum = 0;
                                    $YderzSumm = 0;  // Сумма удержаний
                                    $CorrectSumm = 0; // Сумма коррекции
                                    $YBSumm = 0;
                                    $AbsencesSumm = 0; // Отсутствия дни
                                    $HolidaySumm = 0; // Сумма за отпус
                                    $DiseaseSumm = 0; // Сумма за болезнь
                                }
                                if (isset($Next_Name)) {
                                    $Name = $Next_Name;
                                }

                                if ($item->getSumm() > 0) {
                                    $BankSumm += $item->getSumm(); // Суммируем Итого Банк
                                };

                                $ItogoSumm += $item->getSumm(); // Суммируем Итого
                                $OtsutstviyaSumm += $item->getAbsences(); // Суммируем отсутствия
                                $AbsencesSumm += $item->getAbsencesDays(); // Отсутствия дни
                                $HolidaySumm += $item->getHoliday(); // Сумма за отпус
                                $DiseaseSumm += $item->getDisease(); // Сумма за болезнь
                                $NDFLSumm += $item->getNDFL(); // Суммируем НДФЛ
                                $ILSumm += $item->getRO(); // Суммируем ИЛ
                                $CommersSumm += $item->getCommercial(); // Суммируем коммерческую премию
                                $KPISumm += $item->getKPI();
                                $AdmSumm += $item->getAdministrative(); // Суммируем административную премию
                                $MobileSum += $item->getMobile();
                                $GSMSum += $item->getTransport();
                                $YderzSumm += $item->getHold(); // Суммирую удержания
                                $CorrectSumm += $item->getCorrecting(); // Суммирую корректировки
                                if ($item->getTypeOfPayment() == 1) {
                                    $AvansSumm += $item->getAdvancePayment();
                                };

                                $YBSumm += $item->getYearBonusPay();

                                if ($ShowExtended == 1) {
                                    $SummAkcii += $item->getAdministrativeData()["Promotions"];
                                    $SummAdmPremia += $item->getAdministrativeData()["AdministrativePrize"];
                                    $SummObuchenie += $item->getAdministrativeData()["Training"];
                                    $SummPrDrOtd += $item->getAdministrativeData()["PrizeAnotherDepartment"];
                                    $SummBrak += $item->getAdministrativeData()["Defect"];
                                    $SummVoskr += $item->getAdministrativeData()["Sunday"];
                                    $SummViplaty += $item->getAdministrativeData()["NotLessThan"];

                                    $SummPererab += $item->getAdministrativeData()["Overtime"];

                                    $SummOpozdaniya += $item->getRetentionData()["LateWork"];
                                    $SummInternet += $item->getRetentionData()["Internet"];
                                    $SummKassoviyUchet += $item->getRetentionData()["CachAccounting"];
                                    $SummDebetorka += $item->getRetentionData()["Receivables"];
                                    $SummKredit += $item->getRetentionData()["Credit"];
                                    $SummDrugoe += $item->getRetentionData()["Other"];

                                    $SummKorrekBonus += $item->getCorrectionData()["BonusAdjustment"];
                                    $SummRaschet += $item->getCorrectionData()["Calculation"];
                                    $SummGodovoyBonus += $item->getCorrectionData()["YearBonus"];

                                    if ($tmp_date != $item->getDate()) {
                                        $tmp_date = $item->getDate();
                                        $SumMounth = $item->getSumm();
                                    } else {
                                        $SumMounth += $item->getSumm();
                                    }
                                }


                                // Выдерение цветом. Если сумма меньше 0, то помечаю желтым
                                switch ($item->getTypeOfPayment()) {
                                    case 1:
                                        echo "<tr class='bg-warning'>";
                                        break;
                                    case 4:
                                        echo "<tr class='bg-danger'>";
                                        break;
                                    default:
                                        echo "<tr>";
                                        break;
                                }

                                switch ($item->getTypeOfPayment()) { // Вывожу ФИО с ссылкой на персональный расчет
                                    case 1:
                                        echo "<th nowrap>";
                                        echo "<a  href='premium.php?id=" . $item->getId() . "&date=" . $item->getDate() . "'>" . $item->getFio() . "</a>";
                                        echo "</th>";
                                        break;
                                    case 2:
                                        echo "<th nowrap><a href='salary.php?id=" . $item->getId() . "&date=" . $item->getDate() . "'>" . $item->getFio() . "</a>";
                                        echo "</th>";
                                        break;
                                    case 3:
                                        echo "<th nowrap>";
                                        echo "<a href='salary2.php?id=" . $item->getId() . "&date=" . $item->getDate() . "'>" . $item->getFio() . "</a>";
                                        echo "</th>";
                                        break;
                                    case 4:
                                        echo "<th nowrap>";
                                        echo "<a href='premium.php?id=" . $item->getId() . "&date=" . $item->getDate() . "'>" . $item->getFio() . "</a>";
                                        echo "</th>";
                                        break;
                                    case 5:
                                        echo "<th nowrap>";
                                        echo "<span>" . $item->getFio() . "</span>";
                                        echo "</th>";
                                        break;
                                }

                                echo "<td nowrap style='text-align: left'>" . $item->getDirection()['name'] . "</td>"; // Вывожу Направление
                                echo "<td nowrap style='text-align: left'>" . $item->getDepartment()['name'] . "</td>"; // Вывожу отдел

                                echo "<th style='left: 215px;'>" . $item->getDate() . ($item->getFlyCalculation()?'[Расчет на лету]':'') . "</th>";
                                switch ($item->getTypeOfPayment()) {
                                    case 1:
                                        echo "<th style='left: 273px;'>Премия</th>";
                                        break;
                                    case 2:
                                        echo "<th style='left: 273px;'>Аванс</th>";
                                        break;
                                    case 3:
                                        echo "<th style='left: 273px;'>Аванс2</th>";
                                        break;
                                    case 4:
                                        echo "<th style='left: 273px;'>Расчет</th>";
                                        break;
                                    case 5:
                                        echo "<th style='left: 273px;'>Внеплановая выплата</th>";
                                        break;
                                }


                                // Вывожу оклад
                                echo "<td style='text-align: right;'>" . number_format($item->getSalary(), 2, ',', ' ') . "</td>";
                                // Вывожу выплаченный аванс//аванс
                                if ($item->getTypeOfPayment() == 1 || $item->getTypeOfPayment() == 3 || $item->getTypeOfPayment() == 4) {
                                    echo "<td style='text-align: right;'>" . number_format($item->getAdvancePayment(), 2, ',', ' ') . "</td>";
                                } else {
                                    echo "<td style='text-align: right;'></td>";
                                }

                                echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAbsencesDays(), 2, ',', ' ') . "</td>";
                                echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getHoliday(), 2, ',', ' ') . "</td>";
                                echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getDisease(), 2, ',', ' ') . "</td>";

                                echo "<td style='text-align: right;'>" . number_format($item->getAbsences(), 2, ',', ' ') . "</td>";

                                if ($item->getTypeOfPayment() == 1 || $item->getTypeOfPayment() == 3 || $item->getTypeOfPayment() == 4) {
                                    // Вывожу НДФЛ
                                    echo "<td style='text-align: right;'>" . number_format($item->getNDFL(), 2, ',', ' ') . "</td>";
                                    // Вывожу ИЛ
                                    echo "<td style='text-align: right;'>" . number_format($item->getRO(), 2, ',', ' ') . "</td>";
                                    // Вывожу коммерческую премию
                                    echo "<td style='text-align: right;'>" . number_format($item->getCommercial(), 2, ',', ' ') . "</td>";
                                    // Вывожу KPI
                                    echo "<td style='text-align: right;'>" . number_format($item->getKPI(), 2, ',', ' ') . "</td>";
                                    // Вывожу административную премию
                                    echo "<td style='text-align: right;'>" . number_format($item->getAdministrative(), 2, ',', ' ') . "</td>";

                                    if ($ShowExtended == 1) {
                                        if (isset($item->getAdministrativeData()["Promotions"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["Promotions"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getAdministrativeData()["AdministrativePrize"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["AdministrativePrize"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getAdministrativeData()["Training"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["Training"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getAdministrativeData()["PrizeAnotherDepartment"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["PrizeAnotherDepartment"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getAdministrativeData()["Defect"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["Defect"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getAdministrativeData()["Sunday"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["Sunday"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getAdministrativeData()["NotLessThan"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["NotLessThan"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getAdministrativeData()["Overtime"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["Overtime"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";
                                    }

                                    echo "<td style='text-align: right;'>" . number_format($item->getMobile(), 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($item->getTransport(), 2, ',', ' ') . "</td>";
                                    // Вывожу удержания
                                    echo "<td style='text-align: right;'>" . number_format($item->getHold(), 2, ',', ' ') . "</td>";
                                    if ($ShowExtended == 1) {
                                        if (isset($item->getRetentionData()["LateWork"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getRetentionData()["LateWork"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getRetentionData()["Internet"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getRetentionData()["Internet"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getRetentionData()["CachAccounting"]))
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getRetentionData()["CachAccounting"], 2, ',', ' ') . "</td>";
                                        else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getRetentionData()["Receivables"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getRetentionData()["Receivables"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getRetentionData()["Credit"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getRetentionData()["Credit"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getRetentionData()["Other"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getRetentionData()["Other"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";
                                    }
                                    // ВЫвожу корректировки
                                    echo "<td style='text-align: right;'>" . number_format($item->getCorrecting(), 2, ',', ' ') . "</td>";
                                    if ($ShowExtended == 1) {
                                        if (isset($item->getCorrectionData()["BonusAdjustment"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getCorrectionData()["BonusAdjustment"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";
                                        if (isset($item->getCorrectionData()["Calculation"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getCorrectionData()["Calculation"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";
                                        if (isset($item->getCorrectionData()["YearBonus"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getCorrectionData()["YearBonus"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";
                                    }

                                    echo "<td style='text-align: right;'>" . number_format($item->getYearBonusPay(), 2, ',', ' ') . "</td>";
                                } else {
                                    if ($ShowExtended == 1) {
                                        echo "<td colspan='27'></td>";
                                    } else {
                                        echo "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
                                    }
                                }
                                // Вывожу сумму
                                if ($item->getSumm() > 0) {
                                    echo "<td style='background-color: #eff3fd; text-align: right;'>" . number_format($item->getSumm(), 2, ',', ' ') . "</td>";
                                } else {
                                    echo "<td style='background-color: #9f191f; color: #fff; text-align: right;'>" . number_format($item->getSumm(), 2, ',', ' ') . "</td>";
                                }
                                if ($ShowExtended == 1) {
                                    if ($item->getTypeOfPayment()==1) {
                                        echo "<td style='background-color: #6a6a6a; color: #fff; text-align: right;'>" . number_format($SumMounth, 2, ',', ' ') . "</td>";
                                    } else {
                                        echo "<td style='background-color: #6a6a6a; color: #fff; text-align: right;'></td>";
                                    }
                                }
                                echo "</tr>";
                            }

                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <!--  Вывожу все просуммированное выше   -->
                                <th style="font-weight: bold;font-size: 12px" colspan="6">Итого:</th>
                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($AvansSumm, 2, ',', ' '); ?></td>

                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($AbsencesSumm, 2, ',', ' '); ?></td>
                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($HolidaySumm, 2, ',', ' '); ?></td>
                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($DiseaseSumm, 2, ',', ' '); ?></td>
                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($OtsutstviyaSumm, 2, ',', ' '); ?></td>

                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($NDFLSumm, 2, ',', ' '); ?></td>
                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($ILSumm, 2, ',', ' '); ?></td>
                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($CommersSumm, 2, ',', ' '); ?></td>
                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($KPISumm, 2, ',', ' '); ?></td>
                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($AdmSumm, 2, ',', ' '); ?></td>

                                <?php
                                if ($ShowExtended == 1) {
                                    echo "<td style='text-align: right;'>" . number_format($SummAkcii, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummAdmPremia, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummObuchenie, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummPrDrOtd, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummBrak, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummVoskr, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummViplaty, 2, ',', ' ') . "</td>";

                                    echo "<td style='text-align: right;'>" . number_format($SummPererab, 2, ',', ' ') . "</td>";

                                }
                                ?>

                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($MobileSum, 2, ',', ' '); ?></td>
                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($GSMSum, 2, ',', ' '); ?></td>
                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($YderzSumm, 2, ',', ' '); ?></td>
                                <?php
                                if ($ShowExtended == 1) {
                                    echo "<td style='text-align: right;'>" . number_format($SummOpozdaniya, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummInternet, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummKassoviyUchet, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummDebetorka, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummKredit, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummDrugoe, 2, ',', ' ') . "</td>";
                                }
                                ?>
                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($CorrectSumm, 2, ',', ' '); ?></td>
                                <?php
                                if ($ShowExtended == 1) {
                                    echo "<td style='text-align: right;'>" . number_format($SummKorrekBonus, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummRaschet, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummGodovoyBonus, 2, ',', ' ') . "</td>";
                                }
                                ?>
                                <td style='background-color: #fbfbfb; font-weight: bold; text-align: right;'><?php echo number_format($YBSumm, 2, ',', ' '); ?></td>
                                <td style='font-weight: bold;text-align: right;'><?php echo number_format($ItogoSumm, 2, ',', ' '); ?></td>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
                <?php
            }
            ?>
        </section>
    </div>
    <?php
    require_once 'footer.php';
    ?>
</div>

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- DataTables -->
<!--<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>-->
<!--<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>-->

<!--<script src="../../bower_components/FixedHeader-3.1.4/js/dataTables.fixedHeader.js"></script>-->
<!-- date-range-picker -->
<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Select2 -->
<script src="../../bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
    $(function () {
        $('.select2').select2()

        $(function () {
            $('#datepicker').datepicker({
                language: 'ru',
                autoclose: true,
                viewMode: "months",
                minViewMode: "months",
                format: 'yyyy-mm'
            })
        });

        $(function () {
            $('#datepicker_2').datepicker({
                language: 'ru',
                autoclose: true,
                viewMode: "months",
                minViewMode: "months",
                format: 'yyyy-mm'
            })
        });

        // $('#reservation').daterangepicker.setStartDate("2019-06");
    });
</script>
<script>
    $(document).ready(function () {
        var screenWidth = document.documentElement.clientWidth;
        var new_width = screenWidth - 230;
        $("#info-table").width(200);
        $("#info-table").css('max-width', '200');
        // alert(new_width);
    });
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>

