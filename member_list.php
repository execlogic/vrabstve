<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.07.18
 * Time: 14:33
 */

// Время работы скрипта
$start = microtime(true);

/*
 * Подключаю необходимые библиотеки
 */
require_once 'app/DirectionInterface.php';
require_once 'app/DepartmentInterface.php';
require_once 'app/PositionInterface.php';
require_once 'app/NationInterface.php';
require_once 'app/MemberInterface.php';
require_once 'app/Notify.php';

require_once 'app/PhotoFile.php';
require_once 'app/FileCSV.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once "app/ErrorInterface.php";


/*
 * Если глобальной переменной _SESSION нету, то пробую запустить сессии
 */
if (!isset($_SESSION)) {
    session_start();
}

/*
 * Если нету элемента массива UserObj, значит пользователь не аутентифицирован
 */
if (!isset($_SESSION['UserObj'])) {
    header("Location: index.php");
    exit();
}

/*
 * Определяю переменную $User и массива сессии
 */
$User = $_SESSION['UserObj'];

/*
 * Проверяю прошел ли он аутетификаци.
 * Если не аутентифицирован пользователь, то идем на страницу логина и пароля
 */
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}


/*
 * Подключаю объект ролей и доступов
 */
$RI = new RoleInterface();

/*
 * Получаю роли и доступы аутентифицированного пользователя
 */
$Roles = $RI->getRoles($User->getMemberId());

/*
 * Выствляю значение доступа по умолчанию в 0;
 * Доступ на добавление пользователей
 */
$PermissionAddUser = 0;

/*
 * Ищу в массиве ролей доступ на добавление пользователей
 */
$current = array_filter($Roles, function ($Role) {
    return ($Role->getAddUser() == 1);
});

/*
 * Если существует, то выставляю доступ в 1
 */
if ($current) {
    $PermissionAddUser = 1;
}
/****************************************************************************/

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

/*
 * Подключаю объект пользовательского интерфейса
 */
$Members = new MemberInterface();

/*
 * Подключаю объект Интрефейса Ошибок
 */
$Errors = new ErrorInterface();

/*
 * Объявляю массив $Debug
 */
$Debug = array();

/*
 * Если была нажата кнопка submin_add, то пытаюсь добавить пользователя
 */
if (isset($_POST["submit_add"])) {
    /*
     *  Проверяю обязательные поля, они должны присутсвовать и должны быть опреденных типов, так же длинна строки должна быть больше 0
     */
    if (is_numeric($_POST["direction"]) && $_POST["direction"] != 0
        && is_numeric($_POST["department"]) && $_POST["department"] != 0
        && is_numeric($_POST["position"]) && $_POST["position"] != 0
        && is_string($_POST["inputLastName"]) && strlen($_POST["inputLastName"]) > 0
        && is_string($_POST["InputName"]) && strlen($_POST["InputName"]) > 0
        && is_string($_POST["inputMiddle"]) && strlen($_POST["inputMiddle"]) > 0
        && is_numeric($_POST["sex"]) && $_POST["sex"] != -1
        && is_string($_POST["addDate"]) && strlen($_POST["addDate"]) > 0
        && is_numeric($_POST["nation"]) && is_numeric($_POST["addSalary"])
    ) {
        /*
         * Формирую массив для добавления данных
         */
        $adduser = array(trim($_POST["inputLastName"]), trim($_POST["InputName"]), trim($_POST["inputMiddle"]), trim($_POST["sex"]), trim($_POST["birthdate"]), trim($_POST["direction"]), trim($_POST["department"]), trim($_POST["position"]), trim($_POST['nation']), trim($_POST["addDate"]));

        try {
            /*
             * Добавляю сотрудника и получаю в ответ его ID
             */
            $ID = $Members->AddMembers($adduser, $User->getMemberId());
            array_push($Debug, "Добавляю сотрудника " . $adduser[0] . " " . $adduser[1] . ". Получил ID:" . $ID);
        } catch (Exception $e) {
            $Errors->add("Ошибка при добавлении сотрудника. " . $e->getMessage());
        }


        /*
         *  Если мы получили ID и он больше 0
         */
        if (is_numeric($ID) && $ID > 0) {

            /*
             * Добавляю начальную ЗП и Коммент на начальные условия
             */
            if (is_numeric($_POST['addSalary'])) {
                $val = $_POST['addSalary'];
                /*
                 * И пытаюсь добавить личные телефоны в БД
                 */
                try {
                    $Members->ChangeSalary($ID, $val, $User->getMemberId());
                    array_push($Debug, "Добавляю оклад: " . $val . ".");
                } catch (Exception $e) {
                    $Errors->add("Ошибка при добавлении оклада. " . $e->getMessage());
                }
            }

            if (isset($_POST['member_avans_pay'])) {
                $member_avans_pay_status = 0;
            } else {
                $member_avans_pay_status = 1;
            }

            try {
                $Members->changeAvansPay($ID, $member_avans_pay_status, $User->getMemberId());
            } catch (Exception $e) {
                $Errors->add("Таб. финансы. Ошибка изменении чекбокса 'Не выплачивать аванс сотруднику'. " . $e->getMessage());
            }

            /*
             * Коммент к первичным фин. условиям
             */
            if (strlen($_POST['Comment']) > 0) {
                $_POST['Comment'] = strip_tags($_POST['Comment']);
                $_POST['Comment'] = htmlspecialchars($_POST['Comment']);
                try {
                    $Members->AddProbationComment($ID, $_POST['Comment']);
                    array_push($Debug, "Добавляю коммент на фин условия: " . $_POST['Comment'] . ".");
                } catch (Exception $e) {
                    $Errors->add("Ошибка при добавлении финансовых условий. " . $e->getMessage());
                }
            }

            /*
             * То добавляем личные телефоны
             * Перебираю полученный массив
             */
            if (isset($_POST['personal_phone'])) {
                foreach ($_POST['personal_phone'] as $value) {
                    /*
                     * Удаляю символы которые передаются с маской
                     */
                    $value = str_replace(array(")", "-", "("), "", $value);
                    /*
                     * Проверяю что получилось число
                     */
                    if (is_numeric($value)) {
                        /*
                         * И пытаюсь добавить личные телефоны в БД
                         */
                        try {
                            $Members->AddPersonalPhone($value, $ID, $User->getMemberId());
                            array_push($Debug, "Добавляю личный телефон: " . $value . ".");
                        } catch (Exception $e) {
                            $Errors->add("Ошибка при добавлении личных телефонов. " . $e->getMessage());
                        }
                    }
                }
            }

            /*
             *  Добавляем личные мобильные телефоны
             */
            if (isset($_POST['personal_mobile_phone'])) {
                foreach ($_POST['personal_mobile_phone'] as $value) {
                    /*
                     * Удаляю символы которые передаются с маской
                     */
                    $value = str_replace(array(")", "-", "("), "", $value);
                    /*
                     * Проверяю что получилось число
                     */
                    if (is_numeric($value)) {
                        /*
                         * И пытаюсь добавить личные мобильные телефоны в БД
                         */
                        try {
                            $Members->AddMobilePersonalPhone($value, $ID, $User->getMemberId());
                            array_push($Debug, "Добавляю личный мобильный телефон: " . $value . ".");
                        } catch (Exception $e) {
                            $Errors->add("Ошибка при добавлении личных мобильных телефонов. " . $e->getMessage());
                        }
                    }
                }
            }

            /*
             *  Добавляем аварийные телефоны
             */
            if (isset($_POST['emergency'])) {
                foreach ($_POST['emergency'] as $value) {
                    /*
                     * Удаляю символы которые передаются с маской
                     */
                    $value['phone'] = str_replace(array(")", "-", "("), "", $value['phone']);
                    /*
                     * Проверяю что получилось число
                     */
                    if (is_numeric($value['phone'])) {
                        try {
                            /*
                             *  И пытаюсь добавить аварийные телефоны в БД
                             */
                            $Members->AddEmergancyPhone($value, $ID, $User->getMemberId());
                            array_push($Debug, "Добавляю аварийны телефон: " . $value['phone'] . ".");
                        } catch (Exception $e) {
                            $Errors->add("Ошибка при добавлении аварийных телефонов. " . $e->getMessage());
                        }
                    }
                }
            }

            /*
             *  Добавляем почтовые адреса
             */
            if (isset($_POST['inputEmail'])) {
                foreach ($_POST['inputEmail'] as $value) {
                    /*
                     * Использую валидацию почтового адреса, если она неверная, то игнорирую этот адрес
                     */
                    if (!filter_var(trim($value['email']), FILTER_VALIDATE_EMAIL)) {
                        continue;
                    }

                    /*
                     * Пытаюсь добавить почтовый адрес в БД
                     */
                    try {
                        $Members->AddEMail($value, $ID, $User->getMemberId());
                        array_push($Debug, "Добавляю почтовый адрес: " . $value['email'] . ".");
                    } catch (Exception $e) {
                        $Errors->add("Ошибка при добавлении почтовых адресов. " . $e->getMessage());
                    }
                }
            }

            /*
             *  Добавляем рабочие телефоны сотрудника
             */
            if (isset($_POST['work_phone'])) {
                foreach ($_POST['work_phone'] as $item) {
                    /*
                     * Удаляю символы которые передаются с маской
                     */
                    $item = str_replace(array(")", "-", "("), "", $item);
                    /*
                     * Проверяю что получилось число
                     */
                    if (is_numeric($item)) {
                        try {
                            /*
                            * Добавляю раб. телефоны в БД
                            */
                            $Members->AddWorkPhone($item, $ID, $User->getMemberId());
                            array_push($Debug, "Добавляю рабочий телефон: " . $item . ".");
                        } catch (Exception $e) {
                            $Errors->add("Ошибка при добавлении рабочих телефонов. " . $e->getMessage());
                        }
                    }
                }
            }

            /*
             *  Добавляем рабочие мобильные телефоны сотрудника
             */
            if (isset($_POST['work_phone'])) {
                foreach ($_POST['work_mobile_phone'] as $item) {
                    /*
                     * Удаляю символы которые передаются с маской
                     */
                    $item = str_replace(array(")", "-", "("), "", $item);
                    /*
                     * Проверяю что получилось число
                     */
                    if (is_numeric($item)) {
                        /*
                         * Добавляю раб. мобильные телефоны в БД
                         */
                        try {
                            $Members->AddMobileWorkPhone($item, $ID, $User->getMemberId());
                            array_push($Debug, "Добавляю мобильный рабочий телефон: " . $item . ".");
                        } catch (Exception $e) {
                            $Errors->add("Ошибка при добавлении рабочих мобильных телефонов. " . $e->getMessage());
                        }
                    }
                }
            }

            /*
             * Добавляем внутренний рабочий номер сотруднику
             * Проверяю что это число
             */
            if (isset($_POST['work_local'])) {
                if (is_numeric($_POST['work_local'])) {
                    try {
                        /*
                         * Добавляю в БД
                         */
                        $Members->AddWorkLocal($_POST['work_local'], $ID, $User->getMemberId());
                        array_push($Debug, "Добавляю добавочный номер: " . $_POST['work_local'] . ".");
                    } catch (Exception $e) {
                        $Errors->add("Ошибка при добавлении рабочего внутреннего номера. " . $e->getMessage());
                    }
                }
            }

        }

        /*
         * Добавляю фото если оно есть
         */
        if (isset($ID) && $_FILES['photofile']['name']) {
            try {
                /*
                 * Создаю объект PhotoFile
                 */
                $uploadFile = new PhotoFile();
                /*
                 * Устанавливаю имя файла для картинки с фото
                 */
                $uploadFile->setFormName("photofile");
                /*
                 * Указываю путь до папки
                 */
                $uploadFile->setTargetDir("images/" . $ID . "/");
                /*
                 * Загружаю фото
                 */
                $uploadFile->Upload();
            } catch (Exception $e) {
                $Errors->add("Ошибка при загрузки фотографии. " . $e->getMessage());
            }
        }
    } else {
        $Errors->add("Не заполнены обязательные поля, отмеченные звездочкой");
    }


}

/*
 * Импорт данных из CSV
 */
if (isset($_POST["submit"])) {
    /*
     * Создаю массив $array
     */
    $array = array();
    try {
        /*
         * Создаю объект FileCSV для обработки CSV
         */
        $uploadFile = new FileCSV();
        /*
         * Указываю его имя
         */
        $uploadFile->setFormName("CSVfileToUpload");
        /*
         * Загружаю на сервер
         */
        $uploadFile->Upload();
        /*
         * Разбиваю по разделителю и получаю массив данных
         */
        $array = $uploadFile->ParseCSV(";");

        /*
         * Удаляю файл
         */
        $uploadFile->DeleteFile();
    } catch (Exception $e) {
        $Errors->add($e->getMessage());
    }

    /*
     * Полученный массив перебираю и добавляю в БД
     */
    foreach ($array as $value) {
        try {
            $Members->AddMembersFromCSV($value);
        } catch (Exception $e) {
            $Errors->add($e->getMessage());
        }
    }
}


//if (isset($_POST["MotivationChanger"])) {
//    $Members->fetchMembers(false);
//    $members = $Members->GetMembers();
//    foreach ($members as $m) {
////        echo $m->getLastname()." ".$m->getName()." ".$m->getMotivation()."<BR>";
//        if ($m->getMotivation() == -1) {
//            echo $m->getLastname()." ".$m->getName()." ".$m->getMotivation()."<BR>";
//            try {
//                $Members->ChangeMotivation($m->getId(), 6);
//            }catch (Exception $e) {
//                echo $e->getMessage()."<BR>";
//            }
//        }
//    }
//}

/*
 * Скрыть уволенных пользователей
 * 0 - нет
 * 1 - да
 */
$ShowLeaving = 0;
if (isset($_COOKIE['ShowLeaving'])) {
    $ShowLeaving = $_COOKIE['ShowLeaving'];
}

if (isset($_POST['PushButton_1'])) {
    if (isset($_POST['ShowLeaving'])) {
        if (isset($_COOKIE['ShowLeaving'])) {
            setcookie("ShowLeaving", 1);
        } else {
            setcookie("ShowLeaving", 1, time() + 2000);
        }
        $ShowLeaving = 1;
    } else {
        if (isset($_COOKIE['ShowLeaving'])) {
            setcookie("ShowLeaving", 0);
        } else {
            setcookie("ShowLeaving", 0, time() + 2000);
        }
        $ShowLeaving = 0;
    }
}

/*
 * Получаю список сотрудников с минимальными данным.
 */
if ($ShowLeaving == 1 && count($Roles) != 0) {
    $Members->fetchMembers();
}  else {
    $Members->fetchMembers(false);
}

//if (count($Roles) == 0 || $HideLeaving == 1) {
//    $Members->fetchMembers(false);
//} else {
//    $Members->fetchMembers();
//}



try {
    /*
     * Создаю объект DircetionInterface для получения направлений
     */
    $Directions = new DirectionInterface();
    /*
     * Получаю направления
     */
    $Directions->fetchDirections();
} catch (Exception $e) {
    $Errors->add($e->getMessage());
}

try {
    /*
     * Создаю объект DepartmentInterface для получения отделов
     */
    $Departments = new DepartmentInterface();
    /*
     * Получаю отделы
     */
    $Departments->fetchDepartments();
} catch (Exception $e) {
    $Errors->add($e);
}

try {
    /*
     * Создаю объект PositionInterface для получения должностей
     */
    $Positions = new PositionInterface();
    /*
     * Получаю должности
     */
    $Positions->fetchPositions();
} catch (Exception $e) {
    $Errors->add($e->getMessage());
}

try {
    /*
     * Создаю объект NationInterface для получения национальности
     */
    $Nations = new NationInterface();
    /*
     * Получаю данные из БД
     */
    $Nations->fetchNations();
} catch (Exception $e) {
    $Errors->add($e->getMessage());
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Сотрудники компании</title>


    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">

    <!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">


    <!-- bootstrap datepicker -->
    <!--    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">-->

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <link rel="stylesheet" href="css/dropdown-menu.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/main.css">
    <style>
        .btn-aqua {
            background-color: rgba(0, 192, 239, 0.1);
            border-color: #00acd6;
        }
    </style>


</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">

<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>

    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Сотрудники компании</h3>

<!--                    <div class="box-tools pull-right">-->
<!--                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"-->
<!--                                title="Collapse"><i class="fa fa-minus"></i></button>-->
<!--                    </div>-->
                </div>
                <div class="box-body">
                    <form method="post" class="form">
                        <div class="box box-solid box-primary collapsed-box" >
                            <div class="box-header" style="background-color: unset;padding-bottom: 0;padding-top: 0;">
                                <h5 style="color: black" data-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">Настройки</h5>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                            title="Collapse"  style="color: #3c8dbc;"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input name="ShowLeaving" type="checkbox" <?php if ($ShowLeaving == 1) echo "checked"; ?> >
                                            Показывать уволенных сотрудников
                                        </label>
                                    </div>
                                </div>
                                <button name="PushButton_1" class="btn btn-sm btn-default">Применить</button>

                            </div>
                            <div class="box-footer">

                            </div>
                        </div>
                    </form>

                    <table id="MembersTable" class="table table-bordered table-hover table-striped">
                        <thead>
                        <th>#</th>
                        <th>Фамилия</th>
                        <th>Имя</th>
                        <th>Отчество</th>
                        <th>Девичья фамилия</th>
                        <th>Пол</th>
                        <th>Дата рождения</th>
                        <th>Национальность</th>
                        <th>Направление</th>
                        <th>Отдел</th>
                        <th>Должность</th>
                        <th>Дата приема</th>
                        <th>Рабочий статус</th>
                        </thead>

                        <tbody>
                        <?php
                        try {
                            foreach ($Members->GetMembers() as $value) {
//                            $value->ShowForTable();
                                echo "<tr>";
                                echo "<td><a class='text-muted' href='profile.php?id=" . $value->getId() . "'>" . $value->getId() . "</a></td>";
                                echo "<td><a style='color: #595959' href='profile.php?id=" . $value->getId() . "'>" . $value->getLastname() . "</a></td>";
                                echo "<td><a style='color: #595959' href='profile.php?id=" . $value->getId() . "'>" . $value->getName() . "</a></td>";
                                echo "<td><a style='color: #595959' href='profile.php?id=" . $value->getId() . "'>" . $value->getMiddle() . "</a></td>";
                                echo "<td><a class='text-muted' href='profile.php?id=" . $value->getId() . "'>" . $value->getMaidenName() . "</a></td>";
                                echo "<td><a class='text-muted' href='profile.php?id=" . $value->getId() . "'>" . $value->SexToText() . "</a></td>";
                                echo "<td><a class='text-muted' href='profile.php?id=" . $value->getId() . "'>" . $value->getBirthday() . "</a></td>";
                                echo "<td><a class='text-muted' href='profile.php?id=" . $value->getId() . "'>" . $value->getNationName() . "</a></td>";
                                echo "<td><a class='text-muted' href='profile.php?id=" . $value->getId() . "'>" . $value->getDirection()['name'] . "</a></td>";
                                echo "<td><a class='text-muted' href='profile.php?id=" . $value->getId() . "'>" . $value->getDepartment()['name'] . "</a></td>";
                                echo "<td><a class='text-muted' href='profile.php?id=" . $value->getId() . "'>" . $value->getPosition()['name'] . "</a></td>";
                                echo "<td><a class='text-muted' href='profile.php?id=" . $value->getId() . "'>" . $value->getEmploymentDate() . "</a></td>";
                                echo "<td>";
                                switch ($value->getWorkstatus()) {
                                    case 1:
                                        echo "<span class=\"label label-success\">Сотрудник работает</span>";
                                        break;
                                    case 2:
                                        echo "<span class=\"label label-warning\" title='" . $value->getWorkstatusNote() . "' >Сотрудник помечен на увольнение</span>";
                                        break;
                                    case 3:
                                        echo "<span class=\"label label-default\" title='" . $value->getWorkstatusNote() . "' >Сотрудник в декрете</span>";
                                        break;
                                    case 4:
                                        echo "<span class=\"label label-danger\" title='" . $value->getReasonLeaving() . "' >Сотрудник уволен [" . $value->getDismissalDate() . "]</span>";
                                        break;
                                    default:
                                        echo "<span class=\"label label-default\">Ошибка статуса</span>";
                                        break;
                                }
                                echo "</td>";
                                echo "</tr>";
                            }
                        } catch (Exception $e) {
                            echo "<tr><td colspan='12'>" . $e->getMessage() . "</td></tr>";
                        }
                        ?>
                        </tbody>

                        <tfoot>
                        <th>#</th>
                        <th>Фамилия</th>
                        <th>Имя</th>
                        <th>Отчество</th>
                        <th>Девичья фамилия</th>
                        <th>Пол</th>
                        <th>Дата рождения</th>
                        <th>Национальность</th>
                        <th>Направление</th>
                        <th>Отдел</th>
                        <th>Должность</th>
                        <th>Дата приема</th>
                        <th>Рабочий статус</th>
                        </tfoot>
                    </table>
                </div>

                <?php
                /*
                 * Если у аутентифицированного пользователя есть права доступа на добавление сотрудника,
                 * то показываю кнопки и создаю модальное окно
                 */
                if ($PermissionAddUser == 1) {
                    ?>
                    <form method="post" class="form-horizontal">
                        <div class="box-footer">
                            <a class="btn btn-flat btn-warning btn-sm" data-toggle="modal" data-target="#modal-default" title="Добавить сотрудника">
                                <i class="fa fa-plus"></i><span style="margin-left: 5px"> Добавить</span>
                            </a>

                            <a class="btn btn-flat btn-primary btn-sm" data-toggle="modal" data-target="#modal-csv" title="Импорт из csv">
                                <i class="fa fa-file text-white text-sm"></i><span class="text-white" style="margin-left: 5px"> Импорт</span>
                            </a>
                        </div>
                    </form>

                    <div class="modal fade" id="modal-default">
                        <div style="width: 90%;" class="modal-dialog">
                            <div style="width: 100%;" class="modal-content">
                                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span></button>
                                        <h4 class="modal-title">Добавить сотрудника</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="nav-tabs-custom">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Сотрудник</a></li>
                                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Личные данные</a></li>
                                                <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Работа</a></li>
                                                <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Финансовые условия</a></li>
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab_1">
                                                    <div class="box box-solid box-info">
                                                        <div class="box-header with-border">
                                                            <p>Регистрационная информация</p>
                                                        </div>
                                                        <div class="box-body">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Дата приема <b class="text-red">*</b></label>
                                                                <div class="col-sm-10">
                                                                    <div class="input-group date">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <input type="text" class="form-control pull-right"
                                                                               id="datepicker2" name="addDate"
                                                                               data-inputmask="'alias': 'dd.mm.yyyy'" data-mask value="<?php echo date("d.m.Y"); ?>">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Фамилия <b class="text-red">*</b></label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="inputLastName" id="inputLastName" placeholder="Фамилия">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Имя <b class="text-red">*</b></label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="InputName" id="inputName" placeholder="Имя">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Отчество <b class="text-red">*</b></label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="inputMiddle" id="inputMiddle" placeholder="Отчество">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Почтовые адреса</label>
                                                                <div class="col-sm-10">
                                                                    <div class="box box-solid box-default">
                                                                        <div class="box-body">
                                                                            <fieldset id="emails_form">
                                                                            </fieldset>
                                                                        </div>

                                                                        <div class="box-footer">
                                                                            <button type="button" class="btn btn-flat btn-sm  btn-flat pull-right  btn-aqua" id="add_email">Добавить
                                                                                почтовый адрес
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.tab-pane -->
                                                <div class="tab-pane" id="tab_2">
                                                    <div class="box box-solid box-info">
                                                        <div class="box-header with-border">
                                                            <p>Личные данные</p>
                                                        </div>
                                                        <div class="box-body">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Пол <b class="text-red">*</b></label>
                                                                <div class="col-sm-10">
                                                                    <select class="form-control" name="sex" id="sex">
                                                                        <option selected value="0">Мужской</option>
                                                                        <option value="1">Женский</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Дата
                                                                    рождения(ДД.ММ.ГГГГ)</label>
                                                                <div class="col-sm-10">
                                                                    <div class="input-group date">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <input type="text" class="form-control pull-right"
                                                                               id="datepicker" name="birthdate"
                                                                               data-inputmask="'alias': 'dd.mm.yyyy'" data-mask>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Национальность <b class="text-red">*</b></label>
                                                                <div class="col-sm-10">
                                                                    <select class="form-control" name="nation" id="nation">
                                                                        <?php
                                                                        foreach ($Nations->GetNations() as $value) {
                                                                            if ($value->getId() == 0) {
                                                                                echo '<option selected value="' . $value->getId() . '">' . $value->GetName() . '</option>';
                                                                            } else {
                                                                                echo '<option value="' . $value->getId() . '">' . $value->GetName() . '</option>';
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Фотография</label>
                                                                <div class="col-sm-10">
                                                                    <input type="file" name="photofile" id="InputFile">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="box box-solid box-info">
                                                        <div class="box-header with-border">
                                                            <p>Личные телефоны</p>
                                                        </div>
                                                        <div class="box-body">

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Телефон</label>
                                                                <div class="col-md-10">
                                                                    <div class="box box-solid box-info">
                                                                        <div class="box-body">
                                                                            <fieldset id="phone_form">
                                                                            </fieldset>
                                                                        </div>

                                                                        <div class="box-footer">
                                                                            <button type="button" class="btn btn-flat btn-sm  btn-flat pull-right btn-aqua" id="add_presonal_phone">
                                                                                Добавить
                                                                                телефон
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Мобильные телефон</label>
                                                                <div class="col-md-10">
                                                                    <div class="box box-solid box-info">
                                                                        <div class="box-body">
                                                                            <fieldset id="phone_mobile_form">
                                                                            </fieldset>
                                                                        </div>

                                                                        <div class="box-footer">
                                                                            <button type="button" class="btn btn-flat  btn-sm btn-flat pull-right  btn-aqua"
                                                                                    id="add_presonal_mobile_phone">
                                                                                Добавить телефон
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Аварийные телефон</label>
                                                                <div class="col-md-10">
                                                                    <div class="box box-solid box-info">
                                                                        <div class="box-body">
                                                                            <fieldset id="emergency_phone_form">
                                                                            </fieldset>
                                                                        </div>

                                                                        <div class="box-footer">
                                                                            <button type="button" class="btn btn-flat btn-sm  btn-flat pull-right  btn-aqua" id="add_emergency_phone">
                                                                                Добавить телефон
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="tab_3">
                                                    <div class="box box-solid box-info">
                                                        <div class="box-header with-border">
                                                            <p>Должность в организации</p>
                                                        </div>
                                                        <div class="box-body">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Направление<b class="text-red">*</b></label>
                                                                <div class="col-sm-10">

                                                                    <select name="direction" class="form-control">
                                                                        <option value="0">Не выбрано</option>
                                                                        <?php
                                                                        try {
                                                                            foreach ($Directions->GetDirections() as $value) {
                                                                                echo "<option value='" . $value->GetID() . "''>" . $value->GetName() . "</option>";
                                                                            }
                                                                        } catch (Exception $e) {
                                                                        }
                                                                        ?>
                                                                    </select>

                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Отдел<b class="text-red">*</b></label>
                                                                <div class="col-sm-10">

                                                                    <select name="department" class="form-control">
                                                                        <option value="0">Не выбрано</option>
                                                                        <?php
                                                                        try {
                                                                            foreach ($Departments->GetDepartments() as $value) {
                                                                                foreach ($value->getDirectionId() as $d) {
                                                                                    echo "<option class='".$d."' value=\"" . $value->GetID() . "\">" . $value->GetName() . "</option>";
                                                                                }
                                                                            }
                                                                        } catch (Exception $e) {
                                                                        }
                                                                        ?>
                                                                    </select>

                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Должность<b class="text-red">*</b></label>
                                                                <div class="col-sm-10">
                                                                    <select name="position" class="form-control">
                                                                        <option value="0">Не выбрано</option>
                                                                        <?php
                                                                        try {
                                                                            foreach ($Positions->GetPositions() as $value) {

                                                                                foreach ($value->getDepartmentID() as $d) {
                                                                                    echo "<option class='" . $d . "' value=\"" . $value->GetID() . "\">" . $value->GetName() . "</option>";
                                                                                }
                                                                            }
                                                                        } catch (Exception $e) {
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="box box-solid box-info">
                                                        <div class="box-header with-border">
                                                            <p>Рабочие телефоны</p>
                                                        </div>
                                                        <div class="box-body">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Телефон</label>
                                                                <div class="col-sm-10">
                                                                    <div class="box box-solid box-info">
                                                                        <div class="box-body">
                                                                            <fieldset id="work_phone_form">
                                                                            </fieldset>
                                                                        </div>

                                                                        <div class="box-footer">
                                                                            <button type="button" class="btn btn-flat  btn-sm btn-flat pull-right  btn-aqua" id="add_work_phone">
                                                                                Добавить телефон
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Мобильный телефон</label>
                                                                <div class="col-sm-10">
                                                                    <div class="box box-solid box-info">
                                                                        <div class="box-body">
                                                                            <fieldset id="work_mobile_phone_form">
                                                                            </fieldset>
                                                                        </div>

                                                                        <div class="box-footer">
                                                                            <button type="button" class="btn btn-flat btn-sm  btn-flat pull-right  btn-aqua" id="add_work_mobile_phone">
                                                                                Добавить телефон
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Внутренний
                                                                    телефон</label>
                                                                <div class="col-sm-10">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-phone"></i>
                                                                        </div>
                                                                        <input type="text" class="form-control" name="work_local"
                                                                               data-inputmask='"mask": "9999"' data-mask>
                                                                    </div>
                                                                    <!-- /.input group -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="tab_4">
                                                    <div class="box box-solid box-info">
                                                        <div class="box-body">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Оклад<b class="text-red">*</b></label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group date">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-ruble"></i>
                                                                        </div>
                                                                        <input type="text" class="form-control pull-right" id="addSalary" name="addSalary" value="0">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Не выплачивать аванс сотруднику</label>
                                                                <div class="col-sm-9">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input name="member_avans_pay" type="checkbox">
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Комментарий</label>
                                                                <div class="col-sm-9">
                                                                    <textarea name="Comment" id="Comment" cols="1" rows="1" class="form-control" placeholder="В этом поле нужно описать начальные финансовые условия"></textarea>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.tab-pane -->
                                            </div>
                                            <!-- /.tab-content -->
                                        </div>
                                    </div>
                                    <div class="modal-footer" style="border:0;">
                                        <a class="btn pull-left btn-success btn-flat btn-sm" data-dismiss="modal" title="Закрыть">
                                            <i class="fa fa-times"></i> Закрыть
                                        </a>

                                        <button type="submit" class="btn btn-danger btn-flat btn-sm" name="submit_add"><i class="fa fa-save"></i> Добавить сотрудника</button>

                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                    </div>

                    <div class="modal fade" id="modal-csv">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form method="post" enctype="multipart/form-data">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Импорт из CSV</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="box box-info box-solid">
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label>Выберите файл: </label>
                                                    <input type="file" name="CSVfileToUpload" id="CSVfileToUpload">

                                                    <p class="help-block">
                                                        В CSV файле поля должны быть разделены с помощью точки с запятой';'.
                                                    </p>
                                                    <p class="help-block">
                                                        Импортируются поля в следующей последовательности:
                                                    <ul class="help-block">
                                                        <li>Фамилия <b class="text-red">(Обязательное поле)</b></li>
                                                        <li>Имя <b class="text-red">(Обязательное поле)</b></li>
                                                        <li>Отчество <b class="text-red">(Обязательное поле)</b></li>
                                                        <li>Девичья фамилия</li>
                                                        <li>Пол</li>
                                                        <li>Дата рождения</li>
                                                        <li>Национальность</li>
                                                        <li>Номер 1С Зик</li>
                                                        <li>Направление <b class="text-red">(Обязательное поле)</b></li>
                                                        <li>Отдел <b class="text-red">(Обязательное поле)</b></li>
                                                        <li>Должность <b class="text-red">(Обязательное поле)</b></li>
                                                        <li>Дата приема <b class="text-red">(Обязательное поле)</b></li>
                                                        <li>Логин</li>
                                                        <li>Имя в 1С77</li>
                                                        <li>Дата увольнения</li>
                                                    </ul>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-flat btn-primary pull-left btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                                        <button type="submit" class="btn btn-flat btn-warning btn-sm" name="submit"><i class="fa fa-save"></i> Импортировать</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <?php
                }
                ?>



                <?php
                /*
                 * Если был добавлен пользователь
                 * То вывожу масси debug
                 */
                if (isset($_POST["submit_add"])) {
                    ?>
                    <div class="modal fade" id="modal-debug">
                        <div class="modal-dialog" style="width: 90%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h3 class="modal-title">Добавлен новый сотрудник</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="box box-solid box-success">
                                        <div class="box-body">
                                            <?php
                                            foreach ($_POST as $key => $value) {
                                                if (is_array($value)) {
                                                    foreach ($value as $k => $i) {
                                                        if (is_array($i)) {
                                                            foreach ($i as $k1 => $i1) {
                                                                echo "<p>&emsp;<b>" . $key . "</b>&emsp;&emsp;&emsp;" . $k . "&emsp;&emsp;&emsp;" . $k1 . "&nbsp; => &nbsp;" . $i1 . "</p>";
                                                            }
                                                        } else {
                                                            echo "<p>&emsp;<b>" . $key . "</b>&emsp;&emsp;&emsp;" . $k . "&nbsp; => &nbsp;" . $i . "</p>";
                                                        }
                                                    }
                                                } else {
                                                    $aNation = [];
                                                    foreach ($Nations->GetNations() as $nation) {
                                                        $aNation[$nation->getId()] = $nation;
                                                    }

                                                    $aDirection = [];
                                                    foreach ($Directions->GetDirections() as $direction) {
                                                        $aDirection[$direction->getId()] = $direction;
                                                    }

                                                    $aDepartment = [];
                                                    foreach ($Departments->GetDepartments() as $department) {
                                                        $aDepartment[$department->getId()] = $department;
                                                    }

                                                    $aPosition = [];
                                                    foreach ($Positions->GetPositions() as $position) {
                                                        $aPosition[$position->getId()] = $position;
                                                    }

                                                    switch ($key) {
                                                        case "addDate":
                                                            echo "<p style='font-size: 15px;'><b>Дата добавления:</b> " . $value."</p><BR>";
                                                            break;
                                                        case "inputLastName":
                                                            echo "<p style='font-size: 15px;'><b>Фамилия:</b> " . $value."</p><BR>";
                                                            break;
                                                        case "InputName":
                                                            echo "<p style='font-size: 15px;'><b>Имя:</b> " . $value."</p><BR>";
                                                            break;
                                                        case "inputMiddle":
                                                            echo "<p style='font-size: 15px;'><b>Отчество:</b> " . $value."</p><BR>";
                                                            break;
                                                        case "sex":
                                                            echo "<p style='font-size: 15px;'><b>Пол:</b> ".($i==0?'мужской':'женский')."</p><BR>";
                                                            break;
                                                        case "nation":
                                                            $nation = $aNation[$value];
                                                            echo "<p style='font-size: 15px;'><b>Национальность:</b> ".$nation->GetName()."</p><BR>";
                                                            break;
                                                        case "direction":
                                                            echo "<p style='font-size: 15px;'><b>Направление:</b> ".$aDirection[$value]->GetName()."</p><BR>";
                                                            break;
                                                        case "department":
                                                            echo "<p style='font-size: 15px;'><b>Отдел:</b> ".$aDepartment[$value]->GetName()."</p><BR>";
                                                            break;
                                                        case "position":
                                                            echo "<p style='font-size: 15px;'><b>Должность:</b> ".$aPosition[$value]->GetName()."</p><BR>";
                                                            break;
                                                        case "addSalary":
                                                            echo "<p style='font-size: 15px;'><b>Оклад:</b> ".number_format($value, 2, ',', ' ')."</p><BR>";
                                                            break;
                                                        case "Comment":
                                                            echo "<p style='font-size: 15px;'>Комментарий: ".$value."</p><BR>";
                                                            break;
                                                    }
                                                }
                                            }
//                                            echo '<BR><BR>';

//                                            foreach ($Debug as $value) {
//                                                echo "<p>" . $value . "</p>";
//                                            }
                                            ?>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <?php
                /*
                 * Вывод информации об ощибках
                 */
                if ($Errors->getCount() > 0) {
                    ?>
                    <div class="modal fade" id="modal-errors">
                        <div class="modal-dialog" style="width: 90%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h3 class="modal-title">Ошибки</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="box box-solid box-danger">
                                        <div class="box-body">
                                            <?php
                                            foreach ($Errors->getErrors() as $value) {
                                                echo "<p style='color: black;'>" . htmlspecialchars($value) . "</p>";
                                            }
                                            ?>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>

        </section>
    </div>
    <?php require_once 'footer.php'; ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>


<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<!-- Page script -->
<script>
    $(function () {
        $('[data-mask]').inputmask()

        //Date picker
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'dd.mm.yyyy',
            startDate: '-80Y',
            endDate: '-18Y'
        })

        $('#datepicker2').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'dd.mm.yyyy'
        })

        $('#MembersTable').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': false,
            'autoWidth': true,
            'pageLength': 25,
            "language": {
                "paginate" : {
                    "previous" : "Назад",
                    "next" : "Вперед",
                },
                "lengthMenu": 'Показать <select class="form-control input-sm">'+
                            '<option value="25">25</option>'+
                            '<option value="30">30</option>'+
                            '<option value="35">35</option>'+
                            '<option value="40">40</option>'+
                            '<option value="45">45</option>'+
                            '<option value="50">50</option>'+
                            '<option value="-1">Все</option>'+
                            '</select> записей',
                "search": "Найти в таблице _INPUT_",
                "searchPlaceholder": "ФИО",
            }
        })
    })

    $(document).ready(function () {
        $("#add_presonal_phone").click(function () {
            var lastField = $("#phone_form div:last"); // получаем last div

            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;


            var fieldWrapper = $("<span id=\"field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);

            var fForm = $("<div class=\"form-group\">");
            var fName = $("<div class=\"col-md-10\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-phone\"></i></div><input maxlength=14 type=\"text\" class=\"form-control\" name=\"personal_phone[]\" data-inputmask='\"mask\": \"(999)999-99-99\"' data-mask value=\"812\" /></div></div>");
            var removeButton = $("<div class=\"col-md-2\"><div class=\"input-group\"><input type=\"button\" class=\"btn btn-flat btn-aqua\" value=\"Удалить\" /></div></div>");

            removeButton.click(function () {
                $(this).parent().remove();
            });

            fForm.append(fName);
            fForm.append(removeButton);
            fieldWrapper.append(fForm);

            $("#phone_form").append(fieldWrapper);
            $('[data-mask]').inputmask();
        });

        $("#add_presonal_mobile_phone").click(function () {
            var lastField = $("#phone_mobile_form div:last"); // получаем last div

            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;


            var fieldWrapper = $("<span id=\"field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);

            var fForm = $("<div class=\"form-group\">");
            var fName = $("<div class=\"col-md-10\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-phone\"></i></div><input type=\"text\" class=\"form-control\" name=\"add_presonal_mobile_phone[]\" data-inputmask='\"mask\": \"(999)999-99-99\"' data-mask /></div></div>");
            var removeButton = $("<div class=\"col-md-2\"><div class=\"input-group\"><input type=\"button\" class=\"btn btn-flat btn-aqua\" value=\"Удалить\" /></div></div>");

            removeButton.click(function () {
                $(this).parent().remove();
            });

            fForm.append(fName);
            fForm.append(removeButton);
            fieldWrapper.append(fForm);

            $("#phone_mobile_form").append(fieldWrapper);
            $('[data-mask]').inputmask();
        });

        $("#add_emergency_phone").click(function () {
            var lastField = $("#emergency_phone_form span:last"); // получаем last div
            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
            var fieldWrapper = $("<span id=\"em_field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);
            var fForm = $("<div class=\"form-group\">");
            var fPhone = $("<div class=\"col-md-5\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-phone\"></i></div><input type=\"text\" class=\"form-control\" name=\"emergency[" + intId + "][phone]\" data-inputmask='\"mask\": \"(999)999-99-99\"' data-mask /></div></div>");
            var fName = $("<div class=\"col-md-5\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-user\"></i></div><input type=\"text\" class=\"form-control\" name=\"emergency[" + intId + "][name]\" /></div></div>");
            var removeButton = $("<div class=\"col-md-2\"><div class=\"input-group\"><input type=\"button\" class=\"btn btn-flat btn-aqua\" value=\"Удалить\" /></div></div>");

            removeButton.click(function () {
                $(this).parent().remove();
            });

            fForm.append(fPhone);
            fForm.append(fName);
            fForm.append(removeButton);
            fieldWrapper.append(fForm);

            $("#emergency_phone_form").append(fieldWrapper);
            $('[data-mask]').inputmask();
        });

        $("#add_work_phone").click(function () {
            var lastField = $("#work_phone_form div:last"); // получаем last div

            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;


            var fieldWrapper = $("<span id=\"field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);

            var fForm = $("<div class=\"form-group\">");
            var fName = $("<div class=\"col-md-10\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-phone\"></i></div><input type=\"text\" class=\"form-control\" name=\"work_phone[]\" data-inputmask='\"mask\": \"(999)999-99-99\"' data-mask value=\"812\" /></div></div>");
            var removeButton = $("<div class=\"col-md-2\"><div class=\"input-group\"><input type=\"button\" class=\"btn btn-flat btn-aqua\" value=\"Удалить\" /></div></div>");

            removeButton.click(function () {
                $(this).parent().remove();
            });

            fForm.append(fName);
            fForm.append(removeButton);
            fieldWrapper.append(fForm);

            $("#work_phone_form").append(fieldWrapper);
            $('[data-mask]').inputmask();
        });

        $("#add_work_mobile_phone").click(function () {
            var lastField = $("#work_mobile_phone_form div:last"); // получаем last div

            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;


            var fieldWrapper = $("<span id=\"field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);

            var fForm = $("<div class=\"form-group\">");
            var fName = $("<div class=\"col-md-10\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-phone\"></i></div><input type=\"text\" class=\"form-control\" name=\"work_mobile_phone[]\" data-inputmask='\"mask\": \"(999)999-99-99\"' data-mask /></div></div>");
            var removeButton = $("<div class=\"col-md-2\"><div class=\"input-group\"><input type=\"button\" class=\"btn btn-flat btn-aqua\" value=\"Удалить\" /></div></div>");

            removeButton.click(function () {
                $(this).parent().remove();
            });

            fForm.append(fName);
            fForm.append(removeButton);
            fieldWrapper.append(fForm);

            $("#work_mobile_phone_form").append(fieldWrapper);
            $('[data-mask]').inputmask();
        });

        $("#add_email").click(function () {
            var lastField = $("#emails_form span:last"); // получаем last div
            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
            var fieldWrapper = $("<span id=\"field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);
            var fForm = $("<div class=\"form-group\">");
            var fMail = $("<div class=\"col-md-5\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-envelope\"></i></div><input type=\"text\" class=\"form-control\" name=\"inputEmail[" + intId + "][email]\" /></div></div>");
            var fName = $("<div class=\"col-md-5\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-sticky-note\"></i></div><input type=\"text\" class=\"form-control\" name=\"inputEmail[" + intId + "][comment]\" /></div></div>");
            var removeButton = $("<div class=\"col-md-2\"><div class=\"input-group\"><input type=\"button\" class=\"btn btn-flat btn-aqua\" value=\"Удалить\" /></div></div>");

            removeButton.click(function () {
                $(this).parent().remove();
            });

            fForm.append(fMail);
            fForm.append(fName);
            fForm.append(removeButton);
            fieldWrapper.append(fForm);

            $("#emails_form").append(fieldWrapper);
        });

        <?php
        if (isset($_POST["submit_add"])) {
        ?>
        $("#modal-debug").modal('show');
        <?php
        }

        if ($Errors->getCount() > 0) {
        ?>
        $("#modal-errors").modal('show');
        <?php
        }

        ?>
    });
</script>

<script>
    $(document).ready(function () {
        $('select[name=department] option').hide();
        $('select[name=position] option').hide();
    });

    $('select[name=direction]').change(function() {
        $('select[name=department] option[value="-1"]').prop('selected',true);
        $('select[name=position] option[value="-1"]').prop('selected',true);

        $('select[name=department] option').hide();
        $("select[name=department] option:selected").prop("selected", false)
        $('select[name=position] option').hide();
        $("select[name=position] option:selected").prop("selected", false)

        $('select[name=department] option.' + $('select[name=direction]').val()).show();
    });

    $('select[name=department]').change(function() {
        $('select[name=position] option[value="-1"]').prop('selected',true);
        $('select[name=position] option').hide();
        $("select[name=position] option:selected").prop("selected", false)
        $('select[name=position] option.' + $('select[name=department]').val()).show();
    });

</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>

</body>
</html>

