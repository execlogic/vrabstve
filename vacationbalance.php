<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 15.04.22
 * Time: 17:28
 */

// Время работы скрипта
$start = microtime(true);

require_once 'app/MemberInterface.php';
require_once 'app/AwhInterface.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

require_once 'app/AwhMemberInterface.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());


// Настройки

$Permission_ReportMemberAWH = 0;
$RoleDepartmentFilter = array();
foreach ($Roles as $Role) {
    if ($Role->getReportMemberAWH() == 1) {
        $Permission_ReportMemberAWH = 1;

        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[] = $Role->getDepartment();
        }
    }
}

if ($Permission_ReportMemberAWH == 0) {
    header("Location: 404.php");
    exit();
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

$date_m = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
$AWH = new AwhInterface();


$MI = new MemberInterface();

$date = date("Y-m");
$AWHData = array();

if (isset($_POST['date'])) {
    $aData = [];
    $date = $_POST['date'];
    $MI->fetchMembers(false, null, true);
    $aMembers = $MI->GetMembers();
    $aDate = explode("-", $date);
    foreach ($aMembers as $member) {
        if ($member->getNotCountUrv()==1)
            continue;
        // Зятькова
        if ($member->getId() == 66 && $date<2022) {
            continue;
        }
        if (in_array($member->getPosition()['id'], AwhMemberInterface::$positionBanList)) {
            continue;
        }
        $employment_date = DateTime::createFromFormat("d.m.Y", $member->getEmploymentDate());
        if ($employment_date->format("Y") > $aDate[0])
            continue;

        $AMI = new AwhMemberInterface();
        $AMI->calculateHoliday($member->getId(), $date);
        $aData[] = [
            'id'                     => $member->getId(),
            'name'                   => $member->getLastname(). " ". $member->getName(),
            'absence'                => $AMI->sumAbsence,
            'disease'                => $AMI->sumDisease,
            'diseaseBalance'         => $AMI->diseaseBalance,
            'lastVacationBalance'    => $AMI->vacation_balance,
            'currentVacationBalance' => $AMI->holiday_balance_calculation,
            'unspentVacation'        => $AMI->vacation_balance_calculation,
        ];

        if (isset($_POST['saveData']) && isset($aDate[1]) && $aDate[1]==12) {
            $AMI->save();
            // Сохраняем данные
        }
    }

    if (isset($_POST['saveData']) && isset($aDate[1]) && $aDate[1]==12) {
        // рефреш страницы
        header('Location: '.$_SERVER['REQUEST_URI']);
    }
}

$menu_open = 3;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Журнал УРВ по сотруднику</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
<!--    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        thead th {
            position: sticky;
            top: 0;
            background: white;
        }
    </style>
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse" style="overflow-x: auto !important; height: auto; min-height: 100%;">
<div class="wrapper" style="overflow: unset !important; height: auto; min-height: 100%;">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
                УРВ Остатки отпуска болезни
            </h4>
            <ol class="breadcrumb">
                <li class="active">УРВ Остатки отпуска болезни</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="box box-solid box-plane">
                <div class="box-body">
                    <form class="form-horizontal" method="post">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Выберите дату c: </label>
                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker" name="date" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                           value="<?php echo $date; ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-2"><input type="submit" class="btn btn-flat btn-default" name="SendDate" value="Получить данные"></div>
                            <?php
                            $aDate = explode("-",$date);
                            if (isset($aDate[1]) && $aDate[1]==12) {
                                ?>
                                <div class="col-sm-2"><input type="submit" class="btn btn-flat btn-warning" name="saveData" value="Перенести остатки на следующий год"></div>
                                <?php
                            }
                            ?>
                        </div>
                    </form>
                </div>
            </div>

            <?php
            if (isset($_POST['SendDate']) && isset($_POST['date'])) {

                ?>
                <div class="box box-solid box-plane">
                    <div class="box-body">
                        <table class="table table-striped table-hover" style="overflow-y: auto" id="DataTable">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>ФИО</th>
                                <th>Накопленные отсутствия</th>
                                <th>Остаток отпусков за прошлый год</th>
                                <th>Остаток отпусков на <?=$date?>-<?php
                                    if ($aDate[0] < date("Y") || (date("m") == 12 && date("d") >= 20)) {
                                        echo "31";
                                    } else {
                                        echo "01";
                                    }
                                        ?></th>
                                <th>Сгоревшие отпуска</th>
                                <th>Потраченные больничные</th>
                                <th>Остаток больничных</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($aData) > 0) {
                                $sumAbsence = 0;
                                $sumDiseaseBalance = 0;
                                $sumDisease = 0;
                                $sumLastVacationBalance = 0;
                                $sumCurrentVacationBalance = 0;
                                $sumUnspentVacation = 0;
                                foreach ($aData as $key=>$aItem) {
                                    $sumAbsence += $aItem['absence'];
                                    $sumDiseaseBalance += $aItem['diseaseBalance'];
                                    $sumDisease += $aItem['disease'];
                                    $sumLastVacationBalance += $aItem['lastVacationBalance'];
                                    $sumCurrentVacationBalance += $aItem['currentVacationBalance'];
                                    $sumUnspentVacation += $aItem['unspentVacation'];
                                    ?>
                                    <tr>
                                        <td><?=($key+1)?></td>
                                        <td><a href="journal_awh_member.php?MemberId=<?=$aItem['id']?>&date=<?=$aDate[0]?>&SendDate='SendDate'">
                                            <?=$aItem['name'];?>
                                            </a>
                                        </td>
                                        <td <?=round($aItem['absence'],2)<0?'style="color:red"':''?>>
                                            <?=number_format($aItem['absence'], 2, '.', ' ');?>
                                        </td>

                                        <td <?=round($aItem['lastVacationBalance'],2)<0?'style="color:red"':''?>>
                                            <?=number_format($aItem['lastVacationBalance'], 2, '.', ' ');?>
                                        </td>
                                        <td <?=round($aItem['currentVacationBalance'],2)<0?'style="color:red"':''?>>
                                            <?=number_format($aItem['currentVacationBalance'], 2, '.', ' ');?>
                                        </td>
                                        <td>
                                            <?=number_format($aItem['unspentVacation'], 2, '.', ' ');?>
                                        </td>
                                        <td>
                                            <?=number_format($aItem['disease'], 2, '.', ' ');?>
                                        </td>
                                        <td <?=round($aItem['diseaseBalance'],2)<0?'style="color:red"':''?>>
                                            <?=number_format($aItem['diseaseBalance'], 2, '.', ' ');?>
                                        </td>

                                    </tr>
                            <?php
                                }
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="2">Итого</th>
                                <th><?=number_format($sumAbsence, 2, '.', ' ');?></th>
                                <th><?=number_format($sumLastVacationBalance, 2, '.', ' ');?></th>
                                <th><?=number_format($sumCurrentVacationBalance, 2, '.', ' ');?></th>
                                <th><?=number_format($sumUnspentVacation, 2, '.', ' ');?></th>
                                <th><?=number_format($sumDisease, 2, '.', ' ');?></th>
                                <th><?=number_format($sumDiseaseBalance, 2, '.', ' ');?></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <?php
            }
            ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- DataTables -->
<!--<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>-->
<!--<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>-->

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
    $(function () {
        // $('#DataTable').DataTable({
        //     'paging': false,
        //     'lengthChange': false,
        //     'searching': false,
        //     'ordering': true,
        //     'info': false,
        //     'autoWidth': true
        // })


        // Выбор даты
        // $('#datepicker').datepicker({
        //     language: 'ru',
        //     autoclose: true,
        //     viewMode: "Years",
        //     minViewMode: "years",
        //     format: 'yyyy',
        // })

        // Выбор даты
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            format: 'yyyy-mm',
        })
    })
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>

