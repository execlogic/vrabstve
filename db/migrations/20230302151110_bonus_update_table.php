<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class BonusUpdateTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    private $tableName = "bonus";

    public function up()
    {
        $table = $this->table($this->tableName);
        $table->addColumn('service_id', 'integer')
            ->addForeignKey('service_id', 'member', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'NO_ACTION'])
            ->update();
    }

    public function down()
    {
        $table = $this->table($this->tableName);
        $table->removeColumn('service_id')->save();
    }

}
