<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class WindowManagerMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    private $tableName = "window_manager_percent";

    public function up()
    {
        $table = $this->table($this->tableName);
        $table->addColumn('manager_id', 'integer')
            ->addColumn('window_id', 'integer')
            ->addColumn('percent', 'decimal', [
                'default' => null,
                'null' => false,
                'precision'=>8,
                'scale'=>2
            ])
            ->addColumn('created', 'datetime')
            ->addColumn('admin_id', 'integer')
            ->addForeignKey('manager_id', 'member', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->addForeignKey('window_id', 'member', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
//            ->addForeignKey('admin_id', 'member', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->create();
    }

    public function down() {
        $table = $this->table($this->tableName);
        $table->drop()->save();
    }
}
