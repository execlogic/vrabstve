<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 14.09.18
 * Time: 18:32
 */

// Время работы скрипта
$start = microtime(true);
require_once "app/ErrorInterface.php";
require_once "app/MemberInterface.php";
require_once 'app/AwhInterface.php';
require_once 'app/AdministrativeBlock.php';
require_once 'app/Retention.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once "app/Avans.php";
require_once 'app/ReportStatus.php';
require_once 'app/PayRoll.php';
require_once 'app/PayRollInterface.php';

require_once 'app/Notify.php';


function roundSumm($summ)
{
    // Округление до 10
    if ($summ > 0) { //Если сумма <= 0 то не обрабатываем данные
        //$summ = round($summ); // округляю сотые и десятые
        $summ = (int)$summ;

        $ostatok = ($summ % 10); // Получаю остаток от деления на 10
        if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
            $summ = $summ - $ostatok + 10;
        } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
            $summ = $summ - $ostatok;
        };
    } else {
        $summ = 0;
    }
    return $summ;
}


session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

$Permission_AvansCalculation = 0; // Расчет аванса

$Permission_ShowAdvanceCalculation = 0;
$Permission_ShowAdministrativeBlock = 0;
$Permission_ShowRetitiotionBlock = 0;
$Permission_AWH = 0;

$Error = new ErrorInterface();

if ($_REQUEST['id']) {
    // id пользователя
    $user_id = $_REQUEST['id'];

    // Если не число то ошибка выходим
    if (!is_numeric($user_id)) {
        header('Location: index.php');
        exit();
    }

    // Создаем объект MemberInterface
    $MI = new MemberInterface();
    // Получаю данные по пользователю
    try {
        $member = $MI->fetchMemberByID($user_id);
    } catch (Exception $e) {
        echo $e->getMessage();
        exit();
    }

    if ($member->getAvansPay() == 0) {
        header('Location: 404.php');
        exit();
    }

    if ($member->getEmploymentDate()) {
        $emp_date_arr = explode(".", $member->getEmploymentDate());
    }

    /*** Права доступа ***/
    $member_department_id = $member->getDepartment()['id'];
    if ($User->getMemberId() != $user_id) {

        $current = current(array_filter($Roles, function ($Role) use ($member_department_id) { // Если в ролях есть аванс и (отдел == 0 или отдел == отделу пользователя)
            return (($Role->getAvansCalculation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        }));

        if ($current) { // Если существует объект, то можно смотреть и править.
            $Permission_AvansCalculation = 1;
        } else { // Если это не свой профиль и нету объекта ролей, то слать в 404
            if (!($User->getMemberId() == 94 && $member->getId() == 65 )) {
                header("Location: 404.php");
            }
        }

        array_filter($Roles, function ($Role) use (&$Permission_AWH) {
            if ($Role->getId() == 1 || $Role->getId() == 5 || $Role->getId() == 6) {
                $Permission_AWH = 1;
                return true;
            } else {
                return false;
            };
        });

        array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_AWH) {
            if (($Role->getAWHEdit() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
                $Permission_AWH = 1;
                return true;
            } else {
                return false;
            };
        });

        array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ShowAdvanceCalculation) {
            if (($Role->getShowAdvanceCalculation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
                $Permission_ShowAdvanceCalculation = 1;
                return true;
            }
            return false;
        });

    } else {
        $Permission_ShowAdvanceCalculation = 1;
    }

    if ($User->getMemberId() == 94 && $member->getId() == 65 ) {
        $Permission_AvansCalculation = 1; // Расчет аванса
        $Permission_ShowAdvanceCalculation = 1;
        $Permission_AWH = 1;
    }

    /*
     * Выставляю текущую дату, если в запросе не будет ['date']
     */
    $date = date("m.Y");

    if (isset($_COOKIE['salary_date'])) {
        $date = $_COOKIE['salary_date'];
    }

    if (isset($_REQUEST['date']) || isset($_COOKIE['salary_date'])) {
        $date = $_REQUEST['date']; // то выставляю дату с новыми зачениями и начинаю разбор блока

        $RS = new ReportStatus();
        $Status = $RS->getFiltredStatus($date,2, $member_department_id)['status_id'];

        $JP = $MI->getJobPostByDate($user_id, $date);
        if ($JP) {
            $member->setDepartment(["id" => $JP["department_id"], "name" => $JP["department_name"]]);
        }


        $member_department_id = $member->getDepartment()['id'];

        $current = current(array_filter($Roles, function ($Role) use ($member_department_id) { // Если в ролях есть аванс и (отдел == 0 или отдел == отделу пользователя)
            return (($Role->getPremiyaCalculation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        }));

        if (!$current) { // Если существует объект, то можно смотреть и править.
            header("Location: 404.php");
        }

        $current = null;

        /*
        *  Создаю объект производственный календарь
        */
//        $PC = new ProductionCalendar();
        /*
         *  Извлекаю из БД
         */
//        $PC->fetchByDateWithDepartment($date, $member_department_id);

        $AV = new Avans();
        $AV->setMember($member);
        $AV->setDate($date);
        $AV->fetch($user_id);
        $AWH = $AV->getAwh();

        // ******************************* Обработка событий
        if (isset($_POST['date']) && isset($_POST['SendForm']) && ($AV->getPaymentMade() == true) && ($Permission_AvansCalculation == 1)) {
            $Error->add("Ведомость уже на стадии, в которой нельзя вносить изменения в УРВ");
        }

        if (isset($_POST['date']) && isset($_POST['SendForm']) && ($AV->getPaymentMade() == false) && ($Permission_AvansCalculation == 1)) {
            $member_awh = $AWH->getAwhByMember($_POST['date'], 2, $user_id);

            /** ******************** УРВ ******************** */
            if (isset($_POST['Absence'])) {
                $_POST['Absence'] = str_replace(",", ".", $_POST['Absence']);
                if (is_numeric($_POST['Absence']) && ($_POST['Absence'] != $member_awh['absence'])) {
                    try {
                        $AWH->setAbsenceByMember($user_id,$date,2,$_POST['Absence']);
                        $member_awh['absence'] = $_POST['Absence'];
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['Holiday'])) {
                $_POST['Holiday'] = str_replace(",", ".", $_POST['Holiday']);
                if (is_numeric($_POST['Holiday']) && (isset($member_awh['holiday']) && $_POST['Holiday'] != $member_awh['holiday'])) {
                    try {
                        $AWH->setHolidayByMember($user_id,$date,2, $_POST['Holiday']);
                        $member_awh['holiday'] = $_POST['Holiday'];
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['Disease'])) {
                $_POST['Disease'] = str_replace(",", ".", $_POST['Disease']);
                if (is_numeric($_POST['Disease']) && (isset($member_awh['disease']) && $_POST['Disease']!=$member_awh['disease'])) {
                    try {
                        $AWH->setDiseaseByMember($user_id, $date,2,$_POST['Disease']);
                        $member_awh['disease'] = $_POST['Disease'];
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['awh_note'])) {
                if ($_POST['awh_note'] != $AV->getAwhNote()) {
                    try {
                        $AWH->setNoteByMember($user_id, $date, 2, $_POST['awh_note']);
                        $AV->setAwhNote($_POST['awh_note']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            $AV->fetch($user_id);

            if ($Status > 1 && $Status < 4) {
                /*
                * Создаю объект интерфейса ведомости
                */
                $PI = new PayRollInterface();
                // Создаю объект ведомости и заполняю его
                $PayRoll = new PayRoll();
                $PayRoll->setDate($date); // Дата
                $PayRoll->setTypeOfPayment(2); // Тип выплаты
                $PayRoll->setId($member->getId()); // Пользовательский ID

                $PayRoll->setDirection($member->getDirection()); // Напраление
                $PayRoll->setDepartment($member->getDepartment()); // Отдел
                $PayRoll->setPosition($member->getPosition()); // Должность

                $PayRoll->setFio($member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()); // ФИО
                $PayRoll->setSalary($member->getSalary()); // Оклад
                $PayRoll->setAdvancePayment($AV->getAllSalary()); // АВАНС

                $PayRoll->setAwhdata(["absence" => ($AV->getAbsence() + +$AV->getNewAbsence()), "holiday" => $AV->getHoliday(), "disease" => $AV->getDisease()]);
                $PayRoll->setAbsences($AV->getAllAbsenceRub()); // Отсутствия
                $PayRoll->setCommercial(0); // Коммерческая премия
                $PayRoll->setAdministrative($AV->getAllAdministrativeRubles()); // Административная премия
                $PayRoll->setHold($AV->getAllHoldRubles()); // Удержания
                $PayRoll->setCorrecting(0); // Корректировки

                $PayRoll->setMotivation($member->getMotivation());
                $PayRoll->Calculation();

                //Добавляю в массив объектов
                $PayRollArray[] = $PayRoll;

                foreach ($PayRollArray as $item) {
                    try {
                        $item->setSumm(roundSumm($item->getSumm()));
                        $PI->UpdateFinancialPayout($item);
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }
                }
            }
        }

    }

    if (isset($_POST['rm_statment'])) {
        if ($MI->checkActivePayout($member->getId(), $date, 2)) {
            $MI->disableInPayout($member->getId(), $date, 2);
        } else {
            $MI->enableInPayout($member->getId(), $date, 2);
        }
    }
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Аванс</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Расчет аванса
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Список пользователей</a></li>
                <li><a href="profile.php?id=<?php echo $user_id; ?>">Профиль сотрудника</a></li>
                <li class="active">Расчет аванса</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php
            /*
                 * Вывод информации об ощибках
                 */
            if ($Error->getCount() > 0) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="TableErr">
                            <thead>
                            <th>Сообщение</th>
                            </thead>
                            <tbody>
                            <?php
                            foreach (array_unique($Error->getErrors()) as $value) {
                                echo "<tr><td>" . $value . "</td><td></td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php
            }
            ?>

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header">
                            <h3>
                                <?php
                                // Вывожу ФИО
                                if ($member->getMaidenName()) {
                                    echo $member->getLastName() . " ( " . $member->getMaidenName() . " ) " . $member->getName() . " " . $member->getMiddle();
                                } else {
                                    echo $member->getLastName() . " " . $member->getName() . " " . $member->getMiddle();
                                };
                                ?>
                            </h3>
                        </div>
                        <div class="box-body">
                            <form class="form-horizontal" method="post">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Выберите дату: </label>
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="datepicker" name="date" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                                   value="<?php echo $date; ?>" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-2"><input type="submit" class="btn btn-flat btn-default" name="SendDate" value="Получить данные"></div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <?php
                    if (isset($_POST['date'])) {
                        ?>

                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <?php
                                if ($Permission_ShowAdvanceCalculation) {
                                    ?>
                                    <li class="active"><a href="#tab_0" data-toggle="tab" aria-expanded="false">Предварительный расчет</a></li>
                                    <?php
                                };
                                ?>

                                <li><a href="#tab_4" data-toggle="tab" aria-expanded="false">УРВ</a></li>
                            </ul>
                        </div>

                        <form method="post" class="form-horizontal">
                            <input type="hidden" name="date" value="<?php echo $date; ?>">
                            <div class="tab-content">
                                <div class="tab-pane" id="tab_4">
                                    <div class="box box-info box-solid">
                                        <div class="box-body">
                                            <?php
                                            if (isset($_POST['date'])) {
                                                ?>
                                                <div class="box box-info box-solid">
                                                    <div class="box-body">
                                                        <label class="col-sm-2">Количество рабочих дней:</label>
                                                        <div class="col-md-10">
                                                            <?php echo $AV->getWorkingDays(); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                            <?php
                                            if ($emp_date_arr[1] . "." . $emp_date_arr[2] == $date && $emp_date_arr[0] != 1) {
                                                ?>
                                                <div class="alert alert-warning alert-dismissible" style="border-radius: unset">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                    <h4><i class="icon fa fa-warning"></i> Внимание!</h4>
                                                    Сотрудник принят на работу <?php echo $member->getEmploymentDate(); ?>. Дни отсутсвия с <?php echo "01." . $emp_date_arr[1] . "." . $emp_date_arr[2] . " по " . $member->getEmploymentDate(); ?> присваиваются автоматически.
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Отсутствия до выхода на работу</label>
                                                    <div class="col-md-10">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Holiday"
                                                                   autocomplete="off"
                                                                   value="<?php echo $AV->getNewAbsence(); ?>" disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                            <table class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <th>Отсутствия</th>
                                                <th>Отпуск</th>
                                                <th>Болезнь</th>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><input type="text" class="form-control" name="Absence" id="Absence" autocomplete="off" value="<?php echo $AV->getAbsence();?>" <?php if ($Permission_AWH == 0) {
                                                            echo "disabled";
                                                        }; ?>></td>
                                                    <td><input type="text" class="form-control" name="Holiday" id="Holiday" autocomplete="off" value="<?php echo $AV->getHoliday(); ?>" <?php if ($Permission_AWH == 0) {
                                                            echo "disabled";
                                                        }; ?>></td>
                                                    <td><input type="text" class="form-control" name="Disease" id="Disease" autocomplete="off" value="<?php echo $AV->getDisease(); ?>" <?php if ($Permission_AWH == 0) {
                                                            echo "disabled";
                                                        }; ?>></td>
                                                </tr>
                                                </tbody>
                                            </table>

                                            <div class="form-group">
                                                <label class="col-sm-1 control-label pull-left">Заметка: </label>
                                                <div class="col-sm-11">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-comment"></i>
                                                        </div>
                                                        <textarea class="form-control pull-right" style="height: 34px;"
                                                                  name="awh_note" autocomplete="off"><?php echo $AV->getAwhNote(); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="box-footer">
                                            <?php if ($Permission_AWH == 1) {
                                                ?>
                                                <input type="submit" class="btn btn-danger btn-flat pull-right" name="SendForm" value="Сохранить" <?=(!$Status?'onclick="return confirmation();"':'');?> >
                                                <?php
                                            };
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                if ($Permission_ShowAdvanceCalculation) {
                                    ?>
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="box box-info box-solid">
                                            <div class="box-body">
                                                <!--                                            Вывод данных в таблице-->
                                                <table class="table table-bordered table-hover table-striped">
                                                    <thead>
                                                    <th>Оклад</th>
                                                    <th>Аванс (руб)</th>
                                                    <th>Количество рабочих дней (аванс)</th>
                                                    <th>Отсутствие (дней)</th>
                                                    <th>Отсутствия (руб)</th>
                                                    <th>Итого (руб)</th>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td><?php echo(number_format(($member->getSalary()), 2, ',', ' ')); ?></td>
                                                        <td><?php echo(number_format(($AV->getAllSalary()), 2, ',', ' ')); ?></td>
                                                        <td><?php echo round(($AV->getWorkingDays() / 2),2); ?></td>
                                                        <td><?php echo ($AV->getAbsence()+ $AV->getNewAbsence()); ?></td>
                                                        <td><?php echo(number_format($AV->getAllAbsenceRub(), 2, ',', ' ')); ?></td>
                                                        <td><?php echo(number_format(($AV->getSummary()), 2, ',', ' ')); ?></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="box-body">
                                                <div class="box-footer">
                                                    <?php
                                                    if ($Status > 1 && $Status < 6) {
                                                        if ($MI->checkActivePayout($member->getId(), $date, 2)) {
                                                            ?>
                                                            <button class="btn btn-danger btn-flat pull-right" id="rm_statment" name="rm_statment">Убрать из ведомости</button>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <button class="btn btn-success btn-flat pull-right" id="rm_statment" name="rm_statment">Вернуть в ведомость</button>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </form>
                        <?php
                    }
                    ?>

                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    function confirmation() {
        return confirm("Ведомость премии на дату <?=$date?> не открыта. Хотите сохранить?");
    }

    // Выбор даты
    $(function () {
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            format: 'mm.yyyy',
            //startDate: <?php //echo '\''.$member->getEmploymentDate().'\''; ?>//,
        })
    });

</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>


