<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.10.18
 * Time: 18:30
 */

// Время работы скрипта
$start = microtime(true);

require_once 'app/MemberInterface.php';
require_once 'app/Retention.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

// Настройки
if (!array_filter($Roles, function ($Role) {
    return ($Role->getReportRetetition() == 1);
})) {
    header("Location: 404.php" );
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

$date_m = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
$MI = new MemberInterface();
$MI->fetchMembers();

// Удержания
$RN = new Retention();
//Тип выплат аванс
$RN->setTypeOfPayment(1);
//$RN->fetch($user_id, $date); // Получаю данные по удержанию

//$date_s = new DateTime();
//$date_e = new DateTime();


//var_dump($AWH_Data);
//$date = date("Y");
//$AWHData = array();
//
//if (isset($_POST['SendDate']) && isset($_POST['date']) && isset($_POST['MemberId'])) {
//    $date = $_POST['date'];
//    $Member_id = $_POST['MemberId'];
//    $AWHData = $AWH->getAwhByMemberYear($Member_id,$date);
//}

$date_s = date("Y-m");
$date_e=$date_s;

$date_array = array();

if (isset($_POST['date_s']) && isset($_POST['date_e']) && isset($_POST['MemberId'])) {

    $date_s = $_POST['date_s'];
    $date_e = $_POST['date_e'];
    $Member_id = $_POST['MemberId'];

    if (strtotime($date_s)>strtotime($date_e)) {
        echo "Дата начала не может быть больше!<BR>";
    } else {
        $from = new DateTime($date_s);
        $to   = new DateTime($date_e);
        $to->modify('+1 month');
        $period = new DatePeriod($from, new DateInterval('P1M'), $to);
        foreach (iterator_to_array($period) as $item) {
            try {
                $RN->fetch($Member_id, $item->Format('m.Y'));
            } catch (Exception $e) {
                echo $e->getMessage()."<BR>";
            }
            $date_array[$item->Format('m.Y')] = array("LateWork"=>$RN->getLateWork(), "LateWorkNote"=>$RN->getLateWorkNote(),
                "Internet"=>$RN->getInternet(), "InternetNote"=>$RN->getInternetNote(),
                "CachAccounting"=>$RN->getCachAccounting(), "CachAccountingNote"=>$RN->getCachAccountingNote(),
                "Receivables"=>$RN->getReceivables(), "ReceivablesNote"=>$RN->getReceivablesNote(),
                "Credit"=>$RN->getCredit(), "CreditNote"=>$RN->getCreditNote(),
                "Other"=>$RN->getOther(), "OtherNote"=>$RN->getOtherNote()
            );
        }
    }
}
$menu_open = 3;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Журнал удержаний</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- daterange picker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-daterangepicker/daterangepicker.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
    <style type="text/css">
        .rollover{
            position:relative
        }
        .rollover .tip{
            position:absolute;
            top:-20px;
            left:50%;
            width:200px;
            margin-left:-75px;
            background: #eff3fd;
            padding: 10px;
            display:none;
            border: 1px solid #e1e5ef;
            z-index: 100;
        }
        .rollover:hover .tip{
            display:block
        }
    </style>
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
                Журнал удержаний по сотруднику период
            </h4>
            <ol class="breadcrumb">
                <li class="active">Журнал ведомостей</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="box box-solid box-plane">
                <div class="box-body">
                    <form class="form-horizontal" method="post">


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Выберите дату c: </label>
                            <div class="col-sm-3">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker_s" name="date_s" value="<?php echo $date_s; ?>" autocomplete="off">
                                </div>
                            </div>
                            <label class="col-sm-2 control-label">по: </label>
                            <div class="col-sm-3">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker_e" name="date_e" value="<?php echo $date_e; ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Сотрудники: </label>
                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-check"></i>
                                    </div>
                                    <select class="form-control" name="MemberId">
                                        <?php
                                        foreach ($MI->GetMembers() as $member) {
                                            if (isset($Member_id) && ($Member_id == $member->getId())) {
                                                echo "<option value=\"" . $member->getId() . "\" selected >" . $member->getLastName() . " " . $member->getName() . "</option>";
                                            } else {
                                                echo "<option value=\"" . $member->getId() . "\">" . $member->getLastName() . " " . $member->getName() . "</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2"><input type="submit" class="btn btn-flat btn-default" name="SendDate" value="Получить данные"></div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="box box-solid box-plane">
                <div class="box-body">
                    <table class="table table-striped table-hover" id="DataTable">
                        <thead>
                        <tr>
                            <th>Дата</th>
                            <th>За опоздания</th>
                            <th>За интернет</th>
                            <th>Кассовый учет</th>
                            <th>Дебиторская задолженность</th>
                            <th>Кредит</th>
                            <th>Другое</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (count($date_array)>0) {
                            $Summ_LaterWork = 0;
                            $Summ_Internet = 0;
                            $Summ_CachAccounting = 0;
                            $Summ_Receivables = 0;
                            $Summ_Credit = 0;
                            $Summ_Other = 0;
                            foreach ($date_array as $key=>$item) {
                                if (empty($item['LateWork'])) {
                                    $item['LateWork'] = 0;
                                };

                                if (empty($item['Internet'])) {
                                    $item['Internet'] = 0;
                                };
                                if (empty($item['CachAccounting'])) {
                                    $item['CachAccounting'] = 0;
                                };
                                if (empty($item['Receivables'])) {
                                    $item['Receivables'] = 0;
                                };
                                if (empty($item['Credit'])) {
                                    $item['Credit'] = 0;
                                };
                                if (empty($item['Other'])) {
                                    $item['Other'] = 0;
                                };

                                $Summ_LaterWork += $item['LateWork'];
                                $Summ_Internet += $item['Internet'];
                                $Summ_CachAccounting += $item['CachAccounting'];
                                $Summ_Receivables += $item['Receivables'];
                                $Summ_Credit += $item['Credit'];
                                $Summ_Other += $item['Other'];

                                echo "<tr>";
                                echo "<td>".$key."</td>";
                                echo "<td><span class=\"rollover\"><div class=\"tip\">".$item['LateWorkNote']."</div>".$item['LateWork']."</span></td>";
                                echo "<td><span class=\"rollover\"><div class=\"tip\">".$item['InternetNote']."</div>".$item['Internet']."</div></td>";
                                echo "<td><span class=\"rollover\"><div class=\"tip\">".$item['CachAccountingNote']."</div>".$item['CachAccounting']."</div></td>";
                                echo "<td><span class=\"rollover\"><div class=\"tip\">".$item['ReceivablesNote']."</div>".$item['Receivables']."</div></td>";
                                echo "<td><span class=\"rollover\"><div class=\"tip\">".$item['CreditNote']."</div>".$item['Credit']."</div></td>";
                                echo "<td><span class=\"rollover\"><div class=\"tip\">".$item['OtherNote']."</div>".$item['Other']."</div></td>";
                                echo "</tr>";
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <?php if (count($date_array)>0) { ?>
                        <tr>
                            <th>Итого</th>
                            <th><?php echo number_format($Summ_LaterWork, 2, ',', ' ');?></th>
                            <th><?php echo number_format($Summ_Internet, 2, ',', ' ');?></th>
                            <th><?php echo number_format($Summ_CachAccounting, 2, ',', ' ');?></th>
                            <th><?php echo number_format($Summ_Receivables, 2, ',', ' ');?></th>
                            <th><?php echo number_format($Summ_Credit, 2, ',', ' ');?></th>
                            <th><?php echo number_format($Summ_Other, 2, ',', ' ');?></th>
                        </tr>
                        <?php }; ?>
                        </tfoot>
                    </table>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- date-range-picker -->
<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
    $(function () {
        $('#DataTable').DataTable({
            'paging': false,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': false,
            'autoWidth': true
        })

        // Выбор даты
        $('#datepicker_s').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            format: 'yyyy-mm',
        })

        $('#datepicker_e').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            format: 'yyyy-mm',
        })
    })
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>


