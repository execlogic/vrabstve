<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 05.10.18
 * Time: 13:55
 */

// Время работы скрипта
$start = microtime(true);

require_once "app/ErrorInterface.php";
require_once "app/MemberInterface.php";
require_once 'app/CustomPercent.php';

require_once "app/MemberFinanceBlock.php";
require_once 'app/DataItem.php';
require_once 'app/DataManager.php';
require_once 'app/ErrorInterface.php';
require_once 'app/AdministrativeBlock.php';
require_once 'app/Retention.php';
require_once 'app/Correction.php';

require_once 'app/ProductionCalendar.php';
require_once 'app/AwhInterface.php';
require_once 'app/ReportStatus.php';

require_once 'app/PayRollInterface.php';
require_once 'app/PayRoll.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once "app/Unscheduled.php";
require_once "app/LeavingPremium.php";

require_once 'app/Notify.php';

function roundSumm($summ, $item=null)
{
    // Округление до 10
    if ($summ > 0) { //Если сумма <= 0 то не обрабатываем данные
        //$summ = round($summ); // округляю сотые и десятые
        $summ = (int)$summ;

        $ostatok = ($summ % 10); // Получаю остаток от деления на 10
        if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
            $summ = $summ - $ostatok + 10;
        } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
            $summ = $summ - $ostatok;
        };
    } else {
        if (!is_null($item) && $item->getTypeOfPayment()==4) {
            $summ = (int)$summ;

            $ostatok = ($summ % 10)*-1; // Получаю остаток от деления на 10
            if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
                $summ = $summ - $ostatok - 10;
            } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
                $summ = $summ + $ostatok;
            };
        } else {
            $summ = 0;
        }
    }
    return $summ;
}

function filter(&$PayRollArray, $RoleDepartmentFilter)
{
    foreach ($PayRollArray as $key => $item) {
        $dep = $item->getDepartment();

        $f = current(array_filter($RoleDepartmentFilter, function ($R) use ($dep) {
            return $R == $dep['id'];
        }));

        if ($f == false) {
            unset($PayRollArray[$key]);
        }
    }
}

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

// Настройки
if (!array_filter($Roles, function ($Role) {
    return ($Role->getFinancialStatement() == 1);
})) {
    header("Location: 404.php");
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

/*
 * Права доступа
 */
$RoleDepartmentFilter = array();

foreach ($Roles as $Role) {
    if ($Role->getId() == 5) {
        $RoleDepartmentFilter = array();
        break;
    }

    if ($Role->getId() == 2 || $Role->getId() == 4) {
        if (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {

            $RoleDepartmentFilter = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[] = $Role->getDepartment();
        }
    }
}


//var_dump($RoleDepartmentFilter);

session_write_close();

$Error = new ErrorInterface();

/*
 * Создаю объект MemberInterface
 */
$MI = new MemberInterface();
/*
 * Получаю список пользователей с минимальными данными
 */
$MI->fetchMembers();

/*
 * Выставляю дату на текущую
 */
$date = date("m.Y");

/*
* Создаю объект интерфейса ведомости
*/
$PI = new PayRollInterface();

/*
* Создаю объект статуса ведомости
*/
$RS = new ReportStatus();


/*
*  ******************************* Подключаемся к БД
*/
$dbh = dbConnect();

$Ud = new Unscheduled();
$LP = new LeavingPremium();

/*
 * Внеплановая выплата обработка событий
 */
if (isset($_POST['Success'])) {

    $ID = $_POST['Success'];
    try {
        $Ud->IncStatus($ID);
    } catch (Exception $e) {
        echo $e->getMessage() . "<BR>";
    }
}

if (isset($_POST['Cansel'])) {
    $ID = $_POST['Cansel'];
    try {
        $Ud->delUnscheduled($ID);
    } catch (Exception $e) {
        echo $e->getMessage() . "<BR>";
    }
}



$raw = $Ud->getData($RoleDepartmentFilter);
$PayRollArray = $LP->getPremium();

if (count($RoleDepartmentFilter) > 0 || (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] != 0)) {
    filter($PayRollArray, $RoleDepartmentFilter);
}

if (isset($_POST['СanselLeaving'])) {
    $ID = $_POST['СanselLeaving'];

    if ($LP->Cansel($ID)) {
        unset($PayRollArray[$ID]);
    }

    unset($_POST);
    $PayRollArray = $LP->getPremium();
    if (count($RoleDepartmentFilter) > 0 || (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] != 0)) {
        filter($PayRollArray, $RoleDepartmentFilter);
    }
    die(header("Location: payment.php"));
}

if (isset($_POST['SuccessLeaving'])) {
    $ID = $_POST['SuccessLeaving'];
    try {
        $LP->IncStatus($ID);
        $PayRollArray = $LP->getPremium();
    } catch (Exception $e) {
        echo $e->getMessage() . "<BR>";
    }

    unset($_POST);
    $PayRollArray = $LP->getPremium();
    if (count($RoleDepartmentFilter) > 0 || (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] != 0)) {
        filter($PayRollArray, $RoleDepartmentFilter);
    }
    die(header("Location: payment.php"));
}

if (isset($_POST['SuccessLeavingAll'])) {
    if (isset($PayRollArray) && count($PayRollArray) > 0) {
        foreach ($PayRollArray as $ID => $item) {
            $item->setSumm(roundSumm($item->getSumm()));
            if ($item->getStatusId() == 4)
                continue;

            $LP->IncStatus($ID);
        }
        $PayRollArray = $LP->getPremium();
        if (count($RoleDepartmentFilter) > 0 || (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] != 0)) {
            filter($PayRollArray, $RoleDepartmentFilter);
        }
    }

    unset($_POST);
}

if (isset($_POST['СanselLeavingAll'])) {
    if (isset($PayRollArray) && count($PayRollArray) > 0) {
        foreach ($PayRollArray as $ID => $item) {
            $item->setSumm(roundSumm($item->getSumm()));
            if ($item->getStatusId() < 5) {
                if ($LP->Cansel($ID)) {
                    unset($PayRollArray[$ID]);
                }
            }
        }
    }

    unset($_POST);
    $PayRollArray = $LP->getPremium();
    if (count($RoleDepartmentFilter) > 0 || (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] != 0)) {
        filter($PayRollArray, $RoleDepartmentFilter);
    }
}




$CancelButton = false;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Единоразовая выплата</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../bower_components/FixedHeader-3.1.4/css/fixedHeader.bootstrap.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">

</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <?php
            if (isset($raw)) {
                ?>
                <div class="box box-solid box-pane" style="overflow-y: auto">
                    <div class="box-header">
                        <h4>Внеплановая выплата</h4>
                    </div>
                    <form class="form-horizontal" method="post">
                        <div class="box-body">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th class="col-sm-1">Дата</th>
                                    <th class="col-sm-1">Направление</th>
                                    <th class="col-sm-1">Отдел</th>
                                    <th class="col-sm-1">Тип выплаты</th>
                                    <th class="col-sm-2">ФИО</th>
                                    <th class="col-sm-1"></th>
                                    <th class="col-sm-1">Статус</th>
                                    <th class="col-sm-2" style="text-align: center;">Сумма(руб)</th>
                                    <th class="col-sm-2">Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($raw)) {
                                    foreach ($raw as $item) {
                                        if (!isset($item['status_name']) || $item['status_name'] == null) {
                                            $item['status_name'] = "Утверждение директоратом";
                                        }
                                        echo "<tr>";
                                        echo "<td>" . $item['date'] . "</td>";
                                        echo "<td>" . $item['direction_name'] . "</td>";
                                        echo "<td>" . $item['department_name'] . "</td>";
                                        switch ($item['type_of_payment']) {
                                            case 4:
                                                echo "<td>" . $item['type_of_payment'] . " Расчет</td>";
                                                break;
                                            case 5:
                                                echo "<td>Внеплановая выплата</td>";
                                                break;
                                        }

                                        echo "<td>" . $item['lastname'] . " " . $item['name'] . " " . $item['middle'] . "</td>";

                                        if ($item['avans']==0)
                                            echo "<td>" . $item['avans']. "</td>";
                                        else
                                            echo "<td>Не выплачивать аванс</td>";

                                        echo "<td>" . $item['status_name'] . "</td>";
                                        echo "<td title='" . $item['note'] . "' style=\"text-align: center;\">" . number_format($item['summ'], 2, ',', ' ') . "</td>";
                                        echo "<td>";
                                        ?>
                                        <div class='btn-group'>
                                            <?php

                                            //                                            if (array_filter($Roles, function ($Role) {
                                            //                                                return $Role->getId() == 3 || $Role->getId() == 5 || $Role->getId() == 6;
                                            //                                            })) {
                                            switch ($item['status_id']) {
                                                case 2:
                                                    /*
                                                     * Переменная для разрешения отображения кнопки следующей стадии
                                                     */
                                                    $NextStage = false;
                                                    /*
                                                     * Переменная для разрешения отображения кнопки предыдущей стадии
                                                     */
                                                    $PrevStage = false;
                                                    /*
                                                     * Пробегаюсь по ролям и проверяю ID
                                                     * Если это отвественный директор или директор или Root, то даю доступ на кнопку сл. стадии
                                                     * Если директор и Root, то показываю кнопку предыдущей стадии
                                                     */
                                                    foreach ($Roles as $Role) {
                                                        if ($Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6) {
                                                            $NextStage = true;
                                                            $PrevStage = true;

                                                        }
                                                    }
                                                    if ($NextStage) {
                                                        echo "<button class='btn btn-sm btn-flat btn-success' name='Success' value=' " . $item['unscheduled_id'] . "'>Подтвердить</button>";
                                                    }

                                                    if ($PrevStage) {
                                                        echo "<button class='btn btn-sm btn-flat btn-danger' style=\"margin-left: 2px;\" name='Cansel' value=' " . $item['unscheduled_id'] . "'>Отменить</button>";
                                                    }
                                                    break;
                                                case 3:
                                                    /*
                                                     * Переменная для разрешения отображения кнопкок
                                                     */
                                                    $NextStage = false;

                                                    /*
                                                    * Переменная для разрешения отображения кнопки предыдущей стадии
                                                    */
                                                    $PrevStage = false;

                                                    foreach ($Roles as $Role) {
                                                        if ($Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6) {
                                                            $PrevStage = true;

                                                        }
                                                    }

                                                    /*
                                                     * Пробегаюсь по ролям и проверяю ID
                                                     * Если это отвественный директор или директор или Root, то даю доступ на кнопку сл. стадии
                                                     * Если директор и Root, то показываю кнопку предыдущей стадии
                                                     */
                                                    foreach ($Roles as $Role) {
                                                        if ($Role->getId() == 5 || $Role->getId() == 6) {
                                                            $NextStage = true;

                                                        }
                                                    }

                                                    if ($NextStage) {
                                                        echo "<button class='btn btn-sm btn-flat btn-success' name='Success' value=' " . $item['unscheduled_id'] . "'>Подтвердить</button>";

                                                    }
                                                    if ($PrevStage) {
                                                        echo "<button class='btn btn-sm btn-flat btn-danger' style=\"margin-left: 2px;\" name='Cansel' value=' " . $item['unscheduled_id'] . "'>Отменить</button>";
                                                    }
                                                    break;
                                                case 4:
                                                    /*
                                                     * Переменная для разрешения отображения кнопкок
                                                     */
                                                    $NextPrevStage = false;

                                                    /*
                                                     * Пробегаюсь по ролям и проверяю ID
                                                     * Если это отвественный директор или директор или Root, то даю доступ на кнопку сл. стадии
                                                     * Если директор и Root, то показываю кнопку предыдущей стадии
                                                     */
                                                    foreach ($Roles as $Role) {
                                                        if ($Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6) {
                                                            $NextPrevStage = true;

                                                        }
                                                    }

                                                    if ($NextPrevStage) {
                                                        echo "<button class='btn btn-sm btn-flat btn-danger' style=\"margin-left: 2px;\" name='Cansel' value=' " . $item['unscheduled_id'] . "'>Отменить</button>";
                                                    }
                                                    break;
                                                case 5:
                                                    /*
                                                    * Переменная для разрешения отображения кнопкок
                                                    */
                                                    $NextPrevStage = false;


                                                    /*
                                                    * Пробегаюсь по ролям и проверяю ID
                                                    * Если это отвественный директор или директор или Root, то даю доступ на кнопку сл. стадии
                                                    * Если директор и Root, то показываю кнопку предыдущей стадии
                                                    */
                                                    foreach ($Roles as $Role) {
                                                        if ($Role->getId() == 5 || $Role->getId() == 6) {
                                                            $NextPrevStage = true;
                                                        }
                                                    }
                                                    if ($NextPrevStage) {
                                                        echo "<button class='btn btn-sm btn-flat btn-success' name='Success' value=' " . $item['unscheduled_id'] . "'>Подтвердить</button>";
                                                    }

                                                    break;

                                            }
                                            //                                            }
                                            ?>
                                        </div>

                                        <?php
                                        echo "</td>";
                                        echo "</tr>";
                                    }
                                }
                                ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                </tr>
                                </tfoot>
                            </table>


                        </div>

                    </form>
                </div>
                <?php
            }
            ?>

            <?php
            if (isset($PayRollArray) && count($PayRollArray) > 0) {
                ?>

                <div class="box box-solid">
                    <div class="box-header">
                        <h4>Расчет сотрудников</h4>
                    </div>
                    <form class="form-horizontal" method="post">
                        <div class="box-body">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th class="col-sm-1">Дата</th>
                                    <th class="col-sm-1">Направление</th>
                                    <th class="col-sm-2">Отдел</th>
                                    <th class="col-sm-1">Тип выплаты</th>
                                    <th class="col-sm-2">ФИО</th>
                                    <th class="col-sm-1">Статус</th>
                                    <th class="col-sm-2" style="text-align: center;">Сумма(руб)</th>
                                    <th class="col-sm-2">Действия</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                // Объявляю переменные для суммирвания колонок
                                $BankSumm = 0; // Итого сумма в банк
                                $ItogoSumm = 0; // Итого сумма
                                $OkladSumm = 0; // Сумма оклада
                                $AvansSumm = 0; // Сумма аванса
                                $OtsutstviyaSumm = 0; // Сумма за отсутсвия
                                $NDFLSumm = 0; // Сумма НДФЛ
                                $ILSumm = 0; // Сумма ИЛ
                                $CommersSumm = 0; // Сумма коммерческой премии
                                $AdmSumm = 0; // Сумма административной премии
                                $YderzSumm = 0;  // Сумма удержаний
                                $CorrectSumm = 0; // Сумма коррекции


                                foreach ($PayRollArray as $key => $item) {
//                                    if ($item->getSumm() > 0) {
                                        $BankSumm += $item->getSumm(); // Суммируем Итого Банк
//                                    };

                                    $ItogoSumm += roundSumm($item->getSumm()); // Суммируем Итого
                                    $OkladSumm += $item->getSalary();  // Суммеруем Оклад
                                    $OtsutstviyaSumm += $item->getAbsences(); // Суммируем отсутствия
                                    $NDFLSumm += $item->getNDFL(); // Суммируем НДФЛ
                                    $ILSumm += $item->getRO(); // Суммируем ИЛ
                                    $CommersSumm += $item->getCommercial(); // Суммируем коммерческую премию
                                    $AdmSumm += $item->getAdministrative(); // Суммируем административную премию
                                    $YderzSumm += $item->getHold(); // Суммирую удержания
                                    $CorrectSumm += $item->getCorrecting(); // Суммирую корректировки

//                                    if ($TypeOfPayment == 1) {
                                    $AvansSumm += $item->getAdvancePayment();
//                                    };

//                                    $status = current(array_filter($rawLeaving, function ($r) use ($key){
//                                       return $r["id"]==$key;
//                                    }));

                                    //                                    $PayRoll->setHold(0); // Удержания
                                    //                                    $PayRoll->setCorrecting(0); // Корректировки

                                    // Выдерение цветом. Если сумма меньше 0, то помечаю желтым
                                    if ($item->getSumm() < 0) {
                                        echo "<tr class='warning'>";
                                    } else {
                                        echo "<tr>";
                                    }

                                    echo "<td>" . $item->getDate() . "</td>";

                                    echo "<td>" . $item->getDirection()['name'] . "</td>"; // Вывожу Направление
                                    echo "<td>" . $item->getDepartment()['name'] . "</td>"; // Вывожу отдел

                                    switch ($item->getTypeOfPayment()) {
                                        case 1:
                                            echo "<td>Премия</td>";
                                            break;
                                        case 2:
                                            echo "<td>Аванс</td>";
                                            break;
                                        case 4:
                                            echo "<td>Расчет</td>";
                                            break;
                                    }


                                    switch ($item->getTypeOfPayment()) { // Вывожу ФИО с ссылкой на персональный расчет
                                        case 1:
                                            echo "<td>";
                                            echo "<a href='premium.php?id=" . $item->getId() . "&date=" . $date . "'>" . $item->getFio() . "</a>";
                                            echo "</td>";
                                            break;
                                        case 2:
                                            echo "<td><a href='salary.php?id=" . $item->getId() . "&date=" . $date . "'>" . $item->getFio() . "</a>";
                                            echo "</td>";
                                            break;
                                        case 4:
                                            echo "<td><a href='leaving_premium.php?id=" . $item->getId() . "'>" . $item->getFio() . "</a>";
                                            echo "</td>";
                                            break;
                                    }

                                    echo "<td>" . $item->getStatusName() . "</td>";

                                    // Вывожу сумму
                                    echo "<td style='text-align: center;'>" . number_format(roundSumm($item->getSumm(), $item), 2, ',', ' ') . "</td>";

                                    echo "<td>";
                                    ?>
                                    <div class='btn-group'>
                                        <?php
                                        switch ($item->getStatusId()) {
                                            case 2:
                                                /*
                                                 * Переменная для разрешения отображения кнопки следующей стадии
                                                 */
                                                $NextStage = false;
                                                /*
                                                 * Переменная для разрешения отображения кнопки предыдущей стадии
                                                 */
                                                $PrevStage = false;
                                                /*
                                                 * Пробегаюсь по ролям и проверяю ID
                                                 * Если это отвественный директор или директор или Root, то даю доступ на кнопку сл. стадии
                                                 * Если директор и Root, то показываю кнопку предыдущей стадии
                                                 */
                                                foreach ($Roles as $Role) {
                                                    if ($Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6) {
                                                        $NextStage = true;
                                                        $PrevStage = true;

                                                    }
                                                }
                                                if ($NextStage) {
                                                    echo "<button class='btn btn-sm btn-flat btn-success' name='SuccessLeaving' value=' " . $key . "'>Подтвердить</button>";
                                                }

                                                if ($PrevStage) {
                                                    echo "<button class='btn btn-sm btn-flat btn-danger' style=\"margin-left: 2px;\" name='СanselLeaving' value=' " . $key . "'>Отменить</button>";
                                                }
                                                break;
                                            case 3:
                                                /*
                                                 * Переменная для разрешения отображения кнопкок
                                                 */
                                                $NextStage = false;

                                                /*
                                                * Переменная для разрешения отображения кнопки предыдущей стадии
                                                */
                                                $PrevStage = false;

                                                foreach ($Roles as $Role) {
                                                    if ($Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6) {
                                                        $PrevStage = true;

                                                    }
                                                }

                                                /*
                                                 * Пробегаюсь по ролям и проверяю ID
                                                 * Если это отвественный директор или директор или Root, то даю доступ на кнопку сл. стадии
                                                 * Если директор и Root, то показываю кнопку предыдущей стадии
                                                 */
                                                foreach ($Roles as $Role) {
                                                    if ($Role->getId() == 5 || $Role->getId() == 6) {
                                                        $NextStage = true;

                                                    }
                                                }

                                                if ($NextStage) {
                                                    echo "<button class='btn btn-sm btn-flat btn-success' name='SuccessLeaving' value=' " . $key . "'>Подтвердить</button>";

                                                }
                                                if ($PrevStage) {
                                                    echo "<button class='btn btn-sm btn-flat btn-danger' style=\"margin-left: 2px;\" name='СanselLeaving' value=' " . $key . "'>Отменить</button>";
                                                }
                                                break;
                                            case 4:
                                                /*
                                                 * Переменная для разрешения отображения кнопкок
                                                 */
                                                $NextPrevStage = false;

                                                /*
                                                 * Пробегаюсь по ролям и проверяю ID
                                                 * Если это отвественный директор или директор или Root, то даю доступ на кнопку сл. стадии
                                                 * Если директор и Root, то показываю кнопку предыдущей стадии
                                                 */
                                                foreach ($Roles as $Role) {
                                                    if ($Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6) {
                                                        $NextPrevStage = true;

                                                    }
                                                }

                                                if ($NextPrevStage) {
                                                    echo "<button class='btn btn-sm btn-flat btn-danger' name='СanselLeaving' value=' " . $key . "'>Отменить</button>";
                                                }
                                                break;
                                            case 5:
                                                /*
                                                * Переменная для разрешения отображения кнопкок
                                                */
                                                $NextPrevStage = false;


                                                /*
                                                * Пробегаюсь по ролям и проверяю ID
                                                * Если это отвественный директор или директор или Root, то даю доступ на кнопку сл. стадии
                                                * Если директор и Root, то показываю кнопку предыдущей стадии
                                                */
                                                foreach ($Roles as $Role) {
                                                    if ($Role->getId() == 5 || $Role->getId() == 6) {
                                                        $NextPrevStage = true;
                                                    }
                                                }
                                                if ($NextPrevStage) {
                                                    echo "<button class='btn btn-sm btn-flat btn-success' name='SuccessLeaving' style=\"margin: 1px;\" value=' " . $key . "'>Подтвердить</button>";
                                                }

                                                break;
                                        }
                                        ?>
                                    </div>

                                    <?php
                                    echo "</td>";
                                    echo "</tr>";
                                }
                                ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <!--  Вывожу все просуммированное выше   -->
                                    <th style="font-size: 14px" colspan="6">Итого:</th>
                                    <th style='text-align: center; font-size: 14px;'><?php echo number_format($ItogoSumm, 2, ',', ' '); ?></th>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                        <div class="box-footer">
                            <div class='btn-group'>
                                <?php
                                //                                switch ($item->getStatusId()) {
                                //                                    case 2:
                                //                                    case 3:
                                //                                    case 5:
                                //                                        echo "<button class='btn btn-sm btn-flat btn-success' name='SuccessLeavingAll'>Подтвердить все</button>";
                                //                                }
                                ?>
                            </div>
                            <div class='btn-group'>
                                <?php
                                //                                switch ($item->getStatusId()) {
                                //                                    case 2:
                                //                                    case 3:
                                //                                    case 4:
                                //                                    case 5:
                                //                                        echo "<button class='btn btn-sm btn-flat btn-danger' name='СanselLeavingAll'>Отменить все</button>";
                                //                                }
                                ?>
                            </div>
                        </div>
                    </form>
                </div>
                <?php
            }
            ?>

            <?php
            if (!isset($PayRollArray) && !isset($raw)) {
                ?>
                <div class="box box-solid">
                    <div class="box-body">
                        <h4>Выплат нет</h4>
                    </div>
                </div>
                <?php
            }
            ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="../../bower_components/FixedHeader-3.1.4/js/dataTables.fixedHeader.js"></script>


<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(function () {
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            format: 'mm.yyyy'
        })
    });

    $(document).ready(function () {
        $('#PayrollTable').DataTable({
            fixedHeader: true,
            paging: false,
            lengthChange: false,
            searching: true,
            ordering: true,
            info: false,
            autoWidth: true
        })
    });
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
