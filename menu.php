<?php

$date_m = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
$Permission_Settings = 0;

$Permission_ImportAWH = 0;
$Permission_ImportNDFL = 0;
$Permission_Import1C = 0;

$Permission_ReportLeaving = 0;
$Permission_ReportTransfer = 0;
$Permission_ReportAWH = 0;
$Permission_ReportMemberAWH = 0;
$Permission_ReportRetetition = 0;
$Permission_ReportCorrecting = 0;
$Permission_FinancialStatement = 0; //Финансовая ведомость (расчет)
$Permission_ReportJournal = 0;

$Permission_GroupDataChanges = 0;
$Permission_UserList = 0;

$NewPrepaid = 0;
$NewPayment = 0;

$Reglament = 0;

$Permission_AWH = 0;

// Проверяю права
array_filter($Roles, function ($Role) use (&$Reglament) {
    switch ($Role->getId()){
        case 2:
        case 4:
        case 5:
        case 6:
            $Reglament = 1;
            break;
        default:
            $Reglament = 0;
    }
    return true;
});

array_filter($Roles, function ($Role) use (&$Permission_AWH) {
//        if ($Role->getId() == 1 || $Role->getId() == 5 || $Role->getId() == 6) {
    if ($Role->getAWHEdit() == 1) {
        $Permission_AWH = 1;
        return true;
    } else {
        return false;
    };
});

// Настройки
array_filter($Roles, function ($Role) use (&$NewPrepaid) {
    if ($Role->getPrepaid() == 1) {
        $NewPrepaid = 1;
        return true;
    };
    return false;
});

array_filter($Roles, function ($Role) use (&$NewPayment) {
    if ($Role->getPayment() == 1) {
        $NewPayment = 1;
        return true;
    };
    return false;
});


// Настройки
array_filter($Roles, function ($Role) use (&$Permission_Settings) {
    if ($Role->getSettings() == 1) {
        $Permission_Settings = 1;
        return true;
    };
    return false;
});

// ************************ ИМПОРТ ДАННЫХ *******************************/
// УРВ
array_filter($Roles, function ($Role) use (&$Permission_ImportAWH) {
    if ($Role->getImportAWH() == 1) {
        $Permission_ImportAWH = 1;
        return true;
    };
    return false;
});

// НДФЛ
array_filter($Roles, function ($Role) use (&$Permission_ImportNDFL) {
    if ($Role->getImportNDFL() == 1) {
        $Permission_ImportNDFL = 1;
        return true;
    };
    return false;
});

// 1С
array_filter($Roles, function ($Role) use (&$Permission_Import1C) {
    if ($Role->getImport1C() == 1) {
        $Permission_Import1C = 1;
        return true;
    };
    return false;
});


// ************************ Отчеты *******************************/

array_filter($Roles, function ($Role) use (&$Permission_ReportJournal) {
    if ($Role->getReportJournal() == 1) {
        $Permission_ReportJournal = 1;
        return true;
    };
    return false;
});

array_filter($Roles, function ($Role) use (&$Permission_ReportLeaving) {
    if ($Role->getReportLeaving() == 1) {
        $Permission_ReportLeaving = 1;
        return true;
    };
    return false;
});

array_filter($Roles, function ($Role) use (&$Permission_ReportTransfer) {
    if ($Role->getReportTransfer() == 1) {
        $Permission_ReportTransfer = 1;
        return true;
    };
    return false;
});

array_filter($Roles, function ($Role) use (&$Permission_ReportAWH) {
    if ($Role->getReportAWH() == 1) {
        $Permission_ReportAWH = 1;
        return true;
    };
    return false;
});

array_filter($Roles, function ($Role) use (&$Permission_ReportMemberAWH) {
    if ($Role->getReportMemberAWH() == 1) {
        $Permission_ReportMemberAWH = 1;
        return true;
    };
    return false;
});

array_filter($Roles, function ($Role) use (&$Permission_ReportRetetition) {
    if ($Role->getReportRetetition() == 1) {
        $Permission_ReportRetetition = 1;
        return true;
    };
    return false;
});

array_filter($Roles, function ($Role) use (&$Permission_ReportCorrecting) {
    if ($Role->getReportCorrecting() == 1) {
        $Permission_ReportCorrecting = 1;
        return true;
    };
    return false;
});


//Финансовая ведомость
array_filter($Roles, function ($Role) use (&$Permission_FinancialStatement) {
    if ($Role->getFinancialStatement() == 1) {
        $Permission_FinancialStatement = 1;
        return true;
    };
    return false;
});

//Групповое внесение данных
array_filter($Roles, function ($Role) use (&$Permission_GroupDataChanges) {
    if ($Role->getId() == 2 || $Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6) {
        $Permission_GroupDataChanges = 1;
        return true;
    };
    return false;
});

//Список всех пользователей
array_filter($Roles, function ($Role) use (&$Permission_UserList) {
    if ($Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6) {
        $Permission_UserList = 1;
        return true;
    };
    return false;
});

?>

<style>
    ul.sidebar-menu > li {
        font-size: 12.5px;
    }

    .sidebar-menu > li > a {
        font-weight: 400 !important;
        color: #285473 !important;
    }

    ul.treeview-menu > li > a {
        font-size: 12px;
        /*color: #27333c!important;*/
        font-weight: 400 !important;
    }

</style>

<header class="main-header">
    <!-- Logo -->
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <?php
    echo '<a href="../" class="logo">';
    echo '<span class="logo-mini">ЗиК</span>';
    echo '<span class="logo-lg" title="Маши киркой пока живой!"><img src="images/ussa_25.png"><b>Зарплата и Кадры</b></span>';
    echo '</a>';
    ?>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <?php
                if (isset($NF) && $NF->getCount($User->getMemberId()) > 0) {
                    ?>
                    <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-danger"><?php echo $NF->getCount($User->getMemberId()); ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header header-sm">У вас <?php echo $NF->getCount($User->getMemberId()); ?> уведомление(ий)</li>
                            <li>
                                <ul class="menu">
                                    <?php
                                    foreach ($NF->getMessages($User->getMemberId()) as $item) {
                                        echo "<li>";
                                        echo "<a href=\"#\">";
                                        echo "<div class=\"pull-left\">";
                                        echo "<i class=\"fa fa-warning\" style='color:white'></i>";
                                        echo "</div>";
                                        echo "<h4>";
                                        echo $item['subject'];
                                        echo "<small><i class=\"fa fa-clock-o\"></i> " . $item['date'] . "</small>";
                                        echo "</h4>";
                                        echo "<p> " . $item['message'] . "</p>";
                                        echo "</a>";
                                        echo "</li>";
                                    }
                                    ?>
                                </ul>
                            </li>
                            <li class="footer"><a href="cleannotify.php">Отметить как прочитанное</a></li>
                        </ul>
                    </li>
                    <?php
                }
                ?>

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php
                        if (file_exists("images/" . $User->getMemberId())) {
                            ?>
                            <img src="images/<?php echo $User->getMemberId(); ?>/photo-160x160.jpg" class="user-image" alt="User Image">
                            <?php
                        } else {
                            echo "<i class='fa fa-user'></i>";
                        }
                        ?>
                        <span class="hidden-xs"><?php echo $User->getFN(); ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?php
                            if ($User->getMemberId() != -1) {
                                if (file_exists("images/" . $User->getMemberId())) {
                                    ?>
                                    <img src="images/<?php echo $User->getMemberId(); ?>/photo-160x160.jpg" class="img-circle" alt="Фото сотрудника">
                                    <?php
                                }
                            }
                            ?>
                            <p>
                                <?php
                                if ($User->getMemberId() != -1) {
                                    echo $User->getFN() . " - " . $User->getPosition();
                                } else {
                                    echo $User->getFN();
                                }
                                ?>
                                <small>Работает с <?php
                                    if (is_null($User->getEmployment())) {
                                        $empdate = explode("-", $User->getEmployment());
                                        echo $empdate[2] . " " . $date_m[$empdate[1] - 1] . " " . $empdate[0] . " г. ";
                                    }
                                    ?></small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!--                        <li class="user-body">-->
                        <!-- /.row -->
                        <!--                        </li>-->
                        <!-- Menu Footer-->
                        <li style="background-color: #3c3a65;" class="user-footer">
                            <div class="pull-left">
                                <?php
                                if ($User->getMemberId() != -1) {
                                    ?>
                                    <a href="profile.php?id=<?php echo $User->getMemberId(); ?>" class="btn btn-default btn-flat btn-sm">Профиль</a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="pull-right">
                                <a href="index.php?exit=1" class="btn btn-default btn-flat btn-sm">Выход</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>


</header>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="height: auto;">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Навигация</li>

            <li><a href="member_list.php"><i class="fa fa-user"></i> <span>Список пользователей</span></a></li>
            <?php
            if ($Permission_UserList == 1) {
                ?>
                <li><a href="userlist.php"><i class="fa fa-list-ol"></i> <span>Список всех пользователей</span></a></li>
                <?php
            }
            ?>

            <?php if ($Permission_ImportAWH == 1 || $Permission_ImportNDFL == 1 || $Permission_Import1C == 1) { ?>
                <li class="treeview"><a><i class="fa fa-cogs"></i><span>Импорт данных</span><span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span></a>
                    <ul class="treeview-menu ">

                <?php if ($Permission_ImportAWH == 1) { ?>
                    <li><a href="awh.php"><i class="fa fa-clock-o"></i> <span>Блок УРВ</span></a></li>
                <?php }
            ; ?>

                <?php if ($Permission_ImportNDFL == 1) { ?>
                    <li><a href="ndfl.php"><i class="fa fa-info"></i> <span>Блок НДФЛ</span></a></li>
                <?php }
            ; ?>

                <?php if ($Permission_Import1C == 1) { ?>
                    <li><a href="data_from_sklad.php"><i class="fa fa-briefcase"></i> <span>Блок данных из 1С</span></a></li>
                <?php }
            ; ?>

            <?php

            ?>
                    </ul> </li>
            <?php
            }
            ?>
            <li class="treeview <?php if (isset($menu_open) && $menu_open==1) echo "menu-open"; ?>">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>Справочники</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu  <?php if (isset($menu_open) && $menu_open==1) echo "menu-open"; ?>"  <?php if (isset($menu_open) && $menu_open==1) echo "style='display:block;'"; ?>>
                    <li><a href="productioncalendar.php"><i class="fa fa-calendar"></i> <span>Производственный календарь</span></a></li>
                    <li><a href="directory_direction.php"><i class="fa fa-group"></i> <span>Направления в компании</span></a></li>
                    <li><a href="directory_department.php"><i class="fa fa-group"></i> <span>Отделы в компании</span></a></li>
                    <li><a href="directory_position.php"><i class="fa fa-group"></i> <span>Должности в компании</span></a></li>
                </ul>

            </li>

            <?php if ($Permission_Settings == 1 || $Permission_AWH == 1) { ?>
                <li class="treeview <?php if (isset($menu_open) && $menu_open==2) echo "menu-open"; ?>">
                    <a href="#">
                        <i class="fa fa-cogs"></i>
                        <span>Настройки</span>
                        <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu <?php if (isset($menu_open) && $menu_open==2) echo "menu-open"; ?>" <?php if (isset($menu_open) && $menu_open==2) echo "style='display:block;'"; ?>>

                        <li><a href="direction.php"><i class="fa fa-cog"></i> <span>Добавление направления</span></a></li>
                        <li><a href="department.php"><i class="fa fa-cog"></i> <span>Добавление отделов(ТТ)</span></a></li>
                        <li><a href="position.php"><i class="fa fa-cog"></i> <span>Добавление должностей</span></a></li>
                        <li><a href="nation.php"><i class="fa fa-cog"></i> <span>Национальность</span></a></li>
                        <li><a href="leaving.php"><i class="fa fa-cog"></i> <span>Причины увольнения</span></a></li>
                        <?php
                        if ($Permission_Settings == 1) {
                        ?>
                            <li><a href="statgroup.php"><i class="fa fa-cog"></i> <span>Статгруппа</span></a></li>
                            <li><a href="motivation.php"><i class="fa fa-cog"></i> <span>Мотивации</span></a></li>
                            <li><a href="roles_settings.php"><i class="fa fa-group"></i> <span>Настройка ролей</span></a></li>
                            <li><a href="directorlist.php"><i class="fa fa-group"></i> <span>Справочник отв. дир. и отд.</span></a></li>
                        <?php
                        }
                        ?>
                    </ul>
                </li>
            <?php }; ?>


            <?php if ($Permission_ReportLeaving == 1 || $Permission_ReportTransfer == 1 || $Permission_ReportAWH == 1 ||
                $Permission_ReportMemberAWH == 1 || $Permission_ReportRetetition == 1 || $Permission_ReportCorrecting == 1 || $Permission_FinancialStatement == 1 || $Permission_ReportJournal == 1) {
                ?>
                <li class="treeview <?php if (isset($menu_open) && $menu_open==3) echo "menu-open"; ?>""><a href="#"><i class="fa fa-cogs"></i><span>Отчеты</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu <?php if (isset($menu_open) && $menu_open==3) echo "menu-open"; ?>" <?php if (isset($menu_open) && $menu_open==3) echo "style='display:block;'"; ?>>
                <?php if ($Permission_ReportLeaving == 1) {
                    ?>
                    <li><a href="employment.php"><i class="fa fa-user"></i> <span>Принятые сотрудники</span></a></li>
                <?php } ?>
                <?php if ($Permission_ReportLeaving == 1) {
                    ?>
                    <li><a href="dismissal.php"><i class="fa fa-user-times"></i> <span>Уволенные сотрудники</span></a></li>
                <?php } ?>
                <?php if ($Permission_ReportLeaving == 1) {
                    ?>
                    <li><a href="probation.php"><i class="fa fa-user-times"></i> <span>Прошедшие испытательный срок</span></a></li>
                <?php } ?>
                <?php if ($Permission_ReportTransfer == 1) { ?>
                    <li><a href="anotherposition.php"><i class="fa fa-exchange"></i> <span>Переводы</span></a></li>
                <?php } ?>

                <?php if ($Permission_ReportLeaving == 1) {
                    ?>
                    <li><a href="usersbydepartmnet.php"><i class="fa fa-user"></i> <span>Пользователи по отделам</span></a></li>
                <?php } ?>
                <?php if ($Permission_ReportJournal == 1) { ?>
                    <li><a href="journal.php"><i class="fa fa-list"></i> <span>Журнал ведомостей</span></a></li>
                <?php } ?>

                <li class="treeview"><a href="#"><i class="fa fa-clock-o"></i><span>УРВ</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <?php if ($Permission_ReportAWH == 1) { ?>
                        <li><a href="journal_awh.php"><i class="fa fa-clock-o"></i> <span>Журнал УРВ + НДФЛ/ИЛ</span></a></li>
                    <?php } ?>

                    <?php if ($Permission_ReportAWH == 1) { ?>
                        <li><a href="vacationbalance.php"><i class="fa fa-clock-o"></i> <span>УРВ Остатки отпуска болезни</span></a></li>
                    <?php } ?>

                    <?php if ($Permission_ReportMemberAWH == 1) { ?>
                        <li><a href="journal_awh_member.php"><i class="fa fa-clock-o"></i> <span>УРВ по сотруднику</span></a></li>
                    <?php } ?>

                    <?php if ($Permission_ReportMemberAWH == 1) { ?>
                        <li><a href="journal_ndfl_member.php"><i class="fa fa-clock-o"></i> <span>НДФЛ/ИЛ по сотруднику</span></a></li>
                    <?php } ?>
                        </ul>
                    <?php if ($Permission_ReportRetetition == 1) { ?>
                        <li><a href="retention_member.php"><i class="fa fa-user"></i> <span>Удержания по сотрудникам</span></a></li>
                    <?php } ?>

                    <?php if ($Permission_ReportCorrecting == 1) { ?>
                        <li><a href="journal_correcting.php"><i class="fa fa-user"></i> <span>Контроль коррект. наценки</span></a></li>
                    <?php } ?>

                    <?php if ($Permission_FinancialStatement == 1) { ?>
                        <li><a href="markup_report.php"><i class="fa fa-money"></i> <span>Отчет по наценке</span></a></li>
                    <?php } ?>

                    <?php if ($Permission_FinancialStatement == 1) { ?>
                        <li><a href="prepaid_expense_vertical.php"><i class="fa fa-money"></i> <span>Вертикальная ведомость</span></a></li>
                    <?php } ?>

                    <?php if ($Permission_ReportLeaving == 1 || $Permission_ReportTransfer == 1 || $Permission_ReportAWH == 1 ||
                    $Permission_ReportMemberAWH == 1 || $Permission_ReportRetetition == 1 || $Permission_ReportCorrecting == 1 || $Permission_FinancialStatement == 1 || $Permission_ReportJournal == 1) {
                        ?>
                    </ul></li>
                    <?php
                } ?>

                <?php if ($Permission_FinancialStatement == 1) { ?>
                    <li class="header">Расчеты</li>
                    <li><a href="prepaid_expense.php"><i class="fa fa-money"></i> <span>Плановые выплаты</span></a></li>

                    <li><a href="payment.php"><i class="fa fa-money"></i> <span>Единоразовая выплата</span></a></li>
                <?php } ?>
                <?php if ($NewPrepaid == 1 || $NewPayment == 1) { ?>
                    <li class="header">Кассиру</li>
                <?php }
            ; ?>
                <?php if ($NewPrepaid == 1) { ?>
                    <li><a href="envelope_cashier.php"><i class="fa fa-envelope"></i><span>Конверты</span></a></li>
                <?php }
            ; ?>
                <?php if ($NewPayment == 1) { ?>
                    <li><a href="payment_cashier.php"><i class="fa fa-envelope"></i><span>Единоразовая выплата</span></a></li>
                <?php }; ?>
                <?php
            };
            ?>
            <?php
            if ($Permission_GroupDataChanges == 1) {
                ?>
                <li class="header">Внесение данных</li>
                <li><a href="groupchanges.php"><i class="fa fa-table"></i><span>Груп. внесение данных</span></a></li>
                <?php
            }
            ?>
            <li class="header">Справочная информация</li>
            <?php
            if ($Reglament == 1) {
                ?>
                <li><a href="reglament.php"><i class="fa fa-info"></i><span>Регламент</span></a></li>
                <?php
            }
            ?>
            <li><a href="help.php"><i class="fa fa-info"></i><span>Техническая документация</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

