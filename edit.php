<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 03.08.18
 * Time: 12:17
 */

// Время работы скрипта
$start = microtime(true);

require_once 'app/DirectionInterface.php';
require_once 'app/DepartmentInterface.php';
require_once 'app/PositionInterface.php';

require_once 'app/MemberInterface.php';
require_once 'app/Member.php';
require_once 'app/PhotoFile.php';
require_once 'app/IntChat.php';
require_once 'app/NationInterface.php';
require_once 'app/LeavingInterface.php';

require_once 'app/MotivationInterface.php';
require_once 'app/StatgroupInterface.php';
require_once 'app/CustomPercent.php';

require_once 'app/ErrorInterface.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/ReportStatus.php';

require_once 'app/Notify.php';

require_once 'app/Kpi.php';
require_once 'app/WindowPercent.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

if($_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest'){
    $MI = new MemberInterface();
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($MI->getWindowManagers(),JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    die();
}

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());


$Permission_ChangeMainInfo = 0; // Изменение основного (фото, почта, тел, мобильные тел., аварийный тел.)
$Permission_ChangeWorkPersonal = 0; // Изменение рабочего и личного
$Permission_ChangeFinanceMotivation = 0; // Изменение фин. условий и мотивации
$Permission_ChangeWorkStatus = 0; // Изменение рабочего статуса
$Permission_Settings = 0; // Настройки, в нашем случае ролей
$Permission_ChangeFinanceProbation = 1; // Изменение комментария в начальных условиях
$is_director = 0; // Проверка на НЛ

$Errors = new ErrorInterface();

if ($_REQUEST['id']) {
    $user_id = $_REQUEST['id'];

    // Если не число то ошибка выходим
    if (!is_numeric($user_id)) {
        header("Location: 404.php");
    }

    // Создаем объект MemberInterface
    $MI = new MemberInterface();

    // Извлекаю данные по сотруднику из БД
    try {
        $member = $MI->fetchMemberByID($user_id);
    } catch (Exception $e) {
        header("Location: 404.php");
//        echo $e->getMessage();
        exit();
    }

    // Нету пользователя или не создался объект пользователя
    if (is_null($member)) {
        header("Location: 404.php");
    }

    /*
    * Проверка на новые сообщения
    */
    $NF = new Notify();

    // ********************************************************* ПРАВА ДОСТУПА пользователя
    array_filter($Roles, function ($Role) use (&$Permission_Settings) {
        if ($Role->getSettings() == 1) {
            $Permission_Settings = 1;
            return true;
        };
        return false;
    });

    $member_department_id = $member->getDepartment()['id'];
    if ($User->getMemberId() != $user_id) {
        if (!array_filter($Roles, function ($Role) use ($member_department_id) {
            return (($Role->getChangeMember() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        })) {
            header("Location: 404.php");
        }
    }

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ChangeWorkPersonal, &$Permission_ChangeMainInfo) {
        if (($Role->getChangeWorkPersonal() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ChangeWorkPersonal = 1; // Можно изменять личные данные и рабочие данные
            $Permission_ChangeMainInfo = 1; // Можно изменять почтовые адреса, телефоны итд
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ChangeFinanceMotivation) {
        if (($Role->getChangeFinanceMotivation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ChangeFinanceMotivation = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ChangeWorkStatus) {
        if (($Role->getChangeWorkStatus() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ChangeWorkStatus = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use (&$is_director) {
        if ($Role->getId() == 5 || $Role->getId() == 6) {
            $is_director = 1;
            return true;
        }
    });

    if ($User->getMemberId() == $user_id) {
        $Permission_ChangeMainInfo = 1;
        /*
         * Пользователь не руководитель не должен менять фин. условия, статус и роль
         */

//        $Permission_ChangeWorkPersonal = 1; // Изменение рабочего и личного
//        $Permission_ChangeFinanceMotivation = 1; // Изменение фин. условий и мотивации
//        $Permission_ChangeWorkStatus = 1; // Изменение рабочего статуса
//        $Permission_Settings = 1; // Настройки, в нашем случае ролей
    }



    // Если сотрудник уволен, то перенаправляем на страницу профиля
//    if ($member->getWorkstatus() == 4) {
//        header('Location: profile.php?id=' . $user_id);
//        exit;
//    }

    $RolesList = $RI->get(); //Получаю список всех ролей
    $RolesUser = $RI->getRoles($user_id); // Получаю список ролей пользователя

    // Создаю объект управления мотивацией и получаю из БД
    $Motivation = new MotivationInterface();
    $Motivation->fetch();

    $KPI = new Kpi();
    $WM = new WindowPercent();

    // Кастомные проценты
    // Если мотивация 1,2,3 то получаем кастомные проценты, иначе они нам не актуальны
    if (($member->getMotivation() == 1) || ($member->getMotivation() == 2) || ($member->getMotivation() == 3) || ($member->getMotivation() == 4) || ($member->getMotivation() == 5) || ($member->getMotivation() == 7) || ($member->getMotivation()==8) ) {
        try {
            $CPT = new CustomPercent();
            $CPT->setMotivation($member->getMotivation());
            $CPT->setBasePercent($member->getBasePercent());
//            $CPT->fetch($member->getId());
            $CPT->fetchv3($member->getId());
        } catch (Exception $e) {
            $Errors->add($e->getMessage());
        }
    }

    /***
     * * * * * * * * *  Изменение данных профиля сотрудника  * * * * * * * * *
     ***/

    // Получение фото из базы чата
    if (isset($_POST['getPhotoFromChat']) && ($Permission_ChangeWorkPersonal == 1)) {
        $IC = new IntChat();
        try {
            $IC->connect();
            $IC->setUserID($user_id);
            if (!is_null($member->getMaidenName())) {
                $IC->getPicture($member->getLastname() . " (" . $member->getMaidenName() . ") " . $member->getName());
            } else {
                $IC->getPicture($member->getLastname() . " " . $member->getName());
            }
            $IC->close();
        } catch (Exception $e) {
            $Errors->add($e->getMessage());
        }
    }

    /*
     *                 Разбор табов
     *
     * * * * * * * * * Таб личное * * * * * * * * *
     */

    if (isset($_POST['personal_button']) && (($Permission_ChangeWorkPersonal == 1) || ($Permission_ChangeMainInfo == 1))) {
        // Если существует и была изменена национальность, то обновляем ее
        if (isset($_POST['nation']) && ($Permission_ChangeWorkPersonal == 1)) {
            if (($member->getNation() != $_POST['nation']) && is_numeric($_POST['nation'])) {
                $MI->ChangeNation($user_id, $_POST['nation'], $User->getMemberId());
            }
        }

        /* ***              Аварийные телефоны              *** */
        // Если существует и были изменены аварийные телефоны, то обновляем их
        if (isset($_POST['emergency_phone_delete']) && is_array($_POST['emergency_phone_delete']) && (($Permission_ChangeWorkPersonal == 1) || ($Permission_ChangeMainInfo == 1))) {
            foreach ($_POST['emergency_phone_delete'] as $value) {
                $value = str_replace(array(")", "-", "("), "", $value);
                if (is_numeric($value)) {
                    try {
                        echo $value . " " . $user_id . "<BR>";
                        $MI->DeleteEmergencyPhone($value, $user_id, $User->getMemberId());
                    } catch (Exception $e) {
                        $Errors->add($e->getMessage());
                    }
                }
            }
        }

        // Если существует и были добавлены аварийные телефоны, то добавляем  их
        if (isset($_POST['emergency_phone_add']) && is_array($_POST['emergency_phone_add']) && (($Permission_ChangeWorkPersonal == 1) || ($Permission_ChangeMainInfo == 1))) {
            foreach ($_POST['emergency_phone_add'] as $value) {
                $value = str_replace(array(")", "-", "("), "", $value);
                if (is_array($value)) {
                    if (is_numeric($value['phone'])) {
                        try {
                            $MI->AddEmergancyPhone($value, $user_id, $User->getMemberId());
                        } catch (Exception $e) {
                            $Errors->add($e->getMessage());
                        }
                    }
                }
            }
        }

        /* ***              Личные мобильные телефоны              *** */
        // Добавляем личный мобильный телефон
        if (isset($_POST['personal_mobile_phone_add']) && is_array($_POST['personal_mobile_phone_add']) && (($Permission_ChangeWorkPersonal == 1) || ($Permission_ChangeMainInfo == 1))) {
            foreach ($_POST['personal_mobile_phone_add'] as $value) {
                $value = str_replace(array(")", "-", "("), "", $value);
                if (is_numeric($value)) {
                    try {
                        $MI->AddMobilePersonalPhone($value, $user_id, $User->getMemberId());
                    } catch (Exception $e) {
                        $Errors->add($e->getMessage());
                    }
                } else {
                    $Errors->add("Не верный формат номера телефона");
                }
            }
        }

        // Удаляем  личный мобильный телефон
        if (isset($_POST['personal_mobile_phone_delete']) && is_array($_POST['personal_mobile_phone_delete']) && (($Permission_ChangeWorkPersonal == 1) || ($Permission_ChangeMainInfo == 1))) {
            foreach ($_POST['personal_mobile_phone_delete'] as $value) {
                $value = str_replace(array(")", "-", "("), "", $value);
                if (is_numeric($value)) {
                    try {
//                        echo $value . " " . $user_id . "<BR>";
                        $MI->DeletePersonalMobilePhone($value, $user_id, $User->getMemberId());
                    } catch (Exception $e) {
                        $Errors->add($e->getMessage());
                    }
                }
            }
        }

        /* ***              Личные домашний телефон              *** */
        // Добавляем личный домашний телефон
        if (isset($_POST['personal_phone_add']) && is_array($_POST['personal_phone_add']) && (($Permission_ChangeWorkPersonal == 1) || ($Permission_ChangeMainInfo == 1))) {
            foreach ($_POST['personal_phone_add'] as $value) {
                $value = str_replace(array(")", "-", "("), "", $value);
                if (is_numeric($value)) {
                    try {
                        $MI->AddPersonalPhone($value, $user_id, $User->getMemberId());
                    } catch (Exception $e) {
                        $Errors->add($e->getMessage());
                    }
                }
            }
        }

        // Удаляем личный домашний телефон
        if (isset($_POST['personal_phone_delete']) && is_array($_POST['personal_phone_delete']) && (($Permission_ChangeWorkPersonal == 1) || ($Permission_ChangeMainInfo == 1))) {
            foreach ($_POST['personal_phone_delete'] as $value) {
                $value = str_replace(array(")", "-", "("), "", $value);
                if (is_numeric($value)) {
                    try {
                        $MI->DeletePersonalPhone($value, $user_id, $User->getMemberId());
                    } catch (Exception $e) {
                        $Errors->add($e->getMessage());
                    }
                }
            }
        }

        /*
        * Меняем пол, ну мало ли 21 век на дворе.
        */
        if (isset($_POST['sex']) && ($member->getSex() != $_POST['sex']) && ($Permission_ChangeWorkPersonal == 1)) {
            if (is_numeric($_POST['sex'])) {
                try {
                    $MI->ChangeSex($user_id, $_POST['sex']);
                } catch (Exception $e) {
                    $Errors->add($e->getMessage());
                }
            }
        }

        /*
         * Меняем дату рождения, а вдруг указали не верно.
         */
        if (isset($_POST['birthdate']) && ($member->getBirthday() != $_POST['birthdate']) && ($Permission_ChangeWorkPersonal == 1)) {
            try {
                $MI->ChangeBirthday($user_id, $_POST['birthdate']);
            } catch (Exception $e) {
                $Errors->add($e->getMessage());
            }
        }
    }

    /*
     *
     * * * * * * * *  Таб работа  * * * * * * * *
     *
     */
    if (isset($_POST['work_button']) && (($Permission_ChangeWorkPersonal == 1) || ($Permission_ChangeMainInfo == 1))) {
        /* ***              Рабочий мобильный телефон              *** */
        // Если существует и были изменены рабочие мобильные телефоны, то добавляем их
        if (isset($_POST['work_mobile_phone_add']) && is_array($_POST['work_mobile_phone_add']) && (($Permission_ChangeWorkPersonal == 1) || ($Permission_ChangeMainInfo == 1))) {
            foreach ($_POST['work_mobile_phone_add'] as $value) {
                $value = str_replace(array(")", "-", "("), "", $value);
                if (is_numeric($value)) {
                    try {
                        $MI->AddMobileWorkPhone($value, $user_id);
                    } catch (Exception $e) {
                        $Errors->add($e->getMessage());
                    }
                }
            }
        }

        /*
         * Изменение даты выхода на работу
         */
        if (isset($_POST['emp_date']) && $_POST['emp_date'] != $member->getEmploymentDate()) {
            $new_emp_date = $_POST['emp_date'];
            $d = DateTime::createFromFormat("d.m.Y", $new_emp_date);
            $old = DateTime::createFromFormat("d.m.Y", $member->getEmploymentDate());
            if ($d && $d->format("d.m.Y") === $new_emp_date) {
                $MI->empDateModify($member->getId(), $old->format("Y-m-d"), $d->format("Y-m-d"));
            }

        }

        // Если существует и были изменены рабочие мобильные телефоны, то удаляем их
        if (isset($_POST['work_mobile_phone_delete']) && is_array($_POST['work_mobile_phone_delete']) && (($Permission_ChangeWorkPersonal == 1) || ($Permission_ChangeMainInfo == 1))) {
            foreach ($_POST['work_mobile_phone_delete'] as $value) {
                $value = str_replace(array(")", "-", "("), "", $value);
                if (is_numeric($value)) {
                    try {
                        $MI->DeleteMobileWorkPhone($value, $user_id, $User->getMemberId());
                    } catch (Exception $e) {
                        $Errors->add($e->getMessage());
                    }
                }
            }
        }

        /* ***              Рабочий телефон              *** */
        // Добавляем рабочий телефон
        if (isset($_POST['work_phone_add']) && is_array($_POST['work_phone_add']) && (($Permission_ChangeWorkPersonal == 1) || ($Permission_ChangeMainInfo == 1))) {
            foreach ($_POST['work_phone_add'] as $value) {
                $value = str_replace(array(")", "-", "("), "", $value);
                if (is_numeric($value)) {
                    try {
                        $MI->AddWorkPhone($value, $user_id, $User->getMemberId());
                    } catch (Exception $e) {
                        $Errors->add($e->getMessage());
                    }
                }
            }
        }

        // Удаляем рабочий телефон
        if (isset($_POST['work_phone_delete']) && is_array($_POST['work_phone_delete']) && (($Permission_ChangeWorkPersonal == 1) || ($Permission_ChangeMainInfo == 1))) {
            foreach ($_POST['work_phone_delete'] as $value) {
                $value = str_replace(array(")", "-", "("), "", $value);
                if (is_numeric($value)) {
                    try {
                        $MI->DeleteWorkPhone($value, $user_id, $User->getMemberId());
                    } catch (Exception $e) {
                        $Errors->add($e->getMessage());
                    }
                }
            }
        }

        // Меняем внутренний телефон
        if (($member->getWorklocalphone() != $_POST['work_local']) && (($Permission_ChangeWorkPersonal == 1) || ($Permission_ChangeMainInfo == 1))) {
            if (is_numeric($_POST['work_local'])) {
                if ($member->getWorklocalphone()['phone'] != $_POST['work_local']) {
                    try {
                        $MI->ChangeWLocalPhone($user_id, $_POST['work_local'], $User->getMemberId());
                    } catch (Exception $e) {
                        $Errors->add("Ошибка изменении рабочего внутреннего номера: " . $e->getMessage());
                    }
                }
            }
        }

        // Сопоставление с именем по 1С Склад
        if (($member->getSkladName() != trim($_POST['sklad'])) && ($Permission_ChangeWorkPersonal == 1)) {
            try {
                $MI->ChangeSklad($user_id, trim($_POST['sklad']));
            } catch (Exception $e) {
                $Errors->add("Ошибка 1C Склад: " . $e->getMessage());
            }
        }

        // Сопоставление с номером по 1С8 ЗиК
        if (($member->getPersonalNumber() != trim($_POST['1szik'])) && ($Permission_ChangeWorkPersonal == 1)) {
            try {
                $MI->ChangeZik($user_id, trim($_POST['1szik']));
            } catch (Exception $e) {
                $Errors->add("Ошибка 1С ЗиК: " . $e->getMessage());
            }
        }

        // Логин на вход в систему
        if (($member->getLogin() != trim($_POST['login'])) && ($Permission_ChangeWorkPersonal == 1)) {
            try {
                $MI->ChangeLogin($user_id, trim($_POST['login']));
            } catch (Exception $e) {
                $Errors->add("Ошибка логина: " . $e->getMessage());
            }
        }

        // Изменение Направление,Отдел,Должность.
        if ((isset($_POST['department']) && isset($_POST['direction']) && isset($_POST['position']) && isset($_POST['work_rekvz'])) && ($Permission_ChangeWorkPersonal == 1)) {
            if (is_numeric($_POST['department']) &&
                is_numeric($_POST['direction']) &&
                is_numeric($_POST['position'])
            ) {
                if ($member->getDirection()['id'] != $_POST['direction'] || $member->getDepartment()['id'] != $_POST['department'] || $member->getPosition()['id'] != $_POST['position'] || $member->getDirection()['date']!=$_POST['work_rekvz']) {
                    try {
                        if ($_POST['work_rekvz'] == $member->getEmploymentDate()) {
                            $MI->updateJobPost($user_id, $_POST['department'], $_POST['direction'], $_POST['position'], $_POST['work_rekvz'], $User->getMemberId());
                        } else {
                            $MI->ChangeJobpost($user_id, $_POST['department'], $_POST['direction'], $_POST['position'], $_POST['work_rekvz'], $User->getMemberId());
                        }
                    } catch (Exception $e) {
                        $Errors->add("Ошибка в изменении рабочих реквизитов: " . $e->getMessage());
                    }
                }
            }
        }
    }

    /*
     * * * * * * * * *  Таб Финансы   * * * * * * * *
     */
    if (isset($_POST['finance_first_motivation'])) {
        $_POST['finance_comment'] = strip_tags($_POST['finance_comment']);
        $_POST['finance_comment'] = htmlspecialchars($_POST['finance_comment']);
        try {
            $MI->AddProbationComment($user_id, $_POST['finance_comment']);
        } catch (Exception $e) {
            $Errors->add("Ошибка при добавлении финансовых условий. " . $e->getMessage());
        }
    }

    if (isset($_POST['finance_percent_button']) && ($Permission_ChangeFinanceMotivation == 1)) {
        if (isset($_POST['member_avans_pay'])) {
            $member_avans_pay_status = 0;

        } else {
            $member_avans_pay_status = 1;
        }

        if ($member->getAvansPay() != $member_avans_pay_status) {
            try {
//                if ($_POST['salary_date']) {
//                    $MI->changeAvansPayWithDate($user_id, $member_avans_pay_status, $User->getMemberId(), $_POST['salary_date']);
//                } else {
                    $MI->changeAvansPay($user_id, $member_avans_pay_status, $User->getMemberId());
//                }
            } catch (Exception $e) {
                $Errors->add("Таб. финансы. Ошибка изменении чекбокса 'Не выплачивать аванс сотруднику'. " . $e->getMessage());
            }
        }
    }

    if (isset($_POST['finance_percent_button']) && isset($_POST['member_avans_procent']) && ($Permission_ChangeFinanceMotivation == 1)) {
        if (is_numeric($_POST['member_avans_procent'])) {
            if ($_POST['member_avans_procent'] >= 0 && $_POST['member_avans_procent'] <= 100) {
                if ($member->getAvansProcent() != $_POST['member_avans_procent']) {
                    try {
//                        if ($_POST['salary_date']) {
//                            $MI->changeAvansProcentWithDate($user_id, $_POST['member_avans_procent'], $User->getMemberId(), $_POST['salary_date']);
//                        } else {
                            $MI->changeAvansProcent($user_id, $_POST['member_avans_procent'], $User->getMemberId());
//                        }
                    } catch (Exception $e) {
                        $Errors->add("Таб. финансы. Ошибка % от оклада для аванса" . $e->getMessage());
                    }
                }
            } else {
                $Errors->add("Аванс. Процент от оклада должен быть от 0 до 100%");
            }
        }
    }

    /*
     * Отчисления в ГБ
     */
    if (isset($_POST['finance_percent_button']) && isset($_POST['YearBonusType']) && isset($_POST['member_yearbonus_pay']) && ($Permission_ChangeFinanceMotivation == 1)) {
        $_POST['member_yearbonus_pay'] = str_replace(",", ".", $_POST['member_yearbonus_pay']); // Мало ли с запятой ввели

        if (is_numeric($_POST['member_yearbonus_pay'])) {
            if ($member->getYearbonusPay() != $_POST['member_yearbonus_pay'] || $member->getYearbonusNote() != $_POST['yearbonus_note']) {
                try {
//                    if (isset($_POST['datepicker8'])) {
//                        $MI->changeYearBonusPayWithDate($user_id, $_POST['YearBonusType'], $_POST['member_yearbonus_pay'], $User->getMemberId(), $_POST['yearbonus_note'], $_POST['datepicker8']);
//                    } else {
                        $MI->changeYearBonusPay($user_id, $_POST['YearBonusType'], $_POST['member_yearbonus_pay'], $User->getMemberId(), $_POST['yearbonus_note']);
//                    }
                } catch (Exception $e) {
                    $Errors->add("Таб. финансы. Ошибка в отчислении в ГБ: " . $e->getMessage());
                }
            }
        }
    }

    /*
     * Оклад
     */
    if ((isset($_POST['finance_percent_button'])) && (isset($_POST['member_salary'])) && (!empty($_POST['member_salary'])) && ($Permission_ChangeFinanceMotivation == 1)) {
        $change_salary_date = $_POST['salary_date'];
        $salary = $_POST['member_salary'];
        if (strpos($salary,",")) {
            $salary = str_replace(",", ".", $salary); // Мало ли с запятой ввели
        }

        if ($member->getSalary() != $salary) {
            if (is_numeric($salary)) {
                try {
//                    if (isset($_POST['salary_date'])) {
//                        $MI->ChangeSalarywithDate($user_id, $salary, $User->getMemberId(), $change_salary_date);
//                    } else {
                        $MI->ChangeSalary($user_id, $salary, $User->getMemberId());
//                    }
                } catch (Exception $e) {
                    $Errors->add("Таб финансы " . $e->getMessage());
                }

            }
        }
    }

    // Изменение базового процента
    if ((isset($_POST['finance_percent_button'])) && (isset($_POST['member_percent'])) && ($Permission_ChangeFinanceMotivation == 1)) {

        $member_percent = $_POST['member_percent'];
        $member_percent = str_replace(",", ".", $member_percent); // Мало ли с запятой ввели

        if ($member->getBasePercent() != $member_percent) {
            try {
//                if (isset($_POST['datepicker5'])) {
//                    $MI->ChangeBaseProcentWithDate($user_id, $member_percent, $_POST['datepicker5'], $User->getMemberId());
//                } else {
                    $MI->ChangeBaseProcent($user_id, $member_percent, $User->getMemberId());
//                }
            } catch (Exception $e) {
                $Errors->add("Таб финансы. " . $e->getMessage());
            }
        }
    }

    // Изменение базового процента
    if ((isset($_POST['finance_percent_button'])) && (isset($_POST['brend_percent'])) && ($Permission_ChangeFinanceMotivation == 1)) {

        $brend_percent = $_POST['brend_percent'];
        $brend_percent = str_replace(",", ".", $brend_percent); // Мало ли с запятой ввели

        if ($member->getBrandPercent() != $brend_percent) {
            try {
                $MI->ChangeBrandProcent($user_id, $brend_percent, $User->getMemberId());
//                }
            } catch (Exception $e) {
                $Errors->add("Таб финансы. " . $e->getMessage());
            }
        }
    }

    // Изменение Административная премия
    if ((isset($_POST['finance_percent_button'])) && (isset($_POST['member_AdministrativePrize'])) && ($Permission_ChangeFinanceMotivation == 1)) {
        if (empty($_POST['member_AdministrativePrize'])) {
            $member_AdministrativePrize = 0;
        } else {
            $member_AdministrativePrize = $_POST['member_AdministrativePrize'];
            $member_AdministrativePrize = str_replace(",", ".", $member_AdministrativePrize); // Мало ли с запятой ввели
        }

        if (empty($_POST['AdministrativePrize_note'])) {
            $AdministrativePrize_note = "";
        } else {
            $AdministrativePrize_note = $_POST['AdministrativePrize_note'];
            $AdministrativePrize_note = str_replace(",", ".", $AdministrativePrize_note); // Мало ли с запятой ввели
        }

        try {
            if (($member->getAdministrativePrize() != $member_AdministrativePrize) || ($member->getAdministrativePrizeNote() != $AdministrativePrize_note)) {
//                if (isset($_POST['datepicker5'])) {
//                    $MI->ChangeAdministrativePrizeWithDate($user_id, $member_AdministrativePrize, $AdministrativePrize_note, $_POST['datepicker5'], $User->getMemberId());
//                } else {
                    $MI->ChangeAdministrativePrize($user_id, $member_AdministrativePrize, $AdministrativePrize_note, $User->getMemberId());
//                }
            }
        } catch (Exception $e) {
            $Errors->add("Таб финансы. " . $e->getMessage());
        }
    }


    // Изменение Мобильная связь
    if ((isset($_POST['finance_percent_button'])) && (isset($_POST['member_MobileCommunication'])) && ($Permission_ChangeFinanceMotivation == 1)) {
        if (empty($_POST['member_MobileCommunication'])) {
            $member_MobileCommunication = 0;
        } else {
            $member_MobileCommunication = $_POST['member_MobileCommunication'];
            $member_MobileCommunication = str_replace(",", ".", $member_MobileCommunication); // Мало ли с запятой ввели
        }

        if (empty($_POST['member_mobile_formula'])) {
            $member_MobileFormula = 0;
        } else {
            $member_MobileFormula = 1;
        }


        try {
            if (($member->getMobileCommunication() != $member_MobileCommunication) || ($member->getMobileFormula() != $member_MobileFormula)) {
                    $MI->ChangeMobileCommunication($user_id, $member_MobileCommunication, $member_MobileFormula, $User->getMemberId());
            }
        } catch (Exception $e) {
            $Errors->add("Таб финансы. " . $e->getMessage());
        }
    }

    // Изменение Транспорт
    if ((isset($_POST['finance_percent_button'])) && (isset($_POST['member_Transport'])) && ($Permission_ChangeFinanceMotivation == 1)) {
        if (empty($_POST['member_Transport'])) {
            $member_Transport = 0;
        } else {
            $member_Transport = $_POST['member_Transport'];
            $member_Transport = str_replace(",", ".", $member_Transport); // Мало ли с запятой ввели
        }

        if (empty($_POST['member_transport_formula'])) {
            $member_TransportFormula = 0;
        } else {
            $member_TransportFormula = 1;
        }


        try {
            if (($member->getTransport() != $member_Transport) || ($member->getTransportFormula() != $member_TransportFormula)) {
                $MI->changeTransport($user_id, $member_Transport, $member_TransportFormula, $User->getMemberId());
            }
        } catch (Exception $e) {
            $Errors->add("Таб финансы. Транспорт. " . $e->getMessage());
        }
    }

    if (isset($_POST['SaveKPI'])) {
        foreach ($_POST["kpi"] as $item) {
//            if ($_POST['datepicker9']) {
//                $KPI->updateWithDate($user_id, $item,$_POST['datepicker9']);
//            } else {
                $KPI->update($user_id, $item, $User->getMemberId());
//            }
        }
    }

    /*
     * * * * * * * * *  Таб Мотивация
     */
    // Изменение мотивации
    if ((isset($_POST['motivation_button'])) && (isset($_POST['motivation_id'])) && ($Permission_ChangeFinanceMotivation == 1)) {
        $motivation_id = $_POST['motivation_id'];

        if (is_numeric($motivation_id) && ($motivation_id != -1)) {
            try {
//                if (isset($_POST['motivation_date'])) {
//                    $MI->ChangeMotivationWithDate($user_id, $motivation_id, $_POST['motivation_date'], $User->getMemberId());
//                } else {
                    $MI->ChangeMotivation($user_id, $motivation_id, $User->getMemberId());
//                }
            } catch (Exception $e) {
                $Errors->add("Таб мотивация " . $e->getMessage());
            }
        }

        // После изменения мотивации получаем новые значения по кастомным процентам
        header('Location: edit.php?id=' . $user_id);
    }

    /*
     * * * * * * * * *  Таб УРВ
     */
    // Изменение не считать УРВ
    if ((isset($_POST['urv_button'])) && ($Permission_ChangeWorkStatus == 1)) {
        if (isset($_POST['not_count_urv'])) {
            $not_count_urv = 1;

        } else {
            $not_count_urv = 0;
        }

        if ($member->getNotCountUrv() != $not_count_urv) {
            try {
                $MI->changeNotCountUrv($user_id, $not_count_urv);
            } catch (Exception $e) {
                $Errors->add("Таб УРВ (Не считать УРВ) " . $e->getMessage());
            }
        }

//        if (isset($_POST['vacation_not_expire'])) {
//            $vacation_not_expire = 1;
//
//        } else {
//            $vacation_not_expire = 0;
//        }
//
//        if ($member->getVacationNotExpireDate() != $vacation_not_expire && isset($_POST['vacation_not_expire_date'])) {
//            try {
//                $MI->changeVacationNotExpire($user_id, $vacation_not_expire, $_POST['vacation_not_expire_date']);
//            } catch (Exception $e) {
//                $Errors->add("Таб мотивация " . $e->getMessage());
//            }
//        }

        if (isset($_POST['vacation_extension_date']) && !empty($_POST['vacation_extension_date'])) {

            try {
                $vacation_extension_date = htmlspecialchars($_POST['vacation_extension_date']);
                $vacation_extension_note = htmlspecialchars($_POST['vacation_extension_note']);

                $MI->changeVacationExtensionDate($user_id, $vacation_extension_date, $vacation_extension_note, $User->getMemberId());
            } catch (Exception $e) {
                $Errors->add("Таб УРВ продление отпуска до" . $e->getMessage());
            }
        }

        // После изменения мотивации получаем новые значения по кастомным процентам
        header('Location: edit.php?id=' . $user_id);
    }

    /*
     * Удаление роли у пользователя
     */
    if (isset($_POST['VacationExtensionDelButton']) && ($Permission_Settings == 1)) {
        /*
         * Проверяю что значение элемента RoleDelButton является числом, если не число то вывожу ошибку
         */
        if (is_numeric($_POST['VacationExtensionDelButton'])) {
            try {
                $MI->deleteVacationExtensionDate($_POST['VacationExtensionDelButton']);
            } catch (Exception $e) {
                $Errors->add($e->getMessage());
            }
            header('Location: edit.php?id=' . $user_id);
        }
    }

    /*
     * * * * * * * * *  Таб Кастомный  процент
     */

    // Изменяем кастомный процент
    if ((isset($_POST['SaveCustomPercent'])) && (isset($_POST['custom_percent'])) && ($Permission_ChangeFinanceMotivation == 1)) {
        $custom_procent_array_edit = $_POST['custom_percent'];

        try {
            $CPT->add2($custom_procent_array_edit, $member->getId(), $User->getMemberId());
        } catch (Exception $e) {
            echo $e->getMessage()."<BR>";
        }

//        foreach ($custom_procent_array_edit as $key => $value) {
//            if (empty($value)) {
//
//                if (!isset($CPT->get()[$key])) {
//                    continue;
//                }
//                $value = 0;
//            }
//
//            $value = str_replace(",", ".", $value);  // Мало ли с запятой ввели
//            if (isset($CPT->get()[$key]) && ($CPT->get()[$key] == $value)) { // Не было изменений
//                continue;
//            } else {
//
//                // Если не было изменеий и == базовому проценту, то не обновляем
////                if (!isset($CPT->get()[$key]) && $custom_procent_array_edit[$key] == $member->getBasePercent()) {
////                    continue;
////                }
//
//                if (is_numeric($value)) {
//                    try {
////                        if ($_POST['datepicker6']) {
////                            $CPT->addWithDate($key, $value, $member->getId(), $User->getMemberId(), $_POST['datepicker6']);
////                        } else {
//                            $CPT->add($key, $value, $member->getId(), $User->getMemberId());
////                        }
//                    } catch (Exception $e) {
//                        $Errors->add("Таб Финансы " . $e->getMessage());
//                    }
//                } else {
//                    $Errors .= "Таб Финансы не число: " . $value . "<BR>";
//                }
//            }
//        }

//        $CPT->fetch($member->getId()); // Обновляем объект кастомных процентов получаем из бд
//        $CPT->fetchv3($member->getId());
    }

    /*
     * Процент окна
     */
    if ((isset($_POST['SaveWindowPercent'])) && ($Permission_ChangeFinanceMotivation == 1)) {
        $WM->delete($user_id);

        foreach ($_POST['window_manager'] as $item) {
            $item['manager_id'] = $user_id;
            $item['admin_id'] = $User->getMemberId();
            $item['created'] = date('Y-m-d H:i');
            $WM->add($item);
        }

        header('Location: edit.php?id=' . $user_id);
    }

    /*
     * ******** Таб роль
     */
    if (isset($_POST['RoleAddButton']) && ($Permission_Settings == 1)) {
        /*
         * Если не существует RoleID, то выводим ошибку
         */
        if (!isset($_POST['RoleId'])) {
            $Errors->add("Не задана роль");
        } else {
            /*
             * Иначе, проверяем, что это число.
             * Если это не число, то вывожу ошибку
             */
            if (!is_numeric($_POST['RoleId'])) {
                $Errors->add("Не верная роль");
            } else {
                /*
                 * Задаю переменную newRoleId
                 */
                $newRoleID = $_POST['RoleId'];

                /*
                 * Проверяю существования RoleDirection
                 * Если этого элемента в массиве $_POST нету, то создаю с нулевым значеним
                 */
                if (!isset($_POST['RoleDirection'])) {
                    $RoleDirection = array(0);
                } else {
                    /*
                     * Иначе в переменную RoleDirection присваиваю значение элемента из $_POST
                     */
                    $RoleDirection = $_POST['RoleDirection'];
                }

                /*
                 * Перебираю массив RoleDirection
                 */
                foreach ($RoleDirection as $DirectionID) {
                    /*
                     * Если каким-то образом выбрали все отделы, то остальной выбор не нужен.
                     */
                    if ($DirectionID == 0) {
                        $RI->addToMember($newRoleID, $DirectionID, $user_id);
                        break;
                    }

                    /*
                     * Если это не число, то мы должны пропустить этот элемент.
                     */
                    if (!is_numeric($DirectionID)) {
                        continue;
                    }

                    /*
                     * Добавляю роли
                     */
                    if ((isset($newRoleID)) && ($newRoleID != 0)) {
                        $RI->addToMember($newRoleID, $DirectionID, $user_id);

                    }
                }
                /*
                 * Обновляю список ролей у пользователя
                 */
                $RolesUser = $RI->getRoles($user_id);
            }
        }
    }

    /*
     * Удаление роли у пользователя
     */
    if (isset($_POST['RoleDelButton']) && ($Permission_Settings == 1)) {
        /*
         * Проверяю что значение элемента RoleDelButton является числом, если не число то вывожу ошибку
         */
        if (is_numeric($_POST['RoleDelButton'])) {
            try {
                /*
                 * Удаляю роль
                 */
                $RI->deleteMemberRole($_POST['RoleDelButton']);
                /*
                 * Обновляю список ролей у пользователя
                 */
                $RolesUser = $RI->getRoles($user_id);
            } catch (Exception $e) {
                $Errors->add($e->getMessage());
            }
        } else {
            $Errors->add("Нету такой роли");
        }
    }

    /*
     * * * * * * * * *  Таб основное
     */
    if (isset($_POST['remove_user']))
    {
        try {
            if ($MI->deleteUser($user_id)) {
                header('Location: /member_list.php');
                exit();
            }
        } catch (Exception $e) {
            $Errors->add("Ошибка при удалении пользователя из БД. " . $e->getMessage());
        }
    }

    if (isset($_POST['main_button'])) {
        // Добавляем фото
        if (($_FILES['photofile']['name']) && ($Permission_ChangeMainInfo == 1)) {
            try {
                $uploadFile = new PhotoFile();
                $uploadFile->setFormName("photofile");
                $uploadFile->setTargetDir("images/" . $user_id . "/");
                $uploadFile->Upload();
            } catch (Exception $e) {
                $Errors->add("Ошибка при загрузки фотографии. " . $e->getMessage());
            }
        }

        // Удаление почтового адреса
        if (isset($_POST['email_delete']) && is_array($_POST['email_delete']) && ($Permission_ChangeMainInfo == 1)) {
            foreach ($_POST['email_delete'] as $value) {
                try {
                    $MI->DeleteEmail($value, $user_id, $User->getMemberId());
                } catch (Exception $e) {
                    $Errors->add($e->getMessage());
                }
            }
        }

        // Добавление почтового адреса
        if (isset($_POST['email_add']) && is_array($_POST['email_add']) && ($Permission_ChangeMainInfo == 1)) {
            foreach ($_POST['email_add'] as $value) {
                if (is_array($value)) {
                    if (!filter_var(trim($value['email']), FILTER_VALIDATE_EMAIL)) { // Проводим валидацию почтового адреса
                        continue;
                    }

                    try {
                        $MI->AddEMail($value, $user_id, $User->getMemberId());
                    } catch (Exception $e) {
                        $Errors->add($e->getMessage());
                    }
                }
            }
        }

        //*****************   Изменение в ФИО  ***********************
        $fullname = array();
        $changes = false;

        // Были ли изменения в ФИО, если да то выставляем в true
        if ((($member->getLastname() != trim($_POST['inputLastName'])) ||
                ($member->getName() != trim($_POST['InputName'])) ||
                ($member->getMiddle() != trim($_POST['inputMiddle'])) ||
                ($member->getMaidenName() != trim($_POST['inputMaiden']))

            ) && ($Permission_ChangeWorkPersonal == 1)) {
            $changes = true;
        }

        // Получаем значения переменных и обрабатываем их

        // Девичья фамилия
        $fullname['maiden'] = trim($_POST['inputMaiden']);

        // Фамилия
        $fullname['lastname'] = trim($_POST['inputLastName']);
        if ($member->getLastname() != trim($_POST['inputLastName']) && !$member->getMaidenName()) {
            $fullname['maiden'] = $member->getLastname();
        }

        // Имя
        $fullname['name'] = trim($_POST['InputName']);

        // Отчество
        $fullname['middle'] = trim($_POST['inputMiddle']);

        // Если изменения true, то обрабатываем их и заносим в БД
        if ($changes && ($Permission_ChangeWorkPersonal == 1)) {
            try {
                $MI->ChangeFullName($user_id, $fullname, $User->getMemberId());
            } catch (Exception $e) {
                $Errors->add($e->getMessage());
            }
        }
    }
    // Таб увольнение, пометить на увольнение
    if (isset($_POST["SetStatusButton"]) && ($Permission_ChangeWorkStatus == 1)) {
        if (isset($_POST['WorkStatus']) && is_numeric($_POST['WorkStatus'])) {
            try {
                switch ($_POST['WorkStatus']) {
                    case 1:
                        /*
                         * Если найдены ведомости со статусом >=3 and < 5 и это не директор, то выходим
                         */

                        if (ReportStatus::getReportStatusLastMonth() && !$is_director) {
                            $Errors->add("Внимание! Зарплатная ведомость на стадии директората. Изменение статуса сотрудника запрещено");
                            break;
                        }
                        $MI->ChangeWorkStatus($user_id, "", $_POST['WorkStatus'], $User->getMemberId());
                        header("Location:edit.php?id=" . $user_id);
                        exit();
                        break;
                    case 2:
                        /*
                         * Если найдены ведомости со статусом >=3 and < 5 и это не директор, то выходим
                         */
                        if (ReportStatus::getReportStatusLastMonth() && !$is_director) {
                            $Errors->add("Внимание! Зарплатная ведомость на стадии директората. Изменение статуса сотрудника запрещено");
                            break;
                        }
                        $MI->ChangeWorkStatus($user_id, $_POST['TextArea2'], $_POST['WorkStatus'], $User->getMemberId());
                        $date = new DateTime();
                        $MI->checkPayout($user_id, $date->format("d.m.Y"));
                        header("Location:edit.php?id=" . $user_id);
                        exit();
                        break;
                    case 3:
                        /*
                         * Если найдены ведомости со статусом >=3 and < 5 и это не директор, то выходим
                         */
                        if (ReportStatus::getReportStatusLastMonth() && !$is_director) {
                            $Errors->add("Внимание! Зарплатная ведомость на стадии директората. Изменение статуса сотрудника запрещено");
                            break;
                        }
                        $MI->ChangeWorkStatus($user_id, $_POST['TextArea3'], $_POST['WorkStatus'], $User->getMemberId(), $_POST['decreedate']);
                        $date = new DateTime();
                        $MI->checkPayout($user_id, $date->format("d.m.Y"));
                        header("Location:edit.php?id=" . $user_id);
                        exit();
                        break;
                    case 4:
                        /*
                         * Если найдены ведомости со статусом >=3 and < 5 и это не директор, то выходим
                         */
                        if (ReportStatus::getReportStatusLastMonth() && !$is_director) {
                            $Errors->add("Внимание! Зарплатная ведомость на стадии директората. Изменение статуса сотрудника запрещено");
                            break;
                        }
                        // ChangeWorkStatus происходит в LeavingByID  !!!
                        ($_POST['leavingdate']) ? $leavingdate = $_POST['leavingdate'] : $leavingdate = date("d.m.Y");
                        try {
                            if ($MI->LeavingByID($user_id, $_POST['leavingid'], $leavingdate, $_POST['TextArea4'], $User->getMemberId())) {
                                $MI->checkPayout($user_id, $leavingdate);
                                $member = $MI->fetchMemberByID($user_id);
                                header("Location:edit.php?id=" . $user_id);
                                exit();
                            }
                        } catch (Exception $e) {
                            $Errors->add($e->getMessage());
                        }
                        break;
                }
            } catch (Exception $e) {
                $Errors->add("Ошибка при увольнении сотрудника." . $e->getMessage());
            }
        }
//        header("Refresh:0");
    }

    // Если вносим изменения нужно вытащить данные снова
    if (isset($_POST['personal_button']) || isset($_POST['work_button']) || isset($_POST['main_button']) || isset($_POST['motivation_button']) || isset($_POST['finance_percent_button']) || isset($_POST['finance_first_motivation'])) {
        $member = null;
        try {
            $member = $MI->fetchMemberByID($user_id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    /*           Конец изменений в профиле пользователя         */

    // Получаем объект национальностей, для списка национальностей
    $Nations = new NationInterface();
    try {
        $Nations->fetchNations();
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    // Получаем объект направлений, для списка направлений
    $Directions = new DirectionInterface();
    try {
        $Directions->fetchDirections();
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    // Получаем объект Отделов, для списка отделов
    try {
        $Departments = new DepartmentInterface(); // Отдел
        $Departments->fetchDepartments();
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    // Получаем объект должностей, для списка должностей
    $Positions = new PositionInterface(); // Должность
    try {
        $Positions->fetchPositions();
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    // Получаю объект увольнений, со списком причин увольнения
    $Leaving = new LeavingInterface();
    $Leaving->fetchReasons();


    $KPI->fetch($member->getId());

    // Если мотивация по стат группам, то получаю эти статгруппы
    if (($member->getMotivation() == 1) || ($member->getMotivation() == 4) || ($member->getMotivation() == 5) || ($member->getMotivation() == 7) || ($member->getMotivation() == 8)) { //Получаю статгруппы
        $ST = new StatgroupInterface();
        $ST->fetch();
    }


} else {
    header("Location: 404.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Корректировка досье</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <!--    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">-->
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Profile Styles -->
    <link rel="stylesheet" href="../../css/profile.css">
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- Select2 -->
    <link rel="stylesheet" href="../../bower_components/select2/dist/css/select2.min.css">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/edit.css">
    <link rel="stylesheet" href="css/main.css">

</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
                Корректировка досье: <?php echo $member->getLastname() . " " . $member->getName(); ?>
            </h4>
            <ol class="breadcrumb">
                <li><a href="member_list.php"><i class="fa fa-dashboard"></i> Список пользователей</a></li>
                <li><a href="profile.php?id=<?php echo $user_id; ?>">Профиль сотрудника</a></li>
                <li class="active">Изменения</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-9">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <form method="post" class="form-horizontal" enctype="multipart/form-data" name="tab1">
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h4>Основное</h4>
                                    </div>
                                    <div class="box-body">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Фотография</label>
                                            <div class="col-sm-5">
                                                <?php
                                                if (is_file("images/" . $user_id . "/photo.jpg")) {
                                                    echo '<img src="images/' . $user_id . '/photo.jpg" style="padding-bottom: 10px;">';
                                                }
                                                if ($Permission_ChangeMainInfo == 1) {
                                                    ?>
                                                    <input type="file" name="photofile" id="InputFile">
                                                    <?php
                                                };
                                                ?>
                                            </div>
                                            <div class="col-sm-5">
                                                <?php if ($Permission_ChangeMainInfo == 1) { ?>
                                                    <input type="submit" name="getPhotoFromChat" class="btn btn-aqua btn-flat" value="Получить фото из чата">
                                                <?php }; ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Фамилия</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="inputLastName" id="inputLastName" placeholder="Фамилия"
                                                       value="<?php echo $member->getLastname(); ?>" autocomplete="off" <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                    echo "readonly";
                                                }; ?>>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Имя</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="InputName" id="inputName" placeholder="Имя" value="<?php echo $member->getName(); ?>"
                                                       autocomplete="off" <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                    echo "readonly";
                                                }; ?>>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Отчество</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="inputMiddle" id="inputMiddle" placeholder="Отчество"
                                                       value="<?php echo $member->getMiddle(); ?>" autocomplete="off" <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                    echo "readonly";
                                                }; ?>>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Девичья фамилия</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="inputMaiden" id="inputMaiden" placeholder="Девичья фамилия"
                                                       value="<?php echo $member->getMaidenname(); ?>" autocomplete="off" <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                    echo "readonly";
                                                }; ?>>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Почтовые адреса</label>
                                            <div class="col-sm-10">
                                                <div class="box box-solid box-default">
                                                    <div class="box-body">
                                                        <fieldset id="emails_form">
                                                            <?php
                                                            if (($member->getEmails() !== null) && count($member->getEmails()) > 0) {
                                                                foreach ($member->getEmails() as $key => $item) {
                                                                    ?>
                                                                    <div id="em_field<?php echo $key; ?>">
                                                                        <div class="form-group">
                                                                            <div class="col-md-5">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-envelope"></i>
                                                                                    </div>

                                                                                    <input type="hidden" id="email_id-<?php echo $key ?>" value="<?php echo $item['id'] ?>">

                                                                                    <input type="text" class="form-control" id="email-<?php echo $key ?>"
                                                                                           value="<?php echo $item['email'] ?>" autocomplete="off" readonly>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-5">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-sticky-note"></i>
                                                                                    </div>

                                                                                    <input type="text" class="form-control" id="email-note-<?php echo $key ?>"
                                                                                           value="<?php echo $item['note'] ?>" autocomplete="off" readonly>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-2">
                                                                                <div class="input-group">
                                                                                    <?php if ($Permission_ChangeMainInfo == 1) {
                                                                                        ?>
                                                                                        <input type="button" class="btn btn-flat btn-aqua"
                                                                                               onclick="document.getElementById('email-<?php echo $key ?>').style.color = 'red'; document.getElementById('email-note-<?php echo $key ?>').style.color = 'red';  document.getElementById('email_id-<?php echo $key ?>').setAttribute('name','email_delete[]'); "
                                                                                               value="Удалить"/>
                                                                                        <?php
                                                                                    };
                                                                                    ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </fieldset>
                                                    </div>

                                                    <div class="box-footer">
                                                        <?php if ($Permission_ChangeMainInfo == 1) {
                                                            ?>
                                                            <button type="button" class="btn btn-flat btn-sm  btn-flat pull-right  btn-aqua" id="add_email">Добавить почтовый
                                                                адрес
                                                            </button>
                                                            <?php
                                                        };
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="box-footer">
                                        <div class="form-group">
                                            <div class="col-sm-12" style="text-align: center">
                                                <?php if ($Permission_ChangeMainInfo == 1 || $Permission_ChangeWorkPersonal == 1) {
                                                    ?>
                                                    <button type="submit" class="btn btn-primary btn-flat" name="main_button" style="width: 140px;">Обновить
                                                    </button>
                                                    <?php
                                                }
                                                if ($User->getMemberId() == -1)
                                                {
                                                    ?>
                                                    <button type="submit" class="btn btn-danger btn-flat" name="remove_user" onclick="if (!confirm('Внимание! Опасная операция. Выполнять только если знаешь что делаешь!')) return false;" style="width: 140px;">Удалить пользователя</button>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <!--                            Личные данные-->
                        <?php
                        if ($Permission_ChangeWorkPersonal == 1 || ($User->getMemberId() == $user_id)) {
                            ?>
                            <div class="tab-pane" id="tab_2">
                                <form method="post" class="form-horizontal" name="tab2">
                                    <div class="box box-solid">
                                        <div class="box-header">
                                            <h4>Личное</h4>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Пол</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="sex" id="sex" <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                        echo "disabled";
                                                    }; ?> >
                                                        <option value="-1">Не определился</option>
                                                        <option <?php echo ($member->getSex() == 0) ? "selected" : ''; ?> value="0">Мужской</option>
                                                        <option <?php echo ($member->getSex() == 1) ? "selected" : ''; ?> value="1">Женский</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Дата
                                                    рождения</label>
                                                <div class="col-sm-10">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" class="form-control pull-right"
                                                               id="datepicker" name="birthdate"
                                                               data-inputmask="'alias': 'dd.mm.yyyy'" data-mask
                                                               value="<?php echo $member->getBirthday(); ?>" <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                            echo "disabled";
                                                        }; ?> >
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Национальность </label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="nation" id="nation" <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                        echo "disabled";
                                                    }; ?>>
                                                        <?php
                                                        foreach ($Nations->GetNations() as $nation) {
                                                            if ($nation->getId() == $member->getNation()) {
                                                                echo '<option selected value="' . $nation->getId() . '">' . $nation->getName() . '</option>';
                                                            } else {
                                                                echo '<option value="' . $nation->getId() . '">' . $nation->getName() . '</option>';
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Телефон</label>
                                                <div class="col-md-10">
                                                    <div class="box box-solid box-info">
                                                        <div class="box-body">
                                                            <fieldset id="phone_form">
                                                                <?php
                                                                if ($member->getPersonalphone() !== null  && count($member->getPersonalphone()) > 0) {
                                                                    foreach ($member->getPersonalphone() as $key => $item) {
                                                                        ?>

                                                                        <div id="field<?php echo $key; ?>">
                                                                            <div class="form-group">
                                                                                <div class="col-md-10">
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon">
                                                                                            <i class="fa fa-phone"></i>
                                                                                        </div>

                                                                                        <input type="text" class="form-control" id="personal_phone-<?php echo $key ?>"
                                                                                               data-inputmask='"mask": "(999)999-99-99"' data-mask
                                                                                               value="<?php echo $item['phone'] ?>"
                                                                                               readonly>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-2">
                                                                                    <div class="input-group">
                                                                                        <?php if ($Permission_ChangeMainInfo == 1) {
                                                                                            ?>
                                                                                            <input type="button" class="btn btn-flat btn-aqua"
                                                                                                   onclick="document.getElementById('personal_phone-<?php echo $key ?>').readOnly = true; document.getElementById('personal_phone-<?php echo $key ?>').style.color = 'red'; document.getElementById('personal_phone-<?php echo $key ?>').setAttribute('name','personal_phone_delete[]'); "
                                                                                                   value="Удалить">
                                                                                            <?php
                                                                                        };
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <?php
                                                                    }
                                                                };
                                                                ?>
                                                            </fieldset>
                                                        </div>

                                                        <div class="box-footer">
                                                            <?php if ($Permission_ChangeMainInfo == 1) { ?>
                                                                <button type="button" class="btn btn-flat btn-sm  btn-flat pull-right btn-aqua" id="add_presonal_phone">Добавить
                                                                    телефон
                                                                </button>
                                                            <?php }; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Мобильные телефон</label>
                                                <div class="col-md-10">
                                                    <div class="box box-solid box-info">
                                                        <div class="box-body">
                                                            <fieldset id="phone_mobile_form">
                                                                <?php
                                                                if ($member->getPersonalmobilephone()!==null && count($member->getPersonalmobilephone()) > 0) {
                                                                    foreach ($member->getPersonalmobilephone() as $key => $item) {
                                                                        ?>
                                                                        <div id="field<?php echo $key; ?>">
                                                                            <div class="form-group">
                                                                                <div class="col-md-10">
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon">
                                                                                            <i class="fa fa-phone"></i>
                                                                                        </div>
                                                                                        <input type="text" class="form-control" id="personal_mobile_phone-<?php echo $key ?>"
                                                                                               data-inputmask='"mask": "(999)999-99-99"' data-mask
                                                                                               value="<?php echo $item['phone'] ?>"
                                                                                               readonly>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-2">
                                                                                    <div class="input-group">
                                                                                        <?php if ($Permission_ChangeMainInfo == 1) { ?>
                                                                                            <input type="button" class="btn btn-flat btn-aqua"
                                                                                                   onclick="document.getElementById('personal_mobile_phone-<?php echo $key ?>').readOnly = true; document.getElementById('personal_mobile_phone-<?php echo $key ?>').style.color = 'red'; document.getElementById('personal_mobile_phone-<?php echo $key ?>').setAttribute('name','personal_mobile_phone_delete[]'); "
                                                                                                   value="Удалить">
                                                                                        <?php }; ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </fieldset>
                                                        </div>

                                                        <div class="box-footer">
                                                            <?php if ($Permission_ChangeMainInfo == 1) { ?>
                                                                <button type="button" class="btn btn-flat  btn-sm btn-flat pull-right  btn-aqua" id="add_presonal_mobile_phone">
                                                                    Добавить
                                                                    телефон
                                                                </button>
                                                            <?php }; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Аварийные телефон</label>
                                                <div class="col-md-10">
                                                    <div class="box box-solid box-info">
                                                        <div class="box-body">
                                                            <fieldset id="emergency_phone_form">
                                                                <?php
                                                                if ($member->getEmergencyphone() !== null && count($member->getEmergencyphone()) > 0) {
                                                                    foreach ($member->getEmergencyphone() as $key => $item) {
                                                                        ?>
                                                                        <div id="em_field<?php echo $key; ?>">
                                                                            <div class="form-group">
                                                                                <div class="col-md-5">
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon">
                                                                                            <i class="fa fa-phone"></i>
                                                                                        </div>

                                                                                        <input type="text" class="form-control" id="emergency_phone-<?php echo $key ?>"
                                                                                               name="emergency[<?php echo $key ?>][phone]"
                                                                                               data-inputmask='"mask": "(999)999-9999"' data-mask
                                                                                               value="<?php echo $item['phone'] ?>"
                                                                                               readonly <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                                                            echo "disabled";
                                                                                        }; ?>>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-5">
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon">
                                                                                            <i class="fa fa-user"></i>
                                                                                        </div>

                                                                                        <input type="text" class="form-control" id="emergency_name-<?php echo $key ?>"
                                                                                               name="emergency[<?php echo $key ?>][name]" value="<?php echo $item['note'] ?>"
                                                                                               readonly>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-2">
                                                                                    <div class="input-group">
                                                                                        <?php if ($Permission_ChangeMainInfo == 1) { ?>
                                                                                            <input type="button" class="btn btn-flat btn-aqua"
                                                                                                   onclick="document.getElementById('emergency_phone-<?php echo $key ?>').style.color = 'red'; document.getElementById('emergency_name-<?php echo $key ?>').style.color = 'red';  document.getElementById('emergency_phone-<?php echo $key ?>').setAttribute('name','emergency_phone_delete[]'); "
                                                                                                   value="Удалить">
                                                                                        <?php }; ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </fieldset>
                                                        </div>

                                                        <div class="box-footer">
                                                            <?php if ($Permission_ChangeMainInfo == 1) { ?>
                                                                <button type="button" class="btn btn-flat btn-sm  btn-flat pull-right  btn-aqua" id="add_emergency_phone">
                                                                    Добавить
                                                                    телефон
                                                                </button>
                                                            <?php }; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <div class="form-group">
                                                <div class="col-md-4 col-md-push-4">
                                                    <?php if (($Permission_ChangeWorkPersonal == 1) || ($Permission_ChangeMainInfo == 1)) { ?>
                                                        <button type="submit" name="personal_button" class="btn btn-block btn-primary center-block btn-flat"
                                                                style="width: 140px;">
                                                            Обновить
                                                        </button>
                                                    <?php }; ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <?php
                        }
                        ?>

                        <!--                            Работа-->
                        <?php
                        if ($Permission_ChangeWorkPersonal == 1 || ($User->getMemberId() == $user_id)) {
                            ?>
                            <div class="tab-pane" id="tab_3">
                                <form method="post" class="form-horizontal" name="tab3">
                                    <div class="box box-solid">
                                        <div class="box-header">
                                            <h4>Работа</h4>
                                        </div>
                                        <div class="box-body">
                                            <div class="box box-solid box-info">
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Дата выхода на работу</label>
                                                        <div class="col-sm-10">
                                                            <div class="input-group date">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <input type="text" class="form-control pull-right"
                                                                       id="datepicker10" name="emp_date"
                                                                       data-inputmask="'alias': 'dd.mm.yyyy'" data-mask value="<?php echo $member->getEmploymentDate(); ?>"
                                                                    <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                                        echo "disabled";
                                                                    }; ?> >
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="box box-solid box-info">
                                                <div class="box-body">

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Дата изменения</label>
                                                        <div class="col-sm-10">
                                                            <div class="input-group date">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <input type="text" class="form-control pull-right"
                                                                       id="datepicker3" name="work_rekvz"
                                                                       data-inputmask="'alias': 'dd.mm.yyyy'" data-mask value="<?= $member->getDirection()['date']; ?>"
                                                                    <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                                        echo "disabled";
                                                                    }; ?> >
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Направление</label>
                                                        <div class="col-sm-10">
                                                            <select name="direction" class="form-control" <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                                echo "disabled";
                                                            }; ?> >
                                                                <option value="0">Не выбрано</option>
                                                                <?php
                                                                try {
                                                                    foreach ($Directions->GetDirections() as $value) {
                                                                        if ($member->getDirection()['id'] == $value->GetID()) {
                                                                            echo "<option value='" . $value->GetID() . "' selected >" . $value->GetName() . "</option>";
                                                                        } else {
                                                                            echo "<option value='" . $value->GetID() . "' >" . $value->GetName() . "</option>";
                                                                        }
                                                                    }
                                                                } catch (Exception $e) {
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Отдел</label>
                                                        <div class="col-sm-10">
                                                            <select name="department" class="form-control" <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                                echo "disabled";
                                                            }; ?> >
                                                                <option value="0">Не выбрано</option>
                                                                <?php
                                                                try {
                                                                    foreach ($Departments->GetDepartments() as $value) {

                                                                        foreach ($value->getDirectionId() as $d) {
                                                                            if ($member->getDirection()['id'] == $d && $member->getDepartment()['id'] == $value->GetID()) {
                                                                                echo "<option class='" . $d . "' value=\"" . $value->GetID() . "\" selected >" . $value->GetName() . "</option>";
                                                                            } else {
                                                                                echo "<option class='" . $d . "' value=\"" . $value->GetID() . "\">" . $value->GetName() . "</option>";
                                                                            }
                                                                        }
                                                                    }
                                                                } catch (Exception $e) {
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Должность</label>
                                                        <div class="col-sm-10">
                                                            <select name="position" class="form-control" <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                                echo "disabled";
                                                            }; ?> >
                                                                <option value="0">Не выбрано</option>
                                                                <?php
                                                                try {
                                                                    foreach ($Positions->GetPositions() as $value) {

                                                                        foreach ($value->getDepartmentID() as $d) {
                                                                            if ($member->getDepartment()['id'] == $d && $member->getPosition()['id'] == $value->GetID()) {
                                                                                echo "<option class='" . $d . "' value=\"" . $value->GetID() . "\" selected >" . $value->GetName() . "</option>";
                                                                            } else {
                                                                                echo "<option class='" . $d . "' value=\"" . $value->GetID() . "\">" . $value->GetName() . "</option>";
                                                                            };
                                                                        }
                                                                    }
                                                                } catch (Exception $e) {
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">1С Склад</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="sklad"
                                                           value="<?php echo $member->getSkladName(); ?>" <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                        echo "disabled";
                                                    }; ?> >
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">1C ЗиК</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="1szik"
                                                           value="<?php echo $member->getPersonalnumber(); ?>" <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                        echo "disabled";
                                                    }; ?> >
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Логин</label>
                                                <div class="col-sm-10">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                        <input type="text" class="form-control" name="login"
                                                               value="<?php echo $member->getLogin(); ?>" <?php if ($Permission_ChangeWorkPersonal == 0) {
                                                            echo "disabled";
                                                        }; ?> >
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Телефон</label>
                                                <div class="col-sm-10">
                                                    <div class="box box-solid box-info">
                                                        <div class="box-body">
                                                            <fieldset id="work_phone_form">
                                                                <?php
                                                                if ($member->getWorkphone()!==null && count($member->getWorkphone()) > 0) {
                                                                    foreach ($member->getWorkphone() as $key => $item) {
                                                                        ?>
                                                                        <div id="field<?php echo $key; ?>">
                                                                            <div class="form-group">
                                                                                <div class="col-md-10">
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon">
                                                                                            <i class="fa fa-phone"></i>
                                                                                        </div>

                                                                                        <input type="text" class="form-control" id="work_phone-<?php echo $key ?>"
                                                                                               data-inputmask='"mask": "(999)999-99-99"' data-mask
                                                                                               value="<?php echo $item['phone'] ?>"
                                                                                               readonly>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-2">
                                                                                    <div class="input-group">
                                                                                        <?php if ($Permission_ChangeMainInfo == 1) {
                                                                                            ?>
                                                                                            <input type="button" class="btn btn-flat btn-aqua"
                                                                                                   onclick="document.getElementById('work_phone-<?php echo $key ?>').readOnly = true; document.getElementById('work_phone-<?php echo $key ?>').style.color = 'red'; document.getElementById('work_phone-<?php echo $key ?>').setAttribute('name','work_phone_delete[]'); "
                                                                                                   value="Удалить"/>
                                                                                            <?php
                                                                                        };
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </fieldset>
                                                        </div>

                                                        <div class="box-footer">
                                                            <?php
                                                            if ($Permission_ChangeMainInfo == 1) {
                                                                ?>
                                                                <button type="button" class="btn btn-flat  btn-sm btn-flat pull-right  btn-aqua" id="add_work_phone">Добавить
                                                                    телефон
                                                                </button>
                                                                <?php
                                                            };
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Мобильный телефон</label>
                                                <div class="col-sm-10">
                                                    <div class="box box-solid box-info">
                                                        <div class="box-body">
                                                            <fieldset id="work_mobile_phone_form">
                                                                <?php
                                                                if ($member->getWorkmobilephone()!==null && count($member->getWorkmobilephone()) > 0) {
                                                                    foreach ($member->getWorkmobilephone() as $key => $item) {
                                                                        ?>
                                                                        <div id="field<?php echo $key; ?>">
                                                                            <div class="form-group">
                                                                                <div class="col-md-10">
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon">
                                                                                            <i class="fa fa-phone"></i>
                                                                                        </div>

                                                                                        <input type="text" class="form-control" id="work_mobile_phone-<?php echo $key ?>"
                                                                                               data-inputmask='"mask": "(999)999-99-99"' data-mask
                                                                                               value="<?php echo $item['phone'] ?>"
                                                                                               readonly>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-2">
                                                                                    <div class="input-group">
                                                                                        <?php if ($Permission_ChangeMainInfo == 1) {
                                                                                            ?>
                                                                                            <input type="button" class="btn btn-flat btn-aqua"
                                                                                                   onclick="document.getElementById('work_mobile_phone-<?php echo $key ?>').readOnly = true; document.getElementById('work_mobile_phone-<?php echo $key ?>').style.color = 'red'; document.getElementById('work_mobile_phone-<?php echo $key ?>').setAttribute('name','work_mobile_phone_delete[]'); "
                                                                                                   value="Удалить"/>
                                                                                            <?php
                                                                                        };
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </fieldset>
                                                        </div>

                                                        <div class="box-footer">
                                                            <?php if ($Permission_ChangeMainInfo == 1) {
                                                                ?>
                                                                <button type="button" class="btn btn-flat btn-sm  btn-flat pull-right  btn-aqua" id="add_work_mobile_phone">Добавить
                                                                    телефон
                                                                </button>
                                                                <?php
                                                            };
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Внутренний
                                                    телефон</label>
                                                <div class="col-sm-10">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-phone"></i>
                                                        </div>
                                                        <input type="text" class="form-control" name="work_local" maxlength=4
                                                               data-inputmask='"mask": "9999"' data-mask
                                                               value="<?= (isset($member->getWorklocalphone()['phone'])?$member->getWorklocalphone()['phone']:'')?>" <?php if (($Permission_ChangeMainInfo == 0) && ($Permission_ChangeWorkPersonal == 0)) {
                                                            echo "disabled";
                                                        }; ?> >
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <?php if (($Permission_ChangeMainInfo == 1) || ($Permission_ChangeWorkPersonal == 1)) {
                                                        ?>
                                                        <button type="submit" onclick="if (w()==false) {return false};" class="btn btn-block btn-primary btn-flat center-block" style="width: 140px;" name="work_button">
                                                            Обновить
                                                        </button>
                                                        <?php
                                                    };
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <?php
                        }
                        ?>

                        <!--                            Финансы-->
                        <div class="tab-pane" name="tab_4" id="tab_4">
                            <form method="post" class="form-horizontal" name="tab4">
                                <?php
                                if ($Permission_ChangeFinanceProbation == 1) {
                                    ?>
                                    <div class="box box-solid">
                                        <div class="box-header">
                                            <h4>Финансовые условия на испытательный срок</h4>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Комментарий</label>
                                                <div class="col-sm-10">
                                                <textarea name="finance_comment" id="Comment" cols="1" rows="1" class="form-control"
                                                    <?php
                                                    if (!array_filter($Roles, function ($Role) {
                                                        return $Role->getId() == 1 || $Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6;
                                                    })) {
                                                        echo "readonly";
                                                    };
                                                    ?>
                                                          placeholder="В этом поле нужно описать начальные финансовые условия"><?php echo $member->getProbation(); ?></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="box-footer">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (array_filter($Roles, function ($Role) {
                                                        return $Role->getId() == 1 || $Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6;
                                                    })) {
                                                        ?>
                                                        <button type="submit" class="btn btn-block btn-primary btn-flat center-block" style="width: 140px;"
                                                                name="finance_first_motivation">
                                                            Обновить
                                                        </button>
                                                    <?php }; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>


                                <?php
                                if ($Permission_ChangeFinanceMotivation == 1) {
                                    ?>
                                    <div class="box box-solid">
                                        <div class="box-header">
                                            <h4>Финансовые условия</h4>
                                        </div>
                                        <div class="box-body">

                                            <div class="box box-solid box-primary">
                                                <div class="box-body">

<!--                                                    <div class="form-group">-->
<!--                                                        <label class="col-sm-4 control-label">Дата изменения</label>-->
<!--                                                        <div class="col-sm-8">-->
<!--                                                            <div class="input-group date">-->
<!--                                                                <div class="input-group-addon">-->
<!--                                                                    <i class="fa fa-calendar"></i>-->
<!--                                                                </div>-->
<!--                                                                <input type="text" class="form-control pull-right"-->
<!--                                                                       id="datepicker4" name="salary_date"-->
<!--                                                                       data-inputmask="'alias': 'dd.mm.yyyy'" data-mask value="--><?php //echo date("d.m.Y"); ?><!--"-->
<!--                                                                    --><?php //if ($Permission_ChangeFinanceMotivation == 0) {
//                                                                        echo "disabled";
//                                                                    }; ?><!-- >-->
<!--                                                            </div>-->
<!--                                                        </div>-->
<!--                                                    </div>-->

                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Оклад</label>
                                                        <div class="col-sm-8">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-ruble"></i>
                                                                </div>
                                                                <input type="text" name="member_salary" placeholder="0" value="<?php if ($member->getSalary()) {
                                                                    echo $member->getSalary();
                                                                }; ?>" class="form-control pull-right" <?php if ($Permission_ChangeFinanceMotivation != 1) {
                                                                    echo "disabled";
                                                                } ?> >
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Аванс (Сколько процентов от оклада)</label>

                                                        <div class="col-sm-8">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    %
                                                                </div>
                                                                <input type="text" name="member_avans_procent" placeholder="0" class="form-control pull-right"
                                                                       value="<?php if ($member->getAvansProcent()) {
                                                                           echo $member->getAvansProcent();
                                                                       }; ?>"
                                                                    <?php if ($Permission_ChangeFinanceMotivation != 1 || $member->getAvansPay() == 0) {
                                                                        echo "disabled";
                                                                    } ?>
                                                                >
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Не выплачивать аванс сотруднику</label>
                                                        <div class="col-sm-8">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input name="member_avans_pay" type="checkbox" <?php if ($member->getAvansPay() == 0) echo "checked"; ?> >
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="box box-solid box-primary">
                                                <div class="box-body">
                                                    <h5>Отчисления в годовой бонус</h5>

<!--                                                    <div class="form-group">-->
<!--                                                        <label class="col-sm-4 control-label">Дата изменения</label>-->
<!--                                                        <div class="col-sm-8">-->
<!--                                                            <div class="input-group date">-->
<!--                                                                <div class="input-group-addon">-->
<!--                                                                    <i class="fa fa-calendar"></i>-->
<!--                                                                </div>-->
<!--                                                                <input type="text" class="form-control pull-right"-->
<!--                                                                       id="datepicker8" name="datepicker8"-->
<!--                                                                       data-inputmask="'alias': 'dd.mm.yyyy'" data-mask value="--><?php //echo date("d.m.Y"); ?><!--"-->
<!--                                                                    --><?php //if ($Permission_ChangeFinanceMotivation == 0) {
//                                                                        echo "disabled";
//                                                                    }; ?><!-- >-->
<!--                                                            </div>-->
<!--                                                        </div>-->
<!--                                                    </div>-->

                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Тип отчислений</label>
                                                        <div class="col-sm-8">
                                                            <select name="YearBonusType" class="form-control">
                                                                <option value="0">Не выбрано</option>
                                                                <option value="1" <?php if ($member->getYearbonusType() == 1) {
                                                                    echo "selected";
                                                                } ?> >Константа
                                                                </option>
                                                                <option value="2" <?php if ($member->getYearbonusType() == 2) {
                                                                    echo "selected";
                                                                } ?> >Процент (поставки с апр. по нояб.)
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <!--                                                <div class="form-group">-->
                                                    <!--                                                    <label class="col-sm-4 control-label">Сумма отчислений</label>-->
                                                    <!--                                                    <div class="col-sm-8">-->
                                                    <!--                                                        <div class="input-group">-->
                                                    <!--                                                            <div class="input-group-addon">-->
                                                    <!--                                                                <i id="yearbonuslabel" class="fa --><?php //if ($member->getYearbonusType() == 2) {
                                                    //                                                                    echo 'fa-percent';
                                                    //                                                                } else {
                                                    //                                                                    echo 'fa-ruble';
                                                    //                                                                }; ?><!-- "></i>-->
                                                    <!--                                                            </div>-->
                                                    <!--                                                            <input type="text" name="member_yearbonus_pay" placeholder="0" value="--><?php //if ($member->getYearbonusPay()) {
                                                    //                                                                echo $member->getYearbonusPay();
                                                    //                                                            }; ?><!--" class="form-control pull-right" --><?php //if ($Permission_ChangeFinanceMotivation != 1) {
                                                    //                                                                echo "disabled";
                                                    //                                                            } ?><!-- >-->
                                                    <!--                                                        </div>-->
                                                    <!--                                                    </div>-->
                                                    <!--                                                </div>-->

                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Сумма отчислений</label>
                                                        <div class="col-md-8">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i id="yearbonuslabel" class="fa <?php if ($member->getYearbonusType() == 2) {
                                                                        echo 'fa-percent';
                                                                    } else {
                                                                        echo 'fa-ruble';
                                                                    }; ?> "></i>
                                                                </div>

                                                                <input type="text"
                                                                       name="member_yearbonus_pay"
                                                                       id="member_yearbonus_pay_<?php echo $member->getId(); ?>"
                                                                       placeholder="0"
                                                                       value="<?php if ($member->getYearbonusPay()) {
                                                                           echo $member->getYearbonusPay();
                                                                       }; ?>"
                                                                       class="form-control pull-right"
                                                                    <?php if ($Permission_ChangeFinanceMotivation != 1) {
                                                                        echo "disabled";
                                                                    } ?>
                                                                       autocomplete="off">

                                                                <div class="input-group-addon btn btn-flat"
                                                                     name="member_yearbonus_pay"
                                                                     id='btn_comment_<?php echo $member->getId(); ?>'>
                                                                    <i class="fa fa-comment"></i>
                                                                </div>

                                                                <textarea name='member_yearbonus_pay_note'
                                                                          id='member_yearbonus_pay_note_<?php echo $member->getId(); ?>'
                                                                          cols='1'
                                                                          rows='1'
                                                                          class='form-control' <?php if ($Permission_ChangeFinanceMotivation == 0) {
                                                                    echo "disabled";
                                                                }; ?> ><?php echo $member->getYearbonusNote(); ?></textarea>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!--                                            <div class="form-group">-->
                                                    <!--                                                <label class="col-sm-4 control-label">Комментарий</label>-->
                                                    <!---->
                                                    <!--                                                <div class="col-md-8">-->
                                                    <!--                                                    <div class="input-group">-->
                                                    <!---->
                                                    <!--                                                        <div class="input-group-addon btn btn-flat">-->
                                                    <!--                                                            <i class="fa fa-comment"></i></div>-->
                                                    <!--                                                        <textarea name="yearbonus_note" id="yearbonus_note" cols="1" rows="1" class="form-control">--><?php //if ($member->getYearbonusPay()) {
                                                    //                                                                echo $member->getYearbonusNote();
                                                    //                                                            }; ?><!--</textarea>-->
                                                    <!--                                                    </div>-->
                                                    <!--                                                </div>-->
                                                    <!--                                            </div>-->
                                                </div>
                                            </div>

                                            <div class="box box-solid box-primary">
                                                <div class="box-body">

<!--                                                    <div class="form-group">-->
<!--                                                        <label class="col-sm-4 control-label">Дата изменения</label>-->
<!--                                                        <div class="col-sm-8">-->
<!--                                                            <div class="input-group date">-->
<!--                                                                <div class="input-group-addon">-->
<!--                                                                    <i class="fa fa-calendar"></i>-->
<!--                                                                </div>-->
<!--                                                                <input type="text" class="form-control pull-right"-->
<!--                                                                       id="datepicker5" name="datepicker5"-->
<!--                                                                       data-inputmask="'alias': 'dd.mm.yyyy'" data-mask value="--><?php //echo date("d.m.Y"); ?><!--"-->
<!--                                                                    --><?php //if ($Permission_ChangeFinanceMotivation == 0) {
//                                                                        echo "disabled";
//                                                                    }; ?><!-- >-->
<!--                                                            </div>-->
<!--                                                        </div>-->
<!--                                                    </div>-->

                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Базовый процент</label>

                                                        <div class="col-sm-8">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <b>%</b>
                                                                </div>
                                                                <input type="text" name="member_percent" placeholder="пусто" value="<?php if ($member->getBasePercent()) {
                                                                    echo $member->getBasePercent();
                                                                }; ?>"
                                                                       class="form-control pull-right" <?php if (($member->getMotivation() == 6) || ($Permission_ChangeFinanceMotivation != 1)) {
                                                                    echo "disabled";
                                                                } ?> >
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php
                                                    if ($member->getMotivation() == 10) {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Процент мотивации менеджера монтажа</label>

                                                        <div class="col-sm-8">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <b>%</b>
                                                                </div>
                                                                <input type="text" name="brend_percent" placeholder="пусто" value="<?php if ($member->getBrandPercent()) {
                                                                    echo $member->getBrandPercent();
                                                                }; ?>"
                                                                       class="form-control pull-right" <?php if ($Permission_ChangeFinanceMotivation != 1) {
                                                                    echo "disabled";
                                                                } ?> >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    }
                                                    ?>

                                                    <!--                                                    <div class="form-group">-->
                                                    <!--                                                        <label class="col-sm-4 control-label">Административная премия</label>-->
                                                    <!---->
                                                    <!--                                                        <div class="col-sm-8">-->
                                                    <!--                                                            <div class="input-group">-->
                                                    <!--                                                                <div class="input-group-addon">-->
                                                    <!--                                                                    <i class="fa fa-ruble"></i>-->
                                                    <!--                                                                </div>-->
                                                    <!--                                                                <input type="text" name="member_AdministrativePrize" placeholder="0" class="form-control pull-right"-->
                                                    <!--                                                                       value="--><?php //if ($member->getAdministrativePrize()) {
                                                    //                                                                           echo $member->getAdministrativePrize();
                                                    //                                                                       } ?><!--" --><?php //if ($Permission_ChangeFinanceMotivation != 1) {
                                                    //                                                                    echo "disabled";
                                                    //                                                                } ?><!-- >-->
                                                    <!--                                                            </div>-->
                                                    <!--                                                        </div>-->
                                                    <!--                                                    </div>-->

                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Административная премия</label>

                                                        <div class="col-md-8">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-ruble"></i>
                                                                </div>
                                                                <input type="text" class="form-control" name="member_AdministrativePrize" id="AdministrativePrize_2" placeholder="0" title="'<?php echo $member->getAdministrativePrizeNote(); ?>'" value="<?php if ($member->getAdministrativePrize()) {
                                                                    echo $member->getAdministrativePrize();
                                                                } ?>" <?php if ($Permission_ChangeFinanceMotivation != 1) {
                                                                    echo "disabled";
                                                                } ?> autocomplete="off">
                                                                <div class="input-group-addon btn btn-flat" name="AdministrativePrize" id="btn_comment_2">
                                                                    <i class="fa fa-comment"></i></div>
                                                                <textarea name="AdministrativePrize_note" id="AdministrativePrize_note_2" cols="1" rows="1" class="form-control" style="display: none;"><?php echo $member->getAdministrativePrizeNote(); ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Мобильная связь</label>

                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-ruble"></i>
                                                                </div>
                                                                <input type="text" name="member_MobileCommunication" placeholder="0" class="form-control pull-right"
                                                                       value="<?php if ($member->getMobileCommunication()) {
                                                                           echo $member->getMobileCommunication();
                                                                       }; ?>" <?php if ($Permission_ChangeFinanceMotivation != 1) {
                                                                    echo "disabled";
                                                                } ?> >
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-1">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input style="margin-left:5px" name="member_mobile_formula" type="checkbox" <?php if ($member->getMobileFormula() == 1) echo "checked"; ?> >
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label class="col-sm-3 control-label" style="text-align: left; padding-left: 0; margin-left: 0; font-weight: 300;">Расчет по формуле</label>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Транспортные расходы</label>

                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-ruble"></i>
                                                                </div>
                                                                <input type="text" name="member_Transport" placeholder="0" class="form-control pull-right"
                                                                       value="<?php if ($member->getTransport()) {
                                                                           echo $member->getTransport();
                                                                       }; ?>" <?php if ($Permission_ChangeFinanceMotivation != 1) {
                                                                    echo "disabled";
                                                                } ?> >
                                                            </div>
                                                        </div>


                                                        <div class="col-sm-1">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input style="margin-left:5px" name="member_transport_formula" type="checkbox" <?php if ($member->getTransportFormula() == 1) echo "checked"; ?> >
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label class="col-sm-3 control-label" style="text-align: left; padding-left: 0; margin-left: 0; font-weight: 300;">Расчет по формуле</label>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="box-footer">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <?php if ($Permission_ChangeFinanceMotivation == 1) { ?>
                                                        <button type="submit" class="btn btn-block btn-primary btn-flat center-block" style="width: 140px;"
                                                                name="finance_percent_button">
                                                            Обновить
                                                        </button>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    if (($member->getMotivation() == 2) || ($member->getMotivation() == 4) || ($member->getMotivation() == 5) || ($member->getMotivation() == 7) || ($member->getMotivation() == 8)) {
                                        ?>
                                        <form method="post" class="form-horizontal">
                                            <div class="box box-solid">
                                                <div class="box-header">
                                                    <h4>Расширенные финансовые условия</h4>
                                                </div>

                                                <div class="box-body">
                                                    <div class="box box-solid box-primary">
                                                        <div class="box-body">

<!--                                                            <div class="form-group">-->
<!--                                                                <label class="col-sm-5 control-label" style='text-align: left !important;'>Дата изменения</label>-->
<!--                                                                <div class="col-sm-6 input-group">-->
<!--                                                                    <div class="input-group date">-->
<!--                                                                        <div class="input-group-addon">-->
<!--                                                                            <i class="fa fa-calendar"></i>-->
<!--                                                                        </div>-->
<!--                                                                        <input type="text" class="form-control pull-right"-->
<!--                                                                               id="datepicker6" name="datepicker6"-->
<!--                                                                               data-inputmask="'alias': 'dd.mm.yyyy'" data-mask value="--><?php //echo date("d.m.Y"); ?><!--"-->
<!--                                                                            --><?php //if ($Permission_ChangeFinanceMotivation == 0) {
//                                                                                echo "disabled";
//                                                                            }; ?><!-- >-->
<!--                                                                    </div>-->
<!--                                                                </div>-->
<!--                                                            </div>-->


                                                            <?php
                                                            try {

                                                                if (($member->getMotivation() == 1) || ($member->getMotivation() == 4) || ($member->getMotivation() == 5) || ($member->getMotivation() == 7) || ($member->getMotivation() == 8)) { // Мотивация от статгрупп, личная мотивация (менеджер) и бренд.
                                                                    try {
                                                                        $PercentArray = $ST->get();
                                                                    } catch (Exception $e) {
                                                                        echo $e->getMessage() . "<BR>";
                                                                    }
                                                                } else if (($member->getMotivation() == 2) || ($member->getMotivation() == 3)) { // Мотивация от ТТ
                                                                    $PercentArray = $Departments->GetDepartments();
                                                                }

                                                                foreach ($PercentArray as $value) {
                                                                    if (!isset($CPT)) {
                                                                        continue;
                                                                    }

                                                                    if (($member->getMotivation() == 1) || ($member->getMotivation() == 4) || ($member->getMotivation() == 5) || ($member->getMotivation() == 7) || ($member->getMotivation() == 8)) {
                                                                        $SkladName = $value->getName();
                                                                    } else if (($member->getMotivation() == 2) || ($member->getMotivation() == 3)) { // Мотивация от ТТ
                                                                        $SkladName = $value->getNamesklad();
                                                                    }
                                                                    if ((($member->getMotivation() == 2) || ($member->getMotivation() == 3)) && empty($SkladName)) {
                                                                        continue;
                                                                    }

                                                                    if ($member->getBasePercent()) {
                                                                        $member_custom_procent_value = $member->getBasePercent();
                                                                    } else {
                                                                        $member_custom_procent_value = 0;
                                                                    }

                                                                    if (isset($CPT->get()[$value->getId()])) {
                                                                        $member_custom_procent_value = $CPT->get()[$value->getId()];
                                                                    }
                                                                    echo "<div class=\"form-group\">";
                                                                    if (($member->getMotivation() == 1) || ($member->getMotivation() == 4) || ($member->getMotivation() == 5) || ($member->getMotivation() == 7)|| ($member->getMotivation() == 8)) {
                                                                        if ($value->getId() < 13) {
                                                                            echo "<label class=\"col-sm-5 control-label\" style='text-align: left !important;'>" . $value->getName() . "</label>";
                                                                        } else {
                                                                            echo "<label class=\"col-sm-5 control-label\" style='text-align: left !important; color: #2b6182'>" . $value->getName() . "</label>";
                                                                        }
                                                                    } else if (($member->getMotivation() == 2) || ($member->getMotivation() == 3)) {
                                                                        echo "<label class=\"col-sm-5 control-label\"  style='text-align: left !important;'>" . $value->getNamesklad() . "</label>";
                                                                    }

                                                                    echo "<div class=\"col-sm-6 input-group\">";
                                                                    echo "<span class=\"input-group-addon\"><b>%</b></span>";
                                                                    if (isset($CPT->get()[$value->getId()])) {
                                                                        if ($Permission_ChangeFinanceMotivation == 1) {
                                                                            echo "<input autocomlete=\"off\" type=\"text\" name=\"custom_percent[" . $value->getId() . "]\" value='" . $member_custom_procent_value . "' class=\"form-control pull-right\">";
                                                                        } else {
                                                                            echo "<input autocomlete=\"off\" type=\"text\" name=\"custom_percent[" . $value->getId() . "]\" value='" . $member_custom_procent_value . "' class=\"form-control pull-right\" disabled >";
                                                                        }
                                                                    } else {
                                                                        if ($Permission_ChangeFinanceMotivation == 1) {
                                                                            echo "<input autocomlete=\"off\" type=\"text\" name=\"custom_percent[" . $value->getId() . "]\" placeholder='" . $member_custom_procent_value . "' class=\"form-control pull-right\">";
                                                                        } else {
                                                                            echo "<input autocomlete=\"off\" type=\"text\" name=\"custom_percent[" . $value->getId() . "]\" placeholder='" . $member_custom_procent_value . "' class=\"form-control pull-right\" disabled>";
                                                                        }
                                                                    }
                                                                    echo "</div>";
                                                                    echo "</div>";
                                                                }
                                                            } catch (Exception $e) {
                                                                echo $e->getMessage() . "<BR>";
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>

                                                    <div class="box-footer">
                                                        <?php if ($Permission_ChangeFinanceMotivation == 1) { ?>
                                                            <button type="submit" class="btn btn-block btn-primary btn-flat center-block" style="width: 140px;" name="SaveCustomPercent">
                                                                Сохранить
                                                            </button>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                    }

                                    if (($member->getMotivation() == 1) || ($member->getMotivation() == 2)) {
                                        ?>
                                        <form method="post" class="form-horizontal">
                                            <div class="box box-solid">
                                                <div class="box-header">
                                                    <h4>Окна</h4>
                                                </div>

                                                <div class="box-body">
                                                    <div class="box box-solid box-primary">
                                                        <div class="box-body" id="window_form">
                                                            <select id="serviceManagers" class='form-control' style="display: none">
                                                                <option></option>
                                                                <?php
                                                                foreach ($MI->getWindowManagers() as $serviceManager) {
                                                                    echo '<option value='.$serviceManager['id'].'>'.$serviceManager['lastname'].' '.$serviceManager['name'].' '.$serviceManager['middle'].'</option>';
                                                                }
                                                                ?>
                                                            </select>

                                                            <?php
                                                            foreach ($WM->get($user_id) as $key=>$item) {
                                                                ?>
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Сервис менеджер</label>

                                                                    <div class='col-sm-3'>
                                                                        <select class='form-control' name="window_manager[<?=$key?>][window_id]">
                                                                            <option></option>
                                                                            <?php
                                                                            foreach ($MI->getWindowManagers() as $serviceManager) {
                                                                                echo '<option value='.$serviceManager['id'].' '.($item['window_id']==$serviceManager['id']?"selected":"").'>'.$serviceManager['lastname'].' '.$serviceManager['name'].' '.$serviceManager['middle'].'</option>';
                                                                            }
                                                                            ?>
                                                                        </select></div>

                                                                    <div class="col-sm-3">
                                                                        <div class="input-group">
                                                                        <span class="input-group-addon"><b>%</b></span>
                                                                        <input autocomlete="off" type="text" name="window_manager[<?=$key?>][percent]" value='<?=$item['percent']?>' class="form-control pull-right">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <div class='input-group'>
                                                                            <input type='button' name='removeWindow' class='btn btn-flat btn-aqua removeWindow' value='Удалить' />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>

                                                    </div>

                                                    <div class="box-footer">
                                                        <?php if ($Permission_ChangeFinanceMotivation == 1) { ?>
                                                            <button type="button" class="btn btn-flat  btn-sm btn-flat pull-right btn-aqua addWindow">
                                                                Добавить СМ
                                                            </button>

                                                            <button type="submit" class="btn btn-block btn-primary btn-flat center-block" style="width: 140px;" name="SaveWindowPercent">
                                                                Сохранить
                                                            </button>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                    }
                                }
                                ?>


                                <?php
                                if ($Permission_ChangeFinanceMotivation == 1) {
                                    if (($member->getMotivation() == 1) || ($member->getMotivation() == 2) || ($member->getMotivation() == 4) || ($member->getMotivation() == 5) || ($member->getMotivation() == 7)) {
                                        ?>
                                        <form method="post" class="form-horizontal">
                                            <div class="box box-solid">
                                                <div class="box-header">
                                                    <h4>KPI</h4>
                                                </div>

                                                <div class="box-body">
                                                    <div class="box box-solid box-primary">
                                                        <div class="box-body">

    <!--                                                        <div class="form-group">-->
    <!--                                                            <label class="col-sm-3 control-label">Дата изменения</label>-->
    <!--                                                            <div class="col-sm-8 input-group">-->
    <!--                                                                <div class="input-group date">-->
    <!--                                                                    <div class="input-group-addon">-->
    <!--                                                                        <i class="fa fa-calendar"></i>-->
    <!--                                                                    </div>-->
    <!--                                                                    <input type="text" class="form-control pull-right"-->
    <!--                                                                           id="datepicker9" name="datepicker9"-->
    <!--                                                                           data-inputmask="'alias': 'dd.mm.yyyy'" data-mask value="--><?php //echo date("d.m.Y"); ?><!--"-->
    <!--                                                                        --><?php //if ($Permission_ChangeFinanceMotivation == 0) {
    //                                                                            echo "disabled";
    //                                                                        }; ?><!-- >-->
    <!--                                                                </div>-->
    <!--                                                            </div>-->
    <!--                                                        </div>-->


                                                            <?php
                                                            $pki_summ = 0;
                                                            for ($i = 0; $i < 10; $i++) {
                                                                $current_kpi = $KPI->getByNum($i);
                                                                if ($current_kpi) {
                                                                    $pki_summ += $current_kpi->getSumm();
                                                                }
                                                                ?>

                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label"><?php echo ($i+1); ?></label>
                                                                    <div class="col-sm-8 input-group">
                                                                            <div class="input-group-addon">
                                                                                <i class="fa fa-ruble"></i>
                                                                            </div>

                                                                            <input type="hidden" name="kpi[<?php echo $i; ?>][id]" value="<?php if($current_kpi) { echo $current_kpi->getId(); } ?>">
                                                                            <input type="hidden" name="kpi[<?php echo $i; ?>][num]" value="<?php echo $i; ?>">


                                                                            <input type="text"
                                                                                   name="kpi[<?php echo $i; ?>][summ]"
                                                                                   id="kpi_<?php echo $i; ?>"
                                                                                   placeholder="0"
                                                                                   value="<?php if($current_kpi) { echo $current_kpi->getSumm(); } ?>"
                                                                                   title="<?php if($current_kpi) { echo $current_kpi->getNote(); } ?>"
                                                                                   class="form-control pull-right"
                                                                                <?php if ($Permission_ChangeFinanceMotivation != 1) {
                                                                                    echo "disabled";
                                                                                } ?>
                                                                                   autocomplete="off">

                                                                            <div class="input-group-addon btn btn-flat"
                                                                                 name="kpi"
                                                                                 id='btn_comment_<?php echo $i; ?>'>
                                                                                <i class="fa fa-comment"></i>
                                                                            </div>

                                                                            <textarea name='kpi[<?php echo $i; ?>][note]'
                                                                                      id='kpi_note_<?php echo $i; ?>'
                                                                                      cols='1'
                                                                                      rows='1'
                                                                                      class='form-control' <?php if ($Permission_ChangeFinanceMotivation == 0) {
                                                                                echo "disabled";
                                                                            }; ?> ><?php if($current_kpi) { echo $current_kpi->getNote(); } ?></textarea>
                                                                        </div>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">Итого</label>
                                                                <label class="col-sm-8 control-label"><?php echo $pki_summ; ?></label>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="box-footer">
                                                        <?php if ($Permission_ChangeFinanceMotivation == 1) { ?>
                                                            <button type="submit" class="btn btn-block btn-primary btn-flat center-block" style="width: 140px;" name="SaveKPI">
                                                                Сохранить
                                                            </button>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                    }
                                }
                                ?>
                            </form>
                        </div>

                        <!-- Рабочий статус -->
                        <?php if ($Permission_ChangeWorkStatus == 1) { ?>
                            <div class="tab-pane" id="tab_5">
                                <div class="box box-solid box-pane">
                                    <div class="box-header">
                                        <h4>Изменение рабочего статуса</h4>
                                    </div>

                                    <div class="box-body">
                                        <form method="post" name="tab5" class="form-horizontal">

                                            <div class="box box-solid box-warning box-pane" id="DismissalForm" hidden="hidden">
                                                <div class="box-body ">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Заметка</label>
                                                        <div class="col-md-9">
                                                            <textarea cols='1' rows='1' class='form-control' style="min-height: 34px;" name="TextArea2"></textarea>
                                                        </div>
                                                    </div>
                                                    <p class="col-md-offset-3 help-block">Пользователь помечается на увольнение, только при выставленном флаге.
                                                        После пометки на увольнение пользователю не рассчитывается аванс и премия.</p>
                                                </div>
                                            </div>

                                            <div class="box box-solid box-default box-pane" id="DecreeForm" <?=($member->getWorkstatus() != 3)?"hidden='hidden'":""?> >
                                                <div class="box-body ">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Дата декрета</label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group date">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <input type="text" class="form-control pull-right"
                                                                       id="datepicker11" name="decreedate"
                                                                       data-inputmask="'alias': 'dd.mm.yyyy'" data-mask
                                                                       value="<?=($member->getWorkstatus() == 3)?$member->getWorkStatusDate():date("d.m.Y");?>"
                                                                >
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Заметка</label>
                                                        <div class="col-md-9">
                                                            <textarea cols='1' rows='1' class='form-control' style="min-height: 34px;" name="TextArea3"><?=($member->getWorkstatus() == 3)?$member->getWorkStatusNote():""?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="box box-solid box-danger box-pane" id="LeavingForm" <?php if ($member->getWorkstatus() != 4) {
                                                echo "hidden='hidden'";
                                            } ?> >
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Причина увольнения</label>
                                                        <div class="col-sm-9">
                                                            <select name="leavingid" class="form-control">
                                                                <option selected value="0">Не выбрано</option>
                                                                <?php
                                                                try {
                                                                    foreach ($Leaving->GetReasons() as $reason) {
                                                                        if ($reason['id'] == $member->getReasonLeavingId()) {
                                                                            echo '<option value="' . $reason['id'] . '" selected>' . $reason['name'] . '</option>';
                                                                        } else {
                                                                            echo '<option value="' . $reason['id'] . '">' . $reason['name'] . '</option>';
                                                                        }
                                                                    }
                                                                } catch (Exception $e) {
                                                                    echo $e->getMessage();
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Дата увольнения</label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group date">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <input type="text" class="form-control pull-right"
                                                                       id="datepicker2" name="leavingdate"
                                                                       data-inputmask="'alias': 'dd.mm.yyyy'" data-mask value="<?php
                                                                if ($member->getWorkstatus() == 4) {
                                                                    echo $member->getDismissalDate();
                                                                } else {
                                                                    echo date("d.m.Y");
                                                                } ?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Заметка</label>
                                                        <div class="col-md-9">
                                                            <textarea cols='1' rows='1' class='form-control' style="min-height: 34px;" name="TextArea4"><?php echo $member->getReasonLeavingNote(); ?></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="box box-solid box-pane">
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Рабочий статус</label>
                                                        <div class="col-sm-9">
                                                            <select name="WorkStatus" id="WorkStatus" class="form-control" <?=((!$is_director and ReportStatus::getReportStatusLastMonth())?'disabled':'')?> >
                                                                <option <?php if ($member->getWorkstatus() == 1) {
                                                                    echo 'selected';
                                                                } ?> value="1">Работает
                                                                </option>
                                                                <option <?php if ($member->getWorkstatus() == 2) {
                                                                    echo 'selected';
                                                                } ?> value="2">Сдает дела
                                                                </option>
                                                                <option <?php if ($member->getWorkstatus() == 4) {
                                                                    echo 'selected';
                                                                } ?> value="4">Увольнение
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-footer">
                                                    <?php
                                                    if (!$is_director and ReportStatus::getReportStatusLastMonth()) {
                                                        ?>
                                                        <pre class="alert-danger" style="margin-top: 5px;">Зарплатная ведомость на стадии директората. Изменить статус сотрудника нельзя.</pre>
                                                    <?php
                                                    } else {
                                                    ?>
                                                        <button type="submit" class="btn btn-block btn-info btn-flat center-block" name="SetStatusButton">Установить статус</button>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <!--                            Мотивация-->
                        <div class="tab-pane" id="tab_6">
                            <div class="box box-solid">
                                <form method="post" name="tab6" class="form-horizontal">
                                    <div class="box-header">
                                        <h4>Мотивация сотрудника</h4>
                                    </div>
                                    <div class="box-body">
<!--                                        <div class="form-group">-->
<!--                                            <label class="col-sm-3 control-label">Дата изменения</label>-->
<!--                                            <div class="col-sm-9">-->
<!--                                                <div class="input-group date">-->
<!--                                                    <div class="input-group-addon">-->
<!--                                                        <i class="fa fa-calendar"></i>-->
<!--                                                    </div>-->
<!--                                                    <input type="text" class="form-control pull-right"-->
<!--                                                           id="datepicker7" name="motivation_date"-->
<!--                                                           data-inputmask="'alias': 'dd.mm.yyyy'" data-mask value="--><?php //echo date("d.m.Y"); ?><!--"-->
<!--                                                        --><?php //if ($Permission_ChangeFinanceMotivation == 0) {
//                                                            echo "disabled";
//                                                        }; ?><!-- >-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Выбор мотивации</label>
                                            <div class="col-sm-9">
                                                <select name="motivation_id" class="form-control" <?php if ($Permission_ChangeFinanceMotivation != 1) {
                                                    echo "disabled";
                                                } ?> >
                                                    <option value="-1">Не выбрано</option>
                                                    <?php
                                                    try {
                                                        $MotivationNote = "";
                                                        foreach ($Motivation->get() as $motivation) {
                                                            if ($motivation->getID() == $member->getMotivation()) {
                                                                $MotivationNote = $motivation->getNote();
                                                                echo '<option title="' . $motivation->getNote() . '" value="' . $motivation->getID() . '" selected>' . $motivation->getName() . '</option>';
                                                            } else {
                                                                echo '<option title="' . $motivation->getNote() . '" value="' . $motivation->getID() . '">' . $motivation->getName() . '</option>';
                                                            }
                                                        }
                                                    } catch (Exception $e) {
                                                        echo $e->getMessage();
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Заметка к текущей мотивации</label>
                                            <div class="col-sm-9">
                                                <code><?php echo $MotivationNote; ?></code>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <?php if ($Permission_ChangeFinanceMotivation == 1) { ?>
                                                    <button type="submit" class="btn btn-block btn-primary btn-flat center-block" name="motivation_button"
                                                            style="width: 140px;">Обновить
                                                    </button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--                            Роли -->

                        <div class="tab-pane" id="tab_7">
                            <form method="post" name="tab7" class="form-horizontal">
                                <?php if ($Permission_Settings == 1) {
                                    ?>
                                    <div class="box box-solid">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Роль</label>
                                                <div class="col-sm-10">
                                                    <select name="RoleId" id="RoleId" class="form-control">
                                                        <?php
                                                        if (count($RolesUser) == 0) {
                                                            ?>
                                                            <option value="0">Менеджер (по умолчанию)</option>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        try {
                                                            foreach ($RolesList as $value) {
                                                                echo "<option value='" . $value['id'] . "' >" . $value['name'] . "</option>";
                                                            }
                                                        } catch (Exception $e) {
                                                            echo $e->getMessage();
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group" id="RoleDirectionForm" hidden="hidden">
                                                <label class="col-sm-2 control-label">Отдел</label>
                                                <div class="col-sm-10">
                                                    <select name="RoleDirection[]" id="RoleDirection" class="form-control select2" multiple="multiple" data-placeholder="Выбирите отдел" style="width: 100%;">
                                                        <option value="0">Все отделы</option>
                                                        <?php
                                                        try {
                                                            foreach ($Departments->GetDepartments() as $value) {
                                                                echo "<option value='" . $value->GetID() . "''>" . $value->GetName() . "</option>";
                                                            }
                                                        } catch (Exception $e) {
                                                            echo $e->getMessage();
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="box-footer">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-block btn-primary btn-flat center-block" name="RoleAddButton"
                                                                style="width: 140px;">Добавить роль
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <div class="box box-solid">
                                    <div class="box-body">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th> Роль</th>
                                                <th> Отдел</th>
                                                <?php
                                                if ($Permission_Settings == 1) {
                                                    ?>
                                                    <th class="text-right"> Действия</th>
                                                    <?php
                                                }
                                                ?>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($RolesUser as $item) {
                                                echo "<tr><td>" . $item->getRoleName() . "</td>";
                                                if ($item->getDepartment() != 0) {
                                                    echo "<td>" . $item->getDepartmentName() . "</td>";
                                                } else {
                                                    echo "<td>Все отделы</td>";
                                                }
                                                if ($Permission_Settings == 1) {
                                                    echo "<td><button class='btn btn-danger btn-sm pull-right' name='RoleDelButton' value='" . $item->getRoleId() . "'>Удалить роль</button> </td>";
                                                }
                                                echo "</tr>";
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>


                        <!-- УРВ -->
                        <div class="tab-pane" id="tab_8">
                            <form method="post" name="tab8" class="form-horizontal">
                                <?php if ($Permission_ChangeWorkStatus == 1) {
                                    ?>
                                    <div class="box box-solid">
                                        <div class="box-header">
                                            <h4>УРВ</h4>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Не считать УРВ</label>
                                                <div class="col-sm-8">
                                                    <div class="checkbox">
                                                        <input style="margin-left:5px" name="not_count_urv" type="checkbox" <?php if ($member->getNotCountUrv() == 1) echo "checked"; ?> >
                                                    </div>
                                                </div>
                                            </div>

<!--                                            <div class="form-group">-->
<!--                                                <label class="col-sm-4 control-label">Отпуск не сгорает</label>-->
<!--                                                <div class="col-sm-1">-->
<!--                                                    <div class="checkbox">-->
<!--                                                        <input style="margin-left:5px" name="vacation_not_expire_tmp" disabled type="checkbox" --><?php //if ($member->getVacationNotExpire() == 1) echo "checked"; ?><!-- >-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                <label class="col-sm-1 control-label">С даты</label>-->
<!--                                                <div class="col-sm-6">-->
<!--                                                    <div class="input-group date">-->
<!--                                                        <div class="input-group-addon">-->
<!--                                                            <i class="fa fa-calendar"></i>-->
<!--                                                        </div>-->
<!--                                                        <input type="text" class="form-control pull-right" id="datepicker12" name="vacation_not_expire_date_tmp" disabled data-inputmask="'alias': 'yyyy-mm-dd'" data-mask="" value="--><?php //$member->getVacationNotExpireDate();?><!--"  autocomplete="off">-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                            </div>-->

                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Продление отпуска до</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" class="form-control pull-right" id="datepicker13" name="vacation_extension_date" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask="" value="">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="input-group" style="width: 100%;">
                                                        <textarea class="form-control" name="vacation_extension_note" style="height: 34px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="box-footer">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-block btn-primary btn-flat center-block" name="urv_button"
                                                                style="width: 140px;">Сохранить
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h4>Продление отпуска до</h4>
                                    </div>
                                    <div class="box-body">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th> Год</th>
                                                <th> До какого</th>
                                                <th> Комментарий</th>
                                                <th> Изменил</th>
                                                <th> Дата изменения</th>
                                                <?php
                                                if ($Permission_Settings == 1) {
                                                    ?>
                                                    <th class="text-right"> Действия</th>
                                                    <?php
                                                }
                                                ?>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $vacationExtensionDate =  $member->getVacationExtensionDate();
                                            if ($vacationExtensionDate) {
                                            foreach ($vacationExtensionDate as $item) {
                                                $aDate = explode("-", $item['vacation_extension_date']);
                                                echo "<tr>";
                                                echo "<td>" . ($aDate[0]-1) . "</td>";
                                                echo "<td>" . $item['vacation_extension_date'] . "</td>";
                                                echo "<td>" . $item['vacation_extension_note'] . "</td>";
                                                echo "<td>" . ($item['changer_id']==-1?'Администратор':$item['changer_name']) . "</td>";
                                                echo "<td>" . $item['change_date'] . "</td>";

                                                if ($Permission_Settings == 1) {
                                                    echo "<td><button class='btn btn-danger btn-sm pull-right' name='VacationExtensionDelButton' value='" . $item['id'] . "'>Удалить продление</button> </td>";
                                                }
                                                echo "</tr>";
                                            }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li style="width: 100%;" class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Основное</a></li>
                            <?php
                            if ($Permission_ChangeWorkPersonal == 1 || ($User->getMemberId() == $user_id)) {
                                ?>
                                <li style="width: 100%;" class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Личное</a></li>
                                <li style="width: 100%;" class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Работа</a></li>
                                <?php
                            };

                            if (($Permission_ChangeFinanceMotivation == 1) || ($Permission_ChangeFinanceProbation == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <li style="width: 100%;" class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Финансовые условия</a></li>
                                <?php
                            }
                            if (($Permission_ChangeFinanceMotivation == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <li style="width: 100%;" class=""><a href="#tab_6" data-toggle="tab" aria-expanded="false">Мотивация</a></li>

                                <?php
                            };
                            ?>
                            <li style="width: 100%;"><a href="#tab_7" data-toggle="tab" aria-expanded="false">Роль</a></li>
                            <?php
                            if ($Permission_ChangeWorkStatus == 1) { ?>
                                <li style="width: 100%;" class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false">Рабочий статус</a></li>
                            <?php } ?>

                            <?php
                            if ($Permission_ChangeWorkStatus == 1) { ?>
                                <li style="width: 100%;" class=""><a href="#tab_8" data-toggle="tab" aria-expanded="false">УРВ</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>

            <?php
            /*
             * Вывод информации об ощибках
             */
            if ($Errors->getCount() > 0) {
                ?>
                <div class="modal fade" id="modalError" style="background-color: #1f292e">
                    <div style="width: 90%;" class="modal-dialog">
                        <div style="width: 100%;" class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h5>Ошибки на странице</h5>
                            </div>

                            <div class="modal-body">
                                <?php
                                foreach ($Errors->getErrors() as $error) {
                                    ?>
                                    <div class="alert alert-danger" role="alert">
                                        <i class="fa fa-exclamation-triangle" style="margin-right: 15px; "></i>
                                        <?php echo $error; ?>
                                        <a href="#" class="close justify-content-center" data-dismiss="alert">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                    <?php
                                };
                                ?>
                            </div>

                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </section>
    </div>
    <?php require_once 'footer.php'; ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="../../bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>
<?php
if ($Errors->getCount() > 0) {
    echo "<script> $('#modalError').modal('show'); </script>";
}
?>

<script>
    $(function () {
        $('[data-mask]').inputmask()

        $('.select2').select2()

        //Date picker
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'dd.mm.yyyy',
            startDate: '-80Y',
            endDate: '-18Y'

        })


        $('#datepicker2').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'dd.mm.yyyy',
            startDate: '<?php echo $MI->GetDateForLeaving($user_id);?>'
        })

        $('#datepicker3').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'dd.mm.yyyy',
            startDate: '<?= $MI->GetDateForLeaving($user_id);?>'
        })


        $('#datepicker4').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'dd.mm.yyyy'
        })

        $('#datepicker5').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'dd.mm.yyyy'
        })

        $('#datepicker6').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'dd.mm.yyyy'
        })

        $('#datepicker7').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'dd.mm.yyyy'
        })

        $('#datepicker8').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'dd.mm.yyyy'
        })

        $('#datepicker9').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'dd.mm.yyyy'
        })

        $('#datepicker10').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'dd.mm.yyyy'
        })

        $('#datepicker11').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'dd.mm.yyyy'
        })

        $('#datepicker12').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'yyyy-mm-dd'
        })

        $('#datepicker13').datepicker({
            language: 'ru',
            autoclose: true,
            format: 'yyyy-mm-dd'
        })


    })
</script>

<script>
    $(document).ready(function () {
        $("#WorkStatus").change(function () {
            $("#LeavingForm").hide();
            $("#DismissalForm").hide();
            $("#DecreeForm").hide();

            var Status = $("select#WorkStatus").val();
            switch (parseInt(Status)) {
                case 1:
                    break;
                case 2:
                    $("#DismissalForm").show();
                    break;
                case 3:
                    $("#DecreeForm").show();
                    break;
                case 4:
                    $("#LeavingForm").show();
                    break;

            }
        });

        /*
         * Показываю select2 с выбором отделов.
         */
        $('#RoleId').change(function () {
            /*
            * Выставляю в select2 отделах значение на 0 (все отделы)
            */
            $('#RoleDirection').val('0').trigger('change');

            /*
            * Получаю значение из RoleId
            */
            var value = $(this).val();
            /*
            * Если значение не 0,1,3,6, то показываю select2 с отделами, иначе скрываю
            */
            if (value != 0 && value != 1 && value != 3 && value != 6) {
                $("#RoleDirectionForm").show();
            } else {
                $("#RoleDirectionForm").hide();
            }
        });

        /*
         * Удаляю option "все отделы" если выбран какой-либо другой отдел.
         */
        $('#RoleDirection').on('select2:selecting', function () {
            /*
             * Получаю значения из select2
             */
            var arr = $('#RoleDirection').val();
            /*
             * Фильтрую массив с условием что != 0
             */
            var newArr = arr.filter(function (number) {
                return number != 0;
            })
            /*
             * Изменяю значения у select2
             */
            $('#RoleDirection').val(newArr).trigger('change');
        })

        $("#add_presonal_phone").click(function () {
            var lastField = $("#phone_form div:last"); // получаем last div
            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
            var fieldWrapper = $("<span id=\"field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);

            var fForm = $("<div class=\"form-group\">");
            var fName = $("<div class=\"col-md-10\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-phone\"></i></div><input type=\"text\" class=\"form-control\" name=\"personal_phone_add[]\" data-inputmask='\"mask\": \"(999)999-99-99\"' data-mask /></div></div>");
            var removeButton = $("<div class=\"col-md-2\"><div class=\"input-group\"><input type=\"button\" class=\"btn btn-flat btn-aqua\" value=\"Удалить\" /></div></div>");

            removeButton.click(function () {
                $(this).parent().remove();
            });

            fForm.append(fName);
            fForm.append(removeButton);
            fieldWrapper.append(fForm);

            $("#phone_form").append(fieldWrapper);
            $('[data-mask]').inputmask();
        });

        $("#add_presonal_mobile_phone").click(function () {
            var lastField = $("#phone_mobile_form div:last"); // получаем last div

            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;


            var fieldWrapper = $("<span id=\"field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);

            var fForm = $("<div class=\"form-group\">");
            var fName = $("<div class=\"col-md-10\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-phone\"></i></div><input type=\"text\" class=\"form-control\" name=\"personal_mobile_phone_add[]\" data-inputmask='\"mask\": \"(999)999-99-99\"' data-mask /></div></div>");
            var removeButton = $("<div class=\"col-md-2\"><div class=\"input-group\"><input type=\"button\" class=\"btn btn-flat btn-aqua\" value=\"Удалить\" /></div></div>");

            removeButton.click(function () {
                $(this).parent().remove();
            });

            fForm.append(fName);
            fForm.append(removeButton);
            fieldWrapper.append(fForm);

            $("#phone_mobile_form").append(fieldWrapper);
            $('[data-mask]').inputmask();
        });

        $(".addWindow").click(function () {
            $.get('edit.php').done(function(data) {
                console.log(data);
                var uniq_id = ((new Date()).getTime() + "" + Math.floor(Math.random() * 1000000)).substr(0, 10);
                var addForm = "<div class='form-group'>" +
                    "<label class='col-sm-4 control-label'>Сервис менеджер</label>" +
                    "<div class='col-sm-3'><select name='window_manager["+uniq_id+"][window_id]' class='form-control'><option></option>";
                $.each(data, function (k, item) {
                    addForm += "<option value='"+item['id']+"'>"+item['lastname']+" "+item['name']+" "+item['middle']+"</option>";
                })
                addForm += "</select></div>"
                addForm += "<div class='col-sm-3'><div class='input-group'>" +
                    "<span class='input-group-addon'><b>%</b></span>" +
                    "<input autocomlete='off' type='text' name='window_manager["+uniq_id+"][percent]' value='0' class='form-control pull-right'>" +
                    "</div></div>" +
                    "<div class='col-md-2'><div class='input-group'><input type='button' name='removeWindow' class='btn btn-flat btn-aqua removeWindow' value='Удалить' />" +
                    "</div>";

                $("#window_form").append(addForm);

                $(".removeWindow").click(function () {
                    $(this).parent().parent().parent().remove();
                });
            })
        });

        $(".removeWindow").click(function () {
            $(this).parent().parent().parent().remove();
        });


        $("#add_work_phone").click(function () {
            var lastField = $("#work_phone_form div:last"); // получаем last div

            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;


            var fieldWrapper = $("<span id=\"field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);

            var fForm = $("<div class=\"form-group\">");
            var fName = $("<div class=\"col-md-10\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-phone\"></i></div><input type=\"text\" class=\"form-control\" name=\"work_phone_add[]\" data-inputmask='\"mask\": \"(999)999-99-99\"' data-mask /></div></div>");
            var removeButton = $("<div class=\"col-md-2\"><div class=\"input-group\"><input type=\"button\" class=\"btn btn-flat btn-aqua\" value=\"Удалить\" /></div></div>");

            removeButton.click(function () {
                $(this).parent().remove();
            });

            fForm.append(fName);
            fForm.append(removeButton);
            fieldWrapper.append(fForm);

            $("#work_phone_form").append(fieldWrapper);
            $('[data-mask]').inputmask();
        });

        $("#add_work_mobile_phone").click(function () {
            var lastField = $("#work_mobile_phone_form div:last"); // получаем last div

            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;


            var fieldWrapper = $("<span id=\"field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);

            var fForm = $("<div class=\"form-group\">");
            var fName = $("<div class=\"col-md-10\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-phone\"></i></div><input type=\"text\" class=\"form-control\" name=\"work_mobile_phone_add[]\" data-inputmask='\"mask\": \"(999)999-99-99\"' data-mask /></div></div>");
            var removeButton = $("<div class=\"col-md-2\"><div class=\"input-group\"><input type=\"button\" class=\"btn btn-flat btn-aqua\" value=\"Удалить\" /></div></div>");

            removeButton.click(function () {
                $(this).parent().remove();
            });

            fForm.append(fName);
            fForm.append(removeButton);
            fieldWrapper.append(fForm);

            $("#work_mobile_phone_form").append(fieldWrapper);
            $('[data-mask]').inputmask();
        });

        $("#add_emergency_phone").click(function () {
            var lastField = $("#emergency_phone_form span:last"); // получаем last div
            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
            var fieldWrapper = $("<span id=\"em_field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);
            var fForm = $("<div class=\"form-group\">");
            var fPhone = $("<div class=\"col-md-5\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-phone\"></i></div><input type=\"text\" class=\"form-control\" name=\"emergency_phone_add[" + intId + "][phone]\" data-inputmask='\"mask\": \"(999)999-99-99\"' data-mask /></div></div>");
            var fName = $("<div class=\"col-md-5\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-user\"></i></div><input type=\"text\" class=\"form-control\" name=\"emergency_phone_add[" + intId + "][name]\" /></div></div>");
            var removeButton = $("<div class=\"col-md-2\"><div class=\"input-group\"><input type=\"button\" class=\"btn btn-flat btn-aqua\" value=\"Удалить\" /></div></div>");

            removeButton.click(function () {
                $(this).parent().remove();
            });

            fForm.append(fPhone);
            fForm.append(fName);
            fForm.append(removeButton);
            fieldWrapper.append(fForm);

            $("#emergency_phone_form").append(fieldWrapper);
            $('[data-mask]').inputmask();
        });

        $("#add_email").click(function () {
            var lastField = $("#emails_form span:last"); // получаем last div
            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
            var fieldWrapper = $("<span id=\"field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);
            var fForm = $("<div class=\"form-group\">");
            var fMail = $("<div class=\"col-md-5\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-envelope\"></i></div><input type=\"text\" class=\"form-control\" name=\"email_add[" + intId + "][email]\" /></div></div>");
            var fName = $("<div class=\"col-md-5\"><div class=\"input-group\"><div class=\"input-group-addon\"><i class=\"fa fa-sticky-note\"></i></div><input type=\"text\" class=\"form-control\" name=\"email_add[" + intId + "][comment]\" /></div></div>");
            var removeButton = $("<div class=\"col-md-2\"><div class=\"input-group\"><input type=\"button\" class=\"btn btn-flat btn-aqua\" value=\"Удалить\" /></div></div>");

            removeButton.click(function () {
                $(this).parent().remove();
            });

            fForm.append(fMail);
            fForm.append(fName);
            fForm.append(removeButton);
            fieldWrapper.append(fForm);

            $("#emails_form").append(fieldWrapper);
        });

    });


</script>


<script>
    $(document).ready(function () {
        $('select[name=department] option').hide();
        $('select[name=position] option').hide();
        $('select[name=department] option.' + $('select[name=direction]').val()).show();
        $('select[name=position] option.' + $('select[name=department]').val()).show();

    });

    $('select[name=direction]').change(function () {
        $('select[name=department] option[value="-1"]').prop('selected', true);
        $('select[name=position] option[value="-1"]').prop('selected', true);

        $('select[name=department] option').hide();
        $("select[name=department] option:selected").prop("selected", false)
        $('select[name=position] option').hide();
        $("select[name=position] option:selected").prop("selected", false)

        $('select[name=department] option.' + $('select[name=direction]').val()).show();
    });

    $('select[name=department]').change(function () {
        $('select[name=position] option[value="-1"]').prop('selected', true);
        $('select[name=position] option').hide();
        $("select[name=position] option:selected").prop("selected", false)
        $('select[name=position] option.' + $('select[name=department]').val()).show();
    });

    $('select[name=YearBonusType]').change(function () {
        var value = $('select[name=YearBonusType] option:selected').val();
        if (value == 2) {
            $("#yearbonuslabel").removeClass("fa-ruble");
            $("#yearbonuslabel").addClass("fa-percent");
        } else {
            $("#yearbonuslabel").removeClass("fa-percent");
            $("#yearbonuslabel").addClass("fa-ruble");
        }
    })
</script>

<script>
    // Комментарии бонус и корректировка
    $(document).ready(function () {
        // $("textarea").each(function (i) {
        //     $(this).hide();
        // });

        // $("#yearbonus_note").show();
        // $("#Comment").show();

        $(document).on("click", "div[id^=btn_comment]", function () {
            var currentId = $(this).attr("id");
            var currentName = $(this).attr("name");
            var numberInID = currentId.replace("btn_comment_", "");


            if ($("#" + currentName + "_note_" + numberInID).is(':visible')) {
                $("#" + currentName + "_" + numberInID).show();
                $("#" + currentName + "_note_" + numberInID).hide();
            } else {
                $("#" + currentName + "_" + numberInID).hide();
                $("#" + currentName + "_note_" + numberInID).show();
            }
        });

        $(document).on("change", "textarea", function () {
            var currentVal = $(this).val();
            var currentId = $(this).attr("id");
            var Arr = currentId.split("_"); // bonus_note_
            var numberInID = currentId.replace(Arr[0] + "_note_", "");

            $("#" + Arr[0] + "_" + numberInID).attr("title", currentVal);
            $("#" + Arr[0] + "_note_" + numberInID).hide();
            $("#" + Arr[0] + "_" + numberInID).show();
        })
    });

    function w() {
        var direction =  $("select[name='direction']").val();
        var department = $("select[name='department']").val();
        var position =  $("select[name='position']").val();
        console.log(direction);
        console.log(department);
        console.log(position);
        if (direction==0 || department==0 || position==0) {
            alert("Не заполнены поля: направление, отдел, должность");
            return false;
        }
        return true;
    }
</script>


<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
