<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.08.18
 * Time: 18:29
 */

require_once 'app/FileCSV.php';
require_once 'app/MemberInterface.php';
require_once 'app/NdflInterface.php';
require_once 'app/ErrorInterface.php';
require_once "app/ReportStatus.php";

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

// НДФЛ
if (!array_filter($Roles, function ($Role) {
    return ($Role->getImportNDFL() == 1);
})) {
    header("Location: 404.php" );
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

$Errors = new ErrorInterface();

$MI = new MemberInterface();
$MI->fetchMembers();
$members = $MI->GetMembers();


$NDFL = new NdflInterface();

/*
 * Report статус
 */
$RS = new ReportStatus();


if (isset($_POST["submit"]) || isset($_POST["submit_import"])) {
    $array = array();
    $date = "";
    try {
        $uploadFile = new FileCSV();
        $uploadFile->setFormName("CSVfileToUpload");
        $uploadFile->Upload();
        $array = $uploadFile->ParseCSV(";");
    } catch (Exception $e) {
        $Errors->add($e->getMessage() . "<br>");
    }

    $SupperArray = array();

    foreach ($array as $key => $item) {
        /*
         * Ищу в массиве объектов сотрудников объект который содержит элементы из item
         */
        $neededObject = array_filter(
            $members,
            function ($e) use (&$item) {
                return $e->getPersonalNumber() == trim($item[2]) && is_numeric(trim($item[2]));
            }
        );

        /*
         * Получаю ключи из массива сотрудников
         */
        $member_keys = array_keys($neededObject);
        sort($neededObject);

        /*
         * Если существует первый элемент, то ...
         */
        if (isset($neededObject[0])) {
            /*
            * Проверяю что это объект
            */
            if (is_object($neededObject[0])) {
                /*
                 * Переменной date присваиваю нулевой элемент массива
                 */
                $date = trim($item[1]); /// ОЧЕНЬ КУХОННО!


                /*
                * Если 5,6 элементы пустые то инициализирую нулями
                */
                if (empty($item[5])) {
                    $item[5] = 0;
                }
                if (empty($item[6])) {
                    $item[6] = 0;
                }

                /*
                 * Добавляю НДФЛ
                 */
                if (isset($_POST["submit_import"])) {
                    try {
                        $NDFL->add([trim($item[1]), trim($item[5]), trim($item[6]), $neededObject[0]->getId()]);
                    } catch (Exception $e) {
                        $Errors->add(["Ошибка при добавлении.", $e->getMessage()]);
                    }
                } else if (isset($_POST["submit"])) {
                    $SupperArray[] = [trim($item[1]), trim($item[2]), $neededObject[0]->getLastName(), $neededObject[0]->getName(), $neededObject[0]->getMiddle(), trim($item[5]), trim($item[6]), $neededObject[0]->getId()];
                }
            }
        }

        /*
        * Удаляем из массива данных выборку
        */
        foreach ($member_keys as $kkk => $value) {
            unset($array[$key]);
            unset($members[$value]);
        }
    }

    /*
     * Получаю сотрудников без данных
     */

    /*
     * Не отчисляются НДФЛ
     * Кладовщик
     * Грузчик
     * Уборщица
     * Гибщик
     */

    $work_members_without_awh = array_filter($members, function ($e) use ($date) {
        return $e->getWorkStatus() == 1 && ($e->getPosition()['id'] != 13 && $e->getPosition()['id']!=5 && $e->getPosition()['id']!=34 && $e->getPosition()['id']!=4);
    });

    /*
     * Получаю статус
     */
    if (isset($_POST["submit_import"])) {
        if ($RS->getStatus($date, 1)['status_id'] == 0) {
            /*
             * Проверяю все данные для установки статуса, в зависимости от типа оплаты
             */
            if ($RS->checkStatus($date, 1)) {
                /*
                 * Устанавливаю статус
                 */
                $RS->ChangeStatus($item[0], 1, 1);
            };
        }
    }


    sort($array);
    $uploadFile->DeleteFile();

    if (isset($_POST["submit_import"])) {
        try {
            $Table = $NDFL->get($date);
        } catch (Exception $e) {
            $Errors->add("NDFL Error: " . $e->getMessage());
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Импорт НДФЛ</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <?php
            /*
                 * Вывод информации об ощибках
                 */
            if ($Errors->getCount() > 0) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="Table3">
                            <thead>
                            <th>Причина</th>
                            <th>Сообщение</th>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($Errors->getErrors() as $value) {
                                echo "<tr><td>" . $value['0'] . "</td><td>" . $value[1] . "</td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php
            }

            if (isset($_POST["submit_import"])) {
                if (count($Table) > 0) {
                    ?>
                    <div class="box box-success box-solid">
                        <div class="box-header"><h4>Добавлено</h4></div>
                        <div class="box-body">
                            <table class="table table-bordered" id="MembersTable">
                                <thead>
                                <th>Дата</th>
                                <th>Фамилия</th>
                                <th>Имя</th>
                                <th>Отчество</th>
                                <th>НДФЛ</th>
                                <th>ИЛ</th>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($Table)) {
                                    foreach ($Table as $value) {
                                        echo '<tr>';
                                        echo "<td>" . $value['date'] . "</td><td>" . $value['lastname'] . "</td><td>" . $value['name'] . "</td><td>" . $value['middle'] . "</td><td>" . $value['ndfl'] . "</td><td>" . $value['ro'] . "</td>";
                                        echo '</tr>';
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php
                }
            }

            if (isset($_POST["submit"])) {
                if (count($array) > 0) {
                    ?>

                    <div class="box box-danger box-solid">
                        <div class="box-header">
                            <h4>Ошибки</h4>
                        </div>
                        <div class="box-body">

                            <table class="table table-bordered">
                                <thead>
                                <th>Таб. номер</th>
                                <th>ФИО</th>
                                <th>Должность</th>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($array as $key => $value) {
                                    echo "<tr>";
                                    echo "<td>$value[2]</td><td>$value[3]</td><td>$value[4]</td>";
                                    echo "</tr>";
                                }
                                ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <?php
                }
                ?>

                <?php
                if (count($SupperArray) > 0) {
                    ?>
                    <div class="box box-success box-solid">
                        <div class="box-body">
                            <table class="table table-bordered" id="MembersTable">
                                <thead>
                                <th>Дата</th>
                                <th>Таб. номер</th>
                                <th>Фамилия</th>
                                <th>Имя</th>
                                <th>Отчество</th>
                                <th>НДФЛ</th>
                                <th>ИЛ</th>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($SupperArray)) {
                                    foreach ($SupperArray as $value) {
                                        echo '<tr>';
                                        echo "<td>$value[0]</td><td>$value[1]</td><td><a href='profile.php?id=" . $value[7] . "'>$value[2]</a></td><td><a href='profile.php?id=" . $value[7] . "'>$value[3]</a></td><td><a href='profile.php?id=" . $value[7] . "'>$value[4]</a></td><td>$value[5]</td><td>$value[6]</td>";
                                        echo '</tr>';
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>

            <div class="box box-solid">
                <form method="post" enctype="multipart/form-data">
                    <div class="box-header">
                        <h4>Блок импорта НДФЛ/ИЛ данных из CSV</h4>
                    </div>
                    <div class="box-body">
                        <div class="box box-solid box-info">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Выберите файл: </label>
                                    <input type="file" name="CSVfileToUpload" id="CSVfileToUpload">

                                    <p class="help-block">
                                        В CSV файле поля должны быть разделены с помощью точки с запятой';'.
                                    </p>
                                    <p class="help-block">
                                        Импортируются поля в следующей последовательности:
                                    <ul class="help-block">
                                        <li>№</li>
                                        <li>Дата</li>
                                        <li>Табельный номер</li>
                                        <li>ФИО</li>
                                        <li>Всего</li>
                                        <li>НДФЛ</li>
                                        <li>ИЛ</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-flat btn-sm btn-success" name="submit"><i class="fa fa-check"></i> Проверить</button>
                                <button type="submit" class="btn btn-flat btn-sm btn-primary" name="submit_import"><i class="fa fa-save"></i> Импортировать</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <?php
            /*
             * Вывод информации об ощибках
             */
            if (isset($work_members_without_awh) && count($work_members_without_awh) > 0) {
                ?>
                <div class="modal fade" id="modalError" style="background-color: #1f292e">
                    <div style="width: 90%;" class="modal-dialog">
                        <div style="width: 100%;" class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h5>Нету данных НДФЛ и ИЛ по пользователям</h5>
                            </div>

                            <div class="modal-body">
                                <table class="table table-striped table-responsive">
                                    <thead>
                                    <th>Фамилия</th>
                                    <th>Имя</th>
                                    <th>Отчество</th>
                                    <th>Отдел</th>
                                    <th>Должность</th>
                                    <th>Дата приема</th>
                                    <th>Дата увольнения</th>
                                    </thead>
                                    <tbody>
                                    <?php

                                    foreach ($work_members_without_awh as $obj) {
                                        echo "<tr>";
                                        echo "<td>".$obj->getLastName()."</td><td>".$obj->getName()."</td><td>".$obj->getMiddle()."</td>";
                                        echo "<td>".$obj->getDepartment()['name']."</td><td>".$obj->getPosition()['name']."</td>";
                                        echo "<td>".$obj->getEmploymentDate()."</td>";
                                        if ($obj->getDismissalDate() != "01.01.9999") {
                                            echo "<td>" . $obj->getDismissalDate() . "</td>";
                                        } else {
                                            echo "<td></td>";
                                        }
                                        echo "</tr>";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>

<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<?php
if (isset($work_members_without_awh) && count($work_members_without_awh) > 0) {
    echo "<script> $('#modalError').modal('show'); </script>";
}
?>

<script>
    $(function () {

        $('#MembersTable').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })

        $('#Table3').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })

    })
</script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>

