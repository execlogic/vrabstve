<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.10.18
 * Time: 18:30
 */

// Время работы скрипта
$start = microtime(true);

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";
require_once "app/Data.php";

require_once 'app/Notify.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

// Настройки
//if (!array_filter($Roles, function ($Role) {
//    return ($Role->getReportRetetition() == 1);
//})) {
//    header("Location: 404.php" );
//}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

$date_m = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');


$date_s = date("Y-m-d");
$date_e=$date_s;

$date_array = array();

if (isset($_POST['date_s']) && isset($_POST['date_e'])) {


    $date_s = $_POST['date_s'];
    $date_e = $_POST['date_e'];

    if (strtotime($date_s)>strtotime($date_e)) {
        echo "Дата начала не может быть больше!<BR>";
    } else {
        $DATA = new Data();

        $start = new DateTime($date_s);
        $end   = new DateTime($date_e);

        $aData = $DATA->getMakeupDepartment($start->format("Y-m-d"), $end->format("Y-m-d"));
    }
}
$menu_open = 3;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Отчет по выгруженной наценки из 1С</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- daterange picker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-daterangepicker/daterangepicker.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
    <style type="text/css">
        .rollover{
            position:relative
        }
        .rollover .tip{
            position:absolute;
            top:-20px;
            left:50%;
            width:200px;
            margin-left:-75px;
            background: #eff3fd;
            padding: 10px;
            display:none;
            border: 1px solid #e1e5ef;
            z-index: 100;
        }
        .rollover:hover .tip{
            display:block
        }
    </style>
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
                Отчет по выгруженной наценки из 1С
            </h4>
            <ol class="breadcrumb">
                <li class="active">Журнал ведомостей</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="box box-solid box-plane">
                <div class="box-body">
                    <form class="form-horizontal" method="post">


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Выберите дату c: </label>
                            <div class="col-sm-3">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker_s" name="date_s" value="<?php echo $date_s; ?>" autocomplete="off">
                                </div>
                            </div>
                            <label class="col-sm-2 control-label">по: </label>
                            <div class="col-sm-3">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker_e" name="date_e" value="<?php echo $date_e; ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <input type="submit" class="btn btn-flat btn-default" name="SendDate" value="Получить данные">
                        </div>
                    </form>
                </div>
            </div>

            <div class="box box-solid box-plane">
                <div class="box-body">
                    <table class="table table-striped table-hover" id="DataTable">
                        <thead>
                        <tr>
                            <th>ID отдела</th>
                            <th>Отдел</th>
                            <th>Сумма наценки</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $makeup_sum = 0;
                        foreach ($aData as $item) {
                            $makeup_sum += $item['sum'];
                            echo "<tr>";
                            echo "<td>".$item['department_id']."</td>";
                            echo "<td>".$item['name']."</td>";
                            echo "<td>". number_format($item['sum'], 2, ',', ' ') ."</td>";
                            echo "</tr>";
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="2"></th>
                            <th><?= number_format($makeup_sum, 2, ',', ' '); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- date-range-picker -->
<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
    $(function () {
        $('#DataTable').DataTable({
            'paging': false,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': false,
            'autoWidth': true
        })

        // Выбор даты
        $('#datepicker_s').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "days",
            minViewMode: "days",
            format: 'yyyy-mm-dd',
        })

        $('#datepicker_e').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "days",
            minViewMode: "days",
            format: 'yyyy-mm-dd',
        })
    })
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>


