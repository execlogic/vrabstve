<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.10.18
 * Time: 18:30
 */

// Время работы скрипта
$start = microtime(true);

require_once 'app/ReportStatus.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

session_start();
if (!isset($_SESSION['UserObj'])) {
    header("Location: index.php");
    exit();
}
$User = $_SESSION['UserObj'];

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}

// Настройки
if (!array_filter($Roles, function ($Role) {
    return ($Role->getReportJournal() == 1);
})) {
    header("Location: 404.php" );
    exit();
}


if (isset($_COOKIE['prepaid_expense_TypeOfPayment']))
    $TypeOfPayment = $_COOKIE['prepaid_expense_TypeOfPayment'];

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();
$date_m = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
$RS = new ReportStatus();

$date = date("m.Y");

if (isset($_REQUEST['TypeOfPayment'])) {
    $TypeOfPayment = $_REQUEST['TypeOfPayment'];
} else {
    $TypeOfPayment = 1;
}

if (isset($_REQUEST['date'])) {
    /*
     * Выкусываем теги
     */
    $_REQUEST['date'] = strip_tags($_REQUEST['date']);
    /*
     * Экранирую html спец. символы
     */
    $_REQUEST['date'] = htmlspecialchars($_REQUEST['date']);
    /*
     * в дате проверяю существование только цифр, и точки
     */
    $_REQUEST['date'] = preg_replace("/[^0-9.]/i", "", $_REQUEST['date']);

    /*
    * Объявляю переменные дата
    */
    $date = $_REQUEST['date'];


    $ReportStatus = $RS->getStatus($date, $TypeOfPayment);

    if (isset($_POST['OpenStatus'])) {
        $RS->InitReportStatus($date, $TypeOfPayment, $User->getMemberId());
        header('Location: journal.php?date='.$date."&TypeOfPayment=".$TypeOfPayment);
        die();
    }

} else {
    $ReportStatus = $RS->getStatus($date, $TypeOfPayment);
}

$Journal = $RS->getJournal(explode(".", $date)[1]);

$aJournal = [];
foreach ($Journal as $item) {
    $aJournal[$item['date_month']][$item['type_of_payment']] = $item;
}
//echo "<pre>".print_r($aJournal, true)."</pre><br>";
$menu_open = 3;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Журнал ведомостей</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
                Журнал ведомостей за <?php echo explode(".", $date)[1]; ?>
            </h4>
            <ol class="breadcrumb">
                <li class="active">Журнал ведомостей</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="box box-solid box-plane">
                <div class="box-body">
                    <div class="box box-info box-widget box-solid" style="border: unset">
                        <form class="form-horizontal" method="post">
                            <div class="box-header">
                                <h4>Открыть ведомость</h4>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Выберите дату: </label>
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="datepicker" name="date" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                                   value="<?php echo $date; ?>" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Тип выплаты: </label>
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-check"></i>
                                            </div>
                                            <select class="form-control" name="TypeOfPayment">
                                                <option value="1" <?php if (isset($TypeOfPayment) && ($TypeOfPayment == 1)) {
                                                    echo "selected";
                                                }; ?> >Премия
                                                </option>
                                                <option value="2" <?php if (isset($TypeOfPayment) && ($TypeOfPayment == 2)) {
                                                    echo "selected";
                                                }; ?>>Аванс
                                                </option>
                                                <option value="3" <?php if (isset($TypeOfPayment) && ($TypeOfPayment == 3)) {
                                                    echo "selected";
                                                }; ?>>Аванс2
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer text-center">
                                <div style="text-align: right;" class="col-sm-6">
                                    <input type="submit" class="btn btn-flat btn-primary" name="SendDate" value="Проверить">

                                    <?php
                                    if (array_filter($Roles, function ($Role) {
                                            return $Role->getId() == 1 || $Role->getId() == 6 || $Role->getId() == 5;
                                        }) && $ReportStatus['status_id'] == 0) {
                                        ?>
                                        <input type="submit" class="btn btn-flat btn-warning" name="OpenStatus" value="Открыть ведомость">
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box box-solid box-plane">
                <div class="box-body">
                    <table class="table table-striped table-hover" id="DataTable">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Месяц</th>
                            <th>Тип выплаты</th>
                            <th class="col-md-3">Статус</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
//                        for ($Month = 1; $Month <= 12; $Month++) {
                        foreach ($aJournal as $Month => $item) {
                            foreach ($aJournal[$Month] as $type_of_payment => $value) {
                                if ($type_of_payment>3)
                                    continue;

                                    $i++;
                                    echo "<tr>";
                                    echo "<td>".$i."</td>";
                                    echo "<td><a href='prepaid_expense.php?date=".$value['date']."&TypeOfPayment=".$value['type_of_payment']." '>" . $date_m[(int)$Month - 1] . "</td>";
                                    switch ($type_of_payment) {
                                        case 1:
                                            echo "<td>Премия</td>";
                                            break;
                                        case 2:
                                            echo "<td>Аванс</td>";
                                            break;
                                        case 3:
                                            echo "<td>Аванс2</td>";
                                            break;
                                    }

                                    echo "<td>";
                                    $WhoBrake = $RS->whoBrake($Month.".".explode(".",$date)[1], $type_of_payment);
                                    ?>
                                    <div class="info-box" style="box-shadow: unset; min-height:unset;margin-bottom:unset;background-color: unset;">
                                        <span class="info-box-icon bg-green" style="height: 45px; width: 45px;line-height: 45px;font-size:23px;color: white"><?php echo $value['status_id']; ?></span>
                                        <div class="info-box-content" style="margin-left: 45px; width: 100%;height: 45px !important;display: block;line-height: 30px;">
                                            <div data-html="true" class="custom-tooltip" data-toggle="tooltip"><a href='#' style="color: black" data-toggle="modal" data-target="#modal-whobrake-<?=$Month."_".$type_of_payment?>"><?php echo $value['status_name']; ?></a></div>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="modal-whobrake-<?=$Month."_".$type_of_payment?>">
                                        <div class="modal-dialog" style="width: 90%">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <h3 class="modal-title">Текущие стадии по отделам. <?php
                                                        switch ($type_of_payment) {
                                                            case 1:
                                                                echo "Премия.";
                                                                break;
                                                            case 2:
                                                                echo "Аванс.";
                                                                break;
                                                            case 3:
                                                                echo "Аванс2.";
                                                                break;
                                                        }
                                                        ?>  <?=$Month.".2022"?> </h3>
                                                </div>

                                                <div class="modal-body">
                                                    <div class="box box-solid box-info">
                                                        <div class="box-body">
                                                            <table class="table-striped table-condensed">
                                                                <thead>
                                                                <th>Статус</th>
                                                                <th>Отдел</th>
                                                                <th>Ответственный директор</th>
                                                                <th>Дата изменения последнего статуса</th>
                                                                <th>Кто провел на последний статус</th>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                if ($WhoBrake) {
                                                                    foreach ($WhoBrake as $key => $w) {
                                                                        switch ($w['status_id']) {
                                                                            case 1:
                                                                                $bgcolor = '#88CC66';
                                                                                break;
                                                                            case 2:
                                                                                $bgcolor = '#00CCAA';
                                                                                break;
                                                                            case 3:
                                                                                $bgcolor = '#0088cc';
                                                                                break;
                                                                            case 4:
                                                                                $bgcolor = '#0022cc';
                                                                                break;
                                                                            case 5:
                                                                                $bgcolor = '#4400cc';
                                                                                break;
                                                                            case 6:
                                                                                $bgcolor = '#AA00CC';
                                                                                break;
                                                                        }

                                                                        echo "<p style='background-color: #00cc22;'></p>";
                                                                        echo "<tr>";
                                                                        echo "<td class='col-md-2'><span class=\"info-box-icon\" style=\"background-color: " . $bgcolor . "; height: 25px; width: 25px;line-height: 25px;font-size:13px;color: white\">" . $w['status_id'] . "</span><span style='padding-left:5px;'>" . $w['status_name'] . "</span></td>";
                                                                        echo "<td class='col-md-2'><b>" . $w['name'] . "(" . $w['id'] . ")" . "</b></td>";
                                                                        echo "<td class='col-md-2'>" . (isset($w['director_name'])?$w['director_name']:"") . "</td>";
                                                                        echo "<td class='col-md-1'>" . $w['change_date'] . "</td>";
                                                                        if (is_null($w['change_member_name'])) {
                                                                            if ($w['change_member'] == -1)
                                                                                echo "<td >Администратор</td>";

                                                                            if ($w['change_member'] == -2)
                                                                                echo "<td >System</td>";
                                                                        } else {
                                                                            echo "<td class='col-md-1'>" . $w['change_member_name'] . "</td>";
                                                                        }
                                                                        echo "</tr>";
                                                                    }
                                                                }
                                                                ?>
                                                                </tbody>
                                                            </table>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    echo "</td>";
                                    echo "</tr>";
                            }
                        }
                        ?>



                        </tbody>
                    </table>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
    $(function () {
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            format: 'mm.yyyy'
        })
    });

    $(function () {
        $('#DataTable').DataTable({
            'paging': false,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': false,
            'autoWidth': true
        })

    })
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>


