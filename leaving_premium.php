<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 14.09.18
 * Time: 18:32
 */

// Время работы скрипта
$start = microtime(true);
require_once "app/ErrorInterface.php";
require_once "app/MemberInterface.php";
require_once 'app/CustomPercent.php';

require_once "app/MemberFinanceBlock.php";
require_once 'app/DataItem.php';
require_once 'app/DataManager.php';
require_once 'app/ErrorInterface.php';
require_once 'app/AdministrativeBlock.php';
require_once 'app/Retention.php';
require_once 'app/Correction.php';

require_once 'app/ProductionCalendar.php';
require_once 'app/AwhInterface.php';
require_once 'app/PayRollInterface.php';
require_once 'app/PayRoll.php';
require_once 'app/ReportStatus.php';
require_once "app/EnvelopeInterface.php";

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once "app/functions.php";

require_once 'app/Notify.php';
require_once 'app/LeavingPremium.php';
require_once "app/Premium.php";
require_once 'app/Member.php';

function roundSumm($summ)
{
    // Округление до 10
    if ($summ > 0) { //Если сумма <= 0 то не обрабатываем данные
        //$summ = round($summ); // округляю сотые и десятые
        $summ = (int)$summ;

        $ostatok = ($summ % 10); // Получаю остаток от деления на 10
        if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
            $summ = $summ - $ostatok + 10;
        } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
            $summ = $summ - $ostatok;
        };
    } else {
//        $summ = 0;
        $summ = (int)$summ;

        $ostatok = ($summ % 10)*-1; // Получаю остаток от деления на 10
        if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
            $summ = $summ - $ostatok - 10;
        } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
            $summ = $summ + $ostatok;
        };
    }
    return $summ;
}


session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

/*
 * Объект расчета
 */

$LP = new LeavingPremium();

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

$Error = new ErrorInterface();

$Permission_PremiyaCalculation = 0;



if ($_REQUEST['id']) {
    $user_id = $_REQUEST['id'];

    // Если не число, то ошибка выходим
    if (!is_numeric($user_id)) {
        echo "Error!";
        header('Location: index.php');
    }

    // Создаем объект MemberInterface
    $MI = new MemberInterface();

    // Получаю данные по пользователю
    try {
        $member = $MI->fetchMemberByID($user_id);
    } catch (Exception $e) {
        echo $e->getMessage();
        exit();
    }

    /** Права доступа */
    $member_department_id = $member->getDepartment()['id'];

    /** /Права доступа */
    $current = current(array_filter($Roles, function ($Role) use ($member_department_id) { // Если в ролях есть аванс и (отдел == 0 или отдел == отделу пользователя)
        return (($Role->getCommercialBlockNachenka() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
    }));

    if ($current) {
        $Permission_PremiyaCalculation = 1;
    }

    /**
     * Member $member
     **/
    $RolesMember = $RI->getRoles($member->getId());
    /*
     * Если номер роле 2 и из этого же отдела
     */
    /**
     * Получили роли увольняемого сотрудника
     * Перебираем их, если это руководитель отдела, то перебираем доступные роли
     * Если пользователя который смотрит ведомость роль руководителя и он из такого же отдела, то запрещаем
     * просмотр ведомости.
     */
    foreach ($RolesMember as $memberRole) {
        if ($memberRole->getId() == 2) {
            $member_is_ruk = array_filter($Roles, function ($Role) use ($member_department_id) {
                return (($Role->getId() == 2) && ($Role->getDepartment() == $member_department_id));
            });
            if ($member->getDismissalDate() != "01.01.9999" && $member_is_ruk) {
                $Permission_PremiyaCalculation = 0;
            }
        }
    }

    //Выставляю текущую дату, если в запросе не будет ['date']
    $date = date("m.Y");

    // Создаю объект ведомости
    $PI = new PayRollInterface();

    /*
     * Создаю объект статуса ведомости
     */
    $RS = new ReportStatus();

    /**
     * @prop Member $member
     */
    $employmentDate = DateTime::createFromFormat("d.m.Y", $member->getEmploymentDate());
    $dismissalDate = DateTime::createFromFormat("d.m.Y", $member->getDismissalDate());
    /**
     * @prop Member $member
     */
    if ($member->getWorkStatus() == 3) {
        $dismissalDate = DateTime::createFromFormat("d.m.Y", $member->getWorkStatusDate());
    }

//     Получаю месяцы за последние полгода
    for ($i = 0; $i < 6; $i++) {
        $testDate = new DateTime('now');
        $testDate->modify("first day of this month");
        $testDate->modify("-$i months");
//        $testDate->modify("last day of this month");

        if ($testDate->format("Ym")>$dismissalDate->format("Ym"))
            continue;

        if ($testDate->format("mY") == $employmentDate->format("mY")) {
            $months[] = $testDate->format("m.Y");
            break;
        }
        $months[] = $testDate->format("m.Y");
    }

    $Show_Button = false;
    $PR = new Premium();
    foreach ($months as $m) {
        if (!$PI->checkPaid($user_id, $m, 1)) {

            $fin = $PI->getFinancialPayout($user_id, $m, 4);
            /*
            * Мотивацию нужно проверять каждом месяце, а если она изменилась?
            */
            $member->setMotivation($MI->getMotivationByDate($user_id, $m));
            $TypeOfPayment = 1;
            if ($RS->getStatusMember($m, 4, $user_id, $TypeOfPayment)["status_id"] == 1) {
                $Show_Button = true;
            }

            $PR->setDate($m);
            try {
                $PR->fetch($member->getId(), true);
            } catch (Exception $e) {
                echo "Ошибка получения ведомости: ".$e->getMessage()."<BR>";
            }

            // Создаю объект ведомости и заполняю его
            $PayRoll = new PayRoll();
            $PayRoll->setDate($m); // Дата
            $PayRoll->setTypeOfPayment(4); // Тип выплаты
            $PayRoll->setId($member->getId()); // Пользовательский ID
            $PayRoll->setDirection($member->getDirection()); // Напраление
            $PayRoll->setDepartment($member->getDepartment()); // Отдел
            $PayRoll->setFio($member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()); // ФИО
            $PayRoll->setSalary($member->getSalary()); // Оклад
            $PayRoll->setNDFL($PR->getNdfl()); // НДФЛ
            $PayRoll->setRO($PR->getRo()); // ИЛ
            $PayRoll->setAdvancePayment($PR->getAllSalary());
            $PayRoll->setKPI($PR->getKPISumm());
            $PayRoll->setMobile($PR->getAllMobile());
            $PayRoll->setTransport($PR->getAllTransport());
            if ($RS->getStatusMember($m, 4, $user_id, $TypeOfPayment)["status_id"] == 1) {
                $PayRoll->setAbsences($PR->getAllAbsenceRub()); // Отсутствия
            } else {
                $PayRoll->setCommercial($fin['CommercialPremium']); // Коммерческая премия
            }

            if ($member->getMotivation() == 7) {
                $PayRoll->setCommercial(0); // Коммерческая премия
            } else {
                $PayRoll->setCommercial($PR->getAllMarkupRubles()); // Коммерческая премия
            }

//            $PayRoll->setCommercial($PR->getAllMarkupRubles()); // Коммерческая премия
            $PayRoll->setYearbonusPay($PR->getYearBonusPay());
            $PayRoll->setAdministrative($PR->getAllAdministrativeRubles()); // Административная премия
            $PayRoll->setHold($PR->getAllHoldRubles()); // Удержания
            $PayRoll->setCorrecting($PR->getAllCorrectionRubles()); // Корректировки
            $PayRoll->setMotivation($member->getMotivation());
            $PayRoll->setData($PR->getCommercialJson());
            $PayRoll->Calculation();

            //Добавляю в массив объектов
            $PayRollArray[] = $PayRoll;
        }
    }

    if (isset($_POST['NextStatus'])) {
        foreach ($PayRollArray as $item) {
            try {
                $RS->ChangeStatusMember($item->getId(), $item->getDate(), 4, 2, NULL);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

        unset($_POST);
        $Show_Button = false;
    }

    if ($LP->checkStatus($user_id)) {
        $Show_Button = false;
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Расчет сотрудника</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Расчет сотрудника
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Список пользователей</a></li>
                <li><a href="profile.php?id=<?php echo $user_id; ?>">Профиль сотрудника</a></li>
                <li class="active">Расчет сотрудника</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php
            /*
                 * Вывод информации об ощибках
                 */
            if ($Error->getCount() > 0) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="TableErr">
                            <thead>
                            <th>Сообщение</th>
                            </thead>
                            <tbody>
                            <?php
                            foreach (array_unique($Error->getErrors()) as $value) {
                                echo "<tr><td>" . $value . "</td><td></td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php
            }
            ?>

            <div class="row">
                <div class="col-md-12">

                    <div class="box box-solid">
                        <div class="box-header">
                            <h3>
                                <?php
                                // Вывожу ФИО
                                if ($member->getMaidenName()) {
                                    echo $member->getLastName() . " ( " . $member->getMaidenName() . " ) " . $member->getName() . " " . $member->getMiddle();
                                } else {
                                    echo $member->getLastName() . " " . $member->getName() . " " . $member->getMiddle();
                                };
                                ?>
                            </h3>
                        </div>
                    </div>


                    <form method="post" class="form-horizontal">
                        <div class="box box-solid box-pane box-widget" style="overflow-y: auto">
                            <form class="form-horizontal" method="post">
                                <div class="box-body">
                                    <table class="table table-striped table-hover" id="PayrollTable" style="overflow-y: auto">
                                        <thead>
                                        <tr>
                                            <th>Дата</th>
                                            <th>Направление</th>
                                            <th>Отдел</th>
                                            <th>ФИО</th>
                                            <?php
                                            if ($Permission_PremiyaCalculation) {
                                                ?>
                                                <th class="col-md-2">Оклад</th>
                                                <?php
                                                switch ($TypeOfPayment) {
                                                    case 1:
                                                        echo "<th class=\"col-md-2\">Выплаченный аванс</th>";
                                                        break;
                                                    case 2:
                                                        echo "<th class=\"col-md-2\">Аванс</th>";
                                                        break;
                                                }
                                                ?>

                                                <th class="col-md-2">Отсутствия</th>
                                                <th class="col-md-1">НДФЛ</th>
                                                <th class="col-md-1">ИЛ</th>
                                                <th>Коммерческая <br>премия(руб)</th>
                                                <th>KPI</th>
                                                <th>Мобильная связь(руб)</th>
                                                <th>ГСМ(руб)</th>
                                                <th>Административная <br>премия(руб)</th>
                                                <th>Удержания(руб)</th>
                                                <th>Отчисления в ГБ</th>
                                                <th>Корректировки(руб)</th>
                                                <th>Итого(руб)</th>
                                                <?php
                                            }
                                            ?>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php
                                        // Объявляю переменные для суммирвания колонок
                                        $BankSumm = 0; // Итого сумма в банк
                                        $ItogoSumm = 0; // Итого сумма
                                        $OkladSumm = 0; // Сумма оклада
                                        $AvansSumm = 0; // Сумма аванса
                                        $OtsutstviyaSumm = 0; // Сумма за отсутсвия
                                        $NDFLSumm = 0; // Сумма НДФЛ
                                        $ILSumm = 0; // Сумма ИЛ
                                        $CommersSumm = 0; // Сумма коммерческой премии
                                        $KPISumm = 0; // Итого сумма KPI
                                        $AdmSumm = 0; // Сумма административной премии
                                        $YderzSumm = 0;  // Сумма удержаний
                                        $CorrectSumm = 0; // Сумма коррекции
                                        $MobileSum = 0; // Мобильная связь
                                        $TransportSum = 0; // Транспорт
                                        $YearBonusPay = 0; //Удержания в ГБ


                                        foreach ($PayRollArray as $item) {
//                                            if ($item->getSumm() > 0) {
                                            $BankSumm += roundSumm($item->getSumm()); // Суммируем Итого Банк
//                                            };

                                            $ItogoSumm += roundSumm($item->getSumm()); // Суммируем Итого
                                            $OkladSumm += $item->getSalary();  // Суммеруем Оклад
                                            $OtsutstviyaSumm += $item->getAbsences(); // Суммируем отсутствия
                                            $NDFLSumm += $item->getNDFL(); // Суммируем НДФЛ
                                            $ILSumm += $item->getRO(); // Суммируем ИЛ
                                            $CommersSumm += $item->getCommercial(); // Суммируем коммерческую премию
                                            $KPISumm += $item->getKPI();
                                            $AdmSumm += $item->getAdministrative(); // Суммируем административную премию
                                            $YderzSumm += $item->getHold(); // Суммирую удержания
                                            $CorrectSumm += $item->getCorrecting(); // Суммирую корректировки
                                            $MobileSum += $item->getMobile();
                                            $TransportSum += $item->getTransport();
                                            if ($TypeOfPayment == 1) {
                                                $AvansSumm += $item->getAdvancePayment();
                                            };
                                            $YearBonusPay += $item->getYearBonusPay();


                                            // Выдерение цветом. Если сумма меньше 0, то помечаю желтым
                                            if ($item->getSumm() < 0) {
                                                echo "<tr class='warning'>";
                                            } else {
                                                echo "<tr>";
                                            }
                                            echo "<td>" . $item->getDate() . "</td>";

                                            echo "<td>" . $item->getDirection()['name'] . "</td>"; // Вывожу Направление
                                            echo "<td>" . $item->getDepartment()['name'] . "</td>"; // Вывожу отдел
                                            echo "<td><a href='premium.php?id=" . $item->getId() . "&date=" . $item->getDate() . "'>" . $item->getFio() . "</a></td>";
                                            if ($Permission_PremiyaCalculation) {
                                                // Вывожу оклад
                                                echo "<td style='text-align: center;'>" . number_format($item->getSalary(), 2, ',', ' ') . "</td>";
                                                // Вывожу выплаченный аванс//аванс
                                                echo "<td style='text-align: center;'>" . number_format($item->getAdvancePayment(), 2, ',', ' ') . "</td>";
                                                // Вывожу отсутсвия
                                                echo "<td style='text-align: center;'>" . number_format($item->getAbsences(), 2, ',', ' ') . "</td>";
                                                // Вывожу НДФЛ
                                                echo "<td style='text-align: center;'>" . number_format($item->getNDFL(), 2, ',', ' ') . "</td>";
                                                // Вывожу ИЛ
                                                echo "<td style='text-align: center;'>" . number_format($item->getRO(), 2, ',', ' ') . "</td>";
                                                // Вывожу коммерческую премию
                                                echo "<td style='text-align: center;'>" . number_format($item->getCommercial(), 2, ',', ' ') . "</td>";
                                                // Вывожу административную премию
                                                echo "<td style='text-align: center;'>" . number_format($item->getKPI(), 2, ',', ' ') . "</td>";

                                                echo "<td style='text-align: center;'>" . number_format($item->getMobile(), 2, ',', ' ') . "</td>";
                                                echo "<td style='text-align: center;'>" . number_format($item->getTransport(), 2, ',', ' ') . "</td>";

                                                echo "<td style='text-align: center;'>" . number_format($item->getAdministrative(), 2, ',', ' ') . "</td>";

                                                // Вывожу удержания
                                                echo "<td style='text-align: center;'>" . number_format($item->getHold(), 2, ',', ' ') . "</td>";
                                                echo "<td style='text-align: right'>" . number_format($item->getYearBonusPay(), 2, ',', ' ') . "</td>";
                                                // ВЫвожу корректировки
                                                echo "<td style='text-align: center;'>" . number_format($item->getCorrecting(), 2, ',', ' ') . "</td>";
                                                // Вывожу сумму
                                                echo "<td style='text-align: center;'>" . number_format(roundSumm($item->getSumm()), 2, ',', ' ') . "</td>";
                                            }
                                            echo "</tr>";
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <!--  Вывожу все просуммированное выше   -->
                                            <th style="font-size: 14px" colspan="4">Итого:</th>
                                            <?php
                                            if ($Permission_PremiyaCalculation) {
                                                ?>
                                                <th style='text-align: center; font-size: 14px;'><?php echo number_format($OkladSumm, 2, ',', ' '); ?></th>
                                                <th style='text-align: center; font-size: 14px;'><?php echo number_format($AvansSumm, 2, ',', ' '); ?></th>
                                                <th style='text-align: center; font-size: 14px;'><?php echo number_format($OtsutstviyaSumm, 2, ',', ' '); ?></th>
                                                <th style='text-align: center; font-size: 14px;'><?php echo number_format($NDFLSumm, 2, ',', ' '); ?></th>
                                                <th style='text-align: center; font-size: 14px;'><?php echo number_format($ILSumm, 2, ',', ' '); ?></th>
                                                <th style='text-align: center; font-size: 14px;'><?php echo number_format($CommersSumm, 2, ',', ' '); ?></th>
                                                <th style='text-align: center; font-size: 14px;'><?php echo number_format($KPISumm, 2, ',', ' '); ?></th>
                                                <th style='text-align: center; font-size: 14px;'><?php echo number_format($MobileSum, 2, ',', ' '); ?></th>
                                                <th style='text-align: center; font-size: 14px;'><?php echo number_format($TransportSum, 2, ',', ' '); ?></th>
                                                <th style='text-align: center; font-size: 14px;'><?php echo number_format($AdmSumm, 2, ',', ' '); ?></th>
                                                <th style='text-align: center; font-size: 14px;'><?php echo number_format($YderzSumm, 2, ',', ' '); ?></th>
                                                <th style='text-align: right; font-size: 14px;'><?=number_format($YearBonusPay, 2, ',', ' ') ?></th>
                                                <th style='text-align: center; font-size: 14px;'><?php echo number_format($CorrectSumm, 2, ',', ' '); ?></th>
                                                <th nowrap="" style='text-align: center; font-size: 14px;'><?php echo number_format($ItogoSumm, 2, ',', ' '); ?></th>
                                                <?php
                                            }
                                            ?>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    <?php
                                    if ($Permission_PremiyaCalculation) {
                                        ?>
                                        <div class="box box-solid box-info">
                                            <div class="box-body">
                                                <div class="col-md-3 text-left"><b>Итого: <b></b></div>

                                                <div class="col-md-9 text-right"><b></b><?php echo number_format($BankSumm, 2, ',', ' '); ?></b> <span class="fa fa-ruble" style="font-size:12px;"></span></div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>


                                </div>
                                <div class="box-footer">
                                    <?php
                                    if ($Show_Button && $Permission_PremiyaCalculation) {
                                        ?>
                                        <button class="btn btn-flat btn-success" name="NextStatus">На проверку</button>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </form>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
require_once 'footer.php';
?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    // Выбор даты
    $(function () {
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            format: 'mm.yyyy',
            //startDate: <?php //echo '\''.$member->getEmploymentDate().'\''; ?>//,
        })
    });


    // Комментарии бонус и корректировка
    $(document).ready(function () {
        $("textarea").each(function (i) {
            $(this).hide();
        });

        $(document).on("click", "div[id^=btn_comment]", function () {
            var currentId = $(this).attr("id");
            var currentName = $(this).attr("name");
            var numberInID = currentId.replace("btn_comment_", "");


            if ($("#" + currentName + "_note_" + numberInID).is(':visible')) {
                $("#" + currentName + "_" + numberInID).show();
                $("#" + currentName + "_note_" + numberInID).hide();
            } else {
                $("#" + currentName + "_" + numberInID).hide();
                $("#" + currentName + "_note_" + numberInID).show();
            }
        });

        $(document).on("change", "textarea", function () {
            var currentVal = $(this).val();
            var currentId = $(this).attr("id");
            var Arr = currentId.split("_"); // bonus_note_
            var numberInID = currentId.replace(Arr[0] + "_note_", "");

            $("#" + Arr[0] + "_" + numberInID).attr("title", currentVal);
            $("#" + Arr[0] + "_note_" + numberInID).hide();
            $("#" + Arr[0] + "_" + numberInID).show();
        })
    });

    // Изменение бонуса
    $(document).ready(function () {
        $(document).on("change", "input[id^=bonus]",
            function () {

                var Bonus = $(this).val();
                var currentId = $(this).attr("id");
                var numberInID = currentId.replace("bonus_", "");

                var CommercialCorrectionProcent = 1;
                if ($("#CommercialCorrectingProcent").val() != "undefined") {
                    CommercialCorrectionProcent = $("#CommercialCorrectingProcent").val();
                    CommercialCorrectionProcent = CommercialCorrectionProcent.replace(",", ".");
                }

                if (!Bonus) {
                    Bonus = 0;
                } else {
                    Bonus = Bonus.replace(",", ".");
                }

                var Correcting = $("#correcting_" + numberInID).val();
                if (!Correcting) {
                    Correcting = 0;
                } else {
                    Correcting = Correcting.replace(",", ".");
                }

                var Profit = $("#profit_" + numberInID).html();
                var Procent = $("#procent_" + numberInID).html();
                var Summ = $("#summ_" + numberInID).html();

                // profit, correcting, procent, summ
                var NewSumm = (parseFloat(Profit) + parseFloat(Bonus) + parseFloat(Correcting)) * parseFloat(CommercialCorrectionProcent) * parseFloat(Procent) / 100;
                if (!$.isNumeric(NewSumm)) {
                    NewSumm = 0;
                }

                $("#summ_" + numberInID).html(NewSumm.toFixed(3));
                $("#rubsumm_" + numberInID).html((NewSumm * 25).toFixed(3));

                var FinalSumm = 0;
                $("div[id^=summ]").each(function () {
                    FinalSumm = parseFloat(FinalSumm) + parseFloat($(this).html());
                });

                // console.log(numberInID+" ======> "+currentVal+" "+Correcting+" "+Profit+" "+Procent+" "+Summ+" "+FinalSumm);
                $("#final_summ").html(FinalSumm.toFixed(3));
                $("#final_rubsumm").html((FinalSumm * 25).toFixed(3));
                $("#nac_summ_" + numberInID).html(parseFloat(Bonus) + parseFloat(Correcting) + parseFloat(Profit));
            });

    });

    // Изменение корректировки
    $(document).ready(function () {
        $(document).on("change", "input[id^=correcting]",
            function () {
                var Correcting = $(this).val();
                var currentId = $(this).attr("id");
                var numberInID = currentId.replace("correcting_", "");


                var CommercialCorrectionProcent = 1;
                if ($("#CommercialCorrectingProcent").val() != undefined) {
                    CommercialCorrectionProcent == $("#CommercialCorrectingProcent").val();
                    CommercialCorrectionProcent = CommercialCorrectionProcent.replace(",", ".");
                }

                if (!Correcting) {
                    Correcting = 0;
                } else {
                    Correcting = Correcting.replace(",", ".");
                }


                var Bonus = $("#bonus_" + numberInID).val();
                if (!Bonus) {
                    Bonus = 0;
                } else {
                    Bonus = Bonus.replace(",", ".");
                }

                var Profit = $("#profit_" + numberInID).html();
                var Procent = $("#procent_" + numberInID).html();
                var Summ = $("#summ_" + numberInID).html();

                // console.log(currentVal+" "+Premiya+" "+Profit+" "+Procent+" "+Summ+" "+FinalSumm);

                // profit, correcting, procent, summ
                var NewSumm = (parseFloat(Profit) + parseFloat(Correcting) + parseFloat(Bonus)) * parseFloat(CommercialCorrectionProcent) * parseFloat(Procent) / 100;
                if (!$.isNumeric(NewSumm)) {
                    NewSumm = 0;
                }

                $("#summ_" + numberInID).html(NewSumm.toFixed(3));
                $("#rubsumm_" + numberInID).html((NewSumm * 25).toFixed(3));


                var FinalSumm = 0;
                $("div[id^=summ]").each(function (i) {
                    FinalSumm = parseFloat(FinalSumm) + parseFloat($(this).html());
                });

                $("#final_summ").html(FinalSumm.toFixed(3));
                $("#final_rubsumm").html((FinalSumm * 25).toFixed(3));
                $("#nac_summ_" + numberInID).html(parseFloat(Correcting) + parseFloat(Bonus) + parseFloat(Profit));
            });
    });

    // Изменение корректировки процента
    $(document).ready(function () {
        $(document).on("change", "input[id^=CommercialCorrectingProcent]",
            function () {
                var CommercialCorrectionProcent = 1;

                if (($("#CommercialCorrectingProcent").val()) != undefined) {
                    CommercialCorrectionProcent = $("#CommercialCorrectingProcent").val();
                    CommercialCorrectionProcent = CommercialCorrectionProcent.replace(",", ".");
                }

                $("div[id^=profit_]").each(function (i) {
                    var currentId = $(this).attr("id");
                    var numberInID = currentId.replace("profit_", "");

                    var Profit = $("#profit_" + numberInID).html();  // Наценка
                    var Correcting = $("#correcting_" + numberInID).val(); // корректировка наценки
                    if (Correcting == "") {
                        Correcting = 0;
                    } else {
                        Correcting = Correcting.replace(",", ".");
                    }

                    var Bonus = $("#bonus_" + numberInID).val(); // Бонус
                    if (Bonus == "") {
                        Bonus = 0;
                    } else {
                        Bonus = Bonus.replace(",", ".");
                    }

                    var Procent = $("#procent_" + numberInID).html(); // Процент

                    // console.log("Profit: "+Profit+" Correcting: "+Correcting+" Bonus: "+Bonus+" Procent: "+Procent);

                    // profit, correcting, procent, summ
                    var NewSumm = (parseFloat(Profit) + parseFloat(Bonus) + parseFloat(Correcting)) * parseFloat(CommercialCorrectionProcent) * parseFloat(Procent) / 100;
                    if (!$.isNumeric(NewSumm)) {
                        NewSumm = 0;
                    }

                    $("#summ_" + numberInID).html(NewSumm.toFixed(3));
                    $("#rubsumm_" + numberInID).html((NewSumm * 25).toFixed(3));

                    var FinalSumm = 0;
                    $("div[id^=summ]").each(function () {
                        FinalSumm = parseFloat(FinalSumm) + parseFloat($(this).html());
                    });

                    // console.log(numberInID+" ======> "+currentVal+" "+Correcting+" "+Profit+" "+Procent+" "+Summ+" "+FinalSumm);
                    $("#final_summ").html(FinalSumm.toFixed(3));
                    $("#final_rubsumm").html((FinalSumm * 25).toFixed(3));
                    $("#nac_summ_" + numberInID).html(parseFloat(Bonus) + parseFloat(Correcting) + parseFloat(Profit));

                    console.log($(this).attr("id"));

                });


            });
    });

</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>


