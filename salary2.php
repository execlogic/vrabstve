<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 14.09.18
 * Time: 18:32
 */

// Время работы скрипта
$start = microtime(true);
require_once "app/ErrorInterface.php";

require_once "app/MemberInterface.php";

require_once 'app/AdministrativeBlock.php';
require_once 'app/Retention.php';
require_once 'app/Correction.php';

require_once 'app/ProductionCalendar.php';
require_once 'app/AwhInterface.php';
require_once 'app/PayRollInterface.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

require_once 'app/PayHistory.php';

require_once 'app/Avans2.php';

require_once 'app/ReportStatus.php';
require_once 'app/PayRoll.php';
require_once 'app/PayRollInterface.php';

require_once 'app/TransportBlock.php';
require_once 'app/MobileBlock.php';


function roundSumm($summ)
{
    // Округление до 10
    if ($summ > 0) { //Если сумма <= 0 то не обрабатываем данные
        //$summ = round($summ); // округляю сотые и десятые
        $summ = (int)$summ;

        $ostatok = ($summ % 10); // Получаю остаток от деления на 10
        if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
            $summ = $summ - $ostatok + 10;
        } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
            $summ = $summ - $ostatok;
        };
    } else {
        $summ = 0;
    }
    return $summ;
}

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

$Permission_PremiyaCalculation = 0;

$Permission_CommercialBlockBonus = 0;
$Permission_CommercialBlockNachenka = 0;
$Permission_CommercialBlockProcent = 0;

$Permission_Promotions = 0;
$Permission_AdministrativePrize = 0;
$Permission_Training = 0;
$Permission_PrizeAnotherDepartment = 0;
$Permission_Defect = 0;
$Permission_Sunday = 0;
$Permission_NotLessThan = 0;
$Permission_MobileCommunication = 0;
$Permission_Overtime = 0;
$Permission_CompensationFAL = 0;

$Permission_LateWork = 0;
$Permission_Schedule = 0;
$Permission_Internet = 0;
$Permission_CachAccounting = 0;
$Permission_Receivables = 0;
$Permission_Credit = 0;
$Permission_Other = 0;

$Permission_BonusAdjustment = 0;
$Permission_Calculation = 0;
$Permission_YearBonus = 0;

$Permission_ShowAdvanceCalculation = 0;
$Permission_ShowCommercialBlock = 0;
$Permission_ShowAdministrativeBlock = 0;
$Permission_ShowRetitiotionBlock = 0;
$Permission_ShowCorrectingBlock = 0;
$Permission_ShowAWHBlock = 0;

$Permission_AWH = 0;

$AdministrativeBlock_Array = array();
$RetitiotionBlock_Array = array();
$CorrectingBlock_Array = array();

$Error = new ErrorInterface();

if ($_REQUEST['id']) {
    $user_id = $_REQUEST['id'];


    // Если не число, то ошибка выходим
    if (!is_numeric($user_id)) {
        header('Location: index.php');
        exit();
    }

    // Создаем объект MemberInterface
    $MI = new MemberInterface();
    // Получаю данные по пользователю
    try {
        $member = $MI->fetchMemberByID($user_id);
    } catch (Exception $e) {
        echo $e->getMessage();
        exit();
    }


    /** Права доступа */

    $member_department_id = $member->getDepartment()['id'];

    if ($User->getMemberId() != $user_id) {
        $current = current(array_filter($Roles, function ($Role) use ($member_department_id) { // Если в ролях есть аванс и (отдел == 0 или отдел == отделу пользователя)
            return (($Role->getPremiyaCalculation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        }));

        if ($current) { // Если существует объект, то можно смотреть и править.
            $Permission_PremiyaCalculation = 1;
        } else { // Если это не свой профиль и нету объекта ролей, то слать в 404
//            header("Location: 404.php");
        }
    };

    if ($User->getMemberId() == 94 && $member->getId() == 65) {
        $Permission_PremiyaCalculation = 1;

        $Permission_CommercialBlockBonus = 1;
        $Permission_CommercialBlockNachenka = 1;
        $Permission_CommercialBlockProcent = 1;

        $Permission_Promotions = 1;
        $Permission_AdministrativePrize = 1;
        $Permission_Training = 1;
        $Permission_PrizeAnotherDepartment = 1;
        $Permission_Defect = 1;
        $Permission_Sunday = 1;
        $Permission_NotLessThan = 1;
        $Permission_MobileCommunication = 1;
        $Permission_Overtime = 1;
        $Permission_CompensationFAL = 1;

        $Permission_LateWork = 1;
        $Permission_Schedule = 1;
        $Permission_Internet = 1;
        $Permission_CachAccounting = 1;
        $Permission_Receivables = 1;
        $Permission_Credit = 1;
        $Permission_Other = 1;

        $Permission_BonusAdjustment = 1;
        $Permission_Calculation = 1;
        $Permission_YearBonus = 1;

        $Permission_ShowAdvanceCalculation = 1;
        $Permission_ShowCommercialBlock = 1;
        $Permission_ShowAdministrativeBlock = 1;
        $Permission_ShowRetitiotionBlock = 1;
        $Permission_ShowCorrectingBlock = 1;
        $Permission_ShowAWHBlock = 1;

        $Permission_AWH = 1;

    }

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_CommercialBlockBonus) {
        if (($Role->getCommercialBlockBonus() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_CommercialBlockBonus = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_CommercialBlockNachenka) {
        if (($Role->getCommercialBlockNachenka() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_CommercialBlockNachenka = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_CommercialBlockProcent) {
        if (($Role->getCommercialBlockProcent() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_CommercialBlockProcent = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Promotions, &$AdministrativeBlock_Array) {
        if (($Role->getPromotions() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Promotions = 1;
            $AdministrativeBlock_Array[1] = 1;
            return true;
        }
        $AdministrativeBlock_Array[1] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_AdministrativePrize, &$AdministrativeBlock_Array) {
        if (($Role->getAdministrativePrize() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_AdministrativePrize = 1;
            $AdministrativeBlock_Array[2] = 1;
            return true;
        }
        $AdministrativeBlock_Array[2] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Training, &$AdministrativeBlock_Array) {
        if (($Role->getTraining() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Training = 1;
            $AdministrativeBlock_Array[3] = 1;
            return true;
        }
        $AdministrativeBlock_Array[3] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_PrizeAnotherDepartment, &$AdministrativeBlock_Array) {
        if (($Role->getPrizeAnotherDepartment() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_PrizeAnotherDepartment = 1;
            $AdministrativeBlock_Array[4] = 1;
            return true;
        }
        $AdministrativeBlock_Array[4] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Defect, &$AdministrativeBlock_Array) {
        if (($Role->getDefect() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Defect = 1;
            $AdministrativeBlock_Array[5] = 1;
            return true;
        }
        $AdministrativeBlock_Array[5] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Sunday, &$AdministrativeBlock_Array) {
        if (($Role->getSunday() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Sunday = 1;
            $AdministrativeBlock_Array[6] = 1;
            return true;
        }
        $AdministrativeBlock_Array[6] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_NotLessThan, &$AdministrativeBlock_Array) {
        if (($Role->getNotLessThan() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_NotLessThan = 1;
            $AdministrativeBlock_Array[7] = 1;
            return true;
        }
        $AdministrativeBlock_Array[7] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_MobileCommunication, &$AdministrativeBlock_Array) {
        if (($Role->getMobileCommunication() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_MobileCommunication = 1;
            $AdministrativeBlock_Array[8] = 1;
            return true;
        }
        $AdministrativeBlock_Array[8] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Overtime, &$AdministrativeBlock_Array) {
        if (($Role->getOvertime() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Overtime = 1;
            $AdministrativeBlock_Array[9] = 1;
            return true;
        }
        $AdministrativeBlock_Array[9] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_CompensationFAL, &$AdministrativeBlock_Array) {
        if (($Role->getCompensationFAL() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_CompensationFAL = 1;
            $AdministrativeBlock_Array[10] = 1;
            return true;
        }
        $AdministrativeBlock_Array[10] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_LateWork, &$RetitiotionBlock_Array) {
        if (($Role->getLateWork() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_LateWork = 1;
            $RetitiotionBlock_Array[1] = 1;
            return true;
        }
        $RetitiotionBlock_Array[1] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Schedule, &$RetitiotionBlock_Array) {
        if (($Role->getSchedule() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Schedule = 1;
            $RetitiotionBlock_Array[2] = 1;
            return true;
        }
        $RetitiotionBlock_Array[2] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Internet, &$RetitiotionBlock_Array) {
        if (($Role->getInternet() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Internet = 1;
            $RetitiotionBlock_Array[3] = 1;
            return true;
        }
        $RetitiotionBlock_Array[3] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_CachAccounting, &$RetitiotionBlock_Array) {
        if (($Role->getCachAccounting() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_CachAccounting = 1;
            $RetitiotionBlock_Array[4] = 1;
            return true;
        }
        $RetitiotionBlock_Array[4] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Receivables, &$RetitiotionBlock_Array) {
        if (($Role->getReceivables() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Receivables = 1;
            $RetitiotionBlock_Array[5] = 1;
            return true;
        }
        $RetitiotionBlock_Array[5] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Credit, &$RetitiotionBlock_Array) {
        if (($Role->getCredit() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Credit = 1;
            $RetitiotionBlock_Array[6] = 1;
            return true;
        }
        $RetitiotionBlock_Array[6] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Other, &$RetitiotionBlock_Array) {
        if (($Role->getOther() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Other = 1;
            $RetitiotionBlock_Array[7] = 1;
            return true;
        }
        $RetitiotionBlock_Array[7] = 1;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_BonusAdjustment, &$CorrectingBlock_Array) {
        if (($Role->getBonusAdjustment() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_BonusAdjustment = 1;
            $CorrectingBlock_Array[1] = 1;
            return true;
        }
        $CorrectingBlock_Array[1] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Calculation, &$CorrectingBlock_Array) {
        if (($Role->getCalculation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Calculation = 1;
            $CorrectingBlock_Array[2] = 1;
            return true;
        }
        $CorrectingBlock_Array[2] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_YearBonus, &$CorrectingBlock_Array) {
        if (($Role->getYearBonus() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_YearBonus = 1;
            $CorrectingBlock_Array[3] = 1;
            return true;
        }
        $CorrectingBlock_Array[3] = 0;
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ShowAdvanceCalculation) {
        if (($Role->getShowAdvanceCalculation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ShowAdvanceCalculation = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ShowCommercialBlock) {
        if (($Role->getShowCommercialBlock() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ShowCommercialBlock = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ShowAdministrativeBlock) {
        if (($Role->getShowAdministrativeBlock() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ShowAdministrativeBlock = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ShowRetitiotionBlock) {
        if (($Role->getShowRetitiotionBlock() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ShowRetitiotionBlock = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ShowCorrectingBlock) {
        if (($Role->getShowCorrectingBlock() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ShowCorrectingBlock = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ShowAWHBlock) {
        if (($Role->getShowAWHBlock() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ShowAWHBlock = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_AWH) {
//        if ($Role->getId() == 1 || $Role->getId() == 5 || $Role->getId() == 6) {
        if (($Role->getAWHEdit() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_AWH = 1;
            return true;
        } else {
            return false;
        };
    });

    $IS_RUK = 0;
    $IS_DIR = 0;

    foreach ($Roles as $R) {
        if ($R->getId() == 4) {
            $IS_DIR = 1;
            $IS_RUK = 0;
            break;
        }

        if (($R->getId() == 5) || ($R->getId() == 6)) {
            $IS_DIR = 2;
            $IS_RUK = 0;
            break;
        }

        if ($R->getId() == 2) {
            $IS_RUK = 1;
        }
    }

    $rtn = array_filter($Roles, function($R) {
        return ($R->getId()==5 || $R->getId()==6);
    });
    if ($rtn) {
        $IS_DIR = 2;
        $IS_RUK = 0;
    }


    $AV2 = new Avans2();
    $AV2->setMember($member);
//    $AV2->setAdministrativeBlockArray($AdministrativeBlock_Array);
//    $AV2->setRetitiotionBlockArray($RetitiotionBlock_Array);
//    $AV2->setCorrectingBlockArray($CorrectingBlock_Array);


    //Выставляю текущую дату, если в запросе не будет ['date']
    $date = date("m.Y");

    if (isset($_COOKIE['premium_date'])) {
        $date = $_COOKIE['premium_date'];
    }

    // Выставляю дату
    if (isset($_REQUEST['date']) || isset($_COOKIE['avans2_date'])) {
        if (isset($_REQUEST['date'])) {
            $date = $_REQUEST['date']; // то выставляю дату с новыми зачениями и начинаю разбор блока
            if (isset($_COOKIE['avans2_date'])) {
                setcookie("avans2_date", $date, time() + 3000);
            } else {
                setcookie("avans2_date", $date);
            }
        }

        $RS = new ReportStatus();
        if ($member->getWorkstatus() == 1) {
            $Status = $RS->getFiltredStatus($date, 3, $member_department_id)['status_id'];
        } else {
            $Status = $RS->getStatusMemberLeaving($date, $member->getId())['status_id'];
        }

        /*
         * Если руководитель офиса
         */
//        if ($member->getWorkstatus() == 1 && $IS_RUK == 1 && $IS_DIR == 0 && $Status > 1) {
//            $AV2->setPremiumLock();
//        }
        /*
         * Отвественный директор
         * Возможность исправлять только на стадии 1,2
         */
//        if ($member->getWorkstatus() == 1 && $IS_DIR == 1 && $Status > 2 ) {
//            $AV2->setPremiumLock();
//        }

        /*
         * Директор и Root
         * Возможность исправлять только на стадии 1,2,3,4
         */
//        if ($member->getWorkstatus() == 1 && $IS_DIR == 2 && $Status > 4) {
//            $AV2->setPremiumLock();
//        }

        $current = current(array_filter($Roles, function ($Role) use ($member_department_id) { // Если в ролях есть аванс и (отдел == 0 или отдел == отделу пользователя)
            return (($Role->getPremiyaCalculation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        }));

        if (!$current) { // Если существует объект, то можно смотреть и править.
            header("Location: 404.php");
        }

        $current = null;

        /*
        *  Создаю объект производственный календарь
        */
        $PC = new ProductionCalendar();
        /*
         *  Извлекаю из БД
         */
        $PC->fetchByDateWithDepartment($date, $member->getDepartment()['id']);

        $AV2->setDate($date);
        try {
            $AV2->fetch($user_id, true);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        /*
         * Получаю объекты
         */
        $AB = $AV2->getAB();
        $RN = $AV2->getRN();
        $CR = $AV2->getCR();
        $AWH = $AV2->getAWH();
        $TR = $AV2->getTR();
        $MB = $AV2->getMB();

        /*
         * Not Implemented
         */
//        $AV2->getPremiumHistory();
//        $PH = $AV2->getPH();

        if ($member->getEmploymentDate()) {
            $emp_date_arr = explode(".", $member->getEmploymentDate());
        }

        /* ****************************** Обработка событий ****************************** */
        if (isset($_REQUEST['date']) && isset($_POST['SendForm']) && ($Status < 5)) {
            /** ********************* Административный блок ********************* **/
            // Проверяю на существование переменной Promotions
            if (!is_null($AB->getPromotions()) && empty($_POST['Promotions'])) {
                $_POST['Promotions'] = "0";
            }
            if (!is_null($AB->getAdministrativePrize()) && empty($_POST['AdministrativePrize'])) {
                $_POST['AdministrativePrize'] = 0;
            }
            if (!is_null($AB->getTraining()) && empty($_POST['Training'])) {
                $_POST['Training'] = 0;
            }
            if (!is_null($AB->getPrizeAnotherDepartment()) && empty($_POST['PrizeAnotherDepartment'])) {
                $_POST['PrizeAnotherDepartment'] = 0;
            }
            if (!is_null($AB->getDefect()) && empty($_POST['Defect'])) {
                $_POST['Defect'] = 0;
            }
            if (!is_null($AB->getSunday()) && empty($_POST['Sunday'])) {
                $_POST['Sunday'] = 0;
            }
            if (!is_null($AB->getNotLessThan()) && empty($_POST['NotLessThan'])) {
                $_POST['NotLessThan'] = 0;
            }
            if (!is_null($MB->getSumm()) && empty($_POST['MobileCommunication'])) {
                $_POST['MobileCommunication'] = 0;
            }
            if (!is_null($AB->getOvertime()) && empty($_POST['Overtime'])) {
                $_POST['Overtime'] = 0;
            }
            if (!is_null($TR->getSumm()) && empty($_POST['CompensationFAL'])) {
                $_POST['CompensationFAL'] = 0;
            }

            if (isset($_POST['Promotions']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_Promotions == 1)) {
                // заменяю запятую на точку
                $_POST['Promotions'] = str_replace(",", ".", $_POST['Promotions']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Promotions'])) {
                    if ($AB->getPromotions() != $_POST['Promotions'] || $AB->getPromotionsNote() != $_POST['Promotions_note']) {
                        try {
                            $AB->addByType($user_id, $date, 1, $_POST['Promotions'], $_POST['Promotions_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной AdministrativePrize и AdministrativePrize не равен значению в профиле
            if (isset($_POST['AdministrativePrize']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_AdministrativePrize == 1)) {
                // заменяю запятую на точку
                $_POST['AdministrativePrize'] = str_replace(",", ".", $_POST['AdministrativePrize']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['AdministrativePrize'])) {
                    if ($AB->getAdministrativePrize() != $_POST['AdministrativePrize'] || $AB->getAdministrativePrizeNote() != $_POST['AdministrativePrize_note']) {
                        try {
                            $AB->addByType($user_id, $date, 2, $_POST['AdministrativePrize'], $_POST['AdministrativePrize_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Training
            if (isset($_POST['Training']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_Training == 1)) {
                // заменяю запятую на точку
                $_POST['Training'] = str_replace(",", ".", $_POST['Training']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Training'])) {
                    if ($AB->getTraining() != $_POST['Training'] || $AB->getTrainingNote() != $_POST['Training_note']) {
                        try {
                            $AB->addByType($user_id, $date, 3, $_POST['Training'], $_POST['Training_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной PrizeAnotherDepartment
            if (isset($_POST['PrizeAnotherDepartment']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_PrizeAnotherDepartment == 1)) {
                // заменяю запятую на точку
                $_POST['PrizeAnotherDepartment'] = str_replace(",", ".", $_POST['PrizeAnotherDepartment']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['PrizeAnotherDepartment'])) {
                    if ($AB->getPrizeAnotherDepartment() != $_POST['PrizeAnotherDepartment'] || $AB->getPrizeAnotherDepartmentNote() != $_POST['PrizeAnotherDepartment_note']) {
                        try {
                            $AB->addByType($user_id, $date, 4, $_POST['PrizeAnotherDepartment'], $_POST['PrizeAnotherDepartment_note'], $User->getMemberId());
//                        $AB->addPrizeAnotherDepartment($user_id, $date, $_POST['PrizeAnotherDepartment'], $_POST['PrizeAnotherDepartment_note']);
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Defect
            if (isset($_POST['Defect']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_Defect == 1)) {
                // заменяю запятую на точку
                $_POST['Defect'] = str_replace(",", ".", $_POST['Defect']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Defect'])) {
                    if ($AB->getDefect() != $_POST['Defect'] || $AB->getDefectNote() != $_POST['Defect_note']) {
                        try {
                            $AB->addByType($user_id, $date, 5, $_POST['Defect'], $_POST['Defect_note'], $User->getMemberId());
//                        $AB->addDefect($user_id, $date, $_POST['Defect'], $_POST['Defect_note']);
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Sunday
            if (isset($_POST['Sunday']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_Sunday == 1)) {
                // заменяю запятую на точку
                $_POST['Sunday'] = str_replace(",", ".", $_POST['Sunday']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Sunday'])) {
                    if ($AB->getSunday() != $_POST['Sunday'] || $AB->getSundayNote() != $_POST['Sunday_note']) {
                        try {
                            $AB->addByType($user_id, $date, 6, $_POST['Sunday'], $_POST['Sunday_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной NotLessThan
            if (isset($_POST['NotLessThan']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_NotLessThan == 1)) {
                // заменяю запятую на точку
                $_POST['NotLessThan'] = str_replace(",", ".", $_POST['NotLessThan']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['NotLessThan'])) {
                    if ($AB->getNotLessThan() != $_POST['NotLessThan'] || $AB->getNotLessThanNote() != $_POST['NotLessThan_note']) {
                        try {
                            $AB->addByType($user_id, $date, 7, $_POST['NotLessThan'], $_POST['NotLessThan_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной MobileCommunication и MobileCommunication не равен значению в профиле
            if (isset($_POST['MobileCommunication']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_MobileCommunication == 1)) {
                // заменяю запятую на точку
                $_POST['MobileCommunication'] = str_replace(",", ".", $_POST['MobileCommunication']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['MobileCommunication'])) {
                    if ($MB->getSumm() != $_POST['MobileCommunication'] || $MB->getNote() != $_POST['MobileCommunication_note']) {
                        try {
                            if (isset($_POST['MobileFormula'])) {
                                $MB->setSumm($_POST['MobileCommunication']);
                                $MB->setNote($_POST['MobileCommunication_note']);
                                $MB->recountMobile(($AV2->getAbsence() + $AV2->getNewAbsence() + $AV2->getHoliday() + $AV2->getDisease()), $PC->get()["working_days"]);
                            } else {
                                $MB->setMobile($date, 3, $user_id, $_POST['MobileCommunication'], $_POST['MobileCommunication_note'], $User->getMemberId(), 0);
                                $MB->setSumm($_POST['MobileCommunication']);
                                $MB->setNote($_POST['MobileCommunication_note']);
                            }
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Overtime
            if (isset($_POST['Overtime']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_Overtime == 1)) {
                // заменяю запятую на точку
                $_POST['Overtime'] = str_replace(",", ".", $_POST['Overtime']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Overtime'])) {
                    if ($AB->getOvertime() != $_POST['Overtime'] || $AB->getOvertimeNote() != $_POST['Overtime_note']) {
                        try {
                            $AB->addByType($user_id, $date, 9, $_POST['Overtime'], $_POST['Overtime_note'], $User->getMemberId());
//                        $AB->addOvertime($user_id, $date, $_POST['Overtime'], $_POST['Overtime_note']);
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной CompensationFAL
            if (isset($_POST['CompensationFAL']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_CompensationFAL == 1)) {
                // заменяю запятую на точку
                $_POST['CompensationFAL'] = str_replace(",", ".", $_POST['CompensationFAL']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['CompensationFAL'])) {
                    if ($TR->getSumm() != $_POST['CompensationFAL'] || $TR->getNote() != $_POST['CompensationFAL_note']) {
                        try {
                            if ($_POST['TransportFormula']) {
                                $TR->setSumm($_POST['CompensationFAL']);
                                $TR->setNote($_POST['CompensationFAL_note']);
                                $TR->recountCompensationFAL(($AV2->getAbsence() + $AV2->getNewAbsence() + $AV2->getHoliday() + $AV2->getDisease()), $PC->get()["working_days"]);
                            } else {
                                $TR->setTransport($date, 3, $user_id, $_POST['CompensationFAL'], $_POST['CompensationFAL_note'], $User->getMemberId(), 0);
                                $TR->setSumm($_POST['CompensationFAL']);
                                $TR->setNote($_POST['CompensationFAL_note']);
                            }
//                        $AB->addCompensationFAL($user_id, $date, $_POST['CompensationFAL'], $_POST['CompensationFAL_note']);
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }


            /** ********************* Удержания ********************* **/

            if (!is_null($RN->getLateWork()) && empty($_POST['LateWork'])) {
                $_POST['LateWork'] = 0;
            }
            if (!is_null($RN->getSchedule()) && empty($_POST['Schedule'])) {
                $_POST['Schedule'] = 0;
            }
            if (!is_null($RN->getInternet()) && empty($_POST['Internet'])) {
                $_POST['Internet'] = 0;
            }
            if (!is_null($RN->getCachAccounting()) && empty($_POST['CachAccounting'])) {
                $_POST['CachAccounting'] = 0;
            }
            if (!is_null($RN->getReceivables()) && empty($_POST['Receivables'])) {
                $_POST['Receivables'] = 0;
            }
            if (!is_null($RN->getCredit()) && empty($_POST['Credit'])) {
                $_POST['Credit'] = 0;
            }
            if (!is_null($RN->getOther()) && empty($_POST['Other'])) {
                $_POST['Other'] = 0;
            }

            // Проверяю на существование переменной LateWork
            if (isset($_POST['LateWork']) && ($Permission_ShowRetitiotionBlock == 1) && ($Permission_LateWork == 1)) {
                // заменяю запятую на точку
                $_POST['LateWork'] = str_replace(",", ".", $_POST['LateWork']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['LateWork'])) {
                    if ($RN->getLateWork() != $_POST['LateWork'] || $RN->getLateWorkNote() != $_POST['LateWork_note']) {
                        try {
                            $RN->addByType($user_id, $date, 1, $_POST['LateWork'], $_POST['LateWork_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Schedule
            if (isset($_POST['Schedule']) && ($Permission_ShowRetitiotionBlock == 1) && ($Permission_Schedule == 1)) {
                // заменяю запятую на точку
                $_POST['Schedule'] = str_replace(",", ".", $_POST['Schedule']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Schedule'])) {
                    if ($RN->getSchedule() != $_POST['Schedule'] || $RN->getScheduleNote() != $_POST['Schedule_note']) {
                        try {
                            $RN->addByType($user_id, $date, 2, $_POST['Schedule'], $_POST['Schedule_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Internet
            if (isset($_POST['Internet']) && ($Permission_ShowRetitiotionBlock == 1) && ($Permission_Internet == 1)) {
                // заменяю запятую на точку
                $_POST['Internet'] = str_replace(",", ".", $_POST['Internet']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Internet'])) {
                    if ($RN->getInternet() != $_POST['Internet'] || $RN->getInternetNote() != $_POST['Internet_note']) {
                        try {
                            $RN->addByType($user_id, $date, 3, $_POST['Internet'], $_POST['Internet_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной CachAccounting
            if (isset($_POST['CachAccounting']) && ($Permission_ShowRetitiotionBlock == 1) && ($Permission_CachAccounting == 1)) {
                // заменяю запятую на точку
                $_POST['CachAccounting'] = str_replace(",", ".", $_POST['CachAccounting']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['CachAccounting'])) {
                    if ($RN->getCachAccounting() != $_POST['CachAccounting'] || $RN->getCachAccountingNote() != $_POST['CachAccounting_note']) {
                        try {
                            $RN->addByType($user_id, $date, 4, $_POST['CachAccounting'], $_POST['CachAccounting_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной CachAccounting
            if (isset($_POST['Receivables']) && ($Permission_ShowRetitiotionBlock == 1) && ($Permission_Receivables == 1)) {
                // заменяю запятую на точку
                $_POST['Receivables'] = str_replace(",", ".", $_POST['Receivables']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Receivables'])) {
                    if ($RN->getReceivables() != $_POST['Receivables'] || $RN->getReceivablesNote() != $_POST['Receivables_note']) {
                        try {
                            $RN->addByType($user_id, $date, 5, $_POST['Receivables'], $_POST['Receivables_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Кредит
            if (isset($_POST['Credit']) && ($Permission_ShowRetitiotionBlock == 1) && ($Permission_Credit == 1)) {
                // заменяю запятую на точку
                $_POST['Credit'] = str_replace(",", ".", $_POST['Credit']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Credit'])) {
                    if ($RN->getCredit() != $_POST['Credit'] || $RN->getCreditNote() != $_POST['Credit_note']) {
                        try {
                            $RN->addByType($user_id, $date, 6, $_POST['Credit'], $_POST['Credit_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Other
            if (isset($_POST['Other']) && ($Permission_ShowRetitiotionBlock == 1) && ($Permission_Other == 1)) {
                // заменяю запятую на точку
                $_POST['Other'] = str_replace(",", ".", $_POST['Other']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Other'])) {
                    if ($RN->getOther() != $_POST['Other'] || $RN->getOtherNote() != $_POST['Other_note']) {
                        try {
                            $RN->addByType($user_id, $date, 7, $_POST['Other'], $_POST['Other_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            /** ********************* Корректировки ********************* **/

            if (!is_null($CR->getBonusAdjustment()) && empty($_POST['BonusAdjustment'])) {
                $_POST['BonusAdjustment'] = 0;
            }
            if (!is_null($CR->getCalculation()) && empty($_POST['Calculation'])) {
                $_POST['Calculation'] = 0;
            }

            // Проверяю на существование переменной BonusAdjustment
            if (isset($_POST['BonusAdjustment']) && ($Permission_ShowCorrectingBlock == 1) && ($Permission_BonusAdjustment == 1)) {
                // заменяю запятую на точку
                $_POST['BonusAdjustment'] = str_replace(",", ".", $_POST['BonusAdjustment']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['BonusAdjustment'])) {
                    if ($CR->getBonusAdjustment() != $_POST['BonusAdjustment'] || $CR->getBonusAdjustmentNote() != $_POST['BonusAdjustment_note']) {
                        try {
                            $CR->addByType($user_id, $date, 1, $_POST['BonusAdjustment'], $_POST['BonusAdjustment_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Calculation
            if (isset($_POST['Calculation']) && ($Permission_ShowCorrectingBlock == 1) && ($Permission_Calculation == 1)) {
                // заменяю запятую на точку
                $_POST['Calculation'] = str_replace(",", ".", $_POST['Calculation']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Calculation'])) {
                    if ($CR->getCalculation() != $_POST['Calculation'] || $CR->getCalculationNote() != $_POST['Calculation_note']) {
                        try {
                            $CR->addByType($user_id, $date, 2, $_POST['Calculation'], $_POST['Calculation_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной AnnualBonus
            if (isset($_POST['YearBonus'])) {
                // заменяю запятую на точку
                $_POST['YearBonus'] = str_replace(",", ".", $_POST['YearBonus']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['YearBonus'])) {
                    if ($CR->getYearBonus() != $_POST['YearBonus'] || $CR->getYearBonusNote() != $_POST['YearBonus_note']) {
                        try {
                            $CR->addByType($user_id, $date, 3, $_POST['YearBonus'], $_POST['YearBonus_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            if ($member->getWorkstatus() >= 2) {
                if (!is_null($CR->getHospital()) && empty($_POST['Hospital'])) {
                    $_POST['Hospital'] = 0;
                }

                if (!is_null($CR->getHoliday()) && empty($_POST['Holiday_R'])) {
                    $_POST['Holiday_R'] = 0;
                }
            }

            if (isset($_POST['Holiday_R']) && ($Permission_ShowCorrectingBlock == 1) && ($Permission_AWH == 1) && ($member->getWorkstatus() >= 2)) {
                // заменяю запятую на точку
                $_POST['Holiday_R'] = str_replace(",", ".", $_POST['Holiday_R']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Holiday_R'])) {
                    try {
                        $CR->addByType($user_id, $date, 4, $_POST['Holiday_R'], $_POST['Holiday_note'], $User->getMemberId());
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            // Проверяю на существование переменной Hospital
            if (isset($_POST['Hospital']) && ($Permission_ShowCorrectingBlock == 1) && ($Permission_AWH == 1) && ($member->getWorkstatus() >= 2)) {
                // заменяю запятую на точку
                $_POST['Hospital'] = str_replace(",", ".", $_POST['Hospital']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Hospital'])) {
                    try {
                        $CR->addByType($user_id, $date, 5, $_POST['Hospital'], $_POST['Hospital_note'], $User->getMemberId());
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            /** ******************** УРВ ******************** */
            if (isset($_POST['Absence'])) {
                $_POST['Absence'] = str_replace(",", ".", $_POST['Absence']);
                if (is_numeric($_POST['Absence']) && $_POST['Absence'] != $AV2->getAbsence()) {
                    try {
                        $AWH->setAbsenceByMember($user_id, $date, 1, $_POST['Absence']);
                        $AV2->setAbsence($_POST['Absence']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['Holiday'])) {
                $_POST['Holiday'] = str_replace(",", ".", $_POST['Holiday']);
                if (is_numeric($_POST['Holiday']) && $_POST['Holiday'] != $AV2->getHoliday()) {
                    try {
                        $AWH->setHolidayByMember($user_id, $date, 1, $_POST['Holiday']);
                        $AV2->setHoliday($_POST['Holiday']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['Disease'])) {
                $_POST['Disease'] = str_replace(",", ".", $_POST['Disease']);
                if (is_numeric($_POST['Disease']) && $_POST['Disease'] != $AV2->getDisease()) {
                    try {
                        $AWH->setDiseaseByMember($user_id, $date, 1, $_POST['Disease']);
                        $AV2->getDisease($_POST['Disease']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['NDFL'])) {
                $_POST['NDFL'] = str_replace(",", ".", $_POST['NDFL']);
                if (is_numeric($_POST['NDFL']) && $_POST['NDFL'] != $AV2->getNdfl()) {
                    try {
                        $AWH->setNDFLByMember($user_id, $date, $_POST['NDFL']);
//                        $member_NDFL['ndfl'] = $_POST['NDFL'];
                        $AV2->setNdfl($_POST['NDFL']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['RO'])) {
                $_POST['RO'] = str_replace(",", ".", $_POST['RO']);
                if (is_numeric($_POST['RO']) && $_POST['RO'] != $AV2->getRo()) {
                    try {
                        $AWH->setROByMember($user_id, $date, $_POST['RO']);
                        $AV2->getRo($_POST['RO']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['awh_note'])) {
                if ($_POST['awh_note'] != $AV2->getAwhNote()) {
                    try {
                        $AWH->setNoteByMember($user_id, $date, 1, $_POST['awh_note']);
                        $AV2->setAwhNote($_POST['awh_note']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['ExtraHoliday'])) {
                if ($_POST['ExtraHoliday'] != $AV2->getAwhExtraholiday()) {
                    try {
                        $AWH->setExtraHoliday($date, $user_id, $_POST['ExtraHoliday'], $User->getMemberId());
                        $AV2->setAwhExtraholiday($_POST['ExtraHoliday']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }


            $member_awh = $AWH->getAwhByMember($date, 1, $user_id); //Премия
            $awh_new_absence = $member_awh['new_absence'];
            $awh_absence = $member_awh['absence'];
            $awh_holiday = $member_awh['holiday'];
            $awh_disease = $member_awh['disease'];
            $awh_extraholiday = $AWH->getExtraHoliday($date, $user_id);
            $vse_otsutstviya = $awh_absence + $awh_new_absence + $awh_holiday + $awh_disease + $awh_extraholiday;
            $working_days_month = $PC->get()["working_days"];
            if ($member->getTransportFormula() == 1) {
                if ($TR->isAuto($date, 1, $user_id) == true) {
                    $TR->setSumm($member->getTransport());
                    $TR->recountCompensationFAL($vse_otsutstviya, $working_days_month);
                }
            }
            if ($member->getMobileFormula() == 1) {
                if ($MB->isAuto($date, 1, $user_id)==true) {
                    $MB->setSumm($member->getMobileCommunication());
                    $MB->recountMobile($vse_otsutstviya, $working_days_month);
                }
            }

            $AV2->fetch($user_id, true);

            if ($Status > 1 && $Status < 5) {
                /*
                * Создаю объект интерфейса ведомости
                */
                $PI = new PayRollInterface();
                // Создаю объект ведомости и заполняю его
                $PayRoll = new PayRoll();
                $PayRoll->setDate($date); // Дата

//                if ($member->getWorkstatus() == 1) {
                $PayRoll->setTypeOfPayment(3); // Тип выплаты
//                } else {
//                    $PayRoll->setTypeOfPayment(4); // Тип выплаты Расчет
//                }

                $PayRoll->setId($member->getId()); // Пользовательский ID

                $PayRoll->setDirection($member->getDirection()); // Напраление
                $PayRoll->setDepartment($member->getDepartment()); // Отдел
                $PayRoll->setPosition($member->getPosition()); // Должность


                $PayRoll->setFio($member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()); // ФИО
                $PayRoll->setSalary($member->getSalary()); // Оклад

                $PayRoll->setNDFL($AV2->getNdfl()); // НДФЛ
                $PayRoll->setRO($AV2->getRo()); // ИЛ
                $PayRoll->setAdvancePayment($AV2->getAllSalary()); // АВАНС
                $PayRoll->setAwhdata(["absence" => ($AV2->getAbsence() + $AV2->getNewAbsence()), "holiday" => $AV2->getHoliday(), "disease" => $AV2->getDisease()]);
                $PayRoll->setAdministrativeData($AV2->getAB_Data());
                $PayRoll->setRetentionData($AV2->getRN_Data());
                $PayRoll->setCorrectionData($AV2->getCR_Data());
                $PayRoll->setTransport($AV2->getAllTransport());
                $PayRoll->setMobile($AV2->getAllMobile());

                $PayRoll->setAbsences($AV2->getAllAbsenceRub()); // Отсутствия
                $PayRoll->setCommercial(0); // Коммерческая премия

                $PayRoll->setAdministrative($AV2->getAllAdministrativeRubles()); // Административная премия
                $PayRoll->setHold($AV2->getAllHoldRubles()); // Удержания
                $PayRoll->setCorrecting($AV2->getAllCorrectionRubles()); // Корректировки
                $PayRoll->setKPI(0);

                $PayRoll->setYearbonusPay($AV2->getYearBonusPay());

                $PayRoll->setMotivation($member->getMotivation());
                $PayRoll->Calculation();

                //Добавляю в массив объектов
                $PayRollArray[] = $PayRoll;

                foreach ($PayRollArray as $item) {
                    try {
                        $item->setSumm($item->getSumm());
                        $PI->UpdateFinancialPayout($item);
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }
                }
            }
        }
    }

    if (isset($_POST['rm_statment'])) {
        if ($MI->checkActivePayout($member->getId(), $date, 3)) {
            $MI->disableInPayout($member->getId(), $date, 3);
        } else {
            $MI->enableInPayout($member->getId(), $date, 3);
        }
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Аванс2 сотрудника</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Расчет Аванса2</h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Список пользователей</a></li>
                <li><a href="profile.php?id=<?php echo $user_id; ?>">Профиль сотрудника</a></li>
                <li class="active">Расчет Аванса2</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php
            /*
                 * Вывод информации об ощибках
                 */
            if ($Error->getCount() > 0) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="TableErr">
                            <thead>
                            <th>Сообщение</th>
                            </thead>
                            <tbody>
                            <?php
                            foreach (array_unique($Error->getErrors()) as $value) {
                                echo "<tr><td>" . $value . "</td><td></td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php
            }
            ?>

            <div class="row">
                <div class="col-md-12">

                    <div class="box box-solid">
                        <div class="box-header">
                            <h3>
                                <?php
                                // Вывожу ФИО
                                if ($member->getMaidenName()) {
                                    echo $member->getLastName() . " ( " . $member->getMaidenName() . " ) " . $member->getName() . " " . $member->getMiddle();
                                } else {
                                    echo $member->getLastName() . " " . $member->getName() . " " . $member->getMiddle();
                                };
                                ?>
                            </h3>
                        </div>
                        <div class="box-body">
                            <form class="form-horizontal" method="post">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Выберите дату: </label>
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="datepicker"
                                                   name="date" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                                   value="<?php echo $date; ?>" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-2"><input type="submit" class="btn btn-flat btn-default"
                                                                 name="SendDate" value="Получить данные"></div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <?php
                    if (isset($_REQUEST['date']) || isset($_COOKIE['avans2_date'])) {
                    ?>
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <?php if (($Permission_ShowAdvanceCalculation == 1) || ($User->getMemberId() == $user_id)) { ?>
                                <li class="active"><a href="#tab_0" onclick="document.cookie='Avans2Tab=0';"
                                                      data-toggle="tab" aria-expanded="true">Предварительный расчет</a>
                                </li>
                                <?php
                            };
                            ?>
                            <?php
                            // В зависимости от типа мотивации вывожу нужные табы.
                            if ($member->getMotivation() != 6) {
                                if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                    ?>
                                    <li><a href="#tab_2" onclick="document.cookie='PremiumTab=2';" data-toggle="tab"
                                           aria-expanded="false">Административный блок</a></li>
                                    <?php
                                };
                            } else {
                                if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                    ?>
                                    <li><a href="#tab_2" onclick="document.cookie='PremiumTab=2';" data-toggle="tab"
                                           aria-expanded="false">Административный блок</a></li>
                                    <?php
                                }
                            }

                            if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <li><a href="#tab_8" onclick="document.cookie='PremiumTab=8';" data-toggle="tab"
                                       aria-expanded="false">ГСМ</a></li>
                                <?php
                            } else {
                                if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                    ?>
                                    <li><a href="#tab_8" onclick="document.cookie='PremiumTab=8';" data-toggle="tab"
                                           aria-expanded="false">ГСМ</a></li>
                                    <?php
                                }
                            }

                            if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <li><a href="#tab_9" onclick="document.cookie='PremiumTab=9';" data-toggle="tab"
                                       aria-expanded="false">Мобильная связь</a></li>
                                <?php
                            } else {
                                if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                    ?>
                                    <li><a href="#tab_9" onclick="document.cookie='PremiumTab=9';" data-toggle="tab"
                                           aria-expanded="false">Мобильная связь</a></li>
                                    <?php
                                }
                            }
                            ?>

                            <?php
                            if (($Permission_ShowRetitiotionBlock == 1) || ($User->getMemberId() == $user_id)) { ?>
                                <li><a href="#tab_3" onclick="document.cookie='PremiumTab=3';" data-toggle="tab"
                                       aria-expanded="false">Удержания</a></li>
                            <?php };
                            if (($Permission_ShowCorrectingBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <li><a href="#tab_4" onclick="document.cookie='PremiumTab=4';" data-toggle="tab"
                                       aria-expanded="false">Корректировки</a></li>
                                <?php
                            };
                            if (($Permission_ShowAWHBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <li><a href="#tab_5" onclick="document.cookie='PremiumTab=5';" data-toggle="tab"
                                       aria-expanded="false">УРВ</a></li>
                                <?php
                            }
                            ?>
                            <?php
                            if ($Permission_ShowCommercialBlock || $Permission_ShowAdministrativeBlock || $Permission_ShowRetitiotionBlock || $Permission_ShowCorrectingBlock) {
                                ?>
                                <li><a href="#tab_6" onclick="document.cookie='PremiumTab=6';" data-toggle="tab"
                                       aria-expanded="false">История</a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>

                    <form method="post" id="form-data" class="form-horizontal">
                        <div class="tab-content">

                            <input type="hidden" name="date" value="<?php echo $date; ?>">

                            <?php
                            if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <div class="tab-pane" id="tab_2">
                                    <div class="box box-info box-solid">
                                        <div class="box-body">
                                            <?php if (($Permission_Promotions == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Акции</label>

                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Promotions"
                                                                   id="Promotions_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getPromotionsNote(); ?>"
                                                                   value="<?php echo $AB->getPromotions(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Promotions == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>

                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Promotions_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Promotions == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getPromotionsNote(); ?></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_AdministrativePrize == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Административная
                                                        премия</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   name="AdministrativePrize"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getAdministrativePrizeNote(); ?>"
                                                                   value="<?php echo $AB->getAdministrativePrize(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_AdministrativePrize == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?>>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='AdministrativePrize_note'
                                                                      cols='1'
                                                                      rows='1'
                                                                      class='form-control' <?php if ($Permission_AdministrativePrize == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getAdministrativePrizeNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_Training == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Обучение</label>

                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Training"
                                                                   id="Training_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getTrainingNote(); ?>"
                                                                   value="<?php echo $AB->getTraining(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Training == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>

                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat' name='Training'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Training_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Training == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getTrainingNote(); ?></textarea>

                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_PrizeAnotherDepartment == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Премия другого отдела</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   name="PrizeAnotherDepartment"
                                                                   id="PrizeAnotherDepartment_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getPrizeAnotherDepartmentNote(); ?>"
                                                                   value="<?php echo $AB->getPrizeAnotherDepartment(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_PrizeAnotherDepartment == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='PrizeAnotherDepartment_note'
                                                                      cols='1'
                                                                      rows='1'
                                                                      class='form-control' <?php if ($Permission_PrizeAnotherDepartment == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getPrizeAnotherDepartmentNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_Defect == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Неликвиды и брак</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Defect"
                                                                   id="Defect_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getDefectNote(); ?>"
                                                                   value="<?php echo $AB->getDefect(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Defect == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat' name='Defect'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Defect_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Defect == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getDefectNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_Sunday == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Воскресения</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Sunday"
                                                                   id="Sunday_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getSundayNote(); ?>"
                                                                   value="<?php echo $AB->getSunday(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Sunday == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Sunday_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Sunday == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getSundayNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_NotLessThan == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Выплаты не менее</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="NotLessThan"
                                                                   id="NotLessThan_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getNotLessThanNote(); ?>"
                                                                   value="<?php echo $AB->getNotLessThan(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_NotLessThan == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='NotLessThan_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_NotLessThan == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getNotLessThanNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_Overtime == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Переработки</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Overtime"
                                                                   id="Overtime_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getOvertimeNote(); ?>"
                                                                   value="<?php echo $AB->getOvertime(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Overtime == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat' name='Overtime'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Overtime_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Overtime == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getOvertimeNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>
                                        </div>
                                        <div class="box-footer">
                                            <?php
                                            if (($Permission_Promotions == 1 ||
                                                    $Permission_AdministrativePrize == 1 ||
                                                    $Permission_Training == 1 ||
                                                    $Permission_PrizeAnotherDepartment == 1 ||
                                                    $Permission_Defect == 1 ||
                                                    $Permission_Sunday == 1 ||
                                                    $Permission_NotLessThan == 1 ||
                                                    $Permission_Overtime == 1) && $AV2->getAdministrativeLock() != 1) {

                                                if ($Status != 6) {
                                                    echo '<input type="submit" class="btn btn-primary pull-right btn-flat" name="SendForm" value="Сохранить">';
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <div class="tab-pane" id="tab_8">
                                    <div class="box box-info box-solid">
                                        <div class="box-body">
                                            <?php if (($Permission_CompensationFAL == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Данные из профиля</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   placeholder="0"
                                                                   value="<?php echo $member->getTransport(); ?>"
                                                                   autocomplete="off"
                                                                   disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Компенсация ГСМ</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   name="CompensationFAL"
                                                                   id="CompensationFAL_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $TR->getNote(); ?>"
                                                                   value="<?php echo $TR->getSumm(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_CompensationFAL == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='CompensationFAL_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_CompensationFAL == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $TR->getNote(); ?></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                                <?php
//                                                echo "<input type=checkbox name=TransportFormula ".($member->getTransportFormula()?'checked':'')."> Пересчитать по формуле";
                                            };
                                            ?>
                                        </div>
                                        <div class="box-footer">
                                            <?php
                                            if (($Permission_CompensationFAL == 1) && $AV2->getAdministrativeLock() != 1) {
                                                if ($Status < 5) {
                                                    echo '<input type="submit" class="btn btn-primary pull-right btn-flat" name="SendForm" value="Сохранить">';
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <div class="tab-pane" id="tab_9">
                                    <div class="box box-info box-solid">
                                        <div class="box-body">
                                            <?php if (($Permission_MobileCommunication == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Данные из профиля</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   placeholder="0"
                                                                   value="<?php echo $member->getMobileCommunication(); ?>"
                                                                   autocomplete="off"
                                                                   disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Мобильная связь</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   name="MobileCommunication"
                                                                   id="MobileCommunication_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $MB->getNote(); ?>"
                                                                   value="<?php echo $MB->getSumm(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_MobileCommunication == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'
                                                                 name='MobileCommunication'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='MobileCommunication_note'
                                                                      cols='1'
                                                                      rows='1'
                                                                      class='form-control' <?php if ($Permission_MobileCommunication == 0 || $AV2->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $MB->getNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
//                                                echo "<input type=checkbox name=MobileFormula ".($member->getMobileFormula()?'checked':'')."> Пересчитать по формуле";
                                            };
                                            ?>
                                        </div>
                                        <div class="box-footer">
                                            <?php
                                            if (($Permission_MobileCommunication == 1) && $AV2->getAdministrativeLock() != 1) {
                                                if ($Status < 5) {
                                                    echo '<input type="submit" class="btn btn-primary pull-right btn-flat" name="SendForm" value="Сохранить">';
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (($Permission_ShowRetitiotionBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <div class="tab-pane" id="tab_3">
                                    <div class="box box-danger box-solid">
                                        <div class="box-body">

                                            <?php if (($Permission_LateWork == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">За опоздания</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="LateWork"
                                                                   id="LateWork_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $RN->getLateWorkNote(); ?>"
                                                                   value="<?php echo $RN->getLateWork(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_LateWork == 0 || $AV2->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat' name='LateWork'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='LateWork_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_LateWork == 0 || $AV2->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $RN->getLateWorkNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_Internet == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">За интернет</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Internet"
                                                                   id="Internet_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $RN->getInternetNote(); ?>"
                                                                   value="<?php echo $RN->getInternet(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Internet == 0 || $AV2->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Internet_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Internet == 0 || $AV2->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $RN->getInternetNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_CachAccounting == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Кассовый учет</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   name="CachAccounting"
                                                                   id="CachAccounting_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $RN->getCachAccountingNote(); ?>"
                                                                   value="<?php echo $RN->getCachAccounting(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_CachAccounting == 0 || $AV2->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='CachAccounting_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_CachAccounting == 0 || $AV2->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $RN->getCachAccountingNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_Receivables == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Дебиторская
                                                        задолженность</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Receivables"
                                                                   id="Receivables_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $RN->getReceivablesNote(); ?>"
                                                                   value="<?php echo $RN->getReceivables(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Receivables == 0 || $AV2->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Receivables_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Receivables == 0 || $AV2->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $RN->getReceivablesNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_Credit == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Кредит</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Credit"
                                                                   id="Credit_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $RN->getCreditNote(); ?>"
                                                                   value="<?php echo $RN->getCredit(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Credit == 0 || $AV2->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Credit_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Credit == 0 || $AV2->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $RN->getCreditNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_Other == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Другое</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Other"
                                                                   id="Other_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $RN->getOtherNote(); ?>"
                                                                   value="<?php echo $RN->getOther(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Other == 0 || $AV2->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat' name='Other'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Other_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Other == 0 || $AV2->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $RN->getOtherNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Отчисления в годовой бонус</label>
                                                <div class="col-md-10">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-ruble"></i>
                                                        </div>
                                                        <input type="text" class="form-control"
                                                               placeholder="0"
                                                               value="<?php echo $AV2->getYearBonusPay(); ?>"
                                                               autocomplete="off"
                                                               disabled
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <?php
                                            if ((
                                                    $Permission_LateWork == 1 ||
                                                    $Permission_Schedule == 1 ||
                                                    $Permission_Internet == 1 ||
                                                    $Permission_CachAccounting == 1 ||
                                                    $Permission_Receivables == 1 ||
                                                    $Permission_Credit == 1 ||
                                                    $Permission_Other == 1
                                                ) && $AV2->getRetentionLock() != 1) {
                                                if ($AV2->getPaymentMade() == false) {
                                                    echo '<input type="submit" class="btn btn-primary pull-right btn-flat" name="SendForm" value="Сохранить">';
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (($Permission_ShowCorrectingBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <div class="tab-pane" id="tab_4">
                                    <div class="box box-info box-solid">
                                        <div class="box-body">

                                            <?php if (($Permission_BonusAdjustment == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Корректировка бонуса</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   name="BonusAdjustment"
                                                                   id="BonusAdjustment_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $CR->getBonusAdjustmentNote(); ?>"
                                                                   value="<?php echo $CR->getBonusAdjustment(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_BonusAdjustment == 0 || $AV2->getCorrectingLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='BonusAdjustment_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_BonusAdjustment == 0 || $AV2->getCorrectingLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $CR->getBonusAdjustmentNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_Calculation == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Расчет</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Calculation"
                                                                   id="Calculation_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $CR->getCalculationNote(); ?>"
                                                                   value="<?php echo $CR->getCalculation(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Calculation == 0 || $AV2->getCorrectingLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Calculation_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Calculation == 0 || $AV2->getCorrectingLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $CR->getCalculationNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php
                                            if ($Permission_YearBonus == 1) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Годовой бонус</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="YearBonus"
                                                                   id="YearBonus_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $CR->getYearBonusNote(); ?>"
                                                                   value="<?php echo $CR->getYearBonus(); ?>"
                                                                   autocomplete="off"
                                                                <?php if ($Permission_YearBonus == 0 || $AV2->getCorrectingLock() == 1) {
                                                                    echo "disabled";
                                                                }; ?>
                                                            >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='YearBonus_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_YearBonus == 0 || $AV2->getCorrectingLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $CR->getYearBonusNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                            <?php
                                            if ($member->getWorkstatus() >= 2) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Корректировка отпуска
                                                        (+/-)</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Holiday_R"
                                                                   placeholder="Корректировка отпуска в днях"
                                                                   title="<?php echo $CR->getHolidayNote(); ?>"
                                                                   value="<?php echo $CR->getHoliday(); ?>"
                                                                   autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat' name='Holiday'>
                                                                <i class='fa fa-comment'></i></div>

                                                            <textarea name='Holiday_note'
                                                                      cols='1' rows='1' style="height: 34px;"
                                                                      class='form-control'><?php echo $CR->getHolidayNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Корректировка больничных
                                                        (-) </label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Hospital"
                                                                   placeholder="Корректировка больничных в днях"
                                                                   title="<?php echo $CR->getHospitalNote(); ?>"
                                                                   value="<?php echo $CR->getHospital(); ?>"
                                                                   autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat' name='Hospital'>
                                                                <i class='fa fa-comment'></i></div>
                                                            <textarea name='Hospital_note'
                                                                      cols='1' rows='1' style="height: 34px;"
                                                                      class='form-control'><?php echo $CR->getHospitalNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                        </div>
                                        <div class="box-footer">
                                            <?php
                                            if ((
                                                    $Permission_BonusAdjustment == 1 ||
                                                    $Permission_Calculation == 1 ||
                                                    $Permission_AWH == 1
                                                ) && $AV2->getCorrectingLock() != 1) {
                                                if ($AV2->getPaymentMade() == false) {
                                                    echo '<input type="submit" class="btn btn-primary pull-right btn-flat" name="SendForm" value="Сохранить">';
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (($Permission_ShowAWHBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <div class="tab-pane" id="tab_5">
                                    <div class="box box-info box-solid">
                                        <div class="box-body">

                                            <div class="panel" style="border: unset; box-shadow: unset">
                                                <div class="panel-body">
                                                    <?php
                                                    if (isset($_POST['date'])) {
                                                        ?>
                                                        <div class="box box-info box-solid">
                                                            <div class="box-body">
                                                                <label class="col-sm-2">Количество рабочих дней:</label>
                                                                <div class="col-md-10">
                                                                    <?php echo $AV2->getWorkingDays(); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>

                                                    <?php
                                                    if ($emp_date_arr[1] . "." . $emp_date_arr[2] == $date && $emp_date_arr[0] != 1) {
                                                        ?>
                                                        <div class="alert alert-warning alert-dismissible"
                                                             style="border-radius: unset">
                                                            <button type="button" class="close" data-dismiss="alert"
                                                                    aria-hidden="true">×
                                                            </button>
                                                            <h4><i class="icon fa fa-warning"></i> Внимание!</h4>
                                                            Сотрудник принят на
                                                            работу <?php echo $member->getEmploymentDate(); ?>.
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Отсутствия до выхода на работу</label>
                                                            <div class="col-md-10">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input type="text" class="form-control" name="Holiday"
                                                                           autocomplete="off"
                                                                           value="<?php echo $AV2->getNewAbsence(); ?>" disabled>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <?php
                                                    }
                                                    ?>

                                                    <table class="table table-hover table-striped">
                                                        <thead>
                                                        <th>Отсутствия</th>
                                                        <th>Отпуск</th>
                                                        <th>Болезнь</th>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><input type="text" class="form-control" name="Absence"
                                                                       id="Absence" autocomplete="off" placeholder="0"
                                                                       value="<?php echo $AV2->getAbsence(); ?>" <?php if ($Permission_AWH == 0 || $AV2->getAWHLock() == 1) {
                                                                    echo "disabled";
                                                                }; ?>></td>
                                                            <td><input type="text" class="form-control" name="Holiday"
                                                                       id="Holiday" autocomplete="off" placeholder="0"
                                                                       value="<?php echo $AV2->getHoliday(); ?>" <?php if ($Permission_AWH == 0 || $AV2->getAWHLock() == 1) {
                                                                    echo "disabled";
                                                                }; ?>></td>
                                                            <td><input type="text" class="form-control" name="Disease"
                                                                       id="Disease" autocomplete="off" placeholder="0"
                                                                       value="<?php echo $AV2->getDisease(); ?>" <?php if ($Permission_AWH == 0 || $AV2->getAWHLock() == 1) {
                                                                    echo "disabled";
                                                                }; ?>></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                    <div class="form-group">
                                                        <label class="col-sm-1 control-label pull-left">Заметка </label>
                                                        <div class="col-sm-11">
                                                            <div class="input-group date">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-comment"></i>
                                                                </div>
                                                                <textarea class="form-control pull-right" style="height: 34px;"
                                                                          name="awh_note" autocomplete="off"><?php echo $AV2->getAwhNote(); ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label pull-left">Дополнительный отпуск </label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" name="ExtraHoliday"
                                                                   id="ExtraHoliday" autocomplete="off" placeholder="0"
                                                                   value="<?php echo $AV2->getAwhExtraholiday(); ?>" <?php if ($Permission_AWH == 0 || $AV2->getAWHLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="panel" style="border: unset; box-shadow: unset">
                                                <div class="panel-body">
                                                    <table class="table table-hover table-striped">
                                                        <thead>
                                                        <th>НДФЛ</th>
                                                        <th>ИЛ</th>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><input type="text" class="form-control" name="NDFL"
                                                                       id="NDFL" autocomplete="off" placeholder="0"
                                                                       value="<?php echo $AV2->getNdfl(); ?>" <?php if ($Permission_AWH == 0 || $AV2->getAWHLock() == 1) {
                                                                    echo "disabled";
                                                                }; ?> ></td>
                                                            <td><input type="text" class="form-control" name="RO"
                                                                       id="RO" autocomplete="off" placeholder="0"
                                                                       value="<?php echo $AV2->getRo(); ?>" <?php if ($Permission_AWH == 0 || $AV2->getAWHLock() == 1) {
                                                                    echo "disabled";
                                                                }; ?> ></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="box-footer">
                                            <?php if ($Permission_AWH == 1 && $AV2->getAWHLock() == 0) {
                                                ?>
                                                <input type="submit" class="btn btn-danger btn-flat pull-right"
                                                       name="SendForm" value="Сохранить">
                                                <?php
                                            };
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (($Permission_ShowAdvanceCalculation == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <div class="tab-pane active" id="tab_0">
                                    <div class="box box-info box-solid">
                                        <?php
                                        if ((!$MI->checkActivePayout($member->getId(), $date, 3)) && ($Status > 1) && $member->getWorkStatus()==1) {
                                            ?>
                                            <div class="box-header" style="background-color: #FFF;">
                                                <h4 class='text-red'> Сотрудник не отображается в ведомости</h4>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <div class="box-body">
                                            <!--                                            Таблица с предварительным расчетом -->
                                            <table class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <th style="text-align:center">Оклад</th>
                                                <th style="text-align:center">Выплаченный аванс</th>
                                                <th style="text-align:center">Отсутствия(все)</th>
                                                <th style="text-align:center">НДФЛ</th>
                                                <th style="text-align:center">ИЛ</th>
                                                <?php if ($AV2->getMotivation() != 6) {
                                                    echo '<th style="text-align:center">Коммерческая премия</th>';
                                                    echo '<th style="text-align:center">KPI</th>';
                                                }
                                                ?>

                                                <th style="text-align:center">Административная премия</th>
                                                <th style="text-align:center">ГСМ</th>
                                                <th style="text-align:center">Мобильная связь</th>
                                                <th style="text-align:center">Удержания</th>
                                                <th style="text-align:center">Отчисления в ГБ</th>
                                                <th style="text-align:center">Корректировки</th>

                                                <?php
                                                if ($AV2->getYearBonus() > 0) {
                                                    ?>
                                                    <th style="text-align:center">ГБ</th>
                                                    <?php
                                                }
                                                ?>
                                                <th style="text-align:center">Итого</th>
                                                <th style="text-align:center">Итого округление</th>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td style="text-align: right"><?php echo(number_format($AV2->getSalary(), 2, ',', ' ')); ?></td>
                                                    <!-- Оклад -->
                                                    <td style="text-align: right"><?php echo(number_format($AV2->getAllSalary(), 2, ',', ' ')); ?></td>
                                                    <!-- Выплаченный аванс -->
                                                    <td style="text-align: right"><?php echo(number_format($AV2->getAllAbsenceRub(), 2, ',', ' ')); ?></td>
                                                    <!-- Отсутствия -->
                                                    <td style="text-align: right"><?php echo(number_format($AV2->getNdfl(), 2, ',', ' ')); ?></td>
                                                    <!-- НДФЛ -->
                                                    <td style="text-align: right"><?php echo(number_format($AV2->getRo(), 2, ',', ' ')); ?></td>
                                                    <!-- ИЛ -->

                                                    <?php
                                                    // Вывожу сумму коммерческого блока, если мотивация != 6
                                                    if ($AV2->getMotivation() != 6) {
                                                        echo '<td style="text-align: right">';
                                                        echo "0";
                                                        echo '</td>';

                                                        echo "<td style=\"text-align: right\">" . number_format(0, 2, ',', ' ') . "</td>";
                                                    }
                                                    ?>
                                                    <td style="text-align: right"><?php echo(number_format($AV2->getAllAdministrativeRubles(), 2, ',', ' ')); ?></td>
                                                    <td style="text-align: right"><?php echo(number_format($AV2->getAllTransport(), 2, ',', ' ')); ?></td>
                                                    <td style="text-align: right"><?php echo(number_format($MB->getSumm(), 2, ',', ' ')); ?></td>
                                                    <td style="text-align: right"><?php echo(number_format($AV2->getAllHoldRubles(), 2, ',', ' ')); ?></td>
                                                    <td style="text-align: right"><?php echo(number_format($AV2->getYearBonusPay(), 2, ',', ' ')); ?></td>
                                                    <td style="text-align: right"><?php echo(number_format($AV2->getAllCorrectionRubles(), 2, ',', ' ')); ?></td>
                                                    <?php
                                                    if ($AV2->getYearBonus() > 0) {
                                                        ?>
                                                        <td style="text-align: right"><?php echo(number_format($AV2->getYearBonus(), 2, ',', ' ')); ?></td>
                                                        <?php
                                                    }
                                                    ?>
                                                    <td style="text-align: right">
                                                        <?php
                                                        // Если мотивация не равна 6
                                                        echo(number_format(($AV2->getSummary()), 2, ',', ' '));
                                                        ?>
                                                    </td>
                                                    <td style="text-align: right">
                                                        <?php
                                                        // Если мотивация не равна 6
                                                        echo(number_format((roundSumm($AV2->getSummary())), 2, ',', ' '));
                                                        ?>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="box-body">
                                            <div class="box-footer">
                                                <?php
                                                if ($Status > 1 && $Status < 6) {
                                                    if ($MI->checkActivePayout($member->getId(), $date, 1)) {
                                                        ?>
                                                        <button class="btn btn-danger btn-flat pull-right" id="rm_statment" name="rm_statment">Убрать из ведомости</button>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <button class="btn btn-success btn-flat pull-right" id="rm_statment" name="rm_statment">Вернуть в ведомость</button>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                        </div>
                    </form>
                </div>
                <?php
                }
                ?>
            </div>
        </section>
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
require_once 'footer.php';
?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    // Обработка событий по нажатию enter
    $(document).ready(function () {
        $("#form-data").keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    $(document).ready(function () {
        var tab = getCookie('PremiumTab');
        $('.nav-tabs a[href="#tab_' + tab + '"]').tab('show');
    });

    // Выбор даты
    $(function () {
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            format: 'mm.yyyy',
            //startDate: <?php //echo '\''.$member->getEmploymentDate().'\''; ?>//,
        })
    });

    function stringToLocale(input_string) {
        input_string = parseFloat(input_string);
        return input_string.toLocaleString();
    }


    // Комментарии бонус и корректировка
    $(document).ready(function () {
        $("textarea[id^=correcting_note_]").each(function (i) {
            $(this).hide();
        });

        $("textarea[id^=bonus_note_]").each(function (i) {
            $(this).hide();
        });

        // $("#KPI_note").show();

        $(document).on("change", "input[id^=Hospital]", function () {
            var current = $(this).val();
            current = current.replace(",", ".");
            if (isNaN(current)) {
                current = 0;
            } else {
                if (current > 0) {
                    current *= -1;
                }
            }
            $(this).val(current);
        });

        $(document).on("change", "input[id^=Holiday]", function () {
            var current = $(this).val();
            current = current.replace(",", ".");
            if (isNaN(current)) {
                current = 0;
            }
            $(this).val(current);
        });

        $(document).on("click", "div[id^=btn_comment]", function () {
            var currentId = $(this).attr("id");
            var currentName = $(this).attr("name");
            var numberInID = currentId.replace("btn_comment_", "");


            if ($("#" + currentName + "_note_" + numberInID).is(':visible')) {
                $("#" + currentName + "_" + numberInID).show();
                $("#" + currentName + "_note_" + numberInID).hide();
            } else {
                $("#" + currentName + "_" + numberInID).hide();
                $("#" + currentName + "_note_" + numberInID).show();
            }
        });

        $(document).on("change", "textarea", function () {
            var currentVal = $(this).val();
            var currentId = $(this).attr("id");
            var Arr = currentId.split("_"); // bonus_note_
            var numberInID = currentId.replace(Arr[0] + "_note_", "");

            $("#" + Arr[0] + "_" + numberInID).attr("title", currentVal);
            $("#" + Arr[0] + "_note_" + numberInID).hide();
            $("#" + Arr[0] + "_" + numberInID).show();
        })

        $("input[id^=bonus]").on('input', function() {
            $(this).val($(this).val().replace(/[^\d,]/g,''))
        });

        $("input[id^=correcting]").on('input', function() {
            $(this).val($(this).val().replace(/[^\d,-]/g,''))
        });
    });

    // Изменение бонуса
    $(document).ready(function () {
        $(document).on("change", "input[id^=bonus]",
            function () {
                var Bonus = $(this).val();
                var currentId = $(this).attr("id");
                var numberInID = currentId.replace("bonus_", "");

                var CommercialCorrectionProcent = "1";
                if ($("#CommercialCorrectingProcent").val() != undefined) {
                    CommercialCorrectionProcent = $("#CommercialCorrectingProcent").val();
                    if (CommercialCorrectionProcent.indexOf(",") == 1) {
                        CommercialCorrectionProcent = CommercialCorrectionProcent.replace(",", ".");
                    }
                }

                if (!Bonus) {
                    Bonus = 0;
                } else {
                    Bonus = Bonus.replace(",", ".");
                }

                var Correcting = $("#correcting_" + numberInID).val();

                if (!Correcting) {
                    Correcting = 0;
                } else {
                    Correcting = Correcting.replace(",", ".");
                }

                var BonusSumm = 0;
                $("input[id^=bonus]").each(function (i) {
                    if ($(this).val() == '') {
                        var current_value = 0;
                    } else {
                        var current_value = $(this).val();
                        current_value = current_value.replace(",", ".");
                    }
                    BonusSumm = parseFloat(BonusSumm) + parseFloat(current_value);
                });

                $("#bonus_summ").html(stringToLocale(BonusSumm.toFixed(3)));

                var Profit = $("#profit_" + numberInID).html();
                var Procent = $("#procent_" + numberInID).html();
                var Summ = $("#summ_" + numberInID).html();

                // profit, correcting, procent, summ
                var NewSumm = (parseFloat(Profit) - parseFloat(Bonus) + parseFloat(Correcting)) * parseFloat(CommercialCorrectionProcent) * parseFloat(Procent) / 100;
                if (!$.isNumeric(NewSumm)) {
                    NewSumm = 0;
                }

                $("#summ_" + numberInID).html(stringToLocale(NewSumm.toFixed(3)));
                $("#rubsumm_" + numberInID).html(stringToLocale((NewSumm * 25).toFixed(3)));

                var FinalSumm = 0;
                $("div[id^=summ]").each(function (i) {
                    var current_value = $(this).html();
                    current_value = current_value.replace(",", ".");
                    FinalSumm = parseFloat(FinalSumm) + parseFloat(current_value);
                });

                // console.log(numberInID+" ======> "+currentVal+" "+Correcting+" "+Profit+" "+Procent+" "+Summ+" "+FinalSumm);
                $("#final_summ").html(stringToLocale(FinalSumm.toFixed(3)));
                $("#final_rubsumm").html(stringToLocale((FinalSumm * 25).toFixed(3)));
                $("#nac_summ_" + numberInID).html(stringToLocale((parseFloat(Correcting) + parseFloat(Profit) - parseFloat(Bonus)).toFixed(3)));

                var SummNacenka = 0;
                $("div[id^=nac_summ]").each(function (i) {
                    var current_value = $(this).html();
                    current_value = current_value.replace(",", ".");
                    SummNacenka = parseFloat(SummNacenka) + parseFloat(current_value);
                });

                $("#summ_nacenka").html(stringToLocale(SummNacenka.toFixed(2)));
            });

    });

    // Изменение корректировки
    $(document).ready(function () {
        $(document).on("change", "input[id^=correcting]",
            function () {
                var Correcting = $(this).val();
                var currentId = $(this).attr("id");
                var numberInID = currentId.replace("correcting_", "");


                var CommercialCorrectionProcent = "1";
                if ($("#CommercialCorrectingProcent").val() != undefined) {
                    CommercialCorrectionProcent == $("#CommercialCorrectingProcent").val();
                    if (CommercialCorrectionProcent.indexOf(",") == 1) {
                        CommercialCorrectionProcent = CommercialCorrectionProcent.replace(",", ".");
                    }
                }

                if (!Correcting) {
                    Correcting = 0;
                } else {
                    Correcting = Correcting.replace(",", ".");
                }


                var Bonus = $("#bonus_" + numberInID).val();
                if (!Bonus) {
                    Bonus = 0;
                } else {
                    Bonus = Bonus.replace(",", ".");
                }

                var Profit = $("#profit_" + numberInID).html();
                var Procent = $("#procent_" + numberInID).html();
                var Summ = $("#summ_" + numberInID).html();

                var CorrectingSumm = 0;
                $("input[id^=correcting]").each(function (i) {
                    if ($(this).val() == '') {
                        var current_value = 0;
                    } else {
                        var current_value = $(this).val();
                        current_value = current_value.replace(",", ".");
                    }
                    CorrectingSumm = parseFloat(CorrectingSumm) + parseFloat(current_value);
                });
                $("#correct_summ").html(stringToLocale(CorrectingSumm.toFixed(3)));

                // console.log(currentVal+" "+Premiya+" "+Profit+" "+Procent+" "+Summ+" "+FinalSumm);

                // profit, correcting, procent, summ
                var NewSumm = (parseFloat(Profit) + parseFloat(Correcting) - parseFloat(Bonus)) * parseFloat(CommercialCorrectionProcent) * parseFloat(Procent) / 100;
                if (!$.isNumeric(NewSumm)) {
                    NewSumm = 0;
                }

                $("#summ_" + numberInID).html(stringToLocale(NewSumm.toFixed(3)));
                $("#rubsumm_" + numberInID).html(stringToLocale((NewSumm * 25).toFixed(3)));


                var FinalSumm = 0;
                $("div[id^=summ]").each(function (i) {
                    var current_value = $(this).html();
                    current_value = current_value.replace(",", ".");
                    FinalSumm = parseFloat(FinalSumm) + parseFloat(current_value);
                });


                $("#final_summ").html(stringToLocale(FinalSumm.toFixed(3)));

                $("#final_rubsumm").html(stringToLocale((FinalSumm * 25).toFixed(3)));
                $("#nac_summ_" + numberInID).html(stringToLocale(parseFloat(Correcting) - parseFloat(Bonus) + parseFloat(Profit)));

                var SummNacenka = 0;
                $("div[id^=nac_summ]").each(function (i) {
                    var current_value = $(this).html();
                    current_value = current_value.replace(",", ".");
                    SummNacenka = parseFloat(SummNacenka) + parseFloat(current_value);
                });
                $("#summ_nacenka").html(stringToLocale(SummNacenka.toFixed(2)));
            });
    });

    // Изменение корректировки процента
    $(document).ready(function () {
        $(document).on("change", "input[id^=CommercialCorrectingProcent]",
            function () {
                var CommercialCorrectionProcent = 1;

                if (($("#CommercialCorrectingProcent").val()) != undefined) {
                    CommercialCorrectionProcent = $("#CommercialCorrectingProcent").val();
                    CommercialCorrectionProcent = CommercialCorrectionProcent.replace(",", ".");
                }

                $("div[id^=profit_]").each(function (i) {
                    var currentId = $(this).attr("id");
                    var numberInID = currentId.replace("profit_", "");

                    var Profit = $("#profit_" + numberInID).html();  // Наценка
                    var Correcting = $("#correcting_" + numberInID).val(); // корректировка наценки
                    if (Correcting == "") {
                        Correcting = 0;
                    } else {
                        Correcting = Correcting.replace(",", ".");
                    }

                    var Bonus = $("#bonus_" + numberInID).val(); // Бонус
                    if (Bonus == "") {
                        Bonus = 0;
                    } else {
                        Bonus = Bonus.replace(",", ".");
                    }

                    var Procent = $("#procent_" + numberInID).html(); // Процент

                    // profit, correcting, procent, summ
                    var NewSumm = (parseFloat(Profit) - parseFloat(Bonus) + parseFloat(Correcting)) * parseFloat(CommercialCorrectionProcent) * parseFloat(Procent) / 100;
                    if (!$.isNumeric(NewSumm)) {
                        NewSumm = 0;
                    }

                    $("#summ_" + numberInID).html(stringToLocale(NewSumm.toFixed(3)));
                    $("#rubsumm_" + numberInID).html(stringToLocale((NewSumm * 25).toFixed(3)));

                    var FinalSumm = 0;
                    $("div[id^=summ]").each(function () {
                        var current_value = $(this).html();
                        current_value = current_value.replace(",", ".");
                        FinalSumm = parseFloat(FinalSumm) + parseFloat(current_value);
                    });

                    // console.log(numberInID+" ======> "+currentVal+" "+Correcting+" "+Profit+" "+Procent+" "+Summ+" "+FinalSumm);
                    $("#final_summ").html(stringToLocale(FinalSumm.toFixed(3)));
                    $("#final_rubsumm").html(stringToLocale((FinalSumm * 25).toFixed(3)));
                    $("#nac_summ_" + numberInID).html(stringToLocale(parseFloat(Bonus) + parseFloat(Correcting) + parseFloat(Profit)));

                    console.log($(this).attr("id"));

                });


            });
    });

</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>


