<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 19.09.19
 * Time: 12:04
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/app/DirectionInterface.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/app/DepartmentInterface.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/app/MemberInterface.php';
require_once $_SERVER['DOCUMENT_ROOT']."/admin/RoleInterface.php";

$ID = array();
if (isset($_POST['data'])) {
    $ID = $_POST['data'];
}

//$member_id = intval($_POST['member_id']);
$member_id = 90; //Крылов
//$member_id = 70; // НЛ

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($member_id);
$RoleDepartmentFilter = array();
$is_director = 0;

foreach ($Roles as $Role) {
    if ($Role->getId() == 2 || $Role->getId() == 4) {
        if (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] == 0) {
            continue;
        }
        if ($Role->getDirection() == 0) {
            $RoleDirectionFilter = array(0);
        } else {
            $RoleDirectionFilter[] = $Role->getDirection();
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[] = $Role->getDepartment();
        }
    }
    if ($Role->getId() == 5) {
        $is_director = 1;
        $RoleDepartmentFilter = array(0);
        $RoleDirectionFilter = array(0);
        break;
    }
}

$DI = new DepartmentInterface();
$DI->fetchDepartments();
$Departments = $DI->GetDepartments();

$DR = new DirectionInterface();
$DR->fetchDirections();
$Directions = $DR->GetDirections();

$DirectorsData = $RI->getSubordination();
/*
 * Создаю объект MemberInterface
 */
$Members = new MemberInterface();

/*
 * Получаю список пользователей с минимальными данными
 * Только пользователей которые работают, уволенных не берем
 */
if (isset($_POST['ShowLeaving']))
    $ShowLeaving = 1;

if ($ShowLeaving == 0) {
    $Members->fetchMembers(false);
} else {
    $Members->fetchMembers();
}

$MemberList = $Members->GetMembers();

//var_dump($RoleDirectionFilter);
//echo "<BR><BR>";
/*
 * Удаляю лишние направления, из-за роли
 */
if ($is_director==0) {
    foreach ($Directions as $k => $d) {
        if ((count($Directions) != 0) && count($RoleDirectionFilter)!=0) {
            if (!in_array($d->getId(), $RoleDirectionFilter)) {
                unset($Directions[$k]);
            }
        }
    }
}

/*
 * Удаляю лишние отделы, из-за роли
 */
if ($is_director==0) {
    foreach ($Departments as $k => $d) {
        if (count($RoleDepartmentFilter) != 0) {
            if (!in_array($d->getId(), $RoleDepartmentFilter)) {
                unset($Departments[$k]);
            }
        }
    }
}


/*
 * Удаляю лишних сотрудников, из-за роли
 */
if ($is_director == 0) {
    foreach ($MemberList as $k => $m) {
        if (count($RoleDepartmentFilter) != 0) {
            if (!in_array($m->getDepartment()["id"], $RoleDepartmentFilter)) {
                unset($MemberList[$k]);
            }
        }
    }
}

$DirectorList = array();
foreach ($DirectorsData as $d) {
    if ($is_director == 0 &&   (count($RoleDepartmentFilter) != 0) && !in_array($d['id'], $RoleDepartmentFilter)){
        continue;
    }

    $DirectorList[$d["director_id"]] = array("id" => $d["id"], "director_id" => $d["director_id"], "director_name" => $d["director_name"]);
}


$DirectorFilter = array(90);


if (isset($DirectorFilter)) {
    foreach ($DirectorsData as $k=>$v) {
        if (!in_array($v['director_id'], $DirectorFilter)) {
            unset($DirectorsData[$k]);
        }
    }

    foreach ($Departments as $key=>$v) {
        $r = current(array_filter($DirectorsData, function ($d) use($v){
            return $d['id']==$v->getId();
        }));

        if (!$r) {
            unset($Departments[$key]);
        }
    }

    foreach ($Directions as $key=>$v) {
        $r = current(array_filter($DirectorsData, function ($d) use($v){
            return $d['direction_id']==$v->getId();
        }));

        if (!$r) {
            unset($Directions[$key]);
        }
    }

    foreach ($MemberList as $key=>$m) {
        $md_id= $m->getDepartment()['id'];
        $r = current(array_filter($Departments, function($d) use ($md_id){
            return $md_id == $d->getId();
        }));

        if (!$r)
            unset($MemberList[$key]);

    }
}


$Directions_Array = array();
foreach ($Directions as $d) {
    $Directions_Array[] = array("id"=>$d->getId(),"name"=>$d->getName());
}


$Departments_Array = array();
foreach ($Departments as $d) {
    $Departments_Array[] = array("id"=>$d->getId(),"name"=>$d->getName());
}

$Member_Array = array();
foreach ($MemberList as $m) {
    $Member_Array[] = array("id"=>$m->getId(), "name"=>$m->getLastName()." ".$m->getName());
}

//var_dump($Directions_Array);echo "<BR>";
//var_dump($Departments_Array);echo "<BR>";
//var_dump($Member_Array); echo "<BR>";

echo json_encode($Directions_Array);
echo json_encode($Departments_Array);
echo json_encode($Member_Array);


exit();
?>