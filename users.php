<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 28.03.19
 * Time: 12:07
 */

require_once "app/functions.php";
require_once "admin/RoleInterface.php";
require_once 'admin/User.php';
require_once 'app/Notify.php';
require_once 'app/MemberInterface.php';


session_start();
if (isset($_SESSION['UserObj'])) {
    $User = $_SESSION['UserObj'];
} else {
    header("Location: index.php");
    exit();
}

if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

$dbh = dbConnect();
$query = "select t1.*,t2.name,t2.lastname,t2.middle from member_auth as t1 LEFT JOIN member as t2 ON (t1.member_id = t2.id)";
$sth = $dbh->prepare($query);
$sth->execute();
$raw = $sth->fetchAll(PDO::FETCH_ASSOC);
$sth = null;

$MI = new MemberInterface();
$MI->fetchMembers(false);
$members = $MI->GetMembers();

if (isset($_POST['PushButton1'])) {
    $login = $_POST['InputLogin'];
    $password = $_POST['InputPassword'];
    $member_id = $_POST['member_id'];
    $hash = hash("sha256", $password);

    if (is_numeric($member_id)) {
        $query = "insert into member_auth (login,hash,member_id) VALUE (?,?,?)";
        $sth = $dbh->prepare($query);
        $sth->execute([$login,$hash,$member_id]);
        $sth = null;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Управление пользователями</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Пользователи</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <th>#</th>
                        <th>login</th>
                        <th>ФИО</th>
                        <th>Активен</th>
                        <th>Управление</th>
                        </thead>

                        <tbody>
                        <?php
                        try {
                            foreach ($raw as $value) {
                                echo "<tr>";
                                echo "<td>" . $value['id'] . "</td>";
                                echo "<td>" . $value['login'] . "</td>";
                                echo "<td>" . $value['lastname'] . " " . $value['name'] . " " . $value['middle'] . "</td>";
                                echo "<td>" . $value['active'] . "</td>";
                                echo "</tr>";
                            }
                        } catch (Exception $e) {
                            echo "<tr><td colspan='12'>" . $e->getMessage() . "</td></tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    <button class="btn btn-sm btn-flat btn-success" data-toggle="modal" data-target="#modal">Добавить пользователя</button>
                </div>
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
        <form method="post" class="form-horizontal">
            <div class="modal fade" id="modal">
                <div class="modal-dialog" style="width: 90%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Добавление пользователя</h4>
                        </div>
                        <div class="modal-body">
                            <div class="box box-info box-solid">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">login <b class="text-red">*</b></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="InputLogin" id="inputLogin" placeholder="login">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Пароль <b class="text-red">*</b></label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" name="InputPassword" id="InputPassword" placeholder="password">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Сотрудник <b class="text-red">*</b></label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="member_id" id="member_id">
                                                <option selected value="0">Не выбрано</option>
                                                <?php
                                                foreach ($members as $member) {
                                                    echo "<option value='" . $member->getId() . "'>" . $member->getLastname() . " " . $member->getName() . " " . $member->getMiddle() . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-flat btn-primary btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                            <button type="submit" class="btn btn-flat btn-success btn-sm" name="PushButton1"><i class="fa fa-save"></i> Добавить</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </form>
    </div>

    <!-- /.content-wrapper -->
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>


