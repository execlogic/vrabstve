<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 19.10.18
 * Time: 14:27
 */

// Время работы скрипта
$start = microtime(true);
require_once 'app/PayRollInterface.php';
require_once 'app/Envelopes.php';
require_once 'app/functions.php';
require_once 'app/DirectionInterface.php';
require_once 'app/DepartmentInterface.php';


require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

function roundSumm($summ)
{
    // Округление до 10
    if ($summ > 0) { //Если сумма <= 0 то не обрабатываем данные
        //$summ = round($summ); // округляю сотые и десятые
        $summ = (int)$summ;

        $ostatok = ($summ % 10); // Получаю остаток от деления на 10
        if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
            $summ = $summ - $ostatok + 10;
        } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
            $summ = $summ - $ostatok;
        };
    } else {
        $summ = 0;
    }
    return $summ;
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

if (!isset($_REQUEST['date']) && !isset($_REQUEST['TypeOfPayment'])) {
    echo "Ошибка в дате или типе выплат";
    exit;
}

$_REQUEST['date'] = strip_tags($_REQUEST['date']);
$_REQUEST['date'] = htmlspecialchars($_REQUEST['date']);
$_REQUEST['date'] = preg_replace("/[^0-9.]/i", "", $_REQUEST['date']);

$_REQUEST['TypeOfPayment'] = strip_tags($_REQUEST['TypeOfPayment']);
$_REQUEST['TypeOfPayment'] = htmlspecialchars($_REQUEST['TypeOfPayment']);
$_REQUEST['TypeOfPayment'] = preg_replace("/[^0-9]/i", "", $_REQUEST['TypeOfPayment']);

$dbh = dbConnect();

// Создаю объект ведомости
//$PI = new PayRollInterface();

$date = $_REQUEST['date'];
$TypeOfPayment = $_REQUEST['TypeOfPayment'];

$DR = new DirectionInterface(); // Направление
$DR->fetchDirections();
$Directions = $DR->GetDirections();

$DP = new DepartmentInterface(); // Отдел
$DP->fetchDepartments();
$Departments = $DP->GetDepartments();

// Выдана ли премия или нет
$RawData = array();

$query="SELECT t1.member_id,t1.direction_id,t1.department_id,t1.Total as summ, t2.name as direction_name, t3.name as department_name, t3.namesklad as sklad_name
        FROM financial_payout as t1
        LEFT JOIN direction as t2 ON (t1.direction_id = t2.id)
        LEFT JOIN department as t3 ON (t1.department_id = t3.id)
        WHERE t1.type_of_payment=? and t1.date=STR_TO_DATE(?,'%m.%Y') and t1.active=1";

try {
    $sth = $dbh->prepare($query); // Подготавливаем запрос
    $sth->execute([$TypeOfPayment,$date]);
} catch (PDOException $e) {
    throw new Exception('Ошибка в PDO' . $e->getMessage());
}

if ($sth->rowCount() > 0) {
    $RawData = $sth->fetchAll(PDO::FETCH_ASSOC);
};

$DirectionPayout = array();
$DirectionPayout['FullSumm'] = 0;

foreach ($RawData as $array) {
    if ($array['department_id']==26) {
        $array['direction_id']=1;
    }

    if (!isset($DirectionPayout['data'][$array['direction_id']])) {
        $DirectionPayout['data'][$array['direction_id']] = array();
        $DirectionPayout['data'][$array['direction_id']]['itog'] = 0;
        $DirectionPayout['data'][$array['direction_id']]['office_data'] = array();
//        $DirectionPayout['data'][$array['direction_id']]['name']=$array['direction_name'];
        $DirectionPayout['data'][$array['direction_id']]['name']=$DR->findById($array['direction_id'])->getName();;
    }

    if (!isset($DirectionPayout['data'][$array['direction_id']]['office_data'][$array['department_id']] )) {
        $DirectionPayout['data'][$array['direction_id']]['office_data'][$array['department_id']]['summ'] = 0;
        if (isset($array['sklad_name']) && strlen($array['sklad_name'])>1)  {
            $DirectionPayout['data'][$array['direction_id']]['office_data'][$array['department_id']]['name'] = $array['sklad_name'];
        } else {
            $DirectionPayout['data'][$array['direction_id']]['office_data'][$array['department_id']]['name'] = $array['department_name'];
        }
    }

    $DirectionPayout['data'][$array['direction_id']]['office_data'][$array['department_id']]['summ'] += roundSumm($array['summ']);
    $DirectionPayout['data'][$array['direction_id']]['itog'] += roundSumm($array['summ']);
//    $DirectionPayout['itog'][$array['direction_id']] += $array['summ'];
    $DirectionPayout['FullSumm'] += roundSumm($array['summ']);
}

if (isset($DirectionPayout)) {
    ksort($DirectionPayout['data']);
    foreach ($DirectionPayout['data'] as $k=>$v) {
        usort($DirectionPayout['data'][$k]['office_data'], function ($a, $b) {
            return strnatcmp($a['name'], $b['name']);
        });
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Детальные расходы</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
                Детальные расходы <?php echo $date; ?>
            </h4>
            <ol class="breadcrumb">
                <li><a href="prepaid_expense.php?date=<?php echo $date;?>&TypeOfPayment=<?php echo $TypeOfPayment; ?>">Ведомость</a></li>
                <li><a href="envelope.php?date=<?php echo $date;?>&TypeOfPayment=<?php echo $TypeOfPayment; ?>">Конверты</a></li>
                <li class="active">Детальные расходы</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">


            <div class="box box-solid box-plane">
                <div class="box-body">
                    <table class="table table-striped table-hover" id="DataTable">
                        <thead>
                        <tr>
                            <th>Направление</th>
                            <th>Отдел</th>
                            <th>Сумма</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach ($DirectionPayout['data'] as $key=>$value) {
                                foreach ($value['office_data'] as $k=>$item) {
                                    if ($key!=1 && $k!=26)
                                        continue;

                                    echo "<tr>";
                                    echo "<td>".$value['name']."</td>";
                                    echo "<td>".$item['name']."</td>";
                                    echo "<td>" . number_format($item['summ'], 2, ',', ' ') . "</td></tr>";
                                }
                                echo "<tr></tr><td><h5><b>".$value['name']."</b></h5></td><td></td><td><h5><b>".number_format($value['itog'], 2, ',', ' ')."</b></h5></td></tr>";
                            }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="2"><h5><b>Итого</b></h5></th>
                            <th><h5><b><?php echo number_format($DirectionPayout['FullSumm'], 2, ',', ' '); ?></b></h5></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
