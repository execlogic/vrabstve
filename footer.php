<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 11.10.18
 * Time: 12:08
 */
?>


<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Зарплата и кадры</b> 1.5.0
    </div>
    <?php
        if (isset($start)) {
            echo '<span style="font-size: 12px;">Время выполнения скрипта: ' . round(microtime(true) - $start, 4) . ' сек.</span>';
        }
    ?>
</footer>
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>

