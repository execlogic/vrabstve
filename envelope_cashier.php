<?php

// Время работы скрипта
$start = microtime(true);

require_once 'app/PayRoll.php';
require_once 'app/PayRollInterface.php';
require_once 'app/functions.php';
require_once 'app/ReportStatus.php';
require_once 'app/Envelopes.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once "app/EnvelopeInterface.php";

require_once 'app/Notify.php';

if (!isset($_SESSION)) {
    session_start();
    $User = $_SESSION['UserObj'];
}

if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

$NewPrepaid = 0;

// Проверяю права

// Настройки
array_filter($Roles, function ($Role) use (&$NewPrepaid) {
    if ($Role->getPrepaid() == 1) {
        $NewPrepaid = 1;
        return true;
    };
    return false;
});

if ($NewPrepaid == 0) {
    header("Location: index.php");
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

$EI = new EnvelopeInterface();

if (!isset($_SESSION)) {
    session_name('Envelope');
    session_start();
}

$RS = new ReportStatus(); // Статус ведомости.
$RS_last = $RS->getCatcherStatus();
$EI->setDateType($RS_last['date'], $RS_last['type_of_payment']);

$ReportStatus = $RS->getStatus($EI->getDate(), $EI->getTypeOfPayment());


//var_dump($RS_last);
//$rawData = $_SESSION['Envelope']['Data'];

//$EI->setRawData($rawData); // Подучаю данные из сессии

//session_commit();

if (isset($_POST['MAX_ENV_SUMM'])) {
    $EI->setMaxEnvSumm($_POST['MAX_ENV_SUMM']);
}

if (isset($_POST['MIN_ENV_SUMM'])) {
    $EI->setMinEnvSumm($_POST['MIN_ENV_SUMM']);
}

if (isset($_POST['MAX_ENV_NUMSIZE'])) {
    $EI->setMaxEnvNumSize($_POST['MAX_ENV_NUMSIZE']);
}


$EI->fetch();
$EI->CatcherCalc();

$EnvMembers = array();

foreach ($EI->get() as $obj) {
    $EnvMembers[] = $obj->getMemberId();
}

$EnvMembers = array_unique($EnvMembers);

$Envelope = [];
foreach ($EI->get() as $item) {
    $Envelope[$item->getEnvelopeId()] = $item->getSumm();
}

$FullSumm = 0;
$envelope_count = 0;
foreach ($Envelope as $key => $summ) {
    $FullSumm += $summ;
    if ($summ > 0)
        $envelope_count++;
}

if (isset($_POST['Confirm'])) {
    $RS->ChangeStatus($EI->getDate(), $EI->getTypeOfPayment(), (++$ReportStatus['status_id']), $User->getMemberId());
    header("Location: index.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Конверты кассира</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
    <style>
        .more {
            page-break-after: always;
        }

        div {
            font-size: 14px;
        }
    </style>
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
                <?php
                if ($EI->getTypeOfPayment() !== null) {
                    switch ($EI->getTypeOfPayment()) {
                        case 1:
                            echo "Конверты премии " . $EI->getDate();
                            break;
                        case 2:
                            echo "Конверты аванса " . $EI->getDate();
                            break;
                        case 3:
                            echo "Конверты аванса2 " . $EI->getDate();
                            break;
                        case 4:
                            echo "Конверты \"расчета\" " . $EI->getDate();
                            break;
                    }
                } else {
                    echo "Не выбран тип выплаты";
                }
                ?>
            </h4>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Список пользователей</a></li>
                <li class="active">Конверты</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="box box-info box-solid">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="info-box bg-aqua">
                                <span class="info-box-icon"><i class="fa fa-envelope"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Конвертов</span>
                                    <span class="info-box-number"><?php echo $envelope_count; ?></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="info-box bg-olive">
                                <span class="info-box-icon"><i class="fa fa-ruble"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Сумма</span>
                                    <span class="info-box-number"><?php echo number_format($FullSumm, 2, ',', ' '); ?> руб.</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="box box-warning collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Конверты</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div id="print_tableA4" class="box-body">
                                    <div class="row">
                                        <div class="col-xs-2"><h4><b>Номер</h4></b></div>
                                        <div class="col-xs-7"><h4><b>&sum; (руб)</b></h4></div>
                                    </div>
                                    <div id="print_table">
                                        <?php
                                        foreach ($Envelope as $key => $summ) {
                                            if ($summ > 0) {
                                                echo '<div class="row">';
                                                echo "<div class='col-xs-2'><p>" . $key . "</p></div>";
                                                echo "<div class='col-xs-7 more'><p>" . number_format($summ, 0, ',', ' ') . "</p></div>";
                                                echo "</div>";
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="callout callout-danger">
                        <h4>Печать номеров и сумм на конвертах</h4>

                        <p>Для печати номеров и сумм в настройках принтера нужно выбрать: <BR>
                        <ul>
                            <li> Ориентация альбомная</li>
                            <li>Размер бумаги Env DL</li>
                        </ul>
                        Положить конверты в лоток ручной подачи.
                        </p>

                        <h4>Печать A4</h4>
                        <p>Для печати номеров и сумм в настройках принтера нужно выбрать: <BR>
                        <ul>
                            <li> Ориентация портретная</li>
                            <li>Размер бумаги A4</li>
                        </ul>
                        Бумага берется с основного лотка.
                        </p>
                    </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <form method="post">
                        <?php
                        echo "<input type=\"submit\" class=\"btn btn-primary btn-flat\" onclick=\"if (confirm('Провести на следующую стадию?')) {  $('#modal-save').modal('show');} else { return false;};\"\" name=\"Confirm\" value=\"Выдано\">";
                        ?>
                        <input type="submit" class="btn btn-primary btn-flat" name="print" onclick="print_text()" value="Печать на конвертах">
                        <input type="submit" class="btn btn-primary btn-flat" name="print" onclick="printA4_text()" value="Печать на A4">
                    </form>
                </div>
            </div>

            <div class="modal fade" id="modal-save" style="background: rgba(0,0,0,1);">
                <div class="modal-dialog" style="width: 95%">
                    <div class="modal-content">
                        <div class="modal-header text-center" style="color: white;background-color: #000;border: unset;">
                            <h3 class="modal-title" style="color: white;background-color: #000;border: unset;">Сохранение данных</h3>
                        </div>

                        <div class="modal-body" style="background-color: #000;">
                            <div class="box box-solid box-info" style="height: 70%;background-color: #000;border: unset;">
                                <div class="box-body">
                                    <img class="img-responsive center-block" src="images/7TwN.gif">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


<script>
    $(function () {

        $('#MembersTable').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })
    })
</script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    function print_text() {
        var txt = document.getElementById("print_table").innerHTML;

        var newWin = window.open("about:blank", "Печать",);
        var style = newWin.document.createElement('style');

        style.type = 'text/css';
        // style.innerHTML = 'tr,td { font-size: 8px;}';
        style.innerHTML = '.more { page-break-after: always; }; div {font-size:35px;};';

        var link = newWin.document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = 'bower_components/bootstrap/dist/css/bootstrap.min.css';

        var div = newWin.document.createElement('div'),
            body = newWin.document.body,
            head = newWin.document.head;

        div.innerHTML = txt
        div.style.fontSize = '35px'

        // вставить первым элементом в body нового окна
        head.insertBefore(style, head.firstChild);
        head.appendChild(link);
        body.insertBefore(div, body.firstChild);
        newWin.focus();
        newWin.print();
    }

    function printA4_text() {
        var txt = document.getElementById("print_tableA4").innerHTML;

        var newWin = window.open("about:blank", "Печать",);

        var style = newWin.document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = 'p { font-size: 9px;}';

        var link = newWin.document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = 'http://<?php echo $_SERVER["HTTP_HOST"]; ?>/bower_components/bootstrap/dist/css/bootstrap.min.css';

        var div = newWin.document.createElement('div'),
            body = newWin.document.body,
            head = newWin.document.head;

        div.innerHTML = txt

        // вставить первым элементом в body нового окна
        head.insertBefore(style, head.firstChild);
        head.appendChild(link);
        body.insertBefore(div, body.firstChild);
        newWin.focus();
        newWin.print();
    }
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>

