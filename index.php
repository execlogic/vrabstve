<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 22.10.18
 * Time: 15:59
 */
require_once 'admin/User.php';
require_once 'admin/Authentication.php';

$AU = new Authentication();

if (isset($_REQUEST['exit'])) {
    $AU->out();
    header("Location: index.php");
}

if ($AU->isAuth() == true) {
    if ($AU->getLogin() == "administrator") {
        header("Location: member_list.php");
    } else {
        $UserObj = $_SESSION['UserObj'];
        if (count($UserObj->getRole()) == 0) {

            echo "<H1>Доступ в программу, с ролью менеджера, временно ограничен.</H1><BR>";
            $AU->out();
            exit();
        }

        header("Location: profile.php?id=" . $AU->getMemberId());
    }
//    echo  $AU->getMemberId()."<BR>";
//    $AU->out();
//    exit();
}

if (isset($_POST['username']) && isset($_POST['password'])) {
    $_REQUEST['username'] = strip_tags($_REQUEST['username']);
    $_REQUEST['username'] = htmlspecialchars($_REQUEST['username']);
    $_REQUEST['username'] = preg_replace("/[^A-Za-z0-9]/i", "", $_REQUEST['username']);

    $_REQUEST['password'] = strip_tags($_REQUEST['password']);
    $_REQUEST['password'] = htmlspecialchars($_REQUEST['password']);
    $_REQUEST['password'] = preg_replace("/[^A-Za-z0-9]/i", "", $_REQUEST['password']);


    if ($AU->auth($_POST['username'], $_POST['password'])) {
        if ($_POST['username'] == "administrator") {
            header("Location: member_list.php");
        } else {
            $UserObj = $_SESSION['UserObj'];
            if (count($UserObj->getRole()) == 0) {
                echo "<H1>Доступ в программу, с ролью менеджера, временно ограничен.</H1><BR>";
                $AU->out();
                exit();
            }

            header("Location: profile.php?id=" . $AU->getMemberId());
        }
    }
}


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Вход в ЗиК</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../../plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="css/main.css">
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <b>З</b>и<b>К</b>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Введите логин и пароль</p>

        <form method="post">
            <div class="form-group has-feedback">
                <input type="login" name="username" class="form-control" placeholder="Логин">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Пароль">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <?php
                    if (isset($_POST['username']) && isset($_POST['password'])) {
                        echo "<div class='label-danger'> Неверный логин или пароль!</div>";
                    }
                    ?>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
    });
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
