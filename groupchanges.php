<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.08.18
 * Time: 18:29
 */

// Время работы скрипта
$start = microtime(true);

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/MemberInterface.php';
require_once 'app/AdministrativeBlock.php';
require_once 'app/Retention.php';
require_once 'app/Correction.php';
require_once 'app/Kpi.php';

require_once 'app/DepartmentInterface.php';
require_once 'app/PositionInterface.php';

require_once 'app/Notify.php';
require_once 'app/ReportStatus.php';
require_once 'app/Premium.php';
require_once 'app/PayRoll.php';
require_once 'app/PayRollInterface.php';
require_once 'app/MobileBlock.php';
require_once 'app/TransportBlock.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

function roundSumm($summ)
{
    // Округление до 10
    if ($summ > 0) { //Если сумма <= 0 то не обрабатываем данные
        //$summ = round($summ); // округляю сотые и десятые
        $summ = (int)$summ;

        $ostatok = ($summ % 10); // Получаю остаток от деления на 10
        if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
            $summ = $summ - $ostatok + 10;
        } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
            $summ = $summ - $ostatok;
        };
    } else {
        $summ = 0;
    }
    return $summ;
}

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

$Permission_Promotions = 0;
$Permission_AdministrativePrize = 0;
$Permission_Training = 0;
$Permission_PrizeAnotherDepartment = 0;
$Permission_Defect = 0;
$Permission_Sunday = 0;
$Permission_NotLessThan = 0;
$Permission_MobileCommunication = 0;
$Permission_Overtime = 0;
$Permission_CompensationFAL = 0;
$Permission_LateWork = 0;
$Permission_Schedule = 0;
$Permission_Internet = 0;
$Permission_CachAccounting = 0;
$Permission_Receivables = 0;
$Permission_Credit = 0;
$Permission_Other = 0;
$Permission_BonusAdjustment = 0;
$Permission_Calculation = 0;
$Permission_YearBonus = 0; // Not implemented yet

/*
 * Права доступа
 */

$RoleDepartmentFilter = array();
/*
 * Номера отделов по ID для фильтрации
 */
$RoleDepartmentList = array();

foreach ($Roles as $Role) {
    $RoleDepartmentList[] = $Role->getDepartment();

    if ($Role->getPromotions() == 1) {
        $Permission_Promotions = 1;

        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[1][0]) && $RoleDepartmentFilter[1][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[1] = array(0);
            $RoleDepartmentFilter[21] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[1][] = $Role->getDepartment();
            $RoleDepartmentFilter[21][] = $Role->getDepartment();
        }
    }

    if ($Role->getAdministrativePrize() == 1) {
        $Permission_AdministrativePrize = 1;

        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[2][0]) && $RoleDepartmentFilter[2][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[2] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[2][] = $Role->getDepartment();
        }
    }

    if ($Role->getTraining() == 1) {
        $Permission_Training = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[3][0]) && $RoleDepartmentFilter[3][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[3] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[3][] = $Role->getDepartment();
        }
    }

    if ($Role->getPrizeAnotherDepartment() == 1) {
        $Permission_PrizeAnotherDepartment = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[4][0]) && $RoleDepartmentFilter[4][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[4] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[4][] = $Role->getDepartment();
        }
    }

    if ($Role->getDefect() == 1) {
        $Permission_Defect = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[5][0]) && $RoleDepartmentFilter[5][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[5] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[5][] = $Role->getDepartment();
        }
    }

    if ($Role->getSunday() == 1) {
        $Permission_Sunday = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[6][0]) && $RoleDepartmentFilter[6][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[6] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[6][] = $Role->getDepartment();
        }
    }

    if ($Role->getNotLessThan() == 1) {
        $Permission_NotLessThan = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[7][0]) && $RoleDepartmentFilter[7][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[7] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[7][] = $Role->getDepartment();
        }
    }

    if ($Role->getMobileCommunication() == 1) {
        $Permission_MobileCommunication = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[8][0]) && $RoleDepartmentFilter[8][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[22] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[22][] = $Role->getDepartment();
        }
    }

    if ($Role->getOvertime() == 1) {
        $Permission_Overtime = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[9][0]) && $RoleDepartmentFilter[9][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[9] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[9][] = $Role->getDepartment();
        }
    }

    if ($Role->getCompensationFAL() == 1) {
        $Permission_CompensationFAL = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[10][0]) && $RoleDepartmentFilter[10][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[23] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[23][] = $Role->getDepartment();
        }
    }

    if ($Role->getLateWork() == 1) {
        $Permission_LateWork = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[11][0]) && $RoleDepartmentFilter[11][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[11] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[11][] = $Role->getDepartment();
        }
    }

    if ($Role->getSchedule() == 1) {
        $Permission_Schedule = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[12][0]) && $RoleDepartmentFilter[12][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[12] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[12][] = $Role->getDepartment();
        }
    }

    if ($Role->getInternet() == 1) {
        $Permission_Internet = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[13][0]) && $RoleDepartmentFilter[13][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[13] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[13][] = $Role->getDepartment();
        }
    }

    if ($Role->getCachAccounting() == 1) {
        $Permission_CachAccounting = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[14][0]) && $RoleDepartmentFilter[14][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[14] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[14][] = $Role->getDepartment();
        }

    }

    if ($Role->getReceivables() == 1) {
        $Permission_Receivables = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[15][0]) && $RoleDepartmentFilter[15][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[15] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[15][] = $Role->getDepartment();
        }
    }

    if ($Role->getCredit() == 1) {
        $Permission_Credit = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[16][0]) && $RoleDepartmentFilter[16][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[16] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[16][] = $Role->getDepartment();
        }
    }

    if ($Role->getOther() == 1) {
        $Permission_Other = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[17][0]) && $RoleDepartmentFilter[17][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[17] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[17][] = $Role->getDepartment();
        }
    }

    if ($Role->getBonusAdjustment() == 1) {
        $Permission_BonusAdjustment = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[18][0]) && $RoleDepartmentFilter[18][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[18] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[18][] = $Role->getDepartment();
        }
    }

    if ($Role->getCalculation() == 1) {
        $Permission_Calculation = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[19][0]) && $RoleDepartmentFilter[19][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[19] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[19][] = $Role->getDepartment();
        }
    }

    /*
     * Годовой бонус
     */
    if ($Role->getYearBonus() == 1) {
        $Permission_YearBonus = 1;
        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */

        if (isset($RoleDepartmentFilter[20][0]) && $RoleDepartmentFilter[20][0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[20] = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[20][] = $Role->getDepartment();
        }
    }
}

//Выставляю текущую дату, если в запросе не будет ['date']
$date = date("m.Y");

$DI = new DepartmentInterface();
$DI->fetchDepartments();


$PI = new PositionInterface();
$PI->fetchPositions();

$RS = new ReportStatus();

$Ini_Max_Input_Vars = ini_get('max_input_vars');

if (isset($_POST['SendDate']) || isset($_POST['PushButton_1'])) {
    /*
    * Выкусываем теги
    */
    $_POST['date'] = strip_tags($_POST['date']);
    /*
     * Экранирую html спец. символы
     */
    $_POST['date'] = htmlspecialchars($_POST['date']);
    /*
     * в дате проверяю существование только цифр, и точки
     */
    $_POST['date'] = preg_replace("/[^0-9.]/i", "", $_POST['date']);

    /*
     * Записываю дату в переменную
     */
    $date = &$_POST['date'];

    /*
     * Что заводим Акции, Обучения, Воскресения итд...
     */
    $Type = &$_POST['Type'];

    $Status = $RS->getStatus($date, 1)['status_id'];

    $Members = new MemberInterface();
    /*
     * Получаю информацию по всем пользователями,
     * кроме уволенных
     */
    $Members->fetchMembers(false, null,true);

    // ******************************* ИНИЦИАЛИЗАЦИЯ ОСНОВНЫХ ОБЪЕКТОВ (по табам)
    if ($Type > 0 && $Type <= 10) {
        //Таб Административный объект
        $AB = new AdministrativeBlock();
        //Тип выплат премия
        $AB->setTypeOfPayment(1);
        /*
         * Получаю данные из БД или NULL если нету данных
         */
    } else if ($Type > 10 && $Type <= 17) {
        //Таб Удержания
        $RN = new Retention();
        //Тип выплат премия
        $RN->setTypeOfPayment(1);
    } else if ($Type > 17 and $Type < 21) {
        //Таб Корректировки
        $CR = new Correction();
        //Тип выплат премия
        $CR->setTypeOfPayment(1);
    } else if ($Type==21) {
        $KPI = new Kpi();
        if (isset($_POST['KPINum'])) {
            $KPI_Num_Filter = $_POST['KPINum'];
        } else {
            $KPI_Num_Filter = -1;
        }

        if (isset($_POST['FilterPosition'])) {
            $FilterPosition = $_POST['FilterPosition'];
        } else {
            $FilterPosition = -1;
        }

        $PositionList = array();
        foreach ($PI->GetPositions() as $k=>$p) {
            if (isset($_POST['FilterDepartment']) && $_POST['FilterDepartment']!=-1 && $FilterPosition!=-1) {
                foreach ($p->getDepartmentId() as $d) {
                    if ($d == $_POST['FilterDepartment']) {
                        $PositionList[$p->getId()] = array("id" => $p->getId(), "name" => $p->getName());
                    }
                }
            } else {
                foreach ($p->getDepartmentId() as $d) {
                    if ($RoleDepartmentList[0] != 0) {
                        if (in_array($d, $RoleDepartmentList)) {
                            $PositionList[$p->getId()] = array("id" => $p->getId(), "name" => $p->getName());
                        }
                    } else {
                        $PositionList[$p->getId()] = array("id" => $p->getId(), "name" => $p->getName());
                    }
                }
            }
        }
    } else if ($Type==22) {
        $MB = new MobileBlock();
    } else if ($Type==23) {
        $TR = new TransportBlock();
    }

    /*
     * В зависимости от выбранного типа получаю данные из БД
     * или NULL если нету данных
     */
    $AllData = null;
    switch ($Type) {
        case 1:
            $AllData = $AB->fetchAllByDate($date, 1);
            break;
        case 2:
            $AllData = $AB->fetchAllByDate($date, 2);
            break;
        case 3:
            $AllData = $AB->fetchAllByDate($date, 3);
            break;
        case 4:
            $AllData = $AB->fetchAllByDate($date, 4);
            break;
        case 5:
            $AllData = $AB->fetchAllByDate($date, 5);
            break;
        case 6:
            $AllData = $AB->fetchAllByDate($date, 6);
            break;
        case 7:
            $AllData = $AB->fetchAllByDate($date, 7);
            break;
        case 9:
            $AllData = $AB->fetchAllByDate($date, 9);
            break;
//        case 10:
//            $AllData = $AB->fetchAllByDate($date, 10);
//            break;
        case 11:
            $AllData = $RN->fetchAllByDate($date, 1);
            break;
        case 12:
            $AllData = $RN->fetchAllByDate($date, 2);
            break;
        case 13:
            $AllData = $RN->fetchAllByDate($date, 3);
            break;
        case 14:
            $AllData = $RN->fetchAllByDate($date, 4);
            break;
        case 15:
            $AllData = $RN->fetchAllByDate($date, 5);
            break;
        case 16:
            $AllData = $RN->fetchAllByDate($date, 6);
            break;
        case 17:
            $AllData = $RN->fetchAllByDate($date, 7);
            break;
        case 18:
            $AllData = $CR->fetchAllByDate($date, 1);
            break;
        case 19:
            $AllData = $CR->fetchAllByDate($date, 2);
            break;
        case 20:
            $AllData = $CR->fetchAllByDate($date, 3);
            break;
        case 21:
            $AllData = $KPI->fetchAllByDate($date);
            if ($KPI_Num_Filter!=-1) {
                foreach ($AllData as $k=>$v) {
                    foreach ($v as $num=>$item) {
                        if ($num != $KPI_Num_Filter) {
                            unset($AllData[$k][$num]);
                        }
                    }
                }
            }
            break;
        case 22:
            $AllData = $MB->fetchAllByDate($date, 1);
            break;
        case 23:
            $AllData = $TR->fetchAllByDate($date, 1);
            break;
    }

}

if (isset($_POST['PushButton_1'])) {

    $member_id_list = array();
    if ($Type != 21) {

        if (isset($_POST['CommentForAll']) && !empty($_POST['CommentForAll'])) {
            $_POST['CommentForAll'] = strip_tags($_POST['CommentForAll']);
            $_POST['CommentForAll'] = htmlspecialchars($_POST['CommentForAll']);
            $CommentForAll = $_POST['CommentForAll'];
        }

        foreach ($_POST['GroupChanges'] as $id => $item) {
            /*
             * Если первый элемент фильтра не 0, т.е. не на все отделы, то проверяем можно ли
             * пользователю производить действия по данному типу и отделу
             */
            if (isset($RoleDepartmentFilter[$Type][0]) && $RoleDepartmentFilter[$Type][0] != 0) {
                /*
                 * Получаю данные пользователя по ID
                 */
                $cur = current(array_filter($Members->GetMembers(), function ($Member) use (&$id) {
                    return $Member->getId() == $id;
                }));

                /*
                 * Получаю id отдела пользователя
                 */
                $dep_id = $cur->getDepartment()['id'];
                /*
                 * Если этот отдел не в массиве разрешенных отделов,
                 * то пользователь выполняет действия которые ему не разрешены.
                 * Отколняю эти действия
                 */
                if (!in_array($dep_id, $RoleDepartmentFilter[$Type])) {
                    continue;
                }
            }


            /*
            * Выкусываем теги
            */
            $item["value"] = strip_tags($item["value"]);
            $item["note"] = strip_tags($item["note"]);
            /*
             * Экранирую html спец. символы
             */
            $item["value"] = htmlspecialchars($item["value"]);
            $item["note"] = htmlspecialchars($item["note"]);
            /*
             * проверяю существование только цифр, и точки
             */
            $item["value"] = preg_replace("/[^0-9.,\-]/i", "", $item["value"]);

            /*
             * Заменяю , на .
             */
            $item["value"] = str_replace(",", ".", $item["value"]);

            $value = $item['value'];
            if ($value == '')
                $value = 0;

            $note = $item['note'];

//            if (empty($value)) {
//                continue;
//            }

            if (isset($CommentForAll)) {
                $note = $CommentForAll;
            }

            /*
             * Если значения не равны или
             * не существует комментария на всх + комментарии не равны, то
             * обновляю или создаю записи в БД
             */
            if (!(isset($AllData[$id]))) {
                $AllData[$id]["summ"] = 0;
                $AllData[$id]["note"] = 0;
            }

            if ((($AllData[$id]["summ"] != $value) || ((!isset($CommentForAll)) && ($AllData[$id]["note"] != $note)))) {
                /*
                * ДОбавляю или Обновляю значения в БД
                */
                switch ($Type) {
                    case 1:
                        $AB->addByType($id, $date, 1, $value, $note, $User->getMemberId());
                        break;
                    case 2:
                        $AB->addByType($id, $date, 2, $value, $note, $User->getMemberId());
                        break;
                    case 3:
                        $AB->addByType($id, $date, 3, $value, $note, $User->getMemberId());
                        break;
                    case 4:
                        $AB->addByType($id, $date, 4, $value, $note, $User->getMemberId());
                        break;
                    case 5:
                        $AB->addByType($id, $date, 5, $value, $note, $User->getMemberId());
                        break;
                    case 6:
                        $AB->addByType($id, $date, 6, $value, $note, $User->getMemberId());
                        break;
                    case 7:
                        $AB->addByType($id, $date, 7, $value, $note, $User->getMemberId());
                        break;
//                    case 8:
//                        $AB->addByType($id, $date, 8, $value, $note, $User->getMemberId());
//                        break;
                    case 9:
                        $AB->addByType($id, $date, 9, $value, $note, $User->getMemberId());
                        break;
//                    case 10:
//                        $AB->addByType($id, $date, 10, $value, $note, $User->getMemberId());
//                        break;
                    case 11:
                        $RN->addByType($id, $date, 1, $value, $note, $User->getMemberId());
                        break;
                    case 12:
                        $RN->addByType($id, $date, 2, $value, $note, $User->getMemberId());
                        break;
                    case 13:
                        $RN->addByType($id, $date, 3, $value, $note, $User->getMemberId());
                        break;
                    case 14:
                        $RN->addByType($id, $date, 4, $value, $note, $User->getMemberId());
                        break;
                    case 15:
                        $RN->addByType($id, $date, 5, $value, $note, $User->getMemberId());
                        break;
                    case 16:
                        $RN->addByType($id, $date, 6, $value, $note, $User->getMemberId());
                        break;
                    case 17:
                        $RN->addByType($id, $date, 7, $value, $note, $User->getMemberId());
                        break;
                    case 18:
                        $CR->addByType($id, $date, 1, $value, $note, $User->getMemberId());
                        break;
                    case 19:
                        $CR->addByType($id, $date, 2, $value, $note, $User->getMemberId());
                        break;
                    case 20:
                        $CR->addByType($id, $date, 3, $value, $note, $User->getMemberId());
                        break;
                    case 22:
                        $MB->setMobile($date,1, $id, $value, $note, $User->getMemberId(),0);
                        break;
                    case 23:
                        $TR->setTransport($date,1,$id,$value,$note,$User->getMemberId(),0);
                        break;
                }
                $member_id_list[] = $id;
            }
        }
    } else {
        $KPI_Group = $_POST['kpi'];
        foreach ($KPI_Group as $member_id=>$value) {
            foreach ($value as $num=>$item) {
                $item["num"] = $num;
                if (isset($AllData[$member_id])) {
                    $cr = current(array_filter($AllData[$member_id], function ($data) use ($num) {
                        return $data->getNumber() == $num;
                    }));

                    if ($cr) {
                        if ($cr->getNumber() == $num && $cr->getSumm() == $item['summ'] && $cr->getNote() == $item['note']) {
                            continue;
                        } else {
                            try {
                                $KPI->updateWithDateToPayment($member_id, $item, $date, 1, $User->getMemberId());
                            } catch (Exception $e) {
                                echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
                            }
                        }
                    } else {
                        if (!empty($item['summ']) || !empty($item['note'])) {
                            $KPI->updateWithDateToPayment($member_id, $item, $date, 1, $User->getMemberId());
                        }
                    }
                } else {
                    if (!empty($item['summ']) || !empty($item['note'])) {
                        $KPI->updateWithDateToPayment($member_id, $item, $date, 1, $User->getMemberId());
                    }
                }

                $member_id_list[] = $member_id;
            }
        }
    }

//    $RS = new ReportStatus();
//    if (isset($_POST['FilterDepartment'])) {
//        $Status = $RS->getFiltredStatus($date, 1, $_POST['FilterDepartment']);
//    } else {
//        $Status = $RS->getStatus($date, 1);
//    }

    $RS = new ReportStatus();

    if (count($member_id_list)>0) {
        $MI = new MemberInterface();
        foreach ($member_id_list as $member_id) {
            $member = $MI->fetchMemberByID($member_id);
            $member->getDepartment()['id'];
            $Status = $RS->getFiltredStatus($date, 1, $member->getDepartment()['id']);

            if ($Status['status_id'] <= 1 || $Status['status_id']>4) {
                echo "Статус ведомости у сотрудника (".$member->getLastName()." ".$member->getName().") <1 и >4";
                continue;
            }

            $PR = new Premium();
            $PR->setMember($member);
            $PR->setDate($date);
            $MB = new MobileBlock();
            $TR = new TransportBlock();
            $MB->getMobile($date,1, $member->getId());
            $TR->getTransport($date,1, $member->getId());

            try {
                $PR->fetch($member_id, true);
            } catch (Exception $e) {
                echo $e->getMessage();
            }

            $PR->reloadRetentions();
            $PR->reloadAdministrative();
            $PR->reloadCorrections();

            /*
                * Создаю объект интерфейса ведомости
                */
            $PI = new PayRollInterface();
            // Создаю объект ведомости и заполняю его
            $PayRoll = new PayRoll();
            $PayRoll->setDate($date); // Дата
            $PayRoll->setTypeOfPayment(1); // Тип выплаты
            $PayRoll->setId($member->getId()); // Пользовательский ID

            $PayRoll->setDirection($member->getDirection()); // Напраление
            $PayRoll->setDepartment($member->getDepartment()); // Отдел
            $PayRoll->setPosition($member->getPosition()); // Должность


            $PayRoll->setFio($member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()); // ФИО
            $PayRoll->setSalary($member->getSalary()); // Оклад

            $PayRoll->setNDFL($PR->getNdfl()); // НДФЛ
            $PayRoll->setRO($PR->getRo()); // ИЛ
            $PayRoll->setData($PR->getCommercialJson());

//            $PayRoll->setAdvancePayment($All_Salary); // АВАНС
            $PayRoll->setAdvancePayment($PR->getAllSalary()); // АВАНС

            $PayRoll->setAwhdata(["absence" => ($PR->getAbsence() + $PR->getNewAbsence()), "holiday" => $PR->getHoliday(), "disease" => $PR->getDisease()]);
            $PayRoll->setAdministrativeData($PR->getAB_Data());
            $PayRoll->setRetentionData($PR->getRN_Data());
            $PayRoll->setCorrectionData($PR->getCR_Data());

            $PayRoll->setAbsences($PR->getAllAbsenceRub()); // Отсутствия
            if ($member->getMotivation() == 7) {
                $PayRoll->setCommercial(0); // Коммерческая премия
            } else {
                $PayRoll->setCommercial($PR->getAllMarkupRubles()); // Коммерческая премия
            }

            $PayRoll->setMobile($MB->getSumm());
            $PayRoll->setTransport($TR->getSumm());

            $PayRoll->setAdministrative($PR->getAllAdministrativeRubles()); // Административная премия
            $PayRoll->setHold($PR->getAllHoldRubles()); // Удержания
            $PayRoll->setCorrecting($PR->getAllCorrectionRubles()); // Корректировки

            $PayRoll->setKPI($PR->getKPISumm());

            $PayRoll->setYearbonusPay($PR->getYearBonusPay());

            $PayRoll->setBaseprocentId($PR->getBasepercentId());
            $PayRoll->setAdvancedProcentId($PR->getAdvancedPercentId());

            $PayRoll->setMotivation($member->getMotivation());
            $PayRoll->Calculation();

            //Добавляю в массив объектов
            $PayRollArray[] = $PayRoll;
        }

        unset($PR);
        unset($MI);

        foreach ($PayRollArray as $item) {
            try {
                $item->setSumm(roundSumm($item->getSumm()));
                $PI->UpdateFinancialPayout($item);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

    }

    /*
     * В зависимости от выбранного типа получаю данные из БД
     * или NULL если нету данных
     */
    switch ($Type) {
        case 1:
            $AllData = $AB->fetchAllByDate($date, 1);
            break;
        case 2:
            $AllData = $AB->fetchAllByDate($date, 2);
            break;
        case 3:
            $AllData = $AB->fetchAllByDate($date, 3);
            break;
        case 4:
            $AllData = $AB->fetchAllByDate($date, 4);
            break;
        case 5:
            $AllData = $AB->fetchAllByDate($date, 5);
            break;
        case 6:
            $AllData = $AB->fetchAllByDate($date, 6);
            break;
        case 7:
            $AllData = $AB->fetchAllByDate($date, 7);
            break;

        case 9:
            $AllData = $AB->fetchAllByDate($date, 9);
            break;
//        case 10:
//            $AllData = $AB->fetchAllByDate($date, 10);
//            break;
        case 11:
            $AllData = $RN->fetchAllByDate($date, 1);
            break;
        case 12:
            $AllData = $RN->fetchAllByDate($date, 2);
            break;
        case 13:
            $AllData = $RN->fetchAllByDate($date, 3);
            break;
        case 14:
            $AllData = $RN->fetchAllByDate($date, 4);
            break;
        case 15:
            $AllData = $RN->fetchAllByDate($date, 5);
            break;
        case 16:
            $AllData = $RN->fetchAllByDate($date, 6);
            break;
        case 17:
            $AllData = $RN->fetchAllByDate($date, 7);
            break;
        case 18:
            $AllData = $CR->fetchAllByDate($date, 1);
            break;
        case 19:
            $AllData = $CR->fetchAllByDate($date, 2);
            break;
        case 20:
            $AllData = $CR->fetchAllByDate($date, 3);
            break;
        case 21:
            $AllData = $KPI->fetchAllByDate($date);
            break;
        case 22:
            $AllData = $MB->fetchAllByDate($date, 1);
            break;
        case 23:
            $AllData = $TR->fetchAllByDate($date, 1);
            break;
    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Групповое занесение данных</title>
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../bower_components/FixedHeader-3.1.4/css/fixedHeader.bootstrap.css">

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">

    <style>
        .input-group .form-control:not(:first-child):not(:last-child), .input-group-addon:not(:first-child):not(:last-child), .input-group-btn:not(:first-child):not(:last-child) {
            z-index: 0;
        }

        input {
            height: 30px !important;
            padding: 6px 12px;
            font-size: 11px !important;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            padding: 5px;
        }

        table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
            position: inherit !important;
            float: left;
             bottom: 0px;
             right: 0px;
            /* display: block; */
            font-family: 'Glyphicons Halflings';
            opacity: 0.5;
            margin-right: 10px !important;
        }

    </style>
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">

            <?php
            if ($Ini_Max_Input_Vars < 2000) {
                ?>
                <div class="box box-warning box-solid">
                    <div class="box-header">Предупреждение</div>
                    <div class="box-body">
                        Данные могут не полностью передаваться. Требуется настроить файл <code>php.ini</code> и увеличить значение параметра
                        <code>max_input_vars</code> более 2000.
                    </div>
                </div>
                <?php
            }
            ?>

            <div class="box box-solid">
                <div class="box-header">
                    <h4>Групповое занесение данных</h4>
                </div>
                <div class="box-body">
                    <form class="form-horizontal" id="settings" method="post">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Выберите дату: </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker" name="date" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                           value="<?php echo $date; ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="form-group" style="display: none">
                            <label class="col-sm-2 control-label">Тип выплаты: </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-check"></i>
                                    </div>
                                    <select class="form-control" name="TypeOfPayment" disabled>
                                        <option value="1" <?php if (isset($TypeOfPayment) && ($TypeOfPayment == 1)) {
                                            echo "selected";
                                        }; ?> >Премия
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Тип начислений/удержаний: </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-table"></i>
                                    </div>
                                    <select class="form-control" id="Type" name="Type">
                                        <?php
                                        if ($Permission_Promotions == 1) {
                                            ?>
                                            <option id="1" value="1" <?php if (isset($Type) && $Type == 1) echo "selected"; ?>>Административный блок - Акции</option>
                                            <?php
                                        }
                                        if ($Permission_AdministrativePrize == 1) {
                                            ?>
                                            <option id="2" value="2" <?php if (isset($Type) && $Type == 2) echo "selected"; ?>>Административный блок - Административная премия</option>
                                            <?php
                                        }
                                        if ($Permission_Training == 1) {
                                            ?>
                                            <option id="3" value="3" <?php if (isset($Type) && $Type == 3) echo "selected"; ?>>Административный блок - Обучение</option>
                                            <?php
                                        }
                                        if ($Permission_PrizeAnotherDepartment == 1) {
                                            ?>
                                            <option id="4" value="4" <?php if (isset($Type) && $Type == 4) echo "selected"; ?>>Административный блок - Премия другого отдела</option>
                                            <?php
                                        }
                                        if ($Permission_Defect == 1) {
                                            ?>
                                            <option id="5" value="5" <?php if (isset($Type) && $Type == 5) echo "selected"; ?>>Административный блок - Неликвиды и брак</option>
                                            <?php
                                        }
                                        if ($Permission_Sunday == 1) {
                                            ?>
                                            <option id="6" value="6" <?php if (isset($Type) && $Type == 6) echo "selected"; ?>>Административный блок - Воскресения</option>
                                            <?php
                                        }
                                        if ($Permission_NotLessThan == 1) {
                                            ?>
                                            <option id="7" value="7" <?php if (isset($Type) && $Type == 7) echo "selected"; ?>>Административный блок - Выплаты не менее</option>
                                            <?php
                                        }

                                        if ($Permission_Overtime == 1) {
                                            ?>
                                            <option id="9" value="9" <?php if (isset($Type) && $Type == 9) echo "selected"; ?>>Административный блок - Переработки</option>
                                            <?php
                                        }

                                        if ($Permission_MobileCommunication == 1) {
                                            ?>
                                            <option id="22" value="22" <?php if (isset($Type) && $Type == 22) echo "selected"; ?>>Мобильная связь</option>
                                        <?php
                                        }

                                        if ($Permission_CompensationFAL == 1) {
                                            ?>
                                            <option id="23" value="23" <?php if (isset($Type) && $Type == 23) echo "selected"; ?>>Компенсация ГСМ</option>
                                            <?php
                                        }

                                        if ($Permission_LateWork == 1) {
                                            ?>
                                            <option id="11" value="11" <?php if (isset($Type) && $Type == 11) echo "selected"; ?>>Удержания - За опоздания</option>
                                            <?php
                                        }
                                        if ($Permission_Schedule == 1) {
                                            ?>
                                            <!-- <option id="12" value="12" <?php if (isset($Type) && $Type == 12) echo "selected"; ?>>Удержания - За график работы</option> -->
                                            <?php
                                        }
                                        if ($Permission_Internet == 1) {
                                            ?>
                                            <option id="13" value="13" <?php if (isset($Type) && $Type == 13) echo "selected"; ?>>Удержания - За интернет</option>
                                            <?php
                                        }
                                        if ($Permission_CachAccounting == 1) {
                                            ?>
                                            <option id="14" value="14" <?php if (isset($Type) && $Type == 14) echo "selected"; ?>>Удержания - Кассовый учет</option>
                                            <?php
                                        }
                                        if ($Permission_Receivables == 1) {
                                            ?>
                                            <option id="15" value="15" <?php if (isset($Type) && $Type == 15) echo "selected"; ?>>Удержания - Дебиторская задолженность</option>
                                            <?php
                                        }
                                        if ($Permission_Credit == 1) {
                                            ?>
                                            <option id="16" value="16" <?php if (isset($Type) && $Type == 16) echo "selected"; ?>>Удержания - Кредит</option>
                                            <?php
                                        }
                                        if ($Permission_Other == 1) {
                                            ?>
                                            <option id="17" value="17" <?php if (isset($Type) && $Type == 17) echo "selected"; ?>>Удержания - Другое</option>
                                            <?php
                                        }

                                        if ($Permission_BonusAdjustment == 1) {
                                            ?>
                                            <option id="18" value="18" <?php if (isset($Type) && $Type == 18) echo "selected"; ?>>Корректировки - Корректировка бонуса</option>
                                            <?php
                                        }
                                        if ($Permission_Calculation == 1) {
                                            ?>
                                            <option id="19" value="19" <?php if (isset($Type) && $Type == 19) echo "selected"; ?>>Корректировки - Расчет</option>
                                            <?php
                                        }
                                        if ($Permission_YearBonus == 1) {
                                            ?>
                                            <option id="20" value="20" <?php if (isset($Type) && $Type == 20) echo "selected"; ?>>Корректировки - Годовой бонус</option>
                                            ?>
                                            <?php
                                        }
                                        if ($Permission_Promotions == 1) {
                                            ?>
                                            <option id="21" value="21" <?php if (isset($Type) && $Type == 21) echo "selected"; ?>>KPI</option>
                                            <?php
                                        }
                                        ?>

                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Отдел: </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-check"></i>
                                    </div>

                                    <select class="form-control" name="FilterDepartment" id="FilterDepartment">
                                        <option id="-1" value="-1">Все отделы</option>
                                        <?php
                                        if (isset($DI)) {
                                            foreach ($DI->GetDepartments() as $department) {
                                                if (isset($_POST['FilterDepartment'])) {
                                                    if ($department->getId() == $_POST['FilterDepartment']) {
                                                        echo "<option id='" . $department->getId() . "' value='" . $department->getId() . "' selected>" . $department->getName() . "</option>";
                                                    } else {
                                                        echo "<option id='" . $department->getId() . "' value='" . $department->getId() . "'>" . $department->getName() . "</option>";
                                                    }
                                                } else {
                                                    echo "<option id='" . $department->getId() . "' value='" . $department->getId() . "'>" . $department->getName() . "</option>";
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <?php
                        if (isset($Type) && $Type == 21 ) {
                            ?>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Должность: </label>
                                <div class="col-sm-10">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-check"></i>
                                        </div>
                                        <select class="form-control" name="FilterPosition" id="FilterPosition">
                                            <option value="-1">Все</option>
                                            <?php
                                            if (isset($PositionList)) {
                                                foreach ($PositionList as $p) {
                                                    if (isset($FilterPosition)) {
                                                        if ($p['id'] == $FilterPosition) {
                                                            echo "<option id='" . $p['id'] . "' value='" . $p['id'] . "' selected>" . $p['name'] . "</option>";
                                                        } else {
                                                            echo "<option id='" . $p['id'] . "' value='" . $p['id'] . "'>" . $p['name'] . "</option>";
                                                        }
                                                    } else {
                                                        echo "<option id='" .$p['id'] . "' value='" . $p['id'] . "'>" . $p['name'] . "</option>";
                                                    }
                                                }
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">Номер KPI: </label>
                                <div class="col-sm-10">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-check"></i>
                                        </div>
                                        <select class="form-control" name="KPINum" id="KPINum">
                                            <option value="-1">Все KPI</option>
                                            <option value="0" <?php if ($KPI_Num_Filter==0) echo "selected";?> >1</option>
                                            <option value="1" <?php if ($KPI_Num_Filter==1) echo "selected";?> >2</option>
                                            <option value="2" <?php if ($KPI_Num_Filter==2) echo "selected";?> >3</option>
                                            <option value="3" <?php if ($KPI_Num_Filter==3) echo "selected";?> >4</option>
                                            <option value="4" <?php if ($KPI_Num_Filter==4) echo "selected";?> >5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                        ?>

                        <input type="submit" class="btn btn-flat btn-default center-block" name="SendDate" value="Получить данные">
                    </form>

                </div>
            </div>

            <?php
            if (isset($_POST['SendDate']) || isset($_POST['PushButton_1'])) {
                ?>
                <form method="POST" id="form-data" class="form-horizontal">
                    <input type="hidden" name="Type" value="<?php echo $Type; ?>">
                    <input type="hidden" name="date" value="<?php echo $date; ?>">

                    <?php
                    if (isset($_POST['FilterDepartment'])) {
                        ?>
                        <input type="hidden" name="FilterDepartment" value="<?php echo $_POST['FilterDepartment']; ?>">
                        <?php
                    }
                    ?>

                    <div class="box box-solid" id="ReturnBox">
                        <div class="box-body">
                            <div class="col-md-12">

                                <?php
                                if (isset($_POST['FilterDepartment'])) {
                                    $FilterDepartment = $_POST['FilterDepartment'];
                                };

                                if ($Type != 21) {
                                    ?>
                                    <table id="MembersTable" class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-2">Фамилия Имя</th>
                                            <th clas="col-md-2">Имя Фамилия</th>
                                            <th class="col-md-2">Отдел</th>
                                            <th class="col-md-3">Сумма</th>
                                            <th class="col-md-3">Комментарий</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php
                                        $FullSumm = 0;
                                        foreach ($Members->GetMembers() as $member) {
                                            if ($member->getWorkStatus() != 1)
                                                continue;

                                            $dep_id = $member->getDepartment()['id'];
                                            if (isset($RoleDepartmentFilter[$Type][0]) && $RoleDepartmentFilter[$Type][0] != 0) {
                                                if (!in_array($dep_id, $RoleDepartmentFilter[$Type])) {
                                                    continue;
                                                }
                                            }

                                            /*
                                            * Если дата выхода на работу меньше расчитываемомй даты, то пропускаем этих сотрудников
                                            */
                                            $member_date = date("m.Y", strtotime($member->getEmploymentDate()));
                                            if (strtotime("01." . $date) < strtotime("01." . $member_date)) {
                                                continue;
                                            }

                                            if ($FilterDepartment != -1) {
                                                if ($member->getDepartment()['id'] != $FilterDepartment) {
                                                    continue;
                                                }
                                            }

                                            $value = NULL;
                                            $note = NULL;

                                            if ($AllData != NULL) {
                                                if (isset($AllData[$member->getId()])) {
                                                    $value = $AllData[$member->getId()]['summ'];
                                                    $note = $AllData[$member->getId()]['note'];
                                                    $FullSumm += $value;
                                                }
                                            }

                                            ?>
                                            <tr>
                                                <td>
                                                    <a href="premium.php?id=<?php echo $member->getId(); ?>&date=<?php echo $date; ?>"> <?php echo $member->getLastName() . " " . $member->getName(); ?></a>
                                                </td>
                                                <td>
                                                    <?php echo $member->getName() . " " . $member->getLastName(); ?>
                                                </td>
                                                <td>

                                                    <?php echo $member->getDepartment()['name']; ?>

                                                </td>
                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px; margin-right:0px; margin-left:0px;">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <?php
                                                            if ($value != NULL) {
                                                                ?>
                                                                    <span style="display: none"><?php echo $value; ?></span>
                                                                <input type="text" class="form-control" name="GroupChanges[<?php echo $member->getId(); ?>][value]" id="GroupChanges_<?php echo $member->getId(); ?>" placeholder="<?php echo $value; ?>" title="<?php echo $note; ?>" value="<?php echo $value; ?>" autocomplete="off">
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <input type="text" class="form-control" name="GroupChanges[<?php echo $member->getId(); ?>][value]" id="GroupChanges_<?php echo $member->getId(); ?>" placeholder="0" title="" autocomplete="off">
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px; margin-right:0px; margin-left:0px;">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                    <i class='fa fa-comment'></i>
                                                                </div>
                                                                <?php
                                                                if ($note != NULL) {
                                                                    ?>
                                                                    <textarea name='GroupChanges[<?php echo $member->getId(); ?>][note]' id='GroupChanges_note_<?php echo $member->getId(); ?>' cols='1' rows='1' class='form-control'><?php echo $note; ?></textarea>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <textarea name='GroupChanges[<?php echo $member->getId(); ?>][note]' id='GroupChanges_note_<?php echo $member->getId(); ?>' cols='1' rows='1' class='form-control'></textarea>
                                                                    <?php
                                                                }
                                                                ?>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <?php
                                } else {
                                    ?>
                                    <input type="hidden" name="KPINum" value="<?php echo $KPI_Num_Filter; ?>">

                                    <input type="hidden" name="FilterPosition" value="<?php echo $FilterPosition; ?>">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">Фамилия Имя</th>
                                            <th clas="col-md-1">Имя Фамилия</th>
                                            <th clas="col-md-1">Отдел</th>
                                            <th clas="col-md-1">Должность</th>
                                            <th class="col-md-1">Номер KPI</th>
                                            <th class="col-md-3">Сумма</th>
                                            <th class="col-md-3">Комментарий</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php
                                    $counter = 0;
                                    $FullSumm = 0;
                                    foreach ($Members->GetMembers() as $member) {
                                        if ($member->getWorkStatus() != 1)
                                            continue;

                                        $dep_id = $member->getDepartment()['id'];
                                        if (isset($RoleDepartmentFilter[$Type][0]) && $RoleDepartmentFilter[$Type][0] != 0) {
                                            if (!in_array($dep_id, $RoleDepartmentFilter[$Type])) {
                                                continue;
                                            }
                                        }

                                        if (isset($FilterPosition) && $FilterPosition != -1){
                                            if ($member->getPosition()['id'] != $FilterPosition)
                                                continue;
                                        }

                                        /*
                                        * Если дата выхода на работу меньше расчитываемомй даты, то пропускаем этих сотрудников
                                        */
                                        $member_date = date("m.Y", strtotime($member->getEmploymentDate()));
                                        if (strtotime("01." . $date) < strtotime("01." . $member_date)) {
                                            continue;
                                        }

                                        if ($FilterDepartment != -1) {
                                            if ($member->getDepartment()['id'] != $FilterDepartment) {
                                                continue;
                                            }
                                        }

                                        if ($member->getMotivation() != 1 && $member->getMotivation() != 2 && $member->getMotivation() != 3 && $member->getMotivation() != 4)
                                            continue;

                                        $counter++;
                                        ?>
                                        <?php
                                        if ($KPI_Num_Filter == -1 ) {
                                            for ($i = 0; $i < 5; $i++) {
                                                if (isset($AllData[$member->getId()][$i])) $FullSumm += $AllData[$member->getId()][$i]->getSumm();
                                                ?>
                                                <?php
                                                if ($counter % 2 == 0) {
                                                    ?>
                                                    <tr>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <tr style="background-color: #f7f7f7">
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                                if ($i == 0) {
                                                    ?>
                                                    <td>
                                                        <a href="premium.php?id=<?php echo $member->getId(); ?>&date=<?php echo $date; ?>"> <?php echo $member->getLastName() . " " . $member->getName() . "(" . $member->getId() . ")"; ?></a>
                                                    </td>
                                                    <td>
                                                        <?php echo $member->getName() . " " . $member->getLastName(); ?>
                                                    </td>
                                                    <td>

                                                        <?php echo $member->getDepartment()['name']; ?>

                                                    </td>
                                                    <td>

                                                        <?php echo $member->getPosition()['name']; ?>

                                                    </td>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <?php
                                                }

                                                echo "<td>" . ($i + 1) . "</td>";
                                                ?>
                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px; margin-right:0px; margin-left:0px;">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="kpi[<?php echo $member->getId(); ?>][<?php echo $i; ?>][summ]" placeholder="0" title=""
                                                                   value="<?php if (isset($AllData[$member->getId()][$i])) echo $AllData[$member->getId()][$i]->getSumm(); ?>" autocomplete="off">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px; margin-right:0px; margin-left:0px;">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-comment"></i>
                                                            </div>
                                                            <textarea class="form-control" style="height: 30px;" name="kpi[<?php echo $member->getId(); ?>][<?php echo $i; ?>][note]" placeholder=""
                                                                      autocomplete="off"><?php if (isset($AllData[$member->getId()][$i])) echo $AllData[$member->getId()][$i]->getNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </td>
                                                </tr>
                                                <?php
                                            }
                                        } else {
                                            if (isset($AllData[$member->getId()][$KPI_Num_Filter])) $FullSumm += $AllData[$member->getId()][$KPI_Num_Filter]->getSumm();
                                            ?>
                                            <?php
                                            if ($counter % 2 == 0) {
                                                ?>
                                                <tr>
                                                <?php
                                            } else {
                                                ?>
                                                <tr style="background-color: #f7f7f7">
                                                <?php
                                            }
                                            ?>

                                            <td>
                                                <a href="premium.php?id=<?php echo $member->getId(); ?>&date=<?php echo $date; ?>"> <?php echo $member->getLastName() . " " . $member->getName() . "(" . $member->getId() . ")"; ?></a>
                                            </td>
                                            <td>
                                                <?php echo $member->getName() . " " . $member->getLastName(); ?>
                                            </td>
                                            <td>

                                                <?php echo $member->getDepartment()['name']; ?>

                                            </td>
                                            <td>

                                                <?php echo $member->getPosition()['name']; ?>

                                            </td>

                                            <?php
                                            echo "<td>" . ($KPI_Num_Filter + 1) . "</td>";
                                            ?>
                                            <td>
                                                <div class="form-group" style="margin-bottom: 0px; margin-right:0px; margin-left:0px;">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-ruble"></i>
                                                        </div>
                                                        <input type="text" class="form-control" name="kpi[<?php echo $member->getId(); ?>][<?php echo $KPI_Num_Filter; ?>][summ]" placeholder="0" title=""
                                                               value="<?php if (isset($AllData[$member->getId()][$KPI_Num_Filter])) echo $AllData[$member->getId()][$KPI_Num_Filter]->getSumm(); ?>" autocomplete="off">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group" style="margin-bottom: 0px; margin-right:0px; margin-left:0px;">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-comment"></i>
                                                        </div>
                                                        <textarea class="form-control" style="height: 30px;" name="kpi[<?php echo $member->getId(); ?>][<?php echo $KPI_Num_Filter; ?>][note]" placeholder=""
                                                                  autocomplete="off"><?php if (isset($AllData[$member->getId()][$KPI_Num_Filter])) echo $AllData[$member->getId()][$KPI_Num_Filter]->getNote(); ?></textarea>
                                                    </div>
                                                </div>
                                            </td>
                                            </tr>
                                            <?php
                                        }
                                            ?>
                                    <?php
                                    }
                                    ?>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>

                        <div class="box-footer">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Сумма итого</label>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-ruble"></i>
                                        </div>
                                        <input type="text" class="form-control" id="Itogo" placeholder="0" title="" value="<?php echo $FullSumm; ?>" autocomplete="off" disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Общий комментарий</label>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <div class='input-group-addon'>
                                            <i class='fa fa-comment'></i>
                                        </div>
                                        <textarea name='CommentForAll' cols='1' rows='1' class='form-control'></textarea>
                                    </div>
                                </div>
                            </div>

                            <button class="btn btn-flat btn-primary center-block" onclick="<?=(!$Status?'if (!confirmation()) return false;':'');?>$('#modal-save').modal('show');" name="PushButton_1">Сохранить</button>
                        </div>
                    </div>
                </form>
                <?php
            }
            ?>

            <div class="modal fade" id="modal-save" style="background: rgba(0,0,0,1);">
                <div class="modal-dialog" style="width: 95%">
                    <div class="modal-content">
                        <div class="modal-header text-center" style="color: white;background-color: #000;border: unset;">
                            <h3 class="modal-title" style="color: white;background-color: #000;border: unset;">Сохранение данных</h3>
                        </div>

                        <div class="modal-body" style="background-color: #000;">
                            <div class="box box-solid box-info" style="height: 70%;background-color: #000;border: unset;">
                                <div class="box-body">
                                    <img class="img-responsive center-block" src="images/7TwN.gif">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/FixedHeader-3.1.4/js/dataTables.fixedHeader.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>

<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>


<script>
    function confirmation() {
        return confirm("Ведомость премии на дату <?=$date?> не открыта. Хотите сохранить?");
    }

    // Обработка событий по нажатию enter
    $(document).ready(function() {
        $("#form-data").keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });

    $(document).ready(function() {
        var n = $("#Itogo").val();
        n = n.replace(/\B(?=(?:\d{3})+(?!\d))/g, ' ');
        $("#Itogo").val(n)
    });

    $(document).ready(function () {
        $(document).on("change", "input[id^=GroupChanges]", function () {
            var FinalSumm = 0;
            $("input[id^=GroupChanges]").each(function () {

                var Value = $(this).val();
                if (!Value) {
                    Value = 0;
                } else {
                    Value = Value.replace(",", ".");
                }
                FinalSumm = parseInt(FinalSumm) + parseInt(Value);
            });

            FinalSumm = FinalSumm.replace(/\B(?=(?:\d{3})+(?!\d))/g, ' ');
            $("#Itogo").val(FinalSumm);
        });


    });

    $(function () {
        $(document).on("click", "div[id^=btn_comment]", function () {
            var currentId = $(this).attr("id");
            var currentName = $(this).attr("name");
            var numberInID = currentId.replace("btn_comment_", "");


            if ($("#" + currentName + "_note_" + numberInID).is(':visible')) {
                $("#" + currentName + "_" + numberInID).show();
                $("#" + currentName + "_note_" + numberInID).hide();
            } else {
                $("#" + currentName + "_" + numberInID).hide();
                $("#" + currentName + "_note_" + numberInID).show();
            }
        });

        // Выбор даты
        $(function () {
            $('#datepicker').datepicker({
                language: 'ru',
                autoclose: true,
                viewMode: "months",
                minViewMode: "months",
                format: 'mm.yyyy',
            })
        });

        //
        $('#MembersTable').DataTable({
            'fixedHeader': true,
            'paging': false,
            'lengthChange': false,
            'searching': true,
            'ordering': true,
            'info': false,
            'autoWidth': false
        })
    })


    $(document).ready(function () {
        /*
         * Получаю массив разрешенных отделов в зависимости от роли
         */
        var Filter = <?php echo json_encode($RoleDepartmentFilter); ?>;
        /*
         * Скрываю все отделы
         */
        $('select[name=FilterDepartment] option').hide();
        $('select[name=FilterDepartment] option[id="-1"]').show();
        /*
         * Получаю текущий выбранный тип извлекаю его значение
         */
        var val = $('select[name=Type]').val();
        /*
         * Если значение у фильтра == 0, то разрешены все группы
         */
        if (Filter[val] == 0) {
            $('select[name=FilterDepartment] option').show();
        } else {
            /*
             * Если по этому значению в переменной Filter находится массив массив, то
             * перебираю его и отображаю разрешенные option у select с именем FilterDepartmnet
             */
            if (Array.isArray(Filter[val])) {
                Filter[val].forEach(function (item) {
                    $('select[name=FilterDepartment] option[id=' + item + ']').show();
                });
            } else {
                /*
                 * Если это не массив, то просто вывожу разрешенный option
                 */
                $('select[name=FilterDepartment] option[id=' + item + ']').show();
            }
        }

        /*
         * Обрабатываю событие при изменении select'а Type. Логика такая же как в onReady
         */
        $('select[name=Type]').change(function () {
            /*
            * Получаю текущий выбранный тип извлекаю его значение
            */
            var value = $('select[name=Type]').val();
            /*
            * Скрываю все отделы
            */
            $('select[name=FilterDepartment] option').hide();
            $('select[name=FilterDepartment] option[id="-1"]').show();
            /*
             * Если значение у фильтра == 0, то разрешены все группы
             */
            if (Filter[value] == 0) {
                $('select[name=FilterDepartment] option').show();
            } else {
                /*
                * Если по этому значению в переменной Filter находится массив массив, то
                * перебираю его и отображаю разрешенные option у select с именем FilterDepartmnet
                */
                if (Array.isArray(Filter[value])) {
                    Filter[value].forEach(function (item) {
                        $('select[name=FilterDepartment] option[id=' + item + ']').show();
                    });
                } else {
                    /*
                    * Если это не массив, то просто вывожу разрешенный option
                    */
                    $('select[name=FilterDepartment] option[id=' + item + ']').show();
                }
            }
        });
    });
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>

