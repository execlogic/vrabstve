<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 15.02.19
 * Time: 13:33
 */

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/MemberInterface.php';
require_once 'app/DepartmentInterface.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

$RoleDepartmentFilter = NULL;

foreach ($Roles as $Role) {
    if ($Role->getId() == 6 || $Role->getId() == 4 || $Role->getId() == 2) {
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter[-1] = 0;
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[$Role->getDepartment()] = 1;
        }
    }
}

if ($RoleDepartmentFilter == null) {
    header("Location: member_list.php");
    exit();
}

$MI = new MemberInterface();
$MI->fetchMembers();

$members_fininfo = $MI->getMembersFinanceInfo();

$DI = new DepartmentInterface();
$DI->fetchDepartments();

$departments = $DI->GetDepartments();



$FinInfo = array();
$BossInfo = array();

foreach ($departments as $object) {
    $dep_id = $object->getId();
//    echo $object->getName()." <BR>";

    if ($RoleDepartmentFilter != null && !isset($RoleDepartmentFilter['-1'])) {
        if (!isset($RoleDepartmentFilter[$dep_id])) {
            continue;
        }
    }

    $filter = array_filter($members_fininfo, function ($mfi) use($dep_id) {
       return $mfi['department_id'] == $dep_id;
    });


    if ($filter) {
        foreach ($filter as $key=>$item) {
            if ($item['position_id'] == 6 || $item['position_id'] == 7 || $item['position_id'] == 8 || $item['position_id'] == 9 || $item['position_id'] == 10 || $item['position_id'] == 11 ||
                $item['position_id'] == 27 || $item['position_id'] == 28 || $item['position_id'] == 29 || $item['position_id'] == 30 || $item['position_id'] == 39) {
                $tmp = $item;
                unset($filter[$key]);
                array_unshift($filter, $tmp);
                $BossInfo[] = $item;
            }
        }

        foreach ($filter as $item) {
            $FinInfo[$dep_id][] = $item;
        }
    }
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Отчет по основным фин. условиям</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="box box-solid collapsed-box">
                <div class="box-header">
                    <h4>Фин. условия по отделам</h4>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <thead>
                        <th>Фамилия</th>
                        <th>Имя</th>
                        <th>Отчество</th>
                        <th>Должность</th>
                        <th>Оклад</th>
                        <th>Базовый процент</th>
                        <th>Административная премия</th>
                        <th>Мобильная связь</th>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($FinInfo as $key=>$value) {
                            echo "<tr><td style='text-align: center' colspan='8'><h5><b>".$value[0]['department_name']."</b></h5></td></tr>";
                            foreach ($value as $item) {
                                echo "<tr>";
                                echo "
                                <td>" . $item['lastname'] . "</td>
                                <td>" . $item['name'] . "</td><td>" . $item['middle'] . "</td><td>" . $item['position_name'] . "</td>
                                <td>" . $item['salary'] . "</td><td>" . $item['base_percent'] . "</td><td>" . $item['AdministrativePrize'] . "</td><td>" . $item['MobileCommunication'] . "</td>";
                                echo "</tr>";
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="box box-solid collapsed-box">
                <div class="box-header">
                    <h4>Фин. условия руководители</h4>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <thead>
                        <th>Фамилия</th>
                        <th>Имя</th>
                        <th>Отчество</th>
                        <th>Должность</th>
                        <th>Оклад</th>
                        <th>Базовый процент</th>
                        <th>Административная премия</th>
                        <th>Мобильная связь</th>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($BossInfo as $item) {

                                echo "<tr>";
                                echo "
                                <td>" . $item['lastname'] . "</td>
                                <td>" . $item['name'] . "</td><td>" . $item['middle'] . "</td><td>" . $item['position_name'] . "</td>
                                <td>" . $item['salary'] . "</td><td>" . $item['base_percent'] . "</td><td>" . $item['AdministrativePrize'] . "</td><td>" . $item['MobileCommunication'] . "</td>";
                                echo "</tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php require_once 'footer.php'; ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>

<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<script>
    $(function () {

        $('#MembersTable').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })

        $('#Table3').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })

    })
</script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
