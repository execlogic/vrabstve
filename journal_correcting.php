<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.10.18
 * Time: 18:30
 */

// Время работы скрипта
$start = microtime(true);

require_once "app/ErrorInterface.php";
require_once 'app/MemberInterface.php';
require_once "app/MemberFinanceBlock.php";

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

// Настройки
if (!array_filter($Roles, function ($Role) {
    return ($Role->getReportCorrecting() == 1);
})) {
    header("Location: 404.php" );
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();


$date_m = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');

$Error = new ErrorInterface();

$Members = new MemberInterface();
$Members->fetchMembers();

$date = date("m.Y");


$CorrectingNachenki = array();

if (isset($_POST['SendDate']) && isset($_POST['date'])) {
    $date = $_POST['date'];

    foreach ($Members->GetMembers() as $member) {
        if ($Members->getWorkStatusByDate($member->getId(), $date) != 1) {
            continue;
        }

        // Если дата выхода на работу меньше расчитываемомй даты, то игнорируем эти строки
        $member_date = date("m.Y", strtotime($member->getEmploymentDate()));
        if (strtotime("01." . $date) < strtotime("01." . $member_date)) {
            continue;
        }

        // Объявляем переменную с ID пользователя
        $user_id = $member->getId();

        // Выставляю мотивацию по дате и user_id
        $member->setMotivation($Members->getMotivationByDate($user_id, $date));
        // Коммерческий блок
        $MFB = new MemberFinanceBlock($member); // Получение данных наценки в зависимости от мотивации
        try {
            $MFB->fetchWithDateObj($date); // 0 при мотивации <=0 и >=6
        } catch (Exception $e) {
            $Error->add($e->getMessage());
        }

        $Data = $MFB->getData();
        if (!empty($Data)) {
            foreach ($Data as $item) {
                $Data_Correct = $item->getCorrect();
                if (!empty($Data_Correct)) {
		    if (!isset($CorrectingNachenki[$member->getId()])) {
                        $CorrectingNachenki[$member->getId()] = array("FN"=>$member->getLastName()." ".$member->getName(),"Correct"=>$item->getCorrect(),"Note"=>$item->getCorrectNote());
		    } else {
			$CorrectingNachenki[$member->getId()]["Correct"] += $item->getCorrect();
		    }
//                    echo $member->getLastName()." ".$member->getName();
//                    echo $item->getCorrect() . "<BR>";
                }
            }
        }

    }
}
$menu_open = 3;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Журнал корректировок</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
               Контроль корректировки наценки
            </h4>
            <ol class="breadcrumb">
                <li class="active">Журнал ведомостей</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="box box-solid box-plane">
                <div class="box-body">
                    <form class="form-horizontal" method="post">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Выберите дату c: </label>
                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker" name="date" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                           value="<?php echo $date; ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-2"><input type="submit" class="btn btn-flat btn-default" name="SendDate" value="Получить данные"></div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="box box-solid box-plane">
                <div class="box-body">
                    <table class="table table-striped table-hover" id="DataTable">
                        <thead>
                        <tr>
                            <th>ФИО</th>
                            <th>Корректировка</th>
                            <th>Заметка</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach ($CorrectingNachenki as $key=>$value) {
                                echo "<tr>";
                                echo "<td><a href='premium.php?id=".$key."&date=".$date."'>".$value['FN']."</td><td>".$value['Correct']."</td><td>".$value['Note']."</td>";
                                echo "</tr>";
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
    $(function () {
        // Выбор даты
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            format: 'mm.yyyy',
        })
    })
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>


