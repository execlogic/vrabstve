<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 05.10.18
 * Time: 13:55
 */

// Время работы скрипта
$start = microtime(true);

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/ErrorInterface.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/MemberInterface.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/CustomPercent.php';

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/MemberFinanceBlock.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/DataItem.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/DataManager.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/ErrorInterface.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/AdministrativeBlock.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Retention.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Correction.php';

require_once $_SERVER['DOCUMENT_ROOT'] . '/app/ProductionCalendar.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/AwhInterface.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/PayRollInterface.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/PayRoll.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/ReportStatus.php';
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/EnvelopeInterface.php";

require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . "/admin/RoleInterface.php";

require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Notify.php';

require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Premium.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Avans.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/Avans2.php';

require_once $_SERVER['DOCUMENT_ROOT'] . '/app/DepartmentInterface.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/DirectionInterface.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/app/EnvelopeInterface.php';

require_once $_SERVER['DOCUMENT_ROOT'] . '/app/xlsSave.php';

function roundSumm($summ)
{
    // Округление до 10
    if ($summ > 0) { //Если сумма <= 0 то не обрабатываем данные
        //$summ = round($summ); // округляю сотые и десятые
        $summ = (int)$summ;

        $ostatok = ($summ % 10); // Получаю остаток от деления на 10
        if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
            $summ = $summ - $ostatok + 10;
        } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
            $summ = $summ - $ostatok;
        };
    } else {
        $summ = 0;
    }
    return $summ;
}

class Filter
{
    private $direction_id;
    private $department_id;

    /**
     * @param mixed $department_id
     */
    public function setDepartmentId($department_id)
    {
        $this->department_id = $department_id;
    }

    /**
     * @param mixed $direction_id
     */
    public function setDirectionId($direction_id)
    {
        $this->direction_id = $direction_id;
    }

    /**
     * @return mixed
     */
    public function getDepartmentId()
    {
        return $this->department_id;
    }

    /**
     * @return mixed
     */
    public function getDirectionId()
    {
        return $this->direction_id;
    }
}

function is_in_array($F, $key, $value)
{
    foreach ($F as $item) {
        if ($item->$key() == $value) {
            return true;
        }
    }

    return false;
}

session_start();
if (isset($_SESSION['UserObj'])) {
    $User = $_SESSION['UserObj'];
} else {
    header("Location: index.php");
}

if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

/*
 * Права доступа
 */

$ShowExtended = 0;

/*
 *  Если у пользользователя не стоит доступ в FinancialStatement, то выкидываем его в 404
 */
if (!array_filter($Roles, function ($Role) {
    return ($Role->getFinancialStatement() == 1);
})) {
    header("Location: 404.php");
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

/*
 *  Настройки
 */


/*
 * Массив с отделами к которым разрешен доступ
 */

$RoleDepartmentFilter = array();

foreach ($Roles as $Role) {
    switch ($Role->getId()) {
        case 2:
        case 4:
            if (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] == 0) {
                continue 2;
            }

            // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
            if ($Role->getDepartment() == 0) {
                $RoleDepartmentFilter = array();
            } else {
                /*
                 * Иначе добавляю в конец массива значение об отделе
                 */
                $RoleDepartmentFilter[] = $Role->getDepartment();
            }
            break;
        case 5:
        case 6:
            $RoleDepartmentFilter = array();
            break 2;
        default:
            break;
    }
}


session_write_close();

$Error = new ErrorInterface();

/*
 * Создаю объект MemberInterface
 */
$Members = new MemberInterface();

$DI = new DepartmentInterface();
$DI->fetchActiveDepartments();
$Departmetns = $DI->GetDepartments();

$DR = new DirectionInterface();
$DR->fetchDirections();
$Directions = $DR->GetDirections();

$FilterArray = array();

//var_dump($Directions); echo "<BR><BR>";
//var_dump($Departmetns); echo "<BR><BR>";

usort($Departmetns, function ($a, $b) {
    return $a->getName() > $b->getName();
});

$AWH = new AwhInterface();
/*
 * Выставляю дату на текущую
 */
$date = date("m.Y");

if (isset($_COOKIE['prepaid_expense']))
    $date = $_COOKIE['prepaid_expense'];

if (isset($_COOKIE['prepaid_expense_TypeOfPayment']))
    $TypeOfPayment = $_COOKIE['prepaid_expense_TypeOfPayment'];

if (isset($_REQUEST['ShowExtended'])) {
    $ShowExtended = 1;
}

/*
 * Если был запрос в котором присутсвует дата и тип выплаты, то осуществляю расчет
 */
if ((isset($_COOKIE['prepaid_expense']) || isset($_REQUEST['date'])) && (isset($_REQUEST['TypeOfPayment']) || isset($_COOKIE['prepaid_expense_TypeOfPayment']))) {
    if (isset($_REQUEST['TypeOfPayment'])) {
        /*
         * Проверя тип выплат
         * Убираю теги
         */
        $_REQUEST['TypeOfPayment'] = strip_tags($_REQUEST['TypeOfPayment']);
        /*
         * Экранирую специальные символы
         */
        $_REQUEST['TypeOfPayment'] = htmlspecialchars($_REQUEST['TypeOfPayment']);
        /*
         * Проверяю регуляркой на цифры
         */
        $_REQUEST['TypeOfPayment'] = preg_replace("/[^0-9]/i", "", $_REQUEST['TypeOfPayment']);

        /*
        * Объявляю переменные тип выплаты
        */
        $TypeOfPayment = $_REQUEST['TypeOfPayment'];

        if (isset($_COOKIE['prepaid_expense_TypeOfPayment'])) {
            setcookie("prepaid_expense_TypeOfPayment", $TypeOfPayment);
        } else {
            setcookie("prepaid_expense_TypeOfPayment", $TypeOfPayment, time() + 3000);
        }
    }

    /*
     * Создаю объект статуса ведомости
     */
    $RS = new ReportStatus();
    $ReportStatus = $RS->getStatus($date, $TypeOfPayment);

    if ($ReportStatus['status_id'] == 6) {
        $DI->fetchDepartments();
        $Departmetns = $DI->GetDepartments();
        usort($Departmetns, function ($a, $b) {
            return $a->getName() > $b->getName();
        });
    }


    /*
     * Чертовы фильтры
     */
    if (isset($_POST['DepartmentFilter'])) {
        $DepartmentFilter = $_POST['DepartmentFilter'];
    }

    if (isset($_POST['DirectionFilter'])) {
        $DirectionFilter = $_POST['DirectionFilter'];
    }

    if (isset($_POST['DepartmentNot'])) {
        $DepartmentNot = 1;
    } else {
        $DepartmentNot = 0;
    }

    if (isset($_REQUEST['DirectionNot'])) {
        $DirectionNot = 1;
    } else {
        $DirectionNot = 0;
    }

    if (isset($_REQUEST['DirectorFilter'])) {
        /*
         * Получаю в переменную
         */
        $DirectorFilter = $_REQUEST['DirectorFilter'];
    }

    /*
     * Исключаю отделы по правам доступа
     */
    $tmp_Direction = array();
    foreach ($Departmetns as $k => $d) {
        if ((count($RoleDepartmentFilter) > 0) && (!in_array($d->getId(), $RoleDepartmentFilter))) {
            $tmp_Direction[] = $d->getDirectionId()[0];
            unset($Departmetns[$k]);
        }
    }

    /*
     * Исключаю направления
     */
    $tmp_Direction = array_unique($tmp_Direction);
    if (count($tmp_Direction) > 0) {
        foreach ($Directions as $key => $d) {
            if (!in_array($d->getId(), $tmp_Direction)) {
                unset($Directions[$key]);
            }
        }
    }
    unset($tmp_Direction);

    /*
     * Глобальный фильтр по отделам
     */
    $Global_Direction_Filter = array();
    foreach ($Directions as $value) {
        $Global_Direction_Filter[] = $value->getId();
    }

    /*
     * Глобальный фильтр по направлению
     */
    $Global_Department_Filter = array();
    foreach ($Departmetns as $value) {
        $Global_Department_Filter[] = $value->getId();
    }

    /*
     * Если установлен фильтр по выдающему
     */
    if (isset($DirectorFilter)) {
        /*
         * Если не равен нулю, то есть данные (но это не точно)
         */
        if ($DirectorFilter != 0) {
            /*
             * Создаю 2 массива для направления и отдела
             */
            $Filter_Department = array();
            $Filter_Directions = array();

            /*
             * Получаю данные по направлению и отдела по ID выдающего директора
             */
            $R = $RI->getDepartmnetByMemberId($DirectorFilter);

            /*
             * Разбираю данные и заношу в массивы
             */
            foreach ($R as $d) {
                $Filter_Department[] = $d['department_id'];
                $Filter_Directions[] = $d['direction_id'];
            }

            /*
             * Оставляю только совпадающие значения значения
             */
            $Filter_Directions = array_unique($Filter_Directions);
            $Filter_Department = array_unique($Filter_Department);


            $Global_Direction_Filter = array_intersect($Global_Direction_Filter, $Filter_Directions);
            $Global_Department_Filter = array_intersect($Global_Department_Filter, $Filter_Department);


            /*
             * Перебираю отделы и удаляю те что не подходят по фильтру
             */
            foreach ($Departmetns as $key => $d) {
                $id = $d->getId();
                if (!in_array($id, $Filter_Department)) {
                    unset($Departmetns[$key]);
                }
            }

            /*
             * Перебираю направления и удаляю те что не подходят по фильтру
             */
            foreach ($Directions as $key => $d) {
                $direction_id = $d->getId();
                if (!in_array($direction_id, $Filter_Directions)) {
                    unset($Directions[$key]);
                }
            }
        }

    }

    /*
     * Если установлен фильтр по направлению, то разбираю оставшиеся направления
     */
    if (isset($DirectionFilter) && ($DirectionFilter != 0)) {
        $tmp = $Directions;

        /*
         * Перебираю направления
         */
        foreach ($Directions as $key => $d) {
            /*
             * Получаю ID
             */
            $id = $d->getId();

            /*
             * Проверяю стоит ли исключение
             * Так как поле с возможностью выбора только одного элемента,
             */
            if ($DirectionNot == 0) {
                foreach ($Global_Direction_Filter as $k => $v) {
                    if ($v != $DirectionFilter) {
                        unset($Global_Direction_Filter[$k]);
                    }
                }
            } else {
                foreach ($Global_Direction_Filter as $k => $v) {
                    if ($v == $DirectionFilter) {
                        unset($Global_Direction_Filter[$k]);
                    }
                }
            }
        }

        /*
         * Получаю темповый массив направлений
         */
        $d_array = array();
        foreach ($Directions as $d) {
            if ($DirectionFilter == $d->getId())
                $d_array[] = $d->getId();
            else
                continue;
        }

        /*
         * Перебираю отделы и исключаю те что не в фильтре
         */
        $tmp_departments = array();
        foreach ($Departmetns as $key => $value) {
            if (!in_array($value->getId(), $Global_Department_Filter))
                continue;

            /*
             * Не стоит исключение
             */
            if ($DirectionNot == 0) {
                if (!in_array($value->getDirectionId()[0], $d_array)) {
                    unset($Departmetns[$key]);
                } else {
                    $tmp_departments[] = $value->getId();
                }
            } else {
                if (in_array($value->getDirectionId()[0], $d_array)) {
                    unset($Departmetns[$key]);
                } else {
                    $tmp_departments[] = $value->getId();
                }
            }
        }
        $Global_Department_Filter=$tmp_departments;
        unset($d_array);
    };


    if ((isset($DepartmentFilter)) && (count($DepartmentFilter) > 0)) {
        if ($DepartmentNot) {
            $Global_Department_Filter = array_diff($Global_Department_Filter, $DepartmentFilter);
        } else {
            $Global_Department_Filter = array_intersect($Global_Department_Filter, $DepartmentFilter);
        }
    }

    /*
    * Конец фильтров
    */

    if (isset($_REQUEST['date'])) {
        /*
         * Выкусываем теги
         */
        $_REQUEST['date'] = strip_tags($_REQUEST['date']);
        /*
         * Экранирую html спец. символы
         */
        $_REQUEST['date'] = htmlspecialchars($_REQUEST['date']);
        /*
         * в дате проверяю существование только цифр, и точки
         */
        $_REQUEST['date'] = preg_replace("/[^0-9.]/i", "", $_REQUEST['date']);

        /*
        * Объявляю переменные дата
        */
        $date = $_REQUEST['date'];

        if (isset($_COOKIE['prepaid_expense'])) {
            setcookie("prepaid_expense", $date);
        } else {
            setcookie("prepaid_expense", $date, time() + 3000);
        }
    };

    /*
     * Создам массив для данных
     */
    $PayRollArray = array();
    /*
     * Создаю объект интерфейса ведомости
     */
    $PI = new PayRollInterface();

    /*
     * Получаю статус ведомости
     */
    if (isset($Global_Direction_Filter) && is_array($Global_Direction_Filter)) {
        $report_filter = implode(",", $Global_Department_Filter);
    }

    /*
     * Если получили статус == 0, то
     */
//    if ($ReportStatus['status_id'] == 0) {
        /*
         * Проверяю были ли внесены необходимые для ведомости данные
         */
//        if ($RS->checkStatus($date, $TypeOfPayment)) {
//            /*
//             * Если данные все внесены, то  изменяю статус
//             */
////            $RS->ChangeStatus($date, $TypeOfPayment, 1, "-2");
//            $RS->InitReportStatus($date, $TypeOfPayment);
//            /*
//             * Получаю новый статус
//             */
////            $ReportStatus = $RS->getStatus($date, $TypeOfPayment);
//        }
//    }

    if (isset($report_filter) && !empty($report_filter)) {
        $ReportStatus = $RS->getFiltredStatus($date, $TypeOfPayment, $report_filter);
    } else {
        $ReportStatus = $RS->getStatus($date, $TypeOfPayment);
    }

    /*
    * Получаю массив данных кто тупит на стадии 1,2
    */
    $WhoBrake = $RS->whoBrake($date, $TypeOfPayment);

    /*
     * Проверям была ли нажата кнопка следующей стадии
     */

    /*
     * Объявляю переменную которая разрешает отображение кнопки следующей стадии
     */
    $ShowButtonNextStage = true;

    $PC = new ProductionCalendar();
    /*
     * Извлекаю массив с рабочими днями
     */
    $PC->fetchWorkingArray($date);
    /*
     * Получаю массив с рабочими днями
     */
    $working_days_array = $PC->getWDArray();
    /*
     * Получаю вторую часть рабочих дней при котором сотрудник не получает аванса
     */
    $working_part2 = array_splice($working_days_array, ceil((count($working_days_array) / 2)));

    $s1 = []; // Стадия меньше 2
    $s2 = []; // Стадия больше или равно 2
    foreach ($WhoBrake as $w) {
        if ($w["id"] == 31)
            continue;

        if (count($RoleDepartmentFilter) != 0) {
            if (!in_array($w["id"], $RoleDepartmentFilter)) {
                continue;
            }
        }
        if ($w['status_id'] < 2) {
            $s1[] = $w['id'];
        } else {
            $s2[] = $w['id'];
        }
    }


//    foreach ($WhoBrake as $w) {
//        $PC->fetchByDateWithDepartment($date, $w['id']);
//        $working_days_month = $PC->get()["working_days"];

        /*
         * Исключаю бухгалтерию из ведомости
         */
//        if ($w["id"] == 31)
//            continue;

        /*
        * Если отдел не попадает в фильтр роли, то продолжаю
        */
//        if (count($RoleDepartmentFilter) != 0) {
//            if (!in_array($w["id"], $RoleDepartmentFilter)) {
//                continue;
//            }
//        }

        if (count($s1)>0) {
            try {
                /*
                * Получаю пользователей по отделам
                */
                $Members->fetchMembersbyDepartment($s1);

                foreach ($Members->GetMembers() as $member) {
                    $user_id = $member->getId();
                    if ($TypeOfPayment == 2) {
                        if ($member->getEmploymentDate()) {
                            if (strpos($member->getEmploymentDate(), "-")) {
                                $emp_date = DateTime::createFromFormat("Y-m-d H:i:s", $member->getEmploymentDate());
                                $emp_date_arr = explode(".", $emp_date->format("d.m.Y"));
                            } else {
                                $emp_date_arr = explode(".", $member->getEmploymentDate());
                            }
                            if ($emp_date_arr[1] . "." . $emp_date_arr[2] == $date) {
                                if (in_array($emp_date_arr[0], $working_part2)) {
                                    continue;
                                }
                            }
                        }
                    }

                    // Дата принятия > чем дата ...
                    if ($TypeOfPayment == 1) {
                        $d1 = DateTime::createFromFormat('d.m.Y', $member->getEmploymentDate());
                        $d2 = DateTime::createFromFormat('d.m.Y', "01." . $date);
                        $d2->modify("+1 month");

                        if ($d1 > $d2) {
                            continue;
                        }
                    }

                    if (isset($Global_Direction_Filter) && (!is_null($Global_Direction_Filter)) && (count($Global_Direction_Filter) > 0)) {
                        if (!in_array($member->getDirection()["id"], $Global_Direction_Filter)) {
                            continue;
                        }
                    }

                    if (isset($Global_Department_Filter) && (!is_null($Global_Department_Filter)) && (count($Global_Department_Filter) > 0)) {
                        if (!in_array($member->getDepartment()["id"], $Global_Department_Filter)) {
                            continue;
                        }
                    }

                    /*
                     * Если дата выхода на работу меньше расчитываемомй даты, то пропускаем этих сотрудников
                     */
                    $member_date = date("m.Y", strtotime($member->getEmploymentDate()));
                    if (strtotime("01." . $date) < strtotime("01." . $member_date)) {
                        continue;
                    }

                    /*
                    * Уборщица в авансе не учавствует
                    */
                    if (($TypeOfPayment == 2) && $member->getAvansPay() == 0) {
                        continue;
                    }

                    /*
                     * Проверяю рабочий статус
                     * ЭТО БУДЕТ РАБОТАТЬ ТОЛЬКО ЕСЛИ СВОЕВРЕМЕННО УВОЛЬНЯТЬ ПОЛЬЗОВАТЕЛЕЙ
                     */
                    if ($member->getWorkStatus() != 1) {
                        continue;
                    }

                    $PC->fetchByDateWithDepartment($date, $member->getDepartment()['id']);
                    $working_days_month = $PC->get()["working_days"];

                    /*
                     * Создаю объект ведомости и заполняю его
                     */
                    $PayRoll = new PayRoll();
                    $PayRoll->setDate($date); // Дата
                    $PayRoll->setTypeOfPayment($TypeOfPayment); // Тип выплаты
                    $PayRoll->setId($member->getId()); // Пользовательский ID
                    $PayRoll->setEmail($member->getEmails());
                    $PayRoll->setDirection($member->getDirection()); // Напраление
                    $PayRoll->setDepartment($member->getDepartment()); // Отдел
                    $PayRoll->setPosition($member->getPosition()); // Должность
                    $PayRoll->setWorkingdays($working_days_month);
                    $PayRoll->setFio($member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()); // ФИО
                    $PayRoll->setSalary($member->getSalary()); // Оклад

                    if ($TypeOfPayment == 1) {
                        $PR = new Premium();
                        $PR->setMember($member);
                        $PR->setDate($date);

                        try {
                            $PR->fetch($member->GetId());
                        } catch (Exception $e) {
                            echo $e->getMessage()."<BR>";
                        }

                        $CR = $PR->getCR();
                        $MB = new MobileBlock();
                        $TR = new TransportBlock();
                        $MB->getMobile($date, $TypeOfPayment, $user_id);
                        $TR->getTransport($date, $TypeOfPayment, $user_id);

                        if ($member->getMobileFormula() == 1 || $member->getTransportFormula() == 1) {
                            $member_awh = $AWH->getAwhByMember($date, $TypeOfPayment, $user_id); //Премия
                            $awh_new_absence = $member_awh['new_absence'];
                            $awh_absence = $member_awh['absence'];
                            $awh_holiday = $member_awh['holiday'];
                            $awh_disease = $member_awh['disease'];
                            $awh_extraholiday = $AWH->getExtraHoliday($date, $user_id);
                            $vse_otsutstviya = $awh_absence + $awh_new_absence + $awh_holiday + $awh_disease + $awh_extraholiday;
                            $working_days_month = $PC->get()["working_days"];

                            if ($TR->isAuto($date, 1, $user_id)==true) {
                                $TR->setSumm($member->getTransport());
                                $TR->recountCompensationFAL($vse_otsutstviya, $working_days_month);
                            }

                            if ($MB->isAuto($date, 1, $user_id)==true) {
                                $MB->setSumm($member->getMobileCommunication());
                                $MB->recountMobile($vse_otsutstviya, $working_days_month);
                            }
                        }

                        /*
                        * Если выставлен хоть у кого-то годовой бонус и он больше 100к, то
                        * выставляю флаг что есть годовой бонус.
                        */
                        if ($CR->getYearBonus() >= 100000) {
                            $YearBonusStatus = true;
                        }
                        unset($CR);

                    } else if ($TypeOfPayment == 2) {
                        $AV = new Avans();
                        $AV->setMember($member);
                        $AV->setDate($date);
                        $AV->fetch($user_id);
                    } else if ($TypeOfPayment == 3) {
                        $AV2 = new Avans2();
                        $AV2->setMember($member);
                        $AV2->setDate($date);

                        try {
                            $AV2->fetch($user_id);
                        } catch (Exception $e) {
                            echo $e->getMessage() . "<BR>";
                        }

                        $MB = new MobileBlock();
                        $TR = new TransportBlock();
                        $MB->getMobile($date, $TypeOfPayment, $user_id);
                        $TR->getTransport($date, $TypeOfPayment, $user_id);
                    };

                    switch ($TypeOfPayment) {
                        case 1:
                            $PayRoll->setNDFL($PR->getNdfl()); // НДФЛ
                            $PayRoll->setRO($PR->getRo()); // ИЛ
                            $PayRoll->setData($PR->getCommercialJson());
                            break;
                        case 2:
                            $PayRoll->setNDFL(0); // Получаю НДФЛ
                            $PayRoll->setRO(0); // Получаю ИЛ
                            break;
                        case 3:
                            $PayRoll->setNDFL($AV2->getNdfl()); // НДФЛ
                            $PayRoll->setRO($AV2->getRo()); // ИЛ
                            break;
                    }

                    if ($TypeOfPayment == 1) {
                        $PayRoll->setAdvancePayment($PR->getAllSalary()); // АВАНС
                    } else if ($TypeOfPayment == 2) {
                        $PayRoll->setAdvancePayment($AV->getAllSalary());
                    } else if ($TypeOfPayment == 3) {
                        $PayRoll->setAdvancePayment($AV2->getAllSalary());
                    };

                    if ($TypeOfPayment == 1) {
                        $PayRoll->setAwhdata(["absence" => ($PR->getAbsence() + $PR->getNewAbsence()), "holiday" => $PR->getHoliday(), "disease" => $PR->getDisease()]);
                        $PayRoll->setAdministrativeData($PR->getAB_Data());
                        $PayRoll->setRetentionData($PR->getRN_Data());
                        $PayRoll->setCorrectionData($PR->getCR_Data());
                        $PayRoll->setAbsences($PR->getAllAbsenceRub()); // Отсутствия
                        if ($member->getMotivation() == 7) {
                            $PayRoll->setCommercial(0); // Коммерческая премия
                        } else {
                            $PayRoll->setCommercial($PR->getAllMarkupRubles()); // Коммерческая премия
                        }
                        $PayRoll->setAdministrative($PR->getAllAdministrativeRubles()); // Административная премия
                        $PayRoll->setMobile($PR->getAllMobile());
                        $PayRoll->setTransport($PR->getAllTransport());
                        $PayRoll->setHold($PR->getAllHoldRubles()); // Удержания
                        $PayRoll->setCorrecting($PR->getAllCorrectionRubles()); // Корректировки
                        $PayRoll->setKPI($PR->getKPISumm());
                        $PayRoll->setYearbonusPay($PR->getYearBonusPay());
                        $PayRoll->setBaseprocentId($PR->getBasepercentId());
                        $PayRoll->setAdvancedProcentId($PR->getAdvancedPercentId());
                    } else if ($TypeOfPayment == 2) {
                        $PayRoll->setAwhdata(["absence" => ($AV->getAbsence() + $AV->getNewAbsence()), "holiday" => $AV->getHoliday(), "disease" => $AV->getDisease()]);
                        $PayRoll->setAbsences($AV->getAllAbsenceRub()); // Отсутствия
                        $PayRoll->setCommercial(0); // Коммерческая премия
                        $PayRoll->setAdministrative($AV->getAllAdministrativeRubles()); // Административная премия
                        $PayRoll->setHold($AV->getAllHoldRubles()); // Удержания
                        $PayRoll->setAdministrative(0);
                        $PayRoll->setCorrecting(0); // Корректировки
                    } else if ($TypeOfPayment == 3) {
                        $PayRoll->setAwhdata(["absence" => ($AV2->getAbsence() + $AV2->getNewAbsence()), "holiday" => $AV2->getHoliday(), "disease" => $AV2->getDisease()]);
                        $PayRoll->setAbsences($AV2->getAllAbsenceRub()); // Отсутствия
                        $PayRoll->setCommercial(0); // Коммерческая премия
                        $PayRoll->setAdministrative($AV2->getAllAdministrativeRubles()); // Административная премия
                        $PayRoll->setMobile($AV2->getAllMobile());
                        $PayRoll->setTransport($AV2->getAllTransport());
                        $PayRoll->setHold($AV2->getAllHoldRubles()); // Удержания
                        $PayRoll->setCorrecting($AV2->getAllCorrectionRubles()); // Корректировки
                        $PayRoll->setYearbonusPay($AV2->getYearBonusPay());
                        $PayRoll->setAdministrativeData($AV2->getAB_Data());
                        $PayRoll->setRetentionData($AV2->getRN_Data());
                        $PayRoll->setCorrectionData($AV2->getCR_Data());
                        $PayRoll->setBaseprocentId(0);
                        $PayRoll->setAdvancedProcentId(0);
                    }
                    $PayRoll->setMotivation($member->getMotivation());
                    $PayRoll->Calculation();
                    $PayRoll->setFullSumm($PayRoll->getSumm());
                    //Добавляю в массив объектов
                    $PayRollArray[] = $PayRoll;
                }
            } catch (Exception $e) {
            }
        };
        if (count($s2)>0) {
            /*
            * Получаю пользователей по отделам из фикс ведомости
            */
	    try {
                $Members->fetchMembersFinancialbyDepartment($date, $TypeOfPayment, $s2);
	    } catch (Exception $e) {
	    }

            $AllData = $PI->getAllFinancialPayoutByDepartment($date, $TypeOfPayment, $s2);
            if ($AllData) {
                try {
                    foreach ($Members->GetMembers() as $member) {
                        $member_id = $member->getId();

                        $item = current(array_filter($AllData, function ($d) use ($member_id) {
                            return ($d['member_id'] == $member_id);
                        }));

                        if (!$item)
                            continue;

                        if ($item['active'] == 0) {
                            continue;
                        }

                        if (isset($item['department_id'])) {
                            try {
                                $member->setDepartmentFromObject($DI->findById($item['department_id']));
                            } catch (Exception $e) {
                                echo "Ошибка в получении отдела у пользователя: ".$member_id."<BR>";
                                exit(0);
                            }
                        }

                        if (isset($item['direction_id']))
                            $member->setDirectionFromObject($DR->findById($item['direction_id']));

                        /*
                         * Исключаю пользователей которые не состаят в отделах
                        */
                        $D_ID = $member->getDepartment()["id"];

                        $result = current(array_filter($Departmetns, function ($d) use ($D_ID) {
                            return $d->getId() == $D_ID;
                        }));

                        if (!$result) {
                            continue;
                        }

                        if (isset($Global_Direction_Filter) && (!is_null($Global_Direction_Filter)) && (count($Global_Direction_Filter) > 0)) {
                            if (!in_array($member->getDirection()["id"], $Global_Direction_Filter)) {
                                continue;
                            }
                        }

                        if (isset($Global_Department_Filter) && (!is_null($Global_Department_Filter)) && (count($Global_Department_Filter) > 0)) {
                            if (!in_array($D_ID, $Global_Department_Filter)) {
                                continue;
                            }
                        }

                        /*
                         * Если сотрудник не попадает в фильтр роли, то продолжаю
                         */
                        if (count($RoleDepartmentFilter) != 0) {
                            if (!in_array($member->getDepartment()["id"], $RoleDepartmentFilter)) {
                                continue;
                            }
                        }

                        if (isset($item['direction_id'])) {
                            $member_direction = current(array_filter($Directions, function ($d) use ($item) {
                                return $d->getId() == $item['direction_id'];
                            }));
                        }

                        if (isset($item['department_id'])) {
                            $member_departmnet = current(array_filter($Departmetns, function ($d) use ($item) {
                                return $d->getId() == $item['department_id'];
                            }));
                        }

                        if (isset($item['direction_id'])) {
                            $member->setDirection(array("id" => $member_direction->getId(), "name" => $member_direction->getName()));
                        } else {
                            $member->setDirection(array("id" => Null, "name" => "None"));
                        }

                        if (isset($item['department_id']) and $member_departmnet != false) {
                            $member->setDepartment(array("id" => $member_departmnet->getId(), "name" => $member_departmnet->getName()));
                        } else {
                            $member->setDepartment(array("id" => NULL, "name" => "None"));
                        }

                        $PC->fetchByDateWithDepartment($date, $member->getDepartment()['id']);
                        $working_days_month = $PC->get()["working_days"];

                        $MB = new MobileBlock();
                        $TR = new TransportBlock();
                        $MB->getMobile($date,$TypeOfPayment, $member->getId());
                        $TR->getTransport($date,$TypeOfPayment, $member->getId());

                        // Создаю объект ведомости и заполняю его
                        $PayRoll = new PayRoll();
                        $PayRoll->setDate($date); // Дата
                        $PayRoll->setTypeOfPayment($TypeOfPayment); // Тип выплаты
                        $PayRoll->setId($member->getId()); // Пользовательский ID
                        $PayRoll->setEmail($member->getEmails());
                        $PayRoll->setDirection($member->getDirection()); // Напраление
                        $PayRoll->setDepartment($member->getDepartment()); // Отдел
                        $PayRoll->setPosition($member->getPosition()); // Должность
                        $PayRoll->setWorkingdays($working_days_month);
                        $PayRoll->setFio($member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()); // ФИО
//                        $PayRoll->setSalary($member->getSalary()); // Оклад
                        $PayRoll->setSalary($item['Salary']); // получаю данные по окладу
                        $PayRoll->setNDFL($item['ndfl']); // Получаю НДФЛ
                        $PayRoll->setRO($item['ro']); // Получаю ИЛ
                        $PayRoll->setAdvancePayment($item['PrepaidExpense']); // Получаю выплаченный аванс
                        $PayRoll->setAbsences($item['absence']); // Отсутствия
                        $PayRoll->setCommercial($item['CommercialPremium']); // Устанавливаю коммерческую премию
                        $PayRoll->setAdministrative($item['AdministrativePremium']); // Устанавливаю Административную премию
                        $PayRoll->setMobile($MB->getSumm());
                        $PayRoll->setTransport($TR->getSumm());
                        $PayRoll->setHold($item['Hold']); // Устанавливаю удержания
                        $PayRoll->setCorrecting($item['Correcting']); //Устанавливаю корректировки
                        $PayRoll->setYearbonusPay($item['YearBonusPay']);
                        $PayRoll->setMotivation($member->getMotivation());
                        $PayRoll->setKPI($item['Kpi']);
                        $PayRoll->setSumm($item['Total']);
                        $PayRoll->setFullSumm($item['sum']);
                        $PayRoll->setData($item['data']);

                        if (isset($item['AwhData']) && !is_null($item['AwhData'])) {
                            $PayRoll->setAwhdata(unserialize($item['AwhData']));
                        } else {
                            try {
                                $PayRoll->setAwhdata($AWH->getAwhByMember($date, $TypeOfPayment, $member_id));
                            } catch (Exception $e) {
                                echo $e->getMessage() . "<BR>";
                            }
                        }

                        if ($ReportStatus['status_id']<4) {
                            $member_awh = $AWH->getAwhByMember($date, $TypeOfPayment, $member_id);
                            $AWH->fetch_NdflByDate($date, $member_id);
                            $member_ndfl = $AWH->getNdfl();

                            $mobile_prev = $MB->getMobile($date, $TypeOfPayment, $member_id);
                            $transport_prev = $TR->getTransport($date, $TypeOfPayment, $member_id);

                            if ($TypeOfPayment == 1 || $TypeOfPayment == 3) {
                                $awh_new_absence = $member_awh['new_absence'];
                                $awh_absence = $member_awh['absence'];
                                $awh_holiday = $member_awh['holiday'];
                                $awh_disease = $member_awh['disease'];

                                $awh_extraholiday = $AWH->getExtraHoliday($date, $user_id);
                                $vse_otsutstviya = $awh_absence + $awh_new_absence + $awh_holiday + $awh_disease + $awh_extraholiday;
                                $working_days_month = $PC->get()["working_days"];

                                if ($member->getMobileFormula() == 1) {
                                    if ($TR->isAuto($date, $TypeOfPayment, $member_id) == true) {
                                        $TR->setSumm($member->getTransport());
                                        $TR->recountCompensationFAL($vse_otsutstviya, $working_days_month);
                                        $PayRoll->setTransport($TR->getSumm());
                                    }
                                }

                                if ($member->getMobileFormula() == 1) {
                                    if ($MB->isAuto($date, $TypeOfPayment, $member_id) == true) {
                                        $MB->setSumm($member->getMobileCommunication());
                                        $MB->recountMobile($vse_otsutstviya, $working_days_month);
                                        $PayRoll->setMobile($MB->getSumm());
                                    }
                                }

                                if ($mobile_prev != $MB->getSumm() || $transport_prev !=  $TR->getSumm()){
                                    $PayRoll->Calculation();
                                    $PayRoll->setSumm($PayRoll->getSumm());
                                    $PI->UpdateFinancialPayout($PayRoll);
                                }
                            }

                            if (!isset($member_ndfl['ndfl'])) {
                                $member_ndfl['ndfl'] = 0;
                            }
                            if (!isset($member_ndfl['ro'])) {
                                $member_ndfl['ro'] = 0;
                            }

                            $member_ndfl['ndfl'] = round($member_ndfl['ndfl'],0);
                            $member_ndfl['ro'] = round($member_ndfl['ro'],0);

                            if ( (!isset($member_awh['absence'])) || (is_null($member_awh['absence'])) ) {
                                $member_awh['absence'] = 0;
                            }
                            if  ((!isset($member_awh['new_absence'])) || (is_null($member_awh['new_absence']))) {
                                $member_awh['new_absence'] = 0;
                            }

                            if ((!empty($PC->get()["working_days"]))) {
                                /*
                                 *  Расчитываю сумму отсутствия в рублях по формуле ( КОЛИЧЕСТВО_ПРОПУСКОВ * (ОКЛАД / КОЛИЧЕСТВО_РАБОЧИХ_ДНЕЙ))
                                 */
                                $Member_Absence_Rub = (($member_awh['absence'] + $member_awh['new_absence']) * ($member->getSalary() / $PC->get()["working_days"])); // пропуск * За один день.
                            } else {
                                $Member_Absence_Rub = 0;
                            }

                            if ($item['ndfl'] != $member_ndfl['ndfl']) {
//                                echo "НДФЛ у пользователя(".$member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()."): " . $member_ndfl['ndfl'] . " в фиксах: " . $item['ndfl'] . "<BR>";
                                $PayRoll->setNDFL($member_ndfl['ndfl']);
                            }

                            if ($item['ro'] != $member_ndfl['ro']) {
//                                echo "ИЛ пользователя(".$member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()."): " . $member_ndfl['ro'] . " в фиксах: " . $item['ro'] . "<BR>";
                                $PayRoll->setRO($member_ndfl['ro']);
                            }

                            if ($item['absence'] != $Member_Absence_Rub) {
//                                echo "Отсутствия у пользователя(".$member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()."): " . $Member_Absence_Rub . " в фиксах: " . $item['absence'] . "<BR>";
                                $PayRoll->setAbsences($Member_Absence_Rub);
                            }

                            if ($item['Salary']!=$member->getSalary()) {
                                if ($TypeOfPayment==2) {
                                    $PayRoll->setAdvancePayment($member->getSalary()/2);
                                }
                                $PayRoll->setSalary($member->getSalary());
                                $PayRoll->Calculation();
                                $PayRoll->setSumm($PayRoll->getSumm());
                                $PI->UpdateFinancialPayout($PayRoll);
                            }

                            if (($item['ndfl'] != $member_ndfl['ndfl']) ||
                                ($item['ro'] != $member_ndfl['ro']) ||
                                ($item['absence'] != $Member_Absence_Rub)
                            )
                            {
                                $PayRoll->Calculation();
                                $PayRoll->setSumm($PayRoll->getSumm());
                                $PI->UpdateFinancialPayout($PayRoll);
                            }
                        }

                        if ($TypeOfPayment == 1 || $TypeOfPayment == 3) {
                            /*
                            * Таб Административный объект
                            */
                            $AB = new AdministrativeBlock();
                            /*
                             * Тип выплат премия
                             */
                            $AB->setTypeOfPayment($TypeOfPayment);
                            $AB->fetch($item['member_id'], $date);

                            $MB = new MobileBlock();
                            $MB->getMobile($date, $TypeOfPayment, $member_id);

                            $TR = new TransportBlock();
                            $TR->getTransport($date, $TypeOfPayment, $member_id);

                            if (isset($item['AwhData']) && !is_null($item['AwhData'])) {
                                $awh = unserialize($item['AwhData']);
                            } else {
                                $awh = $AWH->getAwhByMember($date, $TypeOfPayment, $member_id);
                            }

                            $vse_otsutstviya = $PayRoll->getAbsences() + $PayRoll->getDisease() + $PayRoll->getHoliday();
                            $PayRoll->setAdministrativeData(["Promotions" => $AB->getPromotions(), "AdministrativePrize" => $AB->getAdministrativePrize(), "Training" => $AB->getTraining(), "PrizeAnotherDepartment" => $AB->getPrizeAnotherDepartment(), "Defect" => $AB->getDefect(), "Sunday" => $AB->getSunday(), "NotLessThan" => $AB->getNotLessThan(), "MobileCommunication" => $MB->getSumm(), "Overtime" => $AB->getOvertime(), "CompensationFAL" => $TR->getSumm()]);
                            unset($AB);

                            $CR = new Correction();
                            $CR->setTypeOfPayment($TypeOfPayment);
                            $CR->fetch($item['member_id'], $date);
                            $PayRoll->setCorrectionData(["BonusAdjustment" => $CR->getBonusAdjustment(), "Calculation" => $CR->getCalculation(), "YearBonus" => $CR->getYearBonus()]);
                            unset($CR);

                            $RN = new Retention();
                            $RN->setTypeOfPayment($TypeOfPayment);
                            $RN->fetch($item['member_id'], $date);
                            $PayRoll->setRetentionData(["LateWork" => $RN->getLateWork(), "Internet" => $RN->getInternet(), "CachAccounting" => $RN->getCachAccounting(), "Receivables" => $RN->getReceivables(), "Credit" => $RN->getCredit(), "Other" => $RN->getOther()]);
                            unset($RN);
                        }


                        $PayRollArray[] = $PayRoll;
                    }
                } catch (Exception $e) {
                    echo $e->getMessage() . "<BR>";
                }
            }
        }
//    }

    // Сортирую полученный массив объектов по ФИО
    if (count($PayRollArray) > 0) {
        usort($PayRollArray, function ($a, $b) {
            return $a->getFio() > $b->getFio();
        });
    }


    if (isset($_POST['OpenStatus'])) {
        $RS->InitReportStatus($date, $TypeOfPayment, $User->getMemberId());
        header('Location: prepaid_expense.php');
        exit;
    }

    /*
     * Откат статуса, при id == 2 или 3
     */
    if (isset($_POST['PrevStatusCustom'])) {
        /*
         * Если существует элемент массива check_all, то скидываем все заначения
         */
        if (isset($_POST['check_all'])) {
            $RS->BackSubStatus($date, $TypeOfPayment, $Global_Department_Filter, $User->getMemberId());
        } else if (isset($_POST['dep'])) {
            foreach ($_POST['dep'] as $id) {
                if (!in_array($id, $Global_Department_Filter)) {
                    echo "Ошибка! Нет прав на изменение отдела<BR>";
                    unset($_POST['dep'][$id]);
                }
            }
            /*
             * Иначе скидываем только переданные подстатусы
             */
            if (count($_POST['dep'])>0)
                $RS->BackSubStatus($date, $TypeOfPayment, $_POST['dep'], $User->getMemberId());
            else
                echo "Нет выбранных отделов<BR>";
        }
    }

    /*
     * Полный откат статуса, если он не равен 2 или 3
     */
    if (isset($_POST['BackStatus'])) {
        if ($ReportStatus['status_id'] == 4) {
            EnvelopeInterface::dropItems($date, $TypeOfPayment);
        }

        if ($ReportStatus['status_id'] != 6) {
            $RS->ChangeStatus($date, $TypeOfPayment, --$ReportStatus['status_id'], $User->getMemberId());

            $ReportStatus = $RS->getStatus($date, $TypeOfPayment);
        }
    }

    /*
     * Следующий статус
     */
    if (isset($_POST['NextStatus'])) {
        $MaxNextStatusID = [];
        foreach ($Roles as $r) {
            if ($r->getId() == 2 || $r->getId() == 4 || $r->getId() == 5 || $r->getId() == 6) {
                $MaxNextStatusID[1] = 0;
            }
            if ($r->getId() == 4 || $r->getId() == 5 || $r->getId() == 6) {
                $MaxNextStatusID[2] = 0;
            }
            if ($r->getId() == 5 || $r->getId() == 6) {
                $MaxNextStatusID[3] = 0;
                $MaxNextStatusID[4] = 0;
                $MaxNextStatusID[5] = 0;
            }
        }

        if (isset($MaxNextStatusID[$ReportStatus['status_id']])) {
            switch ($ReportStatus['status_id']) {
                case 1:
                case 2:
                    if (count($Global_Department_Filter) > 0) {
                        foreach ($Global_Department_Filter as $item) {
                            $r = current(array_filter($WhoBrake, function ($w) use ($item) {
                                return $w['id'] == $item;
                            }));

                            if ($r['status_id'] != $ReportStatus['status_id'])
                                continue;

                            if ($r['status_id'] > 2)
                                continue;

                            try {
                                $RS->ChangeSubStatus($date, $TypeOfPayment, $item, $User->getMemberId());
//                                if ($RS->ChangeSubStatus($date, $TypeOfPayment, $item, $User->getMemberId())) {
                                    /*
                                     * Если изменить статус удалось, то выставляю кнопку в false;
                                     */
//                                    $ShowButtonNextStage = false;
//                                };
                            } catch (Exception $e) {
                                echo "Error:" . $e->getMessage() . "<BR>";
                            }
                        }
                    } else {
                        $RS->ChangeSubStatus($date, $TypeOfPayment, 0, $User->getMemberId());
                    }

                    break;
//                case 3:
//                case 4:
                case 5:
                    $RS->ChangeStatus($date, $TypeOfPayment, (++$ReportStatus['status_id']), $User->getMemberId());
                    break;
            }
        }

        if ($ReportStatus['status_id'] == 1 && isset($MaxNextStatusID[1])) {
            foreach ($PayRollArray as $item) {
                try {
                    $item->setSumm(roundSumm($item->getSumm()));
                    $PI->AddToFinancialPayout($item);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            }
        }
    }

    if (isset($_POST['PrevStatusCustom']) || isset($_POST['BackStatus']) || isset($_POST['NextStatus'])) {
        if (isset($report_filter)) {
            $ReportStatus = $RS->getFiltredStatus($date, $TypeOfPayment, $report_filter);
        } else {
            $ReportStatus = $RS->getStatus($date, $TypeOfPayment);
        }

        if ($ReportStatus['status_id'] == 1 || $ReportStatus['status_id'] == 2 || $ReportStatus['status_id'] == 3) {
            $WhoBrake = $RS->whoBrake($date, $TypeOfPayment);
        }
    }

    if ($ReportStatus['status_id'] != null) {
        foreach ($RI->getOtvDirectors() as $d) {
            $R = $RI->getDepartmnetByMemberId($d['member_id']);

            foreach ($R as $r) {
                $R_Department_ID = $r['department_id'];
                $dep_obj = current(array_filter($Departmetns, function ($d) use ($R_Department_ID) {
                    return $d->getId() == $R_Department_ID;
                }));
                $key = key(array_filter($WhoBrake, function ($w) use ($R_Department_ID) {
                    return $w['id'] == $R_Department_ID;
                }));
                if (isset($WhoBrake[$key])) {
                    $WhoBrake[$key]['director_name'] = $d['lastname'] . " " . $d['name'];
                }
            }
        }

        usort($WhoBrake, function ($a, $b) {
            if ($a['director_name'] > $b['director_name']) {
                return 1;
            } else if ($a['director_name'] < $b['director_name']) {
                return -1;
            } else {
                if ($a['name'] == $b['name']) return 0;
                return ($a['name'] > $b['name']) ? 1 : -1;
            }
        });
    };

    /*
     * Если нажата кнопка конверты, то
     */
    if (isset($_POST['Envelope'])) {
        session_name('Envelope');
        session_start();

        $d = DateTime::createFromFormat("m.Y", $date);
        if ($d->format("m")==12 and $TypeOfPayment==1) {
            foreach ($PayRollArray as $PayRoll) {
                $sum = roundSumm($PayRoll->getSumm());
                if ($sum>200000) {
                    $PayRoll->setSumm($sum % 100000);
                }
            }
        }

        $_SESSION['Envelope']['Data'] = $PayRollArray;
        $_SESSION['Envelope']['TypeOfPayment'] = $TypeOfPayment;
        $_SESSION['Envelope']['date'] = $date;

        if (count($DepartmentFilter)>0 || $DirectionFilter!=0 || $DirectorFilter!=0) {
            $_SESSION['Envelope']['filter'] = true;
        } else {
            $_SESSION['Envelope']['filter'] = false;
        }
        session_write_close();
        header('Location: envelope.php');
    }

    if (isset($_POST['Email'])) {
        session_name('Envelope');
        session_start();
        $_SESSION['Envelope']['Data'] = $PayRollArray;
        $_SESSION['Envelope']['TypeOfPayment'] = $TypeOfPayment;
        $_SESSION['Envelope']['date'] = $date;
        session_write_close();
        header('Location: sendPayrollToEmail.php');
    }

    if (isset($_POST['Excel'])) {
        try {
            $XLS = new xlsSave();
            $XLS->setDate($date);
            $XLS->setTypeOfPayment($TypeOfPayment);
            $filename = $XLS->save($PayRollArray);

            // заставляем браузер показать окно сохранения файла
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($filename));
            header('Content-Transfer-Encoding: binary');
            // читаем файл и отправляем его пользователю
            exit(readfile($filename));

        } catch (Exception $e) {
            echo $e->getMessage()."<BR>";
            exit(0);
        }
    }
};

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ведомость плановой выплаты</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- Select2 -->
    <link rel="stylesheet" href="../../bower_components/select2/dist/css/select2.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/edit.css">
    <link rel="stylesheet" href="css/main.css">

    <script>
        function tableSearch() {
            var phrase = document.getElementById('search-text');
            var table = document.getElementById('info-table');
            var regPhrase = new RegExp(phrase.value, 'i');
            var flag = false;
            for (var i = 1; i < table.rows.length; i++) {
                flag = false;
                for (var j = table.rows[i].cells.length - 1; j >= 0; j--) {
                    flag = regPhrase.test(table.rows[i].cells[j].innerHTML);
                    if (flag) break;
                }
                if (flag) {
                    table.rows[i].style.display = "";
                } else {
                    table.rows[i].style.display = "none";
                }

            }
        }
    </script>
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse" style="overflow-x: auto !important; ">
<div class="wrapper" style="overflow-x: unset !important; overflow-y: unset !important;">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">

            <div class="box box-info box-widget box-solid" style="border-color:#615e97">
                <div class="box-header" style="background-color: #615e97">
                    <h5>
                        <?php
                        if (isset($TypeOfPayment)) {
                            switch ($TypeOfPayment) {
                                case 1:
                                    echo "Расчет премии " . $date;
                                    break;
                                case 2:
                                    echo "Расчет аванса " . $date;
                                    break;
                                case 3:
                                    echo "Расчет аванса2 " . $date;
                                    break;
                                case 4:
                                    echo "Расчет \"расчета\" " . $date;
                                    break;
                                case 5:
                                    echo "Расчет внеплановой выплаты " . $date;
                                    break;
                            }
                        } else {
                            echo "Выбор типа расчета";
                        }
                        ?>
                    </h5>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <form class="form-horizontal" method="post">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Выберите дату: </label>
                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker" name="date" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                           value="<?php echo $date; ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Тип выплаты: </label>
                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-check"></i>
                                    </div>
                                    <select class="form-control" name="TypeOfPayment">
                                        <option value="1" <?php if (isset($TypeOfPayment) && ($TypeOfPayment == 1)) {
                                            echo "selected";
                                        }; ?> >Премия
                                        </option>
                                        <option value="2" <?php if (isset($TypeOfPayment) && ($TypeOfPayment == 2)) {
                                            echo "selected";
                                        }; ?>>Аванс
                                        </option>
                                        <option value="3" <?php if (isset($TypeOfPayment) && ($TypeOfPayment == 3)) {
                                            echo "selected";
                                        }; ?>>Аванс2
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-8 col-md-offset-2">
                                <div class="checkbox">
                                    <label name="ShowExtendedLabel">
                                        <input name="ShowExtended" type="checkbox" <?php if ($ShowExtended == 1) echo "checked"; ?> >
                                        Расширенные данные
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="box box-solid box-default <?php if (!isset($_POST['DirectionFilter'])) { ?> collapsed-box <?php } ?>" style="box-shadow: unset; border-radius: 0px;">
                            <div class="box-header" style="background-color: #f4f4f4;" data-widget="collapse">
                                Доп. фильтры
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>

                            <div class="box-body">

                                <?php
                                if (array_filter($Roles, function ($Role) {
                                    return $Role->getId() == 5 || $Role->getId() == 6;
                                })) {
                                    ?>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Фильтр по отв.директору: </label>
                                        <div class="col-sm-8">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user" style="width: 15px"></i>
                                                </div>
                                                <select name="DirectorFilter" class="form-control">
                                                    <option class="0" value="0">Все</option>
                                                    <?php
                                                    foreach ($RI->getOtvDirectors() as $d) {
                                                        if (isset($DirectorFilter) && $DirectorFilter == $d['member_id']) {
                                                            echo "<option value=" . $d['member_id'] . " selected>" . $d['lastname'] . " " . $d['name'] . "</option>";
                                                        } else {
                                                            echo "<option value=" . $d['member_id'] . ">" . $d['lastname'] . " " . $d['name'] . "</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Фильтр по направлению: </label>
                                    <div class="col-sm-7">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-group"></i>
                                            </div>

                                            <select class="form-control" name="DirectionFilter">
                                                <option value="0">Все направления</option>
                                                <?php

                                                foreach ($Directions as $d) {
                                                    if (isset($DirectionFilter) && $DirectionFilter == $d->getId()) {
                                                        echo "<option value=" . $d->getId() . " selected>" . $d->getName() . "</option>";
                                                    } else {
                                                        echo "<option value=" . $d->getId() . ">" . $d->getName() . "</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="checkbox">
                                            <label>
                                                <input name="DirectionNot" type="checkbox" <?php if (isset($DirectionNot) && ($DirectionNot == 1)) echo "checked"; ?> > Кроме
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Фильтр по отделу: </label>
                                    <div class="col-sm-7">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-group"></i>
                                            </div>
                                            <select name="DepartmentFilter[]" class="form-control select2" multiple="multiple" style="width: 100%;border-radius: unset;color:black;" data-placeholder="Выберите отдел">
                                                <?php
                                                foreach ($Departmetns as $d) {
                                                    foreach ($d->getDirectionId() as $dir) {
                                                        if (isset($DepartmentFilter)) {
                                                            $result = current(array_filter($DepartmentFilter, function ($D) use ($d) {
                                                                return $D == $d->getId();
                                                            }));


                                                            if ($result != false) {
                                                                echo "<option class='" . $dir . "' value=" . $d->getId() . " selected>" . $d->getName() . "</option>";
                                                            } else {
                                                                echo "<option class='" . $dir . "' value=" . $d->getId() . ">" . $d->getName() . "</option>";
                                                            }
                                                        } else {
                                                            echo "<option class='" . $dir . "' value=" . $d->getId() . ">" . $d->getName() . "</option>";
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="checkbox">
                                            <label>
                                                <input name="DepartmentNot" type="checkbox" <?php if (isset($DepartmentNot) && ($DepartmentNot == 1)) echo "checked"; ?> > Кроме
                                            </label>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>

                    <div class="box-footer text-center">
                        <div style="text-align: right;" class="col-sm-6">
                            <input type="submit" class="btn btn-flat btn-primary" name="SendDate" value="Получить данные">


                        <?php
                        if (array_filter($Roles, function ($Role) {
                            return $Role->getId() == 1 || $Role->getId() == 6 || $Role->getId() == 5;
                        }) && $ReportStatus['status_id'] == 0) {
                        ?>
                                <input type="submit" class="btn btn-flat btn-warning" name="OpenStatus" value="Открыть ведомость">
                            <?php
                        }
                        ?>

                        </div>

                        <?php
                        if (array_filter($Roles, function ($Role) {
                            return $Role->getId() == 5 || $Role->getId() == 6;
                        })) {
                            ?>
                            <div style="text-align: left;" class="col-sm-6">
                                <button type="button" class="btn btn-flat btn-default" data-toggle="modal" data-target="#modal-info">
                                    Начислители ЗП
                                </button>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <!--                </form>-->
            </div>

            <!-- Default box -->
            <?php
            if (isset($_REQUEST['date']) || (isset($_COOKIE['prepaid_expense']) && isset($_COOKIE['prepaid_expense_TypeOfPayment']))) {
                ?>
                <div class="box box-solid box-plane">
                    <div class="box-body">
                        <div class="row">
                            <?php
                            foreach ($RS->getStatusList() as $s) {
                                ?>
                                <div class="col-md-2">
                                    <div class="info-box" style="box-shadow: unset; min-height:unset;margin-bottom:unset;">
                                        <?php
                                        if ($s['id'] == $ReportStatus['status_id']) {
                                            ?>
                                            <span class="info-box-icon bg-green" style="height: 45px; width: 45px;line-height: 45px;font-size:23px;color: white"><?php echo $s['id']; ?></span>
                                            <?php
                                        } else {
                                            ?>
                                            <span class="info-box-icon" style="height: 45px; width: 45px;line-height: 45px;font-size:23px;color: white"><?php echo $s['id']; ?></span>
                                            <?php
                                        }
                                        ?>
                                        <div class="info-box-content" style="margin-left: 45px;">
                                            <?php
                                            $id = $s['id'];
                                            ?>
                                            <div data-html="true" class="custom-tooltip" data-toggle="tooltip"><a href='#' style="color: black" data-toggle="modal" data-target="#modal-whobrake"><?php echo $s['name']; ?></a></div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>


                <!--                <form class="form-horizontal" method="post">-->

                <div class="box box-solid box-pane box-widget" style="width: fit-content">

                    <div class="box-body">
                        <div class="box box-solid box-plane" style="background-color: #615e97;">
                            <div class="box-body ">
                                <div class="form-group" style="margin-bottom: 0px;">
                                    <div class="col-sm-2 sticky">
                                        <label class="control-label">Поиск по таблице </label>
                                    </div>
                                    <div class="col-sm-3 sticky2">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-search"></i>
                                            </div>
                                            <input class="form-control" type="text" placeholder="поиск по таблице" id="search-text" onkeyup="tableSearch()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <table class="table table-striped table-hover" id="info-table" style="overflow-y: auto">
                            <thead>
                            <tr>
                                <th>ФИО</th>
                                <th>Направление</th>
                                <th>Отдел</th>
                                <th>Оклад</th>

                                <?php
                                switch ($TypeOfPayment) {
                                    case 1:
                                    case 3:
                                        echo "<th>Выплаченный аванс</th>";
                                        break;
                                    case 2:
                                        echo "<th>Аванс</th>";
                                        break;
                                }
                                ?>


                                <?php
                                if (($TypeOfPayment == 1 || $TypeOfPayment == 2 || $TypeOfPayment == 3) && $ShowExtended == 1) {
                                    echo "<th style='background-color: #627aa7'>Отсутствия (дни)</th>";
                                    echo "<th style='background-color: #627aa7'>Отпуск (дни)</th>";
                                    echo "<th style='background-color: #627aa7'>Болезнь (дни)</th>";
                                }
                                ?>
                                <th>Отсутствия</th>
                                <?php
                                if ($TypeOfPayment == 1 || $TypeOfPayment == 3) {
                                    ?>
                                    <th>НДФЛ</th>
                                    <th>ИЛ</th>

                                    <th>Коммерческая <br>премия(руб)</th>
                                    <th>KPI</th>
                                    <th>Административная <br>премия(руб)</th>

                                    <?php
                                    if (($TypeOfPayment == 1 || $TypeOfPayment == 3) && $ShowExtended == 1) {
                                        echo "<th style='background-color: #627aa7'>Акции</th>";
                                        echo "<th style='background-color: #627aa7'>Административная премия</th>";
                                        echo "<th style='background-color: #627aa7'>Обучение</th>";
                                        echo "<th style='background-color: #627aa7'>Премия другого отдела</th>";
                                        echo "<th style='background-color: #627aa7'>Неликвиды и брак</th>";
                                        echo "<th style='background-color: #627aa7'>Воскресения</th>";
                                        echo "<th style='background-color: #627aa7'>Выплаты не менее</th>";

                                        echo "<th style='background-color: #627aa7'>Переработки</th>";

                                    }
                                    echo "<th>Мобильная связь</th>";
                                    echo "<th>Компенсация ГСМ</th>";
                                    ?>

                                    <th>Удержания(руб)</th>
                                    <?php
                                    if (($TypeOfPayment == 1 || $TypeOfPayment == 3) && $ShowExtended == 1) {
                                        echo "<th style='background-color: #627aa7'>За опоздания</th>";
                                        echo "<th style='background-color: #627aa7'>За интернет</th>";
                                        echo "<th style='background-color: #627aa7'>Кассовый учет</th>";
                                        echo "<th style='background-color: #627aa7'>Дебиторская задолженность</th>";
                                        echo "<th style='background-color: #627aa7'>Кредит</th>";
                                        echo "<th style='background-color: #627aa7'>Другое</th>";
                                    }
                                    ?>

                                    <th>Корректировки(руб)</th>

                                    <?php
                                    if (($TypeOfPayment == 1 || $TypeOfPayment == 3) && $ShowExtended == 1) {
                                        echo "<th style='background-color: #627aa7'>Корректировка бонуса</th>";
                                        echo "<th style='background-color: #627aa7'>Расчет</th>";
                                        echo "<th style='background-color: #627aa7'>Годовой бонус</th>";
                                    }
                                    ?>

                                    <th title="Отчисления в годовой бонус">Отчисления в ГБ</th>

                                    <?php
                                }
                                ?>
                                <th>Итого(руб)</th>
                                <th>Округление(руб)</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            // Объявляю переменные для суммирвания колонок
                            $BankSumm = 0; // Итого сумма в банк
                            $ItogoSumm = 0; // Итого сумма
                            $ItogoOkrugsumm = 0; //Сумма с округлением
                            $OkladSumm = 0; // Сумма оклада
                            $AvansSumm = 0; // Сумма аванса
                            $OtsutstviyaSumm = 0; // Сумма за отсутсвия
                            $NDFLSumm = 0; // Сумма НДФЛ
                            $ILSumm = 0; // Сумма ИЛ
                            $CommersSumm = 0; // Сумма коммерческой премии
                            $AdmSumm = 0; // Сумма административной премии
                            $YderzSumm = 0;  // Сумма удержаний
                            $CorrectSumm = 0; // Сумма коррекции
                            $YearBonusSumm = 0; //Сумма годового бонуса
                            $KPISumm = 0; // Сумма KPI

                            $SummDaysAbsence = 0;
                            $SummDaysHoliday = 0;
                            $SummDaysDisease = 0;

                            $SummMobile = 0;
                            $SummGSM = 0;
                            if (($TypeOfPayment == 1 || $TypeOfPayment == 3) && $ShowExtended == 1) {
                                $SummAkcii = 0;
                                $SummAdmPremia = 0;
                                $SummObuchenie = 0;
                                $SummPrDrOtd = 0;
                                $SummBrak = 0;
                                $SummVoskr = 0;
                                $SummViplaty = 0;
                                $SummPererab = 0;
                                $SummOpozdaniya = 0;
                                $SummInternet = 0;
                                $SummKassoviyUchet = 0;
                                $SummDebetorka = 0;
                                $SummKredit = 0;
                                $SummDrugoe = 0;
                                $SummKorrekBonus = 0;
                                $SummRaschet = 0;
                                $SummGodovoyBonus = 0;
                            }

                            foreach ($PayRollArray as $item) {
                                try {
                                    $sDate = DateTime::createFromFormat("d.m.Y", "01.".$date);
                                    $sDate->modify("-1 month");
                                    $Member_FP=$PI->getFinancialPayout($item->getId(),$sDate->format("m.Y"), $TypeOfPayment);
                                    if (is_null($Member_FP)) {
                                        $OldSalary = NULL;
                                    } else {
                                        $OldSalary = $Member_FP['Salary'];
                                    }
                                }catch (Exception $exception) {
                                    $OldSalary = NULL;
                                }

                                if ($item->getSumm() > 0) {
                                    $BankSumm += roundSumm($item->getSumm()); // Суммируем Итого Банк
                                };

                                $ItogoSumm += $item->getFullSumm(); // Суммируем Итого
                                $ItogoOkrugsumm += roundSumm($item->getSumm());

                                $OkladSumm += $item->getSalary();  // Суммеруем Оклад
                                $OtsutstviyaSumm += $item->getAbsences(); // Суммируем отсутствия
                                $NDFLSumm += $item->getNDFL(); // Суммируем НДФЛ
                                $ILSumm += $item->getRO(); // Суммируем ИЛ
                                $CommersSumm += $item->getCommercial(); // Суммируем коммерческую премию
                                $AdmSumm += $item->getAdministrative(); // Суммируем административную премию
                                $YderzSumm += $item->getHold(); // Суммирую удержания
                                $CorrectSumm += $item->getCorrecting(); // Суммирую корректировки
                                $KPISumm += $item->getKPI();
                                if ($TypeOfPayment == 1 || $TypeOfPayment == 3) {
                                    $AvansSumm += $item->getAdvancePayment();
                                };
                                $YearBonusSumm += $item->getYearbonusPay();

                                if ($ShowExtended == 1) {
                                    $SummDaysAbsence += $item->getAwhdata()["absence"];
                                    $SummDaysHoliday += $item->getAwhdata()["holiday"];
                                    $SummDaysDisease += $item->getAwhdata()["disease"];

                                    if ($TypeOfPayment == 1 || $TypeOfPayment == 3) {
                                        $SummAkcii += $item->getAdministrativeData()["Promotions"];
                                        $SummAdmPremia += $item->getAdministrativeData()["AdministrativePrize"];
                                        $SummObuchenie += $item->getAdministrativeData()["Training"];
                                        $SummPrDrOtd += $item->getAdministrativeData()["PrizeAnotherDepartment"];
                                        $SummBrak += $item->getAdministrativeData()["Defect"];
                                        $SummVoskr += $item->getAdministrativeData()["Sunday"];
                                        $SummViplaty += $item->getAdministrativeData()["NotLessThan"];

                                        $SummPererab += $item->getAdministrativeData()["Overtime"];

                                        $SummOpozdaniya += $item->getRetentionData()["LateWork"];
                                        $SummInternet += $item->getRetentionData()["Internet"];
                                        $SummKassoviyUchet += $item->getRetentionData()["CachAccounting"];
                                        $SummDebetorka += $item->getRetentionData()["Receivables"];
                                        $SummKredit += $item->getRetentionData()["Credit"];
                                        $SummDrugoe += $item->getRetentionData()["Other"];

                                        $SummKorrekBonus += $item->getCorrectionData()["BonusAdjustment"];
                                        $SummRaschet += $item->getCorrectionData()["Calculation"];
                                        $SummGodovoyBonus += $item->getCorrectionData()["YearBonus"];
                                    }
                                }
                                $SummMobile += $item->getMobile();
                                $SummGSM += $item->getTransport();


//                                    $PayRoll->setHold(0); // Удержания
//                                    $PayRoll->setCorrecting(0); // Корректировки

                                // Выдерение цветом. Если сумма меньше 0, то помечаю желтым
                                if ($item->getSumm() < 0) {
                                    echo "<tr class='danger'>";
                                } elseif (is_null($item->getAwhdata()["absence"])) {
                                    echo "<tr class='warning'>";
                                } else {
                                    echo "<tr>";
                                }
                                switch ($TypeOfPayment) { // Вывожу ФИО с ссылкой на персональный расчет
                                    case 1:
                                        echo "<th nowrap>";
                                        echo "<a  href='premium.php?id=" . $item->getId() . "&date=" . $date . "'>" . $item->getFio() . "</a>";
                                        echo "</th>";
                                        break;
                                    case 2:
                                        echo "<th nowrap><a href='salary.php?id=" . $item->getId() . "&date=" . $date . "'>" . $item->getFio() . "</a>";
                                        echo "</th>";
                                        break;
                                    case 3:
                                        echo "<th nowrap>";
                                        echo "<a href='salary2.php?id=" . $item->getId() . "&date=" . $date . "'>" . $item->getFio() . "</a>";
                                        echo "</th>";
                                        break;
                                }

                                echo "<td nowrap style='text-align: left'>" . $item->getDirection()['name'] . " </td>"; // Вывожу Направление
                                echo "<td nowrap style='text-align: left'>" . $item->getDepartment()['name'] . "</td>"; // Вывожу отдел


                                // Вывожу оклад
                                echo "<td style='text-align: right; ".((!is_null($OldSalary) && ($OldSalary!=$item->getSalary())) ? 'background-color: purple; color: white;':'')."' title='".$OldSalary."'>". number_format($item->getSalary(), 2, ',', ' ') . "</td>";
                                // Вывожу выплаченный аванс//аванс
                                echo "<td style='text-align: right;'>" . number_format($item->getAdvancePayment(), 2, ',', ' ') . "</td>";
                                // Вывожу отсутсвия
                                if (($TypeOfPayment == 1 || $TypeOfPayment == 2  || $TypeOfPayment == 3) && $ShowExtended == 1) {
                                    if ($item->getAwhdata()["absence"] > 0)
                                        echo "<td style='background-color: #f6e7cf; text-align: right;'>" . number_format($item->getAwhdata()["absence"], 2, ',', ' ') . "</td>";
                                    else
                                        echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAwhdata()["absence"], 2, ',', ' ') . "</td>";
                                    if ($item->getAwhdata()["holiday"] > 0)
                                        echo "<td style='background-color: #f6e7cf; text-align: right;'>" . number_format($item->getAwhdata()["holiday"], 2, ',', ' ') . "</td>";
                                    else
                                        echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAwhdata()["holiday"], 2, ',', ' ') . "</td>";
                                    if ($item->getAwhdata()["disease"] > 0)
                                        echo "<td style='background-color: #f6e7cf; text-align: right;'>" . number_format($item->getAwhdata()["disease"], 2, ',', ' ') . "</td>";
                                    else
                                        echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAwhdata()["disease"], 2, ',', ' ') . "</td>";
                                }
                                echo "<td style='text-align: right;'>" . number_format($item->getAbsences(), 2, ',', ' ') . "</td>";

                                if ($TypeOfPayment == 1 || $TypeOfPayment == 3) {
                                    // Вывожу НДФЛ
                                    echo "<td style='text-align: right;'>" . number_format($item->getNDFL(), 2, ',', ' ') . "</td>";
                                    // Вывожу ИЛ
                                    echo "<td style='text-align: right;'>" . number_format($item->getRO(), 2, ',', ' ') . "</td>";
                                    // Вывожу коммерческую премию
                                    echo "<td style='text-align: right;'>" . number_format($item->getCommercial(), 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($item->getKPI(), 2, ',', ' ') . "</td>";

                                    // Вывожу административную премию
                                    echo "<td style='text-align: right;'>" . number_format(($item->getAdministrative()), 2, ',', ' ') . "</td>";
                                    if (($TypeOfPayment == 1 || $TypeOfPayment == 3) && $ShowExtended == 1) {

                                        if (isset($item->getAdministrativeData()["Promotions"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["Promotions"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getAdministrativeData()["AdministrativePrize"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["AdministrativePrize"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getAdministrativeData()["Training"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["Training"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getAdministrativeData()["PrizeAnotherDepartment"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["PrizeAnotherDepartment"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getAdministrativeData()["Defect"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["Defect"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getAdministrativeData()["Sunday"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["Sunday"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getAdministrativeData()["NotLessThan"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["NotLessThan"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getAdministrativeData()["Overtime"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getAdministrativeData()["Overtime"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";
                                    }

                                    if (isset($item->getAdministrativeData()["MobileCommunication"])) {
                                        echo "<td style='text-align: right;'>" . number_format($item->getMobile(), 2, ',', ' ') . "</td>";
                                    } else
                                        echo "<td style='text-align: right;'>0,00</td>";

                                    if (isset($item->getAdministrativeData()["CompensationFAL"])) {
                                        echo "<td style='text-align: right;'>" . number_format($item->getTransport(), 2, ',', ' ') . "</td>";
                                    } else
                                        echo "<td style='text-align: right;'>0,00</td>";

                                    // Вывожу удержания
                                    echo "<td style='text-align: right;".($item->getHold()<0?"color:red;font-weight: 700;":"")."'>" . number_format($item->getHold(), 2, ',', ' ') . "</td>";
                                    if (($TypeOfPayment == 1 || $TypeOfPayment == 3) && $ShowExtended == 1) {
                                        if (isset($item->getRetentionData()["LateWork"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getRetentionData()["LateWork"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getRetentionData()["Internet"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getRetentionData()["Internet"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getRetentionData()["CachAccounting"]))
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getRetentionData()["CachAccounting"], 2, ',', ' ') . "</td>";
                                        else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getRetentionData()["Receivables"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getRetentionData()["Receivables"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getRetentionData()["Credit"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getRetentionData()["Credit"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";

                                        if (isset($item->getRetentionData()["Other"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getRetentionData()["Other"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";
                                    }
                                    // ВЫвожу корректировки
                                    echo "<td style='text-align: right;'>" . number_format($item->getCorrecting(), 2, ',', ' ') . "</td>";
                                    if (($TypeOfPayment == 1 || $TypeOfPayment == 3) && $ShowExtended == 1) {
                                        if (isset($item->getCorrectionData()["BonusAdjustment"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getCorrectionData()["BonusAdjustment"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";
                                        if (isset($item->getCorrectionData()["Calculation"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getCorrectionData()["Calculation"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";
                                        if (isset($item->getCorrectionData()["YearBonus"])) {
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>" . number_format($item->getCorrectionData()["YearBonus"], 2, ',', ' ') . "</td>";
                                        } else
                                            echo "<td style='background-color: #f2f2f2; text-align: right;'>0,00</td>";
                                    }

                                    // ВЫвожу годовой бонус
                                    echo "<td style='text-align: right;'>" . number_format($item->getYearbonusPay(), 2, ',', ' ') . "</td>";

                                }
                                // Вывожу сумму
                                if ($item->getFullSumm() > 0) {
                                    echo "<td style='background-color: #eff3fd; text-align: right;'>" . number_format($item->getFullSumm(), 2, ',', ' ') . "</td>";
                                    echo "<td style='background-color: #eff3fd; text-align: right;'>" . number_format(roundSumm($item->getSumm()), 2, ',', ' ') . "</td>";
                                } else {
                                    echo "<td style='background-color: #9f191f; color: #fff; text-align: right;'>" . number_format($item->getFullSumm(), 2, ',', ' ') . "</td>";
                                    echo "<td style='background-color: #9f191f; color: #fff; text-align: right;'>" . number_format(roundSumm($item->getSumm()), 2, ',', ' ') . "</td>";
                                }
                                echo "</tr>";
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <!--  Вывожу все просуммированное выше   -->
                                <th style="font-size: 12px" colspan="3">Итого:</th>
                                <td style='text-align: right;'><?php echo number_format($OkladSumm, 2, ',', ' '); ?></td>
                                <td style='text-align: right;'><?php echo number_format($AvansSumm, 2, ',', ' '); ?></td>
                                <?php
                                if ($ShowExtended == 1) {
                                    echo "<td style='text-align: right;'>" . number_format($SummDaysAbsence, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummDaysHoliday, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummDaysDisease, 2, ',', ' ') . "</td>";
                                }
                                ?>
                                <td style='text-align: right;'><?php echo number_format($OtsutstviyaSumm, 2, ',', ' '); ?></td>
                                <?php
                                if ($TypeOfPayment == 1 || $TypeOfPayment == 3) {
                                    ?>
                                    <td style='text-align: right;'><?php echo number_format($NDFLSumm, 2, ',', ' '); ?></td>
                                    <td style='text-align: right;'><?php echo number_format($ILSumm, 2, ',', ' '); ?></td>
                                    <td style='text-align: right;'><?php echo number_format($CommersSumm, 2, ',', ' '); ?></td>
                                    <td style='text-align: right;'><?php echo number_format($KPISumm, 2, ',', ' '); ?></td>
                                    <td style='text-align: right;'><?php echo number_format(($AdmSumm), 2, ',', ' '); ?></td>

                                    <?php
                                    if (($TypeOfPayment == 1 || $TypeOfPayment == 3) && $ShowExtended == 1) {
                                        echo "<td style='text-align: right;'>" . number_format($SummAkcii, 2, ',', ' ') . "</td>";
                                        echo "<td style='text-align: right;'>" . number_format($SummAdmPremia, 2, ',', ' ') . "</td>";
                                        echo "<td style='text-align: right;'>" . number_format($SummObuchenie, 2, ',', ' ') . "</td>";
                                        echo "<td style='text-align: right;'>" . number_format($SummPrDrOtd, 2, ',', ' ') . "</td>";
                                        echo "<td style='text-align: right;'>" . number_format($SummBrak, 2, ',', ' ') . "</td>";
                                        echo "<td style='text-align: right;'>" . number_format($SummVoskr, 2, ',', ' ') . "</td>";
                                        echo "<td style='text-align: right;'>" . number_format($SummViplaty, 2, ',', ' ') . "</td>";

                                        echo "<td style='text-align: right;'>" . number_format($SummPererab, 2, ',', ' ') . "</td>";

                                    }
                                    echo "<td style='text-align: right;'>" . number_format($SummMobile, 2, ',', ' ') . "</td>";
                                    echo "<td style='text-align: right;'>" . number_format($SummGSM, 2, ',', ' ') . "</td>";
                                    ?>

                                    <td style='text-align: right;'><?php echo number_format($YderzSumm, 2, ',', ' '); ?></td>

                                    <?php
                                    if (($TypeOfPayment == 1 || $TypeOfPayment == 3) && $ShowExtended == 1) {
                                        echo "<td style='text-align: right;'>" . number_format($SummOpozdaniya, 2, ',', ' ') . "</td>";
                                        echo "<td style='text-align: right;'>" . number_format($SummInternet, 2, ',', ' ') . "</td>";
                                        echo "<td style='text-align: right;'>" . number_format($SummKassoviyUchet, 2, ',', ' ') . "</td>";
                                        echo "<td style='text-align: right;'>" . number_format($SummDebetorka, 2, ',', ' ') . "</td>";
                                        echo "<td style='text-align: right;'>" . number_format($SummKredit, 2, ',', ' ') . "</td>";
                                        echo "<td style='text-align: right;'>" . number_format($SummDrugoe, 2, ',', ' ') . "</td>";
                                    }
                                    ?>

                                    <td style='text-align: right;'><?php echo number_format($CorrectSumm, 2, ',', ' '); ?></td>
                                    <?php
                                    if (($TypeOfPayment == 1 || $TypeOfPayment == 3) && $ShowExtended == 1) {
                                        echo "<td style='text-align: right;'>" . number_format($SummKorrekBonus, 2, ',', ' ') . "</td>";
                                        echo "<td style='text-align: right;'>" . number_format($SummRaschet, 2, ',', ' ') . "</td>";
                                        echo "<td style='text-align: right;'>" . number_format($SummGodovoyBonus, 2, ',', ' ') . "</td>";
                                    }
                                    ?>
                                    <td style='text-align: right;'><?php echo number_format($YearBonusSumm, 2, ',', ' '); ?></td>
                                    <?php
                                }
                                ?>
                                <td style='text-align: right;'><?php echo number_format($ItogoSumm, 2, ',', ' '); ?></td>
                                <td style='text-align: right;'><?php echo number_format($ItogoOkrugsumm, 2, ',', ' '); ?></td>
                            </tr>
                            </tfoot>
                        </table>

                        <div class="box box-solid box-info">
                            <div class="box-body">
                                <div class="col-md-3 text-left"><b>Итого для заказа: <b></b></div>
                                <div class="col-md-9 text-right"><b></b><?php echo number_format($BankSumm, 2, ',', ' '); ?></b> <span class="fa fa-ruble" style="font-size:12px;"></span></div>
                            </div>
                        </div>


                    </div>
                    <div class="box-footer">
                        <?php
                        switch ($ReportStatus['status_id']) {
                            case 1:
                                /*
                                 * Переменная для разрешения отображения кнопки следующей стадии
                                 */
                                $NextStage = false;
                                /*
                                 * Переменная для разрешения отображения кнопки предыдущей стадии
                                 */
                                $PrevStage = false;

                                foreach ($Roles as $Role) {
                                    if ($Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6) {
                                        $PrevStage = true;

                                    }
                                }


                                if ($ShowButtonNextStage || $Role->getId() == 5 || $Role->getId() == 6) {
                                    echo "<button class=\"btn btn-flat btn-success\" onclick=\"if (confirm('Провести на стадию 2?')) {  $('#modal-save').modal('show');} else { return false;};\" name=\"NextStatus\">На проверку отв. директору</button>";
                                }

                                if ($PrevStage) {
                                    echo "<a href='#' data-toggle=\"modal\" data-target=\"#modal-window\" class=\"btn btn-flat btn-danger\" style='margin: 5px;' name=\"BackStatus\">На предыдущую стадию</a>";
                                }

                                break;
                            case 2:
                                /*
                                 * Переменная для разрешения отображения кнопки следующей стадии
                                 */
                                $NextStage = false;
                                /*
                                 * Переменная для разрешения отображения кнопки предыдущей стадии
                                 */
                                $PrevStage = false;
                                /*
                                 * Пробегаюсь по ролям и проверяю ID
                                 * Если это отвественный директор или директор или Root, то даю доступ на кнопку сл. стадии
                                 * Если директор и Root, то показываю кнопку предыдущей стадии
                                 */
                                foreach ($Roles as $Role) {
                                    if ($Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6) {
                                        $NextStage = true;
                                        $PrevStage = true;

                                    }
                                }

                                if ($NextStage) {
                                    /*
                                    * Если кнопка не была нажата, то отображаем ее
                                    */
                                    if ($ShowButtonNextStage || $Role->getId() == 5 || $Role->getId() == 6) {
                                        echo "<button onclick=\"if (confirm('Провести на стадию 3?')) {  $('#modal-save').modal('show');} else { return false;};\" class=\"btn btn-flat btn-success\" style='margin: 5px;' name=\"NextStatus\">На утверждение</button>";
                                    }
                                }

                                if ($PrevStage) {
                                    echo "<a href='#' data-toggle=\"modal\" data-target=\"#modal-window\" class=\"btn btn-flat btn-danger\" style='margin: 5px;' name=\"BackStatus\">На предыдущую стадию</a>";
                                }
                                break;
                            case 3:

                                /*
                                 * Переменная для разрешения отображения кнопкок
                                 */
                                $NextPrevStage = false;

                                $Email = false;

                                /*
                                 * Пробегаюсь по ролям и проверяю ID
                                 * Если это отвественный директор или директор или Root, то даю доступ на кнопку сл. стадии
                                 * Если директор и Root, то показываю кнопку предыдущей стадии
                                 */
                                foreach ($Roles as $Role) {


                                    if ($Role->getId() == 5 || $Role->getId() == 6) {
                                        $NextPrevStage = true;
                                    }

                                    if ($Role->getId() == 5 || $Role->getId() == 6 || $Role->getId()==2 || $Role->getId()==4) {
                                        $Email = true;
                                    }
                                }

//                                if ($Email) {
//                                    echo "<a href='#' class=\"btn btn-flat btn-primary\" style='margin: 5px;' data-toggle=\"modal\" data-target=\"#modal-email\">Отправить сотрудникам на почту</a>";
//                                }

                                if ($NextPrevStage) {
                                    echo "<a href='#' data-toggle=\"modal\" data-target=\"#modal-window\" class=\"btn btn-flat btn-danger\" style='margin: 5px;' name=\"BackStatus\">На предыдущую стадию</a>";
                                    echo "<button class=\"btn btn-flat btn-primary\"  style='margin: 5px;' name=\"Envelope\">Конверты</button>";

//                                    if (isset($YearBonusStatus) && ($YearBonusStatus == true)) {
                                        echo "<a class=\"btn btn-flat btn-primary\" href='envelope_year.php?date=" . $date . "'  style='margin: 5px;' name=\"YearBonus\">Годовой бонус</a>";
//                                    }
                                }
                                break;
                            case 4:
                                /*
                                 * Переменная для разрешения отображения кнопкок
                                 */
                                $NextPrevStage = false;

                                /*
                                 * Показывать кнопку конвертов?
                                 */
                                $EnvelopeShow = false;

                                /*
                                 * Пробегаюсь по ролям и проверяю ID
                                 * Если это отвественный директор или директор или Root, то даю доступ на кнопку сл. стадии
                                 * Если директор и Root, то показываю кнопку предыдущей стадии
                                 */
                                foreach ($Roles as $Role) {
                                    if ($Role->getId() == 5 || $Role->getId() == 6) {
                                        $NextPrevStage = true;

                                    }

                                    if ($Role->getId() == 5 || $Role->getId() == 6 || $Role->getId()==2 || $Role->getId()==4) {
                                        $EnvelopeShow = true;
                                    }
                                }

                                if ($EnvelopeShow) {
                                    echo "<button class=\"btn btn-flat btn-primary\"  style='margin: 5px;' name=\"Envelope\">Конверты</button>";
                                }

                                if ($NextPrevStage) {
                                    echo "<button onclick=\"if (confirm('Вернуть на предыдущую стадию')) { $('#modal-save').modal('show');} else { return false; };\" class=\"btn btn-flat btn-success\" style='margin: 5px;' name=\"BackStatus\">Вернуть на предыдущую стадию</button>";

//                                    if (isset($YearBonusStatus) && ($YearBonusStatus == true)) {
                                        echo "<a class=\"btn btn-flat btn-primary\" href='envelope_year.php?date=" . $date . "'  style='margin: 5px;' name=\"YearBonus\">Годовой бонус</a>";
//                                    }
                                }
                                break;
                            case 5:
                                /*
                                 * Переменная для разрешения отображения кнопкок
                                 */
                                $NextPrevStage = false;

                                /*
                                 * Показывать кнопку конвертов?
                                 */
                                $EnvelopeShow = false;

                                /*
                                 * Пробегаюсь по ролям и проверяю ID
                                 * Если это отвественный директор или директор или Root, то даю доступ на кнопку сл. стадии
                                 * Если директор и Root, то показываю кнопку предыдущей стадии
                                 */
                                foreach ($Roles as $Role) {
                                    if ($Role->getId() == 5 || $Role->getId() == 6) {
                                        $NextPrevStage = true;

                                    }

                                    if ($Role->getId() == 5 || $Role->getId() == 6 || $Role->getId()==2 || $Role->getId()==4) {
                                        $EnvelopeShow = true;

                                    }
                                }

                                if ($EnvelopeShow) {
                                    echo "<button class=\"btn btn-flat btn-primary\" style='margin: 5px;' name=\"Envelope\">Конверты</button>";
//                                    echo "<a href='#' class=\"btn btn-flat btn-primary\" style='margin: 5px;' data-toggle=\"modal\" data-target=\"#modal-email\">Отправить сотрудникам на почту</a>";
                                }

                                if ($NextPrevStage) {
                                    echo "<button onclick=\"if (confirm('Вернуть на предыдущую стадию')) { $('#modal-save').modal('show');} else { return false; };\" class=\"btn btn-flat btn-danger\" style='margin: 5px;' name=\"BackStatus\">На предыдущую стадию</button>";
                                    echo "<button onclick=\"if (confirm('Провести на стадию 6?')) {  $('#modal-save').modal('show');} else { return false;};\" class=\"btn btn-flat btn-success\" style='margin: 5px;' name=\"NextStatus\">В расходы</button>";

//                                    if (isset($YearBonusStatus) && ($YearBonusStatus == true)) {
                                        echo "<a class=\"btn btn-flat btn-primary\" href='envelope_year.php?date=" . $date . "'  style='margin: 5px;' name=\"YearBonus\">Годовой бонус</a>";
//                                    }
                                }
                                break;
                            case 6:
                                /*
                                 * Переменная для разрешения отображения кнопкок
                                 */
                                $NextPrevStage = false;

                                /*
                                 * Показывать кнопку конвертов?
                                 */
                                $EnvelopeShow = false;

                                /*
                                 * Пробегаюсь по ролям и проверяю ID
                                 * Если это отвественный директор или директор или Root, то даю доступ на кнопку сл. стадии
                                 * Если директор и Root, то показываю кнопку предыдущей стадии
                                 */
                                foreach ($Roles as $Role) {
                                    if ($Role->getId() == 5 || $Role->getId() == 6) {
                                        $NextPrevStage = true;

                                    }

                                    if ($Role->getId() == 5 || $Role->getId() == 6 || $Role->getId()==2 || $Role->getId()==4) {
                                        $EnvelopeShow = true;

                                    }
                                }

                                if ($EnvelopeShow) {
                                    echo "<button class=\"btn btn-flat btn-primary\"  style='margin: 5px;' name=\"Envelope\">Конверты</button>";

                                }


                                if ($NextPrevStage) {
                                    echo "<a href=\"departmentalreport.php?date=" . $date . "&TypeOfPayment=" . $TypeOfPayment . "\" class='btn btn-flat btn-success'  style='margin: 5px;' >Детальные расходы</a>";
                                    if ($RS->getStatus($date, 6)['status_id'] == 6) {
                                        echo "<a class=\"btn btn-flat btn-primary\" href='envelope_year.php?date=" . $date . "'  style='margin: 5px;' name=\"YearBonus\">Годовой бонус</a>";
                                    }
                                }

                                break;
                            default:
//                                echo "<button onclick=\"if (confirm('Провести ведомость на следующую стадию?')) {  $('#modal-save').modal('show');} else { return false;};\" class=\"btn btn-flat btn-success\" name=\"NextStatus\">На следующую стадию</button>";
                                break;
                        }

                        echo "<button class='btn btn-flat btn-primary' name='Excel'>Сохранить в xls</button>";
                        ?>

                    </div>

                </div>
                </form>


                <div class="modal fade" id="modal-window">
                    <div class="modal-dialog" style="width: 90%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title">Изменение стадии</h3>
                            </div>

                            <div class="modal-body">

                                <div class="box box-solid box-info">
                                    <div class="box-body">
                                        <form method="post">
                                            <input type="hidden" name="date" value="<?php echo $date; ?>">
                                            <input type="hidden" name="TypeOfPayment" value="<?php echo $TypeOfPayment; ?>">
                                            <select multiple name="DepartmentFilter[]" style="visibility: hidden">
                                                <?php
                                                foreach ($Global_Department_Filter as $item) {
                                                    echo "<option selected value='".$item."'>".$item."</option>";
                                                }
                                                ?>
                                            </select>

                                            <div class="form-group">
                                                <?php
                                                if ($ReportStatus['status_id'] == 2) {
                                                    echo "<h4>Руководители отделов</h4>";
                                                } else if ($ReportStatus['status_id'] == 3) {
                                                    echo "<h4>Отвественные директора офисов</h4>";
                                                }
                                                ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input name="check_all" id="check_all" checked type="checkbox">
                                                        Все отделы
                                                    </label>
                                                </div>
                                            </div>

                                            <table class="table-striped table-condensed">
                                                <thead>
                                                <th class='col-md-1'></th>
                                                <th class='col-md-2'>Статус</th>
                                                <th class='col-md-2'>Отдел</th>
                                                <th class='col-md-1'>Ответственный директор</th>
                                                <th class='col-md-1'>Дата изменения последнего статуса</th>
                                                <th class='col-md-5'>Кто провел на последний статус</th>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if ($WhoBrake) {
                                                    foreach ($WhoBrake as $key => $w) {
                                                        if (isset($Global_Department_Filter) && (!in_array($w['id'], $Global_Department_Filter)))
                                                            continue;

                                                        switch ($w['status_id']) {
                                                            case 1:
                                                                $bgcolor = '#E3E2B0';
                                                                break;
                                                            case 2:
                                                                $bgcolor = '#00CCAA';
                                                                break;
                                                            case 3:
                                                                $bgcolor = '#0088cc';
                                                                break;
                                                            case 4:
                                                                $bgcolor = '#0022cc';
                                                                break;
                                                            case 5:
                                                                $bgcolor = '#4400cc';
                                                                break;
                                                            case 6:
                                                                $bgcolor = '#AA00CC';
                                                                break;
                                                        }

                                                        echo "<tr>";
                                                        echo "<td>";

                                                        echo '<label>';
                                                        if ($w['status_id'] > 1 && $w['status_id'] < 6) {
                                                            echo '<input id="custom_check" name="dep[' . $w['id'] . ']" value="'.$w['id'].'" type="checkbox" disabled>';
                                                        }
                                                        echo '</label>';

                                                        echo "</td>";
                                                        echo "<td ><span class=\"info-box-icon\" style=\"background-color: " . $bgcolor . "; height: 25px; width: 25px;line-height: 25px;font-size:13px;color: white\">" . $w['status_id'] . "</span><span style='padding-left:5px;'>" . $w['status_name'] . "</span></td>";
                                                        echo "<td><b>" . $w['name'] . "</b></td>";
                                                        echo "<td>" . $w['director_name'] . "</td>";
                                                        echo "<td>" . $w['change_date'] . "</td>";
                                                        if (is_null($w['change_member_name'])) {
                                                            if ($w['change_member'] == -1)
                                                                echo "<td >Администратор</td>";

                                                            if ($w['change_member'] == -2)
                                                                echo "<td >System</td>";
                                                        } else {
                                                            echo "<td>" . $w['change_member_name'] . "</td>";
                                                        }
                                                        echo "</tr>";
                                                    }
                                                }
                                                ?>
                                                </tbody>
                                            </table>

                                            <button class="btn btn-flat btn-sm btn-success" onclick="if (confirm('Вернуть на предыдущую стадию')) { $('#modal-save').modal('show');} else { return false; };" name="PrevStatusCustom">На предыдущую стадию</button>
                                        </form>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="modal fade" id="modal-email">
                    <div class="modal-dialog" style="width: 90%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title">Отправка персональной ведомости сотрудникам на почту</h3>
                            </div>

<!--                            <div class="modal-body">-->
<!--                                <div class="box box-solid box-info">-->
<!--                                    <div class="box-body">-->
<!--                                        <table class="table-striped table-condensed">-->
<!--                                            <thead class="text-center">-->
<!--                                            <th class="col-md-2">ФИО</th>-->
<!--                                            <th class="col-md-2">Направление</th>-->
<!--                                            <th class="col-md-2">Отдел</th>-->
<!--                                            <th class="col-md-1">EMail</th>-->
<!--                                            <th class="col-md-2">Сумма</th>-->
<!--                                            </thead>-->
<!--                                            <tbody>-->
<!--                                        --><?php
//                                        foreach ($PayRollArray as $item) {
//                                            if ($item->getEmail()==null) {
//                                                echo "<tr class='text-muted'>";
//                                            } else {
//                                                echo "<tr>";
//                                            }
//                                            echo "<td>".$item->getFio()."</td>";
//                                            echo "<td>".$item->getDirection()['name']."</td>";
//                                            echo "<td>".$item->getDepartment()['name']."</td>";
//                                            echo "<td>" . $item->getEmail() . "</td>";
//                                            echo "<td>".  number_format($item->getSumm(), 2, ',', ' ') ." &#8381; </td>";
//                                            echo "</tr>";
//                                        }
//                                        ?>
<!--                                        </table>-->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!--                            </div>-->

                            <div class="modal-footer">
                                <form method="post">
                                    <button class="btn btn-flat btn-sm btn-success" name="Email">Отправить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="modal-whobrake">
                    <div class="modal-dialog" style="width: 90%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title">Текущие стадии по отделам</h3>
                            </div>

                            <div class="modal-body">
                                <div class="box box-solid box-info">
                                    <div class="box-body">
                                        <table class="table-striped table-condensed">
                                            <thead>
                                            <th>Статус</th>
                                            <th>Отдел</th>
                                            <th>Ответственный директор</th>
                                            <th>Дата изменения последнего статуса</th>
                                            <th>Кто провел на последний статус</th>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if ($WhoBrake) {
                                                foreach ($WhoBrake as $key => $w) {
                                                    switch ($w['status_id']) {
                                                        case 1:
                                                            $bgcolor = '#88CC66';
                                                            break;
                                                        case 2:
                                                            $bgcolor = '#00CCAA';
                                                            break;
                                                        case 3:
                                                            $bgcolor = '#0088cc';
                                                            break;
                                                        case 4:
                                                            $bgcolor = '#0022cc';
                                                            break;
                                                        case 5:
                                                            $bgcolor = '#4400cc';
                                                            break;
                                                        case 6:
                                                            $bgcolor = '#AA00CC';
                                                            break;
                                                    }

                                                    echo "<p style='background-color: #00cc22;'></p>";
                                                    echo "<tr>";
                                                    echo "<td class='col-md-2'><span class=\"info-box-icon\" style=\"background-color: " . $bgcolor . "; height: 25px; width: 25px;line-height: 25px;font-size:13px;color: white\">" . $w['status_id'] . "</span><span style='padding-left:5px;'>" . $w['status_name'] . "</span></td>";
                                                    echo "<td class='col-md-2'><b>" . $w['name'] . "(" . $w['id'] . ")" . "</b></td>";
                                                    echo "<td class='col-md-2'>" . $w['director_name'] . "</td>";
                                                    echo "<td class='col-md-1'>" . $w['change_date'] . "</td>";
                                                    if (is_null($w['change_member_name'])) {
                                                        if ($w['change_member'] == -1)
                                                            echo "<td >Администратор</td>";

                                                        if ($w['change_member'] == -2)
                                                            echo "<td >System</td>";
                                                    } else {
                                                        echo "<td class='col-md-1'>" . $w['change_member_name'] . "</td>";
                                                    }
                                                    echo "</tr>";
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <?php
                if (array_filter($Roles, function ($Role) {
                    return $Role->getId() == 5 || $Role->getId() == 6;
                })) {
                    ?>
                    <div class="modal fade" id="modal-info" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title">Начислители ЗП</h4>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>ФИО</th>
                                            <th>Отдел</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($RI->getRukList() as $item) {
                                            echo "<tr>";
                                            echo "<td>" . $item['lastname'] . " " . $item['name'] . " " . $item['middle'] . " " . $item['maiden_name'] . "</td>";
                                            echo "<td>" . $item['department_name'] . "</td>";
                                            echo "</tr>";
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-outline">Save changes</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <?php
                }
                ?>
                <?php
            }
            ?>
            <div class="modal fade" id="modal-save" style="background: rgba(0,0,0,1);">
                <div class="modal-dialog" style="width: 95%">
                    <div class="modal-content">
                        <div class="modal-header text-center" style="color: white;background-color: #000;border: unset;">
                            <h3 class="modal-title" style="color: white;background-color: #000;border: unset;">Сохранение данных</h3>
                        </div>

                        <div class="modal-body" style="background-color: #000;">
                            <div class="box box-solid box-info" style="height: 70%;background-color: #000;border: unset;">
                                <div class="box-body">
                                    <img class="img-responsive center-block" src="images/7TwN.gif">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php
    require_once 'footer.php';
    ?>
</div>

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<script src="../../bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- DataTables -->
<!--<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>-->
<!--<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>-->

<!--<script src="../../bower_components/FixedHeader-3.1.4/js/dataTables.fixedHeader.js"></script>-->


<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>

    $(function () {
        $('.select2').select2()
    })

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(function () {
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            format: 'mm.yyyy'
        })
    });

    $(document).ready(function () {
        $('#check_all').change(function () {
            var val = $(this).prop("checked");

            if (val == true) {
                $("input[id=custom_check]").each(function () {
                    $(this).prop("checked", false);
                    $(this).prop("disabled", "disabled");
                })
            } else {
                $("input[id=custom_check]").each(function () {
                    $(this).prop("disabled", false);
                })
            }
        })
    });

    $(document).ready(function () {
        $('select[name=DepartmentFilter] option').hide();
        var val = $('select[name=DirectionFilter]').val();
        if (val == 0) {
            $('select[name=DepartmentFilter] option').show();
        } else {
            $('select[name=DepartmentFilter] option.0').show();
            $('select[name=DepartmentFilter] option.' + $('select[name=DirectionFilter]').val()).show();
        }

        if ($('input[name=DirectionNot]').prop("checked")) {
            $('select[name=DepartmentFilter]').prop("disabled", true);
        }

        if ($('select[name=TypeOfPayment]').val() == 1 || $('select[name=TypeOfPayment]').val() == 2  || $('select[name=TypeOfPayment]').val() == 3) {
            $('label[name=ShowExtendedLabel]').show();
        } else {
            $('label[name=ShowExtendedLabel]').hide();
        }

    });

    $('input[name=DirectionNot]').change(function () {
        if ($(this).prop("checked")) {
            $('select[name=DepartmentFilter]').prop("disabled", true);
        } else {
            $('select[name=DepartmentFilter]').prop("disabled", false);
        }
    })

    $('select[name=DirectionFilter]').change(function () {
        $('select[name=DepartmentFilter] option[value="-1"]').prop('selected', true);

        $('select[name=DepartmentFilter] option').hide();
        $("select[name=DepartmentFilter] option:selected").prop("selected", false)

        var val = $('select[name=DirectionFilter]').val();
        if (val == 0) {
            $('select[name=DepartmentFilter] option').show();
        } else {
            $('select[name=DepartmentFilter] option.0').show();
            $('select[name=DepartmentFilter] option.' + $('select[name=DirectionFilter]').val()).show();
        }
    });

    $('select[name=TypeOfPayment]').change(function () {
        if ($('select[name=TypeOfPayment]').val() == 1 || $('select[name=TypeOfPayment]').val() == 2) {
            $('label[name=ShowExtendedLabel]').show();
        } else {
            $('label[name=ShowExtendedLabel]').hide();
        }
    })

    // $(document).ready(function () {
    //     $('#PayrollTable').DataTable({
    //         fixedHeader: true,
    //         paging: false,
    //         lengthChange: false,
    //         searching: true,
    //         ordering: true,
    //         info: false,
    //         autoWidth: false
    //     })
    // });
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
