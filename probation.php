<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 22.08.18
 * Time: 18:10
 */

// Время работы скрипта
$start = microtime(true);

require_once 'app/DismissalInterface.php';
require_once 'app/ErrorInterface.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

require_once 'app/functions.php';
require_once 'app/DepartmentInterface.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

// Настройки
if (!array_filter($Roles, function ($Role) {
    return ($Role->getReportLeaving() == 1);
})) {
    header("Location: 404.php" );
}

$Errors = new ErrorInterface();
$DI = new DepartmentInterface();

$DI->fetchDepartments();
$DepartmentList = $DI->GetDepartments();

if (isset($_POST['department']) && $_POST['department']!=-1) {
    $dp = intval($_POST['department']);
};

$dbh = dbConnect();
if (isset($dp)) {
    $query = "select t1.id, t1.lastname,t1.name,t1.middle,DATE_FORMAT(t1.employment_date, \"%Y-%m-%d\") as employment_date,t3.name as direction, t4.name as department,t5.name as position, DATE_FORMAT(t1.employment_date, '%Y-%m') as gd
from member as t1 
LEFT JOIN member_jobpost as t2 ON (t1.id = t2.member_id and t2.active=1)
LEFT JOIN direction as t3 on (t2.direction_id = t3.id)
LEFT JOIN department as t4 on (t2.department_id = t4.id)
LEFT JOIN position as t5 on (t2.position_id = t5.id)
where t1.employment_date>DATE_SUB(NOW(), INTERVAL 12 MONTH) and t1.employment_date<DATE_SUB(NOW(), INTERVAL 3 MONTH) and t1.dismissal_date='9999-01-01' and t2.department_id = ?; 
order by gd, t1.lastname,t1.name,t1.middle";
} else {
    $query = "select t1.id, t1.lastname,t1.name,t1.middle,DATE_FORMAT(t1.employment_date, \"%Y-%m-%d\") as employment_date,t3.name as direction, t4.name as department,t5.name as position, DATE_FORMAT(t1.employment_date, '%Y-%m') as gd
from member as t1 
LEFT JOIN member_jobpost as t2 ON (t1.id = t2.member_id and t2.active=1)
LEFT JOIN direction as t3 on (t2.direction_id = t3.id)
LEFT JOIN department as t4 on (t2.department_id = t4.id)
LEFT JOIN position as t5 on (t2.position_id = t5.id)
where t1.employment_date>DATE_SUB(NOW(), INTERVAL 12 MONTH) and t1.employment_date<DATE_SUB(NOW(), INTERVAL 3 MONTH) and t1.dismissal_date='9999-01-01' 
order by gd, t1.lastname,t1.name,t1.middle";
}

$sth = $dbh->prepare($query);
if (isset($dp)) {
    $sth->execute([$dp]);
} else {
    $sth->execute();
}

$raw = $sth->fetchAll();
$menu_open = 3;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Принятые сотрудники за последние 90 дней</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">

    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">

<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">


        <!-- Main content -->
        <section class="content">

            <?php
            /*
                 * Вывод информации об ощибках
                 */
            if ($Errors->getCount() > 0) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="Table3">
                            <thead>
                            <th>Причина</th>
                            <th>Сообщение</th>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($Errors->getErrors() as $value) {
                                echo "<tr><td>" . $value['0'] . "</td><td>" . $value[1] . "</td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php
            }
            ?>
            <form method="post">
                <div class="box box-default box-solid">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Выбор отдела</label>
                            <select class="form-control" name="department">
                                <?php
                                echo "<option value='-1'>Все отделы</option>";
                                foreach ($DepartmentList as $d) {
                                    if ((isset($dp)) && ($d->getId() == $dp)) {
                                        echo "<option selected value=".$d->getId().">".$d->getName()."</option>";
                                    } else {
                                        echo "<option value=".$d->getId().">".$d->getName()."</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="col-sm-12"><input type="submit" class="btn btn-flat btn-primary" name="PushButton_1" value="Получить данные"></div>
                    </div>
                </div>
            </form>

            <div class="box box-default box-solid">
                <div class="box-body">
                    <h4>Прошли испытательный срок</h4>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <th>Конец ИС</th>
                        <th>ФИО</th>
                        <th>Дата приема</th>
                        <th>Окончание ИС</th>
                        <th>Направление</th>
                        <th>Отдел</th>
                        <th>Должность</th>
                        </thead>
                        <tbody>
                        <?php
                        if (isset($raw)) {
                            $date_m = array('январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь');
                            foreach ($raw as $value) {
                                $end = DateTime::createFromFormat("Y-m-d", $value['employment_date']);
                                $end->modify("+3 month");
                                if (($old != $end->format("Y-m")))
                                    echo "<tr><th colspan='7' class='text-center' style='font-size: 14px;'>".$date_m[(int)$end->format("m")-1]."</th></tr>";
                                echo '<tr>';
//                                echo "<td>".$value['gd']."</td>";
                                echo "<td>".$date_m[(int)$end->format("m")-1]. "</td>";
                                echo "<th><a href='profile.php?id=" . $value['id'] . "'>" . $value['lastname'] . " " . $value['name'] . " " . $value['middle'] . "</a></th>";
                                echo "<td>" . $value['employment_date'] . "</td>";
                                echo "<td>" . $end->format("Y-m-d") . "</td>";
                                echo "<td>" . $value['direction'] . "</td>";
                                echo "<td>" . $value['department'] . "</td>";
                                echo "<td>" . $value['position'] . "</td>";
                                echo '</tr>';

                                $old = $end->format("Y-m");
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <?php require_once 'footer.php'; ?>
    <!-- /.content-wrapper -->
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>

<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<script>
    $(function () {

        $('#Table3').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })

    })
</script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
