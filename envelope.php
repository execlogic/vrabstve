<?php

// Время работы скрипта
$start = microtime(true);

require_once 'app/PayRoll.php';
require_once 'app/PayRollInterface.php';
require_once 'app/functions.php';
require_once 'app/ReportStatus.php';
require_once 'app/Envelopes.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once "app/EnvelopeInterface.php";

require_once 'app/Notify.php';

function roundSumm($summ)
{
    // Округление до 10
    if ($summ > 0) { //Если сумма <= 0 то не обрабатываем данные
//        $summ = round($summ); // округляю сотые и десятые
        $summ = (int)$summ;

        $ostatok = ($summ % 10); // Получаю остаток от деления на 10
        if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
            $summ = $summ - $ostatok + 10;
        } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
            $summ = $summ - $ostatok;
        };
    } else {
        $summ = 0;
    }
    return $summ;
}


if (!isset($_SESSION)) {
    session_start();
    $User = $_SESSION['UserObj'];
}

if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

$Envelope_Update = 0;
$is_Leader = 0;
$is_Director = 0;

if (!array_filter($Roles, function ($Role) use (&$Envelope_Update, &$is_Leader, &$is_Director) {
    if ($Role->getId() == 5 || $Role->getId() == 6) {
        $Envelope_Update = 1;
    }

    if(($Role->getId() == 2 || $Role->getId() == 4) && ($is_Director==0)) {
        $is_Leader = 1;
    }

    if ($Role->getId() == 5 || $Role->getId() == 6) {
        $is_Director = 1;
        $is_Leader = 0;
    }

    return ($Role->getId() == 2 || $Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6);
})) {
    header("Location: 404.php");
}

/*
 * Если администратор, то ему можно )
 */
if ($User->getMemberId() == -1 ) {
    $Envelope_Update = 1;
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

$EI = new EnvelopeInterface();

if (!isset($_SESSION)) {
    session_name('Envelope');
    session_start();
}

$rawData = $_SESSION['Envelope']['Data'];

if ($_SESSION['Envelope']['filter']) {
    $EI->filter = true;
}

if ($is_Leader == 0) {
    $EI->setRawData($rawData); // Подучаю данные из сессии
}

$EI->setDateType($_SESSION['Envelope']['date'], $_SESSION['Envelope']['TypeOfPayment']);
session_commit();

$RS = new ReportStatus(); // Статус ведомости.
$ReportStatus = $RS->getStatus($EI->getDate(), $EI->getTypeOfPayment());

if (isset($_POST['MAX_ENV_SUMM'])) {
    $EI->setMaxEnvSumm($_POST['MAX_ENV_SUMM']);
}

if (isset($_POST['MIN_ENV_SUMM'])) {
    $EI->setMinEnvSumm($_POST['MIN_ENV_SUMM']);
}

if (isset($_POST['MAX_ENV_NUMSIZE'])) {
    $EI->setMaxEnvNumSize($_POST['MAX_ENV_NUMSIZE']);
}

$EI->fetch();
if ($is_Leader == 1) {
    $EI->CatcherCalc();
} else {
    $RTN_VALUE = $EI->Calculation();
}

if (isset($_POST['Confirm']) && ($is_Leader == 0)) {
    $EI->Confirm();
    if ($ReportStatus['status_id'] == 3) {
        $RS->ChangeStatus($EI->getDate(), $EI->getTypeOfPayment(), (++$ReportStatus['status_id']), $User->getMemberId());
    }
    header("Location: prepaid_expense.php?date=" . $EI->getDate() . "&TypeOfPayment=" . $EI->getTypeOfPayment());
}

if (isset($_POST['Update'])) {
    $EI->Confirm();
    header("Location: prepaid_expense.php?date=" . $EI->getDate() . "&TypeOfPayment=" . $EI->getTypeOfPayment());
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Конверты</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
    <style>
        td > a {
            font-weight: bold;
        }
    </style>
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
                <?php
                if ($EI->getTypeOfPayment() !== null) {
                    switch ($EI->getTypeOfPayment()) {
                        case 1:
                            echo "Конверты премии " . $EI->getDate();
                            break;
                        case 2:
                            echo "Конверты аванса " . $EI->getDate();
                            break;
                        case 3:
                            echo "Конверты аванса2 " . $EI->getDate();
                            break;
                        case 4:
                            echo "Конверты \"расчета\" " . $EI->getDate();
                            break;
                    }
                } else {
                    echo "Не выбран тип выплаты";
                }
                ?>
            </h4>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Список пользователей</a></li>
                <li><a href="prepaid_expense.php?date=<?php echo $EI->getDate(); ?>&TypeOfPayment=<?php echo $EI->getTypeOfPayment(); ?>">Расчет</a></li>
                <li class="active">Конверты</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">

            <?php
            if ($EI->countSqlData() == 0) {
                ?>

                <div class="box box-default box-solid">
                    <form method="post" class="form-horizontal">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Максимальная сумма в конверте</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="MAX_ENV_SUMM" value="<?php echo $EI->getMaxEnvSumm(); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Минимальная сумма в конверте</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="MIN_ENV_SUMM" value="<?php echo $EI->getMinEnvSumm() ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Количество конвертов</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="MAX_ENV_NUMSIZE" value="<?php echo $EI->getMaxEnvNumSize(); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary btn-flat pull-left" value="Применить" name="Ok">
                        </div>
                    </form>
                </div>
                <?php
            }
            ?>

            <div class="box box-info box-solid">
                <div class="box-body" style="overflow-y: auto">
                    <div id="print_table">
                        <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th id='fio'>#</th>
                            <th id='fio'>ФИО</th>
                            <th id='fio'>Отдел</th>
                            <th class="col-md-2 text-center" id='fio'>Сумма начисленно</th>
                            <?php
                            for ($i = 0; $i < $EI->getMaxEnvNumSize(); $i++) {
                                echo "<th>№ (" . ($i + 1) . ")</th>";
                                echo "<th class='col-md-1'>&sum; (руб)</th>";
                            }
                            ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $count = 0;
                        $FullSumm = 0;
                        $envelope_count = 0;

                        foreach ($rawData as $item) {
                            $current = array_filter($EI->get(), function ($obj) use ($item) {
                                return $obj->getMemberId() == $item->getId(); // Ищем все конверты где ID пользователя в ведомости в конвертах совпадает
                            });

                            if ($current) {
                                $count++;
                                $FullSumm += roundSumm($item->getSumm());// Сумма итого


                                echo "<tr>";
                                echo "<td id='fio'>" . $count . "</td>";
                                echo "<td id='fio'>".$item->getFio()."</td>";
                                echo "<td>".$item->getDepartment()['name']."</td>";

//                                echo "<td>" . $item->getFio()  . "/td>";
                                echo "<td class='text-center' id='fio'>" . number_format(roundSumm($item->getSumm()), 2, ',', ' ') . "</td>";


                                foreach ($current as $env_item) {
                                    if ($env_item->getSumm() > 0)
                                        $envelope_count++;

                                    echo "<td class='text-center'> <b>" . $env_item->getEnvelopeId() . "</b></td>";
                                    switch ($env_item->getStatus()) {
                                        case 0:
                                            echo "<td>" . number_format($env_item->getSumm(), 2, ',', ' ') . "</td>";
                                            break;
                                        case 1:
                                            echo "<td style='color: red' title='" . $env_item->getOldSumm() . "'>" . number_format($env_item->getSumm(), 2, ',', ' ') . "</td>";
                                            break;
                                        case 2:
                                            echo "<td style='color: cornflowerblue' title='" . $env_item->getOldSumm() . "'>" . number_format($env_item->getSumm(), 2, ',', ' ') . "</td>";
                                            break;
                                    }
                                }
                                echo "</tr>";
                            } else {
                                continue;
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="2">Итого:</th>
                            <th class="text-center"><?php echo number_format($FullSumm, 2, ',', ' '); ?></th>
                        </tr>
                        <tr>
                            <th colspan="2">Количество конвертов:</th>
                            <th class="text-center"><?php echo $envelope_count; ?></th>
                        </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
                <div class="box-footer">
                    <form method="post">
                        <input type="hidden" class="form-control" name="MAX_ENV_SUMM" value="<?php echo $EI->getMaxEnvSumm(); ?>">
                        <input type="hidden" class="form-control" name="MIN_ENV_SUMM" value="<?php echo $EI->getMinEnvSumm() ?>">
                        <input type="hidden" class="form-control" name="MAX_ENV_NUMSIZE" value="<?php echo $EI->getMaxEnvNumSize(); ?>">

                        <?php
                        if ($Envelope_Update==1) {

                            switch ($ReportStatus['status_id']) {
                                case 1:
                                    break;
                                case 2:
                                    break;
                                case 3:
                                    break;
                                case 4:
//                                echo "<input type=\"submit\" class=\"btn btn-warning\" name=\"Confirm\" value=\"Выплачено\">";
                                    break;
                                case 5:
                                    break;
                                case 6:
                                    echo "<a href=\"departmentalreport.php?date=" . $EI->getDate() . "&TypeOfPayment=" . $EI->getTypeOfPayment() . "\" class='btn btn-flat btn-primary'  style='margin: 5px;' >Детальные расходы</a>";
                                    break;
                                default:
                                    break;
                            }
                            ?>
                            <?php


                            if ($ReportStatus['status_id'] == 3) {
                                ?>
                                <input type="submit" onclick="if (confirm('Отправить в кассу?')) { $('#modal-save').modal('show');} else { return false; };" class="btn btn-warning btn-flat" name="Confirm" value="Отправить в кассу">
                                <?php
                            }

                            if ($RTN_VALUE == 1 && $ReportStatus['status_id']<5 && $EI->filter==false) {
                                ?>
                                <input type="submit" class="btn btn-danger btn-flat" name="Update" onclick="if (confirm('Обновить конверты?')) { $('#modal-save').modal('show'); } else {return false;}" value="Обновить конверты">
                                <?php
                            }

                        }
                        ?>
                        <input type="submit" class="btn btn-primary btn-flat" name="print" onclick="print_text()" value="Печать">
                        <input type="submit" class="btn btn-primary btn-flat" name="print" onclick="print_wofio_text()" value="Печать без ФИО">
                    </form>
                </div>
            </div>

            <div class="modal fade" id="modal-save" style="background: rgba(0,0,0,1);">
                <div class="modal-dialog" style="width: 95%">
                    <div class="modal-content">
                        <div class="modal-header text-center" style="color: white;background-color: #000;border: unset;">
                            <h3 class="modal-title" style="color: white;background-color: #000;border: unset;">Сохранение данных</h3>
                        </div>

                        <div class="modal-body" style="background-color: #000;">
                            <div class="box box-solid box-info" style="height: 70%;background-color: #000;border: unset;">
                                <div class="box-body">
                                    <img class="img-responsive center-block" src="images/7TwN.gif">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


<script>
    $(function () {

        $('#MembersTable').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true
        })
    })
</script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    function print_wofio_text(){
        $("th[id^=fio]").each(function (i) {
            $(this).remove();
        });

        $("td[id^=fio]").each(function (i) {
            $(this).remove();
        });

        // $("td").each(function (i) {
        //     $(this).css("padding", "0px");
        // });

        var txt=document.getElementById("print_table").innerHTML ;
        // document.getElementsByTagName("body")[0].innerHTML=txt;
        // var text=document;
        //
        // print(text);
        var newWin = window.open("about:blank", "Печать",);
        var style = newWin.document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = 'tr,td { font-size: 8px;}';
        var div = newWin.document.createElement('div'),
            body = newWin.document.body,
            head = newWin.document.head;

        div.innerHTML = txt
        div.style.fontSize = '8px'

        // вставить первым элементом в body нового окна
        head.insertBefore(style, head.firstChild);
        body.insertBefore(div, body.firstChild);
        newWin.focus();
        newWin.print();
    }

    function print_text(){
        // $("td").each(function (i) {
        //     $(this).css("padding", "0px");
        // });

        var txt=document.getElementById("print_table").innerHTML ;
        // document.getElementsByTagName("body")[0].innerHTML=txt;
        // var text=document;
        //
        // print(text);
        var newWin = window.open("about:blank", "Печать",);
        var style = newWin.document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = 'tr,td { font-size: 8px;}';
        var div = newWin.document.createElement('div'),
            body = newWin.document.body,
            head = newWin.document.head;

        div.innerHTML = txt
        div.style.fontSize = '8px'

        // вставить первым элементом в body нового окна
        head.insertBefore(style, head.firstChild);
        body.insertBefore(div, body.firstChild);
        newWin.focus();
        newWin.print();
    }
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>

