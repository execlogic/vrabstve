<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 01.08.19
 * Time: 11:20
 */
require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());


$Reglament = 0;

// Проверяю права
array_filter($Roles, function ($Role) use (&$Reglament) {
    switch ($Role->getId()){
        case 2:
        case 4:
        case 5:
        case 6:
            $Reglament = 1;
            break;
        default:
            $Reglament = 0;
    }
    return true;
});

if ($Reglament == 0 ) {
    header("Location: 404.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Отделы компании</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">

    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>

    <div class="content-wrapper">
        <section class="content">

            <!-- Default box -->
            <div class="box box-solid box-primary">

                <!-- /.box-header -->
                <div class="box-body">
                    <h3 class="text-light-blue">Регламент занесения данных в программу ЗиК 2019</h3>
                    <ul>
                        <li> На время тестового периода руководители отделов заносят и проверяют данные по своим сотрудникам в программу и в xls файл, одновременно.</li>
                        <li> Стадия ведомости 1 «Внесение данных»</li>
                        <li> 01 числа каждого месяца (*1) отдел персонала передает данные по УРВ и НДФЛ отдельным файлом в формате ods, при добавлении в 1С ЗиК заносят номер по ЗиК сотрудникам, добавляет и увольняет сотрудников по мере необходимости, а так же следит за актуальностью рабочих и личных  данных сотрудников.</li>
                        <li> 01 числа каждого месяца (*1) руководители отделов проверяют тип мотивации сотрудников и их финансовые условия (оклад, базовый процент и т.п.) и расширенные финансовые условия, если они есть.
                            При необходимости руководители вносят изменения в условия сотрудников.</li>
                        <li> Внимание! Изменения типа Мотивация и Финансовых условий будут применены только к открытым периодам. То есть, только в те выплаты, которые ещё не произошли.</li>
                        <li> 01 числа каждого месяца (*1) руководители отделов проверяют коммерческую часть, выгружаемую в программу из 1с склад автоматически, дополняют и корректируют остальные данные ведомости (корректировки наценки, KPI, административные премии, премии по обучению, штрафы и т.п.). Проверяют итоговые данные во вкладке Предварительный расчет, кроме данных по УРВ.</li>
                        <li> После внесения и проверки данных, руководитель отдела отправляет свою ведомость на проверку ответственному директору. Левая панель, Плановые выплаты, Выбор периода, Тип выплат, Кнопка «Получить данные», далее проверяем и внизу зеленая кнопка «На проверку отв. Директору»</li>
                        <li> Стадия ведомости 2 «Проверка отв. дир.»</li>
                        <li> Ответственные директора проверяют данную ведомость и отправляют на проверку директору Компании</li>
                        <li> Стадия ведомости 3 «Утверждение директоратом»</li>
                        <li> Директор Компании вносит необходимые корректировки в ведомость и утверждает её. Формирует конверты с суммами. И отправляет в кассу. Кнопка «Отправить в кассу».</li>
                        <li> Стадия ведомости 4 «Отправлено в кассу»</li>
                        <li> Финансовый менеджер (кассир) после выдачи ответственным директорам проводит ведомость на следующую стадию кнопкой «Выплачено».</li>
                        <li> Стадия ведомости 5 «Выплачено»</li>

                        <li> Директор Компании проводит ведомость на стадию 6 «В расходы»</li>
                        <li> Стадия ведомости 6 «В расходы»</li>
                        <li> Итоговые фактические данные по ЗП выгружаются в расходы.</li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
    <?php require_once 'footer.php'; ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
