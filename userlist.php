<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.07.18
 * Time: 14:33
 */

require_once 'app/MemberInterface.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";
require_once 'app/Notify.php';


session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

$Permission_UserList = 0;

/*
 * Проверяю доступы
 */
array_filter($Roles, function ($Role) use (&$Permission_UserList) {
    if ($Role->getId() == 4 || $Role->getId() == 5 || $Role->getId() == 6) {
        $Permission_UserList = 1;
        return true;
    };
    return false;
});

/*
 * Доступ запрещен
 */
if ($Permission_UserList == 0) {
    header("Location: profile.php?id=".$User->getMemberId());
    exit();
}

function comparator($object1, $object2) {
    return $object1->getLastName() > $object2->getLastName();
}
/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

$Members = new MemberInterface();

$Errors = array();
$Debug = array();

$Members->fetchMembers();

$AllMembers = $Members->GetMembers();
usort($AllMembers, 'comparator');




?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Сотрудники компании</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Сотрудники компании</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <th>#</th>
                        <th>Направление</th>
                        <th>Отдел</th>
                        <th>Должность</th>
                        <th>ФИО</th>
                        <th>1Cv77</th>
                        </thead>

                        <tbody>
                        <?php
                        try {
                            foreach ($AllMembers as $value) {
                                echo "<tr>";
                                echo "<td><a class='text-muted' href='profile.php?id=" . $value->getId() . "'>" . $value->getId() . "</a></td>";
                                echo "<td><a class='text-muted' href='profile.php?id=" . $value->getId() . "'>" . $value->getDirection()['name'] . "</a></td>";
                                echo "<td><a class='text-muted' href='profile.php?id=" . $value->getId() . "'>" . $value->getDepartment()['name'] . "</a></td>";
                                echo "<td><a class='text-muted' href='profile.php?id=" . $value->getId() . "'>" . $value->getPosition()['name'] . "</a></td>";
                                echo "<td><a class='text-muted' href='profile.php?id=" . $value->getId() . "'>" . $value->getLastname() . " " . $value->getName() . " " . $value->getMiddle() . "</a></td>";
                                echo "<td>" . $value->getSkladName() . "</td>";
                                echo "</tr>";
                            }
                        } catch (Exception $e) {
                            echo "<tr><td colspan='12'>" . $e->getMessage() . "</td></tr>";
                        }
                        ?>
                        </tbody>

                        <tfoot>
                        <th>#</th>
                        <th>Направление</th>
                        <th>Отдел</th>
                        <th>Должность</th>
                        <th>ФИО</th>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
                <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>

</body>
</html>

