<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 16.01.19
 * Time: 17:02
 */

require_once 'app/Notify.php';
require_once 'admin/User.php';
/*
 * Если глобальной переменной _SESSION нету, то пробую запустить сессии
 */
if (!isset($_SESSION)) {
    session_start();
}

/*
 * Если нету элемента массива UserObj, значит пользователь не аутентифицирован
 */
if (!isset($_SESSION['UserObj'])) {
    header("Location: index.php");
    exit();
}

/*
 * Определяю переменную $User и массива сессии
 */
$User = $_SESSION['UserObj'];


/*
 * Проверяю прошел ли он аутетификаци.
 * Если не аутентифицирован пользователь, то идем на страницу логина и пароля
 */
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}



$NF = new Notify();
$NF->cleanNotify($User->getMemberID());

header('Location: ' . $_SERVER['HTTP_REFERER']);

?>