<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 14.09.18
 * Time: 18:32
 */

// Время работы скрипта
$start = microtime(true);
require_once "app/ErrorInterface.php";

require_once "app/MemberInterface.php";
require_once 'app/CustomPercent.php';

require_once "app/MemberFinanceBlock.php";
require_once 'app/DataItem.php';
require_once 'app/DataManager.php';

require_once 'app/AdministrativeBlock.php';
require_once 'app/Retention.php';
require_once 'app/Correction.php';

require_once 'app/ProductionCalendar.php';
require_once 'app/AwhInterface.php';
require_once 'app/PayRollInterface.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

require_once 'app/PayHistory.php';

require_once 'app/Premium.php';
require_once 'app/Kpi.php';

require_once 'app/ReportStatus.php';
require_once 'app/PayRoll.php';
require_once 'app/PayRollInterface.php';

require_once 'app/TransportBlock.php';
require_once 'app/MobileBlock.php';

function roundSumm($summ)
{
    global $member;
    // Округление до 10
    if ($summ > 0) { //Если сумма <= 0 то не обрабатываем данные
        //$summ = round($summ); // округляю сотые и десятые
        $summ = (int)$summ;

        $ostatok = ($summ % 10); // Получаю остаток от деления на 10
        if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
            $summ = $summ - $ostatok + 10;
        } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
            $summ = $summ - $ostatok;
        };
    } else {
        if ($member->getWorkstatus() == 1) {
            $summ = 0;
        } else {
            $summ = (int)$summ;

            $ostatok = ($summ % 10)*-1; // Получаю остаток от деления на 10
            if ($ostatok >= 5) { // Если остаток >= 5 значит округляю в большую сторону
                $summ = $summ - $ostatok - 10;
            } else if ($ostatok < 5 && $ostatok > 0) { // Если остаток меньше 5, то округляю в меньшую сторону.
                $summ = $summ + $ostatok;
            };
        }
    }
    return $summ;
}

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

$Permission_PremiyaCalculation = 0;

$Permission_CommercialBlockBonus = 0;
$Permission_CommercialBlockNachenka = 0;
$Permission_CommercialBlockProcent = 0;

$Permission_Promotions = 0;
$Permission_AdministrativePrize = 0;
$Permission_Training = 0;
$Permission_PrizeAnotherDepartment = 0;
$Permission_Defect = 0;
$Permission_Sunday = 0;
$Permission_NotLessThan = 0;
$Permission_MobileCommunication = 0;
$Permission_Overtime = 0;
$Permission_CompensationFAL = 0;

$Permission_LateWork = 0;
$Permission_Schedule = 0;
$Permission_Internet = 0;
$Permission_CachAccounting = 0;
$Permission_Receivables = 0;
$Permission_Credit = 0;
$Permission_Other = 0;

$Permission_BonusAdjustment = 0;
$Permission_Calculation = 0;
$Permission_YearBonus = 0;

$Permission_ShowAdvanceCalculation = 0;
$Permission_ShowCommercialBlock = 0;
$Permission_ShowAdministrativeBlock = 0;
$Permission_ShowRetitiotionBlock = 0;
$Permission_ShowCorrectingBlock = 0;
$Permission_ShowAWHBlock = 0;

$Permission_AWH = 0;

$AdministrativeBlock_Array = array(0,0,0,0,0,0,0,0,0,0,0);
$RetitiotionBlock_Array = array(0,0,0,0,0,0,0,0);
$CorrectingBlock_Array = array(0,0,0,0,0);

$Error = new ErrorInterface();

if ($_REQUEST['id']) {
    $user_id = $_REQUEST['id'];

    // Если не число, то ошибка выходим
    if (!is_numeric($user_id)) {
        header('Location: index.php');
        exit();
    }

    // Создаем объект MemberInterface
    $MI = new MemberInterface();
    // Получаю данные по пользователю
    try {
        $member = $MI->fetchMemberByID($user_id);
    } catch (Exception $e) {
        echo $e->getMessage();
        exit();
    }

    /** Права доступа */

    $member_department_id = $member->getDepartment()['id'];

    if ($User->getMemberId() != $user_id) {
        $current = current(array_filter($Roles, function ($Role) use ($member_department_id) { // Если в ролях есть аванс и (отдел == 0 или отдел == отделу пользователя)
            return (($Role->getPremiyaCalculation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        }));

        if ($current) { // Если существует объект, то можно смотреть и править.
            $Permission_PremiyaCalculation = 1;
        } else { // Если это не свой профиль и нету объекта ролей, то слать в 404
//            header("Location: 404.php");
        }
    };

    if ($User->getMemberId() == 94 && $member->getId() == 65) {
        $Permission_PremiyaCalculation = 1;

        $Permission_CommercialBlockBonus = 1;
        $Permission_CommercialBlockNachenka = 1;
        $Permission_CommercialBlockProcent = 1;

        $Permission_Promotions = 1;
        $Permission_AdministrativePrize = 1;
        $Permission_Training = 1;
        $Permission_PrizeAnotherDepartment = 1;
        $Permission_Defect = 1;
        $Permission_Sunday = 1;
        $Permission_NotLessThan = 1;
        $Permission_MobileCommunication = 1;
        $Permission_Overtime = 1;
        $Permission_CompensationFAL = 1;

        $Permission_LateWork = 1;
        $Permission_Schedule = 1;
        $Permission_Internet = 1;
        $Permission_CachAccounting = 1;
        $Permission_Receivables = 1;
        $Permission_Credit = 1;
        $Permission_Other = 1;

        $Permission_BonusAdjustment = 1;
        $Permission_Calculation = 1;
        $Permission_YearBonus = 1;

        $Permission_ShowAdvanceCalculation = 1;
        $Permission_ShowCommercialBlock = 1;
        $Permission_ShowAdministrativeBlock = 1;
        $Permission_ShowRetitiotionBlock = 1;
        $Permission_ShowCorrectingBlock = 1;
        $Permission_ShowAWHBlock = 1;

        $Permission_AWH = 1;

    }

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_CommercialBlockBonus) {
        if (($Role->getCommercialBlockBonus() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_CommercialBlockBonus = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_CommercialBlockNachenka) {
        if (($Role->getCommercialBlockNachenka() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_CommercialBlockNachenka = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_CommercialBlockProcent) {
        if (($Role->getCommercialBlockProcent() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_CommercialBlockProcent = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Promotions, &$AdministrativeBlock_Array) {
        if (($Role->getPromotions() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Promotions = 1;
            $AdministrativeBlock_Array[1] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_AdministrativePrize, &$AdministrativeBlock_Array) {
        if (($Role->getAdministrativePrize() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_AdministrativePrize = 1;
            $AdministrativeBlock_Array[2] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Training, &$AdministrativeBlock_Array) {
        if (($Role->getTraining() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Training = 1;
            $AdministrativeBlock_Array[3] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_PrizeAnotherDepartment, &$AdministrativeBlock_Array) {
        if (($Role->getPrizeAnotherDepartment() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_PrizeAnotherDepartment = 1;
            $AdministrativeBlock_Array[4] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Defect, &$AdministrativeBlock_Array) {
        if (($Role->getDefect() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Defect = 1;
            $AdministrativeBlock_Array[5] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Sunday, &$AdministrativeBlock_Array) {
        if (($Role->getSunday() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Sunday = 1;
            $AdministrativeBlock_Array[6] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_NotLessThan, &$AdministrativeBlock_Array) {
        if (($Role->getNotLessThan() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_NotLessThan = 1;
            $AdministrativeBlock_Array[7] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_MobileCommunication, &$AdministrativeBlock_Array) {
        if (($Role->getMobileCommunication() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_MobileCommunication = 1;
            $AdministrativeBlock_Array[8] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Overtime, &$AdministrativeBlock_Array) {
        if (($Role->getOvertime() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Overtime = 1;
            $AdministrativeBlock_Array[9] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_CompensationFAL, &$AdministrativeBlock_Array) {
        if (($Role->getCompensationFAL() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_CompensationFAL = 1;
            $AdministrativeBlock_Array[10] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_LateWork, &$RetitiotionBlock_Array) {
        if (($Role->getLateWork() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_LateWork = 1;
            $RetitiotionBlock_Array[1] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Schedule, &$RetitiotionBlock_Array) {
        if (($Role->getSchedule() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Schedule = 1;
            $RetitiotionBlock_Array[2] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Internet, &$RetitiotionBlock_Array) {
        if (($Role->getInternet() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Internet = 1;
            $RetitiotionBlock_Array[3] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_CachAccounting, &$RetitiotionBlock_Array) {
        if (($Role->getCachAccounting() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_CachAccounting = 1;
            $RetitiotionBlock_Array[4] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Receivables, &$RetitiotionBlock_Array) {
        if (($Role->getReceivables() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Receivables = 1;
            $RetitiotionBlock_Array[5] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Credit, &$RetitiotionBlock_Array) {
        if (($Role->getCredit() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Credit = 1;
            $RetitiotionBlock_Array[6] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Other, &$RetitiotionBlock_Array) {
        if (($Role->getOther() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Other = 1;
            $RetitiotionBlock_Array[7] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_BonusAdjustment, &$CorrectingBlock_Array) {
        if (($Role->getBonusAdjustment() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_BonusAdjustment = 1;
            $CorrectingBlock_Array[1] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_Calculation, &$CorrectingBlock_Array) {
        if (($Role->getCalculation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_Calculation = 1;
            $CorrectingBlock_Array[2] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_YearBonus, &$CorrectingBlock_Array) {
        if (($Role->getYearBonus() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_YearBonus = 1;
            $CorrectingBlock_Array[3] = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ShowAdvanceCalculation) {
        if (($Role->getShowAdvanceCalculation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ShowAdvanceCalculation = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ShowCommercialBlock) {
        if (($Role->getShowCommercialBlock() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ShowCommercialBlock = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ShowAdministrativeBlock) {
        if (($Role->getShowAdministrativeBlock() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ShowAdministrativeBlock = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ShowRetitiotionBlock) {
        if (($Role->getShowRetitiotionBlock() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ShowRetitiotionBlock = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ShowCorrectingBlock) {
        if (($Role->getShowCorrectingBlock() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ShowCorrectingBlock = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_ShowAWHBlock) {
        if (($Role->getShowAWHBlock() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_ShowAWHBlock = 1;
            return true;
        }
        return false;
    });

    array_filter($Roles, function ($Role) use ($member_department_id, &$Permission_AWH) {
//        if ($Role->getId() == 1 || $Role->getId() == 5 || $Role->getId() == 6) {
        if (($Role->getAWHEdit() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id))) {
            $Permission_AWH = 1;
            return true;
        } else {
            return false;
        };
    });

    $IS_RUK = 0;
    $IS_DIR = 0;

    foreach ($Roles as $R) {
        if ($R->getId() == 4) {
            $IS_DIR = 1;
            $IS_RUK = 0;
            break;
        }

        if (($R->getId() == 5) || ($R->getId() == 6)) {
            $IS_DIR = 2;
            $IS_RUK = 0;

            break;
        }

        if ($R->getId() == 2) {
            $IS_RUK = 1;
        }
    }

    $rtn = array_filter($Roles, function($R) {
        return ($R->getId()==5 || $R->getId()==6);
    });

    if ($rtn) {
        $IS_DIR = 2;
        $IS_RUK = 0;
    }

    if ($IS_DIR == 2) {
        foreach ($AdministrativeBlock_Array as $key=>$item)
            $AdministrativeBlock_Array[$key] = 1;

        foreach ($CorrectingBlock_Array as $key=>$item)
            $CorrectingBlock_Array[$key] = 1;

        foreach ($CorrectingBlock_Array as $key=>$item)
            $CorrectingBlock_Array[$key] = 1;
    }

    $PR = new Premium();
    $PR->setMember($member);
    $PR->setAdministrativeBlockArray($AdministrativeBlock_Array);
    $PR->setRetitiotionBlockArray($RetitiotionBlock_Array);
    $PR->setCorrectingBlockArray($CorrectingBlock_Array);

    //Выставляю текущую дату, если в запросе не будет ['date']
    $date = date("m.Y");

    if (isset($_COOKIE['premium_date'])) {
        $date = $_COOKIE['premium_date'];
    }

    // Выставляю дату
    if (isset($_REQUEST['date']) || isset($_COOKIE['premium_date'])) {
        if (isset($_REQUEST['date'])) {
            $date = $_REQUEST['date']; // то выставляю дату с новыми зачениями и начинаю разбор блока
            if (isset($_COOKIE['premium_date'])) {
                setcookie("premium_date", $date, time() + 3000);
            } else {
                setcookie("premium_date", $date);
            }
        }

        $test_date = DateTime::createFromFormat("d.m.Y", "01.".$date);
        if ($test_date->format("Y-m-d")<'2021-05-01')
            $older_format = 1;
        else
            $older_format = 0;
        unset($test_date);

        $RS = new ReportStatus();
        if ($member->getWorkstatus() == 1) {
            $Status = $RS->getFiltredStatus($date, 1, $member_department_id)['status_id'];
        } else {
            $Status = $RS->getStatusMemberLeaving($date, $member->getId())['status_id'];
        }

        /*
         * Если руководитель офиса
         */
        if ($member->getWorkstatus() == 1 && $IS_RUK == 1 && $IS_DIR == 0 && $Status > 1) {
            $PR->setPremiumLock();
        }
        /*
         * Отвественный директор
         * Возможность исправлять только на стадии 1,2
         */
        if ($member->getWorkstatus() == 1 && $IS_DIR == 1 && $Status > 2 ) {
            $PR->setPremiumLock();
        }

        /*
         * Директор и Root
         * Возможность исправлять только на стадии 1,2,3,4
         */
        if ($member->getWorkstatus() == 1 && $IS_DIR == 2 && $Status > 4) {
            $PR->setPremiumLock();
        }

//        $JP = $MI->getJobPostByDate($user_id, $date);
//        if ($JP) {
//            $member->setDepartment(["id" => $JP["department_id"], "name" => $JP["department_name"]]);
//        }


//        $member_department_id = $member->getDepartment()['id'];

//        echo "Department: ".$member_department_id."<BR>";

        $current = current(array_filter($Roles, function ($Role) use ($member_department_id) { // Если в ролях есть аванс и (отдел == 0 или отдел == отделу пользователя)
            return (($Role->getPremiyaCalculation() == 1) && (($Role->getDepartment() == 0) || ($Role->getDepartment() == $member_department_id)));
        }));

        if (!$current) { // Если существует объект, то можно смотреть и править.
            header("Location: 404.php");
        }

        $current = null;

        /*
        *  Создаю объект производственный календарь
        */
        $PC = new ProductionCalendar();
//        $MB = new MobileBlock();
//        $TR = new TransportBlock();
        /*
         *  Извлекаю из БД
         */
//        $PC->fetchByDate($date);
        $PC->fetchByDateWithDepartment($date, $member->getDepartment()['id']);


//        $member->setMotivation($MI->getMotivationByDate($user_id, $date));

        $PR->setDate($date);
        try {
            $PR->fetch($user_id, true);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        /*
         * Получаю объекты
         */
        $AB = $PR->getAB();
        $RN = $PR->getRN();
        $CR = $PR->getCR();
        $AWH = $PR->getAWH();
        $KPI = $PR->getKPI();
        $TR = $PR->getTR();
        $MB = $PR->getMB();

//        if (($IS_RUK==1) && ($Status>1)) {
//            $PR->setPaymentMade(true);
//        }

        $PR->getPremiumHistory();
        $PH = $PR->getPH();

        if ($member->getEmploymentDate()) {
            $emp_date_arr = explode(".", $member->getEmploymentDate());
        }

        if (isset($_REQUEST['date']) && isset($_POST['getBonusCorrect'])) {
            try {
                $PR->calculateBonusCorrect($date);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

        /* ****************************** Обработка событий ****************************** */
        if (isset($_REQUEST['date']) && isset($_POST['SendForm']) && ($Status < 5)) {

            // Обновляю данные коммерческого блока если мотивация пользователя != 6 !!!
            //          !!! !!! !!! Бонус, наценка и корректирующий процент

            if (($PR->getMotivation() != 6) && ($Permission_ShowCommercialBlock == 1)) {
                // Корректирующий коэффициент (Коммерческий корректирующий коэффициент)
                // Если существует CommercialCorrectingProcent и он не равен текущему коммерческому проценту, это значит что было изменение
                if (isset($_POST['CommercialCorrectingProcent']) && $Permission_CommercialBlockProcent == 1) {
                    // Меняю с запятой на точку, т.к. float всегда с точкой
                    $_POST['CommercialCorrectingProcent'] = str_replace(",", ".", $_POST['CommercialCorrectingProcent']);

                    // Проверяю число ли эта переменная
                    if (is_numeric($_POST['CommercialCorrectingProcent']) && ($_POST['CommercialCorrectingProcent'] != $PR->getCommercialProcent())) {
                        // Если число, то изменяю коммерческой процент.
                        try {

                            $PR->changeCommercialProcent($date, $_POST['CommercialCorrectingProcent'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    };
                };

                if (($Permission_CommercialBlockNachenka == 1 || $Permission_CommercialBlockBonus == 1)) {
                    if (isset($_POST['correcting'])) {
                        if ($older_format) {
                            $PR->changeCorrecting($_POST['correcting'], $User->getMemberId());
                        } else {
                            if (!empty($_POST['correcting'])) {
//                                echo "<pre>".print_r($_POST['correcting'],true)."</pre>";
//                                exit(0);
                                $CorrectingData = [];

                                foreach ($_POST['correcting'] as $key => $correct) {
                                    if (($correct[0]=='' and $correct[0]!=0) && ($correct[2]=='' and $correct[2]!=0))
                                        continue;

                                    $key= explode("_",$key)[0];

                                    $CorrectingData[] = [
                                        $user_id,
                                        $key,
                                        $date,
                                        $correct[0],
                                        $correct[1],
                                        $correct[2],
                                        $correct[3],
                                        ($correct['service_id']?$correct['service_id']:NULL),
                                    ];
                                }

                                try {
                                    $PR->changeCorrectingNew($CorrectingData, $User->getMemberId());
                                    /*
                                    * Создаю объект интерфейса ведомости
                                    */
                                    $PI = new PayRollInterface();
                                    $PI->UpdateDataFinancialPayout([
                                       'data' => $PR->getCommercialJson(),
                                       'date' => $date,
                                       'member_id' => $member->getId(),
                                       'type_of_payment' => 1
                                    ]);
                                } catch (Exception $e) {
                                    echo $e->getMessage()."<BR>";
                                    exit(255);
                                }
                            }
                        }
                    }
                }
            }

            /**  KPI  **/
            if (isset($_POST['kpi'])) {
                foreach ($_POST["kpi"] as $item) {
                    try {
                        $KPI->updateWithDateToPayment($user_id, $item, $date, 1, $User->getMemberId());
                    } catch (Exception $e) {
                        echo $e . "<BR>";
                    }
                }
            }


            /** ********************* Административный блок ********************* **/

            // Проверяю на существование переменной Promotions
            if (!is_null($AB->getPromotions()) && empty($_POST['Promotions'])) {
                $_POST['Promotions'] = "0";
            }
            if (!is_null($AB->getAdministrativePrize()) && empty($_POST['AdministrativePrize'])) {
                $_POST['AdministrativePrize'] = 0;
            }
            if (!is_null($AB->getTraining()) && empty($_POST['Training'])) {
                $_POST['Training'] = 0;
            }
            if (!is_null($AB->getPrizeAnotherDepartment()) && empty($_POST['PrizeAnotherDepartment'])) {
                $_POST['PrizeAnotherDepartment'] = 0;
            }
            if (!is_null($AB->getDefect()) && empty($_POST['Defect'])) {
                $_POST['Defect'] = 0;
            }
            if (!is_null($AB->getSunday()) && empty($_POST['Sunday'])) {
                $_POST['Sunday'] = 0;
            }
            if (!is_null($AB->getNotLessThan()) && empty($_POST['NotLessThan'])) {
                $_POST['NotLessThan'] = 0;
            }
            if (!is_null($MB->getSumm()) && empty($_POST['MobileCommunication'])) {
                $_POST['MobileCommunication'] = 0;
            }
            if (!is_null($AB->getOvertime()) && empty($_POST['Overtime'])) {
                $_POST['Overtime'] = 0;
            }
            if (!is_null($TR->getSumm()) && empty($_POST['CompensationFAL'])) {
                $_POST['CompensationFAL'] = 0;
            }

            if (isset($_POST['Promotions']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_Promotions == 1)) {
                // заменяю запятую на точку
                $_POST['Promotions'] = str_replace(",", ".", $_POST['Promotions']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Promotions'])) {
                    if ($AB->getPromotions() != $_POST['Promotions'] || $AB->getPromotionsNote() != $_POST['Promotions_note']) {
                        try {
                            $AB->addByType($user_id, $date, 1, $_POST['Promotions'], $_POST['Promotions_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной AdministrativePrize и AdministrativePrize не равен значению в профиле
//            if (isset($_POST['AdministrativePrize']) && ($member->getAdministrativePrize() != $_POST['AdministrativePrize']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_AdministrativePrize == 1)) {
            if (isset($_POST['AdministrativePrize']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_AdministrativePrize == 1)) {
                // заменяю запятую на точку
                $_POST['AdministrativePrize'] = str_replace(",", ".", $_POST['AdministrativePrize']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['AdministrativePrize'])) {
                    if ($AB->getAdministrativePrize() != $_POST['AdministrativePrize'] || $AB->getAdministrativePrizeNote() != $_POST['AdministrativePrize_note']) {
                        try {
                            $AB->addByType($user_id, $date, 2, $_POST['AdministrativePrize'], $_POST['AdministrativePrize_note'], $User->getMemberId());
//                        $AB->addAdministrativePrize($user_id, $date, $_POST['AdministrativePrize'], $_POST['AdministrativePrize_note']);
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Training
            if (isset($_POST['Training']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_Training == 1)) {
                // заменяю запятую на точку
                $_POST['Training'] = str_replace(",", ".", $_POST['Training']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Training'])) {
                    if ($AB->getTraining() != $_POST['Training'] || $AB->getTrainingNote() != $_POST['Training_note']) {
                        try {
                            $AB->addByType($user_id, $date, 3, $_POST['Training'], $_POST['Training_note'], $User->getMemberId());
//                        $AB->addTraining($user_id, $date, $_POST['Training'], $_POST['Training_note']);
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной PrizeAnotherDepartment
            if (isset($_POST['PrizeAnotherDepartment']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_PrizeAnotherDepartment == 1)) {
                // заменяю запятую на точку
                $_POST['PrizeAnotherDepartment'] = str_replace(",", ".", $_POST['PrizeAnotherDepartment']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['PrizeAnotherDepartment'])) {
                    if ($AB->getPrizeAnotherDepartment() != $_POST['PrizeAnotherDepartment'] || $AB->getPrizeAnotherDepartmentNote() != $_POST['PrizeAnotherDepartment_note']) {
                        try {
                            $AB->addByType($user_id, $date, 4, $_POST['PrizeAnotherDepartment'], $_POST['PrizeAnotherDepartment_note'], $User->getMemberId());
//                        $AB->addPrizeAnotherDepartment($user_id, $date, $_POST['PrizeAnotherDepartment'], $_POST['PrizeAnotherDepartment_note']);
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Defect
            if (isset($_POST['Defect']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_Defect == 1)) {
                // заменяю запятую на точку
                $_POST['Defect'] = str_replace(",", ".", $_POST['Defect']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Defect'])) {
                    if ($AB->getDefect() != $_POST['Defect'] || $AB->getDefectNote() != $_POST['Defect_note']) {
                        try {
                            $AB->addByType($user_id, $date, 5, $_POST['Defect'], $_POST['Defect_note'], $User->getMemberId());
//                        $AB->addDefect($user_id, $date, $_POST['Defect'], $_POST['Defect_note']);
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Sunday
            if (isset($_POST['Sunday']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_Sunday == 1)) {
                // заменяю запятую на точку
                $_POST['Sunday'] = str_replace(",", ".", $_POST['Sunday']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Sunday'])) {
                    if ($AB->getSunday() != $_POST['Sunday'] || $AB->getSundayNote() != $_POST['Sunday_note']) {
                        try {
                            $AB->addByType($user_id, $date, 6, $_POST['Sunday'], $_POST['Sunday_note'], $User->getMemberId());
//                        $AB->addSunday($user_id, $date, $_POST['Sunday'], $_POST['Sunday_note']);
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной NotLessThan
            if (isset($_POST['NotLessThan']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_NotLessThan == 1)) {
                // заменяю запятую на точку
                $_POST['NotLessThan'] = str_replace(",", ".", $_POST['NotLessThan']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['NotLessThan'])) {
                    if ($AB->getNotLessThan() != $_POST['NotLessThan'] || $AB->getNotLessThanNote() != $_POST['NotLessThan_note']) {
                        try {
                            $AB->addByType($user_id, $date, 7, $_POST['NotLessThan'], $_POST['NotLessThan_note'], $User->getMemberId());
//                        $AB->addNotLessThan($user_id, $date, $_POST['NotLessThan'], $_POST['NotLessThan_note']);
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной MobileCommunication и MobileCommunication не равен значению в профиле
            if (isset($_POST['MobileCommunication']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_MobileCommunication == 1)) {
                // заменяю запятую на точку
                $_POST['MobileCommunication'] = str_replace(",", ".", $_POST['MobileCommunication']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['MobileCommunication'])) {
                    if ($MB->getSumm() != $_POST['MobileCommunication'] || $MB->getNote() != $_POST['MobileCommunication_note']) {
                        try {
                            if (isset($_POST['MobileFormula'])) {
                                $MB->setSumm($_POST['MobileCommunication']);
                                $MB->setNote($_POST['MobileCommunication_note']);
                                $MB->recountMobile(($PR->getAbsence() + $PR->getNewAbsence() + $PR->getHoliday() + $PR->getDisease()), $PC->get()["working_days"]);
                            } else {
                                $MB->setMobile($date, 1, $user_id, $_POST['MobileCommunication'], $_POST['MobileCommunication_note'], $User->getMemberId(), 0);
                                $MB->setSumm($_POST['MobileCommunication']);
                                $MB->setNote($_POST['MobileCommunication_note']);
                            }
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Overtime
            if (isset($_POST['Overtime']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_Overtime == 1)) {
                // заменяю запятую на точку
                $_POST['Overtime'] = str_replace(",", ".", $_POST['Overtime']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Overtime'])) {
                    if ($AB->getOvertime() != $_POST['Overtime'] || $AB->getOvertimeNote() != $_POST['Overtime_note']) {
                        try {
                            $AB->addByType($user_id, $date, 9, $_POST['Overtime'], $_POST['Overtime_note'], $User->getMemberId());
//                        $AB->addOvertime($user_id, $date, $_POST['Overtime'], $_POST['Overtime_note']);
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной CompensationFAL
            if (isset($_POST['CompensationFAL']) && ($Permission_ShowAdministrativeBlock == 1) && ($Permission_CompensationFAL == 1)) {
                // заменяю запятую на точку
                $_POST['CompensationFAL'] = str_replace(",", ".", $_POST['CompensationFAL']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['CompensationFAL'])) {
                    if ($TR->getSumm() != $_POST['CompensationFAL'] || $TR->getNote() != $_POST['CompensationFAL_note']) {
                        try {
                            if ($_POST['TransportFormula']) {
                                $TR->setSumm($_POST['CompensationFAL']);
                                $TR->setNote($_POST['CompensationFAL_note']);
                                $TR->recountCompensationFAL(($PR->getAbsence() + $PR->getNewAbsence() + $PR->getHoliday() + $PR->getDisease()), $PC->get()["working_days"]);
                            } else {
                                $TR->setTransport($date, 1, $user_id, $_POST['CompensationFAL'], $_POST['CompensationFAL_note'], $User->getMemberId(), 0);
                                $TR->setSumm($_POST['CompensationFAL']);
                                $TR->setNote($_POST['CompensationFAL_note']);
                            }
//                        $AB->addCompensationFAL($user_id, $date, $_POST['CompensationFAL'], $_POST['CompensationFAL_note']);
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }


            /** ********************* Удержания ********************* **/

            if (!is_null($RN->getLateWork()) && empty($_POST['LateWork'])) {
                $_POST['LateWork'] = 0;
            }
            if (!is_null($RN->getSchedule()) && empty($_POST['Schedule'])) {
                $_POST['Schedule'] = 0;
            }
            if (!is_null($RN->getInternet()) && empty($_POST['Internet'])) {
                $_POST['Internet'] = 0;
            }
            if (!is_null($RN->getCachAccounting()) && empty($_POST['CachAccounting'])) {
                $_POST['CachAccounting'] = 0;
            }
            if (!is_null($RN->getReceivables()) && empty($_POST['Receivables'])) {
                $_POST['Receivables'] = 0;
            }
            if (!is_null($RN->getCredit()) && empty($_POST['Credit'])) {
                $_POST['Credit'] = 0;
            }
            if (!is_null($RN->getOther()) && empty($_POST['Other'])) {
                $_POST['Other'] = 0;
            }

            // Проверяю на существование переменной LateWork
            if (isset($_POST['LateWork']) && ($Permission_ShowRetitiotionBlock == 1) && ($Permission_LateWork == 1)) {
                // заменяю запятую на точку
                $_POST['LateWork'] = str_replace(",", ".", $_POST['LateWork']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['LateWork'])) {
                    if ($RN->getLateWork() != $_POST['LateWork'] || $RN->getLateWorkNote() != $_POST['LateWork_note']) {
                        try {
                            $RN->addByType($user_id, $date, 1, $_POST['LateWork'], $_POST['LateWork_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Schedule
            if (isset($_POST['Schedule']) && ($Permission_ShowRetitiotionBlock == 1) && ($Permission_Schedule == 1)) {
                // заменяю запятую на точку
                $_POST['Schedule'] = str_replace(",", ".", $_POST['Schedule']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Schedule'])) {
                    if ($RN->getSchedule() != $_POST['Schedule'] || $RN->getScheduleNote() != $_POST['Schedule_note']) {
                        try {
                            $RN->addByType($user_id, $date, 2, $_POST['Schedule'], $_POST['Schedule_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Internet
            if (isset($_POST['Internet']) && ($Permission_ShowRetitiotionBlock == 1) && ($Permission_Internet == 1)) {
                // заменяю запятую на точку
                $_POST['Internet'] = str_replace(",", ".", $_POST['Internet']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Internet'])) {
                    if ($RN->getInternet() != $_POST['Internet'] || $RN->getInternetNote() != $_POST['Internet_note']) {
                        try {
                            $RN->addByType($user_id, $date, 3, $_POST['Internet'], $_POST['Internet_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной CachAccounting
            if (isset($_POST['CachAccounting']) && ($Permission_ShowRetitiotionBlock == 1) && ($Permission_CachAccounting == 1)) {
                // заменяю запятую на точку
                $_POST['CachAccounting'] = str_replace(",", ".", $_POST['CachAccounting']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['CachAccounting'])) {
                    if ($RN->getCachAccounting() != $_POST['CachAccounting'] || $RN->getCachAccountingNote() != $_POST['CachAccounting_note']) {
                        try {
                            $RN->addByType($user_id, $date, 4, $_POST['CachAccounting'], $_POST['CachAccounting_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной CachAccounting
            if (isset($_POST['Receivables']) && ($Permission_ShowRetitiotionBlock == 1) && ($Permission_Receivables == 1)) {
                // заменяю запятую на точку
                $_POST['Receivables'] = str_replace(",", ".", $_POST['Receivables']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Receivables'])) {
                    if ($RN->getReceivables() != $_POST['Receivables'] || $RN->getReceivablesNote() != $_POST['Receivables_note']) {
                        try {
                            $RN->addByType($user_id, $date, 5, $_POST['Receivables'], $_POST['Receivables_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Кредит
            if (isset($_POST['Credit']) && ($Permission_ShowRetitiotionBlock == 1) && ($Permission_Credit == 1)) {
                // заменяю запятую на точку
                $_POST['Credit'] = str_replace(",", ".", $_POST['Credit']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Credit'])) {
                    if ($RN->getCredit() != $_POST['Credit'] || $RN->getCreditNote() != $_POST['Credit_note']) {
                        try {
                            $RN->addByType($user_id, $date, 6, $_POST['Credit'], $_POST['Credit_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Other
            if (isset($_POST['Other']) && ($Permission_ShowRetitiotionBlock == 1) && ($Permission_Other == 1)) {
                // заменяю запятую на точку
                $_POST['Other'] = str_replace(",", ".", $_POST['Other']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Other'])) {
                    if ($RN->getOther() != $_POST['Other'] || $RN->getOtherNote() != $_POST['Other_note']) {
                        try {
                            $RN->addByType($user_id, $date, 7, $_POST['Other'], $_POST['Other_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            /** ********************* Корректировки ********************* **/

            if (!is_null($CR->getBonusAdjustment()) && empty($_POST['BonusAdjustment'])) {
                $_POST['BonusAdjustment'] = 0;
            }

            if (!is_null($CR->getCalculation()) && empty($_POST['Calculation'])) {
                $_POST['Calculation'] = 0;
            }

            // Проверяю на существование переменной BonusAdjustment
            if (isset($_POST['BonusAdjustment']) && ($Permission_ShowCorrectingBlock == 1) && ($Permission_BonusAdjustment == 1)) {
                // заменяю запятую на точку
                $_POST['BonusAdjustment'] = str_replace(",", ".", $_POST['BonusAdjustment']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['BonusAdjustment'])) {
                    if ($CR->getBonusAdjustment() != $_POST['BonusAdjustment'] || $CR->getBonusAdjustmentNote() != $_POST['BonusAdjustment_note']) {
                        try {
                            $CR->addByType($user_id, $date, 1, $_POST['BonusAdjustment'], $_POST['BonusAdjustment_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Calculation
            if (isset($_POST['Calculation']) && ($Permission_ShowCorrectingBlock == 1) && ($Permission_Calculation == 1)) {
                // заменяю запятую на точку
                $_POST['Calculation'] = str_replace(",", ".", $_POST['Calculation']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Calculation'])) {
                    if ($CR->getCalculation() != $_POST['Calculation'] || $CR->getCalculationNote() != $_POST['Calculation_note']) {
                        try {
                            $CR->addByType($user_id, $date, 2, $_POST['Calculation'], $_POST['Calculation_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной AnnualBonus
            if (isset($_POST['YearBonus'])) {
                // заменяю запятую на точку
                $_POST['YearBonus'] = str_replace(",", ".", $_POST['YearBonus']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['YearBonus'])) {
                    if ($CR->getYearBonus() != $_POST['YearBonus'] || $CR->getYearBonusNote() != $_POST['YearBonus_note']) {
                        try {
                            $CR->addByType($user_id, $date, 3, $_POST['YearBonus'], $_POST['YearBonus_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            if ($member->getWorkstatus() >= 2) {
                if (!is_null($CR->getHospital()) && empty($_POST['Hospital'])) {
                    $_POST['Hospital'] = 0;
                }

                if (!is_null($CR->getHoliday()) && empty($_POST['Holiday_R'])) {
                    $_POST['Holiday_R'] = 0;
                }
            }

            if (isset($_POST['Holiday_R']) && ($Permission_ShowCorrectingBlock == 1) && ($Permission_AWH == 1) && ($member->getWorkstatus() >= 2)) {
                // заменяю запятую на точку

                $_POST['Holiday_R'] = str_replace(",", ".", $_POST['Holiday_R']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Holiday_R'])) {
                    if ($CR->getHoliday() != $_POST['Holiday_R'] || $CR->getHolidayNote() != $_POST['Holiday_note']) {
                        try {
                            $CR->addByType($user_id, $date, 4, $_POST['Holiday_R'], $_POST['Holiday_note'], $User->getMemberId());
                        } catch (Exception $e) {
                            $Error->add($e->getMessage());
                        }
                    }
                }
            }

            // Проверяю на существование переменной Hospital
            if (isset($_POST['Hospital']) && ($Permission_ShowCorrectingBlock == 1) && ($Permission_AWH == 1) && ($member->getWorkstatus() >= 2)) {
                // заменяю запятую на точку
                $_POST['Hospital'] = str_replace(",", ".", $_POST['Hospital']);
                // Если это число то вношу изменения в БД
                if (is_numeric($_POST['Hospital'])) {
                    try {
                        $CR->addByType($user_id, $date, 5, $_POST['Hospital'], $_POST['Hospital_note'], $User->getMemberId());
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            /** ******************** УРВ ******************** */
            if (isset($_POST['Absence'])) {
                $_POST['Absence'] = str_replace(",", ".", $_POST['Absence']);
                if (is_numeric($_POST['Absence']) && $_POST['Absence'] != $PR->getAbsence()) {
                    try {
                        $AWH->setAbsenceByMember($user_id, $date, 1, $_POST['Absence']);
                        $PR->setAbsence($_POST['Absence']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['Holiday'])) {
                $_POST['Holiday'] = str_replace(",", ".", $_POST['Holiday']);
                if (is_numeric($_POST['Holiday']) && $_POST['Holiday'] != $PR->getHoliday()) {
                    try {

                        $AWH->setHolidayByMember($user_id, $date, 1, $_POST['Holiday']);
                        $PR->setHoliday($_POST['Holiday']);


                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['Disease'])) {
                $_POST['Disease'] = str_replace(",", ".", $_POST['Disease']);
                if (is_numeric($_POST['Disease']) && $_POST['Disease'] != $PR->getDisease()) {
                    try {
                        $AWH->setDiseaseByMember($user_id, $date, 1, $_POST['Disease']);
                        $PR->getDisease($_POST['Disease']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['NDFL'])) {
                $_POST['NDFL'] = str_replace(",", ".", $_POST['NDFL']);
                if (is_numeric($_POST['NDFL']) && $_POST['NDFL'] != $PR->getNdfl()) {
                    try {
                        $AWH->setNDFLByMember($user_id, $date, $_POST['NDFL']);
//                        $member_NDFL['ndfl'] = $_POST['NDFL'];
                        $PR->setNdfl($_POST['NDFL']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['RO'])) {
                $_POST['RO'] = str_replace(",", ".", $_POST['RO']);
                if (is_numeric($_POST['RO']) && $_POST['RO'] != $PR->getRo()) {
                    try {
                        $AWH->setROByMember($user_id, $date, $_POST['RO']);
                        $PR->getRo($_POST['RO']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['awh_note'])) {
                if ($_POST['awh_note'] != $PR->getAwhNote()) {
                    try {
                        $AWH->setNoteByMember($user_id, $date, 1, $_POST['awh_note']);
                        $PR->setAwhNote($_POST['awh_note']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['ExtraHoliday'])) {
                if ($_POST['ExtraHoliday'] != $PR->getAwhExtraholiday()) {
                    try {
                        $AWH->setExtraHoliday($date, $user_id, $_POST['ExtraHoliday'], $User->getMemberId());
                        $PR->setAwhExtraholiday($_POST['ExtraHoliday']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }


            $member_awh = $AWH->getAwhByMember($date, 1, $user_id); //Премия
            $awh_new_absence = $member_awh['new_absence'];
            $awh_absence = $member_awh['absence'];
            $awh_holiday = $member_awh['holiday'];
            $awh_disease = $member_awh['disease'];
            $awh_extraholiday = $AWH->getExtraHoliday($date, $user_id);
            $vse_otsutstviya = $awh_absence + $awh_new_absence + $awh_holiday + $awh_disease + $awh_extraholiday;
            $working_days_month = $PC->get()["working_days"];
            if ($member->getTransportFormula() == 1) {
                if ($TR->isAuto($date, 1, $user_id) == true) {
                    $TR->setSumm($member->getTransport());
                    $TR->recountCompensationFAL($vse_otsutstviya, $working_days_month);
                }
            }
            if ($member->getMobileFormula() == 1) {
                if ($MB->isAuto($date, 1, $user_id)==true) {
                    $MB->setSumm($member->getMobileCommunication());
                    $MB->recountMobile($vse_otsutstviya, $working_days_month);
                }
            }

//            $PR->fetch($user_id, true);

            if ($Status > 1 && $Status < 5) {
                try {
                    $PR->fetch($user_id, true);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }

//                echo "НДФЛ: ".$_POST['NDFL']."<BR>";
                if ($_POST['NDFL']) {
                    $PR->setNdfl($_POST['NDFL']);
                }
                /*
                * Создаю объект интерфейса ведомости
                */
                $PI = new PayRollInterface();
                // Создаю объект ведомости и заполняю его
                $PayRoll = new PayRoll();
                $PayRoll->setDate($date); // Дата

                if ($member->getWorkstatus() == 1) {
                    $PayRoll->setTypeOfPayment(1); // Тип выплаты
                } else {
                    $PayRoll->setTypeOfPayment(4); // Тип выплаты Расчет
                }

                $PayRoll->setId($member->getId()); // Пользовательский ID

                $PayRoll->setDirection($member->getDirection()); // Напраление
                $PayRoll->setDepartment($member->getDepartment()); // Отдел
                $PayRoll->setPosition($member->getPosition()); // Должность


                $PayRoll->setFio($member->getLastname() . " " . $member->getName() . " " . $member->getMiddle()); // ФИО
                $PayRoll->setSalary($member->getSalary()); // Оклад

                $PayRoll->setNDFL($PR->getNdfl()); // НДФЛ
                $PayRoll->setRO($PR->getRo()); // ИЛ

//            $PayRoll->setAdvancePayment($All_Salary); // АВАНС
                $PayRoll->setAdvancePayment($PR->getAllSalary()); // АВАНС

                $PayRoll->setAwhdata(["absence" => ($PR->getAbsence() + $PR->getNewAbsence()), "holiday" => $PR->getHoliday(), "disease" => $PR->getDisease()]);
                $PayRoll->setAdministrativeData($PR->getAB_Data());
                $PayRoll->setRetentionData($PR->getRN_Data());
                $PayRoll->setCorrectionData($PR->getCR_Data());
//                var_dump($PR->getCR_Data()); echo "<BR><BR>";
                $PayRoll->setTransport($PR->getAllTransport());
                $PayRoll->setMobile($PR->getAllMobile());

                $PayRoll->setAbsences($PR->getAllAbsenceRub()); // Отсутствия
                if ($member->getMotivation() == 7) {
                    $PayRoll->setCommercial(0); // Коммерческая премия
                } else {
                    $PayRoll->setCommercial($PR->getAllMarkupRubles()); // Коммерческая премия
                }

                $PayRoll->setAdministrative($PR->getAllAdministrativeRubles()); // Административная премия
                $PayRoll->setHold($PR->getAllHoldRubles()); // Удержания
                $PayRoll->setCorrecting($PR->getAllCorrectionRubles()); // Корректировки
//                echo $PR->getAllCorrectionRubles()."<BR><BR>";
                $PayRoll->setKPI($PR->getKPISumm());

                $PayRoll->setYearbonusPay($PR->getYearBonusPay());

                $PayRoll->setBaseprocentId($PR->getBasepercentId());
                $PayRoll->setAdvancedProcentId($PR->getAdvancedPercentId());
//                echo $PR->getCommercialJson() . "<BR>"; exit(0);
                $PayRoll->setData($PR->getCommercialJson());

                $PayRoll->setMotivation($member->getMotivation());
                $PayRoll->Calculation();

//                echo $PayRoll->getCorrecting()."<BR><BR>";
//                exit(255);

                //Добавляю в массив объектов
                $PayRollArray[] = $PayRoll;

                foreach ($PayRollArray as $item) {
                    try {
                        $item->setSumm($item->getSumm());
                        $PI->UpdateFinancialPayout($item);
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }
                }
            }

            header('Location: '.$_SERVER['REQUEST_URI']);
            die();
        } // Можно править только УРВ
        else if (isset($_REQUEST['date']) && isset($_POST['SendForm'])) {
            if (isset($_POST['Holiday'])) {
                $_POST['Holiday'] = str_replace(",", ".", $_POST['Holiday']);
                if (is_numeric($_POST['Holiday']) && $_POST['Holiday'] != $PR->getHoliday()) {
                    try {

                        $AWH->setHolidayByMember($user_id, $date, 1, $_POST['Holiday']);
                        $PR->setHoliday($_POST['Holiday']);

                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            if (isset($_POST['Disease'])) {
                $_POST['Disease'] = str_replace(",", ".", $_POST['Disease']);
                if (is_numeric($_POST['Disease']) && $_POST['Disease'] != $PR->getDisease()) {
                    try {
                        $AWH->setDiseaseByMember($user_id, $date, 1, $_POST['Disease']);
                        $PR->getDisease($_POST['Disease']);
                    } catch (Exception $e) {
                        $Error->add($e->getMessage());
                    }
                }
            }

            header('Location: '.$_SERVER['REQUEST_URI']);
            die();
        }
    }

    if (isset($_POST['rm_statment'])) {
        if ($MI->checkActivePayout($member->getId(), $date, 1)) {
            $MI->disableInPayout($member->getId(), $date, 1);
        } else {
            $MI->enableInPayout($member->getId(), $date, 1);
        }
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Премия сотрудника</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <?php
            if ($member->getWorkstatus() == 1) {
                ?>
                <h1>Расчет премии</h1>
                <?php
            } else {
                ?>
                <h1>Расчет сотрудника</h1>
                <?php
            }
            ?>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Список пользователей</a></li>
                <li><a href="profile.php?id=<?php echo $user_id; ?>">Профиль сотрудника</a></li>
                <?php
                if ($member->getWorkstatus() == 1) {
                    ?>
                    <li class="active">Расчет премии</li>
                    <?php
                } else {
                    ?>
                    <li class="active">Расчет сотрудника</li>
                    <?php
                }
                ?>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php
            /*
                 * Вывод информации об ощибках
                 */
            if ($Error->getCount() > 0) {
                ?>
                <div class="box box-solid box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ошибки</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="TableErr">
                            <thead>
                            <th>Сообщение</th>
                            </thead>
                            <tbody>
                            <?php
                            foreach (array_unique($Error->getErrors()) as $value) {
                                echo "<tr><td>" . $value . "</td><td></td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <?php
            }
            ?>

            <div class="row">
                <div class="col-md-12">

                    <div class="box box-solid">
                        <div class="box-header">
                            <h3>
                                <?php
                                // Вывожу ФИО
                                if ($member->getMaidenName()) {
                                    echo $member->getLastName() . " ( " . $member->getMaidenName() . " ) " . $member->getName() . " " . $member->getMiddle();
                                } else {
                                    echo $member->getLastName() . " " . $member->getName() . " " . $member->getMiddle();
                                };
                                ?>
                            </h3>
                        </div>
                        <div class="box-body">
                            <form class="form-horizontal" method="post">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Выберите дату: </label>
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="datepicker"
                                                   name="date" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                                   value="<?php echo $date; ?>" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-2"><input type="submit" class="btn btn-flat btn-default"
                                                                 name="SendDate" value="Получить данные"></div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <?php
//                    if (($member->getWorkstatus() == 1 && $Status < 5) || $member->getWorkstatus()!=1 ){
                        if (isset($_REQUEST['date']) || isset($_COOKIE['premium_date'])) {
                    ?>
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <?php if (($Permission_ShowAdvanceCalculation == 1) || ($User->getMemberId() == $user_id)) { ?>
                                <li class="active"><a href="#tab_0" onclick="document.cookie='PremiumTab=0';"
                                                      data-toggle="tab" aria-expanded="true">Предварительный расчет</a>
                                </li>
                                <?php
                            };
                            ?>
                            <?php
                            // В зависимости от типа мотивации вывожу нужные табы.
                            if ($member->getMotivation() != 6) {
                                if (($Permission_ShowCommercialBlock == 1) || ($User->getMemberId() == $user_id)) {
                                    ?>
                                    <li><a href="#tab_1" onclick="document.cookie='PremiumTab=1';" data-toggle="tab"
                                           aria-expanded="false">Коммерческий блок</a></li>

                                    <li><a href="#tab_7" onclick="document.cookie='PremiumTab=7';" data-toggle="tab"
                                           aria-expanded="false">KPI</a></li>
                                    <?php
                                }
                            ;

                                if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                    ?>
                                    <li><a href="#tab_2" onclick="document.cookie='PremiumTab=2';" data-toggle="tab"
                                           aria-expanded="false">Административный блок</a></li>
                                    <?php
                                };
                            } else {
                                if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                    ?>
                                    <li><a href="#tab_2" onclick="document.cookie='PremiumTab=2';" data-toggle="tab"
                                           aria-expanded="false">Административный блок</a></li>
                                    <?php
                                }
                            }

                            if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <li><a href="#tab_8" onclick="document.cookie='PremiumTab=8';" data-toggle="tab"
                                       aria-expanded="false">ГСМ</a></li>
                                <?php
                            } else {
                                if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                    ?>
                                    <li><a href="#tab_8" onclick="document.cookie='PremiumTab=8';" data-toggle="tab"
                                           aria-expanded="false">ГСМ</a></li>
                                    <?php
                                }
                            }

                            if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <li><a href="#tab_9" onclick="document.cookie='PremiumTab=9';" data-toggle="tab"
                                       aria-expanded="false">Мобильная связь</a></li>
                                <?php
                            } else {
                                if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                    ?>
                                    <li><a href="#tab_9" onclick="document.cookie='PremiumTab=9';" data-toggle="tab"
                                           aria-expanded="false">Мобильная связь</a></li>
                                    <?php
                                }
                            }
                            ?>

                            <?php
                            if (($Permission_ShowRetitiotionBlock == 1) || ($User->getMemberId() == $user_id)) { ?>
                                <li><a href="#tab_3" onclick="document.cookie='PremiumTab=3';" data-toggle="tab"
                                       aria-expanded="false">Удержания</a></li>
                            <?php };
                            if (($Permission_ShowCorrectingBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <li><a href="#tab_4" onclick="document.cookie='PremiumTab=4';" data-toggle="tab"
                                       aria-expanded="false">Корректировки</a></li>
                                <?php
                            };
                            if (($Permission_ShowAWHBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <li><a href="#tab_5" onclick="document.cookie='PremiumTab=5';" data-toggle="tab"
                                       aria-expanded="false">УРВ</a></li>
                                <?php
                            }
                            ?>
                            <?php
                            if ($Permission_ShowCommercialBlock || $Permission_ShowAdministrativeBlock || $Permission_ShowRetitiotionBlock || $Permission_ShowCorrectingBlock) {
                                ?>
                                <li><a href="#tab_6" onclick="document.cookie='PremiumTab=6';" data-toggle="tab"
                                       aria-expanded="false">История</a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>

                    <form method="post" id="form-data" class="form-horizontal">
                        <div class="tab-content">

                            <input type="hidden" name="date" value="<?php echo $date; ?>">
                            <?php
                            if (($member->getMotivation() != 6) && (($Permission_ShowCommercialBlock == 1) || ($User->getMemberId() == $user_id))) {
                                ?>
                                <div class="tab-pane" id="tab_1">
                                    <div class="box box-info box-solid">

                                        <div class="box-header">
                                            <?php
                                            if ($older_format) {
                                            ?>
                                            <div><h4 class="pull-left">1 у.е. = 25 руб</h4></div>
                                                <?php
                                            } else{
                                                ?>
                                                <div><h4 class="pull-left" style="text-transform: uppercase">Внимание! С 01.05.2021 все данные заносяится в рублях!</h4></div>
                                                <?php
                                            }
                                            ?>
                                        </div>

                                        <div class="box-body">
                                            <?php
                                            // Если мотивации == 2 или 3, то вывожу корректирующий коэффициент
                                            if (($PR->getMotivation() == 3) || ($PR->getMotivation() == 2)) {
                                                ?>
                                                <div class="box box-solid box-danger box-pane">
                                                    <div class="box-body">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">Корректирующий
                                                                коэффициент</label>
                                                            <div class="col-sm-8">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon"><b>&alpha;</b></div>
                                                                    <input type="text"
                                                                           name="CommercialCorrectingProcent"
                                                                           id="CommercialCorrectingProcent"
                                                                           placeholder="1"
                                                                           value="<?php echo $PR->getCommercialProcent(); ?>"
                                                                           class="form-control pull-right" <?php if (($Permission_CommercialBlockProcent == 0) || ($PR->getPaymentMade() == true)) {
                                                                        echo "disabled";
                                                                    } ?> >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php
                                            if ($member->getMotivation() == 1) {
                                                $manager_nacenka = 0;
                                                $manager_summ_ye = 0;
                                                $bonus_summ = 0;
                                                $correct_summ = 0;
                                                $summ_nachenka_ye = 0;
                                                $nacenka_by_tt = [];
                                                $nacenka_narugka = 0;
                                                $nacenka_garazka = 0;

                                                echo '<table class="table table - striped table - hover table - bordered">';
                                                echo "<thead>";
                                                if ($older_format)
                                                    echo "<th class=\"col-md-2\">Статгруппа</th>";
                                                echo "<th class=\"col-md-1\"> ТТ</th>";

                                                echo "<th class=\"col-md-1\"> Наценка </th>";
                                                echo "<th class=\"col-md-3 text-center\"> Бонусы клиентам ".($older_format?"(-у.е.)":"<b style='color:#ac0000'>(-руб.)</b>")."</th>";
                                                echo "<th class=\"col-md-3 text-center\"> Корректировка наценки ".($older_format?"(+/-у.е.)":"<b style='color:#ac0000'>(+/-руб.)</b>")."</th>";
                                                echo "<th> &sum; ".($older_format?"(у.е.)":"<b style='color:#ac0000'>(руб.)</b>")."</th>";
                                                echo "<th> Процент(%)</th>";

                                                echo "<th ".(!$older_format?"style='display:none'":"")."> Итого(у.е.)</th>";

                                                echo "<th> Итого(руб.)</th>";
                                                echo "</thead>";
                                                echo "<tbody>";
                                                // Получаю данные по коммерческому блоку.
                                                foreach ($PR->getCommercialPartManager() as $item) {
                                                    echo "<tr>";

                                                    // *************************************************** Строю строки таблицы
                                                    // В зависимости от мотивации вывожу значения строк
                                                    if ($older_format)
                                                        echo "<td>" . $item['statgroup'] . "</td>";
                                                    echo "<td>" . $item['department_name'] . " (".$item['department'].") " . "</td>";

                                                    // Вывожу наценку
                                                    $manager_nacenka += $item['profit'];
                                                    $summ_nachenka_ye += $item['preliminary_sum'];
                                                    $correct_summ += $item['correct'];
                                                    $bonus_summ += $item['bonus'];
                                                    $nacenka_by_tt[$item['department_name']] += $item['profit'];

                                                    if ($older_format)
                                                        if ($item['statgroup_id']<=13) {
                                                            $nacenka_garazka += $item['profit'];
                                                        } else {
                                                            $nacenka_narugka += $item['profit'];
                                                        }

                                                    echo "<td><div id='profit_" . $item['department'] . "' >" . number_format($item['profit'], 2, ',', " ") . "</div></td>";
                                                    echo "<td>";
                                                    echo "<input type=hidden name='correcting[" . $item['department'] . ($item['service_id']?"_".$item['service_id']:""). "][service_id]' value='".$item['service_id']."'>";

                                                    echo "<div class='input-group' style=\"border:1px solid #a94442;\">";
                                                    echo "<div class='input-group-addon'><i class='fa fa-dollar'></i></div>";

                                                    echo "<input type='text' 
                                                    class='form-control' 
                                                    name='correcting[" . $item['department'] .($item['service_id']?"_".$item['service_id']:"") . "][0]' 
                                                    id='bonus_" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"") . "' 
                                                    placeholder='0,00' 
                                                    title='" . $item['bonus_note'] . "' 
                                                    value='" . ((is_null($item['bonus']) && empty($item['bonus'])) ?"": number_format($item['bonus'], 2, ',', '')) . "'
                                                    ". (((($Permission_CommercialBlockBonus == 1) || ($PR->getPaymentMade() == 1)) && $PR->getCommercialLock() == 0)?"":" disabled") ."
                                                    >";

                                                    echo "<div class='input-group-addon btn btn-flat' name='bonus' id='btn_comment_" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"")  . "'><i class='fa fa-comment'></i></div>";

                                                    echo "<textarea 
                                                    name='correcting[" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"") . "][1]' 
                                                    id='bonus_note_" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"") . "' 
                                                    cols='1' 
                                                    rows='1' 
                                                    class='form-control'
                                                    ". (((($Permission_CommercialBlockBonus == 1) || ($PR->getPaymentMade() == 1)) && $PR->getCommercialLock() == 0)?"":" disabled") ."
                                                    >" . $item['bonus_note'] . "</textarea>";

                                                    echo "</div>";
                                                    echo "</td>";

                                                    echo "<td>";
                                                    echo "<div class='input-group'  style=\"border:1px solid #3c763d;\">";
                                                    echo "<div class='input-group-addon'><i class='fa fa-dollar'></i></div>";

                                                    echo "<input 
                                                    type='text' 
                                                    class='form-control' 
                                                    name='correcting[" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"") . "][2]' 
                                                    id='correcting_" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"") . "' 
                                                    placeholder='0,00' title='" . $item['correct_note'] . "' 
                                                    value='" . ((is_null($item['correct']) && empty($item['correct'])) ?"": number_format($item['correct'], 2, ',', '')) . "'
                                                    ". ( ($Permission_CommercialBlockNachenka == 1 && $PR->getCommercialLock() == 0) ? "" : " disabled" ) ."
                                                    >";

                                                    echo "<div class='input-group-addon btn btn-flat' name='correcting' id='btn_comment_" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"")  . "'><i class='fa fa-comment'></i></div>";

                                                    echo "<textarea 
                                                    name='correcting[" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"") . "][3]' 
                                                    id='correcting_note_" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"") . "' 
                                                    cols='1' 
                                                    rows='1' 
                                                    class='form-control'
                                                    ". ( ($Permission_CommercialBlockNachenka == 1 && $PR->getCommercialLock() == 0) ? "" : " disabled" ) ."
                                                    >" . $item['correct_note'] . "</textarea>";

                                                    echo "</div>";
                                                    echo "</td>";
                                                    // Вывожу сумму НАЦЕНКА - БОНУС + КОРРЕКТИРОВКА
                                                    if (isset($item['preliminary_sum'])) {
                                                        echo "<td><div id='nac_summ_" . $item['department'] . "'>" . number_format($item['preliminary_sum'], 2, ',', ' ') . "</div></td>";
                                                    } else {
                                                        echo "<td><div id='nac_summ_" . $item['department'] . "'></div></td>";
                                                    }
                                                    // Вывожу сумму процент
                                                    if (isset($item['procent'])) {
                                                        echo "<td><div id='procent_" . $item['department'] . "' >" .  number_format($item['procent'], 2, ',', ' ') . "</div></td>";
                                                    } else {
                                                        echo "<td><div id='procent_" . $item['department'] . "' ></div></td>";
                                                    }
                                                    // Вывожу итого в у.е. (НАЦЕНКА - БОНУС + КОРРЕКТИРОВКА) * ПРОЦЕНТ
                                                    if (isset($item['sum']))
                                                        $manager_summ_ye += $item['sum'];

                                                    if ($older_format) {
                                                        if (isset($item['sum'])) {
                                                            echo "<td><div id='summ_" . $item['department'] . "' >" . number_format($item['sum'], 2, ',', ' ') . "</div></td>";
                                                        } else {
                                                            echo "<td><div id='summ_" . $item['department'] . "' ></div></td>";
                                                        }
                                                    }
                                                    // Перевожу в рубли
                                                    echo "<td><div id='rubsumm_" . $item['department'] . "' > " . ($older_format?number_format(($item['sum'] * 25), 2, ',', ' '):number_format(($item['sum']), 2, ',', ' ')) . "</div></td>";
                                                    echo "</tr>";
                                                }
                                                echo "</tbody>";
                                                echo "<tfoot>";

                                                if ($older_format) {
                                                    foreach ($nacenka_by_tt as $tt => $value) {
                                                        echo "<tr style='background-color: #ebf4ff'><td>" . $tt . "</td><td></td><td>" . number_format($value, 2, ',', ' ') . "</td><td colspan='6'></td></tr>";
                                                    }
                                                }

                                                if ($older_format) {
                                                    echo "<tr style='background-color: #fafafa'><td colspan='2'>Наценка гаражка</td>";
                                                    echo "<td>" . number_format($nacenka_garazka, 2, ',', ' ') . "</td>";
                                                    echo "<td colspan='6'></td></tr>";

                                                    echo "<tr style='background-color: #fafafa'><td colspan='2'>Наценка наружка</td>";
                                                    echo "<td>" . number_format($nacenka_narugka, 2, ',', ' ') . "</td><td colspan='6'></td></tr>";
                                                }
                                                echo "<tr>";
                                                if ($older_format)
                                                    echo "<th colspan='2'>";
                                                else
                                                    echo "<th colspan='1'>";
                                                echo "Итого</th><th >" . number_format($manager_nacenka, 2, ',', ' ') . "</th>";
                                                echo "<th id='bonus_summ' style='text-align: right'>" . number_format($bonus_summ, 2, ',', ' ') . "</th>";
                                                echo "<th id='correct_summ' style='text-align: right'>" . number_format($correct_summ, 2, ',', ' ') . "</th>";
                                                echo "<th id='summ_nacenka'>" . number_format($summ_nachenka_ye, 2, ',', ' ') . "</th>";
                                                echo "<th></th>";


                                                if ($older_format)
                                                    echo "<th><div id='final_summ'>" . number_format($manager_summ_ye, 2, ',', ' ') . "</div></th>";
                                                if ($older_format)
                                                    echo "<th><div id='final_rubsumm'>" . number_format(($manager_summ_ye * 25), 2, ',', ' ') . "</div></th>";
                                                else
                                                    echo "<th><div id='final_rubsumm'>" . number_format(($manager_summ_ye), 2, ',', ' ') . "</div></th>";
                                                echo "</tfoot>";
                                                echo "</table>";
                                            }

                                            if ($member->getMotivation() == 8) {
                                                $manager_nacenka = 0;
                                                $manager_summ_ye = 0;
                                                $bonus_summ = 0;
                                                $correct_summ = 0;
                                                $summ_nachenka_ye = 0;

                                                echo '<table class="table table - striped table - hover table - bordered">';
                                                echo "<thead>";
                                                if ($older_format)
                                                    echo "<th class=\"col-md-2\">Статгруппа</th>";

                                                echo "<th class=\"col-md-1\"> ТТ</th>";
                                                echo "<th class=\"col-md-1\"> Наценка".($older_format?"(у.е.)":"<b style='color:#ac0000'>(руб.)</b>")."</th>";
                                                echo "<th> &sum;".($older_format?"(у.е.)":"<b style='color:#ac0000'>(руб.)</b>")."</th>";
                                                echo "<th> Процент(%)</th>";

                                                if ($older_format)
                                                    echo "<th> Итого".($older_format?"(у.е.)":"(руб.)")."</th>";

                                                echo "<th> Итого(руб.)</th>";
                                                echo "</thead>";
                                                echo "<tbody>";
                                                // Получаю данные по коммерческому блоку.
                                                foreach ($PR->getCommercialPartManager() as $item) {
                                                    echo "<tr>";

                                                    // *************************************************** Строю строки таблицы
                                                    // В зависимости от мотивации вывожу значения строк
                                                    if ($older_format)
                                                        echo "<td>" . $item['date'] . "</td>";
                                                    echo "<td>" . $item['department_name'] . "</td>";

                                                    // Вывожу наценку
                                                    $manager_nacenka += $item['profit'];
                                                    $summ_nachenka_ye += $item['preliminary_sum'];
                                                    $correct_summ += $item['correct'];
                                                    $bonus_summ += $item['bonus'];

                                                    echo "<td><div id='profit_" . $item['department'] . "' >" . number_format($item['profit'],2, ',', ' ') . "</div></td>";
                                                    // Вывожу сумму НАЦЕНКА - БОНУС + КОРРЕКТИРОВКА
                                                    if (isset($item['preliminary_sum'])) {
                                                        echo "<td><div id='nac_summ_" . $item['department'] . "'>" . number_format($item['preliminary_sum'],2, ',', ' ') . "</div></td>";
                                                    } else {
                                                        echo "<td><div id='nac_summ_" . $item['department'] . "'></div></td>";
                                                    }
                                                    // Вывожу сумму процент
                                                    if (isset($item['procent'])) {
                                                        echo "<td><div id='procent_" . $item['department'] . "' >" . number_format($item['procent'],2, ',', ' ') . "</div></td>";
                                                    } else {
                                                        echo "<td><div id='procent_" . $item['department'] . "' ></div></td>";
                                                    }
                                                    // Вывожу итого в у.е. (НАЦЕНКА - БОНУС + КОРРЕКТИРОВКА) * ПРОЦЕНТ
                                                    if (isset($item['sum']))
                                                        $manager_summ_ye += $item['sum'];

                                                    if ($older_format) {
                                                        if (isset($item['sum'])) {
                                                            echo "<td><div id='summ_" . $item['department'] . "' >" . number_format($item['sum'], 2, ',', ' ') . "</div></td>";
                                                        } else {
                                                            echo "<td><div id='summ_" . $item['department'] . "' ></div></td>";
                                                        }
                                                    }

                                                    // Перевожу в рубли
                                                    if ($older_format)
                                                        echo "<td><div id='rubsumm_" . $item['department'] . "' >" . number_format(($item['sum'] * 25),2, ',', ' ') . "</div></td>";
                                                    else
                                                        echo "<td><div id='rubsumm_" . $item['department'] . "' >" . number_format(($item['sum']),2, ',', ' ') . "</div></td>";
                                                    echo "</tr>";
                                                }
                                                echo "</tbody>";
                                                echo "<tfoot>";
                                                if ($older_format)
                                                    echo "<th colspan='2'>Итого</th>";
                                                else
                                                    echo "<th>Итого</th>";
                                                echo "<th>" . number_format($manager_nacenka,2, ',', ' ') . "</th>";
                                                echo "<th id='summ_nacenka'>" . number_format($summ_nachenka_ye,2, ',', ' ') . "</th>";
                                                echo "<th></th>";

                                                if ($older_format)
                                                    echo "<th><div id='final_summ'>" . number_format($manager_summ_ye,2, ',', ' ') . "</div></th>";

                                                if ($older_format)
                                                    echo "<th><div id='final_rubsumm'>" . number_format(($manager_summ_ye * 25),2, ',', ' ') . "</div></th>";
                                                else
                                                    echo "<th><div id='final_rubsumm'>" . number_format(($manager_summ_ye),2, ',', ' ') . "</div></th>";

                                                echo "</tfoot>";
                                                echo "</table>";
                                            }

                                            if ($member->getMotivation() == 9) {
                                                $manager_nacenka = 0;
                                                $manager_summ_ye = 0;
                                                $bonus_summ = 0;
                                                $correct_summ = 0;
                                                $summ_nachenka_ye = 0;

                                                echo '<table class="table table - striped table - hover table - bordered">';
                                                echo "<thead>";

                                                echo "<th class=\"col-md-1\"> Сервис менеджер</th>";
                                                echo "<th> Наценка <b style='color:#ac0000'>(руб.)</b></th>";
                                                echo "<th> &sum;(руб.)</b></th>";
                                                echo "<th> Процент(%)</th>";

                                                if ($older_format)
                                                    echo "<th> Итого(руб.)</th>";

                                                echo "<th> Итого(руб.)</th>";
                                                echo "</thead>";
                                                echo "<tbody>";
                                                // Получаю данные по коммерческому блоку.
                                                foreach ($PR->getCommercialPartManager() as $item) {
                                                    echo "<tr>";

                                                    // *************************************************** Строю строки таблицы
                                                    // В зависимости от мотивации вывожу значения строк
                                                    if ($older_format)
                                                        echo "<td>" . $item['date'] . "</td>";
                                                    echo "<td>" . $item['department_name'] . "</td>";

                                                    // Вывожу наценку
                                                    $manager_nacenka += $item['profit'];
                                                    $summ_nachenka_ye += $item['preliminary_sum'];
                                                    $correct_summ += $item['correct'];
                                                    $bonus_summ += $item['bonus'];

                                                    echo "<td><div id='profit_" . $item['department'] . "' >" . number_format($item['profit'],2, ',', ' ') . "</div></td>";
                                                    // Вывожу сумму НАЦЕНКА - БОНУС + КОРРЕКТИРОВКА
                                                    if (isset($item['preliminary_sum'])) {
                                                        echo "<td><div id='nac_summ_" . $item['department'] . "'>" . number_format($item['preliminary_sum'],2, ',', ' ') . "</div></td>";
                                                    } else {
                                                        echo "<td><div id='nac_summ_" . $item['department'] . "'></div></td>";
                                                    }
                                                    // Вывожу сумму процент
                                                    if (isset($item['procent'])) {
                                                        echo "<td><div id='procent_" . $item['department'] . "' >" . number_format($item['procent'],2, ',', ' ') . "</div></td>";
                                                    } else {
                                                        echo "<td><div id='procent_" . $item['department'] . "' ></div></td>";
                                                    }
                                                    // Вывожу итого в у.е. (НАЦЕНКА - БОНУС + КОРРЕКТИРОВКА) * ПРОЦЕНТ
                                                    if (isset($item['sum']))
                                                        $manager_summ_ye += $item['sum'];

                                                    if ($older_format) {
                                                        if (isset($item['sum'])) {
                                                            echo "<td><div id='summ_" . $item['department'] . "' >" . number_format($item['sum'], 2, ',', ' ') . "</div></td>";
                                                        } else {
                                                            echo "<td><div id='summ_" . $item['department'] . "' ></div></td>";
                                                        }
                                                    }

                                                    // Перевожу в рубли
                                                    if ($older_format)
                                                        echo "<td><div id='rubsumm_" . $item['department'] . "' >" . number_format(($item['sum'] * 25),2, ',', ' ') . "</div></td>";
                                                    else
                                                        echo "<td><div id='rubsumm_" . $item['department'] . "' >" . number_format(($item['sum']),2, ',', ' ') . "</div></td>";
                                                    echo "</tr>";
                                                }
                                                echo "</tbody>";
                                                echo "<tfoot>";
                                                if ($older_format)
                                                    echo "<th colspan='2'>Итого</th>";
                                                else
                                                    echo "<th>Итого</th>";
                                                echo "<th>" . number_format($manager_nacenka,2, ',', ' ') . "</th>";
                                                echo "<th id='summ_nacenka'>" . number_format($summ_nachenka_ye,2, ',', ' ') . "</th>";
                                                echo "<th></th>";

                                                if ($older_format)
                                                    echo "<th><div id='final_summ'>" . number_format($manager_summ_ye,2, ',', ' ') . "</div></th>";

                                                if ($older_format)
                                                    echo "<th><div id='final_rubsumm'>" . number_format(($manager_summ_ye * 25),2, ',', ' ') . "</div></th>";
                                                else
                                                    echo "<th><div id='final_rubsumm'>" . number_format(($manager_summ_ye),2, ',', ' ') . "</div></th>";

                                                echo "</tfoot>";
                                                echo "</table>";
                                            }

                                            if ($member->getMotivation() == 2 || $member->getMotivation() == 3) {
                                                $bonus_summ = 0;
                                                $correct_summ = 0;
                                                $summ_nachenka_ye = 0;

                                                echo '<table class="table table - striped table - hover table - bordered">';
                                                echo "<thead>";
                                                echo "<th class=\"col-md-2\"> ТТ</th>";
                                                echo "<th class=\"col-md-1\"> Наценка".($older_format?"(у.е.)":"<b style='color:#ac0000'>(руб.)</b>")."</th>";
                                                echo "<th class=\"col-md-3 text-center\"> Бонусы клиентам".($older_format?" (- у.е.)":"<b style='color:#ac0000'>(- руб.)</b>")."</th>";
                                                echo "<th class=\"col-md-3 text-center\"> Корректировка наценки".($older_format?" (+/- у.е.)":"<b style='color:#ac0000'>(+/- руб.)</b>")."</th>";
                                                echo "<th> &sum;".($older_format?"(у.е.)":"(руб.)")."</th>";
                                                echo "<th> Процент(%)</th>";
                                                echo "<th ".(!$older_format?"style='display:none'":"")."> Итого(у.е.)</th>";
                                                echo "<th> Итого(руб.)</th>";
                                                echo "</thead>";
                                                echo "<tbody>";
                                                // Получаю данные по коммерческому блоку.
                                                foreach ($PR->getCommercialPartTT() as $item) {
                                                    $correct_summ += $item['correct'];
                                                    $bonus_summ += $item['bonus'];
                                                    $summ_nachenka_ye += $item['preliminary_sum'];

                                                    echo "<tr>";
                                                    // *************************************************** Строю строки таблицы
                                                    // В зависимости от мотивации вывожу значения строк
                                                    echo "<td>" . $item['department_name'] . "</td>";

                                                    // Вывожу наценку
                                                    echo "<td><div id='profit_" . $item['department'] . "' >" . number_format($item['profit'],2, ',', ' ')  . "</div></td>";
                                                    echo "<td>";

                                                    echo "<input type=hidden name='correcting[" . $item['department'] . ($item['service_id']?"_".$item['service_id']:""). "][service_id]' value='".$item['service_id']."'>";

                                                    echo "<div class='input-group' style=\"border:1px solid #a94442;\">";
                                                    echo "<div class='input-group-addon'><i class='fa fa-dollar'></i></div>";
                                                    echo "<input type='text' 
                                                    class='form-control' 
                                                    name='correcting[" . $item['department'] .($item['service_id']?"_".$item['service_id']:"") . "][0]' 
                                                    id='bonus_" . $item['department'] .($item['service_id']?"_".$item['service_id']:"") . "' 
                                                    placeholder='0,00' 
                                                    title='" . $item['bonus_note'] .($item['service_id']?"_".$item['service_id']:"") . "'
                                                    value='" . ((is_null($item['bonus']) && empty($item['bonus'])) ?"": number_format($item['bonus'], 2, ',', '')) . "'' 
                                                    ". (($Permission_CommercialBlockBonus == 1 && $PR->getCommercialLock() == 0)?"":" disabled") ."
                                                    >";

                                                    echo "<div class='input-group-addon btn btn-flat' name='bonus' id='btn_comment_" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"")  . "'><i class='fa fa-comment'></i></div>";

                                                    echo "<textarea 
                                                    name='correcting[" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"") . "][1]' 
                                                    id='bonus_note_" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"") . "' 
                                                    cols='1' 
                                                    rows='1' 
                                                    class='form-control'
                                                    ". (($Permission_CommercialBlockBonus == 1 && $PR->getCommercialLock() == 0)?"":" disabled") ."
                                                    >" . $item['bonus_note'] . "</textarea>";

                                                    echo "</div>";
                                                    echo "</td>";

                                                    echo "<td>";
                                                    echo "<div class='input-group'  style=\"border:1px solid #3c763d;\">";
                                                    echo "<div class='input-group-addon'><i class='fa fa-dollar'></i></div>";

                                                    echo "<input 
                                                    type='text' 
                                                    class='form-control' 
                                                    name='correcting[" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"") . "][2]' 
                                                    id='correcting_" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"") . "' 
                                                    placeholder='0,00' 
                                                    title='" . $item['correct_note'] . "' 
                                                    value='" . ((is_null($item['correct']) && empty($item['correct'])) ?"": number_format($item['correct'], 2, ',', '')) . "''
                                                    ". (($Permission_CommercialBlockNachenka == 1 && $PR->getCommercialLock() == 0)?"":" disabled") ."
                                                    >";

                                                    echo "<div class='input-group-addon btn btn-flat' name='correcting' id='btn_comment_" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"")  . "'><i class='fa fa-comment'></i></div>";

                                                    echo "<textarea 
                                                    name='correcting[" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"") . "][3]' 
                                                    id='correcting_note_" . $item['department'] . ($item['service_id']?"_".$item['service_id']:"") . "' 
                                                    cols='1' 
                                                    rows='1' 
                                                    class='form-control'
                                                    ". (($Permission_CommercialBlockNachenka == 1 && $PR->getCommercialLock() == 0)?"":" disabled") ."
                                                    >" . $item['correct_note'] . "</textarea>";

                                                    echo "</div>";
                                                    echo "</td>";
                                                    // Вывожу сумму НАЦЕНКА - БОНУС + КОРРЕКТИРОВКА
                                                    if (isset($item['preliminary_sum'])) {
                                                        echo "<td><div id='nac_summ_" . $item['department'] . "'>" . number_format($item['preliminary_sum'],2, ',', ' ')  . "</div></td>";
                                                    } else {
                                                        echo "<td><div id='nac_summ_" . $item['department'] . "'></div></td>";
                                                    }
                                                    // Вывожу сумму процент
                                                    if (isset($item['procent'])) {
                                                        echo "<td><div id='procent_" . $item['department'] . "' >" . number_format($item['procent'],2, ',', ' ') . "</div></td>";
                                                    } else {
                                                        echo "<td><div id='procent_" . $item['department'] . "' ></div></td>";
                                                    }
                                                    // Вывожу итого в у.е. (НАЦЕНКА + БОНУС + КОРРЕКТИРОВКА) * ПРОЦЕНТ
                                                    if (isset($item['sum'])) {
                                                        echo "<td><div id='summ_" . $item['department'] . "' >" . number_format($item['sum'],2, ',', ' ') . "</div></td>";
                                                    } else {
                                                        echo "<td><div id='summ_" . $item['department'] . "' ></div></td>";
                                                    }
                                                    // Перевожу в рубли
                                                    if ($older_format)
                                                        echo "<td><div id='rubsumm_" . $item['department'] . "' >" . number_format(($item['sum'] * 25),2, ',', ' ') . "</div></td>";
                                                    else
                                                        echo "<td><div id='rubsumm_" . $item['department'] . "' >" . number_format(($item['sum']),2, ',', ' ') . "</div></td>";
                                                    echo "</tr>";
                                                }
                                                echo "</tbody>";
                                                echo "<tfoot>";
                                                echo "<th>Итого</th><th>" . number_format($PR->getSummNachenka(),2, ',', ' ') . "</th>";
                                                echo "<th id='bonus_summ' style='text-align: right'>" . number_format($bonus_summ,2, ',', ' ') . "</th>";
                                                echo "<th id='correct_summ' style='text-align: right'>" . number_format($correct_summ,2, ',', ' ') . "</th>";
                                                echo "<th id='summ_nacenka'>" . number_format($summ_nachenka_ye,2, ',', ' ') . "</th>";
                                                echo "<th></th>";
                                                echo "<th><div id='final_summ'>" . number_format($PR->getFullsumm(),2, ',', ' ') . "</div></th>";
                                                echo "<th><div id='final_rubsumm'>" . number_format($PR->getAllMarkupRubles(),2, ',', ' ') . "</div></th>";
                                                echo "</tfoot>";
                                                echo "</table>";
                                            }

                                            if ($member->getMotivation() == 4 || $member->getMotivation() == 10) {
                                                $bonus_summ = 0;
                                                $correct_summ = 0;
                                                $summ_nachenka_ye = 0;

                                                echo '<table class="table table - striped table - hover table - bordered">';
                                                echo "<thead>";
                                                if ($older_format)
                                                    echo "<th class=\"col-md-2\"> Статгруппа</th>";
                                                else
                                                    echo "<th class=\"col-md-2\"> ТТ</th>";
                                                echo "<th class=\"col-md-1\"> Наценка ".($older_format?"(у.е.)":"<b style='color:#ac0000'>(руб.)</b>")."</th>";
                                                echo "<th class=\"col-md-3 text-center\"> Бонусы клиентам ".($older_format?"(-у.е.)":"<b style='color:#ac0000'>(-руб.)</b>")."</th>";
                                                echo "<th class=\"col-md-3 text-center\"> Корректировка наценки ".($older_format?"(+/- у.е.)":"<b style='color:#ac0000'>(+/- руб.)</b>")."</th>";
                                                echo "<th> &sum;".($older_format?"(у.е.)": "<b style='color:#ac0000'>(руб.)</b>")."</th>";
                                                echo "<th> Процент(%)</th>";
                                                echo "<th ".(!$older_format?"style='display:none'":"")."> Итого(у.е.)</th>";
                                                echo "<th> Итого(руб.)</th>";
                                                echo "</thead>";
                                                echo "<tbody>";
                                                // Получаю данные по коммерческому блоку.
                                                foreach ($PR->getCommercialPartStatGroup() as $item) {
                                                    $correct_summ += $item['correct'];
                                                    $bonus_summ += $item['bonus'];
                                                    $summ_nachenka_ye += $item['profit'];
                                                    $summ_nachenka_ye_full += $item['preliminary_sum'];
                                                    $summ_rur += $item['sum'];

                                                    echo "<tr>";

                                                    // *************************************************** Строю строки таблицы
                                                    // В зависимости от мотивации вывожу значения строк
                                                    echo "<td>" . $item['department_name'] . "</td>";

                                                    // Вывожу наценку
                                                    echo "<td><div id='profit_" . $item['department'] . "' >" . number_format($item['profit'],2, ',', ' ') . "</div></td>";
                                                    echo "<td>";
                                                    echo "<div class='input-group' style=\"border:1px solid #a94442;\">";
                                                    echo "<div class='input-group-addon'><i class='fa fa-dollar'></i></div>";

                                                    echo "<input 
                                                    type='text' 
                                                    class='form-control' 
                                                    name='correcting[" . $item['department'] . "][0]' 
                                                    id='bonus_" . $item['department'] . "' 
                                                    placeholder='0' 
                                                    title='" . $item['bonus_note'] . "' 
                                                    value='" . ((is_null($item['bonus']) && empty($item['bonus'])) ?"": number_format($item['bonus'], 2, ',', '')) . "''
                                                    ". (($Permission_CommercialBlockBonus == 1 && $PR->getCommercialLock() == 0)?"":" disabled") ."
                                                    >";

                                                    echo "<div class='input-group-addon btn btn-flat' name='bonus' id='btn_comment_" . $item['department'] . "'><i class='fa fa-comment'></i></div>";

                                                    echo "<textarea 
                                                    name='correcting[" . $item['department'] . "][1]' 
                                                    id='bonus_note_" . $item['department'] . "' 
                                                    cols='1' 
                                                    rows='1' 
                                                    class='form-control'
                                                    ". (($Permission_CommercialBlockBonus == 1 && $PR->getCommercialLock() == 0)?"":" disabled") ."
                                                    >" . $item['bonus_note'] . "</textarea>";
                                                    
                                                    echo "</div>";
                                                    echo "</td>";

                                                    echo "<td>";
                                                    echo "<div class='input-group'  style=\"border:1px solid #3c763d;\">";
                                                    echo "<div class='input-group-addon'><i class='fa fa-dollar'></i></div>";

                                                    echo "<input 
                                                    type='text' 
                                                    class='form-control' 
                                                    name='correcting[" . $item['department'] . "][2]' 
                                                    id='correcting_" . $item['department'] . "' 
                                                    placeholder='0' 
                                                    title='" . $item['correct_note'] . "' 
                                                    value='" . ((is_null($item['correct']) && empty($item['correct'])) ?"": number_format($item['correct'], 2, ',', '')) . "''
                                                    ". (($Permission_CommercialBlockNachenka == 1 && $PR->getCommercialLock() == 0)?"":" disabled") ."
                                                    >";

                                                    echo "<div class='input-group-addon btn btn-flat' name='correcting' id='btn_comment_" . $item[0] . "'><i class='fa fa-comment'></i></div>";

                                                    echo "<textarea 
                                                    name='correcting[" . $item['department'] . "][3]' 
                                                    id='correcting_note_" . $item['department'] . "' 
                                                    cols='1' 
                                                    rows='1' 
                                                    class='form-control' 
                                                    " . (($Permission_CommercialBlockNachenka == 1 && $PR->getCommercialLock() == 0)?"":" disabled") . "
                                                    >" . $item['correct_note'] . "</textarea>";

                                                    echo "</div>";
                                                    echo "</td>";
                                                    // Вывожу сумму НАЦЕНКА - БОНУС + КОРРЕКТИРОВКА
                                                    if (isset($item['preliminary_sum'])) {
                                                        echo "<td><div id='nac_summ_" . $item['department'] . "'>" . number_format($item['preliminary_sum'],2, ',', ' ') . "</div></td>";
                                                    } else {
                                                        echo "<td><div id='nac_summ_" . $item['department'] . "'></div></td>";
                                                    }
                                                    // Вывожу сумму процент
                                                    if (isset($item['procent'])) {
                                                        echo "<td><div id='procent_" . $item['department'] . "' >" . number_format($item['procent'],2, ',', ' ') . "</div></td>";
                                                    } else {
                                                        echo "<td><div id='procent_" . $item['department'] . "' ></div></td>";
                                                    }
                                                    // Вывожу итого в у.е. (НАЦЕНКА + БОНУС + КОРРЕКТИРОВКА) * ПРОЦЕНТ
                                                    if ($older_format) {
                                                        if (isset($item['sum'])) {
                                                            echo "<td><div id='summ_" . $item['department'] . "' >" . number_format($item['sum'], 2, ',', ' ') . "</div></td>";
                                                        } else {
                                                            echo "<td><div id='summ_" . $item['department'] . "' ></div></td>";
                                                        }
                                                    }

                                                    // Перевожу в рубли
                                                    if ($older_format)
                                                        echo "<td><div id='rubsumm_" . $item['department'] . "' >" . number_format(($item['sum'] * 25),2, ',', ' ')  . "</div></td>";
                                                    else
                                                        echo "<td><div id='rubsumm_" . $item['department'] . "' >" . number_format(($item['sum']),2, ',', ' ')  . "</div></td>";
                                                    echo "</tr>";
                                                }
                                                echo "</tbody>";
                                                echo "<tfoot>";
                                                echo "<th>Итого</th><th>" . number_format($summ_nachenka_ye,2, ',', ' ') . "</th>";
                                                echo "<th id='bonus_summ' style='text-align: right'>" . number_format($bonus_summ,2, ',', ' ') . "</th>";
                                                echo "<th id='correct_summ' style='text-align: right'>" . number_format($correct_summ,2, ',', ' ') . "</th>";
                                                echo "<th id='summ_nacenka'>" . number_format($summ_nachenka_ye_full,2, ',', ' ') . "</th>"; echo "<th></th>";
                                                if ($older_format)
                                                    echo "<th><div id='final_summ'>" . number_format($PR->getFullsumm(),2, ',', ' ') . "</div></th>";
                                                echo "<th><div id='final_rubsumm'>" . number_format($summ_rur,2, ',', ' ') . "</div></th>";
                                                echo "</tfoot>";
                                                echo "</table>";
                                            }

                                            if ($member->getMotivation() == 5 || $member->getMotivation() == 10) {
                                                $summ_rur = 0;
                                                $summ_nac = 0;
                                                echo '<table class="table table - striped table - hover table - bordered">';
                                                echo "<thead>";
                                                echo "<th class=\"col-md-3\">ФИО</th>";
                                                if ($older_format)
                                                    echo "<th class=\"col-md-3\">Статгруппа</th>";

                                                echo "<th>ТТ</th>";

                                                echo "<th> Наценка ".($older_format?"(у.е.)":"<b style='color:#ac0000'>(руб.)</b>")."</th>";
//                                                echo "<th class=\"col-md-3 text-center\"> Бонусы клиентам(- у.е.)</th>";
//                                                echo "<th class=\"col-md-3 text-center\"> Корректировка наценки(+/- у.е.)</th>";
                                                echo "<th> Процент(%)</th>";
                                                if ($older_format)
                                                    echo "<th> Итого(у.е.)</th>";

                                                echo "<th> Итого(руб.)</th>";
                                                echo "</thead>";
                                                echo "<tbody>";
                                                // Получаю данные по коммерческому блоку.
                                                foreach ($PR->getCommercialPartService() as $item) {
                                                    $summ_rur += $item['sum'];
                                                    $summ_nac += $item['profit'];
                                                    echo "<tr>";

                                                    // *************************************************** Строю строки таблицы
                                                    // В зависимости от мотивации вывожу значения строк
                                                    echo "<td>" . $item['name'] . "</td>";
                                                    if ($older_format)
                                                        echo "<td>" . $item['statgroup'] . "</td>";

                                                    echo "<td>" . $item['department_name'] . "</td>";

                                                    // Вывожу наценку
                                                    echo "<td><div id='profit_" . $item['id'] . "' >" . number_format($item['profit'],2, ',', ' ') . "</div></td>";
                                                    // Вывожу сумму процент
                                                    if (isset($item[9])) {
                                                        echo "<td><div id='procent_" . $item['id'] . "' >" . number_format($item['procent'],2, ',', ' ') . "</div></td>";
                                                    } else {
                                                        echo "<td><div id='procent_" . $item['id'] . "' ></div></td>";
                                                    }

                                                    // Вывожу итого в у.е. (НАЦЕНКА + БОНУС + КОРРЕКТИРОВКА) * ПРОЦЕНТ
                                                    if (isset($item[10])) {
                                                        echo "<td><div id='summ_" . $item['id'] . "' >" . number_format($item['sum'], 2, ',', ' ') . "</div></td>";
                                                    } else {
                                                        echo "<td><div id='summ_" . $item['id'] . "' ></div></td>";
                                                    }

                                                    // Перевожу в рубли
                                                    if ($older_format)
                                                        echo "<td><div id='rubsumm_" . $item['id'] . "' >" . number_format(($item['sum'] * 25),2, ',', ' ') . "</div></td>";
                                                    echo "</tr>";
                                                }
                                                echo "</tbody>";
                                                echo "<tfoot>";
                                                echo "<th colspan='2'>Итого</th>";
                                                echo "<th>" . number_format($summ_nac,2, ',', ' ') . "</th>";
                                                echo "<th></th>";
                                                echo "<th><div id='final_summ'>" . number_format($summ_rur,2, ',', ' ') . "</div></th>";
                                                echo "<th></th>";
                                                if ($older_format)
                                                    echo "<th><div id='final_rubsumm'>" . number_format($PR->getAllMarkupRubles(),2, ',', ' ') . "</div></th>";
                                                echo "</tfoot>";
                                                echo "</table>";
                                            }

                                            if ($member->getMotivation() == 7) {
                                                echo '<table class="table table - striped table - hover table - bordered">';
                                                echo "<thead>";
                                                echo "<th class=\"col-md-3\">ФИО</th>";
                                                if ($older_format)
                                                    echo "<th class=\"col-md-3\">Статгруппа</th>";
                                                echo "<th>ТТ</th>";
                                                echo "<th>Наценка ".($older_format?"(у.е.)":"(руб.)")."</th>";
                                                echo "<th> Процент(%)</th>";
                                                echo "<th ".(!$older_format?"style='display:none'":"")."> Итого (у.е.)</th>";
                                                echo "<th> Итого(руб.)</th>";
                                                echo "</thead>";
                                                echo "<tbody>";

                                                // Получаю данные по коммерческому блоку.
                                                foreach ($PR->getCommercialPartService() as $item) {
                                                    echo "<tr>";
                                                    // *************************************************** Строю строки таблицы
                                                    // В зависимости от мотивации вывожу значения строк
                                                    echo "<td>" . $item['name'] . "</td>";
                                                    if ($older_format)
                                                        echo "<td>" . $item['statgroup'] . "</td>";
                                                    echo "<td>" . $item['department_name'] . "</td>";

                                                    // Вывожу наценку
                                                    echo "<td><div id='profit_" . $item['id'] . "' >" . $item['profit'] . "</div></td>";
                                                    // Вывожу сумму процент
                                                    if (isset($item['preliminary_sum'])) {
                                                        echo "<td><div id='procent_" . $item['id'] . "' >" . $item['procent'] . "</div></td>";
                                                    } else {
                                                        echo "<td><div id='procent_" . $item['id'] . "' ></div></td>";
                                                    }

                                                    // Вывожу итого в у.е. (НАЦЕНКА + БОНУС + КОРРЕКТИРОВКА) * ПРОЦЕНТ
                                                    if ($older_format) {
                                                        if (isset($item['procent'])) {
                                                            echo "<td><div id='summ_" . $item['id'] . "' >" . $item['sum'] . "</div></td>";
                                                        } else {
                                                            echo "<td><div id='summ_" . $item['id'] . "' ></div></td>";
                                                        }
                                                    }

                                                    // Перевожу в рубли
                                                    if ($older_format)
                                                        echo "<td><div id='rubsumm_" . $item['id'] . "' >" . $item['sum'] * 25 . "</div></td>";
                                                    else
                                                        echo "<td><div id='rubsumm_" . $item['id'] . "' >" . $item['sum'] . "</div></td>";
                                                    echo "</tr>";
                                                }
                                                echo "</tbody>";
                                                echo "<tfoot>";
                                                if ($older_format)
                                                    echo "<th colspan='3'>Итого</th><th >" . $PR->getSummNachenka() . "</th><th></th><th><div id='final_summ'>" . $PR->getFullsumm() . "</div></th><th><div id='final_rubsumm'>" . ($PR->getFullsumm() * 25) . "</div></th>";
                                                else
                                                    echo "<th colspan='2'>Итого</th><th >" . $PR->getSummNachenka() . "</th><th></th><th><div id='final_rubsumm'>" . ($PR->getFullsumm()) . "</div></th>";
                                                echo "</tfoot>";
                                                echo "</table>";
                                            }
                                            ?>

                                        </div>
                                        <div class="box-footer">
                                            <?php
                                            if ($member->getMotivation() == 2) {
                                                echo '<input type="submit" class="btn btn-default pull-left btn-flat" name="getBonusCorrect" VALUE="Посчитать бонусы и корректировки" autocomplete="off">';
                                            }
                                            if (($Permission_CommercialBlockProcent == 1 || $Permission_CommercialBlockNachenka == 1 || $Permission_CommercialBlockBonus == 1) && $PR->getCommercialLock() != 1) {
                                                if ($member->getMotivation() != 7 && $member->getMotivation() != 8) {
                                                    if (($PR->getPaymentMade() == false)) {
                                                        echo '<input type="submit" class="btn btn-primary pull-right btn-flat" name="SendForm" VALUE="Сохранить" autocomplete="off" '.(!$Status?'onclick="return confirmation();"':'').'>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            };
                            ?>


                            <?php
                            if (($member->getMotivation() != 6) && (($Permission_ShowCommercialBlock == 1) || ($User->getMemberId() == $user_id))) {
                                ?>
                                <div class="tab-pane" id="tab_7">
                                    <div class="box box-info box-solid">
                                        <div class="box-body">

                                            <?php
                                            $kpi_summ = 0;
                                            for ($i = 0; $i < 10; $i++) {
                                                $current_kpi = $KPI->getByNum($i);
                                                if ($current_kpi) {
                                                    $kpi_summ += $current_kpi->getSumm();
                                                }
                                                ?>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label"><?php echo($i + 1); ?></label>

                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>

                                                            <input type="hidden" name="kpi[<?php echo $i; ?>][id]" value="<?php if ($current_kpi) {
                                                                echo $current_kpi->getId();
                                                            } ?>">
                                                            <input type="hidden" name="kpi[<?php echo $i; ?>][num]" value="<?php echo $i; ?>">


                                                            <input type="text"
                                                                   name="kpi[<?php echo $i; ?>][summ]"
                                                                   id="kpi_<?php echo $i; ?>"
                                                                   placeholder="0"
                                                                   value="<?php if ($current_kpi) {
                                                                       echo $current_kpi->getSumm();
                                                                   } ?>"
                                                                   title="<?php if ($current_kpi) {
                                                                       echo $current_kpi->getNote();
                                                                   } ?>"
                                                                   class="form-control pull-right"
                                                                <?php if ($Permission_CommercialBlockNachenka == 0 || $PR->getKpiLock() == 1) {
                                                                    echo "disabled";
                                                                } ?>
                                                                   autocomplete="off">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <div class="input-group-addon btn btn-flat"
                                                                 name="kpi"
                                                                 id='btn_comment_<?php echo $i; ?>'>
                                                                <i class="fa fa-comment"></i>
                                                            </div>

                                                            <textarea name='kpi[<?php echo $i; ?>][note]'
                                                                      cols='1'
                                                                      rows='1'
                                                                      class='form-control' <?php if ($Permission_CommercialBlockNachenka == 0 || $PR->getKpiLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php if ($current_kpi) {
                                                                    echo $current_kpi->getNote();
                                                                } ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div>
                                                <label class="col-sm-3 control-label">Итого :</label>
                                                <label class="col-sm-4 control-label"><?php echo number_format($kpi_summ, 2, ',', ' '); ?> &#8381;</label>
                                            </div>
                                        </div>

                                        <div class="box-footer">
                                            <?php if ($PR->getKpiLock() != 1 && $Permission_CommercialBlockNachenka == 1 && $PR->getPaymentMade() == false) { ?>
                                                <button type="submit" class="btn btn-primary btn-flat pull-right" name="SendForm"  <?=(!$Status?'onclick="return confirmation();"':'');?> >
                                                    Сохранить
                                                </button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <div class="tab-pane" id="tab_2">
                                    <div class="box box-info box-solid">
                                        <div class="box-body">
                                            <?php if (($Permission_Promotions == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Акции</label>

                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Promotions"
                                                                   id="Promotions_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getPromotionsNote(); ?>"
                                                                   value="<?php echo $AB->getPromotions(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Promotions == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>

                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Promotions_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Promotions == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getPromotionsNote(); ?></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_AdministrativePrize == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Административная
                                                        премия</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   name="AdministrativePrize"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getAdministrativePrizeNote(); ?>"
                                                                   value="<?php echo $AB->getAdministrativePrize(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_AdministrativePrize == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?>>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='AdministrativePrize_note'
                                                                      cols='1'
                                                                      rows='1'
                                                                      class='form-control' <?php if ($Permission_AdministrativePrize == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getAdministrativePrizeNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_Training == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Обучение</label>

                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Training"
                                                                   id="Training_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getTrainingNote(); ?>"
                                                                   value="<?php echo $AB->getTraining(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Training == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>

                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat' name='Training'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Training_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Training == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getTrainingNote(); ?></textarea>

                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_PrizeAnotherDepartment == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Премия другого отдела</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   name="PrizeAnotherDepartment"
                                                                   id="PrizeAnotherDepartment_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getPrizeAnotherDepartmentNote(); ?>"
                                                                   value="<?php echo $AB->getPrizeAnotherDepartment(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_PrizeAnotherDepartment == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='PrizeAnotherDepartment_note'
                                                                      cols='1'
                                                                      rows='1'
                                                                      class='form-control' <?php if ($Permission_PrizeAnotherDepartment == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getPrizeAnotherDepartmentNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_Defect == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Неликвиды и брак</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Defect"
                                                                   id="Defect_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getDefectNote(); ?>"
                                                                   value="<?php echo $AB->getDefect(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Defect == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat' name='Defect'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Defect_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Defect == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getDefectNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_Sunday == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Воскресения</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Sunday"
                                                                   id="Sunday_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getSundayNote(); ?>"
                                                                   value="<?php echo $AB->getSunday(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Sunday == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Sunday_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Sunday == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getSundayNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_NotLessThan == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Выплаты не менее</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="NotLessThan"
                                                                   id="NotLessThan_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getNotLessThanNote(); ?>"
                                                                   value="<?php echo $AB->getNotLessThan(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_NotLessThan == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='NotLessThan_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_NotLessThan == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getNotLessThanNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_Overtime == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Переработки</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Overtime"
                                                                   id="Overtime_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $AB->getOvertimeNote(); ?>"
                                                                   value="<?php echo $AB->getOvertime(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Overtime == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat' name='Overtime'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Overtime_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Overtime == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $AB->getOvertimeNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>
                                        </div>
                                        <div class="box-footer">
                                            <?php
                                            if (($Permission_Promotions == 1 ||
                                                    $Permission_AdministrativePrize == 1 ||
                                                    $Permission_Training == 1 ||
                                                    $Permission_PrizeAnotherDepartment == 1 ||
                                                    $Permission_Defect == 1 ||
                                                    $Permission_Sunday == 1 ||
                                                    $Permission_NotLessThan == 1 ||
                                                    $Permission_Overtime == 1) && $PR->getAdministrativeLock() != 1) {

                                                if ($Status != 6) {
                                                    echo '<input type="submit" class="btn btn-primary pull-right btn-flat" name="SendForm" value="Сохранить" '.(!$Status?'onclick="return confirmation();"':'').'>';
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <div class="tab-pane" id="tab_8">
                                    <div class="box box-info box-solid">
                                        <div class="box-body">
                                            <?php if (($Permission_CompensationFAL == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Данные из профиля</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   placeholder="0"
                                                                   value="<?php echo $member->getTransport(); ?>"
                                                                   autocomplete="off"
                                                                   disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Компенсация ГСМ</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   name="CompensationFAL"
                                                                   id="CompensationFAL_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $TR->getNote(); ?>"
                                                                   value="<?php echo $TR->getSumm(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_CompensationFAL == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='CompensationFAL_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_CompensationFAL == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $TR->getNote(); ?></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                                <?php
//                                                echo "<input type=checkbox name=TransportFormula ".($member->getTransportFormula()?'checked':'')."> Пересчитать по формуле";
                                            };
                                            ?>
                                        </div>
                                        <div class="box-footer">
                                            <?php
                                            if (($Permission_CompensationFAL == 1) && $PR->getAdministrativeLock() != 1) {
                                                if ($Status < 5) {
                                                    echo '<input type="submit" class="btn btn-primary pull-right btn-flat" name="SendForm" value="Сохранить" '.(!$Status?'onclick="return confirmation();"':'').'>';
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (($Permission_ShowAdministrativeBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <div class="tab-pane" id="tab_9">
                                    <div class="box box-info box-solid">
                                        <div class="box-body">
                                            <?php if (($Permission_MobileCommunication == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Данные из профиля</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   placeholder="0"
                                                                   value="<?php echo $member->getMobileCommunication(); ?>"
                                                                   autocomplete="off"
                                                                   disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Мобильная связь</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   name="MobileCommunication"
                                                                   id="MobileCommunication_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $MB->getNote(); ?>"
                                                                   value="<?php echo $MB->getSumm(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_MobileCommunication == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'
                                                                 name='MobileCommunication'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='MobileCommunication_note'
                                                                      cols='1'
                                                                      rows='1'
                                                                      class='form-control' <?php if ($Permission_MobileCommunication == 0 || $PR->getAdministrativeLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $MB->getNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
//                                                echo "<input type=checkbox name=MobileFormula ".($member->getMobileFormula()?'checked':'')."> Пересчитать по формуле";
                                            };
                                            ?>
                                        </div>
                                        <div class="box-footer">
                                            <?php
                                            if (($Permission_MobileCommunication == 1) && $PR->getAdministrativeLock() != 1) {
                                                if ($Status < 5) {
                                                    echo '<input type="submit" class="btn btn-primary pull-right btn-flat" name="SendForm" value="Сохранить" '.(!$Status?'onclick="return confirmation();"':'').'>';
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (($Permission_ShowRetitiotionBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <div class="tab-pane" id="tab_3">
                                    <div class="box box-danger box-solid">
                                        <div class="box-body">

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">За опоздания</label>
                                                <div class="col-md-5">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-ruble"></i>
                                                        </div>
                                                        <input type="text" class="form-control" name="LateWork"
                                                               id="LateWork_<?php echo $member->getId(); ?>"
                                                               placeholder="0" onkeyup="retentionCheck();"
                                                               title="<?php echo $RN->getLateWorkNote(); ?>"
                                                               value="<?php echo $RN->getLateWork(); ?>"
                                                               autocomplete="off" <?php if ($Permission_LateWork == 0 || $PR->getRetentionLock() == 1) {
                                                            echo "disabled";
                                                        }; if ($RN->getLateWork()<0) {
                                                            echo " style='color:red'";
                                                        }; ?>
                                                        >
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="input-group">
                                                        <div class='input-group-addon btn btn-flat' name='LateWork'>
                                                            <i class='fa fa-comment'></i>
                                                        </div>
                                                        <textarea name='LateWork_note'
                                                                  cols='1' rows='1'
                                                                  class='form-control' <?php if ($Permission_LateWork == 0 || $PR->getRetentionLock() == 1) {
                                                            echo "disabled";
                                                        }; ?> ><?php echo $RN->getLateWorkNote(); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">За интернет</label>
                                                <div class="col-md-5">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-ruble"></i>
                                                        </div>
                                                        <input type="text" class="form-control" name="Internet"
                                                               id="Internet_<?php echo $member->getId(); ?>"
                                                               placeholder="0" onkeyup="retentionCheck();"
                                                               title="<?php echo $RN->getInternetNote(); ?>"
                                                               value="<?php echo $RN->getInternet(); ?>"
                                                               autocomplete="off" <?php if ($Permission_Internet == 0 || $PR->getRetentionLock() == 1) {
                                                            echo "disabled";
                                                        }; if ($RN->getInternet()<0) {
                                                            echo " style='color:red'";
                                                        }; ?> >
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="input-group">
                                                        <div class='input-group-addon btn btn-flat'>
                                                            <i class='fa fa-comment'></i>
                                                        </div>
                                                        <textarea name='Internet_note'
                                                                  cols='1' rows='1'
                                                                  class='form-control' <?php if ($Permission_Internet == 0 || $PR->getRetentionLock() == 1) {
                                                            echo "disabled";
                                                        }; ?> ><?php echo $RN->getInternetNote(); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Кассовый учет</label>
                                                <div class="col-md-5">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-ruble"></i>
                                                        </div>
                                                        <input type="text" class="form-control"
                                                               name="CachAccounting"
                                                               id="CachAccounting_<?php echo $member->getId(); ?>"
                                                               placeholder="0" onkeyup="retentionCheck();"
                                                               title="<?php echo $RN->getCachAccountingNote(); ?>"
                                                               value="<?php echo $RN->getCachAccounting(); ?>"
                                                               autocomplete="off" <?php if ($Permission_CachAccounting == 0 || $PR->getRetentionLock() == 1) {
                                                            echo "disabled";
                                                        }; if ($RN->getCachAccounting()<0) {
                                                            echo " style='color:red'";
                                                        }; ?> >
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="input-group">
                                                        <div class='input-group-addon btn btn-flat'>
                                                            <i class='fa fa-comment'></i>
                                                        </div>
                                                        <textarea name='CachAccounting_note'
                                                                  cols='1' rows='1'
                                                                  class='form-control' <?php if ($Permission_CachAccounting == 0 || $PR->getRetentionLock() == 1) {
                                                            echo "disabled";
                                                        }; ?> ><?php echo $RN->getCachAccountingNote(); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>


                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Дебиторская
                                                        задолженность</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Receivables"
                                                                   id="Receivables_<?php echo $member->getId(); ?>"
                                                                   placeholder="0" onkeyup="retentionCheck();"
                                                                   title="<?php echo $RN->getReceivablesNote(); ?>"
                                                                   value="<?php echo $RN->getReceivables(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Receivables == 0 || $PR->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; if ($RN->getReceivables()<0) {
                                                                echo " style='color:red'";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Receivables_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Receivables == 0 || $PR->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $RN->getReceivablesNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Кредит</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Credit"
                                                                   id="Credit_<?php echo $member->getId(); ?>"
                                                                   placeholder="0" onkeyup="retentionCheck();"
                                                                   title="<?php echo $RN->getCreditNote(); ?>"
                                                                   value="<?php echo $RN->getCredit(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Credit == 0 || $PR->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; if ($RN->getCredit()<0) {
                                                                echo " style='color:red'";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Credit_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Credit == 0 || $PR->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $RN->getCreditNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php if (($Permission_Other == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Другое</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Other"
                                                                   id="Other_<?php echo $member->getId(); ?>"
                                                                   placeholder="0" onkeyup="retentionCheck();"
                                                                   title="<?php echo $RN->getOtherNote(); ?>"
                                                                   value="<?php echo $RN->getOther(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Other == 0 || $PR->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; if ($RN->getOther()<0) {
                                                                echo " style='color:red'";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat' name='Other'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Other_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Other == 0 || $PR->getRetentionLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $RN->getOtherNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Отчисления в годовой бонус</label>
                                                <div class="col-md-10">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-ruble"></i>
                                                        </div>
                                                        <input type="text" class="form-control"
                                                               placeholder="0" onkeyup="retentionCheck();"
                                                               value="<?php echo $PR->getYearBonusPay(); ?>"
                                                               autocomplete="off"
                                                               disabled
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <?php
                                            if ((
                                                    $Permission_LateWork == 1 ||
                                                    $Permission_Schedule == 1 ||
                                                    $Permission_Internet == 1 ||
                                                    $Permission_CachAccounting == 1 ||
                                                    $Permission_Receivables == 1 ||
                                                    $Permission_Credit == 1 ||
                                                    $Permission_Other == 1
                                                ) && $PR->getRetentionLock() != 1) {
                                                if ($PR->getPaymentMade() == false) {
                                                    echo '<input type="submit" class="btn btn-primary pull-right btn-flat" name="SendForm" value="Сохранить" '.(!$Status?'onclick="return confirmation();"':'').'>';
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (($Permission_ShowCorrectingBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <div class="tab-pane" id="tab_4">
                                    <div class="box box-info box-solid">
                                        <div class="box-body">

                                            <?php if (($Permission_BonusAdjustment == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Корректировка бонуса</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                                   name="BonusAdjustment"
                                                                   id="BonusAdjustment_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $CR->getBonusAdjustmentNote(); ?>"
                                                                   value="<?php echo $CR->getBonusAdjustment(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_BonusAdjustment == 0 || $PR->getCorrectingLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='BonusAdjustment_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_BonusAdjustment == 0 || $PR->getCorrectingLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $CR->getBonusAdjustmentNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php if (($Permission_Calculation == 1) || ($User->getMemberId() == $user_id)) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Расчет</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Calculation"
                                                                   id="Calculation_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $CR->getCalculationNote(); ?>"
                                                                   value="<?php echo $CR->getCalculation(); ?>"
                                                                   autocomplete="off" <?php if ($Permission_Calculation == 0 || $PR->getCorrectingLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='Calculation_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_Calculation == 0 || $PR->getCorrectingLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $CR->getCalculationNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            };
                                            ?>

                                            <?php
                                            if ($Permission_YearBonus == 1) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Годовой бонус</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-ruble"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="YearBonus"
                                                                   id="YearBonus_<?php echo $member->getId(); ?>"
                                                                   placeholder="0"
                                                                   title="<?php echo $CR->getYearBonusNote(); ?>"
                                                                   value="<?php echo $CR->getYearBonus(); ?>"
                                                                   autocomplete="off"
                                                                <?php if ($Permission_YearBonus == 0 || $PR->getCorrectingLock() == 1) {
                                                                    echo "disabled";
                                                                }; ?>
                                                            >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat'>
                                                                <i class='fa fa-comment'></i>
                                                            </div>
                                                            <textarea name='YearBonus_note'
                                                                      cols='1' rows='1'
                                                                      class='form-control' <?php if ($Permission_YearBonus == 0 || $PR->getCorrectingLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> ><?php echo $CR->getYearBonusNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                            <?php
                                            if ($member->getWorkstatus() >= 2) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Корректировка отпуска
                                                        (+/-)</label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Holiday_R"
                                                                   placeholder="Корректировка отпуска в днях"
                                                                   title="<?php echo $CR->getHolidayNote(); ?>"
                                                                   value="<?php echo $CR->getHoliday(); ?>"
                                                                   autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat' name='Holiday'>
                                                                <i class='fa fa-comment'></i></div>

                                                            <textarea name='Holiday_note'
                                                                      cols='1' rows='1' style="height: 34px;"
                                                                      class='form-control'><?php echo $CR->getHolidayNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Корректировка больничных
                                                        (-) </label>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control" name="Hospital"
                                                                   placeholder="Корректировка больничных в днях"
                                                                   title="<?php echo $CR->getHospitalNote(); ?>"
                                                                   value="<?php echo $CR->getHospital(); ?>"
                                                                   autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="input-group">
                                                            <div class='input-group-addon btn btn-flat' name='Hospital'>
                                                                <i class='fa fa-comment'></i></div>
                                                            <textarea name='Hospital_note'
                                                                      cols='1' rows='1' style="height: 34px;"
                                                                      class='form-control'><?php echo $CR->getHospitalNote(); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                        </div>
                                        <div class="box-footer">
                                            <?php
                                            if ((
                                                    $Permission_BonusAdjustment == 1 ||
                                                    $Permission_Calculation == 1 ||
                                                    $Permission_AWH == 1
                                                ) && $PR->getCorrectingLock() != 1) {
                                                if ($PR->getPaymentMade() == false) {
                                                    echo '<input type="submit" class="btn btn-primary pull-right btn-flat" name="SendForm" value="Сохранить" '.(!$Status?'onclick="return confirmation();"':'').'>';
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (($Permission_ShowAWHBlock == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <div class="tab-pane" id="tab_5">
                                    <div class="box box-info box-solid">
                                        <div class="box-body">

                                            <div class="panel" style="border: unset; box-shadow: unset">
                                                <div class="panel-body">
                                                    <?php
                                                    if (isset($_POST['date'])) {
                                                        ?>
                                                            <div class="box box-info box-solid">
                                                                <div class="box-body">
                                                                    <label class="col-sm-2">Количество рабочих дней:</label>
                                                                    <div class="col-md-10">
                                                                        <?php echo $PR->getWorkingDays(); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php
                                                    }
                                                    ?>

                                                    <?php
                                                    if ($emp_date_arr[1] . "." . $emp_date_arr[2] == $date && $emp_date_arr[0] != 1) {
                                                        ?>
                                                        <div class="alert alert-warning alert-dismissible"
                                                             style="border-radius: unset">
                                                            <button type="button" class="close" data-dismiss="alert"
                                                                    aria-hidden="true">×
                                                            </button>
                                                            <h4><i class="icon fa fa-warning"></i> Внимание!</h4>
                                                            Сотрудник принят на
                                                            работу <?php echo $member->getEmploymentDate(); ?>.
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Отсутствия до выхода на работу</label>
                                                            <div class="col-md-10">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input type="text" class="form-control" name="Holiday"
                                                                           autocomplete="off"
                                                                           value="<?php echo $PR->getNewAbsence(); ?>" disabled>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <?php
                                                    }
                                                    ?>

                                                    <table class="table table-hover table-striped">
                                                        <thead>
                                                        <th>Отсутствия</th>
                                                        <th>Отпуск</th>
                                                        <th>Болезнь</th>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><input type="text" class="form-control" name="Absence"
                                                                       id="Absence" autocomplete="off" placeholder="0"
                                                                       value="<?php echo $PR->getAbsence(); ?>" <?php if ($Permission_AWH == 0 || $PR->getAWHLock() == 1) {
                                                                    echo "disabled";
                                                                }; ?>></td>
                                                            <td><input type="text" class="form-control" name="Holiday"
                                                                       id="Holiday" autocomplete="off" placeholder="0"
                                                                       value="<?php echo $PR->getHoliday(); ?>" <?php if ($Permission_AWH != 1 && ($Permission_AWH == 0 || $PR->getAWHLock() == 1)) {
                                                                    echo "disabled";
                                                                }; ?>></td>
                                                            <td><input type="text" class="form-control" name="Disease"
                                                                       id="Disease" autocomplete="off" placeholder="0"
                                                                       value="<?php echo $PR->getDisease(); ?>" <?php if ($Permission_AWH != 1 && ($Permission_AWH == 0 || $PR->getAWHLock() == 1)) {
                                                                    echo "disabled";
                                                                }; ?>></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                    <div class="form-group">
                                                        <label class="col-sm-1 control-label pull-left">Заметка </label>
                                                        <div class="col-sm-11">
                                                            <div class="input-group date">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-comment"></i>
                                                                </div>
                                                                <textarea class="form-control pull-right" style="height: 34px;"
                                                                          name="awh_note" autocomplete="off"><?php echo $PR->getAwhNote(); ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label pull-left">Дополнительный отпуск </label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" name="ExtraHoliday"
                                                                   id="ExtraHoliday" autocomplete="off" placeholder="0"
                                                                   value="<?php echo $PR->getAwhExtraholiday(); ?>" <?php if ($Permission_AWH == 0 || $PR->getAWHLock() == 1) {
                                                                echo "disabled";
                                                            }; ?> >
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="panel" style="border: unset; box-shadow: unset">
                                                <div class="panel-body">
                                                    <table class="table table-hover table-striped">
                                                        <thead>
                                                        <th>НДФЛ</th>
                                                        <th>ИЛ</th>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><input type="text" class="form-control" name="NDFL"
                                                                       id="NDFL" autocomplete="off" placeholder="0"
                                                                       value="<?php echo $PR->getNdfl(); ?>" <?php if ($Permission_AWH == 0 || $PR->getAWHLock() == 1) {
                                                                    echo "disabled";
                                                                }; ?> ></td>
                                                            <td><input type="text" class="form-control" name="RO"
                                                                       id="RO" autocomplete="off" placeholder="0"
                                                                       value="<?php echo $PR->getRo(); ?>" <?php if ($Permission_AWH == 0 || $PR->getAWHLock() == 1) {
                                                                    echo "disabled";
                                                                }; ?> ></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="box-footer">
                                            <?php if ($Permission_AWH == 1) {
                                                ?>
                                                <input type="submit" class="btn btn-danger btn-flat pull-right"
                                                       name="SendForm" value="Сохранить"  <?=(!$Status?'onclick="return confirmation();"':'');?> >
                                                <?php
                                            };
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if (($Permission_ShowAdvanceCalculation == 1) || ($User->getMemberId() == $user_id)) {
                                ?>
                                <div class="tab-pane active" id="tab_0">
                                    <div class="box box-info box-solid">
                                        <?php
                                        if ((!$MI->checkActivePayout($member->getId(), $date, 1)) && ($Status > 1) && $member->getWorkStatus()==1) {
                                            ?>
                                            <div class="box-header" style="background-color: #FFF;">
                                                <h4 class='text-red'> Сотрудник не отображается в ведомости</h4>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <div class="box-body">
                                            <!--                                            Таблица с предварительным расчетом -->
                                            <table class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <th style="text-align:center">Оклад</th>
                                                <th style="text-align:center">Выплаченный аванс</th>
                                                <th style="text-align:center">Отсутствия(все)</th>
                                                <th style="text-align:center">НДФЛ</th>
                                                <th style="text-align:center">ИЛ</th>
                                                <?php if ($PR->getMotivation() != 6) {
                                                    echo '<th style="text-align:center">Коммерческая премия</th>';
                                                    echo '<th style="text-align:center">KPI</th>';
                                                }
                                                ?>

                                                <th style="text-align:center">Административная премия</th>
                                                <th style="text-align:center">ГСМ</th>
                                                <th style="text-align:center">Мобильная связь</th>
                                                <th style="text-align:center">Удержания</th>
                                                <th style="text-align:center">Отчисления в ГБ</th>
                                                <th style="text-align:center">Корректировки</th>

                                                <?php
                                                if ($PR->getYearBonus() > 0) {
                                                    ?>
                                                    <th style="text-align:center">ГБ</th>
                                                    <?php
                                                }
                                                ?>
                                                <th style="text-align:center">Итого</th>
                                                <th style="text-align:center">Итого округление</th>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td style="text-align: right"><?php echo(number_format($PR->getSalary(), 2, ',', ' ')); ?></td>
                                                    <!-- Оклад -->
                                                    <td style="text-align: right"><?php echo(number_format($PR->getAllSalary(), 2, ',', ' ')); ?></td>
                                                    <!-- Выплаченный аванс -->
                                                    <td style="text-align: right"><?php echo(number_format($PR->getAllAbsenceRub(), 2, ',', ' ')); ?></td>
                                                    <!-- Отсутствия -->
                                                    <td style="text-align: right"><?php echo(number_format($PR->getNdfl(), 2, ',', ' ')); ?></td>
                                                    <!-- НДФЛ -->
                                                    <td style="text-align: right"><?php echo(number_format($PR->getRo(), 2, ',', ' ')); ?></td>
                                                    <!-- ИЛ -->

                                                    <?php
                                                    // Вывожу сумму коммерческого блока, если мотивация != 6
                                                    if ($PR->getMotivation() != 6) {
                                                        echo '<td style="text-align: right">';
                                                        if ($PR->getMotivation() == 7) {
                                                            echo "0";
                                                        } else {
                                                            echo (number_format($PR->getAllMarkupRubles(), 2, ',', ' '));
                                                        }
                                                        echo '</td>';

                                                        echo "<td style=\"text-align: right\">" . number_format($PR->getKPISumm(), 2, ',', ' ') . "</td>";
                                                    }
                                                    ?>
                                                    <td style="text-align: right"><?php echo(number_format($PR->getAllAdministrativeRubles(), 2, ',', ' ')); ?></td>
                                                    <td style="text-align: right"><?php echo(number_format($PR->getAllTransport(), 2, ',', ' ')); ?></td>
                                                    <td style="text-align: right"><?php echo(number_format($MB->getSumm(), 2, ',', ' ')); ?></td>
                                                    <td style="text-align: right;<?=($PR->getAllHoldRubles()<0?"color:red;":"")?>"><?php echo(number_format($PR->getAllHoldRubles(), 2, ',', ' ')); ?></td>
                                                    <td style="text-align: right"><?php echo(number_format($PR->getYearBonusPay(), 2, ',', ' ')); ?></td>
                                                    <td style="text-align: right"><?php echo(number_format($PR->getAllCorrectionRubles(), 2, ',', ' ')); ?></td>
                                                    <?php
                                                    if ($PR->getYearBonus() > 0) {
                                                        ?>
                                                        <td style="text-align: right"><?php echo(number_format($PR->getYearBonus(), 2, ',', ' ')); ?></td>
                                                        <?php
                                                    }
                                                    ?>
                                                    <td style="text-align: right">
                                                        <?php
                                                        // Если мотивация не равна 6
                                                        echo(number_format(($PR->getSummary()), 2, ',', ' '));
                                                        ?>
                                                    </td>
                                                    <td style="text-align: right">
                                                        <?php
                                                        // Если мотивация не равна 6
                                                        echo(number_format((roundSumm($PR->getSummary())), 2, ',', ' '));
                                                        ?>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="box-body">
                                            <div class="box-footer">
                                                <?php
                                                if ($Status > 1 && $Status < 6) {
                                                    if ($MI->checkActivePayout($member->getId(), $date, 1)) {
                                                        ?>
                                                        <button class="btn btn-danger btn-flat pull-right" id="rm_statment" name="rm_statment">Убрать из ведомости</button>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <button class="btn btn-success btn-flat pull-right" id="rm_statment" name="rm_statment">Вернуть в ведомость</button>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <div class="tab-pane" id="tab_6">

                                <?php
                                /*
                                 * История изменений Коммерческий блок
                                 */

                                if (!is_null($PH->getCommercialCorrectingProcent()) && $Permission_ShowCommercialBlock && $Permission_CommercialBlockProcent) {
                                    ?>
                                    <div class="box box-primary box-solid">
                                        <div class="box-body">
                                            <table class="table">
                                                <thead>
                                                <th class="col-md-2">Тип блока</th>
                                                <th>%</th>
                                                <th>Кто изменил</th>
                                                <th>Дата изменения</th>
                                                </thead>
                                                <tbody>
                                                <?php

                                                foreach ($PH->getCommercialCorrectingProcent() as $item) {
                                                    if ($item['changer_id'] == -1) {
                                                        $ChangerName = "Администратор";
                                                    } else {
                                                        $ChangerName = $item['changer_last'] . " " . $item['changer_name'] . " " . $item['changer_middle'];
                                                    }

                                                    echo "<tr>";
                                                    echo "<td>Коммерческий блок</td><td title='" . $item['procent_note'] . "'>" . $item['procent'] . "</td><td>" . $ChangerName . "</td><td>" . $item['change_date'] . "</td>";
                                                    echo "</tr>";
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>


                                <?php
                                /*
                                 * Коммерческий блок
                                 */

                                if ($PH->getCommercialBlok() && $Permission_ShowCommercialBlock && $Permission_CommercialBlockBonus) {
                                    ?>
                                    <div class="box box-primary box-solid">
                                        <div class="box-body">
                                            <table class="table">
                                                <thead>
                                                <th class="col-md-2">Тип блока</th>
                                                <th class="col-md-2">Наименование</th>
                                                <th>Бонус (у.е)</th>
                                                <th>Корректировка (у.е)</th>
                                                <th>Кто изменил</th>
                                                <th>Дата изменения</th>
                                                </thead>
                                                <tbody>
                                                <?php

                                                foreach ($PH->getCommercialBlok() as $item) {
                                                    if ($item['changer_id'] == -1) {
                                                        $ChangerName = "Администратор";
                                                    } else {
                                                        $ChangerName = $item['changer_last'] . " " . $item['changer_name'] . " " . $item['changer_middle'];
                                                    }
                                                    if (isset($item['object'])) {
                                                        $Item_Name = $item['object'];
                                                    } else {
                                                        $Item_Name = "";
                                                    }
                                                    echo "<tr>";
                                                    echo "<td>Коммерческий блок</td><td>" . $Item_Name . "</td><td title='" . $item['bonus_note'] . "'>" . $item['bonus'] . "</td><td title='" . $item['correct_note'] . "'>" . $item['correct'] . "</td><td>" . $ChangerName . "</td><td>" . $item['date'] . "</td>";
                                                    echo "</tr>";
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <?php
                                /*
                                 * Административный блок
                                 */
                                if ($PH->getAdministrativeBlock() && $Permission_ShowAdministrativeBlock) {
                                    ?>
                                    <div class="box box-primary box-solid">
                                        <div class="box-body">
                                            <table class="table">
                                                <thead>
                                                <th class="col-md-2">Тип блока</th>
                                                <th class="col-md-2">Наименование</th>
                                                <th>Сумма (руб.)</th>
                                                <th>Кто изменил</th>
                                                <th>Дата изменения</th>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach ($PH->getAdministrativeBlock() as $item) {
                                                    if ($item['changer_id'] == -1) {
                                                        $ChangerName = "Администратор";
                                                    } else {
                                                        $ChangerName = $item['changer_last'] . " " . $item['changer_name'] . " " . $item['changer_middle'];
                                                    }

                                                    switch ($item['type']) {
                                                        case 1:
                                                            $Item_Name = 'Акции';
                                                            break;
                                                        case 2:
                                                            $Item_Name = 'Административная премия';
                                                            break;
                                                        case 3:
                                                            $Item_Name = 'Обучение';
                                                            break;
                                                        case 4:
                                                            $Item_Name = 'Премия другого отдела';
                                                            break;
                                                        case 5:
                                                            $Item_Name = 'Неликвиды и брак';
                                                            break;
                                                        case 6:
                                                            $Item_Name = 'Воскресения';
                                                            break;
                                                        case 7:
                                                            $Item_Name = 'Выплаты не менее';
                                                            break;
                                                        case 8:
                                                            $Item_Name = 'Мобильная связь';
                                                            break;
                                                        case 9:
                                                            $Item_Name = 'Переработки';
                                                            break;
                                                        case 10:
                                                            $Item_Name = 'Компенсация ГСМ';
                                                            break;
                                                    }

                                                    echo "<tr>";
                                                    echo "<td>Административный блок</td><td>" . $Item_Name . "</td><td title='" . $item['note'] . "'>" . $item['summ'] . "</td><td>" . $ChangerName . "</td><td>" . $item['change_date'] . "</td>";
                                                    echo "</tr>";
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <?php
                                /*
                                 * Удержания
                                 */
                                if ($PH->getRetentionBlock() && $Permission_ShowRetitiotionBlock) {
                                    ?>
                                    <div class="box box-danger box-solid">
                                        <div class="box-body">
                                            <table class="table">
                                                <thead>
                                                <th class="col-md-2">Тип блока</th>
                                                <th class="col-md-2">Наименование</th>
                                                <th>Сумма (руб.)</th>
                                                <th>Кто изменил</th>
                                                <th>Дата изменения</th>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach ($PH->getRetentionBlock() as $item) {
                                                    if ($item['changer_id'] == -1) {
                                                        $ChangerName = "Администратор";
                                                    } else {
                                                        $ChangerName = $item['changer_last'] . " " . $item['changer_name'] . " " . $item['changer_middle'];
                                                    }

                                                    switch ($item['type']) {
                                                        case 1:
                                                            $Item_Name = 'За опоздания';
                                                            break;
                                                        case 2:
                                                            $Item_Name = 'За график работы';
                                                            break;
                                                        case 3:
                                                            $Item_Name = 'За интернет';
                                                            break;
                                                        case 4:
                                                            $Item_Name = 'Кассовый учет';
                                                            break;
                                                        case 5:
                                                            $Item_Name = 'Дебиторская задолженность';
                                                            break;
                                                        case 6:
                                                            $Item_Name = 'Кредит';
                                                            break;
                                                        case 7:
                                                            $Item_Name = 'Другое';
                                                            break;
                                                    }

                                                    echo "<tr>";
                                                    echo "<td>Блок удержаний</td><td>" . $Item_Name . "</td><td>" . $item['summ'] . "</td><td>" . $ChangerName . "</td><td>" . $item['change_date'] . "</td>";
                                                    echo "</tr>";
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <?php
                                /*
                                 * Корректировки
                                 */
                                if ($PH->getCorrectingBlock() && $Permission_ShowCorrectingBlock) {
                                    ?>
                                    <div class="box box-primary box-solid">
                                        <div class="box-body">
                                            <table class="table">
                                                <thead>
                                                <th class="col-md-2">Тип блока</th>
                                                <th class="col-md-2">Наименование</th>
                                                <th>Сумма (руб.)</th>
                                                <th>Кто изменил</th>
                                                <th>Дата изменения</th>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach ($PH->getCorrectingBlock() as $item) {
                                                    if ($item['changer_id'] == -1) {
                                                        $ChangerName = "Администратор";
                                                    } else {
                                                        $ChangerName = $item['changer_last'] . " " . $item['changer_name'] . " " . $item['changer_middle'];
                                                    }

                                                    switch ($item['type']) {
                                                        case 1:
                                                            $Item_Name = 'Корректировка бонуса';
                                                            break;
                                                        case 2:
                                                            $Item_Name = 'Расчет';
                                                            break;
                                                        case 3:
                                                            $Item_Name = 'Годовой бонус';
                                                            break;
                                                    }

                                                    echo "<tr>";
                                                    echo "<td>Блок корректировок</td><td>" . $Item_Name . "</td><td>" . $item['summ'] . "</td><td>" . $ChangerName . "</td><td>" . $item['change_date'] . "</td>";
                                                    echo "</tr>";
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <?php
                                }

                                /*
                                 * KPI
                                 */
                                if ($PH->getKPIBlok()) {
                                    ?>
                                    <div class="box box-default box-solid">
                                        <div class="box-header">
                                            <h5>KPI</h5>
                                        </div>
                                        <div class="box-body">
                                            <table class="table">
                                                <thead>
                                                <th>№ </th>
                                                <th>Сумма</th>
                                                <th>Комментарий</th>
                                                <th>Кто изменил</th>
                                                <th>Дата изменения</th>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach ($PH->getKPIBlok() as $item) {
                                                    if ($item['changer_id'] == -1 || $item['changer_id'] == -2) {
                                                        $ChangerName = "Администратор";
                                                    } else {
                                                        $ChangerName = $item['changer_name'];
                                                    }

                                                    echo "<tr>";
                                                    echo "<td>". $item['num'] . "</td><td>". $item['summ'] ."</td><td>".$item['note']."</td><td>".$ChangerName."</td><td>" . $item['change_date'] . "</td>";
                                                    echo "</tr>";
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </form>
                </div>
                <?php
                }
//                    } else {
//                    ?>
<!--                    <div class="box box-solid">-->
<!--                        <div class="box-header">-->
<!--                            <h4 style="color:red"> Автоматический расчет сотрудника уже произведен. Все данные смотрите в плановой выплате.</h4>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                --><?php
//                }
//                ?>
            </div>
        </section>
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
require_once 'footer.php';
?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>

<script>
    function confirmation() {
        return confirm("Ведомость премии на дату <?=$date?> не открыта. Хотите сохранить?");
    }

    function retentionCheck() {
        var val = document.getElementById(event.currentTarget.id).value;
        if (val < 0) {
            document.getElementById(event.currentTarget.id).style.color = 'red';
        } else {
            document.getElementById(event.currentTarget.id).style.color = 'black';
        }
    }

    // Обработка событий по нажатию enter
    $(document).ready(function () {
        $("#form-data").keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    $(document).ready(function () {
        var tab = getCookie('PremiumTab');
        $('.nav-tabs a[href="#tab_' + tab + '"]').tab('show');
    });

    // Выбор даты
    $(function () {
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            format: 'mm.yyyy',
            //startDate: <?php //echo '\''.$member->getEmploymentDate().'\''; ?>//,
        })
    });

    function stringToLocale(input_string) {
        input_string = parseFloat(input_string);
        return input_string.toLocaleString();
    }


    // Комментарии бонус и корректировка
    $(document).ready(function () {
        $("textarea[id^=correcting_note_]").each(function (i) {
            $(this).hide();
        });

        $("textarea[id^=bonus_note_]").each(function (i) {
            $(this).hide();
        });

        // $("#KPI_note").show();

        $(document).on("change", "input[id^=Hospital]", function () {
            var current = $(this).val();
            current = current.replace(",", ".");
            if (isNaN(current)) {
                current = 0;
            } else {
                if (current > 0) {
                    current *= -1;
                }
            }
            $(this).val(current);
        });

        $(document).on("change", "input[id^=Holiday]", function () {
            var current = $(this).val();
            current = current.replace(",", ".");
            if (isNaN(current)) {
                current = 0;
            }
            $(this).val(current);
        });

        $(document).on("click", "div[id^=btn_comment]", function () {
            var currentId = $(this).attr("id");
            var currentName = $(this).attr("name");
            var numberInID = currentId.replace("btn_comment_", "");


            if ($("#" + currentName + "_note_" + numberInID).is(':visible')) {
                $("#" + currentName + "_" + numberInID).show();
                $("#" + currentName + "_note_" + numberInID).hide();
            } else {
                $("#" + currentName + "_" + numberInID).hide();
                $("#" + currentName + "_note_" + numberInID).show();
            }
        });

        $(document).on("change", "textarea", function () {
            var currentVal = $(this).val();
            var currentId = $(this).attr("id");
            var Arr = currentId.split("_"); // bonus_note_
            var numberInID = currentId.replace(Arr[0] + "_note_", "");

            $("#" + Arr[0] + "_" + numberInID).attr("title", currentVal);
            $("#" + Arr[0] + "_note_" + numberInID).hide();
            $("#" + Arr[0] + "_" + numberInID).show();
        })

        $("input[id^=bonus]").on('input', function() {
            $(this).val($(this).val().replace(/[^\d,]/g,''))
        });

        $("input[id^=correcting]").on('input', function() {
            $(this).val($(this).val().replace(/[^\d,-]/g,''))
        });
    });

    // Изменение бонуса
    $(document).ready(function () {
        $(document).on("change", "input[id^=bonus]",
            function () {
                var Bonus = $(this).val();
                var currentId = $(this).attr("id");
                var numberInID = currentId.replace("bonus_", "");

                var CommercialCorrectionProcent = "1";
                if ($("#CommercialCorrectingProcent").val() != undefined) {
                    CommercialCorrectionProcent = $("#CommercialCorrectingProcent").val();
                    if (CommercialCorrectionProcent.indexOf(",") == 1) {
                        CommercialCorrectionProcent = CommercialCorrectionProcent.replace(",", ".");
                    }
                }

                if (!Bonus) {
                    Bonus = 0;
                } else {
                    Bonus = Bonus.replace(",", ".");
                }

                var Correcting = $("#correcting_" + numberInID).val();

                if (!Correcting) {
                    Correcting = 0;
                } else {
                    Correcting = Correcting.replace(",", ".");
                }

                var BonusSumm = 0;
                $("input[id^=bonus]").each(function (i) {
                    if ($(this).val() == '') {
                        var current_value = 0;
                    } else {
                        var current_value = $(this).val();
                        current_value = current_value.replace(",", ".");
                    }
                    BonusSumm = parseFloat(BonusSumm) + parseFloat(current_value);
                });

                $("#bonus_summ").html(stringToLocale(BonusSumm.toFixed(3)));

                var Profit = $("#profit_" + numberInID).html().replaceAll(" ", "").replace(",", ".");
                var Procent = $("#procent_" + numberInID).html().replaceAll(" ", "").replace(",", ".");
                console.log("Процент: " + Procent);
                var Summ = $("#summ_" + numberInID).html();
                var FinalSumRub = 0;


                // profit, correcting, procent, summ
                var NewSumm = (parseFloat(Profit) - parseFloat(Bonus) + parseFloat(Correcting)) * parseFloat(CommercialCorrectionProcent) * parseFloat(Procent) / 100;
                if (!$.isNumeric(NewSumm)) {
                    NewSumm = 0;
                }

                $("#summ_" + numberInID).html(stringToLocale(NewSumm.toFixed(3)));
                <?php
                if ($older_format) {
                    ?>
                    $("#rubsumm_" + numberInID).html(stringToLocale((NewSumm * 25).toFixed(3)));
                    <?php
                } else {
                    ?>
                    $("#rubsumm_" + numberInID).html(stringToLocale((NewSumm).toFixed(3)));
                    <?php
                }
                ?>

                var FinalSumm = 0;
                $("div[id^=summ]").each(function (i) {
                    var current_value = $(this).html();
                    current_value = current_value.replaceAll(" ", "").replaceAll("&nbsp;","").replace(",", ".");
                    FinalSumm = parseFloat(FinalSumm) + parseFloat(current_value);
                });

                // console.log(numberInID+" ======> "+currentVal+" "+Correcting+" "+Profit+" "+Procent+" "+Summ+" "+FinalSumm);
                $("#final_summ").html(stringToLocale(FinalSumm.toFixed(3)));

                $("#nac_summ_" + numberInID).html(stringToLocale((parseFloat(Correcting) + parseFloat(Profit) - parseFloat(Bonus)).toFixed(3)));

                var SummNacenka = 0;
                $("div[id^=nac_summ]").each(function (i) {
                    var current_value = $(this).html();
                    current_value = current_value.replaceAll(" ", "").replaceAll("&nbsp;","").replace(",", ".");
                    SummNacenka = parseFloat(SummNacenka) + parseFloat(current_value);
                });

                $("#summ_nacenka").html(stringToLocale(SummNacenka.toFixed(2)));

                <?php
                if (!$older_format) {
                ?>
                $("div[id^=nac_summ]").each(function (i) {
                    var current_value = $(this).html();
                    current_value = current_value.replaceAll(" ", "").replaceAll("&nbsp;","").replace(",", ".");
                    FinalSumRub = parseFloat(FinalSumRub) + (parseFloat(current_value) * parseFloat(Procent) / 100);
                });
                <?php
                }
                ?>

                <?php
                if ($older_format) {
                ?>
                $("#final_rubsumm").html(stringToLocale((FinalSumm * 25).toFixed(3)));
                <?php
                } else {
                ?>
                $("#final_rubsumm").html(stringToLocale((FinalSumRub).toFixed(3)));
                <?php
                }
                ?>
            });

    });

    // Изменение корректировки
    $(document).ready(function () {
        $(document).on("change", "input[id^=correcting]",
            function () {
                var Correcting = $(this).val();
                var currentId = $(this).attr("id");
                var numberInID = currentId.replace("correcting_", "");
                var FinalSumRub = 0;

                var CommercialCorrectionProcent = "1";
                if ($("#CommercialCorrectingProcent").val() != undefined) {
                    CommercialCorrectionProcent == $("#CommercialCorrectingProcent").val();
                    if (CommercialCorrectionProcent.indexOf(",") == 1) {
                        CommercialCorrectionProcent = CommercialCorrectionProcent.replace(",", ".");
                    }
                }

                if (!Correcting) {
                    Correcting = 0;
                } else {
                    Correcting = Correcting.replace(",", ".");
                }

                var Bonus = $("#bonus_" + numberInID).val();
                if (!Bonus) {
                    Bonus = 0;
                } else {
                    Bonus = Bonus.replace(",", ".");
                }

                var Profit = $("#profit_" + numberInID).html().replaceAll(" ", "").replace(",", ".");
                var Procent = $("#procent_" + numberInID).html().replaceAll(" ", "").replace(",", ".");
                var Summ = $("#summ_" + numberInID).html();

                var CorrectingSumm = 0;
                $("input[id^=correcting]").each(function (i) {
                    if ($(this).val() == '') {
                        var current_value = 0;
                    } else {
                        var current_value = $(this).val();
                        current_value = current_value.replaceAll(" ", "").replaceAll("&nbsp;","").replace(",", ".");
                    }
                    CorrectingSumm = parseFloat(CorrectingSumm) + parseFloat(current_value);
                });
                $("#correct_summ").html(stringToLocale(CorrectingSumm.toFixed(3)));

                // console.log(currentVal+" "+Premiya+" "+Profit+" "+Procent+" "+Summ+" "+FinalSumm);

                // profit, correcting, procent, summ
                var NewSumm = (parseFloat(Profit) + parseFloat(Correcting) - parseFloat(Bonus)) * parseFloat(CommercialCorrectionProcent) * parseFloat(Procent) / 100;
                if (!$.isNumeric(NewSumm)) {
                    NewSumm = 0;
                }

                $("#summ_" + numberInID).html(stringToLocale(NewSumm.toFixed(3)));

                <?php
                if ($older_format) {
                ?>
                    $("#rubsumm_" + numberInID).html(stringToLocale((NewSumm * 25).toFixed(3)));
                <?php
                } else {
                ?>
                    $("#rubsumm_" + numberInID).html(stringToLocale((NewSumm).toFixed(3)));
                <?php
                }
                ?>

                var FinalSumm = 0;
                $("div[id^=summ]").each(function (i) {
                    var current_value = $(this).html();
                    current_value = current_value.replaceAll(" ", "").replaceAll("&nbsp;","").replace(",", ".");
                    FinalSumm = parseFloat(FinalSumm) + parseFloat(current_value);
                });

                $("#final_summ").html(stringToLocale(FinalSumm.toFixed(3)));

                $("#nac_summ_" + numberInID).html(stringToLocale(parseFloat(Correcting) - parseFloat(Bonus) + parseFloat(Profit)));

                var SummNacenka = 0;
                $("div[id^=nac_summ]").each(function (i) {
                    var current_value = $(this).html();
                    // current_value = current_value.replace(",", ".");
                    current_value = current_value.replaceAll(" ", "").replaceAll("&nbsp;","").replace(",", ".");
                    SummNacenka = parseFloat(SummNacenka) + parseFloat(current_value);
                });

                $("#summ_nacenka").html(stringToLocale(SummNacenka.toFixed(2)));

                <?php
                if (!$older_format) {
                ?>
                $("div[id^=nac_summ]").each(function (i) {
                    var current_value = $(this).html();
                    current_value = current_value.replaceAll(" ", "").replaceAll("&nbsp;","").replace(",", ".");
                    FinalSumRub = parseFloat(FinalSumRub) + (parseFloat(current_value) * parseFloat(Procent) / 100);
                });
                <?php
                }
                ?>

                <?php
                if ($older_format) {
                ?>
                $("#final_rubsumm").html(stringToLocale((FinalSumm * 25).toFixed(3)));
                <?php
                } else {
                ?>
                $("#final_rubsumm").html(stringToLocale((FinalSumRub).toFixed(3)));
                <?php
                }
                ?>
            });
    });

    // Изменение корректировки процента

    $(document).ready(function () {
        $(document).on("change", "input[id^=CommercialCorrectingProcent]",
            function () {
                var CommercialCorrectionProcent = 1;

                if (($("#CommercialCorrectingProcent").val()) != undefined) {
                    CommercialCorrectionProcent = $("#CommercialCorrectingProcent").val();
                    CommercialCorrectionProcent = CommercialCorrectionProcent.replace(",", ".");
                }

                $("div[id^=profit_]").each(function (i) {
                    var currentId = $(this).attr("id");
                    var numberInID = currentId.replace("profit_", "");

                    var Profit = $("#profit_" + numberInID).html();  // Наценка
                    var Correcting = $("#correcting_" + numberInID).val(); // корректировка наценки
                    if (Correcting == "") {
                        Correcting = 0;
                    } else {
                        Correcting = Correcting.replace(",", ".");
                    }

                    var Bonus = $("#bonus_" + numberInID).val(); // Бонус
                    if (Bonus == "") {
                        Bonus = 0;
                    } else {
                        Bonus = Bonus.replace(",", ".");
                    }

                    var Procent = $("#procent_" + numberInID).html(); // Процент

                    // console.log("Profit: "+Profit+" Correcting: "+Correcting+" Bonus: "+Bonus+" Procent: "+Procent);

                    // profit, correcting, procent, summ
                    var NewSumm = (parseFloat(Profit) - parseFloat(Bonus) + parseFloat(Correcting)) * parseFloat(CommercialCorrectionProcent) * parseFloat(Procent) / 100;
                    if (!$.isNumeric(NewSumm)) {
                        NewSumm = 0;
                    }

                    $("#summ_" + numberInID).html(stringToLocale(NewSumm.toFixed(3)));

                    <?php
                    if ($older_format) {
                    ?>
                        $("#rubsumm_" + numberInID).html(stringToLocale((NewSumm * 25).toFixed(3)));
                    <?php
                    } else {
                    ?>
                        $("#rubsumm_" + numberInID).html(stringToLocale((NewSumm).toFixed(3)));
                    <?php
                    }
                    ?>

                    var FinalSumm = 0;
                    $("div[id^=summ]").each(function () {
                        var current_value = $(this).html();
                        current_value = current_value.replace(",", ".");
                        FinalSumm = parseFloat(FinalSumm) + parseFloat(current_value);
                    });

                    // console.log(numberInID+" ======> "+currentVal+" "+Correcting+" "+Profit+" "+Procent+" "+Summ+" "+FinalSumm);
                    $("#final_summ").html(stringToLocale(FinalSumm.toFixed(3)));

                    <?php
                    if ($older_format) {
                    ?>
                        $("#final_rubsumm").html(stringToLocale((FinalSumm * 25).toFixed(3)));
                    <?php
                    } else {
                    ?>
                        $("#final_rubsumm").html(stringToLocale((FinalSumm).toFixed(3)));
                    <?php
                    }
                    ?>

                    $("#nac_summ_" + numberInID).html(stringToLocale(parseFloat(Bonus) + parseFloat(Correcting) + parseFloat(Profit)));

                    console.log($(this).attr("id"));

                });


            });
    });

</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>


