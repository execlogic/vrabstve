<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 08.11.18
 * Time: 13:37
 */

// Время работы скрипта
$start = microtime(true);

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";
require_once 'admin/Role.php';
require_once 'app/ErrorInterface.php';

require_once 'app/Notify.php';

// Инициализируем основные объекты
$Errors = new ErrorInterface(); // Объект ошибок

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

if (isset($_POST['PushButton'])) {
    $RS = new Role();

    foreach ($_POST as $key => $value) {
        if (($key == 'inheritance') || ($key == 'PushButton')) {
            continue;
        }

        if ($key != "RoleName") {
            ($value == "on") ? $value = 1 : $value = 0;
        }

        $func = 'set' . $key; // Вызов функции из значения переменной !!!
        $RS->$func($value); // Вызов функции из значения переменной !!!
    }

    if (!empty($_POST['RoleName'])) {
        try {
            $RI->add($RS);
        } catch (Exception $e) {
//            echo $e->getMessage();
            $Errors->add($e->getMessage());
        }
    } else {
        $Errors->add("Неверно задано название роли");
    }
}

// Edit
if (isset($_POST['PushButton_1'])) {
    $RS = new Role();

    foreach ($_POST as $key => $value) {
        if (($key == 'inheritance') || ($key == 'PushButton_1')) {
            continue;
        }

        if (($key != "RoleName") && ($key != "Id")) {
            ($value == "on") ? $value = 1 : $value = 0;
        }

        $func = 'set' . $key; // Вызов функции из значения переменной !!!
        $RS->$func($value); // Вызов функции из значения переменной !!!
    }

    $RI->update($RS);
}

$RolesList_t = $RI->get();

$RolesList = array();
foreach ($RolesList_t as $k1=>$item) {
    foreach ($item as $key=>$val) {
        if ($key == "id" || $key == "name") {
            $RolesList[$k1][$key] = $val;
        } else {
            $t = unserialize($val);
            $RolesList[$k1] = array_merge($RolesList[$k1],$t);

        }
    }
}
$menu_open = 2;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Роли пользователей</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
    <style>
        .alert-warning .alert-icon {
            background-color: #ffeeba;
        }
    </style>
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>

    <div class="content-wrapper">


        <section class="content">
            <!-- Default box -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h6 class="box-title"><b>Роли пользователей</b></h6>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <th>#</th>
                        <th>Название</th>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($RolesList as $item) {
                            if ($item['id'] < 7) {
                                echo "<tr><td>" . $item['id'] . "</td><td><a href=\"javascript:void(0);\" id='edit_" . $item['id'] . "'>" . $item['name'] . " (стандартная роль) </a></td></tr>";
                            } else {
                                echo "<tr><td>" . $item['id'] . "</td><td><a href=\"javascript:void(0);\" id='edit_" . $item['id'] . "'>" . $item['name'] . "</a></td></tr>";
                            }
                        };
                        ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-primary btn-sm btn-flat" data-toggle="modal" data-target="#modal-default" title="Добавить роль">Добавить роль</button>

                </div>

                <form method="post" class="form-horizontal">
                    <div class="modal fade" id="modal-default" style="padding-left: 15px; overflow: auto !important;">
                        <div style="width: 90%;" class="modal-dialog">
                            <div style="width: 100%;" class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                    <h4> Добавить роль </h4>
                                </div>

                                <div class="modal-body">
                                    <div class="box box-solid box-primary">
                                        <div class="box-body">

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Название роли</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="new" class="form-control" name="RoleName" placeholder="Название роли" autocomplete="off">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Наследование</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="inheritance" id="inheritance">
                                                        <option value="0">Не выбрано</option>
                                                        <?php
                                                        foreach ($RolesList as $item) {
                                                            echo "<option value='" . $item['id'] . "'>" . $item['name'] . "</option>";
                                                        };
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Основной доступ</b></h5>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="AddUser"> Добавить пользователя
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="Settings"> Возможность изменения настроек
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Внесение данных</b></h5>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ImportAWH"> Импорт данных УРВ
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ImportNDFL"> Импорт данных НДФЛ
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="Import1C"> Импорт данных 1С7.7
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Профили сотрудников</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ProfilePayments"> Просмотр выплат в профиле
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ProfileAWH"> Просмотр УРВ в профиле
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ProfileWorkInfo"> Просмотр рабочей информации
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ProfilePersonalInfo"> Просмотр личной информации
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ShowHistory"> Просмотр истории по пользователю
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="AvansCalculation"> Кнопка расчета аванса и расчет аванса
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="Avans2Calculation"> Кнопка расчета аванса2 и расчет аванса
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="PremiyaCalculation"> Кнопка расчета премии
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="RaschetCalculation"> Кнопка расчета уволенного сотрудника
                                                    </label>
                                                </div>

                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Изменение профиля сотрудников</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ChangeMember"> Изменение пользователя
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ChangeWorkPersonal"> Изменение личных и рабочих данных
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ChangeFinanceMotivation"> Изменение мотивации и финансового блока
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ChangeWorkStatus"> Изменение рабочего статуса
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Премия</b></h5>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ShowAdvanceCalculation"> Отображение блока предварительный расчет
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Премия. Коммерческий блок</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ShowCommercialBlock"> Показывать коммерческий блок
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="CommercialBlockBonus"> Бонусы
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="CommercialBlockNachenka"> Наценка
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="CommercialBlockProcent"> Корректирующий процент
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Премия. Административный блок</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ShowAdministrativeBlock"> Показывать административный блок
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="Promotions"> Акции
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="AdministrativePrize"> Административная премия
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="Training"> Обучение
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="PrizeAnotherDepartment"> Премия другого отдела
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="Defect"> Неликвиды и брак
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="Sunday"> Воскресения
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="NotLessThan"> Выплаты не менее
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="MobileCommunication"> Мобильная связь
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="Overtime"> Переработки
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="CompensationFAL"> Компенсация ГСМ
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Премия. Удержания</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ShowRetitiotionBlock"> Показывать блок удержаний
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="LateWork"> За опоздания
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="Schedule"> За график работы
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="Internet"> За интернет
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="CachAccounting"> Кассовый учет
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="Receivables"> Дебиторская задолженность
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="Credit"> Кредит
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="Other"> Другое
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Премия. Корректировки</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ShowCorrectingBlock"> Показывать блок корректировок
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="BonusAdjustment"> Корректировка бонуса
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="Calculation"> Расчет
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="YearBonus"> Годовой бонус
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Премия. УРВ</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ShowAWHBlock"> Показывать блок УРВ
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="AWHEdit"> Изменение УРВ
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Отчеты</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ReportLeaving"> Уволенные за 90 дней
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ReportTransfer"> Переводы за 90 дней
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ReportAWH"> Журнал УРВ
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ReportMemberAWH"> УРВ по сотруднику
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ReportRetetition"> Удержания по сотрудникам
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ReportCorrecting"> Контроль корректировки наценки
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Ведомости и расчет</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="ReportJournal"> Журнал ведомостей
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new" name="FinancialStatement"> Финансовая ведомость
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Расчет(стадии ведомости)</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new"> Перевод на стадию «На проверку»
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new"> Перевод на стадию «На утверждение»
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new"> Перевод на стадию «В кассу»
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new"> Конверты (полный доступ)
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new"> Конверты (без ФИО)
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new"> Конверты стадия «Выплачено»
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new"> Перевод на стадию «В расходы»
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="new"> На предыдущую стадию
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary btn-flat btn-sm" name="PushButton"><i class="fa fa-save"></i> Добавить</button>
                                    <button class="btn btn-danger btn-flat btn-sm" data-dismiss="modal" title="Закрыть"><i class="fa fa-times"></i> Закрыть</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>


                <!--                Edit- и просмотр -->

                <form method="post" class="form-horizontal">
                    <div class="modal fade" id="modal-edit" style="padding-left: 15px; overflow: auto !important;">
                        <div style="width: 90%;" class="modal-dialog">
                            <div style="width: 100%;" class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                    <h4> Изменение роли </h4>
                                </div>

                                <div class="modal-body">
                                    <div class="box box-solid box-primary">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Название роли</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="editName" name="RoleName" placeholder="Название роли" readonly>
                                                </div>
                                                <input type="text" hidden="hidden" id="editRoleId" name="Id" readonly>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Основной доступ</b></h5>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="AddUser"> Добавить пользователя
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="Settings"> Возможность изменения настроек
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Внесение данных</b></h5>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ImportAWH"> Импорт данных УРВ
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ImportNDFL"> Импорт данных НДФЛ
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="Import1C"> Импорт данных 1С7.7
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Профили сотрудников</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ProfilePayments"> Просмотр выплат в профиле
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ProfileAWH"> Просмотр УРВ в профиле
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ProfileWorkInfo"> Просмотр рабочей информации
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ProfilePersonalInfo"> Просмотр личной информации
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ShowHistory"> Просмотр истории по пользователю
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="AvansCalculation"> Кнопка расчета аванса и расчет аванса
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="Avans2Calculation"> Кнопка расчета аванса2 и расчет аванса
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="PremiyaCalculation"> Кнопка расчета премии
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="RaschetCalculation"> Кнопка расчета уволенного сотрудника
                                                    </label>
                                                </div>

                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Изменение профиля сотрудников</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ChangeMember"> Изменение пользователя
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ChangeWorkPersonal"> Изменение личных и рабочих данных
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ChangeFinanceMotivation"> Изменение мотивации и финансового блока
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ChangeFinanceProbation"> Изменение комментария на испытательный срок
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ChangeWorkStatus"> Изменение рабочего статуса
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Премия</b></h5>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ShowAdvanceCalculation"> Отображение блока предварительный расчет
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Премия. Коммерческий блок</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ShowCommercialBlock"> Показывать коммерческий блок
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="CommercialBlockBonus"> Бонусы
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="CommercialBlockNachenka"> Наценка
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="CommercialBlockProcent"> Корректирующий процент
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Премия. Административный блок</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ShowAdministrativeBlock"> Показывать административный блок
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="Promotions"> Акции
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="AdministrativePrize"> Административная премия
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="Training"> Обучение
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="PrizeAnotherDepartment"> Премия другого отдела
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="Defect"> Неликвиды и брак
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="Sunday"> Воскресения
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="NotLessThan"> Выплаты не менее
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="MobileCommunication"> Мобильная связь
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="Overtime"> Переработки
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="CompensationFAL"> Компенсация ГСМ
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Премия. Удержания</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ShowRetitiotionBlock"> Показывать блок удержаний
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="LateWork"> За опоздания
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="Schedule"> За график работы
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="Internet"> За интернет
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="CachAccounting"> Кассовый учет
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="Receivables"> Дебиторская задолженность
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="Credit"> Кредит
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="Other"> Другое
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Премия. Корректировки</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ShowCorrectingBlock"> Показывать блок корректировок
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="BonusAdjustment"> Корректировка бонуса
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="Calculation"> Расчет
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="YearBonus"> Годовой бонус
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Премия. УРВ</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ShowAWHBlock"> Показывать блок УРВ
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="AWHEdit"> Изменение УРВ
                                                    </label>
                                                </div>
                                            </div>


                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Отчеты</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ReportLeaving"> Уволенные за 90 дней
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ReportTransfer"> Переводы за 90 дней
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ReportAWH"> Журнал УРВ
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ReportMemberAWH"> УРВ по сотруднику
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ReportRetetition"> Удержания по сотрудникам
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ReportCorrecting"> Контроль корректировки наценки
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Ведомости и расчет</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="ReportJournal"> Журнал ведомостей
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit" name="FinancialStatement"> Финансовая ведомость
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-10 col-sm-offset-2">
                                                <h5 class="text-left"><b>Расчет(стадии ведомости)</b></h5>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit"> Перевод на стадию «На проверку»
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit"> Перевод на стадию «На утверждение»
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit"> Перевод на стадию «В кассу»
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit"> Конверты (полный доступ)
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit"> Конверты (без ФИО)
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit"> Конверты стадия «Выплачено»
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit"> Перевод на стадию «В расходы»
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="edit"> На предыдущую стадию
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary" id="PushButton_1" name="PushButton_1"><i class="fa fa-save"></i> Изменить</button>
                                    <button class="btn btn-danger" data-dismiss="modal" title="Закрыть"><i class="fa fa-times"></i> Закрыть</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>


                <?php
                /*
                 * Вывод информации об ощибках
                 */
                if ($Errors->getCount() > 0) {
                ?>

                <div class="modal fade" id="modalError" style="background-color: #1f292e">
                    <div style="width: 90%;" class="modal-dialog">
                        <div style="width: 100%;" class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h5>Ошибки на странице</h5>
                            </div>

                            <div class="modal-body">
                                <?php
                                foreach ($Errors->getErrors() as $error) {
                                ?>
                                <div class="alert alert-danger" role="alert">
                                     <i class="fa fa-exclamation-triangle" style="margin-right: 15px; "></i>
                                    <?php echo $error; ?>
                                    <a href="#" class="close justify-content-center" data-dismiss="alert">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                                <?php
                                };
                                ?>
                                </div>

                            </div>
                        </div>
                    </div>

                    <?php
                }
                ?>
            </div>
        </section>
    </div>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
    var RoleArray = <?php echo json_encode($RolesList); ?>

        $('select[name=inheritance]').change(function () {
            var value = $('select[name=inheritance]').val();
            if (value > 0) {
                $("input[id^=new]").each(function () {
                    var name = $(this).attr("name");

                    $(this).prop('checked', parseInt(RoleArray[value - 1][name]));
                });
            } else {
                $("#new").each(function () {
                    $(this).prop('checked', 0);
                });
            }
        });

    $('a[id^=edit]').click(function () {
        var currentId = $(this).attr("id");
        var numberInID = currentId.replace("edit_", "");

        if (numberInID > 0) {
            $("input[id^=edit]").each(function () {
                var name = $(this).attr("name");
                $(this).prop('checked', parseInt(RoleArray[numberInID - 1][name]));
            });
            $('#editName').val(RoleArray[numberInID - 1]['name']);
            $('#editRoleId').val(RoleArray[numberInID - 1]['id']);
            // console.log(RoleArray[numberInID - 1]['id']);
        }

        if (numberInID < 7) {
            $("input[id^=edit]").each(function () {
                var name = $(this).attr("name");
                $(this).prop('disabled', "disabled");
            });

            $('#PushButton_1').hide();
        } else {
            $("input[id^=edit]").each(function () {
                var name = $(this).attr("name");
                $(this).prop('disabled', "");
            });

            $('#PushButton_1').show();
        }

        $('#modal-edit').modal('show');
    })


</script>

<?php
if ($Errors->getCount() > 0) {
    echo "<script> $('#modalError').modal('show'); </script>";
}
?>


<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
