<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 17.10.18
 * Time: 18:30
 */

// Время работы скрипта
$start = microtime(true);

require_once 'app/MemberInterface.php';
require_once 'app/AwhInterface.php';

require_once 'admin/User.php';
require_once "admin/RoleInterface.php";

require_once 'app/Notify.php';

require_once 'app/AwhMemberInterface.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
    exit();
}

// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

// Настройки
/*
 * Скрыть уволенных пользователей
 * 0 - нет
 * 1 - да
 */
$ShowLeaving = 0;
if (isset($_REQUEST['ShowLeaving'])) {
    $ShowLeaving = 1;
}

$Permission_ReportMemberAWH = 0;
$RoleDepartmentFilter = array();
foreach ($Roles as $Role) {
    if ($Role->getReportMemberAWH() == 1) {
        $Permission_ReportMemberAWH = 1;

        /*
         * Если в первом элементе фильтра стоит, 0 , то это все отделы и мы продолжаем
         *
         */
        if (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] == 0) {
            continue;
        }

        // Иначе проверям роль на ноль, если ноль, то очищаю массив и добавляю первый элимент с значением 0
        if ($Role->getDepartment() == 0) {
            $RoleDepartmentFilter = array(0);
        } else {
            /*
             * Иначе добавляю в конец массива значение об отделе
             */
            $RoleDepartmentFilter[] = $Role->getDepartment();
        }
    }
}

if ($Permission_ReportMemberAWH == 0) {
    header("Location: 404.php");
    exit();
}


/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

$date_m = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
$AWH = new AwhInterface();
$AMI = new AwhMemberInterface();

$MI = new MemberInterface();


//var_dump($AWH_Data);
$date = date("Y");
$AWHData = array();

if (isset($_REQUEST['SendDate']) && isset($_REQUEST['date']) && isset($_REQUEST['MemberId'])) {
    $date = $_REQUEST['date'];
    $Member_id = $_REQUEST['MemberId'];
    $AMI->calculateHoliday($Member_id, $date);
}

if (isset($Member_id)) {
    $member = $MI->fetchMemberByID($Member_id);
    if ($member->GetId()==66) {
        $member->setEmploymentDate("24.01.2022");
    }
    $employment_date = DateTime::createFromFormat("d.m.Y", $member->getEmploymentDate());
    $show_balance = true;

    if ($date < date("Y") || (date("m")==12 && date("d")>=20))  {
        $date_now = DateTime::createFromFormat("d.m.Y","31.12.".$date);
        $interval = $employment_date->diff($date_now);
    } else {
        $date_now = new DateTime("first day of this month");
        $interval = $employment_date->diff($date_now);
    }

    if ($interval->y==0 && $interval->m<6) {
        $show_balance = false;
    }
}

$menu_open = 3;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Журнал УРВ по сотруднику<?=(isset($member)?": ".$member->getLastName()." ".$member->getName():"")?></title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Select2 -->
    <link rel="stylesheet" href="../../bower_components/select2/dist/css/select2.min.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <section class="content-header">
            <h4>
                Журнал УРВ по сотруднику<?=(isset($member)?": ".$member->getLastName()." ".$member->getName():"")?>
            </h4>
            <ol class="breadcrumb">
                <li class="active">Журнал ведомостей</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="box box-solid box-plane">
                <div class="box-body">
                    <form class="form-horizontal" method="post">
                        <div class="form-group">
                            <label class="col-sm-2 col-md-2 control-label">Выберите дату c: </label>
                            <div class="col-sm-6 col-md-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker" name="date" data-inputmask="'alias': 'mm.yyyy'" data-mask=""
                                           value="<?php echo $date; ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-2">
                                <div class="checkbox">
                                    <label>
                                        <input name="ShowLeaving" id="ShowLeaving" type="checkbox" <?php if ($ShowLeaving == 1) echo "checked"; ?> >
                                        Показывать уволенных сотрудников
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 col-md-2 control-label">Сотрудники: </label>
                            <div class="col-sm-6 col-md-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-check"></i>
                                    </div>
                                    <select class="form-control select2" name="MemberId">
                                        <?php
                                        if ($ShowLeaving) {
                                            $MI->fetchMembers(true, null, true);
                                        } else {
                                            $MI->fetchMembers(false, null, true);
                                        }
                                        foreach ($MI->GetMembers() as $memberItem) {
                                            if ($memberItem->getNotCountUrv()==1)
                                                continue;
                                            if (in_array($memberItem->getPosition()['id'], AwhMemberInterface::$positionBanList)) {
                                                continue;
                                            }

                                            $dep_id = $memberItem->getDepartment()['id'];

                                            if (isset($RoleDepartmentFilter[0]) && $RoleDepartmentFilter[0] != 0) {
                                                if (!in_array($dep_id, $RoleDepartmentFilter)) {
                                                    continue;
                                                }
                                            }

                                            if (isset($Member_id) && $Member_id == $memberItem->getId()) {
                                                echo "<option value=\"" . $memberItem->getId() . "\" selected >" . $memberItem->getLastName() . " " . $memberItem->getName()  ." ". ($memberItem->getDismissalDate()=='01.01.9999'?"":" [уволен ".$memberItem->getDismissalDate()."]") . "</option>";
                                            } else {
                                                echo "<option value=\"" . $memberItem->getId() . "\">" . $memberItem->getLastName() . " " . $memberItem->getName() ." ". ($memberItem->getDismissalDate()=='01.01.9999'?"":" [уволен ".$memberItem->getDismissalDate()."]")  . "</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-2"><input type="submit" class="btn btn-flat btn-default" name="SendDate" value="Получить данные"></div>
                        </div>
                    </form>
                </div>
            </div>

            <?php
            if (isset($_REQUEST['SendDate']) && isset($_REQUEST['date']) && isset($_REQUEST['MemberId'])) {
                ?>
                <div class="box box-solid box-plane">
                    <div class="box-body">
                        <div class="col-md-12">
                            <h4>Дата приема:
                                <?php
                                echo $member->getEmploymentDate();
                                ?>
                            </h4>
                        </div>
                        <table class="table table-striped table-hover" id="DataTable">
                            <thead>
                            <tr>
                                <th>Дата</th>
                                <th>Тип</th>
                                <th>Отсутствия</th>
                                <th>Отпуск</th>
                                <th>Болезнь</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($AMI->aData) > 0) {
                                foreach ($AMI->aData as $value) {
                                    echo "<tr>";
                                    echo "<td>" . $value['date'] . "</td>";
                                    switch ($value['type_of_payment']) {
                                        case 1:
                                            echo "<td>Премия</td>";
                                            break;
                                        case 2:
                                            echo "<td>Аванс</td>";
                                            break;
                                        case 3:
                                            echo "<td>Аванс2</td>";
                                            break;
                                        case 4:
                                            echo "<td>Расчет</td>";
                                            break;
                                        case 5:
                                            echo "<td>Внеплановая выплата</td>";
                                            break;
                                        case 6:
                                            echo "<td>Годовой бонус</td>";
                                            break;
                                    }
                                    echo "<td>" . ($value['absence']) . "</td><td>" . $value['holiday'] . "</td><td>" . $value['disease'] . "</td>";
                                    echo "</tr>";
                                }
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Итого</th>
                                <th></th>
                                <th><?php if (isset($AMI->sumAbsence)) echo $AMI->sumAbsence; ?></th>
                                <th><?php if (isset($AMI->sumHoliday)) echo $AMI->sumHoliday; ?></th>
                                <th><?php if (isset($AMI->sumDisease)) echo $AMI->sumDisease; ?></th>
                            </tr>
                            <tr style="background-color: #f5f5f5;" class="urv-summary">
                                <th>Потрачено отпуска за первые <?=$AMI->HOLIDAY_LAST_YEAR?> месяца </th>
                                <th></th><th></th>
                                <th><?=number_format($AMI->sum_first_holiday, 2, '.', ' ')?></th>
                                <th></th>
                            </tr>
                            <tr style="background-color: #f5f5f5;<?=($AMI->HOLIDAY_LAST_YEAR==12?'display:none;':'')?>"  class="urv-summary">
                                <th>Потрачено отпуска за <?=($AMI->HOLIDAY_LAST_YEAR+1)?>-12 месяц </th>
                                <th></th><th></th>
                                <th><?=number_format($AMI->sum_second_holiday, 2, '.', ' ')?></th>
                                <th></th>
                            </tr>
                            <?php
                            if ($date>=2021) {
                            ?>
                            <tr style="background-color: #f5f5f5" class="urv-summary">
                                <th>Остаток отпуска за <?=($date-1)?> год на 01.01.<?=$date?></th>
                                <th></th><th></th>
                                <th><?=number_format($AMI->vacation_balance, 2, '.', ' ')?></th>
                                <th></th>
                            </tr>
                            <tr style="background-color: #f5f5f5" class="urv-summary">
                                <th>Количество дней положенного отпуска в <?=($date)?> году</th>
                                <th></th><th></th>
                                <th>
                                    <?php
                                    if ($show_balance)
                                        echo number_format($AMI->holidayDays, 2, '.', ' ');
                                    else
                                        echo number_format(0, 2, '.', ' ');
                                    ?>
                                </th>
                                <th></th>
                            </tr>
                            <tr style="background-color: #f5f5f5" class="urv-summary">
                                <?php
                                if (date("Y")>$date) {
                                ?>
                                    <th>Остаток отпуска на 01.01.<?=$date+1?></th>
                                <?php
                                } else {
                                    ?>
                                    <th>Остаток отпуска на 01.<?= date("m")?></th>
                                <?php
                                }
                                ?>
                                <th></th><th></th>
                                <th>
                                    <?php
//                                    if (date("m")<=3) {
//                                        echo number_format(($AWH::$MAX_HOLIDAY - $prevSumHoliday) + $AWH::$MAX_HOLIDAY - $currentumHoliday, 2, '.', ' ');
//                                    } else {
//                                        echo number_format($AWH::$MAX_HOLIDAY - $currentumHoliday, 2, '.', ' ');
//                                    }
                                    if ($show_balance)
                                        echo number_format($AMI->holiday_balance_calculation, 2, '.', ' ');
                                    else
                                        echo number_format(0, 2, '.', ' ');
                                    ?>
                                </th>
                                <th></th>
                            </tr>
                            <tr style="background-color: #f5f5f5" class="urv-summary">
                                <?php
                                if (date("Y")>$date) {
                                    ?>
                                    <th>Остаток больничных на 31.12.<?=$date?></th>
                                    <?php
                                } else {
                                    ?>
                                    <th>Остаток больничных на 01.<?= date("m")?></th>
                                    <?php
                                }
                                ?>
                                <th></th><th></th>
                                <th></th>
                                <th><?=number_format($AMI->diseaseBalance, 2, '.', ' ')?></th>
                            </tr>
                            <?php
                            }
                            ?>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <?php
            }
            ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    require_once 'footer.php';
    ?>
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="../../bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
    $(function () {
        $('.select2').select2()
    })

    $(function () {
        $('#DataTable').DataTable({
            'paging': false,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': false,
            'autoWidth': true
        })


        // Выбор даты
        $('#datepicker').datepicker({
            language: 'ru',
            autoclose: true,
            viewMode: "Years",
            minViewMode: "years",
            format: 'yyyy',
        })
    });

    $(document).ready(function () {
        $('#ShowLeaving').change(function () {
            // alert('changed');
            // alert($('#ShowLeaving').val());
            if ($('#ShowLeaving').is(":checked"))
            {
                window.location.href="journal_awh_member.php?ShowLeaving=1";
            } else {
                window.location.href="journal_awh_member.php";
            }
        });
    })
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>


