<?php
/**
 * Created by PhpStorm.
 * User: savko
 * Date: 26.09.18
 * Time: 17:21
 */

require_once 'app/ProductionCalendar.php';

require_once "admin/RoleInterface.php";
require_once 'admin/User.php';

require_once 'app/Notify.php';

session_start();
$User = $_SESSION['UserObj'];
if (!isset($User) || $User->getAuth() == false) {
    header("Location: index.php");
}
// Роли и доступы
$RI = new RoleInterface();
$Roles = $RI->getRoles($User->getMemberId());

/*
 * Проверка на новые сообщения
 */
$NF = new Notify();

if (isset($_POST['year'])) {
    $year = intval($_POST['year']);
} else {
    $year = date("Y");
}

$PC = new ProductionCalendar();
$PC->fetchYear($year);

$date_m = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');

// открытое меню
$menu_open = 1;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Производственный календарь</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <link rel="stylesheet" href="css/dropdown-menu.css">
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse">
<div class="wrapper">
    <?php
    require_once 'menu.php';
    ?>
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <form method="post" class="form-horizontal">
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-1 control-label">Год</label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right"
                                           id="datepicker" name="year"
                                           data-inputmask="'alias': 'yyyy'" data-mask value="<?php echo $year; ?>"
                                    >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" class="btn btn-primary btn-flat" name="PushButton" value="Получить календарь">
                    </div>
                </div>
            </form>

            <div class="box box-solid">
                <div class="box-header">
                    <h4>Производственный календарь <?php echo $year; ?></h4>
                </div>

                <div class="box-body">



                <table class="table table-striped table-hover table-bordered">
                    <thead>
                        <th style="text-align: center;">Месяц</th>
                        <th style="text-align: center;">Всего дней</th>
                        <th style="text-align: center;">Количество рабочих дней</th>
                        <th style="text-align: center;">Количество выходных дней</th>
                        <th style="text-align: center;">Рабочие дни</th>
                    </thead>
                    <tbody>
                    <?php
                    $all_days = 0;
                    $work_days = 0;
                    $weekend_days = 0;

                    foreach ($PC->get() as $item) {
                        $all_days += $item['all_days'];
                        $work_days += $item['working_days'];
                        $weekend_days += $item['holiday_days'];

                        $d = explode(".",$item['date']);
                        echo "<tr>";
                        echo "<td style='text-align: right'>".$date_m[$d[0]-1]."</td>";
                        echo "<td style='text-align: right'>".$item['all_days']."</td>";
                        echo "<td style='text-align: right'>".$item['working_days']."</td>";
                        echo "<td style='text-align: right'>".$item['holiday_days']."</td>";
                        echo "<td>";
//                             foreach (unserialize($item['wd_array']) as $w) {
//                                echo $w." ";
//                                }
                        $working_days_array = unserialize($item['wd_array']);
                        $wp1 = array_splice($working_days_array, 0, (count($working_days_array) / 2 ));
                        $wp2 = $working_days_array;


                        foreach ($wp1 as $w) {
                            echo $w." ";
                        }

                        echo "<span style='color:red'>";
                        foreach ($wp2 as $w) {
                            echo $w." ";
                        }
                        echo "</span>";
                        echo "</td>";
//                        echo "<td>".$item['wd_array']."</td>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th class="text-right">Итого: </th>
                        <th class="text-right"><?php echo $all_days; ?></th>
                        <th class="text-right"><?php echo $work_days; ?></th>
                        <th class="text-right"><?php echo $weekend_days; ?></th>
                        <th></th>
                    </tr>

                    </tfoot>
                </table>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>

<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
    $(function () {
            //Date picker
            $('#datepicker').datepicker({
                language: 'ru',
                autoclose: true,
                format: 'yyyy',
                viewMode: "years",
                minViewMode: "years",
            })
        });
</script>

<script>
    $(".sidebar-menu").hover( function () {
        $('body').addClass('sidebar-collapse');

    });

    $(".sidebar-menu").mouseover(function () {
        $('body').removeClass('sidebar-collapse');
    })
</script>
</body>
</html>
